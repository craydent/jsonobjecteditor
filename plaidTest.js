const plaid = require('plaid');

  var self = this;
  var plaidClient;

  var PUBLIC_TOKEN = null;

  var ACCESS_TOKENS

  function getAT(data){
    var AT = ACCESS_TOKENS[data.bank];
    if(!AT){return false;}
    return AT.access_token;
  }

  function initPlaidClient() {
    var client_id = "57ec34dbf5c9c9795f46a523";
    var secret = "dc2dd377d986f833101da67dfbac34";
    var public_key = "5b7b648e71bc056b974036d32026db";
    ACCESS_TOKENS = {
      "chase":{
          "access_token":"access-development-655b2910-0e0d-4fde-80da-8c77aa92be87"
      },
      "wf":{
          "access_token":"access-development-5a3be1ca-0150-4e96-8a5d-38964d0f0ebf"
      },
      "corey_discover":{
          "access_token":"access-development-95426771-6f02-4c25-ab5c-7d2488f49c86"
      },
      "liz_discover":{
          "access_token":"access-development-bdd33633-ebc6-4944-a487-3552c4648fa5"
      }
      }
    //console.log('plaidinit',plaidClient);
    if(!client_id || !secret || !ACCESS_TOKENS){
      return 'plaidClient not initialized';
    }
    if(plaidClient){ return;}
    console.log('initializing plaid client')
  try {

    const configuration = new plaid.Configuration({
      basePath: plaid.PlaidEnvironments.development,
      baseOptions: {
        headers: {
          'PLAID-CLIENT-ID': client_id,
          'PLAID-SECRET': secret,
          'Plaid-Version': '2020-09-14',
        },
      },
    });

    plaidClient = new plaid.PlaidApi(configuration);
    return true;
  } catch (e) {
    console.log("plaid client init error",e);
    return e;
  }
}
async function createLink(data, request, response){
  initPlaidClient();
  const options = {
    user: {
      client_user_id: 'user-123',
    },
    client_name: 'Plaid Test',
    products: ['auth'],
    country_codes: ['US'],
    language: 'en',
  };

  try {
    const linkTokenResponse = await plaidClient.linkTokenCreate(options);
    const linkToken = linkTokenResponse.data.link_token;
    console.log(linkToken);
    const response = await plaidClient.itemPublicTokenExchange({ linkToken });
    
    const access_token = response.data.access_token;
    const accounts_response = await plaidClient.accountsGet({ access_token });
    const accounts = accounts_response.data.accounts;
  } catch (error) {
    console.error(error);
  }
}

initPlaidClient();
createLink();