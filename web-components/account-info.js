class AccountInfo extends HTMLElement {
  constructor() {
    super();
  }
  static get observedAttributes() {
    return [];
  }
  connectedCallback() {
    this.plaidid = this.getAttribute('plaid-id');
    this.start_date = new Date(this.getAttribute('start-date')).format('Y-m-d');
    this.end_date = new Date(this.getAttribute('end-date')).format('Y-m-d');
    this.acctName = this.getAttribute('acct-name');
    this.acctId = this.getAttribute('acct-id');
    this.joe_id = this.getAttribute('joe-id');
    this.render();
    var styles = `
      account-info {
        display: block;
        padding: 0;
        background: #fff;
        /* border: 1px solid #eee; */
        margin: 5px auto;
        border-bottom: 1px solid #ccc;
        padding-bottom: 5px;
      }
      acct-row {
        display: block;
        font-weight: bold;
        font-size: 14px;
        padding: 4px;
      }
      acct-row.charge{background: rgba(255,0,0,.1);}
      acct-row.payment{background: rgba(0,255,0,.1);}
      acct-row span.trans-count {
        float: right;
        font-weight: normal;
      }
    
    `;
    document.getElementById(_joe.Components.styleTag).innerHTML+=styles;
  }
  render() {
    var self = this;
    var data = {bank:self.plaidid,start_date:self.start_date,end_date:self.end_date};
    self.acctId && (data.account_ids=self.acctId);
    $.ajax({url:'/API/plugin/money/transactions?',
    data:data,
    success:function(data){
      var transUrl=`/API/plugin/money/transactions?bank=${self.plaidid}&start_date=${self.start_date}&end_date=${self.end_date}${(self.acctId && `&account_ids=${self.acctId}`)||''}`;

      if(data.error){
        this.innerHTML = data.error;
        return;
      }
      let transactions = data.transactions
      if(self.acctId && self.acctId != "undefined"){
        transactions = data.transactions.filter(t=>{
          return t.account_id == self.acctId;
        });
      }

      var summary = {
        total:{count:0,sum:0},
        charge:{count:0,sum:0},
        payment:{count:0,sum:0}
      }

      transactions.map(tran=>{
        summary.total.count++;
        summary.total.sum+= tran.amount;
        
        if(tran.amount > 0){
          summary.charge.count++;
          summary.charge.sum+= tran.amount;
        }else{
          summary.payment.count++;
          summary.payment.sum+= tran.amount;
        }
      })
      function renderRow(obj,label){
        let money = ('$'+(summary[obj].sum).toFixed(2)).replace('$-','-$')
        return `<acct-row class="${obj}">${money}
          <span class="trans-count">${summary[obj].count} ${label||'transactions'}</span>
          </acct-row>`;
      }
      self.innerHTML = 
        `<joe-subtitle>${self.acctName}</joe-subtitle>
        <joe-subtext>${self.joe_id}</joe-subtext>
        ${renderRow('charge','charges')}
        ${renderRow('payment','payments')}
        ${renderRow('total','transactions')}
        <a href="${transUrl}" target="_blank" class="joe-subtext">view transactions json</a>
      `;
    }})

  }
  attributeChangedCallback(attr, oldValue, newValue) {
    this.render();
  }
  disconnectedCallback() {}
}
// window.addEventListener('load', function(){
    window.customElements.define("account-info", AccountInfo);
// })

