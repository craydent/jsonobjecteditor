class JoeUserCube extends HTMLElement {
  constructor() {
    super();
  }
  static get observedAttributes() {
    return [];
  }
  connectedCallback() {
    var atts = _joe.Components.getAttributes(this);
    this.classList.add("joe-initials");
    if (atts.user) {
      this.User = atts.user;
      if (typeof this.User == "string") {
        this.User = _joe.Indexes._id[this.User];
      }
      this.render();
      this.classList.add("generated");
    }

    this.container = this.parentElement;
  }
  render() {
    var atts = _joe.Components.getAttributes(this);
    
    var title,initials;
    var color = this.User.color || '#cbcbcb';
    var u = this.User;
    if(u.first && u.last){
        title = u.first+' '+u.last;
        initials = u.first[0]+u.last[0];
    }else{
        var name = u.fullname || u.name;
        title = name;
        initials = name[0]+ (((name.indexOf(' ') > 0) && name[name.indexOf(' ')+1])||'');
    }
    this.title = title;
    this.style.backgroundColor = color;
    this.innerHTML = `${initials}<span>${title}</span>`;
  }
  attributeChangedCallback(attr, oldValue, newValue) {
    this.render();
  }
  disconnectedCallback() {}
}
// window.addEventListener('load', function(){
window.customElements.define("joe-user-cube", JoeUserCube);
// })
