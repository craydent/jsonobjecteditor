class JoeComponent extends HTMLElement {
  constructor() {
    super();
  }
  static get observedAttributes() {
    return [];
  }
  connectedCallback() {
    var styles= '';
    _joe.Components.appendStyles(styles);
    this.classList.add('joe-component');
    this.container = this.parentElement;
    this.render();
  }
  render() {
    var atts = _joe.Components.getAttributes(this);
   
  }
  attributeChangedCallback(attr, oldValue, newValue) {
    this.render();
  }
  disconnectedCallback() {}
}
// window.addEventListener('load', function(){
    window.customElements.define("joe-component", JoeComponent);
// })

