function getComponentAttributes(comp){
  var d = {};
  for (var a = 0; a < comp.attributes.length; a++) {
    d[comp.attributes[a].name] = comp.attributes[a].value;
  }
  return d;
}
function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  var cookie_string = cname + "=" + cvalue + ";" + expires + ";path="+location.pathname+location.search;
  //var cookie_string = cname + "=" + cvalue + ";" + expires + ";";
  document.cookie = cookie_string;
}

class ReportListItem extends HTMLElement {
    constructor() {
      super();
    }
    static get observedAttributes() {
      return [];
    }
    connectedCallback() {
      this.classList.add('report-list-item')
      this.container = this.parentElement;
      this.render();
    }
    render() {
      var atts = getComponentAttributes(this);
      if(this.hasAttribute('toggleable')){
        this.onclick = this.toggleItem;
        if(window && window.localStorage){
          var completed = (window.localStorage.getItem('completeItems')||'').split(',');
          if(completed.indexOf(this.id)!= -1){
            this.classList.add('strikethrough');
          }
        }
      }
    }
    toggleItem(){
      this.classList.toggle('strikethrough');
        var completed = []; 
        document.querySelectorAll('report-list-item.strikethrough').map(dom=>{
          completed.push(dom.id);
        });


        // Create or update the cookie:
        if(window && window.localStorage){
          window.localStorage.setItem('completeItems',completed.join(','))
        }
      

    }
    attributeChangedCallback(attr, oldValue, newValue) {
      this.render();
    }
    disconnectedCallback() {}
  }
      window.customElements.define("report-list-item", ReportListItem);

  
  