class JoeCard extends HTMLElement {
  constructor() {
    super();
  }
  static get observedAttributes() {
    return [];
  }
  connectedCallback() {
    var atts = _joe.Components.getAttributes(this);
    var styles = `
      joe-card{
        display:block;
        position:relative;
        box-sizing:border-box;
        padding:5px;
        border:1px solid #eee;
        background-color:#fff;
      }
      .joe-panel-content-option-expander joe-card{
        margin-bottom:5px;
        background:#fcfcfc;
      }
      joe-card > joe-title {
        border-bottom: 1px solid #f2f2f2;
        margin: 0 -5px;
        padding: 0px 5px 5px 5px;
        background:#fff;
      }
      joe-card.w50{
        width: calc(50% - 3px);

        display: inline-block;
      }
    `;
    _joe.Components.appendStyles(styles);
    
    this.item = this.getJoeItem(atts.item || atts.itemId || atts.item_id);
    //var schemaname = atts.schema || this.item.itemtype;
    this.schema = _joe.schemas[(atts.schema || this.item.itemtype)];

    
    this.render();
    this.classList.add("joe-card");
  }
  getJoeItem(itemid){
    return _joe.getDataItem(itemid)
  }
  render() {

    var atts = _joe.Components.getAttributes(this);
    if(!this.schema){
      return;
    }
    var card_template = `<joe-title>${this.item.name}</joe-title>`;
    this.cardTemplate = (this.schema.card && _joe.propAsFuncOrValue(this.schema.card,this.item));
    if(this.innerHTML){

    }


  }
  attributeChangedCallback(attr, oldValue, newValue) {
    this.render();
  }
  disconnectedCallback() {}
}
// window.addEventListener('load', function(){
window.customElements.define("joe-card", JoeCard);
// })
