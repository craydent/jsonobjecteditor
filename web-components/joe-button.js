class JoeButton extends HTMLElement {
  constructor() {
    super();
  }
  static get observedAttributes() {
    return [];
  }
  connectedCallback() {
    this.schema = this.getAttribute('schema');
    this.classList.add('joe-button')
    this.initialHTML = this.innerHTML.trim();
    //var initialText = this.innerText.trim();
   // this.newText = this.innerText.trim() && `<jb-text>${this.innerText.trim()}</jb-text>` ||'';
    this.render();
  }
  render() {
    var atts = _joe.Components.getAttributes(this);
    var classesToAdd = [];
    var icon;
    var newHTML = '';
    if(atts.schema){
      this.schema = atts.schema;
      icon = _joe.schemas[this.schema] && _joe.schemas[this.schema].menuicon;
      icon && classesToAdd.push(`joe-svg-button`);
    }

    atts.color && classesToAdd.push(`joe-${atts.color}-button`);
    atts.icon && classesToAdd.push('joe-iconed-button',`joe-${atts.icon}-button`);
    var actionPreset = false;
    if(atts.action){
      var actString = '';
      switch(atts.action){
        case 'create':
        case 'new':
          var actKey = "create"
          //this.classList.add(`joe-${atts.action}-button`)
          classesToAdd.push(`joe-${actKey}-button`);
          if(atts.schema){
            actString = `_joe.Object.${actKey}('${atts.schema}')`;
            newHTML = `new <b>${atts.schema.toUpperCase()}</b>`;
            classesToAdd.push('joe-iconed-button',`joe-plus-button`);
          }
          actionPreset = true;
        break;
        case 'preview':
        case 'view':
          let prefix= atts.prefix ||'';
            actString = `_joe.gotoFieldURL(this,'${prefix}');`;
            //actString="_joe.gotoFieldURL(this,\''+prefix+'\');"
            newHTML = `<jb-text>${atts.action} <b>${atts.schema.toUpperCase()}</b></jb-text>`;
            classesToAdd.push('joe-iconed-button',`joe-view-button`);
            actionPreset = true;
        break;
        default:
          actString = atts.action;
        break;
      }
      this.setAttribute('onclick',actString);
    }
     
    var guts =  actionPreset?(newHTML|| this.initialHTML):(this.initialHTML || newHTML);
    this.innerHTML = (icon||'')+guts;
    /*this.innerText = '<button-text>'+this.innerText+'</button-text>';*/
    this.classList.add(...classesToAdd);
  }
  attributeChangedCallback(attr, oldValue, newValue) {
    this.render();
  }
  disconnectedCallback() {}
}
// window.addEventListener('load', function(){
    window.customElements.define("joe-button", JoeButton);
// })

