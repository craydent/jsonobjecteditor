class JoeField extends HTMLElement {
  constructor() {
    super();
  }
  static get observedAttributes() {
    return [];
  }
  connectedCallback() {
    this.fieldName = this.dataset.name;
    this.fieldType = this.dataset.type;
    this.fieldProp = _joe.Fields.get(this.dataset.name);
    this.input = this.parentElement.querySelector('joe-field > input');
    if(this.fieldProp){
      this.initField();
    }

    this.container = this.parentElement;
    this.render();

  }
  initField(){
    if(this.fieldProp.callback){
      var callback = this.fieldProp.callback;
      switch(this.fieldType){
        case 'text':
        case 'number':

        this.input.addEventListener('blur',function(e,v){
          callback(this.value,e);
        })
        break;

      }
    }

  }
  render() {
    this.classList.add('joe-object-field')
    this.classList.add('wc')
    var atts = _joe.Components.getAttributes(this);
   
  }
  attributeChangedCallback(attr, oldValue, newValue) {
    this.render();
  }
  disconnectedCallback() {}
}
// window.addEventListener('load', function(){
    window.customElements.define("joe-field", JoeField);
// })

