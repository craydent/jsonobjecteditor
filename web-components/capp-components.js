var cappWrapper = document.querySelector("capp-wrapper");
class CappView extends HTMLElement {
  constructor() {
    super();
  }
  static get observedAttributes() {
    return [];
  }
  connectedCallback() {
    var self = this;

    // create a simple instance
    // by default, it only adds horizontal recognizers
    self.addEventListener("click", function() {
      capp.special.joeicon(false);
    });
  }
  render() {
    var atts = _joe.Components.getAttributes(this);
  }
  attributeChangedCallback(attr, oldValue, newValue) {
    this.render();
  }
  disconnectedCallback() {}
}
window.customElements.define("capp-view", CappView);

class CappDashboard extends HTMLElement {
  constructor() {
    super();
  }
  static get observedAttributes() {
    return [];
  }
  connectedCallback() {
    var self = this;

    // create a simple instance
    // by default, it only adds horizontal recognizers
    self.addEventListener("touchstart", function() {
      if(cappWrapper.classList.contains('small-size')){
        capp.special.joeicon(false);
      }
    });
  }
  render() {
    var atts = _joe.Components.getAttributes(this);
  }
  attributeChangedCallback(attr, oldValue, newValue) {
    this.render();
  }
  disconnectedCallback() {}
}
window.customElements.define("capp-dashboard", CappDashboard);

/* CAPP-PANEL */
class CappPanel extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    var self = this;
    var mc = new Hammer(this, { threshold: 20 });
    mc.on("panleft panright", function(ev) {
      switch (ev.type) {
        case "panleft":
          capp.special.joeicon(false);
          break;
        case "panright":
          capp.special.joeicon(true);
          break;
      }
    });
  }
}
window.customElements.define("capp-panel", CappPanel);
