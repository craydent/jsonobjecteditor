class CappPanel extends HTMLElement {
  constructor() {
    super();
  }
  static get observedAttributes() {
    return [];
  }
  connectedCallback() {
    var self = this;

    // create a simple instance
    // by default, it only adds horizontal recognizers
    var mc = new Hammer(this,{threshold:20});

    // listen to events...
    mc.on("panleft panright", function(ev) {
      switch(ev.type){
        case "panleft":
          capp.special.joeicon(false);
        break;
        case "panright":
          capp.special.joeicon(true);
        break;
      }
    });
  }
  render() {
    var atts = _joe.Components.getAttributes(this);
   
  }
  attributeChangedCallback(attr, oldValue, newValue) {
    this.render();
  }
  disconnectedCallback() {}
}
// window.addEventListener('load', function(){
    window.customElements.define("capp-panel", CappPanel);
// })

