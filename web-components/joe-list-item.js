class JoeListItem extends HTMLElement {
  constructor() {
    super();
    //   var styles = `
    //   swipe-option{
    //     position:absolute;
    //     top:0;
    //     width:100px;
    //     bottom:0;
    //     backgorund:#ccc;
    //     display:none;
    //   }
    //   .ui-draggable-dragging swipe-option{
    //     display:block;
    //   }
    // `;
    // document.getElementById(_joe.Components.styleTag).innerHTML+=styles;
  }
  static get observedAttributes() {
    return [];
  }

  renderIcon(){
    this.icon = this.getAttribute('icon');
    if(_joe.schemas[this.icon] && _joe.schemas[this.icon].menuicon){
      this.content.innerHTML = _joe.schemas[this.icon].menuicon + this.content.innerHTML;
      //return _joe.schemas[this.icon].menuicon;
    }
  }
  connectedCallback() {
    var self = this;
    this.schemaObj = _joe.schemas[this.getAttribute('schema')];
    this.itemId = this.getAttribute('itemId');
    this.idprop = this.getAttribute('idprop');
    this.content = this.querySelector('.joe-field-item-content');
    this.initial_onclick=this.content.getAttribute('onclick');
    this.checkbox = this.querySelector('joe-checkbox');

    this.renderIcon();

    if(this.schemaObj && this.schemaObj.onswipe){
      let rightOption = this.schemaObj.onswipe.left;
      let leftOption = this.schemaObj.onswipe.right;
      var l,r;
      if(leftOption){
        l = document.createElement("swipe-option");
        l.className = "left-side";
        l.innerText = leftOption.name;
        l.style.backgroundColor = leftOption.color || '#ccc';
        this.appendChild(l);
      }
      if(rightOption){
        r = document.createElement("swipe-option");
        r.className = "right-side";
        r.innerText = rightOption.name;
        r.style.backgroundColor = rightOption.color || '#ccc';
        this.appendChild(r);
      }

      // this.innerHTML+=`
      //     <swipe-option class="left"></swipe-option>
      //     <swipe-option class="left"></swipe-option>

      // `;

      $(this).draggable({
        axis:'x',revert: true,scroll: false,
        start:function(e,t){
          self.content.removeAttribute('onclick');
          //self.content.removeEventListener('click',this.initial_onclick)
        },
        drag:function(e,t){
          var drag = t.position.left;
          var threshold = this.schemaObj.onswipe.threshold || .6;
          if(Math.abs(drag) > 20){
            self.beingSwiped = true;
            
          }
          if(Math.abs(drag) > e.target.width()*threshold){
            self.classList.add('swiping');
          }else{
            self.classList.remove('swiping');
          }
          console.log(drag);
          //self.content.removeEventListener('click',this.initial_onclick)
        },
        stop:function(e,t){
          
          var drag = t.position.left;
          var dir = '';
          var threshold = this.schemaObj.onswipe.threshold || .6;
          if(Math.abs(drag) < 20 && !self.beingSwiped){
            switch(typeof this.initial_onclick){
              case "string":
              eval(this.initial_onclick);
              break;
              case "function":
              this.initial_onclick(e.target);
              break;
            }

          }
          else if(Math.abs(drag) > e.target.width()*threshold){
            dir = (drag>0)?'right':'left';
            this.schemaObj.onswipe[dir] && this.schemaObj.onswipe[dir].action && this.schemaObj.onswipe[dir].action(_joe.Indexes[self.idprop][self.itemId],self);
            logit(dir+' '+drag);
          }
          self.content.setAttribute('onclick',this.initial_onclick);
          self.beingSwiped = false;
          self.classList.remove('swiping');
          //self.content.addEventListener('click',this.initial_onclick);
        }
      });
    }

    this.container = this.parentElement;
    this.render();
  }
  render() {
    var atts = _joe.Components.getAttributes(this);
   
  }
  attributeChangedCallback(attr, oldValue, newValue) {
    this.render();
  }
  disconnectedCallback() {}
}
// window.addEventListener('load', function(){
    window.customElements.define("joe-list-item", JoeListItem);
// })

