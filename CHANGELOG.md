## CHANGELOG

### 0.10.300
	- initial aws fix for bucketname
	310 - added tag_id to tags
	311 - fixed undefined icon bug in autocomplete
	312 - sourcemap fixed, status subset bug fixed.
	
### 0.10.200
	227 - no product user select
	226 - lists added new default filter
	225 - fixed list issues, capp toggle 
	224 - manifest.json & capp menu
	223 - list custom items updated
	220 - updated user, added ability for manifest.js
		- updated AWS uploaders
	210 - touchpoint updated
	201 - fixed window.set
	200 
	- MemberRegistry takes makeSet:bool||STR and shows count (reqs fields)
	- recipes get family_favorite boolean

### 0.10.120
	100 - Added initiatives
	110 - capp updates for no user select
	120 - updated referencedBy template to include date or blank.
	Removed RTC fix, first clean build.
### 0.9.901
	Ledger Updates 2021 Q4
	fixed ledger css
	- 901 Ledger now properly handles biweekly transactions.
### 0.9.891
	fixed notes bug
	81 - fixed checkChanges bug with objectReferences and chrckbox groups
	82 - project goals filter updated
	83 - Projects have subtasks and priorities
	object references are now labeled with the type of thing they are.
	84 - filesize in uploads
	85 - custom list items
	86 - updated standard list report
	87 - itemmenu css mobile fix
	88 - 2021 in ledger (default), main added to recipe type
	89 - bigger/smaller/margin-10 css styles.
	891 - width50 joe-card

### 0.9.79 Autocomplete and List Updates
	Autocomplete can now work with simple strings
	List updates to account for grocery
	bug fixes
	71 - fullscreen on double click
	72 - updated grouptype to  group_type
	73 - reports in groups / goals
	74 - added epic to task types
	75 - fixed a caching issue
		added subsets to report
	76 - added milestones to key features.
	77 - code editor can be fullscreened
	78 - objectreferences openable in new tab.
		single items can be included for a user
		callback(val,evt) added to text fields on blur
		reports have id, these can be used instead of _id
	79 - goal has item expander
		considerations > thoughts and updated

### 0.9.69 Instance Updates for chores app for Chores App
	pending tasks show properly.
	members used as filters for tasks and instances
	checkbox comes through to instances
	- 61 Project updated to include data for briefing and reports.
	- 62 added complete to deliverables status
	- 63 key features and question flag added to considerations.
	- 65 goal schema created and added to project
	- 66 Goals refined
	- 67 field styling updates
	- 68 added priorities to goals.
	- 69 bug fix to mealplanner schemas so scroll works on mobile.
		updats to mealplanner dashboard

### 0.9.59 Group Auth
	- 56 added instance date_str and subsets
		- scorecard: basic js and css complete.
		- subsets and filters have counts
		-scorecard has all data in it.
	- 57 authorization at the group level
		project schema expansion and updates
	- 58 Pending instances list in projectboard
	- 59
		chore scorecard available works.
		Fixed API/save + API/save/itemid
		added jquery 3.5.1
	


### 0.9.054 Instances
	- instances can show custom fields
		- add instance.fields to a schema
	- schemalist available in __jsc
	- 051 added icon to default objectReference list option
	- 052 Render.schema_icon() && bug fixes
	- 053 reward and instance bug fixes.
	- 054 bug fix

### 0.9.041 Aggregator and Filter Options datasetProperty, OnPanelShow
	- add aggregator prop to schema to run functions and show data in list view
	- _joe.Filter.Options.datasetProperty(dataset,propertyName,specs)
	- 1 fixed sorting issue
	-//, transactions aggregator, no date range.
	- 2 _joe.Filter.Options.getDatasetPropertyValues(dataset,propertyName,specs)
	- 010 Sections expansion, tasks better
	- 020 new task list report. also reports in schemas
	- 030 onPanelShow can be stacked (schema and joe specs)
		onPanelShow for fields (does not fire on rerender)
	-040 financial accounts have types (financial_account_type)
		schema update has delay (dev server)
	-041 list can make cards list, fills extra slots.

### 0.8.990 Polyfills and remote forms
	- webcomponentspolyfills for joe in ie (ie still has some jank).
	- joeinclude updated
	- 1 2020 money update
	- 2 block fixes
	- 3 - "Refactor money schemas" NPM audit fix

### 0.8.980 Form Builder Updates pt 2
	- sorted named fields properly in form
	- added form submission icon
	- 4 fixed web dir for joe web components load 
	
### 0.8.97x Form Builder Updates
	- form UI and submission updates in joe
	- added submission_date_field to form 
	- questions can now be used as date or used as visitor id
	- $J.search(queryObj)
	- includes added to report/form and reportbuilder/formbuilder
	- update to handle PORT better
	- fixed form reference to be more robust
	- question udpated with value_template
	- FORM SUBMISSIONS can now upsert based off visitor and submission_date match
	- 9: form fix for single port includes.



### 0.8.96x Report WEb Components (for Grocery List)
	- grocery list alpha
	- 961 report now has web components
	- report-list-item (toggleable)
	- 962 list updates for mealplanner 
	- 963 added spending history to budget, start and end dates in transaction

### 0.8.95x $J (universal) Shorthand JOE
	- $J.get(itemId)
	- 952: sites bug fixes: set in sites, sorted blocks
	
### 0.8.93x Workflow Cleanup
	- 930 history can be shaped with fields, has indexes
	- no more blank recently updated
	- 931 report styles and builder robustness
	- 932 new report icon
	- checkboxes can be set to default:true
	- 933 possibly fixed item duplicate
	- joe-user-cube web component added, removed shadows
	- 934 joe-card beta web component

### 0.8.92x Swipe and fields API and buttons
	- swipe capp-panel to toggle on mobile
	- API/item and API/object(new) take fields query param for object property projection
	- API/Object takes field param for single property
	- 921 capp app menu multi column on mobile
	- 922 fix joe autocomplete filltemplate
	- 923 switch list view update create button to new "schema" button
	- 924 add description to list
	- 925 Task report fucntion test

###0.8.912 Joe input custom checkbox
	- custom checkbox UI
	- small joe button properly hides text label
	- 911 fixed task user select.
	- 912 updated project, added references.
	- item checkbox can have action function added for on toggle

###0.8.901 Auto Complete Component
	- bug fixes
	- 901 fixed objectref list item bug
	
###0.8.89x List Items Component
	- added joe-list-item web-component
	- swipe that web-component
	- bug fixes
	- 891 fixed swipe direction
	- 892 fixed alt+n and quickAdd new item.
	- tabbedPanel added
	- _joe.getData(specs) function
	- 893 fixed clicking issue on checkbox

###0.8.88x CAPP Updates and smoothness
	- CAPP UI updates, recentlyUpdated card
	- bug fixes in idprop
	- 881 added filters as attribute to recentlyUpdated
	- 882 bug fix for inpu losing focus in capp
	- 883 updated JOE.geMode() to be more accurate, fixed bug with saving

###0.8.87x OAUTH2 Support
	- requires auth plugin or url
	- add params for authorization(header(bearer),url,name)
	- 871 merge changes.
	- 872 authorization.type = "google" now works. requires client_id and client_secret instead of header

###0.8.86x JOE button web component && finish modules in sites
	- 860 joe-button can now be used as custom dom element; set schema, action("add",[STR]), icon("plus"), color("orange") attributes
	- 861 module content_type option added to page and block
	- 862 fixed button links, added view/preview action to joe-button
	- 864 Fix Joe buttons on mobile
	- 865 ledger shows acct to and from.

###0.8.85x JS Literal Templates
	- 850 added module (js literals to reports as template option)
	- util function to make dynamic modules.
	- cache faster with findById
	- page / site urls fixed for PORT
	- 851 svg buttons proper css with for joe-svg-button and joe-iconed-button
	- add me to this for members.
	- 852 big buttons fixed
	- 853 custom styles for users

###0.8.84x CAPP & UI Updates
	- 840 added line chart for timeline 
	``` this.Chart.timeline(valueProp,dateProp,schemaname,cappid,specs) ```
	- 841 turned off cards style by default, reduced inset shadow
	- fonts adjusted, more consistent font/theme colors 068, 404040
	- 842 member, setting and user style updates.
	- 843 JSON in updated field
	- added lock to _protected items in list
	- access token for supers only


###0.8.83x Sites bug fixes
	- 830 blocks can now properly take dynamic data with foreach

###0.8.82x money updates
	- 820 account web-component
	- close filters and dates content section
	- 2019 subset
	- 821 socket fixes
	- 822 money fix
	- 823 extendfield overwrite fix. (status)
	- 824 subset selection bug fix

###0.8.81x https full support
	- 810 default site can be set as "" in url
	- 811 socket uses https when necessary

###0.8.800 single port mode
	- set clusters to 0 for single threaded ops
	- port and socketPort can now be combined
	- upgraded socket.io
	- sites now runs on same port as well.
	- set master http port with PORT

	
###0.8.75x
	- 750: added indexes to mongo

###0.8.74x
	- 740: added duplicate button to app template.


###0.8.70x
	### static webdir
	- /* == /_www/
	- added new ledger features
	- RTC- plan-b


###0.8.60x
	### Renderizer integration
	- /RENDER/contentType/templateName
	
###0.8.55x
	### added Indexes & Print
	- should speed grabbing a data item
	- alt + P to print panel content
	- 1 ledger money format and css updates

###0.8.53x
	### Do Today Button
	- added do today button totask detail view
	- added active flag for statuses
	- 1 acceptance criteria listobject template

###0.8.52x
	### Project Management updates
	- added smart parent task to task field.
	- 1 added acceptance criteria to task
	
###0.8.51x
	### Styles and Section anchors
	- Section anchors now have property anchor
	- _updated field now alerts to new item
	- 1 added url and dessert to recipe
	- 2 unSaved icon/button in main panel recipe sidebar added
	
###0.8.50x
	### Autosave
	- Indicator (*) in the document title, use specs.autosave = number|boolean
	- 1 specs.autosave is the value to be used as interval if ot boolean
	- 2 pb bug fixes
###0.8.46x
	### Formbuilder Fixes/Updates
	- https works properly with cdeploy and proxy
	- forms now have visitor id field
	- formbuilder can now be set to use form.save_submission for default handling
	- updated form, question and submission schemas
	- added submission to sitebuilder schemas
	- 1 schema scroll-in fix & css for details view sections
	- 2 css updates
	- 3 added placeholders to text inputs (text,date,number), remove with placeholder = false 
	- 4 ledger list sort, added notification to default schemas
	- 5 cost in projects
	- 2018 year ledger updates
	- 7 standard templat eupdates, added status
	- 8 css fixes
	- 9 Add Task breakdown chart to project

###0.8.45x
	### Ledger updates and onload functions		
	- 1 protected items and reports
	- 2 fixed bug with objectlist sorting and templates
	- 3 updated field added to more schemas
	
###0.8.44x
	### Cards Style, MealPlanner
	- specs.style.cards now enables card styling on details page
	- mealplanner alpha (ingredient, recipe, meal*)
	- 1 search performance optimization for larger datasets (task, transaction)
	- objectlist field zoom fixes (-date field added number)
	- 2 minor text updates and bug fixes
	
###0.8.43x
	### UX Updates, dashboard
	- 1 task column count hot fix
	- 2 bug fixes, money
	- 3 ledger first save gets ending balance fix
	- 4 post gets files
	- 5 added financial_account
	- removed document.write from joe_include
	- 6 project board gets "my work"
	- half card sizes
	- added moment.js
	- 7 default schemas added to menu
	- 8 projectboard, my work updates and ui/x
	- render.itemcheckbox added

###0.8.42x
	### new charts for CAPP / export CSV button / API datasets enhance
	- 1 npm error
	- 2 get random colors fix
	- 3 docs page updates
	- 4 export CSV in stats card (joe.Export.csv(schemaname))
	- 5 API updates, added counts to datasets api
	- 6 search keywords persist across back btn (includes quick find)
	- 7 optimize subset 
	- 8 optimize filters
	- 9 mobile updates, app-label sizing + single column list view by default.

###0.8.41x
	#### filter and subset menu updates,tabbed panels
	- selectable filter/subset group labels (use name + filter properties)
	- 1 localstorage updates
	- ui fixes
	- 2 utils.getPossibleValues
	- 3 double click section anchors to switch to tabbed mode (desktop only)
	- colcount fix
	- 4 dblclick section labels to focus 
	
###0.8.40x
	#### CHAT COMES to JOE
	- 0 Connect video chat
	- cleaned up caught bugs
	- 1 integrating into UI
	- 2 video chat now works without triple click waiting
	- 4 bug fixes,adapter https
	- 6 CONNECT hardening.
	- 7 mem in app.stats card
	
###0.8.33x
	#### smarter blocks and mysql support
	- 0 blocks can be made into templates
	- blocks can have their own includes, or templates
	- 1 mysql support
	- 2 block templates expanded: fields and can render all sub props, accessible from templates
	- 3 block usage, subsets and sorters
	- block tempalte info in editor, 
	- 4 sql disparate connections fixed
	- 5 layout section grab null bug fix
	- increased code area size in blocks
	- 6 added site to blocks and layouts as optional
	
###0.8.32x
	- 0 page editor out of alpha
	- fixed locked subprop effecting objectlist bug
	- blocks from pages and sections all show up nicely
	- editor now shows content and sections
	- dynamic pages work with blocks.
	
###0.8.31x
	- 0 jpe-editor
	- fixed uploader legaxy server_url bug
	- 1 editor updates
	- 2 sites updates
	- 3 filetypes for uploader fixed
	
###0.8.30x
	- new uploader allows for multiple.
	- google auth JAC fix.
	- 1 bug fix to allow any filetype to be uploaded
	- 3 smarter ports for aws connect
###0.8.28x
	- 0 updated required field message and syntax
	- comment field update
	- 1 status field shows when updated
	- 2 Daylight Savings Time timezone_offset hotfix
	- 3 Scroll panel icons
	- panel header buttons dom element and array

###0.8.27x
	- new GET/SET shortcuts for object-mode
		- joe.Get(property)
		- joe.Set(property,value)
	- tinymce cleanup and fullscreen
	- 1 tags filter for money list sidebar updated
	- 2 time field fixed, google integration hardened
	- current.constructed fixed
	- 3 objectrefence now takes reference_specs for properties
	- 4 assign to me btn
	- joe-svg-button class
	- 5 google auth fixed (reduced) and can view calendar from list
	- calendar double-sync fixed
	- 6 correctclockskew added to aws
	- 7 boolean field label more clickable
	- objectlist headers now sortable
	- 8 added active property to statuses, currently using with task subset
	- 9 svg button css fix, "find" btn > "search" btn

###0.8.267
	- added dashboard icon/btn 
	- fixed erroneous blank array to blank array change check
	- schema onload event
	- turned off debug mode on template
	- reportbuilder and page updates.
	- user organized, has description
	- ledger icons
	- filter/subset organization
	- bug fixes
	- event published_ts timestamp prop added to calendar events on publish
	- fixed objectlist rerender bug
	
###0.8.251
	- subset/filter groups
	- integrated svg icon in field label feature

###0.8.24x
	- JAC Menu
	- fixed item count on mobile issue
	- filters can be stripe of bg uses specs
	- capp card and title updates
	- bordercolor feature added to options
	- capp sizing and response updates
	- report builder flexibility
	- joe-overlay dom
	
###0.8.232
	- added event template setting GOOGLE_EVENT_TEMPLATE
	- apps cssclas fix
	- event schema updates
	- Utils.Settings parses Functions
	- Settings schema fixes
	
###0.8.220
	- JAC aprt 1
	- aws from fix
	- callback on objectreference add/remove

###0.8.200
	- attendees in google calendar integration of events
	- events are default schema
	- standard report in project list
	
###0.8.191
	- minor Feature: pup/unpub button on gCal
	- bug fixes and optimization
	- report template updates
	- date in event subsets bug fixes
	
###0.8.175
	- minor Feature: anchor sections dropdown on mobile
	- fixed recurring transactions bug on year end
	- calendar auth and workflow fixed
	- button added to platform dashboard
	- event list view cleanup (checkbox)
	- Field.rerender now takes a value obj

###0.8.162
	- FEATURE: Google Calendar Integration
	- use setting GOOGLE_CLIENTID and GOOGLE_CALENDARID
	- statuses reflect color: added containercolor to fields
	- minor feature:alt controls for list view
	- fixed filters disappearing after sort/subset bug 
	- ledger year end bug fix 
	- template google api connect check fix.
	
###0.8.135
	- calendar.js plugin
	- ics integration into events
	- boolean erroneous change message fixed
	- tasks get project users
	- icons
	- round users
	- task optimization (itemexpander,details view, sidebars)
	- the new subtask / objectlist (mobilness + templates)
	- color-coded filterbar options
	
##0.8.004 
	- capp header redesign
	- fixed bugs
	- added notes to project
	- fixed task report bug
	- joe header refresh
	- added always visible prop to date
	- subobjectfield updates

###0.7.999 - Nov 26
	- updated header and panel interaction
	- date field zebra updates (subobject and regular)
	- basic site bug fix
	- JOE Logo sidebar - automatically close when I click out
	- fixed apostrophes in subsets and tasks
	- added new window icon

####0.7.993 - Nov 21
	- capp-panel and datepicker ui updates
	- user permission fixes
	- event schema updates (conflict logic)
	- board schema updates

####0.7.991 - Nov start
	- autoreinit apps module
	- app_home ready
	- latest craydent v
	- bug fixes
	-proxy fixes

####0.7.982 - Nov start
	- Dynamic Pages
	- schema info and apps desc first pass.
	- styles for core fields,
	- default schemas added
	- new item check for create fields.
	- added tags to task
	- capp css and js updates
	
*0.7.970 - october end
	- new capp feature: app info card
	- icons

*0.7.963 - october 23
	- ondemand item expander
	- fixed mobile full-screen scrolling glitch
	- header icons
	- file field in image uploader uploader v2.0.0
	
*0.7.953 - mid october 3
	- capp + joe styles
	- Apps quick add updated
	- send notification can take array
	- unbuged notes
	- notification list updates
	- updated select field minwidth
	- usercubes extended
	- !craydent rollback

*0.7.940 - mid october 2
	- note schema
	- new window from panel buttons
	- create from left panel
	- unbugged group field (pt1)
	- close window on no hash
	- tinymce sizing in note
	- notification_code

*0.7.930 - enhancements
	- updated report styles
	- updated task standard report
	- enter key adds another
	- sidebars can be hidden (and labels)
	- left-panel for capp-view margin
	- better messaging from schema events.
	- proper schema numbering in menu

*0.7.910 - enhancements
	- updated report styles
	- updated task standard report
	- enter key adds another

*0.7.900 - Schema Events
		schema.events:{  
        'create':function(item,specs){
            console.log('project'+item.name+' created :'+specs.timestamp);
        },
        'save':function(item,specs){
            console.log('project'+item.name+' saved');
        },
        'status':function(item,specs){
            console.log('project'+item.name+' status :'+specs.status.name);
        },
        'delete':function(item,specs){
            console.log('project'+item.name+' deleted');
        },
    }

	~ bug fixes
	~ craydent duplicate
	- submenu fixes
	- craydent 1.9.2

*0.7.860 - projectboard updates and cache statics
	- report updates, projectboard updates, added group to default schemas.
	- toDateString
	- cache statics

*0.7.850 - Server Updates and Naming Conventions
	bug fixes - missing schems in platform,money start_date,finished apps split
	APPS - resave user apps to update privileges
		questionaire > forms
		VEM > vem

*0.7.841 - Inventory V1
	bug fixes - missing schems in platform,money start_date,finished apps split
	capp-counter
	updated report styles: tasks have project and strikethru
	capp app title is now apps button.
	inventory plugin
	capp updates
	https available for local
	globally available benchmark statistics.
	improved non-keyword listing performance
*0.7.800
	capp-counter
	updated report styles: tasks have project and strikethru
	capp app title is now apps button.
	inventory plugin
	capp updates
	https available for local
	globally available benchmark statistics.
	improved non-keyword listing performance

*0.7.742 - Money Updates and server app bug fix
	Money ux
	input text Field UX
	craydent 0.6.10
	search bar mobile sizing
	capp width updates, state core field.

*0.7.731 - intro of quick links in app dashboard
	Misc Updates
	CAPP updates
	sorting bug fix
	joe.html page
	transaction start/end dates
	money bug fixes

*0.7.720 - Password Reset
	PASSWORD_RESET_MESSAGE setting
	sendNotification from plugin with notificationID

*0.7.705 - Notifications
	notifier plugin and notification schema

*0.7.61 - geoPoly
	bugfixes, geo now has geometrytype poly

*0.7.53 - awsConnect Plugin
	easily connect to AWS S3 using the AWS plugin
	52 - fixed server bug in awsConnect and remote server
	53 - fixed SERVER.Plugins.awsConnect function

*0.7.47 - Money updates + Permissions

	transactions with irregular reccurence and end dates (think shopping at the same places)
	41 - supers can set app Permissions, make sure first user is a super.
	42 - bug fix
	43 - colors, utils and logout
	47 - permissions unbugged
	
*0.7.3 - Workflow
	workflow and money fixes, stripeColor can take {title:'',color:''}

*0.7.241 - New Fields
	7241 - member login updates, clear cookies
	723 - form, include and _joeinclude bug fixes
	722 - "remote" added to plugins formBuilder and memberRegistry capp scroll bar removed
	QRcode(online) and time
	721 - memberRegistry update fix

*0.7.1 - Members Plugin
	0712 - update method
	0711 - name the joe
	profile view,login, search
	

*0.7.0 - Members Plugin
	memberRegistry
	bug fixes
	
*0.6.93 - Members Part 1
	0693 - icon styles
	692 - Geo Field bug fix
	691 - license updates
	fieldnames in forms
	apps internal dir watched

*0.6.82 - Tags and Quickfind/QuickAdd and hotkeys
	682 - Cache reload for supers
	681 - next and prev hot keys + _joe.quickAdd(itemtype)
	use core field 'tags', _joe.quickFind()

*0.6.7 - Capp Popups
	these are fun, also capp-popup-option, bug fixes

*0.6.6 - Configurable Layouts and Generic Reports Fields


*0.6.5 - Save Checks + Mongodb reconnect
	helps user not loose work.

*0.6.49 - Full Craydent Templating
	649 - prettyPrintDTS
	646,647,648 - save bug fix attempt
	645 - relative url updates
	644 - fixed form preview url bug
	643 - customizable dashboard tempalte head, use setting > dashboard_head
	user roles


*0.6.32 - Statuses + Plugins
	632 - bug fixes
	631 - plugins can be in page layouts
	form builder now a plugin
	ux improvements
	status added as default schema

*0.6.2 - Plugin Pages
	select content_type "plugin" under page schema, test with callbackTester
	dashboard fixes
	project phase polish

*0.6.1 - Tasks and Projects get Users
	bug fixes

*0.6.01 - Reports Plugin
	601 - form fields can use datasets fix,  schems dir error fix
	/API/plugin/reportbuilder?reportid,content
	report schema
	
*0.5.9 - Dashboard Quick Add and Project Phases
	JOE.Apps.Cards.quickAdd(schema_name),
	
*0.5.8 - JOE.webconfig.default_schemas & JOE.Apps.Cards.systemStats() added
	can now use thes in app and schema file creation
	also, updates to the money app.
	
*0.5.7 - Comments plugin and fieldtype added
	71 - finished initial version of money. 
	use field.type "comments" works out of box with joe-server
	
*0.5.6 - Plugins can be accessed via api
	especially for forms

*0.5.53 - Apps and plugins made editable by user in the associated folders
	(created on first run)
	51,52 - bug fixes for app collections and schema loading dir
	53 - fixed user schema reloading bug
*0.5.4 - forms almost fully viable. (questionnaire)
*0.5.31 - Forms can now be constructed and previewed with sections. This is still beta
	use app 'questionnaire'
	question schema fix.
*0.5.2 - Datasets added to sites side and joe admin
	use field "datasets"
*0.5.1 - Customizable Init (fix)
	var j = require('json-object-editor)(config);

*v0.4.41
fixed login cookie bug
projectboard now shows projects recently worked on
project completed tasks also link to task.

*v0.4.4
cookie based user stays between logins.
server and client know user info 

*v0.4.3
clustering available (beta)
defaults to no password

*v0.4.21
capp.js dashboard bug fix


*v0.4.2
icons added, bug fixes.

*v0.4.1
files directory added. defaults to /_files/_
*V0.4.0
now ready to be loaded as a node require.