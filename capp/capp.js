function CraydentApp(specs,render){
    if(typeof specs == "boolean"){
        specs = {autoInit:specs}
    }
    var renderQueue = render?$.type(render) == "array"?render:[render]:[];
    var self=this;
    self.instances = {};
    var dspecs = {
        theme:'light',
        container:$('body')
    };
    var specs = $c.merge(dspecs,specs)
    this.constructComponent = function(componentName){
      self[componentName] = {};

    };
    /*----------------------UTIL FUNCTIONS ----------------------*/
    var utils;
    this.utils = utils = {
        addRenderFunction:function(component){
            component.render = function(content,target,specs){
                var cssclass = specs.cssclass || '';
                var CONTENT = (component.before_content || '')+content+(component.after_content||'');
                var h = '<'+component.tag+' class="'+cssclass+'">'+CONTENT+'</'+component.tag+'>';
                if(target || component.target){
                    $(target||component.target).append(h);
                }
                return h;
            }
        },
        addRenderers : function(){

        },
        propAsFuncOrValue:function(prop, toPass){
            /*|{
             featured:true,
             tags:'helper',
             description:'Parses the property passed as a function or value, can be passed an object to be a parameter of the function call.'
             }|*/
            try {
                var toPass = toPass || null;
                if (prop && typeof prop == 'function') {
                    return prop(toPass);
                }
                return prop;
            }catch(e){
                logit('capp error parsing func val:[prop='+prop+']'+(e.stack||e));
                return '';
            }
        },
        initElements:function(elementType,css,callback){
            var callback = callback || null;
            $(elementType).each(function(){
                if(!$(this).hasClass('init')){

                    var data = $(this).data();
                    var id = data['capp'] || cuid();
                    var domid= $(this).attr('id');
                    var css = {};

                    $(this).css(css).addClass('init');
                    $(this).attr('data-capp',id);
                    var type = (self.instances[id]||{}).type || elementType.replace('capp-','').toLowerCase();
                    self.instances[id] = $.extend(self.instances[id]||{},{
                        dom:$(this),
                        data:data,
                        domid:domid,
                        type:type
                    })
                    if(callback){
                        callback($(this),data);
                    }
                }

            });
        }

    };
    /*----------------------UTIL FUNCTIONS ----------------------*/


    /*----------------------BEGIN WRAPPER ----------------------*/
    this.Wrapper = {
        tag:'capp-wrapper',
        target:'body',
        respond:function(){
            var wrapperdom = $(self.Wrapper.tag)[0];
                   var sizeClass='';
            if(wrapperdom.className.indexOf('-size') == -1){
                wrapperdom.className += ' large-size ';
            }
            if($(self.Wrapper.tag).width()<600){
                sizeClass='small-size';
            }else{
                sizeClass='large-size';
            }

            wrapperdom.className = wrapperdom.className.replace(/[a-z]*-size/,sizeClass);
        }
    };
    var Wrapper = this.Wrapper;
    Wrapper.render = function(content,target){
        var h = '<'+self.Wrapper.tag+'>'+content+'</'+self.Wrapper.tag+'>';
        if(target){
            $(target).append(h);
        }
        if($c.isMobile()){
            var capwrap = document.getElementsByTagName('capp-wrapper')[0];
            capwrap.style.minHeight=body.style.minHeight = window.innerHeight+'px';
            capwrap.style.minWidth=body.style.minWidth = window.innerWidth+'px';
        }

        return h;
    };

    /*----------------------END WRAPPER -------------------------*/


    /*----------------------BEGIN HEADER ----------------------*/
    this.Header = {
        tag: 'capp-header',
        target:'capp-wrapper',
        before_content:'<capp-header-bonus ></capp-header-bonus>'
    };
    var Header = this.Header;
    utils.addRenderFunction(Header);
/*    Header.render = function(content,target){
        var h = '<'+self.Header.tag+' class="'+specs.theme+'-bg">'+content+'</'+self.Header.tag+'>';

    };*/
    /*----------------------END HEADER -------------------------*/
    /*----------------------BEGIN BODY -------------------------*/
    this.Body ={
        tag: 'capp-body',
        target:'capp-wrapper'
    };
    var Body = this.Body;
    utils.addRenderFunction(Body);
    /*----------------------END BODY ----------------------*/

    /*----------------------BEGIN VIEWS ----------------------*/
    this.View = {tag:'capp-view'};
    this.View.toggle = function(ids,showhide){
        if(showhide != null){
            showhide = !showhide;
        }
        var ids = ids || null;
        if(typeof ids == "string"){
            ids = ids.split(',');
        }
        if(ids && ids.length){
            ids.map(function(id){
                $(self.View.tag+'#'+id).toggleClass('hidden',showhide);
            })
        }else{
            $(self.View.tag).toggleClass('hidden',showhide);
        }
    }
    this.View.show = function(id){
        self.View.toggle(null,false);    
        self.View.toggle(id,true);
    };

    /*----------------------END VIEWS -------------------------*/

    /*----------------------BEGIN BUTTONS ----------------------*/
    this.Button = {};
    this.Button.add = function(name,icon,action, container,specs) {
        var obj = {};
        if($.type(name) == 'object'){
            obj = name;
            name = utils.propAsFuncOrValue(name.name);
            action = utils.propAsFuncOrValue(obj.action);
            icon = utils.propAsFuncOrValue(obj.icon);
            container = utils.propAsFuncOrValue(obj.container);
        }
        var schemaname;
        var specs = specs ||{};
        if(specs.schema){
            schemaname = specs.schema.display || specs.schema.name || specs.schema;
            specs.create=specs.schema.name || specs.schema;
        }
        var id = (specs.id)?'id="'+specs.id+'"':'';
        var counter = (specs.counter == 0 || specs.counter)?'<capp-counter>'+specs.counter+'</capp-counter>':'';
        // var newWindowLink = (specs.newWindowLink)?'<a class="capp-button-sublink capp-subthemed" onclick="__cancelPropagation();"href="'+(specs.newWindowLink||'')+'" target="_blank" title="new window">'+self.svgs.newwindow+'</a>':'';
        var create = (specs.create)
        ?'<div class="capp-button-sublink capp-subthemed" onclick="_joe.createObject(\''+specs.create+'\'); __cancelPropagation();" title="create">'+self.svgs.quickadd+'</div>':'';
        var btn_html =
            '<capp-button '+id+' onclick="' + action + '" class="'+(specs.cssclass||'')+'">' +
            (specs.newWindowLink && ('<a onclick="__cancelClick();" href="'+specs.newWindowLink+'" >') || '')+
            (icon && ('<capp-button-icon title="'+name+'">' + icon + '</capp-button-icon> ' +
            '<capp-button-label>' + name +counter+ create+/*newWindowLink+*/'</capp-button-label> ') || name) +
            ((specs.newWindowLink && '</a>') || '')+
            '</capp-button>';

        $(container) && $(container).append(btn_html);
        return btn_html;
    };
    this.Button.update = function(id,specs){
        var specs = specs ||{};
        var label = _joe.propAsFuncOrValue(specs.label);
        var counter = _joe.propAsFuncOrValue(specs.counter);
        if(label){
            $('#'+id+' capp-button-label').html(label);
        }
        if(counter){
            if($('#'+id+' capp-counter').length){
                $('#'+id+' capp-counter').html(counter);
            }else{
                $('#'+id+' capp-button-label').append('<capp-counter>'+counter+'</capp-counter>');
            }
        }
    }
    this.Button.addFromSchema = function(schema,specs){
        var specs = $.extend({container:'capp-panel'},specs);
        var icon = schema.name;
        if(schema.menuicon||schema.icon){
            icon = (schema.menuicon||schema.icon)+'<svg-label>'+(schema.display || schema.name)+'</svg-label>';
        }
        return self.Button.add((schema.display||schema.name),icon,
            '_joe.current.clear(); goJoe(\''+schema.name+'\');',
            specs.container,{
                schema:schema,
                id:schema.name+'SchemaBtn',
                newWindowLink:'#/'+schema.name,
                cssclass:(schema.default_schema && 'default-schema')||''
            })

    };

/*----------------------END BUTTONS ----------------------*/

/*----------------------BEGIN MENU ----------------------*/
    this.Menu = {tag:'capp-menu'};
    this.Menu.add = function(label,items,container,specs) {
        var obj = {};
        var specs = specs || {};
        var cssclass;
        if($.type(label) == 'object'){
            obj = label;
            items = label.items;
            container = label.container;
            specs = label.specs;
            cssclass = label.cssclass;
        }
        var temp = '<capp-menu-option class="${cssclass}" onclick="${action}">${name}</capp-menu-option>';

        function renderBonus(side,content){

        }
        var menu_html =
            '<capp-menu class="'+(specs.cssclass || cssclass)+'">'
                
                +'<capp-menu-label class="'+(specs.labelcss||'')+'">'+label+'</capp-menu-label>'
                +'<capp-menu-panel>';
                    items.map(function(i){
                        temp = (_joe &&_joe.propAsFuncOrValue(specs.template,i)) || temp;
                        menu_html += fillTemplate(temp,i);
                    })
                    
                menu_html+='</capp-menu-panel>'
            +'</capp-menu>';
        $(container) && $(container).append(menu_html);

        $('capp-menu-label').not('.init').click(self.Menu.init).addClass('init');
        return menu_html;
    };
    this.Menu.init =function(){
            $(this).parent().toggleClass('expanded').siblings().removeClass('expanded');
    };
    this.Menu.addFromApps = function(apps,label,cssclass){
        var app_items=[],link,appname;
        apps.map(function(a){
            var cssclass= (location.pathname.indexOf(a)!= -1)?"selected":'';
            var link = a+location.search;
            appname = a;
            //TODO allow appname to use app title or display
            app_items.push({
                name:appname+'<capp-menu-option-bonus title="open in new window" onclick="window.open(\''+link+'\'); window.event.stopPropagation();">'
                +self.svgs.newwindow
                +'</capp-menu-option-bonus>',
                action:'window.location=\'/JOE/'+link+'\'',
                cssclass:cssclass
            });
        });

        app_items.push({name:'Docs',action:'window.open(\'/JsonObjectEditor/docs.html#/method/JOE\'); window.event.stopPropagation();'});
        return self.Menu.add((label||'Apps'),app_items,'capp-header',{labelcss:'multi-line capp-app-title',cssclass:'dd capp-apps-menu'});

    };
    this.Menu.addFromSites = function(apps){
        var app_items=[];
        apps.map(function(a){
            app_items.push({name:a,action:'window.location=\'//localhost:2098/'+ a.url+'\''});
        });
        return self.Menu.add('Sites',app_items,'capp-header');

    };

/*----------------------END MENU ----------------------*/

/*---------------------> CAPP-DASHBOARD <---------------------*/
    this.Dashboard = Dashboard =  {
        tag: 'capp-dashboard',
        target:'capp-body'
    };
    this.Dashboard.init = function(){
        var config ={
            containment:'capp-dashboard',
            grid:[self.Card.size+self.Card.offset,self.Card.size+self.Card.offset],
            refreshPositions:true,
            cursor:'move',
            handle:'.handle'
        };
       if(!$c.isMobile()){
           $('capp-dashboard').find('capp-card').draggable(config);
       }
    };
    this.Dashboard.toggle = function(){
        if($('capp-dashboard').draggable('option','disabled')){
            $('capp-dashboard').draggable('enable');
        }else{
            $('capp-dashboard').draggable('disable');
        }
    };
    this.Dashboard.respond = function(){
        //for each dashboard,get width
        var wcutoff = 500;
        var width,height;
        $('capp-dashboard').each(function(){
            width = $(window).width(); //$(this).width();
            if(width < wcutoff && !$(this).hasClass('mobile')){
                $(this).addClass('mobile').find('capp-card.ui-draggable').draggable('disable');
            }else if(width >= wcutoff && $(this).hasClass('mobile')){
                $(this).removeClass('mobile').find('capp-card.ui-draggable').draggable('enable');

            }
        });

    };
    utils.addRenderFunction(Dashboard);
/*---------------------> CAPP-DASHBOARD <---------------------*/

/*---------------------> CAPP-CARD <---------------------*/
    this.Card = {
        size:160,
        tag:'capp-card',
        offset:10
    };
    this.Card.add = function(title,content,css,specs){
        var bk={
            title:title,
            content:content,
            css:css,
            specs:specs
        };
        var card = {}
        var obj = {};
        var specs = specs || {};
        if($.type(title) == 'object'){
            obj = $.extend({},title);
            card.attributes = title.attributes;
            card.title = title = utils.propAsFuncOrValue(title.title);
            card.specs = specs = specs || utils.propAsFuncOrValue(obj.specs);
            card.cappid = specs.capp || cuid();
            card.onload = obj.onload;
            
            content = content || utils.propAsFuncOrValue(obj.content,card);
            css = css || utils.propAsFuncOrValue(obj.cssclass||obj.css);

            
        }
        var defs = {
            title:title||false,
            content:content||'Card Content',
            css:css||''
        };
        
        var cappid = card.cappid || specs.capp || cuid();

        var container = specs.container || 'capp-dashboard';
        var pos = '';
        var left = obj.left || specs.left;
        var top = obj.top || specs.top;

        if (left){pos += ' data-left='+left;}
        if (top){pos += ' data-top='+top;}
        
        self.instances[cappid] = $.extend(defs,{type:'card',capp:cappid,bk:bk,onload:card.onload});
        var atts = ' ';
        if(card.attributes){
            Object.keys(card.attributes).map(att=>{
                atts+=` card-${att}='${card.attributes[att]}'`;
            })
        }
        var temp = '<capp-card data-capp="'+cappid+'" class="${css} '+(!self.instances[cappid].title && 'no-title'||'')+'" '+pos+' '+atts+'>' +
            '<capp-title class="handle">${title}</capp-title>' +
            '<capp-content>${content}</capp-content>' +
            '</capp-card>';


        var code = fillTemplate(temp,defs);
        if(!specs.returnOnly){
            $(container) && $(container).append(code);
            self.Card.init();
            if(container == "capp-dashboard"){ self.Dashboard.init();}
        }
        return code;
    };

    this.Card.init = function(){
        var offset = self.Card.size+self.Card.offset;
        //find l1 and t1 classnames
        $('capp-card').each(function(){
            if(!$(this).hasClass('init')){

                var data = $(this).data();
                var id = data['capp'] || cuid();
                var css = {};
                if(data.left){css.left = offset*data.left;}
                if(data.top){css.top = offset*data.top;}
                $(this).css(css).addClass('init');
                $(this).attr('data-capp',id);
                self.instances[id] = $.extend(self.instances[id]||{},{
                    dom:$(this)
                })
                if(self.instances[id].onload){
                    self.instances[id].onload(self.instances[id]);
                }
            }

        });
    };
    this.Card.reload = function(id){
        //reload a particular card
        var instance= capp.instances[id];
        var specs = $.extend((instance.bk.specs||{}),{capp:id,returnOnly:true});
        var oldDom = instance.dom;
        var html = self.Card.add(instance.bk.title,instance.bk.content,instance.bk.css,specs);

        $('capp-card[data-capp='+id+']').replaceWith(html);
    };
/*---------------------> CAPP-CARD <---------------------*/
/*---------------------> CAPP-CHART <---------------------*/
    this.Chart ={
        tag:'capp-chart'
    };
    this.Chart.timeline = function(valueProp,dateProp,schemaname,cappid,specs){
        var specs = specs || {};
        var dataset = _joe.Data[schemaname];
        var schemaobj = _joe.schemas[schemaname];
        if(dataset){

            if(specs.filter){
                dataset = dataset.where(specs.filter);
            }
            var dates = ['dates'];
            var values = [(specs.label||'values')];
            dataset.map(d=>{
                dates.push(_joe.propAsFuncOrValue(dateProp,d));
                values.push(_joe.propAsFuncOrValue(valueProp,d));
                
            })
            var legendposition = (_joe && _joe.sizeClass == "small-size")?'bottom':'right';
            var chart = c3.generate({
                bindto: 'capp-chart#chart_'+cappid+'',
                legend: {
                    position: legendposition
                },
                data: {
                    x: 'dates',
                    columns: [
                        dates,
                        values
                    ]
                },
                /*onclick: function (d, i) { 
                    var query = {schema:schemaname}
                    goJoe(_joe.getDataset(schemaname)||[],query);

                    }*/
            });
        }
    }
       this.Chart.byProperty = function(propertyname,schemaname,cappid,specs){
        var specs = specs || {};
        var dataset = _joe.Data[schemaname];
        var schemaobj = _joe.schemas[schemaname];
        var propertymap = {};
        var propcolors = specs.colors || {};
        var subsets = specs.subsets;
        if(dataset){
            propertymap = _joe.Utils.getPossibleValues('build_type','cg_model');

            if(specs.filter){
                dataset = dataset.where(specs.filter);
            }

            var cols = [];
            var names = {};
            var colors = [];
            var cnt;
            for(var property in propertymap){
                cnt = propertymap[property];
                cols.push([property,cnt]);
                names[property] =property+' '+cnt;
                colors.push(propcolors[property]||_joe.Utils.getRandomColor())
            }
            var legendposition = (_joe && _joe.sizeClass == "small-size")?'bottom':'right';
            var chart = c3.generate({
                bindto: 'capp-chart#chart_'+cappid+'',
                data: {
                columns: cols,
                type : 'donut',
                names:names,
                onclick: function (d, i) { 
                        console.log(arguments); 
                        var query = {schema:schemaname}
                        if(subsets){
                            query.subset = d.name;
                        }
                        goJoe(_joe.getDataset(schemaname)||[],query);
                            if(specs.filters){
                                var dom = $('joe-filter-option[data-filter="'+d.id+'"]')[0]
                                _joe.toggleFilter(d.id,dom);
                                getJoe(0).toggleFiltersMenu();
                            }
                        },
                },
                donut: {
                    title: specs.chartTitle|| propertyname 
                },
                legend: {
                    position: legendposition
                },
                color: {pattern: colors}
            });
        }
    }
    this.Chart.byStatus = function(schemaname,cappid,specs){
        var specs = specs || {};
        var dataset = _joe.Data[schemaname];
        var schemaobj = _joe.schemas[schemaname];
        if(dataset && _joe.Data.status){
            var statusmap={none:{count:0,name:'none',color:'grey'}};
            
            var statuses = _joe.Data.status.sortBy('index').where({datasets:{$in:[schemaname]}});
            statuses.map(function(status,i){
                statusmap[status._id] = {count:0,name:status.name,color:status.color}
            })
            if(specs.filter){
                dataset = dataset.where(specs.filter);
            }
            dataset.map(function(obj){
                if(statusmap[obj.status]){
                    statusmap[obj.status].count++;
                }else{
                    statusmap['none'].count++;
                }
            })
            var cols = [];
            var colors = [];
            statusmap.map(function(status){
                cols.push([status.name+' '+status.count,status.count]);
                colors.push(status.color||'#ccc')
            })
            var legendposition = (_joe && _joe.sizeClass == "small-size")?'bottom':'right';
            
            if(specs.delay){
                setTimeout(function(){
                    return c3.generate({
                        bindto: specs.target||'capp-chart#chart_'+cappid+'',
                        data: {
                        columns: cols,
                        type : 'donut',
                        onclick: specs.onclick || function (d, i) { 
                            goJoe(_joe.getDataset(schemaname)||[],{schema:schemaname,subset:d.name});
                            },
                        },
                        donut: {
                            title: dataset.length+" "+schemaobj.name.pluralize()+" in "+(statuses.length+1)+" statuses"
                        },
                        legend: {
                            position: legendposition
                        },
                        color: {pattern: colors}
                    });                     
              },100);
              return true;
            }
            var chart = c3.generate({
                bindto: specs.target||'capp-chart#chart_'+cappid+'',
                data: {
                columns: cols,
                type : 'donut',
                onclick: specs.onclick || function (d, i) { 
                    goJoe(_joe.getDataset(schemaname)||[],{schema:schemaname,subset:d.name});
                    },
                },
                donut: {
                    title: dataset.length+" "+schemaobj.name.pluralize()+" in "+(statuses.length+1)+" statuses"
                },
                legend: {
                    position: legendposition
                },
                color: {pattern: colors}
            });
        }
    }

/*---------------------> CAPP-CHART <---------------------*/
/*---------------------> CAPP-PAGE <---------------------*/
    this.Page ={
        tag:'capp-page'
    };
    this.Page.init = function(){
        var offset = self.Card.size+self.Card.offset;
        $('capp-page').each(function(){
            if(!$(this).hasClass('init')){
                var data = $(this).data();
                var css = {};
                if(data.left){css.left = offset*data.left;}
                if(data.top){css.top = offset*data.top;}
                if(data.right){css.right = offset*data.right;}
                if(data.bottom){css.bottom = offset*data.bottom;}

                $(this).css(css).addClass('init');
            }

        });
    };
/*---------------------> CAPP-PAGE <---------------------*/

/*---------------------> CAPP-TOGGLE <---------------------*/
    this.Toggle ={
        toggle:function(id){
            $('*[data-toggle='+id+']').addClass('active').siblings().removeClass('active');
        }
    };

/*---------------------> CAPP-TOGGLE <---------------------*/

/*---------------------> CAPP-POPUP <---------------------*/
    this.Popup = {
        tag:'capp-popup',
        add:function(title,content,specs){
            //pass cssclass id and capp
            var bk={
                title:title,
                content:content,
                specs:specs
            };
            var specs = specs || {};
            var cappid = specs.capp || cuid();
            var closeBTN = (specs.hasOwnProperty('close') && specs.close)||true;
            var active = specs.active || false;
            var popobj = {
                title:self.utils.propAsFuncOrValue(title),
                content:self.utils.propAsFuncOrValue(content),
                capp:cappid,
                cssclass:self.utils.propAsFuncOrValue(specs.cssclass),
                id:specs.cssid||specs.id||''
            }
            var close_action = specs.close_action||'$(this).parent().toggleClass(\'active\');';
            var template = 	
                '<capp-popup id="${id}" class="${cssclass} '+(active&&'active'||'')+'" data-capp="${capp}">'
                +(closeBTN && '<capp-popup-close onclick="'+close_action+'"> X </capp-popup-close>'||'')
                +'<capp-title>${title}</capp-title>\
                <capp-content>${content}</capp-content>\
            </capp-popup>';

            var popup_str = fillTemplate(template,popobj);
            var container = specs.container || 'capp-wrapper';
            $(container) && $(container).append(popup_str);
            
            self.instances[cappid] = {type:'popup',capp:cappid,bk:bk};
            self.Popup.init();
            return self.instances[cappid];
        },
        toggle:function(cappid,title_active,content,callback){
            var popup;
            var setActive;
            var title_active = self.utils.propAsFuncOrValue(title_active);
            
            var content = self.utils.propAsFuncOrValue(content);
            if(!cappid){return;}
            if($c.isCuid(cappid)){
                popup = $('capp-popup[data-capp='+cappid+']');
            }else{
                popup = $('capp-popup#'+cappid);
            }
            if(title_active){setActive = true;}
            if(title_active && title_active !== true){
                popup.find('capp-title').html(title_active);
            }
            if(content && content !== true){
                popup.find('capp-content').html(content);
            }
            
            popup.toggleClass('active',setActive);
            return popup;
        },
        respond:function(){

        },
        delete:function(cappid){
            if(!cappid){return;}
            if($c.isCuid(cappid)){
                popup = $('capp-popup[data-capp='+cappid+']');
            }else{
                popup = $('capp-popup#'+cappid);
            }
            var capp = popup.data('capp');
            delete self.instances[capp];
            popup.remove();
        }
    };
    
    this.Popup.init = function(){
        self.utils.initElements('capp-popup');
    };
/*---------------------> CAPP-POPUP <---------------------*/


/*---------------------> RESPONSIVE <---------------------*/

    this.Responsive = {
        init:function(){
            $(window).on('resize',self.Responsive.go);
            self.Responsive.go();
        },

        go:function(){
            clearTimeout(self.Responsive.timer);

            self.Responsive.timer = setTimeout(function(){
                if(_joe && _joe.resizeOk()){
                    var respBM = new Benchmarker();
                    self.Reload.all();
                    for(var object in self){
                        if(self[object].respond && object != 'Responsive'){
                            self[object].respond();
                        }
                    }
                    logit('responded in '+respBM.stop()+'s');
                }
            },500);
        }
    };
/*---------------------> RESPONSIVE <---------------------*/

/*---------------------> RELOAD <---------------------*/
    this.Reload = {
        instance:function(id){
            if(id){
                var instance= capp.instances[id];
                var itype;
                try{
                    itype = instance.type.capitalize();
                    if(capp[itype].reload){
                        capp[itype].reload(id);
                    }
                }catch(e){
                    logit('could not reload: '+itype+' '+id,e,instance);
                }
            }
        },
        all:function(){

            for(var i in capp.instances){
                self.Reload.instance(i);
            }
            self.Card.init();
            self.Page.init();
            self.Dashboard.init();
        }

    };
/*---------------------> RELOAD <---------------------*/

/*---------------------> HASH <---------------------*/
    this.Hash = {
        init:function(){
             window.addEventListener("hashchange",function(hashinfo) {
                var hashchangehandler = specs.hashchangehandler || foo;
                if(hashchangehandler){
                    try{
                        hashchangehandler(hashinfo);
                    }catch(e){
                        alert(e);
                    }
                }
            });
        }

    };
/*---------------------> HASH <---------------------*/




/*----------------------BEGIN INIT ----------------------*/
    this.init = function(s){
        specs = $c.merge(specs,(s||{}));
        self.renderComponents(renderQueue);
        //componetize('capp-header');
        self.initSVGs();
        self.initInteractions();
        self.Responsive.init();
        self.Hash.init();
    };
    this.renderComponents = function(components){
        if(!components){
            return false;
        }
        if(!components.isArray()){
            components = [components];
        }
        var comtype;
        components.map(function(component){
            component = utils.propAsFuncOrValue(component);
            //(content,target,specs)
            comtype = component.type.capitalize();
            if(component && comtype) {
                if (self[comtype]) {
                    if (self[comtype].render) {
                        self[comtype].render(utils.propAsFuncOrValue(component.content), component.target, component.specs);
                    } else if (self[comtype].add) {
                        component.config ?
                            self[comtype].add(utils.propAsFuncOrValue(component.config))
                            : self[comtype].add(
                            utils.propAsFuncOrValue(component.title),
                            utils.propAsFuncOrValue(component.content),
                            utils.propAsFuncOrValue(component.specs));
                    }
                }
            }
        })
    };
    this.initSVGs = function(){
        /*
         * Replace all SVG images with inline SVG
         */
        jQuery('img.svg').each(function(){
            var $img = jQuery(this);
            var imgID = $img.attr('id');
            var imgClass = $img.attr('class');
            var imgURL = $img.attr('src');

            jQuery.get(imgURL, function(data) {
                // Get the SVG tag, ignore the rest
                var $svg = jQuery(data).find('svg');

                // Add replaced image's ID to the new SVG
                if(typeof imgID !== 'undefined') {
                    $svg = $svg.attr('id', imgID);
                }
                // Add replaced image's classes to the new SVG
                if(typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass+' replaced-svg');
                }

                // Remove any invalid XML tags as per http://validator.w3.org
                $svg = $svg.removeAttr('xmlns:a');

                // Replace image with new SVG
                $img.replaceWith($svg);

            }, 'xml');

        });
    };
    this.special = {
        joeicon:function(open){
            $('capp-menu.expanded').not('.default-schemas-menu').removeClass('expanded'); 
            $('#joePanelMenu').toggleClass('expanded',open); 
            $('capp-dashboard').toggleClass('left-panel-open',$('#joePanelMenu').hasClass('expanded'));
            
        }
    }
    this.initInteractions = function(){
        self.Card.init();
        self.Page.init();
        self.Dashboard.init();
        $('capp-header capp-menu-label').not('.init').click(self.Menu.init).addClass('init');
        /*$('capp-panel capp-menu-label').not('.init').on('dblclick',function(){
            $(this).parents('capp-panel').toggleClass('expanded');
        }).addClass('init');
        */
        $('capp-panel-toggle,.capp-panel-toggle').not('.init').click(function(){
            $(this).parents('capp-panel').toggleClass('expanded');
        }).addClass('init');
        $('capp-menu-toggle').click(function(){
            $('capp-wrapper').toggleClass('expanded');
        });
        $('capp-panel.hover-toggle').hover(
            function(){
                $(this).addClass('capp-targeted');
                window.__cpt = setTimeout(function(){
                    $('.capp-targeted').addClass('expanded');
            },1000)}
            ,function(){

                clearTimeout(window.__cpt);
                $('.capp-targeted').removeClass('expanded').removeClass('capp-targeted');
            }
        )

        //close panels on click
        $('capp-view,capp-dashboard').on('click',function(){ 
            $('capp-panel.expanded,capp-menu.expanded').not('#joePanelMenu,.default-schemas-menu').removeClass('expanded');
        })


    };

    if(specs.autoInit){
        self.init();
    }
    /*----------------------END INIT ----------------------*/
    this.svgs = {
        newwindow:'<svg xmlns="http://www.w3.org/2000/svg"  viewBox="-32 -32 96 96" ><path d="M32 0H8v8C5.1 8 0 8 0 8v24h24v-8h8V0zM20 28H4V16h4v8h12C20 25.8 20 28 20 28zM28 20H12V8h16V20z"></path></svg>',
        quickadd:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-12 -12 64 64"><g data-name="Layer 2"><polygon class="cls-1" points="28 22.3 22.5 22.3 22.5 28 17.6 28 17.6 22.3 12 22.3 12 17.5 17.6 17.5 17.6 12 22.5 12 22.5 17.5 28 17.5 28 22.3"/></g></svg>',
        newwindow2:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26"><path d="M4 4C2.9 4 2 4.9 2 6L2 17 4 15 4 8 23 8 23 20 9 20 7 22 23 22C24.1 22 25 21.1 25 20L25 6C25 4.9 24.1 4 23 4L4 4zM6 13L7.8 14.8 1 21.6 2.4 23 9.2 16.2 11 18 11 13 6 13z"/></svg>'
    }
    return this;
}

function componetize(name,createdCallback,specs){
    var specs = specs || {};

    var componentPrototype = Object.create(HTMLElement.prototype);
    componentPrototype.createdCallback = createdCallback || function() {
        //this.textContent = 'component: '+name;
    };
    $c.merge(componentPrototype,specs)
    //componentPrototype.merge(specs);

    document.registerElement(name,{prototype:componentPrototype})
}

function __cancelPropagation(e){
    var eve = e || window.event;
    if (eve.stopPropagation) eve.stopPropagation();
    //if (eve.preventDefault) eve.preventDefault();
    __closeAllExpanded();
    return true;
}

function __cancelClick(){
    __closeAllExpanded();
    return false;
}

function __closeAllExpanded(){
    $('capp-menu.expanded,capp-panel.expanded').removeClass('expanded');
}