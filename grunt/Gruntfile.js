/* =========================================================
 Grunt Starter Project
 ========================================================= */
module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        banner: '/* -------------------------------------------------------- \n' +
        ' * \n' +
        ' *  <%= pkg.projectName %> - v<%= pkg.version %> \n' +
        ' *  Created by: <%= pkg.author.name %> \n' +
        ' * \n' +
        ' * -------------------------------------------------------- */',
        files:{
            styles:[]
        },

        // Clean the build folder before rebuild
        clean: {
            dist: {
                src: [ '../css/joe.css','../js/joe.js' ]
            }
        },
        traceur: {
            custom: {
                files:{
                    dest: 'build/',
                    'src': ['../web-components/*.js'],
                    expand: true,
                    experimental:true

                }
            },
        },
        // Writes custom banner
        usebanner: {
            fullBanner: {
                options: {
                    banner: '<%= banner %>',
                    linebreak: true
                },
                files: {
                    src: [ '../css/joe.css','../js/joe.js']
                }
            }
        },

        // Compile Sass/SCSS files
        'sass': {
            options: {
                precision: 4,
                sourceMap: true
            },
            dist: {
                options: {
                    outputStyle: 'expanded'
                },
                files: [{
                    expand: true,
                    cwd: '<%= folders.source %>',
                    src: ['**/*.scss'],
                    dest: '<%= folders.build %>',
                    ext: '.css'
                }]
            }
        },
        "babel": {
            options: {
              sourceMap: true
            },
            dist: {
              files: {
                "dist/app.js": "src/app.js"
              }
            }
          },
        //
        uglify: {
            options: {
                mangle: false,
                beautify: false,
                compress: true,
                sourceMap: false
            },

            main: {
                files: {
                    
                    '../css/joe.min.css': ['../css/joe.css'],
                    '../js/joe.min.js': ['../js/joe.js']
                }
            }
        },


        concat: {
          options: {
            separator: ''
          },
          styles: {
            src: [
                "../css/leaflet.css",
                "../css/esri-leaflet-geocoder.css",
                //"../css/jif/style.css",
                "../css/joe-styles.css",
                "../css/jquery-ui.min.css",
                "../css/jquery.timepicker.min.css",
                "../css/variants/style-variant1.css",
                "../css/joe-print.css" 
            ],
            dest: '../css/joe.css'
          },
            scripts: {
                src: [
                    "../js/JsonObjectEditor.jquery.craydent.js",
                    "../js/leaflet.js",
                    "../js/esri-leaflet-geocoder.js",
                    "../js/zebra_datepicker.196.min.js",
                    "../js/jquery.timepicker.min.js",
                   // "../js/shorthand.joe.js"
                    //,"../js/ace/ace.js"
                   // ,"../js/plugins/tinymce.min.js"
                ],
                dest: '../js/joe.js'
            },
            scriptsES5: {
                src: [
                    "../es5-build/js/JsonObjectEditor_es5.jquery.craydent.js",
                    "../js/leaflet.js",
                    "../js/esri-leaflet-geocoder.js",
                    "../js/zebra_datepicker.196.min.js",
                    "../js/jquery.timepicker.min.js",
                   // "../js/shorthand.joe.js"
                    //,"../js/ace/ace.js"
                   // ,"../js/plugins/tinymce.min.js"
                ],
                dest: '../js/joe_es5.js'
            },
            scripts2: {
                src: [
                    "../js/libs/jquery-1.11.3.min.js",
                    "../js/libs/jquery-ui.min.js",
                    "../js/libs/jquery.ui.touch-punch.min.js",
                    "../js/libs/craydent-1.8.1.js",
                    "../js/JsonObjectEditor.jquery.craydent.js",
                    "../js/leaflet.js",
                    "../js/esri-leaflet-geocoder.js",
                    "../js/zebra_datepicker.196.min.js",
                    //"../js/jquery.timepicker.min.js",
                    "../js/ace/ace.js",
                    "../js/plugins/tinymce.min.js"
                ],
                dest: '../js/joe-full.js'
            }
        },

        coverage: {
            default: {
                options: {
                    thresholds: {
                        'statements': 90,
                        'branches': 90,
                        'lines': 90,
                        'functions': 90
                    },
                    dir: 'coverage',
                    //root: 'test'
                }
            }
        },
        // Watches for changes to files
        watch: {
            options: {
                livereload: false,
                spawn: false
            },

            build_include: {
                files: ['../js/**/*.js','../css/**/*.css','../es5-build/**/*.js'],
                tasks: [ 'concat', 'usebanner' /*,'uglify'*/]
            }
        }

    });

    grunt.loadNpmTasks('grunt-istanbul-coverage');
    grunt.loadNpmTasks('grunt-traceur');
    // Load Grunt Plugins
    require('load-grunt-tasks')(grunt);
    //grunt.loadNpmTasks('grunt-contrib-watch');
   // grunt.loadNpmTasks('grunt-contrib-concat');

    // Watch for changes
    grunt.registerTask('default', [ 'concat', 'usebanner', /*'uglify',*/  'watch' ]);
    //grunt.registerTask('build', ['traceur']);

};