var process = require('process');
var fs = require('fs');
var filename = '.pid';
fs.readFile(filename, 'utf8', function(err, data) {
    if (err) throw err;
    console.log('OK: ' + filename);
    console.log(data);
    if(data){
        try {
            process.kill(data);
            fs.writeFileSync(filename, '');
        }catch(e){
            console.log(e);
        }
    }
});
//if(pid) {
//    process.kill(myArgs[2]);
//}