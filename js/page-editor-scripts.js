function JOEPageEditor(){
    var self = this;
    jsc_hostname = (jsc_hostname != 'localhost')?jsc_hostname:location.hostname;
    this.toggleEditorMode = function(){
        var edit_class ="jpe-editing";
        var cn = document.getElementsByTagName('html')[0].className.replace(/  /g,'');
        if(cn.indexOf(edit_class)== -1){
            document.getElementsByTagName('html')[0].className+=' '+edit_class+' ';
        }else{
            document.getElementsByTagName('html')[0].className=cn.replace(edit_class,'');
        }

    }

    this.GUI = {
        container:'jpe-info',
        init:function(){
            var jpes = document.getElementsByTagName('jpe-info');
            for(var j = 0; j < jpes.length; j++){
                self.GUI.populate(jpes[j]);
            }
        },
        populate:function(dom){
            var data = dom.dataset;
            var href="window.open";
            var icon = (window.jsc_svgs && jsc_svgs[data.schema] && 
            '<jpe-info-icon>'+jsc_svgs[data.schema]+'</jpe-info-icon>')||'';
            var html = 
            '<jpe-info-content title="'+data.id+'">'
                +icon
                +'<a href="//'+jsc_hostname+':'+jsc_port+'/JOE/sitebuilder#/'+data.schema+'/'+data.id+'" class="jpe-edit-btn" target="_blank">edit</a>'
                +'<jpe-info-name>'+data.name+'</jpe-info-name>'
                //+'<jpe-info-cuid>'+data.id+'</jpe-info-cuid>'
                +'<jpe-info-schema>'
                    +(data.src && data.src+' '||'')
                    +data.schema
                    +(data.section && ' <small> in ${'+data.section+'}</small>'||'')
                    +(data.block_template && '<br/><small> instance of '+data.block_template_name+'</small>'||'')
                +'</jpe-info-schema>'
            +'</jpe-info-content>';
            dom.innerHTML = html;
        }
    };
    this.SECTION = {
        container:"jpe-section",
        init:function(){
            var jpes = document.getElementsByTagName(self.SECTION.container);
            for(var j = 0; j < jpes.length; j++){
                self.SECTION.populate(jpes[j]);
            }
        },
         populate:function(dom){
            var data = dom.dataset;

             var html = 
             '<jpe-section-label>'
                +'<jpe-info-name>'+data.section+'</jpe-info-name>'
                
             +'</jpe-section-label>';
             dom.innerHTML = dom.innerHTML+html;
        }
    }
    this.CONTENT = {
        container:"jpe-content",
        init:function(){
            var jpes = document.getElementsByTagName(self.CONTENT.container);
            for(var j = 0; j < jpes.length; j++){
                self.CONTENT.populate(jpes[j]);
            }
        },
         populate:function(dom){
            var data = dom.dataset;

             var html = 
             '<jpe-content-label>'
             +data.name   
             +'</jpe-content-label>';
             dom.innerHTML = dom.innerHTML+html;
        }
    }
    this.init = function(){
        this.GUI.init();
        this.SECTION.init();
        this.CONTENT.init();
        this.toggleEditorMode();
    }
    this.init();
    return this;
};

window.jpe = JOEPageEditor();
