/* -------------------------------------------------------- 
 * 
 *  JOE - v1.5.0 
 *  Created by: Corey Hadden 
 * 
 * -------------------------------------------------------- */
/*/---------------------------------------------------------
    Craydent LLC
	Copyright 2014 (http://craydent.com/joe)
    Dual licensed under the MIT or GPL Version 2 licenses.
	(http://craydent.com/license)
---------------------------------------------------------/*/
//render field - self.renderObjectPropFieldUI
//render list item - self.renderListItems()
//single field list item -renderFieldListItem()

if($c.isMobile()){
    document.getElementsByTagName('html')[0].className +=' touch joesmall-size';
}else{
    document.getElementsByTagName('html')[0].className +=' no-touch joelarge-size';
}
window._joeEditingHash = false;
var _webworkers = false;
var _joeworker;

if (!!window.Worker) {
    _webworkers = true


}

var joe_web_dir = '//' + location.hostname + ':' +
        (location.port||((location.protocol=="https:")?443:80)) + "/JsonObjectEditor/";


if(location && location.origin == 'file://'){
    joe_web_dir = location.href.slice(0,location.href.lastIndexOf('/')+1);
}



__docWriter =function(src,type,cancelWrite){
    var type=type || 'js';
    var html = '';
    switch(type.toLowerCase()){
        case 'js':
        case 'javascript':
            html +='<script type="text/javascript" src="'+src+'"></script>';
            break;
        case 'css':
        case 'stylesheet':
            html +='<link href="'+src+'" rel="stylesheet" type="text/css">';
            break;
    }
    if(!cancelWrite){
        document.write(html);
    }
    return html;
};
__loadAdditionalFiles = function(){
    var bonuses ='';
    if (typeof jQuery == 'undefined') {
        //console.log('loading jquery');
       /* bonuses+=
            __docWriter(joe_web_dir+"js/libs/jquery-1.11.3.min.js",'js')+
            __docWriter(joe_web_dir+"js/libs/jquery-ui.min.js");*/
        __docWriter(joe_web_dir+"js/libs/jquery-1.11.3.min.js",'js')+
        __docWriter(joe_web_dir+"js/libs/jquery-ui.min.js");
    }
bonuses +=
    __docWriter(joe_web_dir+"js/libs/jquery.ui.touch-punch.min.js");
    if (typeof Craydent == 'undefined' || (!Craydent.VERSION || Craydent.VERSION < '1.7.37')) {
        bonuses+= __docWriter(joe_web_dir+"js/libs/craydent-1.8.1.js",'js',true);
    }
/*
    bonuses+=
        __docWriter(joe_web_dir+'css/joe.css','css','css',true)
        +__docWriter(joe_web_dir+'js/ace/ace.js','js',true)
        +__docWriter(joe_web_dir+'js/plugins/tinymce.min.js','js',true);
*/

    document.write(bonuses);
};

function __require(file,callback){
    var head=document.getElementsByTagName("head")[0];
    var script=document.createElement('script');
    script.src=file;
    script.type='text/javascript';
    //real browsers
    script.onload=callback;
    //Internet explorer
    script.onreadystatechange = function() {
        if (this.readyState == 'complete') {
            callback();
        }
    }
    head.appendChild(script);
}

/*var __joeFieldTypes = [
    'text',
    'select',
    'code',
    'rendering',
    'date',
    'boolean',
    'geo',
    'image',
    'url',
    'objectList',
    'objectReference',
    'group',
    'content'
];*/
//__loadAdditionalFiles();
function JsonObjectEditor(specs){
    var self = this;
    var htmlRef = document.getElementsByTagName('html')[0];
    self.initialDocumentTitle = document.title;
    $c.TEMPLATE_VARS.push(
        {variable:'/textarea',value:'</textarea>'},
        {variable:'textarea',value:'<textarea>'},
        {variable:'SERVER',value:'//'+$c.SERVER}

    );
    var initialized = false;
    var colCount = 1;
	var listMode = false;

    var gridMode = false;
    var tableMode = false,tableSpecs;
    var multiEdit = false;
	this.VERSION = '1.0.1';
	window._joes = window._joes || [];
	this.joe_index = window._joes.length;
	if(!window._joes.length){window._joe = this;}
    this.Error = {
        log:[],
        add:function(message,error,data){
            var payload = {
                caller: arguments.callee.caller,
                callerargs:
                    arguments.callee.caller.arguments,
                message:message,
                error:error,
                stack:new Error(error).stack,
                data:data,
                timestamp:(new Date()).toISOString(),
                _id:cuid()
            };
            self.Error.log.push(payload);
            logit('[error]: '+message);
        },
        show:function(){
            self.show(self.Error.log,{schema:{
                title:'${message}',
                idprop:'_id',
                listView:{
                    title: '<joe-title>${message}</joe-title><joe-subtitle>${timestamp}</joe-subtitle>',
                    listWindowTitle: 'Errors'
                },
                fields:[
                    'error:code',
                    'message',
                    'stack:code',
                    {name:'data',type:'content',run:function(data,obj){
                        return '<div><pre>'+tryEval(data)+'</pre></div>';
                    }},
                    'callerargs:code',
                    'timestamp:guid',
                    '_id'
                ]
            }});
        }

    };
	window._joes.push(this);

    this.Cache = {
        static:{},
        list:[],
        lookup:{},
        clear:function(){
            self.Cache.lookup = {};
            self.Cache.list = [];
        },
        remove:function(id){
            //self.cache.
        },
        get:function(id,specs){
            var cacheitem = self.Cache.lookup[id];
            if(cacheitem && typeof cacheitem.value == "function" && cacheitem.type =="callback"){
                return cacheitem.value(cacheitem.data,cacheitem.id);
            }
            if(!cacheitem){
                //if(autoadd){
                var obj = self.search(id)[0]||false;
                if(obj){
                    self.Cache.add(obj,{id:id})
                    return obj;
                }
                //}
                return false;
            }

            return cacheitem.value;
        },
        callback:function(id){
            var cacheitem = self.Cache.lookup[id];
            if(typeof cacheitem.value == "function"){
                return cacheitem.value(cacheitem.data,cacheitem.id);
            }
            else {logit('cache item is not a function');}
        },

        add:function(value,specs){
            var specs = specs || {};
            var obj = $.extend({
                id:specs.id||cuid(),
                value:value,
                parent:self.joe_index
            },specs);
            self.Cache.list.push(obj.id);
            self.Cache.lookup[obj.id] =obj;
            return obj;
        }
    };
    this.Cache.set = this.Cache.add;
	this.history = [];
/*-------------------------------------------------------------------->
	0 | CONFIG
<--------------------------------------------------------------------*/
    
	var defaults = {
        localStorage:false,
		container:'body',
		joeprofile:{
			lockedFields:['joeUpdated'],
			hiddenFields:[]
		},
		profiles:{},
		fields:{},
		schemas:{
			'rendering':	{
				title:'HTML Rendering',
				callback:function(){alert('yo');}
				//fields:['id','name','thingType','legs','species','weight','color','gender'],
				//_listID:'id',
				//_listTitle:'${name} ${species}'

		}},
        compact:false,
        useControlEnter:true,//to save
        useEscapeKey:false,//to close window
        autoInit:false,
        autosave:10000,
        dynamicDisplay:($(window).height() < 800 && $c.isMobile())?12:20,
        sans:false,
        listSubMenu:true,
        documentTitle:false,//tmplate to set document title
        style:{
            inset:true,
            cards:false
        },
        speechRecognition:false
	};

	this.specs = $.extend({},defaults,specs||{});
    if(this.specs.localStorage && typeof(Storage) === "undefined"){
        alert('no local storage');
        this.specs.LocalStorage = false;
    }
    this.Indexes ={
        _add:function(idprop,dataitem){
            self.Indexes[idprop] = self.Indexes[idprop] || {};
            self.Indexes[idprop][dataitem[idprop]] = dataitem;
        },
        _usage:0
    };
    this.Printer = {
        init:function(){
            if(!document.querySelector('print-joe')){
                document.body.appendChild(document.createElement("print-joe"));
            }
        },
        print:function(){
            
            self.Printer.init();
            var elmnt = document.getElementsByTagName("joe-panel-content")[0];
            var cln = elmnt.cloneNode(true);
            document.querySelector('print-joe').innerHTML ='';
            document.querySelector('print-joe').appendChild(cln);
            var panelContent = document.querySelector('print-joe joe-panel-content')
            panelContent.className = 'joe-panel-content';
            var activeNodes = document.querySelectorAll('print-joe joe-panel-content .joe-content-section.active');
            if(activeNodes.length){
                panelContent.innerHTML ='';
                for (var i = 0; i < activeNodes.length; i++) {
                    panelContent.appendChild(activeNodes[i]);
                }

            }
            window.print();
             
            document.querySelector('print-joe').innerHTML ='';
            

        }
    }
    this.Data = {};
    this.Render = {
        schema_icon:function(schema,classes){
            var icon = (self.schemas[schema] && self.schemas[schema].menuicon)||'';
            icon && (icon = `<joe-icon class="${classes ||''}">${icon}</joe-icon>`);
            return icon;
        }
    };
    //TODO: make current.clear a function
	//TODO: check for class/id selector
	this.container = $(this.specs.container);
	this.fields = this.specs.fields;
    this.Fields = {
        reset:function(propname){
            var field = self.Fields.get(propname);
            self.current.object[propname] = field.reset;
            self.Fields.rerender(propname);
        },
        onPanelShow:function(cur){
            var cur = cur || _jco();
            var currentFields= self.current.fields;
            //get all fields
            logit('current fields',currentFields);
            currentFields.map(f=>{
                if(f.onPanelShow){
                    f.onPanelShow(cur);
                }
            })
            //run onshow functions

        }
    };
    //configure schemas
    this.schemas = this.specs.schemas;
/*
    this.schemas._function = {
        idprop:'name'
    };*/

    for(var s in _joe.schemas){
        _joe.schemas[s].__schemaname = _joe.schemas[s].name = s;
        _joe.schemas[s]._id = cuid();

    }

    this.current = {};
    //filters:{},
    this.current.clear= function(){
        /*|{
         featured:true,
         description:'cleans up variables that currently exist between a single JOE showings.',
         tags:'cleanup, reset',
         category:'core'
         }|*/
        self.current.list = null;
        self.current.subsets = null;
        self.current.subset = null;
        self.current.filters = {};
        self.current.fields = [];
        self.current.schema = null;
        self.current.object = null;
        self.current.reset = {};
        self.current.cache = {};
        self.current.title = null;
        self.current.keyword = '';
        if(_joes.length == 1){
            document.title = self.initialDocumentTitle;  
        }
    };

    //profile
	this.defaultProfile = this.specs.defaultProfile || this.specs.joeprofile;
	this.current.profile = this.defaultProfile;

	this.ace_editors = {};
    
    this.Render.stripeColor=function(opt,colorfunc){
        var color = self.propAsFuncOrValue((colorfunc||opt.stripecolor),opt,null,_jco());
        var title = '';
        if(color && $c.isObject(color)){
            title = ' title="'+color.title+'" ';
            color = color.color;
        }
        var h = '<joe-stripe-color '+title+(color && 'style="background-color:'+color+';"' || '')+'></joe-stripe-color>';
        return h;
    };
    this.Render.bgColor = function(opt,colorfunc){
        var color = self.propAsFuncOrValue((colorfunc||opt.bgcolor),opt,null,_jco());
        if(color && $c.isObject(color)){
            color = color.color;
        }
        var h = '<joe-bg-color '+(color && 'style="background-color:'+color+';"' || '')+'></joe-bg-color>';
        return h;
    };
    this.Render.itemCheckbox = function(listItem,schema,specs){
        /*|{
            description:'renders a checkbox for a lsititem',
            specs:,expander,gotoButton,itemMenu,schemaprop,nonclickable,bgcolor,stripecolor,action
            tags:'render, field list item, checkbox',
        }|*/
        var specs = specs ||{};
        var checkbox = self.propAsFuncOrValue(self.getCascadingProp('checkbox',schema),listItem);
        var idprop = specs.idprop || self.getIDProp();
        if(checkbox){
            var cbox_prop = checkbox;
            var cbox_percentage = null;
            var cbox_title = '';
            if($.type(checkbox) == "object"){
                cbox_prop = checkbox.prop;
                cbox_title = checkbox.title||cbox_prop;
                cbox_percentage = self.propAsFuncOrValue(checkbox.percentage,listItem);
            }
            var cb_label = self.propAsFuncOrValue(checkbox.label,listItem);
            var checked = [true,'true'].contains(listItem[cbox_prop])?'checked':'';
            checkbox = '<joe-checkbox title="'+cbox_title+'" ' +
                'class="'+checked+'"' +
                'onclick="_joe.checkItem(\''+listItem[idprop]+'\',\''+cbox_prop+'\',null,this)">' +
                ((cbox_percentage !== false && checkbox.hasOwnProperty('percentage'))?
                    '<joe-checkbox-percentage>'+Math.round(cbox_percentage*100)+'%</joe-checkbox-percentage>':'')+
                    ((cb_label && '<joe-checkbox-label>'+cb_label+'</joe-checkbox-label>')||'')+
                '</joe-checkbox>';
        }else{
            checkbox = '';
        }
        return checkbox;
    }
/*-------------------------------------------------------------------->
	1 | INIT
<--------------------------------------------------------------------*/
	this.init = function(callback) {
        /*|{
         featured:true,
         description:'primary entry point into JOE, sets up the UI necessary',
         tags:'init, framework',
         category:'core'
         }|*/
        if(initialized){return false;}
       
        beginLogGroup('JOE init');


        self.current.clear();
        var html = self.renderFramework(
                self.renderEditorHeader() +
                self.renderEditorContent() +
                self.renderEditorFooter()
        );
        self.container.append(html);
        self.overlay = $('.joe-overlay[data-joeindex=' + self.joe_index + ']');
        self.panel = self.overlay.find('.joe-overlay-panel');
        self.initKeyHandlers();
        self.Speech.init();
        //self.Autosave.init();
        self.readHashLink(); 
        
        window.addEventListener("hashchange", function(newH,oldH){
           
            var useHash = $GET('!') || location.hash;
            if (!useHash || self.joe_index != 0 || !specs.useHashlink) {
                return false;
            }
            if(!window._joeEditingHash){
                self.readHashLink();
            }


        }, false);
        var respond_timeout;
        $(window).on('resize',function(){
            if(self.resizeOk()){
                clearTimeout(respond_timeout);
                respond_timeout = setTimeout(self.respond,200);
            }
        });
        initialized = true;
        self.respond();
        endLogGroup();
        self.Components.init();

        callback && callback();
    };

    this.resizeOk = function(){
        if(!$c.isMobile()){
            return true;
        }
        if(document.activeElement.tagName == "INPUT"){
            return false;
        }
        return true;
    }
/*-------------------------------------------------------------------->
     INIT KEY HANDLERS
 <--------------------------------------------------------------------*/
    this.initKeyHandlers = function(){
        if (self.specs.useBackButton) {
            window.onkeydown = function (e) {
                var code = e.keyCode
                var nonBackElements = ['input','select','textarea'];
                var isInputElement = nonBackElements.indexOf(e.target.tagName.toLowerCase()) != -1;
                if (code == 8) {//BACKBUTTON PRESSED
                    if(isInputElement){
                        //return false;
                    }
                    else{
                        self.goBack();
                        return false;
                    }
                }else if([37,39,38,40,13,16,17,27].indexOf(code) == -1){//set focus for alphanumeric keys
                    if(e.altKey){//alt control
                        switch (code) {
                            case 70://QUICKFIND
                                self.quickFind();
                                if (e.stopPropagation) e.stopPropagation();
                                if (e.preventDefault) e.preventDefault();
                            break;
                            case 78://QUICKADD
                                self.quickAdd();
                                if (e.stopPropagation) e.stopPropagation();
                                if (e.preventDefault) e.preventDefault();
                            break;
                            case 80://PRINT
                                self.Printer.print();
                                if (e.stopPropagation) e.stopPropagation();
                                if (e.preventDefault) e.preventDefault();
                            break;
                        }
                    }else{
                        if(listMode) {
                            var inSearchfield = false;
                            if ($(document.activeElement) && $(document.activeElement)[0] != $('.joe-submenu-search-field')[0]) {
                                self.overlay.find('.joe-submenu-search-field').focus();
                                inSearchfield = true;
                                $('.joe-panel-content-option.keyboard-selected').removeClass('keyboard-selected');
                            }
                        }else{//NOT LIST MODE, DETAILS MODE
                            //is control key down
                            if(e.ctrlKey || e.metaKey) {
                                switch (code) {
                                    case 83://look for control save
                                        if(self.container.find('.joe-button.joe-quicksave-button').length) {
                                            self.updateObject(null, null, true);
                                            if (e.stopPropagation) e.stopPropagation();
                                            if (e.preventDefault) e.preventDefault();
                                        }
                                    break;
                                }
                            }
                        }
                    }
                }else{
                    //38 up, 40 dn,13 enter,37 left, 39 right
                    var autocompleteField = $('.joe-text-autocomplete.active').length;

                    if(autocompleteField){
                        var sel = '.joe-text-autocomplete-option.visible'+'.keyboard-selected';
                        //$('.joe-text-autocomplete-option.visible').length();
                        var keyboardSelected = $(sel)[0];
                        var keyboardSelectedIndex = ($(sel).length)? $(sel).index():-1;
                        switch(code){
                            case 38://up
                                keyboardSelectedIndex--;
                                if(keyboardSelectedIndex > -1) {
                                    keyboardSelectOption('.joe-text-autocomplete-option.visible');
                                }
                                break;
                            case 40://down
                                keyboardSelectedIndex++;
                                if(keyboardSelectedIndex < $('.joe-text-autocomplete-option.visible').length) {
                                    keyboardSelectOption('.joe-text-autocomplete-option.visible');
                                }
                                break;
                            case 13://enter
                                if(keyboardSelectedIndex != -1){
                                    keyboardSelected.click();
                                }
                                break;
                        }
                    }
                    if(listMode){
                        if(e.altKey){//alt control
                            //get current index and add 1
                            if(self.current.subsets){ 
                                var subindex = self.current.subsets.indexOf(self.current.subset)+1;//$('joe-subset-option.active').index()+1;
                                switch(code) {
                                    case 38://up
                                        $('joe-subset-option').eq(subindex-1).click();
                                    break;

                                    case 40://down
                                        $('joe-subset-option').eq(subindex+1).click();
                                    break;
                                }
                            }
                        }else{
                            var keyboardSelectedIndex = ($('.joe-panel-content-option.keyboard-selected').length)?
                                $('.joe-panel-content-option.keyboard-selected').index():-1;
                            //logit(keyboardSelectedIndex);

                            switch(code) {
                                case 38://up
                                    keyboardSelectedIndex--;
                                    if (keyboardSelectedIndex > -1) {
                                        keyboardSelectOption('.joe-panel-content-option', top);
                                    }
                                    break;
                                case 40://down
                                    keyboardSelectedIndex++;
                                    if (keyboardSelectedIndex < currentListItems.length) {
                                        keyboardSelectOption('.joe-panel-content-option', top);
                                    }
                                    break;
                                case 13://enter
                                    if (keyboardSelectedIndex != -1) {
                                        $('.joe-panel-content-option.keyboard-selected').find('.joe-panel-content-option-content').click();
                                    }
                                    break;
                            }
                        }
                    }else{//DETAILS MODE
                        if(e.altKey) {
                            switch(code) {
                                case 219://left
                                case 221://right
                                    var sside = (code ==219)?'left':'right';
                                    if(self.current.sidebars[sside].content && !self.current.sidebars[sside].hidden){
                                        self.toggleSidebar(sside)
                                    }
                                break;

                            }
                        }else if(e.ctrlKey) {
                            switch(code) {
                                case 37://left
                                    self.previous();
                                    break;
                                case 39://right
                                    self.next();
                                break;

                            }
                        }else if(code == 13){//add another row on enter click
                            var ae = $(document.activeElement);
                            if(ae && ae.is('input')){
                                var fieldobj = ae.parents('.joe-object-field');
                                if(fieldobj.length){
                                    try{
                                        fieldobj.find('.joe-plus-button').click();
                                    }catch(e){
                                        self.Error.add('adding new row to objectlist',e);
                                    }
                                }
                            }
                        }

                    }
                    function keyboardSelectOption(selector,top){
                        $(selector+'.keyboard-selected').toggleClass('keyboard-selected');
                        var el = $(selector).eq(keyboardSelectedIndex);
                        el.addClass('keyboard-selected');
                        self.overlay.find('.joe-submenu-search-field').blur();
                        // $('.joe-panel-content').scrollTop($('.joe-panel-content-option.keyboard-selected').offset().top);
                        el[0].scrollIntoView(top);
                        //var panel_content = self.overlay.find('.joe-panel-content');
                        //panel_content.animate({ scrollTop: panel_content.scrollTop()-10 });

                        //panel_content.scrollTop(panel_content.scrollTop()-10);
                    }
                }
            }
        }
    };
/*-------------------------------------------------------------------->
	2 | FRAMEWORK START
<--------------------------------------------------------------------*/
    this.getMode = function(){
        if(listMode){
            if(self.current.list){
                return 'list';
            }
        }
        if(self.current.object){
            return 'details';
        }
        if(self.current.list){
            return 'list';
        }
        else{
            return false;
        }
        /*
        if(listMode){return 'list';}
        return 'details';*/
    };
	this.renderFramework = function(content){
		var style = 'style-variant1';
        var html =
		'<joe-overlay class="joe-overlay sans cols-'+colCount+' '+style+' '
            +((self.specs.compact && ' compact ') || '')
            +((self.specs.sans && ' sans ') || '')
            +'" data-joeindex="'+this.joe_index+'">'+
			'<joe-panel class="joe-overlay-panel">'+
				(content || '')+
			'</joe-panel>'+
		//mini
			'<div class="joe-mini-panel">'+
			'</div>'+
		'</joe-overlay>';
		return html;
	};


	this.populateFramework = function(data,setts){
        /*|{
         featured:true,
         description:'primary function for populating and rendering JOE, called by goJoe and joe.show',
         tags:'populate, framework, core'
         }|*/
        self.current.cache = {};
		self.overlay.removeClass('multi-edit');
		var joePopulateBenchmarker = new Benchmarker();
        self.current.reset = self.current.reset || {};
        beginLogGroup('JOE population');
		//logit('------Beginning joe population');
		var specs = setts || {};
		self.current.specs = setts;
		self.current.data = data;
	//clean copy for later;
		self.current.userSpecs = $.extend({},setts);
        gridMode = (self.current.specs.viewMode == 'grid')?true:false;
        tableMode = (self.current.specs.viewMode == 'table')?true:false;


	//update history 1/2
		if(!self.current.specs.noHistory){
			self.history.push({
	/*			_joeHistoryTitle:self.overlay.find('.joe-panel-title').html(),
	*/			specs:self.current.userSpecs,
				data:self.current.data
			});
		}

		var schema = setts.schema || '';
		var profile = setts.profile || null;
		var callback = setts.callback || null;
		var datatype = setts.datatype || '';
		var title = setts.title || '';

		//callback
		if(callback){self.current.callback = callback;}
		else{self.current.callback = null;}


    /*-------------------------
     String data
     -------------------------*/
    if($.type(data) == 'string' && datatype != "string" && self.getDataset(data,{boolean:true})){

        if(!specs.schema && self.schemas[data]){
            schema = data; }
        data = self.getDataset(data);
    }

        /*setup schema*/
        specs.schema = this.setSchema(schema);

/*-------------------------
Column Count
-------------------------*/
        colCount = self.current.specs.colCount || (specs.schema && specs.schema.colCount) /*|| colCount*/ || 1;
        if(self.sizeClass == "small-size"){
            colCount = 1;
        }

/*-------------------------
    Preformat Functions
-------------------------*/
	specs.preformat =
        (specs.schema && specs.schema.preformat) ||
		specs.preformat ||
		function(d){return d;};

	data = specs.preformat(data);
/*-------------------------
	Object
-------------------------*/
	//when object passed in
    if($.type(data) == 'object' || datatype =='object'){
        specs.object = data;
        specs.menu = specs.menu || (specs.schema && specs.schema.menu) || self.specs.menu || (specs.multiedit && __defaultMultiButtons) || __defaultObjectButtons;

        specs.mode="object";
        self.current.object = data;

    }



/*-------------------------
 MultiEdit (Arrays)
 -------------------------*/
        self.toggleMultiEditMode(specs,data);
/*-------------------------
	Lists (Arrays)
-------------------------*/

	//when array passed in
		listMode = false;
		if($.type(data) == 'array' || datatype =='array'){
			listMode = true;
			specs.list = data;
			specs.menu = specs.listMenu || (specs.schema && specs.schema.listMenu )|| __defaultButtons;//__defaultMultiButtons;
			specs.mode="list";

			//TODO: filter list items here.
			self.current.list = data;


/*-------------------------
 Subsets
 -------------------------*/
            var currentSubsets;
			//setup subsets
            currentSubsets = setts.subsets || (specs.schema && specs.schema.subsets)||null;
			if(typeof currentSubsets == 'function'){
                currentSubsets = currentSubsets();
			}

        //a current subset selected
            if(self.current.specs.subset && currentSubsets && currentSubsets
                .where({$or:[{name:specs.subset},{id:specs.subset},{group_start:specs.subset}]}).length){
                    self.current.subset = currentSubsets
                    .where({$or:[{name:specs.subset}, {id:specs.subset},{group_start:specs.subset}]})[0]||false;
            }else{
                //all selected
                if(self.current.specs.subset == "All"){
                    self.current.subset = {name:"All",filter:{}}
                }else {
                    //select default subset if it exists
                    self.current.subset =
                        (currentSubsets && currentSubsets.where({'default': true})[0]) || null;
                }
            }

            self.current.subsets = currentSubsets;

/*-------------------------
 Sorting
 -------------------------*/
            //setup sorting
            self.current.sorter = setts.sorter || (self.current.subset && self.current.subset.sorter)||(specs.schema && specs.schema.sorter)|| 'name';
            if($.type(self.current.sorter) == 'string'){self.current.sorter = [self.current.sorter];}

            //self.current.object = null;

		}

/*-------------------------
 Submenu
 -------------------------*/
    if(specs.mode == 'list') {
        self.current.submenu =
            self.current.specs.listsubmenu ||
            self.current.specs.submenu ||
            (specs.schema && specs.schema.listSubMenu) ||
            self.specs.listSubMenu;
    }else{
        self.current.submenu =
            self.current.specs.submenu ||
            (specs.schema && specs.schema.subMenu) ||
            self.specs.subMenu;
    }

        if(self.current.submenu == 'none'){
            self.current.submenu = null;
        }

/*-------------------------
	Rendering
-------------------------*/
	//when rendering passed in
		if($.type(data) == 'string' && datatype == 'rendering'){
			specs.rendering = data;

			specs.menu = [__replaceBtn__];
			specs.mode="rendering";
			self.current.rendering = specs.rendering;
			//specs.schema

		}

/*-------------------------
	String
-------------------------*/
	//when string passed in
		else if($.type(data) == 'string' || datatype == 'string'){
			specs.text = data;
			specs.menu = __defaultButtons;
			//specs.menu = [{name:'save',label:'Save Object',action:'_joe.updateObject()'}];
			specs.mode="text";
			self.current.text = specs.text;

		}



	//setup window title
		//specs.title = title || (specs.schema)? specs.schema._title : "Viewing "+specs.mode.capitalize();
		specs.listWindowTitle = (
            specs.list
            && (
                specs._listMenuTitle ||
                //specs.listWindowTitle ||
                specs._listWindowTitle ||
                self.getCascadingProp('listWindowTitle')||
                getProperty('specs.list.windowTitle')
                || (specs.schema &&
                    (specs.schema._listMenuTitle || specs.schema._listWindowTitle)
                ))

            )||false;
        specs.title =(
            specs.listWindowTitle
			||title
			|| (specs.schema && (specs.schema.title||specs.schema._title))
            || "Viewing "+((self.current.schema && self.current.schema.__schemaname)
                ||(typeof self.current.userSpecs.schema == 'string' && self.current.userSpecs.schema)
                || specs.mode).capitalize());
	//setup profile
		specs.profile = (profile)?
			(self.specs.profiles[profile]||self.specs.joeprofile):
			self.specs.joeprofile;

		self.current.profile = specs.profile;


	//cleanup variables
		self.cleanUp();

/*-------------------------------------------------------------------->
 Set global view mode specs
 <--------------------------------------------------------------------*/
        if(self.current.schema &&(self.current.schema.table||self.current.schema.tableView)){
            tableSpecs = $.extend({cols:['name',self.getIDProp()]},
                (self.current.schema
                    &&(self.current.schema.table||self.current.schema.tableView)
                    // &&(self.current.schema.table||self.current.schema.tableView).cols
                ) ||{});
        }else{
            tableSpecs = null;
        }
    /*-------------------------------------------------------------------->
     Framework Rendering
     <--------------------------------------------------------------------*/
        var contentBM = new Benchmarker();
        beginLogGroup('Content');
        var content = self.renderEditorContent(specs);
        endLogGroup();
        _bmResponse(contentBM,'JOE [Content]');
        var chromeBM = new Benchmarker();
		var html =

			self.renderEditorHeader(specs)+
            self.renderEditorSubmenu(specs)+
            content+
			self.renderEditorFooter(specs)+
			self.renderMessageContainer();

        _bmResponse(chromeBM,'JOE [overlay-chrome]');
		    self.overlay.find('.joe-overlay-panel').html(html);
		//$('.joe-overlay-panel').html(html);

	//update history 2/2	- add title
		if(!self.current.specs.noHistory && self.history.length){
			$.extend(self.history[self.history.length-1],
                {_joeHistoryTitle:self.overlay.find('.joe-panel-title').html()}
            );
		}
    //clear ace_editors
/*        for (var p in _joe.ace_editors){
            _joe.ace_editors[p].destroy();
        }
        _joe.ace_editors = {};*/

    //update hashlink
        self.updateHashLink();
		//logit('Joe Populated in '+joePopulateBenchmarker.stop()+' seconds');
        _bmResponse(joePopulateBenchmarker,'----Joe Populated');
        endLogGroup();
		return html;
	};
/*-------------------------------------------------------------------->
 2e | FRAMEWORK END
 <--------------------------------------------------------------------*/

    this.toggleMultiEditMode = function(specs, data){
        multiEdit =(self.current.userSpecs && self.current.userSpecs.multiedit)|| false;

    };

/*----------------------------->
	A | Header
<-----------------------------*/
    function createTitleObject(specs){
        var specs = specs || {};
        var titleObj = $.extend({},self.current.object);
        var list = specs.list || self.current.list;
        if(list){
            var lcount = list.length;
            if(self.current.subset){
                lcount = list.where(self.current.subset.filter).length;


            }
            titleObj._listCount =(lcount||'0');
            titleObj._subsetName = self.current.subset && self.current.subset.name+' ' ||'';
        }
        self.current.title = specs.title || self.getCascadingProp('title') || (self.current.schema && "new "+self.current.schema.__schemaname)||'Json Object Editor';
        self.current.subtitle = specs.subtitle || self.getCascadingProp('subtitle'); 
        var title = fillTemplate(self.propAsFuncOrValue(self.current.title),titleObj);
        var subtitle = fillTemplate(self.propAsFuncOrValue(self.current.subtitle),titleObj);
        titleObj.docTitle = title;
        titleObj.subTitle = subtitle
        return titleObj;
    }
    this.Header = {}
    this.toggleHelpMenu = function(show,target){

    }
	this.Header.Render = this.renderEditorHeader = function(specs){
        var BM = new Benchmarker();
		var specs = specs || {};
        var titleObj = createTitleObject(specs);
        var title = titleObj.docTitle || (self.current.schema && "new "+self.current.schema.__schemaname);
        var subtitle = titleObj.subTitle;

        //show doctitle
        if(self.specs.documentTitle){
            var doctitle = (self.specs.documentTitle === true)?
                self.current.title:self.propAsFuncOrValue(self.specs.documentTitle,self.current.title);
            document.title = fillTemplate(self.propAsFuncOrValue(doctitle),titleObj);
        }
		var close_action = specs.close_action||'onclick="getJoe('+self.joe_index+').closeButtonAction()"';
        var reload_action = specs.reload_action||'onclick="getJoe('+self.joe_index+').reload()"';
		function renderHeaderBackButton(){
			var html = '';
			if(self.history.length > 1 /*&& specs.useHeaderBackBtn*/){

                html+= '<div class="jif-header-back-btn jif-panel-header-button standard-button" onclick="window.history.back()" title="back">'
                +self.SVG.icon.left
                //+'<span class="jif-arrow-left"></span>'
                +'</div>';
                
			}
			return html;
		}
        function renderHelpButton(){
            var html = '';
            if(specs.minimode){
                return '';
            }
            html+= '<div class="jif-panel-header-button joe-panel-help" onclick="_joe.toggleHelpMenu()" title="help">'+self.SVG.icon.help+'</div>';
			return html;
        }
        var help_button = renderHelpButton();

        function renderUnsavedIcon(){
            var html = '';
            if(specs.minimode){
                return '';
            }
            html+= '<div class="jif-panel-header-button joe-panel-unsaved" onclick="_joe.updateObject(this,null,true);" title="unsaved changes">'+self.SVG.icon.unsaved+'</div>';
			return html;
        }
        var unsaved_icon = renderUnsavedIcon();

      var speech_action = specs.speech_action||'onclick="getJoe('+self.joe_index+').Speech()"';
        var reload_button = (specs.minimode)?'':
        '<div class="jif-panel-header-button joe-panel-reload" title="reload" '+reload_action+'><span class="jif-reload"></span></div>';
        var mic_button = (!self.specs.speechRecognition||specs.minimode)?'':
        '<div class="jif-panel-header-button joe-panel-speech" title="speech" id="speech-button-'+self.joe_index+'">M</div>';

        var close_button = '<div class="jif-panel-header-button joe-panel-close" title="close" '+close_action+'>' 
        +self.SVG.icon.close
        //+'<span class="jif-close"></span>'+
        +'</div>';

        // var schema_button = (!specs.minimode && (self.current.schema && self.current.schema.menuicon && 
        //         '<joe-schema-icon class="clickable" title="'+self.current.schema.__schemaname+'" onclick="goJoe(_joe.getDataset(\''+self.current.schema.__schemaname+'\'),{schema:\''+self.current.schema.__schemaname+'\'})">'+self.current.schema.menuicon+'</joe-schema-icon>')||'');
        
        var schema_button = (!specs.minimode && (self.current.schema && self.current.schema.menuicon && 
            '<joe-schema-icon class="clickable" title="'+self.current.schema.__schemaname+'" onclick="goJoe(\''+self.current.schema.__schemaname+'\')">'+self.current.schema.menuicon+'</joe-schema-icon>')||'');
        var back_button = (!specs.minimode && renderHeaderBackButton() || '')
        var left_buttons = [  
            back_button,
            schema_button
        ];
        var right_buttons = [  
            close_button,      
            mic_button,
            reload_button,
            //help_button, TODO: add help button.
            unsaved_icon
        ];
		var html =
		//'<div class="joe-panel-header">'+
        '<joe-panel-header >'+
			((specs.schema && specs.schema.subsets && self.renderSubsetselector(specs.schema)) || (specs.subsets && self.renderSubsetselector(specs)) || '')+
            
            
            //'<joe-panel-header-buttons class="left-side">'+
            left_buttons.join(' ')+
			//'</joe-panel-header>'+
            '<div class="joe-vcenter joe-panel-title-holder"><span class="joe-panel-title">'+
				(('<div>'+title+'</div>').toDomElement().innerText || title || 'Json Object Editor')+
 
			'</span></div>'+
        //'<div class="joe-panel-reload joe-panel-header-button" title="reload" '+reload_action+'></div>'+
        '<joe-panel-header-buttons class="right-side">'+
        right_buttons.join(' ')+
        '<div class="clear"></div>'+
        '</joe-panel-header-buttons>'+
			'<div class="clear"></div>'+
		'</joe-panel-header>';
        _bmResponse(BM,'[Header] rendered');
		return html;
	};

    //What happens when the user clicks the close button.
	this.closeButtonAction = function(prechecked){
        if(!this.checkChanges()){
            return;
        }
		self.history = [];
        self.panel.addClass('centerscreen-collapse');
        self.hide(500);

        self.current.clear();
        $(self.container).trigger({
            type: "hideJoe",
            index:self.joe_index/*,
            schema: self.current.specs.schema,
            subset: self.current.specs.subset*/
        });
        self.updateHashLink();
        var closeAction = self.getCascadingProp('onPanelHide');
        if(closeAction){ closeAction(self.getState())}
        
        self.Autosave.deactivate();
	};
    var goingBackFromID;
    var goingBackQuery;
	this.goBack = function(obj){
        if(!this.checkChanges()){
            return;
        }
        //go back to last item and highlight
        if(self.current.object) {
            var gobackItem = self.current.object[self.getIDProp()];
            if (gobackItem) {
                goingBackFromID = gobackItem;
               // logit(goingBackFromID);
            }
        }
        //clearTimeout(self.searchTimeout );
		self.history.pop();
		var joespecs = self.history.pop();
		if(!joespecs){
            self.closeButtonAction(true);
			return;
		}else{
            if(obj && $c.isArray(joespecs.data)){
                var objid = obj[self.getIDProp()];
                if(objid){
                    var query = {};
                    query[self.getIDProp()] = objid;
                    var found = joespecs.data.where(query);
                    found.length && $.extend(found[0],obj);
                }
            }
            if(joespecs.keyword){
                goingBackQuery = joespecs.keyword;
            }
        }

        var specs = $.extend({},joespecs.specs);
        specs.filters = joespecs.filters;
		self.show(joespecs.data,specs);
	};

/*    this.clearAuxiliaryData = function(){
        /!*|{
         featured:true,
         description:'cleans up variables that currently exist between a single JOE showings.',
         tags:'cleanup, reset',
         category:'core'
         }|*!/
        self.current.list = null;
        self.current.subsets = null;
        self.current.subset = null;
        self.current.filters = {};
        self.current.fields = [];
        self.current.schema = null;
        self.current.object = null;
        self.current.reset = {};

    };*/
    //this.current.clear = this.clearAuxiliaryData;
	this.cleanUp = function(){
        /*|{
         featured:true,
         description:'cleans up variables that currently exist between a single JOE showings.',
         tags:'cleanup, reset',
         category:'core'
         }|*/
/*        if(!self._reloading){
            self.current.reset = {};
        }*/
        self.current.fields = [];
        self.shiftSelecting = false;
        self.allSelected = false;
/*        for (var p in _joe.ace_editors){
            _joe.ace_editors[p].destroy();
        }
        _joe.ace_editors = {};*/
        if(self.current.userSpecs && self.current.userSpecs.multiedit){
            self.overlay.addClass('multi-edit');

        }else{
            self.overlay.removeClass('multi-edit');
        }

	};
/*----------------------------->
 B | SubMenu
 <-----------------------------*/
    function renderSubmenuButtons(buttons){
        var html = '';//<div class="joe-submenu-buttons">';
        var buttons = self.propAsFuncOrValue(buttons
            || (listMode && self.getCascadingProp('headerListMenu'))
            || (!listMode && self.getCascadingProp('headerMenu')));
        var content;
        if(buttons){
            var h = self.renderMenuButtons(buttons,'joe-submenu-button');
            if(h){content = true;}
            html+= h;
        }
        return {html:html,content:content};
       // return {html:html +'</div>',content:content};
    };
    //self.current.submenuButtons;
    this.renderEditorSubmenu = function(specs) {
        beginLogGroup('Submenu');
        var BM = new Benchmarker();
        var sectionAnchors =renderSectionAnchors();
        //submenu buttons
        var submenuButtons =  renderSubmenuButtons();
        if(!self.current.submenu && !sectionAnchors.count && (!listMode && !self.getCascadingProp('headerMenu'))){
            return '';
        }
        var showFilters =
            $c.getProperty(self.current,'userSpecs.filters') ||
            $c.getProperty(self.current,'schema.filters') ||
            $c.getProperty(self.current,'schema.subsets')||
            false;
        var subSpecs = {
            search:true,
            itemcount:true,
            filters:showFilters,
            numCols:self.current.schema.columns||0
        };
        var userSubmenu = ($.type(self.current.submenu) != 'object')?{}:self.current.submenu;
        $.extend(subSpecs,userSubmenu);



        if(specs.mode == 'list') {
            var filters_obj = self.Filter.getCached();
            var submenu =
                '<div class="joe-panel-submenu">'
            //right side
                + self.Aggregator.renderToggleButton()
                + self.renderViewModeButtons(subSpecs)
                + self.renderColumnCountSelector(subSpecs.numCols)
                + self.renderSorter()

            //left side
                + ((subSpecs.filters && self.renderSubmenuFilters(subSpecs.filter)) || '')
                + ((subSpecs.search && self.renderSubmenuSearch(subSpecs.search)) || '')
                + ((subSpecs.itemcount && self.renderSubmenuItemcount(subSpecs.itemcount)) || '')

                +submenuButtons.html

                + '</div>'
            //content area
                + "<div class='joe-filters-holder'>"
                + renderSubsetsDiv()
                + ((filters_obj && renderFiltersDiv()) || '')
                    // +'<span class="jif-arrow-left"></span>'
                + "</div>"
                + self.Aggregator.renderAggregatorHolder(false);
        }else{
            var submenu =
                '<div class="joe-panel-submenu">'
                + renderSidebarToggle('left')+ renderSidebarToggle('right')
                 +submenuButtons.html
                    + ((sectionAnchors.count && sectionAnchors.code) || '')
                    
                   
                + '</div>';/*
                + "<div class='joe-filters-holder'>"
               // + renderSubsetsDiv()
                //+ (self.current.schema && (self.propAsFuncOrValue(self.current.schema.filters) && renderFiltersDiv()) || '')
                    // +'<span class="jif-arrow-left"></span>'
                + "</div>";*/
        }
        function renderOptStripeColor(opt){
            var color = self.propAsFuncOrValue((opt.stripecolor||opt.stripeColor),null,null,opt);
            var title = '';
            if(color && $c.isObject(color.title)){
                title = ' title="'+color.title+'" ';
                color = color.color;
            }
            var h = '<joe-stripe-color '+title+(color && 'style="background-color:'+color+';"' || '')+'></joe-stripe-color>';
            return h;
        }
        function renderOptBGColor(opt){
            var color = self.propAsFuncOrValue((opt.bgcolor||opt.bgColor),null,null,opt);
            if(color && $c.isObject(color.title)){
                color = color.color;
            }
            var h = '<joe-bg-color '+(color && 'style="background-color:'+color+';"' || '')+'></joe-bg-color>';
            return h;
        }
        function renderOptBorderColor(opt){
            var color = self.propAsFuncOrValue((opt.bordercolor||opt.borderColor),null,null,opt);
            if(color && $c.isObject(color.title)){
                color = color.color;
            }
            var h = '<joe-border-color '+(color && 'style="border:1px solid '+color+';"' || '')+'></joe-border-color>';
            return h;
        }

        function renderOptionGroupStart(opt,childDom){
            var collapsed =self.propAsFuncOrValue(opt.collapsed)?'collapsed':'';
            if(!opt.hasOwnProperty('filter')){
            
                var html='<joe-option-group id="'+opt.group_start+'-group" class="'+collapsed+'"><joe-menu-label onclick="$(this).parent().toggleClass(\'collapsed\');">'
                +(opt.display||opt.name || opt.group_start)+'</joe-menu-label>';
                
                return html;
            }else{
                var optdisplay = opt.display || opt.name || opt.group_start || '';
                var optname = opt.id ||  opt.name || opt.group_start || '';
                var cbox='',act,onclick;
                var dataname;
                if(childDom == 'joe-filter-option'){
                    dataname = ' data-filter="'+optname+'" ';
                    cbox= '<span class ="joe-option-checkbox"></span>';
                    act = (self.current.filters && self.current.filters[optname])?'active':'';
                    onclick = 'getJoe(' + self.joe_index + ').toggleFilter(\'' + optname + '\',this);_joe.Utils.stopPropagation();';
                }else{
                    dataname = ' data-subset="'+optname+'" ';
                    act = ((self.current.subset && self.current.subset.name == optname) || !self.current.subset && opt.name =="All") ? 'active' : '';
                    onclick = 'getJoe(' + self.joe_index + ').selectSubset(\'' + (optname).toString().replace(/\'/g,"\\'") + '\');';
                }


                
                var optiondiv = '<'+childDom+'  '+dataname+' class="'+childDom+' joe-group-option ' + act + ' " onclick='+onclick+'>' 
                        +renderOptBorderColor(opt)
                        +renderOptBGColor(opt)
                        +renderOptStripeColor(opt)
                        +cbox
                        + '<span style="position:relative;">'+optdisplay+'</span>' 
                        + '</'+childDom+'>'
                
                var html='<joe-option-group id="'+opt.group_start+'-group" class="'+collapsed+' ">'
                +'<joe-menu-label onclick="$(this).parent().toggleClass(\'collapsed\');">'+optiondiv+'</joe-menu-label>';
                
                return html;
            }
        }
        //TODO:move to subsets filter rendering function
        function renderSubsetsDiv(){
            var sh = '<div><joe-menu-label title="select one subset at a time"><b>Subsets</b></joe-menu-label>';
            var act;
            var starting = [];
            if(!self.current.schema || !self.current.schema.hideAllSubset){
                starting.push({name:'All',filter:{}});
            }
            var group = null;
            starting.concat(_joe.current.subsets||[]).map(function(opt){
                if(opt.group_start){
                    group = opt.group_start;
                    sh+= renderOptionGroupStart(opt,'joe-subset-option');
                }else if(opt.group_end){
                    sh+='</joe-option-group>';
                    group = null;
                }else{
                    var idval = opt.id || opt.name || '';
                    //if(!opt.condition || (typeof opt.condition == 'function' && opt.condition(self.current.object)) || (typeof opt.condition != 'function' && opt.condition)) {
                    if(!opt.condition || self.propAsFuncOrValue(opt.condition)) {
                        act = ((self.current.subset && self.current.subset.name == opt.name) || !self.current.subset && opt.name =="All") ? 'active' : '';

                        if(act == 'active' && group){
                            sh = sh.replace('<joe-option-group id="'+group+'-group" class="collapsed',
                            '<joe-option-group id="'+group+'-group" class="');
                        }
                        
                        // sh += '<joe-subset-option  data-subset="'+idval+'" class="joe-subset-option ' + act + ' " onclick="getJoe(' + self.joe_index + ').selectSubset(\'' 
                        // + (opt.id || opt.name || '').toString().replace(/\'/g,"\\'") + '\');">' 
                        // +renderOptBorderColor(opt)
                        // +renderOptBGColor(opt)
                        // +renderOptStripeColor(opt)
                        // + '<span style="position:relative;">'+opt.name+'</span>' 
                        // + '</joe-subset-option>';

                        let optCount = self.current.list.where(opt.filter).length;
                        sh += `<joe-subset-option  data-subset="${idval}" class="joe-subset-option ${act} " 
                            onclick="getJoe(${self.joe_index}).selectSubset(\'${(opt.id || opt.name || '').toString().replace(/\'/g,"\\'")}\');">`
                        +renderOptBorderColor(opt)
                        +renderOptBGColor(opt)
                        +renderOptStripeColor(opt)
                        + '<span style="position:relative;">'+opt.name+'</span>' 
                        + `<option-count>${optCount}</option-count>`
                        + '</joe-subset-option>';
                        
                    }
                }
            });
                //+fillTemplate('<div class="joe-filters-subset">${name}</div>',_joe.current.subsets||[])
                sh+='</div>';
                _bmResponse(BM,'subsets rendered');
            return sh;
        }


        function renderFiltersDiv(){
            _bmResponse(BM,'filters start');
            var fh = '<div><joe-menu-label title="select multiple filters at once"><b>Filters</b></joe-menu-label>';
            var filters = self.current.schema.filters;
            filters = self.propAsFuncOrValue(filters);
            var group = null;
            (filters||[]).map(function(opt){
                if(opt.group_start){
                    group = opt.group_start;
                    fh+= renderOptionGroupStart(opt,'joe-filter-option');
                }else if(opt.group_end){
                    fh+='</joe-option-group>';
                    group = null;
                }else{
                    var idval = opt.id || opt.name || '';
                    if(!opt.condition || self.propAsFuncOrValue(opt.condition)) {
                        act = '';
                        if(self.current.filters && self.current.filters[idval]){
                            act = 'active'
                        }else if(opt.hasOwnProperty('default')){
                            act = (self.propAsFuncOrValue(opt.default))?'active':'';
                            self.toggleFilter(opt.name);
                        }
                        
                        
                        if(act == 'active' && group){
                            fh = fh.replace('<joe-option-group id="'+group+'-group" class="collapsed">',
                            '<joe-option-group id="'+group+'-group" class="">');
                        }
                        let optCount = self.current.list.where(opt.filter).length;
                        fh += '<joe-filter-option data-filter="'+idval+'" class="joe-filter-option ' + act + '" onclick="getJoe(' + self.joe_index + ').toggleFilter(\'' + idval + '\',this);">'
                            +renderOptBorderColor(opt)
                            +renderOptBGColor(opt)
                            +renderOptStripeColor(opt)
                            +'<span class ="joe-option-checkbox"></span>' 
                            + '<span style="position:relative;">'+opt.name+'</span>' 
                            + `<option-count>${optCount}</option-count>`
                        +'</joe-filter-option>'
                    }
                }
            });

            fh+='<joe-subset-option class="joe-subset-option clear-filters" onclick="'+getSelfStr+'.clearFilters()">clear filters</joe-subset-option>';
            fh+='</div>';
            _bmResponse(BM,'filters rendered');
            return fh;
        }

        endLogGroup();  
        _bmResponse(BM,'[Submenu] rendered');
        return submenu;
    };
    
    function renderSectionAnchors(){
        var anchorhtml = '<joe-submenu-section-anchors>'
            +'<joe-panel-button class="sections-toggle" onclick="'+getSelfStr+'.Sections.toggle(this);">'+self.SVG.icon.sections+' <joe-button-label>'
            +(self.current.sections && self.current.sections.count()||'')+' sections</joe-button-label></joe-panel-button>';
        anchorhtml+='<div class="joe-submenu-section" onclick="_joe.Sections.gotoTop('+self.joe_index+')">^ top</div>';
        var scount = 0;
        var onclick='onclick="getJoe('+self.joe_index+').gotoSection(\'${id}\');"';
        var ondblclick='ondblclick="getJoe('+self.joe_index+').gotoSection(\'${id}\',{focus:\'${id}\'});"';
        var template =
            //'<div class="joe-submenu-section" onclick="$(\'.joe-content-section[data-section=${id}]\').removeClass(\'collapsed\')[0].scrollIntoView()">${name}</div>';
            '<joe-submenu-section class="joe-submenu-section f${renderTo}" data-section="${name}" data-panel="${renderTo}" '+onclick+' '+ondblclick+'>${${anchor}||${name}}</joe-submenu-section>';

        var section;
        for(var secname in self.current.sections){
            section = _getSection(secname);
            if(!section.hidden) {
                anchorhtml += fillTemplate(template, section);
                scount++;
            }
        }
        anchorhtml+="<div class='clear'></div></joe-submenu-section-anchors>";
        return {count:scount,code:anchorhtml};
    }

    self.gotoSection =function(section,specs){
        var specs = $.extend({
        },(specs ||{}))
        if (section){
            var i = specs.index || self.joe_index;
            var sectionDom = $('.joe-overlay[data-joeindex='+i+']')
                .find('.joe-content-section[data-section=\''+section+'\']');
            
            var force_activation,activated = sectionDom[0].hasClass('active');
            if(specs.hasOwnProperty('activate')){
                force_activation = specs.activate;
            }
            var submenuDom = $(specs.dblclickdom) ||self.overlay.find('joe-submenu-section[data-section="'+section+'"]');


            if(specs.focus){
                var tabbedmode,
                parent_dom = sectionDom.parents('.joe-content-sidebar,joe-panel-content');
                //$(specs.dblclickdom).domToToggle('active',force_activation);
                if(specs.hasOwnProperty('activate')){
                    submenuDom.toggleClass('active',force_activation);
                    sectionDom.toggleClass('active',force_activation);
                }else{
                    submenuDom.toggleClass('active',!activated);
                    sectionDom.toggleClass('active',!activated);
                }
                tabbedmode = (submenuDom.parent().children('.active[data-panel='+submenuDom.data('panel')+']').length > 0);
            // if(tabbedmode){
                    parent_dom.toggleClass('joe-tabbed-content',tabbedmode);
            // }
            }
            if(force_activation || !activated){
                
                sectionDom.removeClass('collapsed')[0].scrollIntoView();
                var sidebar = sectionDom.parents('.joe-content-sidebar');
                if(sidebar && sidebar.length){
                    var s = sidebar.data('side');
                    self.toggleSidebar(s,true);
                    sectionDom.siblings('.joe-content-section').addClass('collapsed');
                }else if(self.sizeClass == "small-size"){
                    self.toggleSidebar('left',false);
                    self.toggleSidebar('right',false);
                }
                //collapse in mobile view
                $('joe-submenu-section-anchors.expanded').removeClass('expanded');
            }else{


                //collapse in mobile view
                $('joe-submenu-section-anchors.expanded').addClass('expanded');
            }
        }
    };
    self.Sections = {
        renderAchors:renderSectionAnchors,
        goto:self.gotoSection,

        gotoTop:function(index){
            var ajoe = getJoe(index);
            $('.joe-overlay[data-joeindex='+index+']').find('.joe-panel-content').scrollTop(0);
            if(ajoe.panel.hasClass('small-size')){
                ajoe.toggleSidebar('right',false);
                ajoe.toggleSidebar('left',false);
            }
            $('joe-submenu-section-anchors.expanded').removeClass('expanded');
        }
    };
    self.Sections.toggleAll = function(expand){
        document.querySelectorAll('.joe-content-section').map(e=>{
            e.classList.toggle('collapsed',!expand)
            })
    }
    self.Sections.collapseAll = function(){
        document.querySelectorAll('.joe-content-section').map(e=>{
            e.classList.toggle('collapsed',true)
            })
    }
    self.Sections.expandAll = function(){
        document.querySelectorAll('.joe-content-section').map(e=>{
            e.classList.toggle('collapsed',false)
            })
    }
    self.Sections.toggleSome = function(sections,expand){
        sections.map(sect=>{
            document.querySelectorAll('.joe-content-section[data-section="'+sect+'"]').map(e=>{
                e.classList.toggle('collapsed',!expand)
                })
        });
    
    }
    self.Sections.unFocusAll = function(){
        self.overlay.find('joe-submenu-section').removeClass('active');
        
    }
    self.Sections.activate = function(sections){

    }
    self.Sections.setTabbedMode=function(show){
        self.overlay.find('.joe-content-sidebar,joe-panel-content').toggleClass('joe-tabbed-content',show);
    }
    self.Sections.toggle = function(dom){
        if(dom){
            $(dom).parents('joe-submenu-section-anchors').toggleClass('expanded');
        }
    }
    self.Sections.showAll = function(){
        self.Sections.toggleAll(true);
    }
/*------------------>
    Subset
<------------------*/
    this.Subset = {
        getCached:function(list){
            var list = list || self.current.list
            var cache_query = self.current.schema.name+':'+self.current.subset.name;
            var subcache = self.Cache.get(cache_query)
            if(!subcache){
                subcache = list.where(self.current.subset.filter);
                self.Cache.set(subcache,{id:cache_query});
            }
            return subcache;
        }
    }
/*------------------>
    Filter
<------------------*/
    this.Filter = {
        refilter:function(){
            if(!$c.isEmpty(self.current.filters)){   
                self.filterListFromSubmenu(null,true);
            }
        },
        getCached:function(list){
            var BM = new Benchmarker();
            if(!self.current.schema || !self.current.schema.filters){
                _bmResponse(BM,'filters gotten');
                return false;
            }
            var list = list || self.current.list
            var cache_query = self.current.schema.name+':filters';
            var filtercache = self.Cache.get(cache_query)
            if(!filtercache){
                filtercache = self.propAsFuncOrValue(self.current.schema.filters,self.current.list);
                self.Cache.set(filtercache,{id:cache_query});
            }
            _bmResponse(BM,'filters gotten');
            return filtercache;
        }
    }
    this.renderSubmenuFilters = function(s){

        if(!(s || self.current.subsets|| (self.current.schema && self.propAsFuncOrValue(self.current.schema.filters)))){
           return '';
        }

        var action =' onclick="getJoe('+self.joe_index+').toggleFiltersMenu();" ';
        var active = (self.current.subset)?'active':'';
        var count = $c.itemCount(self.current.filters);
        var indicator = (active || count)?'<joe-count-indicator class="'+ active+((count && ' count ') || '')+'">'+(count||'')+'</joe-count-indicator>':'';
        var html =
            "<div class='jif-panel-submenu-button joe-filters-toggle ' "+action+" title='show filters'>"
                +indicator
                +"</div>";



        return html;
    };

    var leftMenuShowing = false;
    var rightMenuShowing = false;

    this.toggleFiltersMenu = function(){
        leftMenuShowing = !leftMenuShowing;
        self.panel.toggleClass('show-filters',leftMenuShowing);
    };
    this.toggleAggregatorMenu = function () {
        rightMenuShowing = !rightMenuShowing;
        self.panel.toggleClass('show-aggregator', rightMenuShowing);
      };
    this.Filter.Options={
        datasetProperty:function(dataset,property,specs){
            //use a dataset as the values for this filter
            var specs = specs || {};
            var filters = [];
            var groupname= (typeof(specs.group) == 'string')?specs.group:property;
            var dataset = self.getDataset(self.propAsFuncOrValue(dataset)).sortBy(specs.sortby || 'name');

            if(specs.query){
                dataset = dataset.where(specs.query);
            }
            if(!dataset.length){
                return [];
            }
            if(specs.group){
                filters.push({group_start:groupname,collapsed:specs.collapsed})
            }


            dataset.map(item=>{
                let props = {
                    name:item.name,
                    filter:{}
                }
                props.filter[property] = item._id;
                filters.push(props);
            });
            if(specs.group){
                filters.push({group_end:groupname})
            }
            return filters;
        },
        getDatasetPropertyValues:function(dataset,property,specs){
            //use all possible values of a property as the filters
            var specs = specs || {};
            var filters = [];
            var groupname= (typeof(specs.group) == 'string')?specs.group:property;
            var dataset = self.getDataset(self.propAsFuncOrValue(dataset)).sortBy(specs.sortby || 'name');

            if(specs.query){
                dataset = dataset.where(specs.query);
            }
            if(!dataset.length){
                return [];
            }
            if(specs.group){
                filters.push({group_start:groupname,collapsed:specs.collapsed})
            }

            var filterValues = specs.values || [];
            dataset.map(item=>{
                //get all values for a particular property

                if(item.hasOwnProperty(property)){
                    filterValues.push(item[property])
                }

            });
                filterValues = Array.from(new Set(filterValues));
            
                filterValues.map(v=>{
                    let filterObj = {
                        name:v,
                        filter:{}
                    }
                    filterObj.filter[property] = v;
                    filters.push(filterObj);
                })
                
            if(specs.group){
                filters.push({group_end:groupname})
            }
            return filters;
        },
        status : function(specs){
            var specs = specs || {};
            if($.type(schemaname) == "object"){
                specs = schemaname;
            }
            var schemaname = specs.schemaname || (self.current.schema && self.current.schema.name || '');
            var stats = [];
            var stat;
            var groupname= (typeof(specs.group) == 'string')?specs.group:'tags';
            if(specs.group){
                stats.push({group_start:groupname,collapsed:specs.collapsed})
            }
            self.Data.status.sortBy(specs.sortby || 'index')
            .where({datasets:{$in:[schemaname]}}).map(function(status){
                var sName = status.name;
                // if(specs.count){
                //     let count = self.Data[schemaname].where({status:status._id}).length;
                //     sName = `${sName} <option-count>${count}</option-count>`;
                // }
                stat= {name:sName,id:status._id,
                    filter:{status:status._id},
                        title:self.propAsFuncOrValue(specs.title)||status.name}
                stat[(specs.color||'bgcolor')] = status.color;
                stats.push(stat);
            })
            if(specs.none){
                let noName = 'no status';
                let noFil = {$or:[{status:{$in:[null,'']}},{status:{$exists:false}}]}
                // if(specs.count){
                //     let count = self.Data[schemaname].where(noFil).length;
                //     noName = `${noName} <option-count>${count}</option-count>`;
                // }
                stats.push({name:noName,filter:noFil})
            }
            if(specs.group){
                stats.push({group_end:groupname})
            }
            return stats;
        },
        tags : function(specs){
            var specs = specs || {};
            var schemaname = specs.schemaname || (self.current.schema && self.current.schema.name || '');
           
            var schemaname = specs.schemaname || (self.current.schema && self.current.schema.name || '');
            var filters = [];
            var groupname= (typeof(specs.group) == 'string')?specs.group:'tags';
            if(specs.group){
                filters.push({group_start:groupname,collapsed:specs.collapsed})
            }

            _joe.Data.tag.sortBy(specs.sortby || 'name').filter(function(tag){
                let fi = {tags:{$in:[tag._id]}};
                if(tag.datasets.indexOf(schemaname) != -1){
                    let sName = tag.name;
                    // if(specs.count){
                    //     let count = self.Data[schemaname].where(fi).length;
                    //     sName = `${sName} <option-count>${count}</option-count>`;
                    // }
                    filters.push({name:sName,id:tag._id,filter:fi});
                }
            });
            if(specs.untagged || specs.none){
                let noName = 'untagged';
                let noFil = {$or:[{tags:{$size:0}},{tags:{$exists:false}}]}
                // if(specs.count){
                //     let count = self.Data[schemaname].where(noFil).length;
                //     noName = `${noName} <option-count>${count}</option-count>`;
                // }
                filters.push({name:noName,
                    filter:noFil
                })
            }
            if(specs.group){
                filters.push({group_end:groupname})
            }
            return filters;
        }

    }
    this.Filter.generateQuery = this.generateFiltersQuery = function(){//comgine all active fitlers into a query
        var query = {};
        var filterinfo,filterobj,addFilter;
        //var qval, fval;
        for(var f in self.current.filters){
            filterinfo=(self.propAsFuncOrValue(self.current.schema.filters)||[]).where({name:f})[0] || self.current.filters[f] || {};
            addFilter = (!filterinfo.schema || 
                (!self.current.schema || !self.current.schema.__schemaname) || 
                filterinfo.schema == (self.current.schema.__Schemaname || self.current.schema.__schemaname) 
            );
            if(addFilter){
                filterobj = filterinfo.filter;
                for(var ff in filterobj) {
                    if (query[ff]) {
                        
                        //TODO: do crazy smart stuff here.
                        switch($.type(filterobj[ff])){
                            case 'number':
                            case 'string':
                            case 'object':
                                if($.type(query[ff]) == 'string' || $.type(query[ff]) == 'number'){
                                    query[ff]={
                                        $in:[query[ff],filterobj[ff]]
                                    };
                                }else if($.type(query[ff]) == 'object'){
                                    if(query[ff].hasOwnProperty('$in')){
                                        query[ff]['$in'] = query[ff]['$in'].concat(filterobj[ff]['$in'] || [filterobj[ff]]);
                                        //query[ff]['$in'] = query[ff]['$in'].concat(filterobj[ff]['$in']);
                                    }
                                }
                                break;
                        }
                    }else{//not there, add it
                        query[ff] = filterobj[ff];

                    }
                }
            }else{//remove from current filters list
                delete self.current.filters[filterinfo.name];
            }
        }
        logit(query);
        return query;
    };

/*------------------>
 Search
 <------------------*/
    self.Search = {
        schema:{
            title:'Search Results',
            name:'search',
            listTitle:function(item){
                var html = 
                '<joe-full-right>'
                    +'<joe-subtext>updated</joe-subtext>'
                    +'<joe-subtitle>'+self.Utils.toDateString(item.joeUpdated)+'</joe-subtitle>'
                    +'</joe-full-right>'
                +(_joe.schemas[item.itemtype].menuicon && '<joe-icon class="icon-40 icon-grey fleft">'+_joe.schemas[item.itemtype].menuicon+'</joe-icon>' || '')
                +'<joe-subtext>${itemtype}</joe-subtext>'
                +'<joe-title>${name}</joe-title><joe-subtitle>${info}</joe-subtitle>'
                +'<joe-subtext>${_id}</joe-subtext>';

                return html;
            },
            listmenu:[],
            sorter:['name',{name:'updated',value:'!joeUpdated'},'itemtype'],
            filters:function(){
                var f = [];
                __appCollections.concat().sort().map(function(s){
                    f.push({name:s,filter:{itemtype:s}});
                })
                return f;
            },
            listAction:function(item){
                return 'goJoeItem(\''+item._id+'\',\''+item.itemtype+'\')';
            //    return 'goJoe(_joe.search(\''+item._id+'\')[0],{schema:\''+item.itemtype+'\'});';
            }
        }
    }

    this.renderSubmenuSearch = function(s){
        var action =' onkeyup="_joe.filterListFromSubmenu(this.value);" ';
        var keyword = self.current.keyword || $GET('keyword')||'';
        var submenusearch =
            "<div class='joe-submenu-search '>"
            +'<input class="joe-submenu-search-field" '+action+' placeholder="search list" value="'+keyword+'"/>'
            +"</div>";
        return submenusearch;
    };

    this.searchTimeout;
    var searchTimeoutTime = ($c.isMobile())?750:500;
    this.filterListFromSubmenu = function(keyword,now){
        /*|{
            featured:true,
            tags:'filter,list',
            description:'Filters the list based on keywords,subsets and filters'
        }|*/
        clearTimeout(self.searchTimeout );
        self.overlay.removeClass('.multi-edit');
        if(!now) {
            self.searchTimeout = setTimeout(function () {
                searchFilter(keyword,true);
            }, searchTimeoutTime);
        }else{
                searchFilter(keyword);
        }
        function searchFilter(keyword,frominput){
            var searchBM = new Benchmarker();
            if(self.current.keyword != keyword){
                keyword = keyword || $('.joe-submenu-search-field').val() || '';
				if(!frominput && !keyword && self.current.keyword){
					keyword = self.current.keyword;
					$('.joe-submenu-search-field').val(keyword);
				}
                var value=keyword.toLowerCase();
                var keywords = value.replace(/,/g,' ').split(' ');
                var filters = self.generateFiltersQuery();

                _joe.history[_joe.history.length-1].keyword = value;
                _joe.history[_joe.history.length-1].filters = self.current.filters;


                var testable;
                var idprop = self.getIDProp();
                var id;
                var listables = self.current.list;
                if(self.current.subset){
                    listables = self.Subset.getCached();
                    
                }
                listables = cleanDeleted(listables);
                var searchables = self.current.schema && self.current.schema.searchables;
                _bmResponse(searchBM,'search where');
                self.current.filteredList = currentListItems = listables.where(filters).filter(function(i){
                    if(!keyword){return true;}
                    id = i[idprop];
                    testable = '';
                    if(searchables){//use searchable array
                        searchables.map(function(s){
                            testable+=i[s]+' ';
                        });
                        testable = testable.toLowerCase()+id;
                        for(var k = 0,tot = keywords.length; k<tot;k++){
                            if(testable.indexOf(keywords[k]) == -1){
                                return false;
                            }
                        }
                        //return (testable.toLowerCase().indexOf(value) != -1);

                        //return (testable+' '+i[idprop].toLowerCase().indexOf(value) != -1);
                    }else {
                        if (tableMode) {
                            testable = self.renderTableItem(i, true);
                        } else if (gridMode) {
                            //testable = self.renderListItem(i,true);
                        } else {
                            testable = self.renderListItem(i, true);
                        }
                        testable = testable.toLowerCase()+id;
                        for (var k = 0, tot = keywords.length; k < tot; k++) {

                            if (testable.indexOf(keywords[k]) == -1) {
                                return false;
                            }
                        }
                        //return ((__removeTags(testable)+id).toLowerCase().indexOf(value) != -1);
                    }
                    return true;
                });

                _bmResponse(searchBM,'search \''+keyword+'\' filtered '+currentListItems.length+'/'+self.current.list.length+' items ['+listables.length+']');
                self.overlay.find('.joe-submenu-itemcount').html(currentListItems.length+' item'+((currentListItems.length > 1 &&'s') ||''));
                self.panel.find('.joe-panel-content').html(self.renderListItems(currentListItems,0,self.specs.dynamicDisplay));
                //var titleObj = $.extend({},self.current.object,{_listCount:currentListItems.length||'0',_subsetName:(self.current.subset && self.current.subset.name ||'')});
                
                //self.panel.find('.joe-panel-title').html(fillTemplate(self.current.title,titleObj));
            }
            self.current.keyword = keyword;
            self.current.search_schema
            var titleObj = createTitleObject();
            if(titleObj.subTitle){
                self.panel.find('.joe-panel-title').addClass('subtitled').html(titleObj.docTitle
                +'<joe-subtext>'+titleObj.subTitle+'</joe-subtext>');
            }else{
                self.panel.find('.joe-panel-title').addClass('subtitled').html(titleObj.docTitle);
            }
            //update filters icons
            self.panel.find('.joe-filters-toggle').replaceWith(self.renderSubmenuFilters());
            self.Aggregator.update();
        } //end searchFilter

    };


/*------------------>
 Count
 <------------------*/

    this.renderSubmenuItemcount = function(s){

        var submenuitem =
            "<div class='joe-submenu-itemcount'>items</div>";
        return submenuitem;
    };

/*------------------------------------------------------>
    //SUBMENU SELECTORS
 <-----------------------------------------------------*/
    function renderSubmenuSelectors(specs){
        var specs = $.extend({
                options:[],
                content:'',
                action:'nothing',
                buttonTemplate: "<div class='selection-label'>${label}</div>${name}",
                label:'label',
                cssclass:''
            },(specs||{}));

        var selectionTemplate =
            "<div data-colcount='${name}' " +
                "onclick='getJoe("+self.joe_index+")" + "."+specs.action+"(${name});' "
                +"class='jif-panel-button selector-button-${name} joe-selector-button " +
                "'>"
                +specs.buttonTemplate
            +'</div>';

        var onclick_action = "onclick='$(this).parent().toggleClass(\"expanded\")'";
        var content = specs.content ||
            "<div class='joe-selector-button selector-label' "+onclick_action+">"+specs.label+"</div>"+
            "<div class='joe-selector-button selector-close joe-close-button' "+onclick_action+">&nbsp;</div>"+
            
            '<joe-submenu-selector-options>'
                +fillTemplate(selectionTemplate,specs.options)
            +'</joe-submenu-selector-options>';

       // var width = 40*specs.options.length + 40;
        var html="<div class='joe-submenu-selector' >"+ content+ "</div>";

        /*function checkSubmenuSelector(value){
            var submenuValue = value || this.value;
            return ''
        }*/
        return html;
    }
    this.nothing = function(nothing){
        alert(value);
    };
/*------------------>
View Mode Buttons
<------------------*/
    this.renderViewModeButtons = function(subspecs){
        var gridspecs = self.current.schema && (self.current.schema.grid || self.current.schema.gridView);
        var tablespecs = tableSpecs; //self.current.schema && (self.current.schema.table||self.current.schema.tableView);

        if(!gridspecs && !tablespecs){return '';}
        var modes = [
            {name:'list'}
        ];
        if(gridspecs){modes.push({name:'grid'})}
        if(tablespecs){modes.push({name:'table'})}

        var modeTemplate="<div data-view='${name}' " +
            "onclick='getJoe("+self.joe_index+")" + ".setViewMode(\"${name}\");' " +
            "class='jif-panel-button joe-viewmode-button ${name}-button joe-selector-button'>&nbsp;</div>";
        
        var onclick_action = "onclick='$(this).parent().toggleClass(\"expanded\")'";
        var submenuitem =
            "<div class='joe-submenu-selector joe-viewmode-selector opts-"+modes.length+"' >"+//onhover='$(this).toggleClass(\"expanded\")'
                "<div class='joe-selector-button selector-label' "+onclick_action+">view</div>"+
                "<div class='joe-selector-button selector-close joe-close-button' "+onclick_action+">&nbsp;</div>"+
            
            '<joe-submenu-selector-options>'+
                fillTemplate(modeTemplate,modes)+
            '</joe-submenu-selector-options>'+
                
            "</div>";
        //return '';
        return submenuitem;
    };

    this.setViewMode = function(mode){
        self.reload(true,{viewMode:mode});
    };



/*------------------------------------------------------>
 Column Count Buttons
 <-----------------------------------------------------*/
    this.renderColumnCountSelector = function(subspecs){
        if(!subspecs){
            return '';
        }
        var modes = [
            {name:'1'},
            {name:'2'},
            {name:'3'},
            {name:'4'},
            {name:'5'}
        ];
        if(typeof eval(subspecs) == "number"){
            modes = modes.slice(0,eval(subspecs));
        }
/*        var modeTemplate="<div data-colcount='${name}' " +
            "onclick='getJoe("+self.joe_index+")" + ".setColumnCount(${name});' " +
            "class='jif-panel-button selector-button-${name} joe-selector-button'>" +
            "<div class='selection-label'>cols</div>${name}</div>";
        var submenuitem =
            "<div class='joe-submenu-selector opts-"+modes.length+"'' >"+
            fillTemplate(modeTemplate,modes)+
            "</div>";*/

        var h = renderSubmenuSelectors({
            options:modes,
            //buttonTemplate:"<div class='selection-label'>cols</div>${name}",
            label:'cols',
            value:colCount,
            action:'setColumnCount'
        });
        return h;
        //return submenuitem;
    };
    this.setColumnCount = function(mode){
        self.overlay[0].className = self.overlay[0].className.replace(/cols-[0-9]/,'cols-'+mode);
        if(mode){colCount = mode;}
        var multi = (mode && mode > 1)?true:false;
        self.overlay.toggleClass('multi-col',multi)
        //self.reload(true,{colCount:mode});
    };


/*------------------------------------------------------>
    Submenu Sorter
 <-----------------------------------------------------*/
this.Sorter ={};

 this.Sorter.render = this.renderSorter = function(subspecs){
     var sorter = (self.current.subset && self.current.subset.sorter)
         ||(self.current.schema && self.current.schema.sorter)|| 'name';
     if($.type(sorter) == 'string'){sorter = sorter.split(',');}
     if(sorter.indexOf('name') == -1 && sorter.indexOf('!name') == -1 && !sorter.where({field:'name'}).length){
         sorter.push('name');
     }
     if(sorter.length == 1){
         return '';
     }
     var newsorter = subspecs;
     var current;
     return '<label for="joe-'+self.joe_index+'-sorter" class="joe-list-sorter" >' +
        '<svg onclick="getJoe('+self.joe_index+').Sorter.reverse();" class="joe-list-sorter-icon" title="reverse sort" xmlns="http://www.w3.org/2000/svg" viewBox="-20 6 40 40"><path d="M-4 16L-9.5 21.5 -8 23 -5 20 -5 28 -3 28 -3 20 0 23 1.5 21.5 -4 16zM3 16L3 18 5 18 5 16 3 16zM3 20L3 22 5 22 5 20 3 20zM3 24L3 32 0 29 -1.5 30.5 4 36 9.5 30.5 8 29 5 32 5 24 3 24zM-5 30L-5 32 -3 32 -3 30 -5 30zM-5 34L-5 36 -3 36 -3 34 -5 34z"/></svg>'+
         '<select title="sort" name="joe-'+self.joe_index+'-sorter" onchange="getJoe('+self.joe_index+').Sorter.resort(this.value);">' +
         sorter.map(function(so){
             var s = so, display = so;
             if (!$c.isString(so)) {
                 s = so.field || so.value;
                 display = so.display || so.name || s;
             }
             s= s.replace('!','');
             var valCheck =  
             (self.current.sorter[0].field || self.current.sorter[0].value ||self.current.sorter[0]).replace('!','');
                /* || (typeof self.current.sorter[0] == "string" &&(self.current.sorter[0] || ('!'+self.current.sorter[0])))
                 );*/
             current = (s == valCheck) ?'selected':'';
             /* if(self.current.sorter[0] == s){
              return '<option value="!'+s+'">!'+s+'</option>';
              }*/
             return '<option '+current+' value="'+s+'">'+display+'</option>';
         }).join('')+
         '</select></label>';
    };

    this.Sorter.resort = function(sorter){
        self.reload(true,{sorter:sorter})
    };
    this.Sorter.reverse = function(sorter){
        var curSorter = self.current.sorter;
        if($.type(curSorter) == "array"){
            curSorter = [].concat(curSorter);
            if($.type(curSorter[0]) == "string"){
                if(curSorter[0].indexOf('!')==0){
                    curSorter[0] = curSorter[0].substr(1)
                }else{
                    curSorter[0] ='!'+curSorter[0];
                }
            }else if($.type(curSorter[0]) == "object" && curSorter[0].value){
                if(curSorter[0].value.indexOf('!')==0){
                    curSorter[0].value = curSorter[0].value.substr(1)
                }else{
                    curSorter[0].value ='!'+curSorter[0].value;
                }
            }
            self.reload(true,{sorter:curSorter})
            
        }
    };

  /*------------------------------------------------------>
      Submenu Aggregator
  <-----------------------------------------------------*/
  this.Submenu = this.Submenu || {};
  this.Aggregator = {};
  this.Aggregator.renderToggleButton = function(){
    if(!(s && (self.current.schema && self.current.schema.hasOwnProperty('aggregator')))){
        return '';
     }

     var action =' onclick="getJoe('+self.joe_index+').toggleAggregatorMenu();" ';
     var html =
         "<div class='jif-panel-submenu-button joe-aggregator-toggle ' "+action+" title='show aggregator'>"
             +"</div>";



     return html;
  }
  this.Aggregator.renderAggregatorHolder = function(contentOnly,content){
    if(!(s && (self.current.schema && self.current.schema.hasOwnProperty('aggregator')))){
        return '';
     }
    
    var content = self.propAsFuncOrValue(self.current.schema.aggregator,(self.current.filteredList||self.current.list));
    //var content = aggFunc(self.current.list);
    var html =content;
      if(!contentOnly){
        html= "<div class='joe-aggregator-holder'>" + content+ "</div>";
      }
    return html;
  }
  this.Aggregator.update = function(list){
    var list = list || self.current.filteredList;
    var content = self.Aggregator.renderAggregatorHolder(true);

    var dom = document.querySelector('.joe-overlay[data-joeindex="0"]').getElementsByClassName('joe-aggregator-holder')[0];
    dom && (dom.innerHTML = content);
  }
    /*----------------------------->
        C | Editor Content
    <-----------------------------*/
    function renderEditorStyles(){
        var style_str = 
        (self.specs.style && self.specs.style.inset &&'joe-style-inset '||'') 
        +(self.specs.style && self.specs.style.cards &&'joe-style-cards '||'');
        return style_str;
    }
	this.renderEditorContent = function(specs){

        self.current.sidebars = {left:{collapsed:false},right:{collapsed:false}};
		//specs = specs || {};
		var content;

		if(!specs){
			specs = {
				mode:'text', //text,list,single
				text:'No object or list selected'
			};
		}
		var mode = specs.mode;

		switch(mode){
			case 'text':
				content = self.renderTextContent(specs);
			break;
			case 'rendering':
				content = self.renderHTMLContent(specs);
			break;
			case 'list':
				content = self.renderListContent(specs);
			break;
			case 'object':
				content = self.renderObjectContent(specs);
			break;

            default:
            content = content || '';
            break;

		}
        var submenu = '';
        if(!specs.minimode) {
            if ((mode == 'list' && self.current.submenu)
                || (self.current.submenu || renderSectionAnchors().count || renderSubmenuButtons().content)) {
                submenu = ' with-submenu '
            }
        }
        //submenu=(self.current.submenu || renderSectionAnchors().count)?' with-submenu ':'';
        var scroll = 'onscroll="getJoe('+self.joe_index+').onListContentScroll(this);"';
        var rightC = content.right||'';
        var leftC = content.left||'';
        //var content_class='joe-panel-content joe-inset ' +submenu;
        var content_class='joe-panel-content '
            +renderEditorStyles()
            
            +submenu;
		var html =


		'<joe-panel-content class="'+content_class+'" ' +((listMode && scroll)||'')+'>'
        +(content.main||content)
		+'</joe-panel-content>'
        +self.renderSideBar('left',leftC,{css:submenu})
        +self.renderSideBar('right',rightC,{css:submenu});
        self.current.sidebars.left.content = leftC;
        self.current.sidebars.right.content = rightC;
		return html;
	};

    this.renderSideBar = function(side,content,specs){
        var side = side || 'right';
        var expanded='';//(content && ' expanded ') ||'';
        var specs = specs || {};
        var addCss = specs.css || '';
        var html="<div class='joe-content-sidebar "+renderEditorStyles()+" joe-absolute "+side+"-side "+expanded+ addCss+"' data-side='"+side+"'>"+(content||'')+__clearDiv__+"</div>";
        return html;
    };
    this.toggleSidebar = function(side,hardset){
        if(['right','left'].indexOf(side) == -1){
            return false;
        }
        self.panel.toggleClass(side+'-sidebar',hardset);
        /*        $('.joe-panel-content').toggleClass(side+'-sidebar',hardset)
         $('.joe-content-sidebar.'+side+'-side').toggleClass('expanded',hardset)*/
    };

    function renderSidebarToggle(side){
        if(self.current.sidebars[side].hidden){
           return ''; 
        }
        var html='<div class="jif-panel-submenu-button joe-sidebar-button joe-sidebar_'+side+'-button" ' +
            'title="toggle" onclick="getJoe('+self.joe_index+').toggleSidebar(\''+side+'\')">'+self.SVG.icon['sidebar-'+side]+'</div>';

        return html;
    }

	this.renderTextContent = function(specs){
		specs = specs || {};
		var text = specs.text || specs.object || '';
		var html = '<div class="joe-text-content">'+text+'</div>';
		return html;

	};

this.renderHTMLContent = function(specs){
		specs = specs || {};
		var html = '<textarea class="joe-rendering-field">'+(specs.rendering || '')+'</textarea>';
		return html;

	};

/*--
//LIST
--*/

    var currentListItems;
	this.Render.listContent = this.renderListContent = function(specs){
		var wBM = new Benchmarker();

        if(specs.minimode) {
            return self.renderMiniListContent(specs);
        }
            currentListItems = [];
            self.current.selectedListItems = [];
            self.current.anchorListItem = null;


		specs = specs || {};
		var schema = specs.schema;
		var list = specs.list || [];
		var html = '';
        var filteredList;




        list = cleanDeleted(list);
        
		var numItemsToRender;
        currentListItems = list;
        if(self.current.subset){
            filteredList = currentListItems = self.Subset.getCached(list);
            _bmResponse(wBM,'list subsetted');
        }
        
        //SORT LIST
        var primer = null;
        if(self.current.sorter &&(($.type(self.current.sorter) != 'array') || self.current.sorter.length) ){
            var sorterValues = self.current.sorter;
            if ($c.isArray(self.current.sorter) && self.current.sorter[0].field) {
                sorterValues = [];
                for (var i = 0, len = self.current.sorter.length; i < len; i++) {
                    sorterValues.push(self.current.sorter[i].field || self.current.sorter[i]);
                    primer = self.current.sorter[i].primer || primer || null;
                }
            }else{
                primer = (self.current.sorter[0].primer)||null;
            }

            list = list.sortBy(sorterValues,null,primer);
        }
        //list = list.sortBy(self.current.sorter);
        _bmResponse(wBM,'list sort complete');

        //FILTER LIST
        if(!$c.isEmpty(_joe.current.filters)){
            filteredList = currentListItems.where(self.Filter.generateQuery());
            currentListItems = filteredList;
            _bmResponse(wBM,'list filtered');
        }
        /*
        if(!self.current.subset){
            currentListItems = list;
		}
		else{
            filteredList = list.where(self.current.subset.filter);
            currentListItems = filteredList;
            _bmResponse(wBM,'list where complete');
		}*/
        _bmResponse(wBM,'list prerender');
        numItemsToRender = self.specs.dynamicDisplay || currentListItems.length;
        html+= self.renderListItems(currentListItems,0,numItemsToRender);

        _bmResponse(wBM,'list complete');
        self.current.filteredList = filteredList;

		return html;

	};
    function cleanDeleted(list,schema){
        var deleted_prop = self.getCascadingProp('deleted') || '_deleted';
        var query = {};
        query[deleted_prop] = {$in:[false,'false',undefined]};
        var activeList = list.where(query);
        return activeList;
    }
    this.renderMiniListContent = function(specs){
        var wBM = new Benchmarker();

        specs = specs || {};
        var schema = specs.schema;
        var list = specs.object || [];
        var idprop = specs.idprop || '_id';
        var html = '';
        var sorter = specs.sorter || 'name';
        //var click = specs.click || 'alert(\'${name}\')';
        list = list.sortBy(sorter);
        var click = 'getJoe('+self.joe_index+').minis[\''+specs.minimode+'\'].callback(\'${'+idprop+'}\')';
        var template;
        // var template = self.propAsFuncOrValue(specs.template) || '<joe-title>${name}</joe-title><joe-subtitle>${'+idprop+'}</joe-subtitle>';
        //     template ='<joe-list-item schema="'+(schema.name || schema)+'" class="joe-field-list-item" onclick="'+click+'">'+template+'</joe-list-item>';

        //TODO: lazy-render images

        for(var li = 0,tot = list.length; li<tot;li++){ 
            let item = list[li];
            let schemaName = item.itemtype || item.schema || item.name;
            template = self.propAsFuncOrValue(specs.template,item) || '<joe-title>${name}</joe-title><joe-subtext>${'+idprop+'}</joe-subtext>';
            template ='<joe-list-item schema="'+(schemaName || schema.name || schema)+'" class="joe-field-list-item clickable" onclick="'+click+'">'+template+'</joe-list-item>'
          html+= fillTemplate(template,item);
        }
        _bmResponse(wBM,'minilist complete');
        return html;

    };

    this.Render.listItems = this.renderListItems = function(items,start,stop){
        var html = '';
        var listItem;
        var items = items || currentListItems;
        var simple_sorter = [];
        self.current.sorter.map(function(s){
            simple_sorter.push(s.field || s.value || s);
        })
        items = items.sortBy(simple_sorter);
        var start = start || 0;
        var stop = stop || currentListItems.length -1;

        if(gridMode) {
            var gridspecs = self.current.schema && (self.current.schema.grid || self.current.schema.gridView)||{};
            var columns = self.propAsFuncOrValue(gridspecs.cols)||[];
            var rows = self.propAsFuncOrValue(gridspecs.rows)||[];
            var cells = [];
            var cell,colinfo,rowinfo,col_filter,row_filter,subitems;
            /*var template = self.propAsFuncOrValue(gridspecs.template) 
                || '<joe-option class=" joe-panel-content-option"><joe-title></joe-title>${name}<joe-content>${info}</joe-content></joe-option>';
            */
            for(var c = 0; c < columns.length; c++){
                colinfo = columns[c];
                col_filter = colinfo.filter;
                subitems = items.where(col_filter);
                if(rows.length){
                    //create array cells with merged filters
                }else{
                    //create array with single cell in it
                    cell_filter = colinfo.filter;
                    cell = {name:colinfo.name,filter:col_filter,html:'',items:[]};
                    subitems.map(function(i){
                        cell.items.push(i);
                        cell.html+=self.Render.gridItem(i);
                    })
                    
                    cells.push([cell]);
                }
            }
            html+='<table class="joe-grid-table"><thead>';
            columns.map(function(c){
                if($c.isString(c)) {
                    html += '<th>' + c + '</th>';
                }else if($c.isObject(c)){
                    html += '<th>' + (c.header||c.display||c.name) + '</th>';
                }
            });
            html+='</thead><tbody>';
            var row_number = 0;
            var row_count = rows.length || 1;
            // cells.map(function(cell_list){
            //     cell_list.map(function(cell){

            //     })
            // })
            while(row_number < row_count){
                html+='<tr>';
                cells.map(function(cell_list){
                    html+= '<td class="joe-grid-cell">'+cell_list[row_number].html+'</td>';
                })
                html+='</tr>';
                row_number++;
            }
            /*
            for (var i = start; i < stop; i++) {
                listItem = items[i];
                if (listItem) {
                    //html += self.renderListItem(listItem, false, i + 1);
                    html += self.renderGridItem(listItem, false, i + 1);

                }
            }*/
            html+='</tbody></table>';

            return html;
        }else if(tableMode){
            /*var tableSpecs = $.extend({cols:['name',self.getIDProp()]},
                (self.current.schema
                    &&(self.current.schema.table||self.current.schema.tableView)
                   // &&(self.current.schema.table||self.current.schema.tableView).cols
                ) ||{});*/
            html+='<table class="joe-item-table" cellspacing="0"><thead class="joe-table-head"><th>&nbsp;</th>';
            tableSpecs.cols.map(function(c){
                if($c.isString(c)) {
                    html += '<th>' + c + '</th>';
                }else if($c.isObject(c)){
                    html += '<th>' + (c.header||c.display) + '</th>';
                }
            });

            html+='</thead><tbody>';
            stop = currentListItems.length;
            for (var i = start; i < stop; i++) {
                listItem = items[i];
                if (listItem) {
                    //html += self.renderListItem(listItem, false, i + 1);
                    html += self.renderTableItem(listItem, false,i + 1);

                }
            }
            html+='</tbody></table>';

            return html;
        }
        else{
            for (var i = start; i < stop; i++) {
                listItem = items[i];
                if (listItem) {
                    html += self.renderListItem(listItem, false, i + 1);
                    //html += $GET('table') ? self.renderGridItem(listItem, false, i + 1) : self.renderListItem(listItem, false, i + 1);
                }
            }

            return html;

        }
    };

    this.onListContentScroll = function(domObj){
     // logit(domObj);
        var listItem = self.panel.find('.joe-panel-content-option').last()[0];
        var currentItemCount = self.panel.find('.joe-panel-content-option').length;
        if( currentItemCount== currentListItems.length){
           // logit('all items showing');
            return;
        }
            //$('.app-list-group-content').not('.events-group').not('.collapsed').find('.app-list-divider-count').prev('.app-list-item');
        //var listItem;
        var viewPortHeight = self.panel.find('.joe-panel-content').height();
        var html = '';
        try {
                if (listItem.getBoundingClientRect().bottom - 500 < viewPortHeight) {
                    //self.generateGroupContent(groupIndex);
                   // logit('more content coming');
                    if(gridMode){

                    }if(tableMode){

                    }else {
                        html += self.renderListItems(null, currentItemCount, currentItemCount + self.specs.dynamicDisplay);
                        self.panel.find('.joe-panel-content').append(html);
                    }
                }
        }catch(e){
            self.Error.add('error scrolling for more content: \n'+e,e,
                {caller:'self.onListContentScroll',domObj:domObj})
        }
    };

/*--
     //OBJECT
 --*/
	//TODO: Add configed listeneres via jquery not strings
    var renderFieldTo;
	this.renderObjectContent = function(specs){
        renderFieldTo = 'main';
		specs = specs || {};
		var object = specs.object;
		var fields = {main:'',left:'',right:''};
		var propObj;
		var fieldProp;
        if(!specs.minimode) {
            self.current.fields = [];
            self.current.sections = {};
        }
        if(specs.schema && typeof specs.schema == 'string'){
            specs.schema = self.schemas[specs.schema];
        }
        var schemaFields = (specs.schema)?self.propAsFuncOrValue(specs.schema.fields):false;
		if(!specs.schema || !schemaFields){//no schema use items as own schema
			for( var prop in object){
				if(object.hasOwnProperty(prop)){
					propObj = $.extend({
						name:prop,
						type:'text',
                        //type:($.type(object[prop]) == 'object')?'rendering':'text',
						value:object[prop]
					},
					self.fields[prop],
					//overwrite with value
					{value:object[prop]}
					);

					fields.main += self.renderObjectField(propObj);
				}
			}
		}
		else{
            var fhtml;
            //self.current.constructed = self.constructObjectFromFields();
			(schemaFields||[]).map(function(prop){

                //logit(renderFieldTo);
                fhtml = self.renderObjectPropFieldUI(prop,specs);
                fields[renderFieldTo]+=fhtml;
			}); //end map
            
		}

		var html ='<div class="joe-object-content">'+fields.main+'<div class="clear"></div></div>';
		return fields;
	};
    var rerenderingField = false;
    self.Fields.rerender = this.rerenderField = function(fieldname,valueObj){
        /*|{
            featured:true,
            description:'rerenders a single or list of fields, pass an optional object to overwrite values',
            tags:'rendering,field'
        }|*/
        if(!fieldname){return;}
        self.current.constructed = self.constructObjectFromFields();
        if(!self.current.constructed || $c.isEmpty(self.current.constructed)){return;}
        $.extend(self.current.constructed,(valueObj||{}));
        rerenderingField = true;
        if($c.isArray(fieldname)){
            fieldname = fieldname.join(',');
        }
        var ftype,field;
        fieldname.split(',').condense().map(function(f){
            var fields = self.renderObjectPropFieldUI(f);
            field = self.getField(f);
            ftype = self.propAsFuncOrValue(field.type,self.current.constructed).toLowerCase();
//            ftype = self.propAsFuncOrValue(field.type,self.current.constructed);
            $('.joe-object-field[data-name='+f+']').parent().replaceWith(fields);
            if(self.Fields[ftype] && self.Fields[ftype].ready){
                self.Fields[ftype].ready(self.current.constructed)
            }
        });
        rerenderingField = false;
        self.current.constructed = null;

	};
    self.Set = self.Fields.set = function(field,value){
        /*|{
            featured:true,
            description:'changes the value of a single field to the passed value',
            tags:'rendering,field,setter'
        }|*/
        if(listMode){
            logit('in list mode');
            return;
        }
        var field = self.propAsFuncOrValue(field);
        var value = self.propAsFuncOrValue(value);
        var vals = {};
        vals[field] = value;
        self.Fields.rerender(field,vals);
        return value;
    };
    self.Get = self.Fields.get = function(field){
        /*|{
            featured:true,
            description:'returns the current value of a field',
            tags:'rendering,field,getter'
        }|*/
        if(listMode){
            logit('in list mode');
            return;
        }
        var obj = self.constructObjectFromFields();
        return obj[field];
    };
	self.renderObjectPropFieldUI = function(prop,specs){
        /*|{
            featured:true,
            description:'entry point to field rendering. takes the prop to be rendered and additional spec overwrites.',
            tags:'rendering,field'
        }|*/
		//var prop = fieldname;
		var fields = '';
        if($.type(prop) == "string" && prop.contains(':')) {
            var ps = prop.split(':');
            prop = {
                name:ps[0],
                type:ps[1]
            };
            if(ps[2]){
                if(ps[2].contains('%') && parseInt(ps[2])){
                    prop.width = ps[2];
                }else{
                    prop.display = ps[2];
                }
            }
        }
        var schemaspec = specs && specs.schema || self.current.schema;
        var existingProp =
            (self.propAsFuncOrValue(schemaspec.fields)||[])
                .filter(function(s){return s.name == prop || s.extend == prop})[0];
        if(existingProp){
            prop = existingProp;
        }
        //construct a working copy of the object to use for values.
        var constructed = (self.current.constructed || self.constructObjectFromFields());

		if($.type(prop) == "string") {
			//var fieldProp = self.fields[prop] || {};

            var propObj = self.fields[prop] || self.current.fields.filter(function(f){return f.name ==prop;})[0]||{};
			var fieldProp = $.extend({},propObj);
			//merge all the items
			var propObj =extendField(prop);
			if(constructed[prop] !== undefined){

				fieldProp.value = constructed[prop]
			}
			fields += self.renderObjectField(propObj);
		}else if($.type(prop) == "object" && prop.name) {
            var fieldProp = $.extend({},prop || {});
            //merge all the items
            var propObj =extendField(prop.name);

            if(constructed[prop.name] !== undefined){

                fieldProp.value = constructed[prop.name]
            }
            fields += self.renderObjectField(propObj);
        }else if(prop && prop.extend){
            var fieldProp = $.extend({},self.fields[prop.extend] || {},prop.specs||{});
            var propObj =extendField(prop.extend);
            if(constructed[prop.extend] !== undefined){
                fieldProp.value = constructed[prop.extend]
            }
            //renderFieldTo = propObj.sidebar_side || propObj.side || 'main';
            fields += self.renderObjectField(propObj);
        }else if($.type(prop) == "object"){

            if(prop.component){
                fields += self.renderPropComponent(prop);
            }
			else if(prop.label){
				fields += self.renderContentLabel(prop);
			}
            else if(prop.section_start){
                fields += self.renderPropSectionStart(prop);
                //renderFieldTo = prop.sidebar_side || prop.side || renderFieldTo;
            }
            else if(prop.section_end){
                fields += self.renderPropSectionEnd(prop);
                //renderFieldTo = 'main';
            }else if(prop.sidebar_start){
                //fields += self.renderPropSectionStart(prop);

                
                renderFieldTo = prop.sidebar_side || prop.sidebar_start || 'main';
                if(renderFieldTo != 'main'){
                    if(prop.hasOwnProperty('collapsed')){
                        self.current.sidebars[renderFieldTo].collapsed = self.propAsFuncOrValue(prop.collapsed);
                    }
                    if(prop.hasOwnProperty('hidden')){
                        self.current.sidebars[renderFieldTo].hidden = self.propAsFuncOrValue(prop.hidden);
                        if(self.current.sidebars[renderFieldTo].hidden){
                            self.current.sidebars[renderFieldTo].collapsed = true;
                        }
                    }
                }
            }
            else if(prop.sidebar_end){
                renderFieldTo = 'main';
            }

		}

        function extendField(propname){
            var propname = propname || prop;
            var cobj = (self.current.constructed||self.current.object)
            var rerenderObj = {};
            if(rerenderingField && (fieldProp.run||fieldProp.value)){
                if(fieldProp.run){
                   // rerenderObj.value = fieldProp.run(cobj,propname)
                }else{
                    rerenderObj.value = fieldProp.value;
                }
            	// let rerenderVal = self.propAsFuncOrValue((fieldProp.run||fieldProp.value));
				// rerenderObj.value = rerenderVal;
			}
            return $.extend(
                {
                    name: propname,
                    type: 'text'
                },
                {
                    onblur: schemaspec.onblur,
                    onchange: schemaspec.onchange,
                    onkeypress: schemaspec.onkeypress,
                    onkeyup: schemaspec.onkeyup

                },
                fieldProp,
                //overwrite with value, possibly constructed
                rerenderObj,
                {value: cobj[propname]},

            );
        }

		return fields;

    };
    //PROP COMPONENT
    self.renderPropComponent = function(prop){
        var html;
        var componentName = self.propAsFuncOrValue(prop.component);
        var cobj = self.current.constructed || self.current.object;
        if(prop.specs){
            let specs = self.propAsFuncOrValue(prop.specs);
            var specHtml = Object.keys(specs).map(spec=>{
                return ` ${specs}="${specs[spec]}"`;
            })
            html =`<${componentName} item_id="${cobj._id}" ${specHtml} >${componentName}</${componentName}>`;
        }else{
            
            html =`<${componentName} item_id="${cobj._id}">${componentName}</${componentName}>`;
        }

        return html;
    };
//PROP LABELS
    self.renderContentLabel = function(specs){
        if(specs.hasOwnProperty('condition') && !_joe.propAsFuncOrValue(specs.condition)){
            return '';
        }
        if(specs.hasOwnProperty('hidden') && _joe.propAsFuncOrValue(specs.hidden)){
            return '';
        }

        var html="<div class='joe-content-label'>"+fillTemplate(specs.label,self.current.object)+"</div>";
        return html;
    };

//prop sections
    self.renderPropSectionStart = function(prop){
        var show = '';
        var hidden = false;
        if(prop.condition && !prop.condition(self.current.object)){
            show =  ' no-section ';
            hidden=true;
        }
        if(self.current.sidebars[renderFieldTo] && self.current.sidebars[renderFieldTo].hidden){
            show =  ' no-section ';
            hidden=true;
        }
        var secname = fillTemplate((prop.display||prop.section_label||prop.section_start),self.current.object);
        var secID = prop.section_start;
        if(!secname || !secID){
         return '';
        }
        var collapsed = (self.propAsFuncOrValue(prop.collapsed))?'collapsed':'';
        var toggle_action = "onclick='$(this).parent().toggleClass(\"collapsed\");'";
        var dblclick=' ondblclick="$(\'joe-submenu-section[data-section='+ secID+']\').dblclick();" ';
        var section_html = '<div class="joe-content-section '+show+' '+collapsed+'" data-section="'+secID+'">' +
            '<joe-content-section-label class="joe-content-section-label" '+toggle_action+dblclick+'>'+secname+'</joe-content-section-label>'+
            '<div class="joe-content-section-content">';
        //add to current sections
        self.current.sections[secID]={open:true,name:secname,id:secID,hidden:hidden,renderTo:renderFieldTo,anchor:prop.anchor};
        return section_html;
    };
    self.renderPropSectionEnd = function(prop){
        var secID = prop.section_end;
        var section = _getSection(secID)
        if(!secID || !(section && section.open)){
            return '';
        }
        var section_html = __clearDiv__+'</div></div>';
        section.open = false;
        return section_html;
    };

    function _getSection(secname){
        return self.current.sections[secname];
    }

/*----------------------------->
	D | Footer
<-----------------------------*/
	this.renderEditorFooter = function(specs){
        var fBM = new Benchmarker();
        var customMenu = true;
		specs = specs || this.specs || {};
		var menu = specs.minimenu || (listMode && (specs.schema && specs.schema.listmenu)||specs.listmenu) ||//list mode
		 (multiEdit && (specs.multimenu ||  (specs.schema && specs.schema.multimenu) || __defaultMultiButtons)) ||
        specs.menu || null;
        if(!menu){
            menu = __defaultObjectButtons;
            customMenu = false;
        }
		/*if(typeof menu =='function'){
			menu = menu();
		}*/
        menu = self.propAsFuncOrValue(menu);

		var html =
		'<div class="joe-panel-footer">'+
			'<div class="joe-panel-menu">';

			menu.map(function(m){
				html+= self.renderFooterMenuItem(m);

			},this);
        //add default list buttons.
			if(!specs.minimenu && self.current.list && $.type(self.current.data) == 'array'){
				html+= self.renderFooterMenuItem(__selectAllBtn__);
                html+= '<div class="joe-selection-indicator"></div>';

                //remove multiedit if menu passed.
                if(!customMenu) {
                    html += self.renderFooterMenuItem(
                        {
                            label: 'Multi-Edit',
                            name: 'multiEdit',
                            css: 'joe-multi-only',
                            action: 'getJoe(' + self.joe_index + ').editMultiple()'
                        }
                    );
                }
			}

		html+=
			__clearDiv__+
			'</div>'+
		'</div>';

        _bmResponse(fBM,'[Footer] rendered');
		return html;
	};

	this.renderFooterMenuItem=function(m){//passes a menu item

        return self.renderMenuButtons(m,'joe-footer-button');
	};



    this.renderMenuButtons = function(button,menucss){
        /*|{
         description:'function for rendering standar menu buttons',
         tags:'render, menu, button',
         category:'core'
         }|*/
        if(!button){return '';}
        var button = $.type(button) == "array"?button:[button];
        var menucss = menucss ||'';
        var display,action,html='',m;

        for(var b = 0,tot= button.length;b<tot; b++) {
            m = button[b];
            let schema = self.propAsFuncOrValue(m.schema);
            if (!m) {
                logit('error loading menu button');
                continue;
                //return '';
            }

            if (m.condition && !self.propAsFuncOrValue(m.condition)) {
                //return '';
                continue
            }
            display = fillTemplate(self.propAsFuncOrValue(m.label || m.display|| m.name), self.current.object);
            if(!m.callback) {
                action = fillTemplate(self.propAsFuncOrValue(m.action), self.current.object) || 'alert(\'' + display + '\')';
            }else{
                action = 'getJoe('+self.joe_index+').Cache.callback(\''+m.callback+'\');'
            }
            // html += '<div class="joe-button ' + menucss + ' ' + (m.css || '') + '" onclick="' + action + '" data-btnid="' + (m.id||m.name) + '" title="' + (m.title || '') + '">' + display + '</div>';

            html += `<joe-button class="joe-button  ${menucss} ${(m.css || '')}" action="${action}" 
            data-btnid="${(m.id||m.name)}" 
            ${schema && `schema="${schema}"` || ''} 
            ${m.color && `color="${m.color}"` || ''} 
            title="${(m.title || display || '')}">
            ${display}</joe-button>`;        }
        return html;
    };
/*-------------------------------------------------------------------->
	3 | OBJECT FORM
<--------------------------------------------------------------------*/
	var preProp;
	var prePropWidths = 0;
	this.renderObjectField = function(prop,mini){
        /*|{
            featured:true,
            tags:''
        }|*/
        if(prop.hasOwnProperty('condition') && !self.propAsFuncOrValue(prop.condition)){
            return '';
        }
        var joeFieldBenchmarker = new Benchmarker();
		//field requires {name,type}
        //if(!mini){
            self.current.fields.push(prop);
        //}

    //value
          prop.value = (prop.asFunction)?prop.value : self.propAsFuncOrValue(prop.value);

    //format
        if(prop.format && $.type(prop.format == 'function')){
            prop.value = prop.format(prop.value,prop,self.current.item);
        }

    //set default value
		if(prop.value == undefined && prop['default'] != undefined){
			prop.value = prop['default'];
		}
    //reset
        if(!self.current.reset[prop.name]) {
            self.current.reset[prop.name] = (typeof prop.value == "object")? $.extend({},prop.value) : prop.value;
        }
        prop.reset = self.current.reset[prop.name];// || prop.value;

    //hidden
		var hidden = '';
        var qHidden = self.propAsFuncOrValue(prop.hidden);
        if(qHidden){
            if(parseBoolean(qHidden) === undefined){
                var negated = qHidden[0] == '!';
                var hiddenPropVal = $.extend({},self.current.object,self.constructObjectFromFields())[qHidden.replace('!','')];
                hidden = negated^!!hiddenPropVal?'hidden':'';
            }else {
                hidden = 'hidden';
            }
        }
	//Locked
        prop.locked = self.propAsFuncOrValue(prop.locked);


    //required
        var required = '';
        if(self.propAsFuncOrValue(prop.required)){
            required = 'joe-required';
        }

        var html ='';

    //propdside
        var propdside = prop.sidebar_side||prop.side || renderFieldTo;
	//add clear div if the previous fields are floated.
		if(preProp){
		//TODO:deal with 50,50,50,50 four way float
			//prePropWidths
			if(preProp.width && !prop.width){
			//if((preProp.width && !prop.width)||){
				html+='<div class="clear"></div>';
			}
		}
        var containercolor = self.propAsFuncOrValue(prop.containercolor,null,null,_jco(true)[prop.name]) || '';
        if(containercolor){
            containercolor = ' background-color:'+containercolor+';';
        }
        var sizestyles=" ";
        if(prop.height){
            sizestyles+="min-height:"+prop.height+'; '
        }
		if(prop.width){
			html+='<joe-field-container class="joe-field-container joe-fleft" style="width:'+prop.width+'; '+sizestyles+
                containercolor+'" data-side="'+propdside+'">';
		}else{
            html+='<joe-field-container field="'+prop.name+'" class="joe-field-container" data-side="'+propdside+'" style="'+sizestyles+containercolor+'">';
        }

        var proptype = self.propAsFuncOrValue(prop.type,self.current.constructed);

        var fieldlabel = self.propAsFuncOrValue(prop.display||prop.label||prop.name);
        var icon = self.propAsFuncOrValue(prop.icon);
        if(icon && self.schemas[icon] && self.schemas[icon].menuicon){
            icon = self.schemas[icon].menuicon;
        }
        var hiddenlabel = (prop.label === false)?' hide-label ':''; html+=
        '<joe-field class="joe-object-field '+hidden+' '+required+' '+proptype+'-field '+hiddenlabel+'" data-type="'+proptype+'" data-name="'+prop.name+'">'
            +renderFieldAttribute('before')

            +'<label class="joe-field-label '+(icon && 'iconed' ||'')+' " title="'+prop.name+'">'
                +(icon||'')
                +fillTemplate(fieldlabel,self.current.object)
                +(required && '*' ||'')
                +self.renderFieldTooltip(prop)
            +'</label>';

            //render comment
            html+= self.renderFieldComment(prop);
            //add multi-edit checkbox
            if(self.current.userSpecs && self.current.userSpecs.multiedit){
                html+='<div class="joe-field-multiedit-toggle" onclick="$(this).parent().toggleClass(\'multi-selected\')"></div>';
            }

            html += self.selectAndRenderFieldType(prop);
            html += self.renderGotoLink(prop);
            html+= renderFieldAttribute('after');
        html+='</joe-field>';//close object field;

		//if(prop.width){
        //close field container
			html+='</joe-field-container>';
	//	}


        function renderFieldAttribute(attribute){
            var obj = (rerenderingField)?self.current.constructed:self.current.object;
            var propval = prop[attribute];
            return (propval &&
            '<joe-field-attribute class="jfa-'+attribute+'">'+fillTemplate(self.propAsFuncOrValue(propval),obj)+'</joe-field-attribute>'
            ||'');
        }
		preProp = prop;
        _bmResponse(joeFieldBenchmarker,'field '+prop.name+ ' '+prop.type);
		return html;
	};

    this.renderGotoLink = function(prop,style){
        var goto = self.propAsFuncOrValue(prop.goto);
      if(goto){
          var action='';
          if($.type(goto) == "string"){

            action = goto;
            //action = 'getJoe('+self.joe_index+').gotoItemByProp(\''+prop.name+'\',$(this).siblings().val(),\''+goto+'\');';
              action = 'getJoe('+self.joe_index+').gotoFieldItem(this,\''+goto+'\');';
          }else {
              //var values= self.getField(prop.name);

          }
          var btnHtml = '';
          switch(style||prop.type){
              case 'select':
                  btnHtml+=
                  '<div class="joe-button inline joe-view-button joe-iconed-button" onclick="' +
                    action + '" title="view '+goto+ '">view</div>';
              break;
          }
          return btnHtml;
      }else{
        return '';
      }
    };

    this.gotoFieldItem =function(dom,schema){
        var fieldname = $(dom).parents('.joe-object-field').data('name');
        var field = self.getField(fieldname);
        var idprop = field.idprop || self.getIDProp(schema);
        //var values = self.propAsFuncOrValue(field.values);
        var values = self.getFieldValues(field.values,field);
        var id = $(dom).siblings('.joe-field').val();
        var object = values.filter(function(v){
            return v[idprop] == id;
        })[0]||false;
        if(!object){
            return false;
        }
        self.show(object,{schema:schema});

    };
    this.renderFieldComment = function(prop){
        var comment = self.propAsFuncOrValue(prop.comment);
        if(!comment){return '';}
        //var comment = ($.type(prop.comment) == "function")?prop.comment():prop.comment;
        var comment_html = '<div class="joe-field-comment">'+comment+'</div>';

        return comment_html;
    };
	this.renderFieldTooltip = function(prop){
		if(!prop.tooltip){
			return '';
		}
		//var tooltip_html = '<p class="joe-tooltip">'+prop.tooltip+'</p>';
		var tooltip_html = '<span class="joe-field-tooltip" title="'+__removeTags(prop.tooltip)+'">i</span>';

		return tooltip_html;
	};

	this.selectAndRenderFieldType = function(prop){
		//var joeFieldBenchmarker = new Benchmarker();

		var html = '';
        var proptype = (self.propAsFuncOrValue(prop.type)||'').toLowerCase();
        if(!proptype){
            console.error(prop);
            self.propAsFuncOrValue(prop.type)
        }
		switch(proptype){
			case 'select':
				html+= self.renderSelectField(prop);
				break;
			case 'multisort':
			case 'multisorter':
				html+= self.renderMultisorterField(prop);
				break;
			/*case 'sorter':
				html+= self.renderSorterField(prop);
				break;*/
				/*			case 'multi-select':
				 html+= self.renderMultiSelectField(prop);*/
				//break;
			case 'guid':
				html+= self.renderGuidField(prop);
				break;
			case 'number':
				html+= self.renderNumberField(prop);
				break;
			case 'int':
				html+= self.renderIntegerField(prop);
				break;

			/*            case 'textarea':
			 prop.type = 'rendering';*/
			/*case 'code':
				html+= self.renderCodeField(prop);
				break;*/
			/*case 'rendering':
				html+= self.renderRenderingField(prop);
				break;*/

			case 'date':
				html+= self.renderDateField(prop);
				break;

			case 'boolean':
				html+= self.renderBooleanField(prop);
				break;

			case 'geo':
			case 'map':
				html += self.renderGeoField(prop);
				break;

			case 'image':
			case 'img':
				html+= self.renderImageField(prop);
				break;

			case 'buckets':
				html+= self.renderBucketsField(prop);
				break;

			case 'content':
				html+= self.renderContentField(prop);
				break;

			case 'url':
				html+= self.renderURLField(prop);
				break;

			/*case 'objectlist':
				html+= self.renderObjectListField(prop);
				break;*/

            /*case 'objectreference':
                html+= self.renderObjectReferenceField(prop);
                break;*/

			case 'tags':
				html+= self.renderTagsField(prop);
				break;
            case 'color':
                html+= self.renderColorField(prop);
                break;

            case 'group':
                html+= self.renderCheckboxGroupField(prop);
                break;

            case 'uploader':
                html+= self.renderUploaderField(prop);
                break;

            case 'preview':
                html+= self.renderPreviewField(prop);
                break;
/*            case 'wysiwyg':
                html+= self.renderTinyMCEField(prop);
                //html+= self.renderCKEditorField(prop);
                break;*/
            case 'passthrough':
                html+= self.renderPassthroughField(prop);
                break;
            case 'userfavorite':
                html+= self.renderUserFavoriteField(prop);
                break;
			default:
                if(self.Fields[proptype] && self.Fields[proptype].render){
                    html+= self.Fields[proptype].render(prop);
                }else {
                    html += self.renderTextField(prop);
                }
				break;
		}

        //_bmResponse(joeFieldBenchmarker,'Joe rendered '+(prop.name||"a field"));
		//logit('Joe rendered '+(prop.name||"a field")+' in '+joeFieldBenchmarker.stop()+' seconds');
		return html;

	};
/*----------------------------->
	0 | Event Handlers
<-----------------------------*/
	this.getActionString = function(evt,prop){
        var evt = prop[evt];
        if(!evt){ return '';}
        if($.type(evt) == "string"){
           return evt;
        }
		var str = (prop[evt])? ' '+self.functionName(prop[evt])+'(this); ' : '' ;
		return str;
	};

    function _getPropPlaceholder(prop){
        var placeholder = self.propAsFuncOrValue(prop.placeholder||prop.display||prop.name) || '';
        if(prop.placeholder === false){
            return '';
        }
        return ((placeholder && ' placeholder="'+placeholder+'"')||'');
        //return ((placeholder && 'placeholder="'+placeholder+'"')||'');
    }

	this.renderFieldAttributes = function(prop, evts){
		evts = evts ||{};

		var bluraction = '';
		//var updateaction = '';
		var changeaction = '';

		var keypressaction = '';
		var keyupaction = '';
        var rerender = (prop.rerender)?
            ' getJoe('+self.joe_index+').rerenderField(\''+prop.rerender+'\'); ':'';

        
		var profile = self.current.profile;

		var disabled = (profile.lockedFields.indexOf(prop.name) == -1)?
			'':'disabled';

		if(evts.onblur || prop.onblur){
			bluraction = 'onblur="'+(evts.onblur||'')+' '+self.getActionString('onblur',prop)+'"';
		}
		if(evts.onchange || prop.onchange || rerender){
			changeaction = 'onchange="'+rerender+(evts.onchange||'')+' '+self.getActionString('onchange',prop)+'"';
		}
		if(evts.onkeypress || prop.onkeypress){
			keypressaction = 'onkeypress="'+(evts.onkeypress||'')+' '+self.getActionString('onkeypress',prop)+'"';
		}
		if(evts.onkeyup || prop.onkeyup){
			keyupaction = 'onkeyup="'+(evts.onkeyup||'')+' '+self.getActionString('onkeyup',prop)+'"';
		}
		return ' '+keyupaction+' '+keypressaction+' '+bluraction+' '+changeaction+' '+disabled+' ';

	};

    function _disableField(prop){
        return ((prop.hasOwnProperty('locked') && self.propAsFuncOrValue(prop.locked)&&' disabled ')||'');
    }
/*----------------------------->
	A | Text Input
<-----------------------------*/
    function cleanString(value){
        return ((value || '')+'').replace(/\"/g,"&quot;");
    }
	this.renderTextField = function(prop){

        /*|{featured:true,
            tags:'field, render,text',
            description:'This renders a text field control. This is the default field type if none specified'
        }|*/
		var autocomplete;
		if(prop.autocomplete && prop.values){

            if(typeof prop.values == "function"){
                prop.values = prop.values(self.current.object);
            }
            if($.type(prop.values) == 'string' && self.getDataset(prop.values)){
                prop.values = self.getDataset(prop.values);
            }
            if($.type(prop.values) != 'array'){

                prop.values = [prop.values];
            }
            autocomplete =prop.autocomplete;
		}

		//TODO: Use jquery ui autocomplete
        var disabled = _disableField(prop);//(prop.locked &&'disabled')||'';
        var fieldtype = (prop.ftype && "data-ftype='"+prop.ftype+"'")||'';
        var placeholder = _getPropPlaceholder(prop);
        
        var ac_function = `document.querySelector('joe-autocomplete[field=\'${prop.name}\']').search(this.val)`;
		var html=
            /*((autocomplete && '<div class="joe-text-autocomplete-label"></div>')||'')+*/
		'<input class="joe-text-field joe-field '+((prop.skip && 'skip-prop')||'')+'" ' +
            placeholder+ //add placeholder
            'type="text"  '+disabled+' name="'+prop.name+'" value="'+cleanString(prop.value || '')+'" maxlength="'+(prop.maxlength || prop.max||'')+'" '
			+self.renderFieldAttributes(prop)
			+((autocomplete &&
				//' onblur="getJoe('+self.joe_index+').hideTextFieldAutoComplete($(this));"'
				//' onkeyup="getJoe('+self.joe_index+').showTextFieldAutoComplete($(this));"'
                ` onkeyup='this.nextSibling.search(this.value)'`
                )
			||''
			)
		+' '+fieldtype+'/>';

		if(autocomplete){
			/*html+='<div class="joe-text-autocomplete">';
            var ac_opt;
            var ac_template = autocomplete.template||'${name}';
            var ac_id, ac_title;
			for(var v = 0, len = prop.values.length; v < len; v++){
                ac_opt = ($.type(prop.values[v]) == "object")?
                    prop.values[v]:
                    {id:prop.values[v],name:prop.values[v]};
                ac_title = fillTemplate(self.propAsFuncOrValue(ac_template,ac_opt,null,self.current.object),ac_opt);
                ac_id = (autocomplete.value && fillTemplate(self.propAsFuncOrValue(autocomplete.value,ac_opt),ac_opt))
                    ||(autocomplete.idprop && ac_opt[autocomplete.idprop])
                    ||ac_opt._id||ac_opt.id||ac_opt.name;
				html+='<div class="joe-text-autocomplete-option" '
					+'onclick="getJoe('+self.joe_index+').autocompleteTextFieldOptionClick(this);" '
                    +'data-value="'+ac_id+'">'+ac_title+'</div>';
			}

            html+='</div>';*/
            html+= `<joe-autocomplete class="joe-text-autocomplete" 
                field="${prop.name}" 
                itemId="${self.current.object._id}" 
                schema="${self.current.object.itemType || (self.current.schema && self.current.schema.name) || ''}" 
                joe-index=${self.joe_index}
                ">
            </joe-autocomplete>`;
		}
		//add onblur: hide panel

		return html;
	};
	var textFieldAutocompleteHandler = function(e){
        var dom = e.target;
        //if($(e.target).parents('.joe-text-autocomplete'));
        $('.joe-text-autocomplete').removeClass('active');
        $('body').unbind( "click", textFieldAutocompleteHandler );
    };

	this.showTextFieldAutoComplete = function(dom){
        $('body').unbind( "click", textFieldAutocompleteHandler ).bind( "click", textFieldAutocompleteHandler );

        var autocomplete = dom.next('.joe-text-autocomplete');
        var content,show
            needles = dom.val().toLowerCase().replace( /,/,' ').split(' ');
            //needles.removeAll('');
		autocomplete.find('.joe-text-autocomplete-option').each(function(i,obj){
            content = (obj.textContent===undefined) ? obj.innerText : obj.textContent;
            //content = obj.innerHTML;
			//self.checkAutocompleteValue(dom.val().toLowerCase(),content,obj);
            show = self.checkAutocompleteValue(needles,content.replace( /,/,' ').toLowerCase(),obj);
            $(obj).toggleClass('visible',show);

		});
		autocomplete.addClass('active');
	};
	this.hideTextFieldAutoComplete = function(dom){
		var autocomplete = dom.next('.joe-text-autocomplete');
		autocomplete.removeClass('active');
	};

/*	this.autocompleteTextFieldOptionClick = function(dom){
        var value = ($(dom).data('value'));
		$(dom).parent().prev('.joe-text-field').val(value);
		$(dom).parent().removeClass('active');
        $(dom).parent().siblings('.joe-reference-add-button').click();
		//$(dom).previous('.joe-text-field').val($(dom).html());
    };*/
    this.autocompleteTextFieldOptionClick = function(dom){
        var value = ($(dom).data('value'));
        var parent = $(dom).parents('joe-autocomplete');
		parent.prev('.joe-text-field').val(value);
		parent.removeClass('active');
        parent.siblings('.joe-reference-add-button').click();
		//$(dom).previous('.joe-text-field').val($(dom).html());
	};

	this.checkAutocompleteValue = function(needles,haystack,dom,additive){
		var d = $(dom);
        //var needles = needle.replace( /,/,' ').split(' ');
        if(!needles.length){
            return true;
            //d.addClass('visible');
        }
        for(var n = 0, tot = needles.length; n<tot; n++){
          if(haystack.indexOf(needles[n]) == -1){//needle not in haystack
            return false;
          }
            //d.removeClass('visible');
        }
        return true;
        //d.addClass('visible');
/*        var overlap = needles.filter(function(n) {
            return haystacks.indexOf(n) != -1
        });
        if(overlap.length || !needle){
            d.addClass('visible');
        }else{
            d.removeClass('visible');
        }*/
       /* if(haystack.toLowerCase().indexOf(needle) != -1 || !needle){
			d.addClass('visible');
		}else{
			d.removeClass('visible');
		}*/
	};

    encapsulateFieldType('text',self.renderTextField);
/*----------------------------->
	A.2 | Password
<-----------------------------*/
	this.renderPasswordField = function(prop){
        /*|{
        description:'forces a float value',
         tags:'render,field,password'
         }|*/

    var disabled = _disableField(prop);
		var html=
		'<input class="joe-password-field joe-field" type="password" '+disabled+' name="'+prop.name+'" value="'+(prop.value || '')+'"  '+
			self.renderFieldAttributes(prop)+
		' />';
		return html;
	};

    encapsulateFieldType('password',self.renderPasswordField);
/*----------------------------->
	B | Number/Int Input
<-----------------------------*/
	this.renderNumberField = function(prop){
        /*|{
        description:'forces a float value',
        params:{step:'how many decimals, use any'},
         tags:'render,field,number'
         }|*/

        var step= prop.step || "any";
        var disabled = _disableField(prop); 
        var placeholder = _getPropPlaceholder(prop);
		var html=
        '<input class="joe-number-field joe-field" type="number" '+disabled+' '+
        placeholder+' step="'+step+'" name="'+prop.name+'" value="'+(prop.value || '')+'"  '+
			self.renderFieldAttributes(prop,{onblur:'getJoe('+self.joe_index+').returnNumber(this);'})+
		' />';
		return html;
	};

	this.returnNumber = function(dom){
		if(!$(dom).val()){return;}
		$(dom).val(parseFloat($(dom).val()));
	};
    encapsulateFieldType('number',self.renderNumberField);
/*----------------------------->
 B.2
 <-----------------------------*/
	this.renderIntegerField = function(prop){
        /*|{
         tags:'render,field,integer'
         }|*/
		/*var disabled = (profile.lockedFields.indexOf(prop.name) != -1 || prop.locked)?
			'disabled':'';
	*/
	//bluraction
		//var bluraction = 'onblur="_joe.returnInt(this); '+self.getActionString('onblur',prop)+'"';
        var placeholder = _getPropPlaceholder(prop);
		var html=
		'<input class="joe-int-field joe-field" type="text" '+placeholder+' name="'+prop.name+'" value="'+(prop.value || '')+'"  '+
			self.renderFieldAttributes(prop,{onblur:'getJoe('+self.joe_index+').returnInt(this);'})+
		' />';
		return html;
	};

	this.returnInt = function(dom){
		if(!$(dom).val()){return;}
		$(dom).val(parseInt($(dom).val()));

	};
    encapsulateFieldType('integer',self.renderIntegerField);
/*----------------------------->
	C | Select
<-----------------------------*/
	this.renderSelectField = function(prop,subObject){
        /*|{
         tags:'render,field,select'
         }|*/
        var disabled = _disableField(prop);
        var subObject = subObject || {};
        //(prop.hasOwnProperty(prop.locked) && self.propAsFuncOrValue(prop.locked) &&'disabled')||'';
		/*var values = ($.type(prop.values) == 'function')?prop.values(self.current.object):prop.values || [prop.value];*/
        var values = self.getFieldValues(prop.values,prop);
        var valObjs = [];
        if(prop.blank){
            switch($.type(prop.blank)){
                case 'string':
                    valObjs.push({name:(prop.blank||'')});
                    break;
                case 'object':
                    valObjs.push({name:(prop.blank.name||''),value:(prop.blank.value||'')});
                    break;
                default:
                    valObjs.push({name:''});
                    break;
            }
        }

        values.map(function(v){
            if($.type(v) != 'object'){
                valObjs.push({name:v});
            }else{
                valObjs.push(v);
            }
        });
/*		if($.type(values[0]) != 'object'){
			values.map(function(v){
				valObjs.push({name:v});
			});
		}
		else{
			valObjs = values;
		}*/


        var mul = prop.multiple || prop.allowMultiple;
		var selected;
		var multiple =(mul)?' multiple ':'';
		var selectSize = prop.size || ((valObjs.length*.5) > 10)? 10 : valObjs.length/2;

		if(!prop.size && !mul){
			selectSize = 1;
		}
        var style='style="';
        if(prop.minwidth){
            style+='min-width:'+self.propAsFuncOrValue(prop.minwidth);
        }
        style+='"';
		var html=
		'<select '+disabled+' class="joe-select-field joe-field" name="'+prop.name+'" '+style+' value="'+(prop.value || '')+'" size="'+selectSize+'"'+
			self.renderFieldAttributes(prop)+
			multiple+
		' >';


        var template;// = self.propAsFuncOrValue(prop.template) || '';
		var val;
        var optionVal;
        var propertyValue;
        var prop_idprop;
			valObjs.map(function(v){
                propertyValue = subObject[prop.name]||self.propAsFuncOrValue(prop.value,null,null,v);
                template = self.propAsFuncOrValue(prop.template,null,null,v) || '';
                optionVal = (template)?fillTemplate(template,v):(v.display||v.label||v.name);
                prop_idprop = (prop.idprop && self.propAsFuncOrValue(prop.idprop,v,null,_jco())) || null;
                val = (prop_idprop && v.hasOwnProperty(prop_idprop))?v[prop_idprop]:v._id||v.value||v.name||'';
				if($.type(propertyValue) == 'array'){
					selected = '';
					selected = (prop.value.indexOf(val) != -1)?'selected':'';

					/*prop.value.map(function(pval){
						if(pval.indexOf)
					});*/
				}else{
					selected = (propertyValue == val)?'selected':'';
				}
				html += '<option value="'+val+'" '
                    +selected
                    +(self.propAsFuncOrValue(v.disabled)&&'disabled="disabled"'||'')+'>'+optionVal+'</option>';
			});

		html+='</select>';
		return html;
	};
    encapsulateFieldType('select',self.renderSelectField);
/*----------------------------->
	D | Date Field
<-----------------------------*/

	this.renderDateField = function(prop,subObject){
        /*|{
         tags:'render,field,date'
         }|*/
         if(prop.native){
            var subObject = subObject || {};
            var placeholder = _getPropPlaceholder(prop);
            var dateVal = _joe.Utils.toDateString(new Date(subObject[prop.name] || prop.value || ''),'Y-m-d')
           // $c.format(new Date(subObject[prop.name] || prop.value || ''),'Y-m-d');;
           var html=
           '<input class="joe-date-field joe-field" type="date"  '+placeholder+' '+_disableField(prop)+' name="'+prop.name+'" value="'+dateVal+'" '+
               self.renderFieldAttributes(prop)+
           ' required pattern="\d{2}/\d{2}/\d{4}" />';
           return html;
         }else{
            var subObject = subObject || {};
            var placeholder = _getPropPlaceholder(prop);
            var html=
            '<input class="joe-date-field joe-field jquery" type="text"  '+placeholder+' '+_disableField(prop)+' name="'+prop.name+'" value="'+(subObject[prop.name] || prop.value || '')+'" '+
                self.renderFieldAttributes(prop)+
            ' />';
            html+= '<joe-datepicker-holder id="'+prop.name+'"></joe-datepicker-holder>';
         }
		return html;
	};
    this.readyDateField = function(prop){
        var inObjectList;
        var field;
        self.overlay.find('.joe-date-field.jquery').each(function(i,dom){
            field = _joe.getField(dom.name);
            if($(dom).parents('.joe-objectlist-row').length){
                $(dom).Zebra_DatePicker({ // in objectlist-row
                    //always_visible:$('joe-datepicker-holder#'+$(dom).attr('name')),
                    //offset:[5,20],
                    format:'m/d/Y',
                    first_day_of_week:0,
                    readonly_element:false,
                    //open_on_focus:true,
                   start_date:(new Date()).format('m/d/y')
                    
                    //container:$('joe-datepicker-holder#'+$(dom).attr('name'))
                });
            }else{//not in objectlist
                $(dom).Zebra_DatePicker({
                    always_visible:(self.propAsFuncOrValue(field.always_visible) && $('joe-datepicker-holder#'+$(dom).attr('name')) ||false),
                    /*offset:[5,20],*/
                    format:'m/d/Y',
                    first_day_of_week:0,
                    readonly_element:false,
                    //open_on_focus:true,
                    container:$('joe-datepicker-holder#'+$(dom).attr('name'))
                });
            }
        });

    };
    encapsulateFieldType('date',self.renderDateField,{ready:self.readyDateField});
  /*----------------------------->
	D.2 | Time Field
<-----------------------------*/
/*	this.renderTimeField = function(prop){

        var html=
		'<input class="joe-time-field joe-field" type="time"  '+_disableField(prop)+' name="'+prop.name+'" value="'+(prop.value || '')+'" '+
			self.renderFieldAttributes(prop)+
		' />';

		return html;
	};*/
    this.renderTimeField = function(prop){
        /*|{
         tags:'render,field,time',
         specs:''
         }|*/
        var html=
		'<input class="joe-time-field joe-field" type="text" '+_disableField(prop)+' name="'+prop.name+'" value="'+(prop.value || '')+'" '+
			self.renderFieldAttributes(prop)+
		' />';

		return html;
	};
    this.readyTimeField = function(prop){
        var inObjectList;
        var field;
        self.overlay.find('.joe-time-field').each(function(i,dom){
            
            field = _joe.getField(dom.name);
            $(dom).timepicker({timeFormat: 'h:mm p',
                minTime:field.minTime,
                maxTime:field.maxTime,
                startTime:field.startTime,
                endTime:field.endTime,
                dropdown:false});
            
        });
    };
    encapsulateFieldType('time',self.renderTimeField,{ready:self.readyTimeField});
      
/*----------------------------->
	E | Geo Field
<-----------------------------*/
	this.renderGeoField = function(prop){
        /*|{
            tags:'render,field,geo',
            specs:'center:[lat,long],hideAttribution:bool,geometry:"point/polygon"'
        }|*/
        var propLocation;
        try{
            propLocation =(prop.value && eval(prop.value));
        }catch(e){
            self.Error.add('error finding geo for '+(prop.name||''),e,prop.value);
        }
		
		var zoom = prop.zoom || 4;
	//map.setView([37.85750715625203,-96.15234375],3)
	//var map = L.map('map').setView(center, zoom);
	var mapDiv = 'joeGEO_'+prop.name;
    var geoval = tryEval(prop.value);//
	var val = prop.value||'';
    var geometrytype = prop.geometry || 'point';
    if(geoval && geoval[0] && $.type(geoval[0]) == "array"){
        var center =  prop.center || [40.513,-96.020];
        geometrytype = "polygon";
    }else{
        var center =  propLocation || prop.center || [40.513,-96.020];
    }
    var geofield = (geometrytype =="polygon")?
    '<textarea placeholder="enter geometry here" onchange="_joe.addMapPolygon(\''+mapDiv+'\',this.value)" class="joe-geo-field joe-field"  name="'+prop.name+'" '+_disableField(prop)+'>'+val.replace(/\n/g,'')+'</textarea>'
    :'<input class="joe-geo-field joe-field" type="text" value="'+val+'" name="'+prop.name+'" '+_disableField(prop)+'/>';
    
	var html=
		'<div class="joe-geo-map joe-field" name="'+prop.name+'" id="'+mapDiv+'" '
			+'data-center="'+JSON.stringify(center)+'" data-zoom="'+zoom+'" '
			+'data-value="'+val+'" '
            +'data-geometrytype="'+geometrytype+'"'
			+'data-hideattribution="'+(prop.hideAttribution||'')+'" '
			+'onload="getJoe('+self.joe_index+').initGeoMap(this);"></div>'
		    +geofield
		+'<script type="text/javascript">setTimeout(function(){getJoe('+self.joe_index+').initGeoMap("'+mapDiv+'");},100)</script>'
		;

		return html;
	};

	this.initGeoMap = function(id){

		var mapspecs = $('#'+id).data();
        var fieldname = $('#'+id).attr('name');
        self.current.cache.leaflet = self.current.cache.leaflet || {}
		var map = self.current.cache.leaflet[id] = L.map(id).setView(mapspecs.center,mapspecs.zoom);
		//var map = L.map(id).setView([51.505, -0.09], 13);
		L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
   			//attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
		}).addTo(map);

	//add geocoder
        if(mapspecs.geometrytype == 'point'){
            var searchControl = new L.esri.Controls.Geosearch().addTo(map);
            searchControl.on("results", function(data) {
                
                var latlng = data.latlng
                var map = self.current.cache.leaflet[$(data.target._input)
                    .parents('.joe-geo-map').attr('id')];
                if(map.marker){
                    map.marker.setLatLng(latlng);
                }else{
                    self.addMapIcon(map,latlng)
                }
                $('input[name='+map.prop+']').val('['+latlng.lat+','+latlng.lng+']');

            });
            map.on('click', self.onMapClick);
            if(mapspecs.value){
                self.addMapIcon(map,mapspecs.value);
            }
        }else if (mapspecs.geometrytype == 'polygon'){
            var geoval = tryEval(mapspecs.value);
            self.addMapPolygon(map,geoval);
        }
	//hide leaflet and esri attribution
		if(mapspecs.hideattribution){
			$('.leaflet-control-attribution').hide();
		}

        map.prop = $('#'+id).attr('name');

	};

	this.onMapClick = function(e,s,d){
		var map = (e.type=="click")?e.target : e.target.map || self.current.leaflet;

		//map.setView(e.latlng);
		var ll = (e && e.latlng && (e.latlng.latlng||e.latlng)) || this.getLatLng();
		map.setView(ll);

		if(e.type=="dragend"){

		}else if(map.marker){
			map.marker.setLatLng(ll);
		}else{
			self.addMapIcon(map,ll);
		}
		$('input[name='+map.prop+']').val('['+ll.lat+','+ll.lng+']');
	};

	this.addMapIcon = function(map,latlng,specs){
        try{
            if(tryEval(latlng) === null){
                throw('could not eval'+latlng);
            }
            specs = specs || {};
            var myIcon = L.icon({
                iconUrl: specs.icon||'/JsonObjectEditor/img/mapstar.png',
                iconSize: [30, 30]
                //iconAnchor: [22, 94],
                //popupAnchor: [-3, -76],
                //shadowUrl: 'my-icon-shadow.png',
                //shadowRetinaUrl: 'my-icon-shadow@2x.png',
                //shadowSize: [68, 95],
                //shadowAnchor: [22, 94]
            });
            map.marker = L.marker(latlng,{
                draggable:true,
                icon:myIcon
            }).addTo(map);
            map.marker.map = map;
            map.marker.on('dragend', self.onMapClick);
        }catch(e){
            self.Error.add('error adding point to map',e,{latlng:latlng})
            if(map.marker){map.removeLayer(map.marker);}
        }
	};
    this.addMapPolygon = function(map,geometry,specs){
        var map = self.getMapFieldMap(map);
        try{
            geometry = tryEval(geometry);
           /* if(tryEval(geometry) === null){
                throw('could not eval'+geometry);
            }*/
            //eval('('+geometry+')');
            specs = specs || {};
            var myPoly = L.polygon(geometry);
            map.polygon = myPoly.addTo(map);
            map.polygon.map = map;
            map.fitBounds(myPoly.getBounds());
            $('textarea[name='+map.prop+']')
            .val($('textarea[name='+map.prop+']').val().replace(/\n/g,''))
        }catch(e){
            self.Error.add('error adding polygon to map',e,{geometry:geometry})
            if(map.polygon){map.removeLayer(map.polygon);}
        }
	};
    this.getMapFieldMap = function(id){
        
        if(typeof(id) == "object"){
            return id;
        }
        if(id.indexOf('joeGEO_') == -1){
            id ='joeGEO_'+id;
        }
        return self.current.cache.leaflet[id];
    }
    encapsulateFieldType('geo',self.renderGeoField,{ready:self.initGeoMap,addPoly:this.addMapPolygon});
/*----------------------------->
	F | Boolean
<-----------------------------*/
	this.renderBooleanField = function(prop){
        /*|{
            tags:'render,field,boolean'
        }|*/
		var profile = self.current.profile;

		var html=
            '<label class="joe-boolean-label" for="joe_checkbox-'+prop.name+'">'
		+'<input class="joe-boolean-field joe-field" type="checkbox" name="'+prop.name+'" id="joe_checkbox-'+prop.name+'" '
			+([true,'true'].contains(prop.value)&&'checked' || '')
			+self.renderFieldAttributes(prop)
		+' '+_disableField(prop)+'/> <small>'
            +(prop.label ||'') +'</small></label>';
		return html;
	};
    encapsulateFieldType('guid',self.renderGuidField);
/*----------------------------->
 F.2 | UserFavorite
 <-----------------------------*/
 this.renderUserFavoriteField = function(prop){
    var html= 'user favorite';
    return html;
};
/*----------------------------->
	G | Guid
<-----------------------------*/
	this.renderGuidField = function(prop){
		var profile = self.current.profile;
        var placeholder = _getPropPlaceholder(prop);
        
		var html=
		'<input class="joe-guid-field joe-field" type="text" '+placeholder+' name="'+prop.name+'" value="'+(prop.value || cuid())+'"  disabled />';
		return html;
	};
    encapsulateFieldType('guid',self.renderGuidField);
/*----------------------------->
	H | Create
<-----------------------------*/
    this.renderCreateField = function(prop){
        var schema = self.schemas[prop.schema] || prop.schema;
        var schemaTitle = '';
        if(schema && schema.name){
            schemaTitle = schema.name.toUpperCase();
        }else{
            return 'schema not found'
        }
        var action = '_joe.Fields.create.go(\''+prop.name+'\')';

        html = `<joe-button schema="${prop.schema}" icon="plus" color="orangegrey" action="_joe.Fields.create.go('${prop.name}')" style="float:none">
        new <b>${schemaTitle}</b>
        </joe-button>`;

        return html;
    };
    this.gotoCreatedItem = function(propname){
        var prop = self.getField(propname);
        if(!prop.schema){
            return 'no schema selected';
        }
		if(self.isNewItem()){
            if(prop.autosave){
                self.updateObject();
            }else{
                alert('please save this item before creating another');
                return;
            }
        }
        var schema = self.schemas[prop.schema] || prop.schema;
        var overwrites = self.propAsFuncOrValue(prop.new || prop.overwrites) || {};
        var newitem = _joe.propAsFuncOrValue(schema.new);
        var item = $.extend({},newitem,overwrites);
        goJoe(item,{schema:schema});
    };
    encapsulateFieldType('create',self.renderCreateField,{go:self.gotoCreatedItem});
/*----------------------------->
	I | Image
<-----------------------------*/
	this.renderImageField = function(prop){
        /*|{
            tags:'render,field,image',
            specs:'hidefield'
        }|*/
        var hidefield = self.propAsFuncOrValue(prop.hidefield);
		var html='';
		if(!hidefield){
            html +='<input class="joe-image-field joe-field" type="text" name="'+prop.name+'" value="'+(prop.value || '')+'" '
		+	self.renderFieldAttributes(prop)
		+' onkeyup="_joe.updateImageFieldImage(this);" '+_disableField(prop)+'/>';}
		html +='<img class="joe-image-field-image" src="'+(prop.value||'')+'" />'
		+'<span class="joe-image-field-size"></span>';
           // +'<script>_joe.updateImageFieldImage($(\'input[name=thumbnail]\'),2000)</script>';

		return html;
	};

	this.updateImageFieldImage = function(dom,timeout){
        /*if($(dom).is('img')) {//img
            var src = $(dom).parent().find('input.joe-image-field').val();
            //var img = $(dom).next('.joe-image-field-image');
            var img = $(dom).parent().find('.joe-image-field-image');
            img.attr('src', src);
        }else{//input*/
        logit('img update');
            var src = $(dom).val();
            //var img = $(dom).next('.joe-image-field-image');
            var img = $(dom).parent().find('.joe-image-field-image');
            self.updateImageSize(img);
            img.attr('src', src);
     //   }
	};

    this.updateImageSize = function(img) {
        var jqImg = $(img);
        //if (!jqImg.hasClass('loaded')) {
            jqImg.siblings('.joe-image-field-size').html(jqImg.width() + 'w x ' + jqImg.height() + 'h');

            //jqImg.addClass('loaded');
       // }

    };
    encapsulateFieldType('image',self.renderImageField);
/*----------------------------->
	J | Multisorter
<-----------------------------*/
	this.renderMultisorterField = function(prop){

		//var values = ($.type(prop.values) == 'function')?prop.values(self.current.object):prop.values||[];
        var values = self.getFieldValues(prop.values,prop);
		var valObjs = [];


	//sort values into selected or option
		var val;
		var optionsHtml ='';
		var selectionsHtml ='';


		var idprop = prop['idprop'] ||'id'||'_id';
		var template = prop.template || '${name} (${'+idprop+'})';
		var value = prop.value || [];
		var selectionsArray = Array(value.length);
		var li;


		function renderMultisorterOption(v){
			var html = '<li data-id="'+v[idprop]+'" ondblclick="_joe.toggleMultisorterBin(this);">'+fillTemplate(template,v)+'</li>';
			return html;
		}

	//render selected list
	/*	values.map(function(v){
		//li = renderMultisorterOption(v);//'<li data-id="'+v[idprop]+'" onclick="_joe.toggleMultisorterBin(this);">'+fillTemplate(template,v)+'</li>';
		selectionsHtml += renderMultisorterOption(v);
		});*/

		var val_index;
	//render options list
		if(!values){
			values = [];
			//todo: do some error reporting here.
		}
		values.map(function(v){
			li = renderMultisorterOption(v);//'<li data-id="'+v[idprop]+'" onclick="_joe.toggleMultisorterBin(this);">'+fillTemplate(template,v)+'</li>';
			val_index = value.indexOf(v[idprop]);
			if(val_index != -1){//currently selected
				//selectionsHtml += li;
				selectionsArray[val_index] = li;

			}else{
				optionsHtml += li;
			}
		});

		var selectionsHtml = selectionsArray.join('');
		var height = (prop.height && "style='max-height:"+prop.height+";'")||'';
		var html=
		'<div class="joe-multisorter-field joe-field"  name="'+prop.name+'" data-ftype="multisorter" data-multiple="'+(prop.allowMultiple||'false')+'">'+

			'<div class="joe-filter-field-holder"><input type="text"class="" onkeyup="_joe.filterSorterOptions(this);"/></div>'+
			'<p class="joe-tooltip"> double click or drag item to switch columns.</p>'+
			'<ul class="joe-multisorter-bin options-bin" '+height+'>'+optionsHtml+'</ul>'+
			'<ul class="joe-multisorter-bin selections-bin" '+height+'>'+selectionsHtml+'</ul>'+
			__clearDiv__

		+'</div>';
		return html;
	};
	this.filterSorterOptions = function(dom){
        var sorterBM = new Benchmarker();
		var query = $(dom).val().toLowerCase();
		//$(dom).parent().next('.joe-multisorter-bin').find('li').each(function(){$(this).toggle($(this).html().toLowerCase().indexOf(query) != -1 );});
        $(dom).parents('.joe-field').find('.joe-multisorter-bin.options-bin').find('li').each(function(){$(this).toggle($(this).html().toLowerCase().indexOf(query) != -1 );});
        _bmResponse(sorterBM,'found results for: '+query);

	};
	this.toggleMultisorterBin = function(dom) {
        var id = $(dom).data('id');
        var parent = $(dom).parents('.joe-multisorter-bin');
        var multisorter = parent.parents('.joe-multisorter-field');
        var target = parent.siblings('.joe-multisorter-bin');

        var newDom = parent.find('li[data-id=' + id + ']').detach();

    //detach if no multiples allowed.
       /* if (!multisorter.data('multiple')) {
            newDom
        }*/

		target.prepend(newDom);



	};

    encapsulateFieldType('multisorter',self.renderMultisorterField);
/*----------------------------->
	K | Buckets
<-----------------------------*/
	//TODO: progressively render bucket options.
	this.renderBucketsField = function(prop){
        /*|{
        description:'renders buckets field',
        tags:'buckets,field,render',
        specs:['idprop','values','allowMultiple','template','bucketCount','bucketNames','bucketWidth']
         }|*/

		/*var values = ($.type(prop.values) == 'function')?prop.values(self.current.object):prop.values||[];*/
        var values = self.getFieldValues(prop.values,prop);
		var valObjs = [];
        var bucketNames = self.propAsFuncOrValue(prop.bucketNames) || [];


	//sort values into selected or option
		var val;
		var bucketCount = self.propAsFuncOrValue(prop.bucketCount) || 3;
		//(typeof prop.bucketCount =="function")?prop.bucketCount():prop.bucketCount || 3;
		var optionsHtml ='';
		var bucketsHtml =[];
		var value = prop.value || [[],[],[]];

	//add arrays to values
		for(var i = 0; i < bucketCount; i++){
			bucketsHtml.push('');
			if(!value[i]){
				value.push([]);
			}
		}

		var idprop = prop.idprop ||'_id'|| 'id';
		var template = prop.template || '${name} <br/><small>(${'+idprop+'})</small>';


		var lihtml;
		var selected;

    //populate selected buckets
		var foundItem;
		var bucketItem;
        var itemCss='joe-bucket-delete cui-block joe-icon';
		var selectedIDs=[];
        for(var i = 0; i < bucketCount; i++){
            for(var li = 0; li < value[i].length; li++){
                bucketItem = value[i][li];
                if($.type(bucketItem) == "string"){
                    //find object in values
                    foundItem = values.filter(function(v){
                            return v[idprop] == bucketItem;
                        })[0]||false;
                    if(!foundItem){
                        foundItem = {};
                        foundItem[idprop] = li;
                    }
                    selectedIDs.push(bucketItem);
                    lihtml = self.renderBucketItem(foundItem,prop,false);
                        //'<li data-value="'+foundItem[idprop]+'" >'+fillTemplate(template,foundItem)+'<div class="'+itemCss+'" onclick="$(this).parent().remove()"></div></li>';
                }else if($.type(bucketItem) == "object"){
                    foundItem = bucketItem;
                    lihtml = self.renderBucketItem(foundItem,prop,true);

                    //'<li data-isobject=true data-value="'+encodeURI(JSON.stringify(bucketItem))+'">'
                       // +fillTemplate(template,bucketItem)
                    //+'<div class="'+itemCss+'" onclick="$(this).parent().remove()"></div></li>';
                }
                bucketsHtml[i] += lihtml;
            }
        }

    //populate options list
        var optionTemplate  = prop.optionTemplate || template;
        values.map(function(v){
            lihtml = //self.renderBucketItem(foundItem,template,idprop,false);
            '<li data-value="'+v[idprop]+'" >'+fillTemplate(optionTemplate,v)+'<div class="joe-bucket-delete cui-block joe-icon" onclick="$(this).parent().remove()"></div></li>';
            selected = false;
        //loop over buckets
            /*if(!prop.allowMultiple){
                for(var i = 0; i < bucketCount; i++){
                    if(value[i].indexOf(v[idprop]) != -1){//currently selected
                        bucketsHtml[i] += lihtml;
                    }
                }
            }*/
            if(selectedIDs.indexOf(v[idprop]) ==-1 || prop.allowMultiple){
                optionsHtml += lihtml;
            }
        });

    function renderBucket(id,name,width){
        var widthStr = (width)?'style="width:'+width+'"':'';
        var nameStr = (name)?'<div class="joe-bucket-label">'+name+'</div>':'';
        return '<ul class="joe-buckets-bin selections-bin" '+widthStr+'>'+nameStr+bucketsHtml[id]+'</ul>';
    }

    //renderHTML
		var html=
		'<div class="joe-buckets-field joe-field" name="'+prop.name+'" data-ftype="buckets">'
			+'<div class="joe-filter-field-holder"><input type="text"class="" onkeyup="_joe.filterBucketOptions(this);"/></div>'
			+'<div class="joe-buckets-field-holder" style="width:25%;">'
				+'<ul class="joe-buckets-bin options-bin '+(prop.allowMultiple && 'allow-multiple' || '')+'">'+optionsHtml+'</ul>'
			+'</div>'
			+'<div class="joe-buckets-field-holder" style="width:75%;">';
				bucketsHtml.map(function(b,i){
					html+=renderBucket(i,(bucketNames[i]||''),prop.bucketWidth);
				});
				//+'<ul class="joe-buckets-bin selections-bin">'+bucketsHtml+'</ul>'


		html+=	__clearDiv__
			+'</div>'//end buckets field holder
			+__clearDiv__
		+'</div>';


		return html;
	};
	this.filterBucketOptions = function(dom){
		var query = $(dom).val().toLowerCase();
		$(dom).parents('.joe-buckets-field').find('.options-bin').find('li').each(function(){$(this).toggle($(this).html().toLowerCase().indexOf(query) != -1 );});
		logit(query);

	};
    this.readyBinFields = function(){
        try {
            self.overlay.find('.joe-multisorter-bin').sortable({
                connectWith: '.joe-multisorter-bin',
                placeholder: "joe-sortable-highlight",
                items: "li"
            });
            self.overlay.find('.joe-buckets-bin').sortable({
                connectWith: '.joe-buckets-bin',
                placeholder: "joe-sortable-highlight",
                items: "li",
                start: function (event, ui) {
                    sortable_index = ui.item.index();
                    ui.item.parents();
                },
                update: function (event, ui) {
                    if (ui.sender && ui.sender.hasClass('options-bin') && ui.sender.hasClass('allow-multiple')) {
                        //add the element back into the list.
                        ui.sender.find('li').eq(sortable_index).before(ui.item.clone());
                        //ui.item.parents('.joe-buckets-bin');
                    }

                }
            });
        }catch(e){
            //logit('Error creating sortables:\n'+e);
            self.Error.add('Error creating sortables:\n'+e,e,
                {caller:'self.readyBinFields'})
        }
    };
    this.renderBucketItem = function(item,fieldnameorobject,specs){
        /*|{
         description:'renders a single bucket item, either from an id string or a passed object. set specs.isobject to true to use an object. pass the fielname as second parameter.',
         tags:'buckets,item,render',
         specs:['isobject','idprop','template']
         }|*/
        var fieldObj = ($.type(fieldnameorobject) == "object")?
            fieldnameorobject:
            self.getField(fieldnameorobject) || {};
        if(specs === true || specs === false){
            specs = {isobject:specs};
        }
        var idprop = fieldObj.idprop || specs.idprop || '_id';
        var template = fieldObj.template || specs.template || '${name} <br/><small>(${'+idprop+'})</small>';
        var itemCss='joe-bucket-delete cui-block joe-icon';
        var value = (fieldObj.isobject || specs.isobject)?
            'data-isobject=true data-value="'+encodeURI(JSON.stringify(item))+'"':
            'data-value="'+item[idprop]+'"';

        var bucketHtml = '<li '+value+'>' +fillTemplate(template,item)
            +'<div class="'+itemCss+'" onclick="$(this).parent().remove()"></div></li>';

        return bucketHtml;
    };
    encapsulateFieldType('buckets',self.renderBucketsField,{create:self.renderBucketItem,ready:self.readyBinFields});
/*----------------------------->
 L | Content
 <-----------------------------*/
    this.renderContentField = function(prop){
        /*
         description:'renders content field',
         tags:'content,field,render',
         specs:['idprop','template','run','value']
         */
        var html = '';
		var itemObj = self.current.object;
		var idProp = prop.idprop || self.getIDProp();
		var constructedItem =self.current.constructed || {};
		if(!self.current.object[idProp] ||(constructedItem[idProp] && constructedItem[idProp] == self.current.object[idProp])){
			itemObj = constructedItem;

            if(self.current.object[idProp] && constructedItem[idProp] == self.current.object[idProp]){
                if(rerenderingField){

                    itemObj = $.extend({},  self.current.object,constructedItem);
                }else{

                    itemObj = $.extend({}, constructedItem, self.current.object);
                }
            }
		}
        try {
            if (prop.run) {
                html += prop.run(itemObj, prop) || '';
            } else if (prop.template) {
                html += fillTemplate(self.propAsFuncOrValue(prop.template), itemObj);
            } else if (prop.value) {
            	html += self.propAsFuncOrValue(prop.value);
            }
        }catch(e){

            self.Error.add('error rendering field:'+e,e,{caller:'self.renderContentField'})
            return 'error rendering field:'+e;
        }
        return html;

    };
    encapsulateFieldType('content',self.renderContentField);
    this.Render.fieldListItem = this.renderFieldListItem = function(item,contentTemplate,schema,specs){
        /*|{
            description:'renders a preformated list item for use in itemexpanders, details page, etc',
            featured:true,
            specs:isObject,deleteButton,expander,gotoButton,itemMenu,schemaprop,nonclickable,bgcolor,stripecolor,action
            tags:'render, field list item',
            cssclass:'.joe-field-list-item || .joe-field-item'
        }|*/

        var specs = $.extend({
            isObject:false,
            deleteButton:false,
            expander:null,
            gotoButton:false,
            itemMenu:false,
            schemaprop:false,
            nonclickable:false,
            action:false,
            stripecolor:false,
            bgcolor:false,
            checkbox:false,
            value:''
        },specs);
        var schemaprop = specs.schemaprop && self.propAsFuncOrValue(specs.schemaprop,item);
        var schema = ((schemaprop && item[schemaprop])||schema||item.itemtype||item.type);
        var schemaobj = self.schemas[schema];
        var contentTemplate = self.propAsFuncOrValue(contentTemplate,null,null,item)
        || self.propAsFuncOrValue(schemaobj && (schemaobj.listTitle || (schemaobj.listView && schemaobj.listView.title)),item)
        || '<joe-title>${name}</joe-title><joe-subtext>${itemtype}</joe-subtext>';
        
        var idprop = specs.idprop || (schemaobj && schemaobj.idprop) ||'_id';
        var hasMenu = specs.itemMenu && specs.itemMenu.length;
        var action = specs.action || ' onclick="goJoe(_joe.search(\'${'+idprop+'}\')[0],{schema:\''+schema+'\'})" ';
        var nonclickable = self.propAsFuncOrValue(specs.nonclickable,item);
        var clickablelistitem = (!specs.gotoButton && !nonclickable/* && !hasMenu*/);
        //var clickablelistitem = !!clickable && (!specs.gotoButton && !hasMenu);
        var deleteHandler = (specs.fieldname && '_joe.Fields.objectreference.remove(\''+specs.fieldname+'\');') || '';
        var deleteButton = '<div class="joe-delete-button joe-block-button right" ' +
            'onclick="__cancelPropagation();$(this).parent().remove();'+deleteHandler+'">&nbsp;</div>';
        var expanderContent = renderItemExpander(item,specs.expander);

        var click = (!clickablelistitem)?'':action;

        var value = (specs.isObject)?
            'data-value="' +encodeURI(JSON.stringify(specs.value))+ '" data-isObject=true'
            :'data-value="' + specs.value + '"';
        
        var html = fillTemplate(`<joe-list-item schema="${schema}" 
            itemId="${item[idprop]}"
            idprop="${idprop}"
            ${(specs.icon && `icon="${specs.icon}"`) ||''}
            class="`
            +((clickablelistitem && 'joe-field-list-item clickable') ||'joe-field-item')
            +(specs.deleteButton &&' deletable' ||'')
            +(specs.gotoButton &&' gotobutton' ||'')
            +(specs.itemMenu &&' itemmenu' ||'')
            +(specs.stripecolor &&' striped' ||'')
            +(specs.checkbox &&' checkboxed' ||'')
            +(specs.expander &&' expander expander-collapsed' ||'')+'" '
            +(value ||'')
            +'>'

                    //if(clickableListItem){
                        +((hasMenu && renderItemMenu(item, specs.itemMenu)) || '')
                        
                        +self.Render.bgColor(item,specs.bgcolor)
                        
                        + self.Render.itemCheckbox(item,schemaobj)

                        +((specs.link && '<a class="non-link" onclick="__cancelClick();" href="'+specs.link+'">')||'')  

                        + '<div class="joe-field-item-content '+(nonclickable && 'nonclickable' || '')+'" ' + click + ' >'
                            + self.propAsFuncOrValue(contentTemplate+__clearDiv__, item)
                        + __clearDiv__+'</div>'

                        +((specs.link && '</a>')||'')   
                        +self.Render.stripeColor(item,specs.stripecolor)
                        + self._renderExpanderButton(expanderContent, item)
                        + (specs.deleteButton && deleteButton || '')
                   /* }else {
                        //+click
                        +((hasMenu && renderItemMenu(item, specs.itemMenu)) || '')
                        + '<div class="joe-field-item-content" ' + click + '>'
                        + (specs.deleteButton && deleteButton || '')
                        + self._renderExpanderButton(expanderContent, item)
                        + self.propAsFuncOrValue(contentTemplate, item)
                        + '</div>'
                    }*/
                +(specs.gotoButton && '${RUN[_renderGotoButton]}' || '')
                +expanderContent
 
            +'</joe-list-item>',item);
        return html;
    };
/*----------------------------->
 M | URL
 <-----------------------------*/
    this.renderURLField = function(prop){
        /*|{

            tags:'render,field,URL'
        }|*/
        var profile = self.current.profile;
        var disabled = _disableField(prop);// (prop.locked &&'disabled')||'';
        
        var prefix = self.propAsFuncOrValue(prop.prefix);
        
        prefix = fillTemplate(prefix||'',self.current.object);
        var html=
            prefix+
            `<joe-button schema="${prop.schema||''}" action="view" class="joe-button" color="green" prefix="${prefix}" ></joe-button>`
            +'<input class="joe-url-field joe-field" type="text" ' +
            self.renderFieldAttributes(prop)+
            'name="'+prop.name+'" value="'+(prop.value || '')+'"  '+disabled+' />'
       + __clearDiv__;
        return html;
    };
    this.gotoFieldURL = function(dom,prefix){
        var url = $(dom).siblings('.joe-url-field').val();
        
        window.open((prefix||'')+url);
    };
    encapsulateFieldType('url',self.renderURLField);
/*----------------------------->
 N | Color
 <-----------------------------*/
    this.renderColorField = function(prop){
        /*|{
            tags:'render,field,color',
            specs:''
        }|*/
        var html=
            '<input class="joe-color-field joe-field" type="text"  name="'+prop.name+'" value="'+(prop.value || '')+'" '
            +self.renderFieldAttributes(prop)
            +' />'+


        //add onblur: hide panel
        '<script>' +
            '_joe._colorFieldListener($(\'input.joe-color-field[name='+prop.name+']\')[0]);'+
        '$(\'input.joe-color-field[name='+prop.name+']\').keyup(_joe._colorFieldListener);' +
        '</script>';
        return html;
    };
    this._colorFieldListener = function(e){
        var field = e.delegateTarget||e;
        var color = field.value;
        $(field).parents('.joe-object-field').css('background-color',color);

    };
    encapsulateFieldType('color',self.renderColorField);


/*----------------------------->
 O | Object List Field
 <-----------------------------*/
    function getObjectlistSubProperty(prop){
        /*|{
        description:'renders an object list field',
            tags:'render,field,objectList'
        }|*/
        var subprop = prop;
        if($.type(prop) == "string"){
            subprop = {name:prop};
            subprop.name = subprop.name || prop;
        }else{
            if(subprop.extend && self.fields[subprop.extend]){
                subprop = $.extend({},subprop,self.fields[subprop.extend],(subprop.specs||{}))
                subprop.name = subprop.name || subprop.extend;
            }
        }
        return subprop;
    }
    this.renderObjectListField = function(prop){
        /*|{
            tags:'render,field,objectlist',
            specs:'new,locked,max,value,properties,standard_field_properties'
        }|*/
        var hiddenHeading = (prop.hideHeadings)?" hidden-heading " :'';
        var sortable = (prop.sortable === false)?"":" sortable ";
	    var html = "<table class='joe-objectlist-table " + hiddenHeading + sortable+"' >"
		    + self.renderObjectListHeaderProperties(prop)
		    + self.renderObjectListObjects(prop)
		    //render a (table/divs) of properties
		    //cross reference with array properties.
		    + "</table>";
        var max = prop.max;
        var pName = (prop.display || prop.name).split(' ');
        pName = pName[pName.length-1];
        if(pName[pName.length-1] == "s"){
            pName = pName.substr(0,pName.length-1).toLowerCase();
        }
        var label = prop.label ||
         (pName && (`add ${pName}`)) 
         || 'add item';
        if ((!max || !prop.value || (prop.value.length < max)) && !prop.locked) {
	        var addaction = 'onclick="getJoe(' + self.joe_index + ').addObjectListItem(\'' + prop.name + '\')"';
            html += '<div style="text-align:center;">';
            var doneaction = 'onclick="_joe.Fields.objectlist.editHandler($(this).parents(\'.joe-object-field\').find(\'.row-template\').eq(0),true)"';
            if(prop.hasOwnProperty('template')){
                html += '<joe-button style="" class=" joe-done-button  joe-button joe-grey-button" ' + doneaction + '> - </joe-button>'
            }
	        html += '<div class="joe-button joe-iconed-button joe-plus-button joe-orangegrey-button" ' + addaction + '> '+label+'</div>' + __clearDiv__ + '</div>';
        }
        return html;
    };
    this.objectlistdefaultproperties = ['name','_id'];
//render headers
    this.renderObjectListHeaderProperties = function(prop){
        var properties = self.propAsFuncOrValue(prop.properties) || self.objectlistdefaultproperties;
        var property;
        var subprop;
        var width;
        var sortable = (prop.sortable === false)?false:true;
        var onclick;
        var html = '<thead><tr>'+
        ((sortable &&'<th class="joe-objectlist-object-row-handle-header" ></th>')||'')+
        '<th class="joe-objectlist-object-row-template-header"></th>';
        for(var p = 0,tot = properties.length; p<tot;p++){
            subprop = getObjectlistSubProperty(properties[p]);
/*            if($.type(properties[p]) == "string"){
                subprop = self.fields[subprop]||{name:properties[p]}
                subprop.name = subprop.name || properties[p];
            }*/

            
            property = {
                name: subprop.display||subprop.name,
                type: subprop.type||'text'
            };
            if(sortable){
                onclick='title="sort by '+property.name+'" onclick="_joe.Fields.objectlist.sortBy(\''+prop.name+'\',\''+subprop.name+'\');"';
            }else{
                onclick = '';
            }
            width=(subprop.width)?'width="'+subprop.width+'"':'';
            html+="<th "+onclick+" data-subprop='"+subprop.name+"' "+width+">"
                +(subprop.display || subprop.name)+"</th>";
        }
        html+="<th class='joe-objectlist-delete-header'></th>";
        html+="</tr></thead>";
        return html;
    };

//render objects
    this.renderObjectListObjects = function(prop){
        var objects = prop.value || 
        (self.current.constructed || self.current.object)[prop.name] || 
        prop['default'] || [];
        var properties = self.propAsFuncOrValue(prop.properties) || self.objectlistdefaultproperties;

        var html = '<tbody>';
        var obj;
        for(var o = 0,objecttot = objects.length; o<objecttot;o++){
            obj = objects[o];
            html += self.renderObjectListRow(obj, properties, o, prop);
	        //parse across properties

        }
        html+="</tbody>";
        return html;
    };

    function _objectlistTemplateClickHandler(templateDom,stopEditing){
        var fieldname = $(templateDom).parents('.joe-object-field').data('name');
        var fieldObj = self.getField(fieldname);
        var templateContent;
        var subobj;
        var constructed = _jco(true);
        (stopEditing && $(templateDom).parents('.joe-object-field').find('.editing') 
        || $(templateDom).parent().siblings('.editing')).each(function(index,dom){
            subobj = constructed[fieldname][$(this).index()];
            templateContent= fillTemplate(self.propAsFuncOrValue(fieldObj.template,null,null,subobj),subobj);
            $(this).find('.row-template').html(templateContent);
            $(this).removeClass('editing');
        })
        if(!stopEditing){
            $(templateDom).parents('.joe-objectlist-row').addClass('editing');
            $(templateDom).parents('.joe-objectlist-row').find('.joe-text-field,.joe-integer-field,.joe-number-field,select,textarea').eq(0)[0].focus();
        }
    }

    this.renderObjectListRow = function (object, objectListProperties, index, prop,editing) {
        var properties = self.propAsFuncOrValue(objectListProperties) || self.objectlistdefaultproperties;
        var prop,property;
        var sortable = (prop.sortable === false)?false:true;
        var html = '';//"<joe-objectlist-item>";
        //var html = "<tr class='joe-object-list-row' data-index='"+index+"'><td class='joe-objectlist-object-row-handle'>|||</td>";
        var templateContent = fillTemplate(self.propAsFuncOrValue(prop.template,null,null,object),object);
        html += 
        "<tr class='joe-objectlist-row "+((templateContent||editing) && 'templated' ||'')+" "+(editing && 'editing' ||'')+"' data-index='" + index + "'>"+
        ((prop.locked)
			? "<td class='template-visible'></td>"
			:((sortable && "<td class='sort-handle template-visible'><div class='joe-panel-button joe-objectlist-object-row-handle'>|||</div></td>")||''));

        var onclick = 'onclick="_joe.Fields.objectlist.editHandler(this);"';
        html+="<td class='row-template template-visible' "+onclick+" colspan='"+(properties.length+1)+"'>"+templateContent+"</td>";
        var renderInput = {
            'text':self.renderTextField,
            'select':self.renderSelectField,
            'date':self.renderDateField,
            'content':self.renderContentField,
            'rendering':self.renderRenderingField,
            'boolean':self.renderBooleanField,
            'url':self.Fields.url.render
        };

        var subprop;
        //show all properties
        for(var p = 0,tot = properties.length; p<tot;p++){
            subprop = getObjectlistSubProperty(properties[p]);
            //prop = ($.type(properties[p]) == "string")?{name:properties[p]}:prop;
            property = $.extend({
                name: subprop.name,
                type:subprop.type||'text',
                value:object[subprop.name] || ''
            },subprop);
            self.Fields['objectlist'].toReady.push(property.type);
            html+="<td>"+(renderInput[property.type]||self.Fields[property.type].render)(property,object)+"</td>";

        }
        var delaction = "onclick='getJoe("+self.joe_index+")._oldeleteaction(this);'";
        html += prop.locked
			? "<td class='template-visible'></td>"
			: "<td class='template-visible'><div class='jif-panel-button joe-delete-button' " + delaction + ">&nbsp;</div></td>";
	    html += '</tr>';//</joe-objectlist-item>';



        return html;
    };
   
    this.addObjectListItem = function(fieldname,specs){
        self.Fields['objectlist'].toReady = [];
        var fieldobj = self.getField(fieldname);
        var index = $('.joe-object-field[data-name='+fieldname+']').find('.joe-objectlist-row').length;
        //$('.joe-object-field[data-name='+fieldname+']').find('.joe-objectlist-row.editing').removeClass('editing');
        var content = self.renderObjectListRow(
            self.propAsFuncOrValue(fieldobj.new)||{},
            fieldobj.properties,
            index,fieldobj,fieldobj.hasOwnProperty('template'));
        $('.joe-object-field[data-name='+fieldname+']').find('tbody').append(content);
        var lastinputfield;
        $('.joe-object-field[data-name='+fieldname+'] .joe-objectlist-row:last').find('.joe-text-field,.joe-integer-field,.joe-number-field,select,textarea').eq(0)[0].focus();
        //ready all newly created fields
        self.Fields['objectlist'].toReady.map(function(fieldtype){
            self.Fields[fieldtype] && self.Fields[fieldtype].ready && self.Fields[fieldtype].ready(fieldobj);
        });
        if(fieldobj.hasOwnProperty('template')){
        _objectlistTemplateClickHandler($('.joe-object-field[data-name='+fieldname+'] .joe-objectlist-row:last .row-template'));
        }

    };
    this._oldeleteaction = function(dom){
        $(dom).parents('.joe-objectlist-row').remove();
    };
    this.removeObjectListItem = function(fieldname,index){
        var fieldobj = self.getField(fieldname);
        $('.joe-object-field[data-name=module_fields]').find('tbody').append(content);
    };
    function _sortObjectListRows(property,sortBy){
        try{
            var overwrites = {};
            overwrites[property] = _jco(true)[property].sortBy(sortBy);
            _joe.Fields.rerender(property,overwrites);
        }catch(e){
            self.Error.add('error sorting objectlist',e,{property:property,sortBy:sortBy})
        }
    }
    function readyObjectListFields(){
        function olHelper(e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            if($helper.hasClass('templated')){
                $helper.children().each(function (index) {
                    $(this).toggle( $(this).hasClass('template-visible')).width($originals.eq(index).width());
                });
                
            }else{
                $helper.width(tr.width());
                $helper.children().each(function (index) {
                    // Set helper cell sizes to match the original sizes
                    $(this).toggle( !$(this).hasClass('row-template'));
                    $(this).width($originals.eq(index).width());
                });
            }
            
            return $helper;
        }
        self.overlay.find('.joe-objectlist-table.sortable').each(function(){
            $(this).find('tbody').sortable(
                {   axis:'y',
                handle:'.joe-objectlist-object-row-handle',
                helper:olHelper
                }
            )
        });
    }
    encapsulateFieldType('objectlist',self.renderObjectListField,
        {   add:self.addObjectListItem,
            ready:readyObjectListFields,
            toReady:[],
            editHandler:_objectlistTemplateClickHandler,
            sortBy:_sortObjectListRows
        });
/*----------------------------->
 P | Render Checkbox Group
 <-----------------------------*/
    this.renderCheckboxGroupField = function(prop){
        /*|{
            tags:'render,field,checkbox,group',
            specs:'values,idprop,standard_field_properties'
        }|*/
        //var profile = self.current.profile;
       /* var values = ($.type(prop.values) == 'function')?prop.values(self.current.object):prop.values||[];*/
        var values = self.getFieldValues(prop.values,prop);
        var locked = self.propAsFuncOrValue(prop.locked)?' disabled ':'';
        var html= '';
        var checked;
        var itemid;
        var propval = self.propAsFuncOrValue(prop.value);
        if($.type(propval) != "array"){
            propval = [propval];
        }
        var idprop = prop.idprop || '_id';
        var cols = prop.cols || prop.numCols|| 2;
        values.map(function(value){
            if($.type(value) != 'object'){
                var tempval = {name:value};
                tempval[idprop]=value;
                value = tempval;
            }
            itemid = 'joe_checkbox-'+prop.name;
            checked = ((propval||[]).indexOf(value[idprop]) != -1)?' checked ':'';
            html+= '<div class="joe-group-item '+locked+' cols-'+cols+'"><label >'
            +'<div class="joe-group-item-checkbox">' +
            '<input class="joe-field" '+locked+' type="checkbox" name="'+prop.name+'" '
            +checked
            +self.renderFieldAttributes(prop)
            +'value="'+value[idprop]+'" '+_disableField(prop)+` onchange="getJoe(${self.joe_index}).Fields.group.change('${prop.name}')" /></div>`
            +((prop.template && fillTemplate(prop.template,value))||value.label ||value.name || '') +'</label></div>';
        });
        html+= __clearDiv__;
        return html;
    };
    this.onCheckboxGroupUpdate = function(fieldname){
        
        var fieldObj = self.Fields.get(fieldname);
        var newTitle = _joe.propAsFuncOrValue(fieldObj.display||name)
        //update display
        document.querySelector(`.joe-field-label[title="${fieldname}"]`).innerHTML = newTitle;
    }
    encapsulateFieldType('group',self.renderCheckboxGroupField,{
        change:self.onCheckboxGroupUpdate
    });
/*----------------------------->
 Q | Code Field
 <-----------------------------*/
	this.renderCodeField = function(prop){
        /*|{
            tags:'render,field,code',
            specs:'value,language,standard_field_properties'
        }|*/
		var profile = self.current.profile;
		var height = (prop.height)?'style="height:'+prop.height+';"' : '';
		var code_language = (self.propAsFuncOrValue(prop.language)||'html').toLowerCase();
		var editor_id = cuid();
		var html=
            `<div>
            <joe-button class="tight" onclick="_joe.Fields.code.fullscreen('${editor_id}');"> &#x26F6; fullscreen</joe-button>
            </div>`+
            '<div class="clear joe-ace-holder joe-rendering-field joe-field" '
			+height+' data-ace_id="'+editor_id+'" data-ftype="ace" name="'+prop.name+'">'+
			'<textarea class=""  id="'+editor_id+'"  >'
			+(prop.value || "")
			+'</textarea>'+
			'</div>'+
		'<script>'+
			'var editor = ace.edit("'+editor_id+'");\
			editor.setTheme("ace/theme/tomorrow");\
			editor.getSession().setUseWrapMode(true);\
			editor.getSession().setMode("ace/mode/'+code_language+'");\
			editor.setOptions({\
				enableBasicAutocompletion: true,\
				enableLiveAutocompletion: false\
			});\
			_joe.ace_editors["'+editor_id+'"] = editor;'
		+' </script>';
		return html;
    };
    this.fullScreenCodeEditor = function(editor_id){
        var elem = document.querySelector(`div.joe-ace-holder[data-ace_id="${editor_id}"]`);
		if (elem.requestFullscreen) {
			elem.requestFullscreen();
		} else if (elem.webkitRequestFullscreen) { /* Safari */
			elem.webkitRequestFullscreen();
		} else if (elem.msRequestFullscreen) { /* IE11 */
			elem.msRequestFullscreen();
		}
    }
    encapsulateFieldType('code',self.renderCodeField,
    {fullscreen:this.fullScreenCodeEditor});
/*----------------------------->
 Q.2 | QR Field
 <-----------------------------*/
	this.renderQRCodeField = function(prop){
        /*|{
            tags:'render,field,qr,qrcode',
            specs:'value,size,template,url,standard_field_properties,showContent'
        }|*/
        var height = (prop.height)?'style="height:'+prop.height+';"' : '';
        var size = prop.size || 280;
        var idprop = _joe.getIDProp();
        var template = self.propAsFuncOrValue(prop.template) || '{\"${itemtype}\":\"${'+idprop+'}\"}';
        var content = fillTemplate;
        template = fillTemplate(template,self.current.object);
        var qrcode_url = prop.url||
        'http://www.qr-code-generator.com/phpqrcode/getCode.php?cht=qr&chl='+encodeURIComponent(template)+'&chs='+size+'x'+size+'&choe=UTF-8&chld=L|0';
        var html = '<a href="'+qrcode_url+'" target="_blank"><img class="joe-qrcode-image" src="'+qrcode_url+'" /></a>';
        if(self.propAsFuncOrValue(prop.showContent)){
            html+='<joe-subtext>'+template+'</joe-subtext>';
        }
		
		return html;
	};
    encapsulateFieldType('qrcode',self.renderQRCodeField);
/*----------------------------->
	R | Rendering Field
<-----------------------------*/
	this.renderRenderingField = function(prop){
        /*|{
            tags:'render,field,rendering,textarea',
            specs:'value,standard_field_properties'
        }|*/
        var locked = self.propAsFuncOrValue(prop.locked)?' disabled ':'';
		var profile = self.current.profile;
        var height = (prop.height)?'style="height:'+prop.height+';"' : '';
        var placeholder = self.propAsFuncOrValue(prop.placeholder) || '';
		var html=
			'<textarea placeholder="'+placeholder+'" class="joe-rendering-field joe-field" '+height+' '+locked+'name="'+prop.name+'" >'
            +(prop.value || "")+'</textarea>';
		return html;
	};
    encapsulateFieldType('rendering',self.renderRenderingField);
/*----------------------------->
 T | Tags Field
 <-----------------------------*/
	this.renderTagsField = function(prop){
        /*|{
            tags:'render,field,tag',
            specs:'value,autocomplete,standard_field_properties'
        }|*/
		var profile = self.current.profile;
		var height = (prop.height)?'style="height:'+prop.height+';"' : '';
		var specs = $.extend({},prop,{autocomplete:true}); //,{onblur:'_joe.showMessage($(this).val());'})
		var html= '<div class="joe-tags-container">'
			+self.renderTextField(specs)
			+'<div class="joe-text-input-button">add</div>'
		+'</div>';

		return html;
	};

/*----------------------------->
 U | Uploader Field
 <-----------------------------*/
    this.uploaders = {};
    this.renderUploaderField = function(prop){
        /*|{
            tags:'render,field,upload,uploader',
            specs:'field,url_field,onupload(file,callback,base64),onConfirm(data,callback),server_url'
        }|*/
        var uploader_id = cuid();
        if(typeof AWS == 'undefined' ){
            $('body').append(
                '<script>function none(){return; }</script>' +
                '<script type="text/javascript" src="https://sdk.amazonaws.com/js/aws-sdk-2.1.34.min.js"></script>'+
                //'<script src="http://webapps-cdn.esri.com/CDN/jslibs/craydent-1.7.37.js"></script>'+
                '<script src="'+joe_web_dir+'js/libs/craydent-upload-2.0.0.js" type="text/javascript"></script>'+
                '');
        }

        var dz_message = '<div style="line-height:100px;">drag files here to upload</div>';

        var files = prop.value || [];
        if(files && !$c.isArray(files)){
            files = [files];
        }
        if(prop.field && self.current.object[prop.field]){
            var kfield = self.getField(prop.field);
            var preview = self.current.object[prop.field]
            if(kfield.prefix){
                preview = fillTemplate(kfield.prefix,self.current.object)+preview;
            }
            dz_message = '<img src="'+preview+'"/>'
        }else if(files){
            if(files.length){
                dz_message = '';
            }
            dz_message+=_renderUploaderFilePreviews(files,uploader_id);
            
            
        }
        var html=
            '<div class="joe-uploader joe-field" data-ftype="uploader" name="'+prop.name+'" data-uploader_id="'+uploader_id+'">'+
                
                '<div class="joe-uploader-dropzone">'+
                    '<div class="joe-uploader-preview">'+dz_message+'</div>'+
                '</div>'+
                '<div class="joe-uploader-message">add a file</div>'+
            '<div class="joe-button joe-green-button joe-upload-cofirm-button hidden"  onclick="_joe.uploaderConfirm(\''+uploader_id+'\',\''+prop.name+'\');">Upload File</div>'+__clearDiv__+
            '</div>'
            ;
        //var idprop = prop.idprop || '_id';
        //html+= __clearDiv__;
        var uploader_obj = {
            cuid:uploader_id,
            prop:prop.name,
            files:files,
            max:prop.max || 0
        }
        self.uploaders[uploader_id] = uploader_obj;
        return html;
    };
    function _removeUploaderFile(id,filename){
        //TODO:finish cleanup
        var joe_uploader = self.uploaders[id];
        file = joe_uploader.files.where({filename:filename})[0];
        if(file){
            joe_uploader.files.remove(file);
            joe_uploader.preview.html(_renderUploaderFilePreviews(joe_uploader.files,joe_uploader.cuid));
        }
    }
    function _renderUploaderFilePreviews(files,cuid){
        var jup_template,html= '';
        var alink ="<a href='${url}${base64}' class='file-link' target='_blank'></a>";
        var delete_btn = '<joe-uploader-file-delete-btn class="svg-shadow" onclick="_joe.Fields.uploader.remove(\''+cuid+'\',\'${filename}\');">'+self.SVG.icon.close+'</joe-uploader-delete-btn>';
        var label = '<joe-uploader-file-label>${filename}</joe-uploader-file-label>';
        
        
        files.map(function(file){
            var filesize = '<joe-uploader-file-label>${filename}</joe-uploader-file-label>';
            if(file.size){
                if(file.size > 1000000){
                    filesize = `${(file.size/1000000).toFixed(2)} mb`;
                }else if(file.size > 1000){
                    filesize = `${(file.size/1000).toFixed(2)} kb`;
                }
                filesize = `<joe-uploader-file-size>${filesize}</joe-uploader-file-size>`;
            }else{
                filesize = '';
            }
            


            if(!$c.isObject(file)){file = {url:file,filename:file.substr(file.lastIndexOf('/') >0 && file.lastIndexOf('/') || 0)};}
            const imageTypes = ['jpg', 'jpeg', 'png', 'gif', 'svg'];
            const docTypes = ['doc', 'docx'];
            
            
            
            
            if((file.type && file.type.contains('image')) || (!file.type && (file.url||file.base64).split('.').contains(['jpg','jpeg','png','gif','svg']))){
                jup_template = '<joe-uploader-file class="'+(file.uploaded && 'uploaded'||'')+'" style="background-image:url(${url}${base64})">'+alink+label+filesize+delete_btn+'</joe-uploader-file>';
            }else if (docTypes.includes((file.url || file.base64).split('.').pop())) {
                jup_template = '<joe-uploader-file class="'+(file.uploaded && 'uploaded'||'')+'">'+
                '<joe-uploader-file-extension >.'+(file.type.split('/')[1] || 'docx')+'</joe-uploader-file-extension>'+
                alink+label+filesize+delete_btn+'</joe-uploader-file>';
            }else{
                jup_template = '<joe-uploader-file class="'+(file.uploaded && 'uploaded'||'')+'">'+
                '<joe-uploader-file-extension >.'+(file.type.split('/')[1] || '???')+'</joe-uploader-file-extension>'+
                alink+label+filesize+delete_btn+'</joe-uploader-file>';
            }    
            html += fillTemplate(jup_template,file);
        })
        return html+'<div class="clear"></div>';
    }
    this.onUserUpload = function(file,base64) {
        var dom = $(this.dropZone).parent();

        var joe_uploader = self.uploaders[dom.data('uploader_id')];
        var field = _joe.getField(joe_uploader.prop);
        if(field.use_legacy){
            if (file) {
                joe_uploader.file = file;
                joe_uploader.base64 = base64;
                
                $(joe_uploader.dropzone).find('img,div').replaceWith('<img src="' + base64 + '">')
                //joe_uploader.dropzone.html('<img src="' + base64 + '">');
                joe_uploader.message.html('<b>'+file.name+'</b> selected');
                joe_uploader.confirmBtn.removeClass('hidden');
            }else {
                results.innerHTML = 'Nothing to upload.';
            }
        }else{
            if (file) {
                var fname = file.name.replace(/ /g,'_');
                if(joe_uploader.files.where({filename:fname}).length){
                    var ext = fname.substr(fname.lastIndexOf('.'))
                    fname = fname.substr(0,fname.lastIndexOf('.'))
                    if(fname.lastIndexOf('_') > 0){
                        var fint =parseInt(fname.substr(fname.lastIndexOf('_')+1));
                        fint+=1;
                        fname = fname.substr(0,fname.lastIndexOf('_')+1)+fint;
                    }else{
                        fname = fname+'_1';
                    }
                    fname += ext;

                }
                var temp_file = {
                    uploaded:false,
                    filename:fname,
                    extension:file.type || file.name.substr(file.name.lastIndexOf('.')),
                    size:file.size,
                    type:file.type,
                    base64:base64,
                    added:new Date()
                }
                joe_uploader.files.push(temp_file);
                $(joe_uploader.preview).html(_renderUploaderFilePreviews(joe_uploader.files,joe_uploader.cuid));
                joe_uploader.confirmBtn.removeClass('hidden');
            }else {
                results.innerHTML = 'Nothing to upload.';
            }
        }
    };
        //setup AWS
    this.uploaderConfirm = function(uploader_id){
        var joe_uploader = self.uploaders[uploader_id];
        var field = _joe.getField(joe_uploader.prop);

        if(field.use_legacy){
            var file = joe_uploader.file;
            var base64 = joe_uploader.base64;
            var extension =/* joe_uploader.file.type || */file.name.substr(file.name.lastIndexOf('.'));
            var callback = function(err,url){
                if(err){
                    joe_uploader.message.append('<div>'+(err.message||err)+'<br/>'+err+'</div>');
                    alert('error uploading');
                }else{
                    joe_uploader.message.html('<div>uploaded</div>'+url);
                    var nprop = {};
                    if(field.field){
                        nprop[field.field] = url;
                    }
                    if(field.url_field){
                        nprop[field.url_field] = url;   
                    }
                        
                        
                    self.rerenderField([field.field,field.url_field],nprop);
                    self.updateObject(null, null, true);
                    // self.panel.find('.joe-panel-menu').find('.joe-quicksave-button').click();

                    
                }
            };
            
            if(field.onConfirm){
                if(file){
                    joe_uploader.message.append('<div>pending</div>');
                    
                    field.onConfirm({extension:extension,file:file,base64:base64,field:field},callback);
                }
            }else{

            

                var uploadFunction = field.upload || field.onupload || function(file,callback,base64_url){
                        //alert('file cached, set upload callback for transmission');
                        var err = null;
                        callback(err,base64_url);
                    };
                if(file){
                        uploadFunction(file,callback,base64);
                }
            }
        }else{//new version
            var files = joe_uploader.files.where({uploaded:false});
            var callback = function(err,url,filename){
                self.upload_progress_counter--;
                if(err){
                    joe_uploader.message.append('<div>'+(err.message||err)+'<br/>'+err+'</div>');
                    alert('error uploading');
                }else{
                    var fileobj = joe_uploader.files.where({filename:filename})[0];
                    fileobj.uploaded = new Date();
                    fileobj.url = url;
                    delete fileobj.base64;
                }

                if(self.upload_progress_counter == 0){
                    joe_uploader.message.html('<div>uploaded '+files.length+' files</div>');
                    
                    joe_uploader.preview.html(_renderUploaderFilePreviews(joe_uploader.files,joe_uploader.cuid));

                    self.updateObject(null, null, true);
                        //TODO:add onchange callback
                    
                }
            };
            
            if(field.onConfirm){
                if(files && files.length){
                    joe_uploader.message.append('<div>'+files.length+' pending</div>');
                    self.upload_progress_counter = files.length;
                    files.map(function(file){
                        field.onConfirm({file:file,field:field},callback);
                    })
                    
                }
            }else{

            

                var uploadFunction = field.upload || field.onupload || function(file,callback,base64_url){
                        //alert('file cached, set upload callback for transmission');
                        var err = null;
                        callback(err,base64_url);
                    };
                if(file){
                        uploadFunction(file,callback,base64);
                }
            }
        }
     
    };
    this.readyUploaders = function(){
        self.panel.find('.joe-uploader').each(function(){
            var id = $(this).data('uploader_id');
          //$(this).find('.joe-uploader-message').html('awaiting file');
            var fieldObj = self.Fields.get($(this).parents('.joe-object-field').data('name'));
            var uploader = new Upload({
                useInputBox:true,
                target:$(this).find('.joe-uploader-dropzone')[0],
                deferUpload:true,
                onerror:function (data, status, response) {
                    var message = data.message || data || "Server responded with error code: " + status;
                    message = message.indexOf('bytes') != -1 ? 'file is too small, needs to be larger than 1MB' : message;
                    alert(message);
                    //objUpload['uploadFaces'].clear();
                    logit(data);
                    //$('divDropZone').innerHTML = data;
                },
                onfileselected:function(inputDom){
                    console.log(inputDom.val);
                    self.uploaders[id].uploader.upload();
                },
                //uploadUrl:
                onafterfileready:self.onUserUpload
            });
            self.uploaders[id].ready = true;
            self.uploaders[id].uploader = uploader;
            self.uploaders[id].message = $(this).find('.joe-uploader-message').html('awaiting file, previewing current');
            self.uploaders[id].preview = $(this).find('.joe-uploader-preview');
            self.uploaders[id].dropzone = $(this).find('.joe-uploader-dropzone');
            self.uploaders[id].confirmBtn = $(this).find('.joe-upload-cofirm-button');

        });
    };
        encapsulateFieldType('uploader',self.renderUploaderField,
        {ready:self.readyUploaders,remove:_removeUploaderFile});
/*----------------------------->
 V | Object Reference
 <-----------------------------*/

    this.renderObjectReferenceField = function(prop){
        /*|{
        featured:true,
         tags:'render,field,objectReference,callback',
         specs:'values,label(forbutton),autocomplete_template,reference_template,template,idprop,placeholder,sortable,callback,reference_specs'
         }|*/
        //var values = self.propAsFuncOrValue(prop.values) || [];
        if(prop.values && prop.values.async){
            if(prop.values.search){
                $.ajax('/API/search/', { 
                    data:prop.values.query ||{},
                    success: (data) => { /* do something with the data */ },
                    error: (err) => { /* do something when an error happens */}
                  });
            }
        }
        else{

        
            var values = self.getFieldValues(prop.values,prop);
            if(values)
            var value = ((rerenderingField)?
                self.current.constructed:self.current.object)[prop.name]
            //self.current.object[prop.name] ||
                prop.value || 
                (!self.current.object.hasOwnProperty(prop.name) && prop['default']) ||
                [];
            if($.type(value) != 'array'){
                value = (value != null)?[value]:[];
            }

            var disabled = _disableField(prop);
            var idprop =prop.idprop || self.getIDProp();
            var template = prop.autocomplete_template || prop.template 
                //|| (self.schemas[prop.values] && (self.schemas[prop.values].listView && self.schemas[prop.values].listView.title) || self.schemas[prop.values].listTitle)
                ||"<joe-subtitle>${name} ${points}</joe-subtitle><joe-subtext>${${info} || ${"+idprop+"}}</joe-subtext>";//prop.template ||
            //var values = ($.type(prop.values) == 'function')?prop.values(self.current.object):prop.values||[];
            var html = "Object Reference";
            var specs = $.extend(
                {},{
                    autocomplete:{idprop:prop.idprop,template:template},
                    values:values,
                    skip:true,
                    name:prop.name,
                    ftype:'objectReference',
                    placeholder:prop.placeholder

                }
            ); //,{onblur:'_joe.showMessage($(this).val());'})
            var sortable = true;
            if(prop.hasOwnProperty('sortable')){sortable = prop.sortable;}
            var html= '';
            if(!disabled){
                html+=
                    '<div class="joe-references-container">'
                    +self.renderTextField(specs)
                    +'<div class="joe-text-input-button joe-reference-add-button" data-fieldname="'+prop.name+'"' +
                    'onclick="getJoe('+self.joe_index+').addObjectReferenceHandler(this);">add</div>'
                    + '</div>'
            }
                html+=__clearDiv__+'<div class ="joe-object-references-holder '+disabled+(sortable && !disabled && 'sortable'||'')+'" data-field="'+prop.name+'">';


            var refSpecs = $.extend({deleteButton : !disabled},(prop.reference_specs||{}));
            //value.map(function(v){
            for(let i = 0;i<value.length;i++){
                let v = value[i];
                if($c.isObject(v)){
                    html += self.createObjectReferenceItem(v,null,prop.name,refSpecs);
                }else if($c.isString(v)){
                    html += self.createObjectReferenceItem(null,v,prop.name,refSpecs);
                }
            }
           // });
            html +='</div>';
            //html+= self.renderTextField(specs);
            return html;
        }
    };

    this.addObjectReferenceHandler = function(btn){
        var id = $(btn).siblings('input').val();
        if(!id){return false;}
        $(btn).siblings('input').val('');
        var fieldname = $(btn).data().fieldname;
        var field = _getField(fieldname);
        $('.joe-object-references-holder[data-field='+fieldname+']').append(self.createObjectReferenceItem(null,id,fieldname,field.reference_specs));
        if(field.callback){
            field.callback(_jco(true)[fieldname]);
        }

    };
    this.removeObjectReferenceHandler = function(fieldname){
        var field = _getField(fieldname);
        if(field.callback){
            field.callback(_jco(true)[fieldname]);
        }

    }
    this.createObjectReferenceItem = function(item,id,fieldname,specs){
        var field = _getField(fieldname);
        var deletable = true;

        var specs = $.extend({
                expander:field.expander||field.itemExpander,
                gotoButton:field.gotoButton||field.goto,
                itemMenu:field.itemMenu,
                schemaprop:field.schemaprop || 'itemtype',
                idprop:field.idprop,
                template:field.reference_template || field.template,
                deleteButton:true,
                nonclickable:field.nonclickable,
                fieldname:fieldname,
                bgcolor:field.bgcolor,
            value:'${_id}',
            },specs||{});
        if(item && typeof item == "object") {
            specs.isObject = true;
            specs.value = item;
        }
        var idprop = field.idprop||'_id';
        if(!item) {
            var values = self.getFieldValues(field.values,field);
            var item;
            for(var i = 0,tot = values.length; i <tot; i++){
                if(values[i][idprop] == id){
                    item = values[i];
                    break;
                }
            }
        }

        if(!item) {
            var deleteButton = '<div class="joe-delete-button joe-block-button left" ' +
                'onclick="$(this).parent().remove();_joe.Fields.objectreference.remove(\''+fieldname+'\');">&nbsp;</div>';
            return '<div class="joe-field-item deletable" data-value="' + id + '">' +
                deleteButton + "<div>REFERENCE NOT FOUND</div><span class='subtext'>" + id + "</span>" + '</div>';
        }

        specs.link = location.href.replace(location.hash,`#/${item.itemtype}/${item._id}`);
        var icon = self.Render.schema_icon(item.itemtype);

        var template = self.propAsFuncOrValue(specs.template, item,null, self.current.object )
        //||  "<joe-title>${name}</joe-title><joe-subtext>${${info} || ${"+idprop+"}}</joe-subtext>";
        ||  `${icon}<joe-title>${item.name}</joe-title><joe-subtext>${item.info||item[idprop]}</joe-subtext>`;
/*        var specs = {
            deleteButton:specs.deleteButton,
            expander:specs.expander,
            gotoButton:specs.gotoButton,
            itemMenu:specs.itemMenu,
            value:specs.value ||'${_id}',
            idprop:specs.idprop,
            schemaprop:specs.schemaprop,
            isObject:specs.isObject
        };*/
        var schema = (item.itemtype && _joe.schemas[item.itemtype] && item.itemtype)||'';
        return self.renderFieldListItem(item,template,schema,specs);

    };

    encapsulateFieldType('objectreference',self.renderObjectReferenceField,
        {   create:self.createObjectReferenceItem,
            add:self.createObjectReferenceItem,
            remove:self.removeObjectReferenceHandler
        });

/*----------------------------->
 W | Preview Field
 <-----------------------------*/
    this.renderPreviewField = function(prop){
        /*|{
         tags:'render,field,preview'
         }|*/
        //var locked = self.propAsFuncOrValue(prop.locked)?' disabled ':'';
        //var profile = self.current.profile;
        var height = prop.height || '600px';
        var url = self.propAsFuncOrValue(prop.url) || joe_web_dir+'pages/joe-preview.html';
        var construct = _joe.constructObjectFromFields();
        obj = (construct[self.getIDProp()] && construct) || self.current.object;
        var content= self.propAsFuncOrValue(prop.content,obj) || '';
        var bodycontent= self.propAsFuncOrValue(prop.bodycontent,obj) || '';
        var previewid = obj[self.getIDProp()] +'_'+prop.name;

        window.__PreviewTest= function(){
            alert('test alert');
        };
        window.__previews = window.__previews || {};
        window.__previews[previewid] = {content:content,bodycontent:bodycontent};
        url+='?pid='+previewid;
        var html=
            '<div class="joe-button joe-reload-button joe-iconed-button" onclick="getJoe('+self.joe_index+').rerenderField(\''+prop.name+'\');">Reload</div>'
                +'<div class="joe-preview-iframe-holder" style="height:'+height+'">'
            +'<iframe class="joe-preview-field joe-field joe-preview-iframe" width="100%" height="100%" name="'+prop.name+'" ' +
            'src="'+url+'"></iframe>'
                +'</div>'
            //+ '<a href="'+url+'" target="_blank"> view fullscreen preview</a><p>' + url.length + ' chars</p>';
            + '<div class="joe-button joe-iconed-button joe-view-button multiline" onclick="window.open(\''+url+'\',\'joe-preview-'+previewid+'\').joeparent = window;"> view fullscreen preview <p class="joe-subtext">' + url.length + ' chars</p></div>'+__clearDiv__;
        return html;
    };
    encapsulateFieldType('preview',self.renderPreviewField);
/*----------------------------->
 X //TextEditor Field
 <-----------------------------*/
    var tinyconfig = {};
    this.renderTinyMCEField = function(prop){
        /*|{
            tags:'render,field,wysiwyg'
        }|*/
         var locked = self.propAsFuncOrValue(prop.locked)?' disabled ':'';
        var height = (prop.height)?'style="height:'+prop.height+';"' : '';
        var editor_id = cuid();
        var html=
            '<div class="joe-tinymce-holder joe-texteditor-field joe-field" '
            /*+height*/+' data-texteditor_id="'+editor_id+'" data-ftype="tinymce" name="'+prop.name+'">'+
            '<div id="'+editor_id+'" class="joe-tinymce">'+(prop.value || "")+'</div>'+
            '</div>';
        return html;
    };

    this.readyTinyMCEs = function() {
        tinymce.init({
            selector: ".joe-tinymce",
            //max_height: 300,
           // height:500,
        plugins:['table wordcount link image code searchreplace autoresize fullscreen'],
        menu : { // this is the complete default configuration
        edit   : {title : 'Edit'  , items : 'undo redo | cut copy paste pastetext | selectall'},
        format : {title : 'Format', items : 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
        table  : {title : 'Table' , items : 'inserttable tableprops deletetable | cell row column'},
        tools  : {title : 'Tools' , items : 'spellchecker code link image'}
        },
        toolbar: 'fullscreen | undo redo | bold italic removeformat | bullist numlist outdent indent',
          //  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
          /*  plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],*/
            /*toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"*/
        });
        $('.joe-tinymce').each(function(){
            var field = self.getField($(this).parents('.wysiwyg-field').data('name'));
            if(field.height){
                $('.mce-edit-area').height(field.height);
            }

        });

    };
    self.texteditors = [];

    this.readyTextEditors = this.readyTinyMCEs;
    encapsulateFieldType('wysiwyg',self.renderTinyMCEField,{ready:self.readyTextEditors});

/*----------------------------->
Y | Object Field
 <-----------------------------*/
    this.renderSubObjectField = function(prop){
        /*|{
         tags:'render,field,subobject',
         specs:'locked',
         value:'(object)'
         }|*/
        var action = 'getJoe('+self.joe_index+').Fields.subobject.openMini(\''+prop.name+'\');';
        var html= prop.locked?'':
            '<div class="joe-button joe-view-button joe-iconed-button" onclick="'+action+'">Edit</div>';
        //textarea
            html+= '<joe-subobject-output><pre>'+JSON.stringify(prop.value,'','\t')+'</pre></joe-subobject-output>';

        return html;
    };
    encapsulateFieldType('subobject',self.renderSubObjectField,{
        openMini:function(propname){
            var field = self.getField(propname);
            var callback = self.Cache.add(function(data,id){
                var field = self.getField(data.prop);
                self.current.object[data.prop] = self.Mini.construct();
                self.Fields.rerender(data.prop);
            },{data:{prop:propname},type:'callback'});

            var specs = {
                object:field.value,
                title:field.display || field.name,
                menu:[{name:'update',callback:callback.id}],
                schema:field.schema,
                name:self.joe_index+'-'+propname
            };
            _joe.showMiniJoe(specs);
        },
        construct:function(parentFind,outputObject){
            var propname;
            parentFind.find('.subobject-field').each(function() {
                propname = $(this).data('name');
                outputObject[propname] = self.current.object[propname]
            });
        }
    });
/*----------------------------->
 Z | Passthrough
 <-----------------------------*/
    this.renderPassthroughField = function(prop){
        var html= 'passthrough';
        return html;
    };
    
/*----------------------------->
 27 | Comments
 <-----------------------------*/
    this.renderCommentsField = function(prop){
        var item = self.propAsFuncOrValue(prop.item) || self.current.object;
        var idprop = self.getIDProp();
        var user = self.propAsFuncOrValue(prop.user) || JOE.User || {};
        var usertemplate = prop.usertemplate || 
        '<joe-subtext><b>${name}</b> says: </joe-subtext>';
            
        var html=
        "<joe-comments-add>" +
        fillTemplate(usertemplate,user)+
        "<textarea></textarea>"+
        self.renderMenuButtons({
            css:'joe-iconed-button joe-comments-button fright',
            name:'comment',
            //action:'_joe.Fields.comments.add(this,\''+item[idprop]+'\');'
             action:'_joe.Fields.comments.add(this);'
        })+
        //<div class="joe-button joe-iconed-button joe-comments-button fright" onclick="addCommentHandler(this);">Comment</div>
//        "<div class='joe-button fright' onclick='addCommentHandler(this);'>Add Comment</div>"  
            __clearDiv__+
        "</joe-comments-add>" +
        "<joe-comments-view data-itemid='"+item[idprop]+"'>"+
            "loading"+
        "</joe-comments-view>";
        return html;
    };
    function _readyCommentsField(fieldname){
        $('joe-comments-view').each(function(){
            $.ajax({
                url:'/API/comments/item/'+$(this).data().itemid,
                dataType:'jsonp',
                success:updateCommentsField
            })
        })

        function updateCommentsField(payload){
            var html = '';
            if(payload.error){
                html+='error:'+payload.error;
                
            }else{
                html+=fillTemplate(
                    '<joe-comments-entry>'  
                    +'<joe-comments-user>${this.user.name}</joe-comments-user>'
                    +'<joe-subtext>${RUN[_joe.Utils.prettyPrintDTS;${timestamp}]}</joe-subtext>'
                    +'<joe-content>${comment}</joe-content>'
                    +'</joe-comments-entry>',
                    payload.comments.reverse());
            }
            $('joe-comments-view').html(html);
            
        }
    }
    function _commentsAddHandler(dom,itemid) {
        var comment = $(dom).siblings('textarea').val().trim();
        $(dom).siblings('textarea').val('');
        if (comment) {
            try{
                var item = self.current.object;
                var fieldName = $(dom).parents('.joe-object-field').data('name');
                var fieldObj = _joe.getField(fieldName);
                var user = self.propAsFuncOrValue(fieldObj.user) || JOE.User || {};
                var flag= self.propAsFuncOrValue(fieldObj.flag) || false;
                var payload = {
                        user:{_id:user._id,name:user.name},
                        item:{_id:item._id,name:item.name,type:item.itemtype},
                        comment:comment,
                        flag:flag,
                        timestamp:(new Date).toISOString()
                    };
                var action = fieldObj.action || function(payload){
                    JOE.SERVER.socket.emit('save_comment',payload);
                }

                action(payload);
            }catch(e){
                self.Error.add('saving comment',e,{
                    user:user,
                    itemid:itemid,
                    comment:comment,
                    flag:flag
                })
            }
            //NPC.saveComment(_joe.current.object, comment);
            // if(fieldObj && fieldObj.callback && $c.isFunction(fieldObj.callback)){
            //     try {
            //         fieldObj.callback(comment, USER, _joe.current.object, fieldObj);
            //     }catch(error){
            //         alert('error in comment callback:\n  error');
            //     }
            // }

        }
    }
    encapsulateFieldType('comments',self.renderCommentsField,{
        add:_commentsAddHandler,ready:_readyCommentsField});
/*----------------------------->
Field Rendering Helpers
 <-----------------------------*/
    function encapsulateFieldType(name,render,additionalFunctions){
        //render = render
        if(!render){
            return;
        }
        self.Fields[name] = {
            render:render
        };
        $.extend(self.Fields[name],additionalFunctions||{});

    }

    function _getField(fieldname){
        /*|{
         featured:true,
         description:'gets a currently registered field as a spec object',
         alias:'window._getField',
         tags:'field,helper'
         }|*/
        var fieldobj;
        for(var f = 0,tot= self.current.fields.length; f<tot; f++){
            fieldobj = self.current.fields[f];
            if(fieldobj.name == fieldname){
                return fieldobj;
            }
        }
        return false;

    }
    this.getField = this.Fields.get = _getField;
/*-------------------------------------------------------------------->
	4 | OBJECT LISTS
<--------------------------------------------------------------------*/

    //ITEM MENU

    function renderItemMenu(item,buttons){
        if(!buttons){
            return '';
        }
        buttons = self.propAsFuncOrValue(buttons,item);
        if(!buttons){
            return '';
        }
        var btn_template = '<td class="joe-option-menu-button" onclick="${action}"> ${name}</td>';
        var html = '<item-menu class="joe-panel-content-option-menu"><table class=""><tbody><tr>';
        var oc;
            buttons.map(function(b) {
                if (!b.hasOwnProperty('condition') || self.propAsFuncOrValue(b.condition,item)) {
                    oc = (b.url && "window.open(\'"+fillTemplate(b.url,item)+"\')" )
                        || b.action || "alert('" + b.name + "');";
                html += fillTemplate('<td class="joe-option-menu-button '+(self.propAsFuncOrValue(b.css,item)||'')+'" onclick="' + oc + '">' + b.name + '</td>', item)
                }
            });

            html+='</tr></tbody></table></item-menu>';


        return html;
    }


    //ITEM EXPANDER
    this._renderExpanderButton =function(expanderContent,item){
            if(!expanderContent){return '';}
            return '<div class="joe-panel-content-option-expander-button" onclick="_joe.toggleItemExpander(this);"></div>';
    };

    //window._renderExpanderButton = this._renderExpanderButton;
    this.toggleItemExpander = function(dom,itemid){
      if(dom){
          var expander = $(dom).siblings('.joe-panel-content-option-expander');
          if(expander.hasClass('on-demand') && !expander.hasClass('demanded')){
              var id = expander.data('itemid');
              var schemaname = expander.data('schema');
              var item = _joe.getDataItem(id,schemaname);
              var content = self.propAsFuncOrValue(self.schemas[schemaname].itemExpander,item);
              expander.addClass('demanded').html(content);
          }
              
              $(dom).closest('.expander').toggleClass('expander-collapsed')
              .toggleClass('expander-expanded');
          

      }
    };
    function renderItemExpander(item,contentVal,specs){
        var specs = specs || {};
        var content = fillTemplate(self.propAsFuncOrValue(contentVal,item,null,self.current.object),item);
        if(!content){return '';}
        if(specs.onDemand){
            var html = '<div class="joe-panel-content-option-expander on-demand" data-itemid="'+item[self.getIDProp(specs.schema)]+'" data-schema="'+(specs.schema || item.itemtype || self.current.schema)+'"></div>';
        }else{
            var html = '<div class="joe-panel-content-option-expander">'+content+'</div>';
        }
        


        return html;
    }
    this.renderItemExpander = renderItemExpander;

    this.renderTableItem = function(listItem,quick,index) {
        //var tableSpecs = tSpecs || tableSpecs;
        var idprop = self.getIDProp();// listSchema._listID;
        var id = listItem[idprop] || null;
        var colprop;
        if(quick){
            var searchable = '';
            tableSpecs.cols.map(function(c){
                if($c.isString(c)) {
                    searchable+=(listItem[c]||'')+' ';
                }else if($c.isObject(c)){
                    colprop = c.property || c.name;
                    searchable+=(
                        (c.template && self.propAsFuncOrValue(c.template,listItem[colprop],null,listItem))
                        ||(listItem[colprop]||(colprop.indexOf('${') != -1 && colprop)||'')
                        )
                        +' ';
                   // html += '<th>' + (c.display || c.header) + '</th>';
                }
                searchable = fillTemplate(searchable,listItem);
            });
            return searchable;
        }
        var action;
        //var action = 'onclick="_joe.editObjectFromList(\''+id+'\');"';
        var schemaprop = self.current.schema && (self.current.schema.listView && self.current.schema.listView.schemaprop);
        if(schemaprop){
            action = 'onclick="getJoe('+self.joe_index+').listItemClickHandler({dom:this,id:\''+id+'\',schema:\''+schemaprop+'\'});"';
        }else{
            action = 'onclick="getJoe('+self.joe_index+').listItemClickHandler({dom:this,id:\''+id+'\'});"';
        }

        var ghtml = '<tr class="joe-panel-content-option trans-bgcol" '+action+'>';
        ghtml +='<td class="joe-table-checkbox">' +
            '<label>'+index+(tableSpecs.multiselect && '<input type="checkbox" />' || '')+'</label></td>';
        //ghtml +='<td>'+index+'</td>';

        tableSpecs.cols.map(function(c){

            if($c.isString(c)) {
                ghtml+=fillTemplate('<td>'+(listItem[c]||'')+'</td>',listItem);
            }else if($c.isObject(c)){
                colprop = c.property || c.name;
                ghtml+=fillTemplate('<td>'
                    +((c.template && self.propAsFuncOrValue(c.template,listItem[colprop],null,listItem))
                    ||(listItem[colprop]||(colprop.indexOf('${') != -1 && colprop)||''))
                +'</td>',listItem);
            }

        });
        ghtml +='</tr>';
    return ghtml;
    };
    // this.renderGridItem = function(listItem,quick,index,specs) {
    //     var ghtml = '<tr class="joe-panel-content-option trans-bgcol">';
    //     ghtml +='<td class="joe-grid-checkbox"><label><input type="checkbox"></label></td>';
    //     ghtml +='<td>'+index+'</td>';
    //     ghtml +='<td>'+listItem[self.getIDProp()]+'</td>';
    //     ghtml +='</tr>';
    //     return ghtml;
    // };


    this.Render.gridItem = this.renderGridItem = 
    function(item,quick,index,specs){
        /*|{
            description:'renders a preformated list item for use in itemexpanders, details page, etc',
            featured:true,
            specs:,expander,gotoButton,itemMenu,schemaprop,nonclickable,bgcolor,stripecolor,action
            tags:'render, field list item',
            cssclass:'.joe-field-list-item || .joe-field-item'
        }|*/
		var specs  = $.extend(
			{
                template:'<joe-title>${name}</joe-title><joe-content>${info}</joe-content>',
                bgcolor:'#fff'
			},
			self.current.schema,
            self.current.schema.gridView,
			specs
		);


        var schema = specs.__schemaname;
        var schemaobj = self.schemas[schema];
        var contentTemplate = self.propAsFuncOrValue(specs.template,item)
        || '<joe-title>${name}</joe-title><joe-subtext>${itemtype}</joe-subtext>';
        
        var idprop = specs.idprop || (schemaobj && schemaobj.idprop) ||'_id';
        var hasMenu = specs.itemMenu && specs.itemMenu.length;
        var action = specs.action || ' onclick="goJoe(_joe.search(\'${'+idprop+'}\')[0],{schema:\''+schema+'\'})" ';
        var nonclickable = self.propAsFuncOrValue(specs.nonclickable,item);
        var clickablelistitem = (!specs.gotoButton && !nonclickable/* && !hasMenu*/);
        //var clickablelistitem = !!clickable && (!specs.gotoButton && !hasMenu);

        var expanderContent = renderItemExpander(item,specs.expander);

        var click = (!clickablelistitem)?'':action;

        
        var html = fillTemplate('<joe-option class="joe-content-option '
            +(specs.itemMenu &&' itemmenu' ||'')
            +(specs.stripecolor &&' striped' ||'')
            +(specs.expander &&' expander expander-collapsed' ||'')+'" '
            +'>'
                +((hasMenu && renderItemMenu(item, specs.itemMenu)) || '')
                
                +self.Render.bgColor(item,specs.bgcolor)
                + '<div class="joe-panel-content-option-content'+(nonclickable && 'nonclickable' || '')+'" ' + click + ' >'
                + self.propAsFuncOrValue(contentTemplate+__clearDiv__, item)
                + '</div>'
                +self.Render.stripeColor(item,specs.stripecolor)
                + self._renderExpanderButton(expanderContent, item)
                +expanderContent
            +'</joe-option>',item);
        return html;
    };

    this.renderListItem = function(listItem,quick,index){
		var listSchema  = $.extend(
			{
				_listID:'id',
                stripeColor:null
			},
			self.current.schema,
			self.current.specs
		);

		var idprop = self.getIDProp();// listSchema._listID;
		var id = listItem[idprop] || null;
		//var action = 'onclick="_joe.editObjectFromList(\''+id+'\');"';
        var action;
        var customAction = (self.current.schema && (
            (self.current.schema.listView && self.current.schema.listView.action )
            || self.current.schema.listAction));
        if(customAction){
            action = 'onclick="'+self.propAsFuncOrValue(customAction,listItem)+'"';
        }else {
            var schemaprop = self.current.schema && (self.current.schema.schemaprop || (self.current.schema.listView && self.current.schema.listView.schemaprop));
            if (schemaprop) {
                action = 'onclick="getJoe(' + self.joe_index + ')' +
                    '.listItemClickHandler({dom:this,id:\'' + id + '\',schema:\'' + listItem[schemaprop] + '\',idprop:\'_id\'});"';
            } else {
                action = 'onclick="getJoe(' + self.joe_index + ')' +
                    '.listItemClickHandler({dom:this,id:\'' + id + '\'});"';
            }
        }



    //add stripe color
        //var stripeColor = ($.type(listSchema.stripeColor)=='function')?listSchema.stripeColor(listItem):fillTemplate(listSchema.stripeColor,listItem);
        var stripeColor = self.propAsFuncOrValue((listSchema.stripeColor||listSchema.stripecolor),listItem);
        var stripeHTML ='';
        var stripeTitle = '';
        if(stripeColor){
            if(typeof stripeColor != "object"){
                stripeColor = {
                    color:stripeColor
                }
            }
            if(stripeColor.title || stripeColor.name){
               stripeTitle = ' title="'+(stripeColor.title || stripeColor.name)+'" ';
            }
            stripeHTML = 'style="background-color:'+stripeColor.color+';" '+stripeTitle;
        }

    //add background color
        var bgColor = self.propAsFuncOrValue((listSchema.bgColor||listSchema.bgcolor),listItem);
        //($.type(listSchema.bgColor)=='function')?listSchema.bgColor(listItem):fillTemplate(listSchema.bgColor,listItem);
        var bgHTML ='';
        if(bgColor){
            if(bgColor.color){
            bgHTML = 'style="background-color:'+bgColor.color+';"';
            }else{
                bgHTML = 'style="background-color:'+bgColor+';"';
            }
        }
        var listSchemaObjIndicator = 'listView';
        var template = //getProperty('listSchema.'+listSchemaObjIndicator+'.template')
            (listSchema.listView && listSchema.listView.template) || listSchema._listTemplate;
        var title = //getProperty('listSchema.'+listSchemaObjIndicator+'.title')
            self.propAsFuncOrValue((listSchema.listView && listSchema.listView.title) || listSchema._listTitle || listSchema.listTitle,listItem);

        if(quick){
            var quicktitle = template || title || '';
            return fillTemplate(quicktitle,listItem);
        }

    //add bordercolor
        var borderColor = fillTemplate(self.propAsFuncOrValue((listSchema.borderColor||listSchema.bordercolor),listItem));
        var borderHTML ='';
        if(borderColor){
            borderHTML = 'style="border:1px solid '+borderColor+';"';
        }
    //add item number
        var numberHTML = '';
        if(index && !listSchema.hideNumbers){
            numberHTML = index;
        }

        if(!template){
			var title = title || listItem.name || id || 'untitled';

            var menu = (listSchema.listView && listSchema.listView.itemMenu)||listSchema.itemMenu;
            var listItemMenu = renderItemMenu(listItem,menu);//<div class="joe-panel-content-option-button fleft">#</div><div class="joe-panel-content-option-button fright">#</div>';

            var expander = (listSchema.listView && listSchema.listView.itemExpander)||listSchema.itemExpander;

            var listItemExpander = renderItemExpander(listItem,expander,{onDemand:listSchema.onDemandExpander,schema:listSchema.__schemaname});
            var listItemExpanderButton = self._renderExpanderButton(listItemExpander,listItem);
            var icon = //self.propAsFuncOrValue(self.getCascadingProp('icon'));
            //getProperty('listSchema.'+listSchemaObjIndicator+'.icon')
                (listSchema.listView && listSchema.listView.icon)||listSchema.icon || listSchema._icon || '';

    //CHECKBOX
            var checkbox = self.Render.itemCheckbox(listItem,listSchema,{idprop:idprop});

            var user_icon = self.propAsFuncOrValue(icon,listItem);
            var listItemIcon = (user_icon && renderIcon(user_icon,listItem)) || '';
            //list item content
            title="<div class='joe-panel-content-option-content ' "+action+">"+checkbox+title+"<div class='clear'></div></div>";

            var html = '<joe-option '+borderHTML+' class="'+(self.allSelected && 'selected' ||'')+' joe-panel-content-option trans-bgcol '+((numberHTML && 'numbered') || '' )+' joe-no-select '+((stripeColor && 'striped')||'')+' '+((listItemExpanderButton && 'expander expander-collapsed')||'')+(listItemMenu && ' item-menu'||'')+'"  data-id="'+id+'" >'

                +'<div class="joe-panel-content-option-bg" '+bgHTML+'></div>'
                +'<div class="joe-panel-content-option-stripe'+((stripeTitle && ' titled') || '')+'" '+stripeHTML+'></div>'
                +(numberHTML && '<div class="joe-panel-content-option-number" >'+numberHTML+'</div>' || numberHTML)
                +(listItem._protected && '<svg xmlns="http://www.w3.org/2000/svg" class="protected-icon" viewBox="-6 -4 36 36"><path d="M12 1C8.7 1 6 3.7 6 7L6 8C4.9 8 4 8.9 4 10L4 20C4 21.1 4.9 22 6 22L18 22C19.1 22 20 21.1 20 20L20 10C20 8.9 19.1 8 18 8L18 7C18 3.7 15.3 1 12 1ZM12 3C14.3 3 16 4.7 16 7L16 8 8 8 8 7C8 4.7 9.7 3 12 3ZM12 13C13.1 13 14 13.9 14 15 14 16.1 13.1 17 12 17 10.9 17 10 16.1 10 15 10 13.9 10.9 13 12 13Z"></path></svg>' || '')
                +listItemExpanderButton
                +listItemIcon
                +listItemMenu
                +fillTemplate(title,listItem)
                +listItemExpander
                +'</joe-option>';
		}
		//if there is a list template
		else{
            var dup = $c.duplicate(listItem);
            dup.action = action;
			html = fillTemplate(template,dup);
		}


        function renderIcon(icon,listItem){
            
            var url = icon.url || icon;
            if(url.indexOf('<svg') == -1){
            var width = (icon.width)?' width:'+icon.width+'; ':'';
            var height = (icon.height)?' height:'+icon.height+'; ':'';
            var iconURL = fillTemplate(url,listItem);
            var iconhtml = '<div style=" background-image:url(\''+iconURL+'\'); " class="joe-panel-content-option-icon trans-bgcol fleft"  >' +
                '<img style="'+width+height+'" src="'+iconURL+'"/>' +
                '</div>';
            return iconhtml;
            }else{
               var iconhtml = '<div  class="joe-panel-content-option-svg fleft"  >' +
                icon+
                '</div>'; 
                return iconhtml;
            }
        }
		return html;
	};
    this.checkItem = function(itemID,prop,schema, dom){
        //rerender field list item
        var item = self.search(itemID)[0]||false;
        var schema = schema || self.current.schema 
        || (item && self.schemas[item.itemtype]);
        var checkboxAction;
        if(schema && schema.checkbox && schema.checkbox.action){
            checkboxAction = schema.checkbox.action;
        }
        if(item) {
            $(dom).toggleClass('checked');
            var overwrite = {joeUpdated : new Date()};
            var isChecked = $(dom).hasClass('checked');
            if(!checkboxAction){
                overwrite[prop] = isChecked;
            }else{
                $.extend(overwrite,(checkboxAction(item,isChecked,$(dom))||{}))
            }

            $.extend(item,overwrite);
            schema.callback(item);
            window.event && window.event.stopPropagation();
        }
    };
	this.shiftSelecting = false;
    var goBackListIndex;
	this.listItemClickHandler= function(specs){
        /*|{
        featured:true,
         tags:'list,handler',
         description:'the default function called when a list item is clicked.'
         }|*/
        goBackListIndex = null;
        if(specs && specs.dom){//store index
            goBackListIndex = $(specs.dom).parents('.joe-panel-content-option').index();
            //logit('clicked index: '+goBackListIndex);
        }
		self.current.selectedListItems = [];
		if(!window.event){//firefox fix
			self.editObjectFromList(specs);
		}
		else if(!window.event.shiftKey && !window.event.ctrlKey){
			self.editObjectFromList(specs);
		}else if(window.event.ctrlKey){

            if($(specs.dom).hasClass('joe-panel-content-option')){
                $(specs.dom).toggleClass('selected');
            }else{
                $(specs.dom).parents('.joe-panel-content-option').toggleClass('selected');
            }

			$('.joe-panel-content-option.selected').map(function(i,listitem){
				self.current.selectedListItems.push($(listitem).data('id'));
			});

        /*    if(!window.event.shiftKey){
                self.shiftSelecting = false;
            }*/

		}else if(window.event.shiftKey){
            var shiftIndex;
            var domItem;
            if($(specs.dom).hasClass('joe-panel-content-option')){
               /* if(!self.shiftSelecting){
                    $(specs.dom).toggleClass('selected');}
                else{

                }*/
                $(specs.dom).addClass('selected');
                shiftIndex = $(specs.dom).index();

            }else{
                $(specs.dom).parents('.joe-panel-content-option').addClass('selected');
                shiftIndex = $(specs.dom).parents('.joe-panel-content-option').index();
               /* if($(specs.dom).parents('.joe-panel-content-option').hasClass('selected')) {
                    shiftIndex = $(specs.dom).parents('.joe-panel-content-option').index();
                }*/
            }

            if(self.shiftSelecting !== false){
                if(shiftIndex < self.shiftSelecting){
                    var t = shiftIndex;
                    var p = self.shiftSelecting;
                    self.shiftSelecting = t;
                    shiftIndex = p;
                }
                $('.joe-panel-content-option').slice(self.shiftSelecting,shiftIndex+1).addClass('selected');
                $('.joe-panel-content-option').slice(shiftIndex+1).removeClass('selected');
                $('.joe-panel-content-option').slice(0,self.shiftSelecting).removeClass('selected');
            }
            else{
                self.shiftSelecting = shiftIndex;
            }



            $('.joe-panel-content-option.selected').map(function(i,listitem){
                self.current.selectedListItems.push($(listitem).data('id'));
            })

        }


        self.updateSelectionVisuals();

	};

    this.updateSelectionVisuals=function(){

        if(self.current.selectedListItems.length){
          //  $(specs.dom).parents('.joe-overlay-panel').addClass('multi-edit');
            self.overlay.addClass('multi-edit');
            self.overlay.find('.joe-selection-indicator').html(self.current.selectedListItems.length +' selected');
        }else{
          //  $(specs.dom).parents('.joe-overlay-panel').removeClass('multi-edit');
            self.overlay.removeClass('multi-edit');
            self.overlay.find('.joe-selection-indicator').html('');
        }
    };

	this.editObjectFromList = function(specs){
        /*|{
            tags:'list,default',
            description:'the default function called by the list item click handler, overwritten by listAction'
        }|*/
		specs = specs || {};

		self.current.schema = specs.schema || self.current.schema || null;
		var list = specs.list || self.current.list;
		var id = specs.id;
		var idprop = specs.idprop || self.getIDProp();//(self.current.schema && self.current.schema._listID) || 'id';

		var object = list.filter(function(li){return li[idprop] == id;})[0]||false;

		if(!object){
			alert('error finding object');
			return;
		}

		var setts ={schema:specs.schema||self.current.schema,callback:specs.callback};
		/*self.populateFramework(object,setts);
		self.overlay.addClass('active');*/
		goJoe(object,setts);
	};
    this.showList = function(view,subset,filter){
        if($.type(view) == "string"){
            view = {collection:view,schema:view};
        }
        view.subset = subset || null;
        var showJoeListBenchmarker = new Benchmarker();
        self.current.clear();
        var dataList = self.Data[view.collection]||[];//NPC.kovm.Data[view.collection]();
        if(filter){
            dataList = dataList.where(filter);
        }
        goJoe(dataList,{schema:view.schema,subset:view.subset});

        _bmResponse(showJoeListBenchmarker,'Joe View: "'+view.collection+'" shown');
    };
/*----------------------------->
	List Multi Select
<-----------------------------*/
	this.editMultiple = function(){
		//create object from shared properties of multiple
		var haystack = self.current.list;
		var idprop = self.getIDProp();
		var needles = self.current.selectedListItems;

		var items = [];
		var protoItem ={};
		haystack.map(function(i){

			if(needles.indexOf(i[idprop]) == -1){//not selected
				return;
			}else{
				$.extend(protoItem,i);
				items.push(i);
			}
		});
		goJoe(protoItem,{title:'Multi-Edit '+(self.current.schema.__schemaname||'')+': '+items.length+' items',schema:(self.current.schema||null),multiedit:true});
	};
/*----------------------------->
	List Filtering
<-----------------------------*/
	this.filterList = function(list,props,specs){
		//var list = list || self.current.list;
		specs = specs || {};
		var arr = list || self.current.list;
		var found = arr.filter(function(arrobj){
			for(var p in props){
				if(arrobj[p] != props[p]) {return false;}
			}
			return true;
		});
		if(found.length){
			if(specs.single){
				return found[0];
			}
			return found;
		}else{
			return false;
		}

	};
/*----------------------------->
	List Subsets
<-----------------------------*/
	this.renderSubsetselector = function(specs){
		if(!listMode){
			return '';
		}
		var html=
		'<div class="joe-subset-selector" >'
			+self.renderSubsetSelectorOptions(specs)
		+'</div>';
		return html;
	};

	this.renderSubsetSelectorOptions = function(specs){
		var subsets = self.current.subsets;
		if(typeof subsets == 'function'){
			subsets = subsets();
		}
		function renderOption(opt){
            var html = '';
            //if(!opt.condition || (typeof opt.condition == 'function' && opt.condition(self.current.object)) || (typeof opt.condition != 'function' && opt.condition)) {
                if(!opt.condition || self.propAsFuncOrValue(opt.condition)){
                var html = '<div class="selector-option" onclick="getJoe(' + self.joe_index + ').selectSubset(\'' + (opt.id || opt.name || '') + '\');">' + opt.name + '</div>';
            }
			return html;
		}
		if(self.current.specs.subset){
			self.current.subset = subsets.filter(function(s){
				s.id = s.id || s.name;
				return s.id == self.current.specs.subset;
			})[0]||false;
		}
		var subsetlabel = (self.current.subset && self.current.subset.name) || 'All';
		var html=
		'<div class="selector-label selector-option" onclick="$(this).parent().toggleClass(\'active\')">'+subsetlabel+'</div>'
		+'<div class="selector-options">'
			+renderOption({name:'All',filter:{}});
			subsets.map(function(s){html += renderOption(s);});
		html+='</div>';
		return html;
	};

	this.selectSubset=function(subset){
		//if (!e) var e = window.event;
		//e.cancelBubble = true;
		//if (e.stopPropagation) e.stopPropagation();
        self.hide();
        var schemaname = self.current.schema && self.current.schema.name;
		goJoe(self.current.list,
    //        $c.merge(self.current.userSpecs,{subset:subset})
            $.extend({},self.current.userSpecs,{subset:subset,schema:schemaname})

        );
	};
    this.toggleFilter = function(filtername,dom){
        /*|{
            description:'toggles a filter on or off, takes a filtername and dom',
            tags:'filter'
        }|*/
        if(parseFloat(filtername) == filtername){
        	filtername = parseFloat(filtername);
        }
        var filter = ((self.current.schema && self.propAsFuncOrValue(self.current.schema.filters))||[])
                .where({$or:[{name:filtername},{id:filtername}]})[0] || false;
        if(!filter){
            logit('issue finding filter: '+filtername);
            return;
        }
        if(self.current.schema && self.current.schema.__schemaname){
            filter.schema = self.current.schema.__schemaname;
        }
        if(self.current.filters[filtername]){
            delete self.current.filters[filtername];
        }else{
            self.current.filters[filtername] = filter;
        }
        if(dom){$(dom).toggleClass('active')}
        self.filterListFromSubmenu(null,true);
    };
    this.toggleFilterObject = function(filtername,filterObj,wait){
        /*|{
         description:'toggles a filter on or off, takes a filtername filterobject and whether or not to do it now',
         tags:'filter,custom',
         featured:true
        }|*/
        if(parseFloat(filtername) == filtername){
        	filtername = parseFloat(filtername);
        }
        var filter = self.propAsFuncOrValue(filterObj);
        if(!filter){
            logit('issue finding filter: '+filtername);
            return;
        }
        if(self.current.schema && self.current.schema.__schemaname){
            filter.schema = self.current.schema.__schemaname;
        }
        if(self.current.filters[filtername]){
            delete self.current.filters[filtername];
        }
        self.current.filters[filtername] = {name:filtername,filter:filterObj};

        self.filterListFromSubmenu(null,!wait);
    };

    this.clearFilters = function(){
        /*|{
         description:'clears all filters',
         tags:'filter'
         }|*/
        self.current.filters = {};
        self.container.find('.joe-filter-option').removeClass('active');
        self.filterListFromSubmenu(null,true);
    };
/*-------------------------------------------------------------------->
	5 | HTML Renderings
<--------------------------------------------------------------------*/
	this.replaceRendering = function(dom,specs){
		var rendering = dom.toString();
		//var data = {rendering:html};
		var specs = {datatype:'rendering', compact:false,dom:dom};
		self.show(rendering,specs);
	};

	this.updateRendering = function(dom, callback){
		var callback = self.current.callback || (self.current.schema && self.current.schema.callback) || logit;
		if(!self.current.specs.dom){
			return false;
		}
		var newVal = $('.joe-rendering-field').val();
		$(self.current.specs.dom).replaceWith(newVal);
		logit('dom updated');
		self.hide();
		callback(newVal);
	};
/*-------------------------------------------------------------------->
	MENUS
<--------------------------------------------------------------------*/

/*-------------------------------------------------------------------->
	MUTATE - Adding Properties
<--------------------------------------------------------------------*/
	this.showPropertyEditor = function(prop){
		self.current.mutant = prop;


	};

	this.addPropertyToEditor = function(prop){

	};

	this.minis = {};
/*-------------------------------------------------------------------->
	MINIJOE WIndow
<-------------------------------------------------------------------*/
	this.showMiniJoe = function(specs,joespecs,data){
        /*|{
         featured:true,
         tags:'mini, show',
         description:'Shows a miniature joe from specs.(props||list||object||content)',
         specs:'mode,(object||list||content||props),(minimenu||menu),callback, title'
         }|*/

		var mini = {};
        specs = $.extend({mode:'text'},
            (specs || {}),(joespecs||{}));
        var object =specs.props || specs.object ||specs.list||specs.content;
		if(!object){
			return;
		}

		var title=specs.title || 'Object Focus';
		//mini.name=specs.prop.name||specs.prop.id || specs.prop._id;
		mini.id = cuid();

		//var html = '<div class="joe-mini-panel joe-panel">';
        var mode = specs.mode || 'object';
        specs.object = object;

        specs.minimenu = specs.minimenu|| specs.menu;
        switch($.type(object)){
            case 'object':
                mode ='object';

            break;
            case 'array':
                mode ='list';
            break;
        }
        specs.mode = mode;
        var minimode = mini.id;
        specs.minimode = minimode;
        var html =
            self.renderEditorHeader({title: title, minimode:minimode,
                close_action: 'onclick="getJoe(' + self.joe_index + ').hideMini(\''+minimode+'\');"'});

            html += self.renderMiniEditorContent(specs);
            html += self.renderEditorFooter({minimenu: specs.menu,minimode:minimode});

        var height = specs.height ||self.overlay.height()/2;
        mini.panel = self.overlay.find(self.Mini.selector);
        if(specs.name){

        }
        mini.panel.attr('name',specs.name || mini.id);
        mini.panel.addClass('active').addClass('focus').html(html);
        if(!$('.joe-mini-panel .joe-panel-footer').find('.joe-button:visible').length){
            $('.joe-mini-panel').addClass('no-footer-menu');
        }
        mini.panel.css('height',height);

        if(specs.width){
            mini.panel.css('width', specs.width);
        }

        mini.panel.draggable({handle:'joe-panel-header',snap:'.joe-overlay'}).resizable({handles:'s'});

        mini.callback = specs.callback || function(itemid){
            alert(itemid);
        };

        mini.data = data || {};
		    self.minis[mini.id] = mini;
        self.Mini.id = mini.id;
        self.overlay.toggleClass('mini-active',true);
	};

	this.hideMini = function(miniid){
        /*|{
            description:'hides the current joes mini.',
            tags:'mini, hide'
        }|*/
        try {
            if(miniid){
                delete self.minis[miniid];
                $(self.Mini.selector + '.ui-resizable').resizable('destroy').draggable('destroy');
                $(self.Mini.selector).removeClass('active');
            }else {
                $(self.Mini.selector + '.ui-resizable').resizable('destroy').draggable('destroy');
                $(self.Mini.selector).removeClass('active');
            }
        }catch(e){
            //warn(e);
            self.Error.add('hidemini error: '+e,e,{warning:true})
        }
        self.overlay.removeClass('mini-active');

	};

	this.constructObjectFromMiniFields = function(){
        /*|{
            featured:true,
            description:'constructs object from the current minis fields',
            tags:'mini, construct'
         }|*/
		var object = {};
		var prop;
		$('.joe-mini-panel.active .joe-object-field').find('.joe-field').each(function(){

			switch($(this).attr('type')){
				case 'checkbox':
					prop = $(this).attr('name');
					if($(this).is(':checked')){
						object[prop] = true;
					}else{
						object[prop] = false;

					}
				break;
				case 'text':
				default:
					prop = $(this).attr('name');
					object[prop] = $(this).val();
				break;
			}
		});
		return object;
	};

    this.renderMiniEditorContent = function(specs){

        self.current.sidebars = {left:{collapsed:false},right:{collapsed:false}};
        //specs = specs || {};
        var content;

        if(!specs){
            specs = {
                mode:'text', //text,list,single
                text:'No object or list selected'
            };
        }
        var mode = specs.mode;

        switch(mode){
            case 'text':
                content = self.renderTextContent(specs);
                break;
            case 'rendering':
                content = self.renderHTMLContent(specs);
                break;
            case 'list':
                content = self.renderListContent(specs);
                break;
            case 'object':
                content = self.renderObjectContent(specs);
                break;

            default:
                content = content || '';
                break;

        }
        var submenu = '';
        if(!specs.minimode) {
            if ((mode == 'list' && self.current.submenu) || (self.current.submenu || renderSectionAnchors().count)) {
                submenu = ' with-submenu '
            }
        }
        //submenu=(self.current.submenu || renderSectionAnchors().count)?' with-submenu ':'';
        var scroll = 'onscroll="getJoe('+self.joe_index+').onListContentScroll(this);"';
        var rightC = content.right||'';
        var leftC = content.left||'';
        var content_class='joe-panel-content '
        //+(self.specs.useInset &&'joe-inset '||'') 
        +renderEditorStyles()
        +submenu;
        var html =


            '<joe-panel-content class="'+content_class+'" ' +((listMode && scroll)||'')+'>'
            +(content.main||content)
            +'</joe-panel-content>'
            +self.renderSideBar('left',leftC,{css:submenu})
            +self.renderSideBar('right',rightC,{css:submenu});
        self.current.sidebars.left.content = leftC;
        self.current.sidebars.right.content = rightC;
        return html;
    };

    this.Mini = {
        selector:'.joe-mini-panel',
        show:self.showMiniJoe,
        hide:self.hideMini,
        construct:self.constructObjectFromMiniFields,
        get:function(){
            /*|{
             featured:true,
             description:'get the current joe mini as an object',
             tags:'mini, get'
             }|*/
            return self.minis[self.Mini.id]||false;
        },
        clear:function(){
            /*|{
             featured:true,
             description:'clear the current mini',
             tags:'mini, clear'
             }|*/
            self.hideMini();
            self.minis = {};
            delete self.Mini.id;

        }
    };
/*-------------------------------------------------------------------->
  D | DATA
 <--------------------------------------------------------------------*/
    self.quickAdd = function(itemtype){
        /*|{
         featured:true,
         tags:'data,search,quick,find',
         description:'Opens a mini joe with a list of datasets that can be created from'
         }|*/
         var itemtype = itemtype;
         if(itemtype !== false){
            
            itemtype = itemtype || (self.current.schema && self.current.schema.__schemaname);
         }
         if(itemtype){
            self.createObject(itemtype);
         }else{
             var t = []; 
             var sch;
             for (var d in self.Data){
                sch = _joe.schemas[d];
                if(sch){
                    t.push({name:sch.name,info:sch.info,menuicon:sch.menuicon,default_schema:sch.default_schema||false}) 
                }
            }

            t.sortBy('name');
            
            _joe.showMiniJoe({
                title:'Create a new:',
                list:t,
                template:
                    '<joe-schema-icon>${menuicon}</joe-schema-icon>'+
                    '<joe-title>${name}</joe-title>'+
                    '<joe-subtitle>${info}</joe-subtitle>',
                idprop:'name',
                sorter:'default_schema,name',
                menu:[],
                callback:function(item_id){
                    self.createObject(item_id);
                }
            })
         }
    }
    function getDefaultSchema(schema){
        var menu = [];
        if(__jsu && ['super','admin','editor'].indexOf(__jsu.role) != -1 
        || $c.isEmpty(self.Data.user)){
            menu = [
                __exportBtn__,
                _joe.SERVER.History.button,
                __quicksaveBtn__,
                __deleteBtn__,
                __duplicateBtn__
            ];
        }
        return{
            subtitle:'${_subsetName}',
            callback:self.SERVER.save,
            onsave:function(item){
                let current = _jco(true);
                if(/*item.status && */current && item[self.getIDProp(item.itemtype)] == current._id){
                   
                    self.Fields.rerender('status,updated,timestamp');
                }
                
            },
            new:function(){return {
                _id:cuid(),
                itemtype:schema,
                created:(new Date).toISOString()
            };},
            menu:menu
        }
    }
    self.schemas.search = {
            title:'Search Results',
            name:'search',
            __schemaname:'search',
            listTitle:function(item){
                var html = 
                '<joe-full-right>'
                    +'<joe-subtext>updated</joe-subtext>'
                    +'<joe-subtitle>'+self.Utils.toDateString(item.joeUpdated)+'</joe-subtitle>'
                    +'</joe-full-right>'
                +(_joe.schemas[item.itemtype].menuicon && '<joe-icon class="icon-40 icon-grey fleft">'+_joe.schemas[item.itemtype].menuicon+'</joe-icon>' || '')
                +'<joe-subtext>${itemtype}</joe-subtext>'
                +'<joe-title>${name}</joe-title><joe-subtitle>${info}</joe-subtitle>'
                +'<joe-subtext>${_id}</joe-subtext>';

                return html;
            },
            listmenu:[],
            sorter:['name',{name:'updated',value:'!joeUpdated'},'itemtype'],
            dataset:function(){
                return _joe.search({name:/[a-z0-9 _\-\!\:]*/});
            },
            filters:function(){
                var f = [];
                __appCollections.concat().sort().map(function(s){
                    f.push({name:s,filter:{itemtype:s}});
                })
                return f;
            },
            listAction:function(item){
                return 'goJoeItem(\''+item._id+'\',\''+item.itemtype+'\')';
            //    return 'goJoe(_joe.search(\''+item._id+'\')[0],{schema:\''+item.itemtype+'\'});';
            }
        };
    self.quickFind = function(phrase){
        /*|{
         featured:true,
         tags:'data,search,quick,find',
         description:'Pops up list of all data items to quickly search against.'
         }|*/
        
        goJoe(_joe.search({name:/[a-z0-9 _\-\!\:]*/}),{schema:'search'},function(){
            self.panel.find('.joe-submenu-search-field').focus();
        });
        
    }
    self.search = function(id,specs){
        /*|{
         featured:true,
         tags:'data,search',
         description:'Finds any item in JOE by name or idm can specify additional search params'
         }|*/
        var specs = specs || {};
        var collections = specs.collections || '';
        var properties = specs.props || specs.properties ||['_id','id','name'];
        //var idprop = specs.idprop || '_id';
      var haystack =[];
        for(var d in self.Data){
            haystack = haystack.concat(self.Data[d]);
        }
        var results;
        if($.type(id) == "string"){
            results = haystack.filter(function(r){

                return r['_id'] == id;
            })
        }else if($.type(id) == "object"){
            results = haystack.where(id);
        }
        return results;
    };
    
    self.addDataset = function(name,values,specs){
        /*|{
            featured:true,
            tags:'data',
            description:'Adds a dataset to JOE'
        }|*/
        //idprop
        //concatenate
        var values = self.propAsFuncOrValue(values,name);
        if(name && values && $.type(values) == "array"){
            self.Data[name] = values;
            var idprop = (self.schemas[name] && self.schemas[name].idprop) || '_id';
            self.Data[name].map(dataitem=>{
                self.Indexes._add(idprop,dataitem);
            })
            
            if(!self.schemas[name]){
                return;
            }

            if(self.schemas[name].onload){
                try{
                    console.log(name+ ' onload')
                self.schemas[name].onload(self.Data[name]);
                }catch(e){
                    self.Error.add('schema onload error: '+name,e);
                }
            }

        
        }


    };
    self.deleteDataset = function(dataset){
        delete self.Data[dataset];
    };
    self.dit = 0;
    self.getData = function(specs) {
        /*|{
            featured:true,
            description:'gets a list of data objects, filterable sortable, limitable',
            tags:'data, getter'
        }|*/
        var st = new Date().getTime();
        var specs = specs ||{};
        var objects = [];
        if(specs.schemas){
            specs.schemas.map(sc=>{
                objects = objects.concat(self.Data[sc]);
            })
        }else{
            for(var d in self.Data){
                objects = objects.concat(self.Data[d]);
            }
        }
        var query = specs.query || specs.filters;
        if(query){
            objects = objects.where(self.propAsFuncOrValue(query));
        }
        var sortBy = specs.sort || specs.sortBy;
        if(sortBy){
            objects = objects.sortBy(sortBy);
        }
        if(specs.limit){
            objects = objects.slice(0,specs.limit);
        }
        return objects;
    };
    self.getDataItem = function(id,datatype,idprop) {
        var st = new Date().getTime();
        function addtoDIT(){
            var el = new Date().getTime() - st;
            self.dit += el;
        }
        /*|{
            featured:true,
            description:'gets a single data item from a data collection',
            tags:'data, getter'
        }|*/
        var item;
        var idprop = idprop || (self.schemas[datatype] && self.schemas[datatype].idprop) || '_id';

        if(self.Indexes[idprop] && self.Indexes[idprop][id]){
            addtoDIT();
            self.Indexes._usage++;
            return self.Indexes[idprop][id];
        }
        var dataset = self.Data[datatype];
        if(!self.Data[datatype]){
            addtoDIT();
            return false;
        }
        for(var i = 0, tot =dataset.length; i < tot; i++){
            item = dataset[i];
            if(item[idprop] == id){
                addtoDIT();
                return item;
            }
        }
        addtoDIT();
        return false;
    };
    self.getDataset = function(datatype,specs) {
        specs = specs || {};
        var sortby = specs.sortby || 'name';

        if (self.Data[datatype]) {
            var data = self.Data[datatype].sortBy(sortby);
        }else{
            if(specs.boolean = true){
                return false;
            }
            var data =[];
        }
        if(specs.filter){
            data = data.where(specs.filter);
        }
        if(specs.reverse){
            data.reverse();
        }

        if(specs.blank){
            return [{name:'',val:''}].concat(data);
        }
        return data;
    };
    self.getDataItemProp = function(id,dataset,prop,specs){
        var specs = specs || {
            delimiter:','
        };
        prop = prop || 'name';
        if(!id){
            return '';
        }

        var vals = '';
        if(!$c.isArray(id)){
            id = [id];
        }
        id.map(function(item_id,i){
            var item = self.getDataItem(item_id,dataset);
            if(i > 0){
                vals+=specs.delimiter||'';
            }
            if(item){
                vals += item[prop];
            }
        })
        return vals;
    };

/*-------------------------------------------------------------------->
	SCHEMAS
<--------------------------------------------------------------------*/
	this.setSchema = function(schemaName){
		if(!schemaName){return false;}
		//setup schema
		var schema = ($.type(schemaName) == 'object')? schemaName : self.schemas[schemaName] || null;
        //clear filters, subsets and keyword
        if(schema  && self.current.schema && self.current.schema.name && self.current.schema.name != schema.name){
            if(listMode && self.current.schema && self.current.schema.name == 'search'){

            }else if(!listMode && schema && schema.name == 'search'){
                //going from 
            }
            else{
                self.current.keyword = '';
            }
        }
		self.current.schema = schema;
        return schema;
	};
	this.resetSchema = function(schemaName){
		var newObj = self.constructObjectFromFields(self.joe_index);
		//var obj = $.extend(self.current.object,newObj);
		self.show(
			$.extend(self.current.object,newObj),
			$c.merge(self.current.userSpecs,{noHistory:true,schema:self.setSchema(schemaName) || self.current.schema})
		)

	};

    this.Schema = {
        add:function(name,config,specs){
            self.schemas[name]=self.propAsFuncOrValue(config);
            var schemaObj = self.schemas[name];
            schemaObj.__schemaname = schemaObj.name = name;
            schemaObj._id = cuid();
            return schemaObj;
        },
        set:this.setSchema,
        reset:this.resetSchema
    };
    
/*-------------------------------------------------------------------->
	I | INTERACTIONS
<--------------------------------------------------------------------*/
	this.toggleOverlay = function(dom){
		$(dom).parents('.joe-overlay').toggleClass('active');
	};
    this.showItem = function(id,schemaname,specs,show_callback){
        if(schemaname){
        var specs = specs || {};
        var item = self.getDataItem(id,schemaname);
        specs = $.extend(specs,{schema:schemaname});
        self.show(item,specs,show_callback);
        }
    }
	this.show = function(data,specs,show_callback){
        /*|{
            featured:true,
            description:'Populates and shows the joe editor, takes full configuration overides.',
            specs:'compact',
            alias:'goJoe()',
            params:'data,specs,show_callback'
         }|*/
         var data = data || '';
         //profile = profile || null
         var specs=specs || {};
         if(!specs.force && !self.checkChanges()){
             return;
         }
        self.setEditingHashLink(true);
        self.current.benchmarkers = {};
        self.showBM = new Benchmarker();
        clearTimeout(self.hideTimeout);
    //handle transition animations.
		/*self.overlay.removeClass('fade-out');
        if(!self.overlay.hasClass('active')){
            self.overlay.
        }
        self.overlay.addClass('fade-in');
        setTimeout(function(){
            self.overlay.removeClass('fade-in');
        },500);*/


        if(specs.container){
            self.container = $(specs.container);
        }

		self.panel.attr('class', 'joe-overlay-panel');
		if(specs.compact === true){self.overlay.addClass('compact');}
		if(specs.compact === false){self.overlay.removeClass('compact');}

		self.populateFramework(data,specs);

        self.overlay.removeClass('hidden');
		self.overlay.addClass('active');
        self.current.show_callback = show_callback;
		setTimeout(self.onPanelShow,0);
/*        var evt = new Event('JoeEvent');
        evt.initEvent("showJoe",true,true);

// custom param
        evt.schemaname = self.current.schema.__schemaname;
        document.addEventListener("myEvent",function(){alert('finished')},false);
        document.dispatchEvent(evt);*/


        $(self.container).trigger({
            type: "showJoe",
            schema: self.current.specs.schema,
            subset: self.current.specs.subset
        });

	};
	this.hide = function(timeout){
        timeout = timeout || 0;
       // self.overlay.removeClass('fade-in');
        self.overlay.addClass('hidden');
        self.hideTimeout = setTimeout(function(){
            self.overlay.removeClass('active');

            self.overlay.removeClass('hidden');
            //self.overlay.removeClass('fade-out');
        },timeout);
	};

    this.reload = function(hideMessage,specs){
        /*|{
         featured:true,
         description:'Reloads the current window. During reload, the joe._reload flag is set to true.',
         tags:'populate, framework,reload'
         }|*/
        logit('reloading joe '+self.joe_index);
        self._reloading = true;
        var specs = specs || {};
        var reloadBM = new Benchmarker();
        var info = self.history.pop();
        if($.type(info.data)== 'object') {
            if(specs.overwrite || specs.overwrites){
                var obj = $.extend({}, info.data, (specs.overwrite || specs.overwrites));
            }else{
                obj = info.data;
            }
        }else{
            var obj = info.data;
        }

        //delete data overwirtes
        delete specs.overwrite;
        delete specs.overwrites;
        var specs = $.extend({},info.specs,specs);
        self.show(obj,specs);
        if(!hideMessage){self.showMessage('reloaded in '+reloadBM.stop()+' secs');}
        self._reloading = false;
    };


	this.compactMode = function(compact){
		if(compact === true){self.overlay.addClass('compact');}
		if(compact === false){self.overlay.removeClass('compact');}


	};

	this.printObject = function(obj){
		goJoe('<pre>'+JSON.stringify(obj,'  ','    ')+'</pre>');
	};

	if(self.joe_index == 0){
		window.joeMini = this.showMiniJoe;
		window.goJoe = this.show;
        window.goJoeItem = this.showItem;
		window.listJoe = this.editObjectFromList;
    }
    window.$J = window.$J || {
        _usages:{get:0,schema:0,search:0},
        get : function(itemID,callback){
            $J._usages.get++;
            var item = self.getDataItem(itemID);
            if(item){
                callback && callback(item);
                return item;
            }
        },
        
        schema : function(schemaname,callback){
            $J._usages.schema++;
            var schema = _joe.schemas[schemaname];
            if(schema){
                callback && callback(schema);
                return schema;
            }
        } ,
        search:function(query,callback){
            $J._usages.search++;
            var results = _joe.search(query)
            callback && callback(results);
            return results;
        }
    }

	window.getJoe = function(index){
		return (window._joes[index] || false)
	};
	$(document).keyup(function(e) {
        var mode = self.getMode();
        if (e.keyCode == 27) {//esc key
            if(mode == "list"){
                self.deselectAllItems();
            }else if(mode == "details"){
                if($('.joe-text-autocomplete.active').length){//close autocoomplete first
                    $('.joe-text-autocomplete.active').removeClass('active');
                }
            }
            if(self.specs.useEscapeKey){
                self.closeButtonAction();
            }
            
        }   // esc
        //TODO:if the joe is also current main joe.
	//ctrl + enter
		else if(e.ctrlKey && e.keyCode == 13 && self.specs.useControlEnter){
			self.overlay.find('.joe-confirm-button').click();
		}
	});

    var sortable_index;
    this.Autosave = {
        possibleChanges:true,
        activate:function(){
            if(!self.specs.autosave){
                return;
            }
            

            // document.addEventListener('blur', function(e){
            //     if(["INPUT","SELECT","TEXTAREA","CHECKBOX"].indexOf(e.target.tagName)!= -1){
            //         self.Autosave.possibleChanges = true;
            //         console.log(e.target.tagName+' blurred')
            //     }
            // },true)
            // $('.joe-object-field').on('click',function(e){
            //     console.log(e.target.dataset().name+' clicked')
            //     possibleChanges = true;
            //     })
            var intercnt = 1000;
            if (typeof self.specs.autosave == "number"){
                intercnt = self.specs.autosave;
            }
            self.Autosave.interval = setInterval(function(){
                    if(!self.Autosave.possibleChanges){
                        return;
                    }
                    var hasChanged = self.checkChanges(true);
                    //document.title = document.title.replace('*','');
                    var showUnsaved = (hasChanged && self.getMode() == "details");
                    document.title =( showUnsaved && '*' ||'')
                        +document.title.replace('*','');
                        self.overlay.toggleClass('unsaved-changes',showUnsaved)
                },intercnt)
        },
        deactivate:function(){
            if(!self.specs.autosave){
                return;
            }
            //document.removeEventListener('blur');
            clearInterval(self.Autosave.interval);
        }
    }
	this.onPanelShow = function(){
        /*|{
            description:'function that initializes any js interactions and calls onPanelShow when it completes',
            tags:'show'
        }|*/
        var BM = new Benchmarker();
        self.respond();

        self.Autosave.activate();
        
        //ready datepicker
        self.Fields.date.ready();
        //ready timepicker
        self.Fields.time.ready();


        //itemcount
        if(currentListItems && self.overlay.find('.joe-submenu-search-field').length){
            if(goingBackQuery  || (!$c.isEmpty(self.current.userSpecs.filters) && listMode) ){
                self.overlay.find('.joe-submenu-search-field').val(goingBackQuery);
                self.filterListFromSubmenu(self.overlay.find('.joe-submenu-search-field')[0].value,true);
                goingBackQuery = '';
            }

            self.overlay.find('.joe-submenu-itemcount').html(currentListItems.length+' item'+((currentListItems.length != 1 &&'s') ||''));

        }
        if(!listMode){
            if(self.isNewItem()){
                $(".joe-object-field.text-field").eq(0).find('input').focus();
            }
        }
        //ready multisorter
        self.readyBinFields();


        //preview
        $('.joe-preview-iframe-holder').resizable({handles:'s'});
        //imagefield
		self.overlay.find('input.joe-image-field').each(function(){_joe.updateImageFieldImage(this);});
        //object reference
        self.overlay.find('.joe-object-references-holder.sortable').sortable({handle:'.joe-field-item-content'});
	    // self.overlay.find('.joe-objectlist-table.sortable').each(function(){
        //     $(this).find('tbody').sortable(
        //         {   axis:'y',
        //         handle:'.joe-objectlist-object-row-handle',
        //         helper:olHelper
        //         }
        //     )

        // });
        
         //ready objectlist
         self.Fields.objectlist.ready();


        //sidebars
        if(self.current.sidebars){
            var lsb = self.current.sidebars.left;
            var rsb = self.current.sidebars.right;

            self.panel.find('.joe-sidebar_left-button').toggleClass('active',lsb.content != '');
            self.panel.find('.joe-sidebar_right-button').toggleClass('active',rsb.content != '');
            if(self.sizeClass != 'small-size'){
                self.panel.toggleClass('right-sidebar',(rsb.content && !rsb.collapsed || false));
            }
            self.panel.toggleClass('left-sidebar',(lsb.content && !lsb.collapsed || false));
        }
        //self.toggleSidebar('right',false);

        //ready comments
        self.Fields.comments.ready();

        //ready uploaders
        self.readyUploaders();

        //ready editors|
        self.readyTextEditors();

        //minis
        self.Mini.clear();

        //go back to previous item
        if(goingBackFromID){
            if(goBackListIndex != null){
                //logit('rendering until '+goBackListIndex);

                //self.overlay.find('.joe-submenu-itemcount').html(currentListItems.length+' item'+((currentListItems.length > 1 &&'s') ||''));
                if(goBackListIndex > self.specs.dynamicDisplay) {
                    self.panel.find('.joe-panel-content').html(self.renderListItems(currentListItems, 0, goBackListIndex+1));
                }
                goBackListIndex = null;
            }
            try {
                self.overlay.find('.joe-panel-content-option[data-id="' + goingBackFromID + '"]')
                    .addClass('keyboard-selected')[0].scrollIntoView(true);
            }catch(e){
                //logit('go back to item not ready yet:'+goingBackFromID);
                self.Error.add('go back to item not ready yet:'+goingBackFromID,e)
                //TODO:'render all necessary items to go back'
            }
            //self.overlay.find('.joe-panel-content').scrollTop(self.overlay.find('.joe-panel-content').scrollTop()-20);
            //$('.joe-panel-content').scrollTop($('.joe-panel-content-option.keyboard-selected').offset().top);[0].scrollIntoView(true);

            goingBackFromID = null;
        }
        self._currentListItems = currentListItems;

        if(listMode) {
            //self.overlay[0].className.replace()
            self.setColumnCount(colCount);
            self.panel.toggleClass('list-mode',listMode);
        }
        if(self.panel.hasClass('small-size') ||
            !(self.current.subsets ||
            (self.current.schema && self.propAsFuncOrValue(self.current.schema.filters)))
        ){
            //$c.isEmpty(self.current.filters)
            leftMenuShowing = false;
        }
        self.panel.toggleClass('show-filters',leftMenuShowing && listMode);
        self.panel.toggleClass('show-aggregator', rightMenuShowing && listMode);
        self.specs.speechRecognition && self.Speech.initMic();

        self.setEditingHashLink(false);
        self.current.changesConfirmed = false;
        var panelShowFunction = self.getCascadingProp('onPanelShow');
        try {
            var exectime = (new Date() - self.showBM._start) / 1000;
            var state = self.getState();
            var timing ={time:exectime,warnings:self.current.benchmarkers._warnings} ;
            
            panelShowFunction && panelShowFunction(state,timing);
            if(panelShowFunction && self.specs.onPanelShow && 
                (panelShowFunction != self.specs.onPanelShow)){
                    self.specs.onPanelShow(state,timing);
            }
            self.current.show_callback && self.current.show_callback(self.getState(),{time:exectime});
        }catch(e){
            //warn(e);
            self.Error.add(e,e,{warning:true})
        }
/*        if($(window).height() > 700) {
            self.overlay.find('.joe-submenu-search-field').focus();
        }*/
        _bmResponse(BM,'on panel show complete');
        
        self.Fields.onPanelShow();
        _bmResponse(BM,'fields onshow complete');
        
        _bmResponse(self.showBM,'----Joe Shown');
    };

    this.checkChanges = function(nopopup){
        /*|{
            tags:'changes,navigation',
            description:'Fires to ask the user if they want to verify changes, returns boolean',
            params:'nopopup:boolean[false]'
        }|*/
        var mode = self.getMode()
        if(!mode || mode == "list"){
            return true;
        }
        var cc_construct = _jco(true);
        if(self.current.schema && self.current.schema.checkChanges === false){
            return true;
        }
        if(self.current.changesConfirmed ===true){
            return true;
        }
        if(!_joe.current.object || !cc_construct){
            return true;
        }
        var changes = $c.changes(_joe.current.object,cc_construct);
        var tocheck = [];
        var check_msg = '';
        if(changes && changes.$length){
            var f,test_info;
            for(var c in changes){
                f = self.getField(c);
                

                if(c[0] != '$' && c != 'joeUpdated' && f && f.type !='content' && !parseBoolean(f.passthrough)){
                    if(JSON.stringify(_joe.current.object[c]) == JSON.stringify(cc_construct[c])){
                        continue;
                    }
                    if(['qrcode'].indexOf(f.type) != -1){

                    }
                    else if((cc_construct[c] === ""|| cc_construct[c] === null || ($c.isArray(cc_construct[c]) && !cc_construct[c].length)) 
                    && (_joe.current.object[c] === undefined || _joe.current.object[c] === null 
                    ||($c.isArray(_joe.current.object[c]) && !_joe.current.object[c].length))){
                        test_info = 'null to blank';
                    }else if((self.current.object.itemtype == "project" && c =='phases')){ 
                        //TODO HACK: remove when clark fixes
                        test_info = 'project phase';
                    }else if(cc_construct[c] === false && !_joe.current.object[c]){
                    	test_info = 'boolean false';		
                    }else if(f.type == "wysiwyg" && __removeTags(cc_construct[c]).trim() == __removeTags(_joe.current.object[c]).trim()){
                    	test_info = 'wysiwyg formatting';
                    }else if(f.type =="objectReference"){
                        test_info = "is an objectReference";
                        //compare stringified data
                        var same =  (JSON.stringify(_joe.current.object[c]) == JSON.stringify(cc_construct[c]));
                    }
                    else{
                
                        tocheck.push(c); 
                        check_msg+=c+'\n';
                    }
                }
            if(test_info)logit(test_info);
            }
            //self.current.changesConfirmed = false;
            //check adds for blanks
        }
        if(nopopup){
            $('.joe-object-field.changed').removeClass('changed');
            tocheck.map(field=>{

            })
            return (tocheck.length > 0);
        }
        if(self.overlay.hasClass('active') && !listMode && !self.current.changesConfirmed && tocheck.length){
            
            var confirmed = confirm('Are you sure you want to navigate away from this item?\nYou have unsaved changes:\n'+check_msg);
            self.current.changesConfirmed = confirmed;
            return confirmed;
        }
        return true;
    }
/*-------------------------------------------------------------------->
 J | MESSAGING
 <--------------------------------------------------------------------*/
	this.renderMessageContainer = function(){
		var mhtml = '<joe-message-holder><div class="joe-message-container left"></div></joe-message-holder>';
		return mhtml;
	};
    var messageTimeouts = Array(4);
	this.showMessage = function(message,specs){
        /*|{
            featured:true,
            tags:'message',
            description:'Shows the destiny style message on the joe panel.',
            specs:'message,timeout,message_class'
        }|*/
		var mspecs = $.extend({
			timeout:3,
			message_class:''
		},(specs||{}));
		var message = message || 'JOE Message';
		var attr = 'class';
		var transition_time = 400;
        if(self.overlay.find('.joe-message-container').hasClass('active')){
            self.overlay.find('.joe-message-container')
                .html('<div class="joe-message-content">'+message+'</div>').attr('class','joe-message-container active show-message');
        }else{
            self.overlay.find('.joe-message-container')
                .html('<div class="joe-message-content">'+message+'</div>').attr('class','joe-message-container active left');
            self.overlay.find('joe-message-holder').addClass('active');
        }
            //TODO: don't toggle hidden class if no timeout.
		var target = "getJoe("+self.joe_index+").overlay.find('.joe-message-container')";
        var holder = "getJoe("+self.joe_index+").overlay.find('joe-message-holder')";
		//setTimeout(target+".attr('class','joe-message-container active')",50);
        clearTimeout(messageTimeouts[0]);
        clearTimeout(messageTimeouts[1]);
        clearTimeout(messageTimeouts[2]);
        clearTimeout(messageTimeouts[3]);
        clearTimeout(messageTimeouts[4]);
        messageTimeouts[0] = setTimeout(target+".attr('class','joe-message-container active show-message')",transition_time);
		if(mspecs.timeout){//only hide if timer is running and active

            messageTimeouts[1] = setTimeout(target+".attr('class','joe-message-container active ')",(mspecs.timeout*1000)+transition_time-250);
            messageTimeouts[2] = setTimeout(target+".attr('class','joe-message-container active right')",(mspecs.timeout*1000)+transition_time);
            messageTimeouts[3] = setTimeout(target+".attr('class','joe-message-container'); ",(mspecs.timeout*1000)+(2*transition_time)+50);
            messageTimeouts[4] = setTimeout(holder+".removeClass('active');",(mspecs.timeout*1000)+(2*transition_time)+150);



		}

/*
		.delay(50)
			.attr(attr,'joe-message-container active')
			.delay(mspecs.timeout*1000)
			.attr(attr,'joe-message-container right')
			.delay(50)
			.attr(attr,'joe-message-container');*/
	};

/*-------------------------------------------------------------------->
	K | OBJECT
<--------------------------------------------------------------------*/
    this.Object = {};
	this.Object.create = this.createObject = function(specs,item_defaults){
        /*|{
         featured:true,
         tags:'object,create',
         description:'Default function called to create a new object. Uses the schema.new as base properties.'
         }|*/
         var specs = specs ||{};
		//takes fields to be deleted
        //var schema = self.current.schema
        var schema = self.current.schema;
        if(typeof specs == "string"){
            
            specs = {schema:specs};
        }

        schema = self.schemas[specs.schema] || self.current.schema;
        var item_defaults = item_defaults || self.propAsFuncOrValue(schema.new) || {};
		var specs = $.extend({schema:schema},(specs ||{}));
        var overwrites = self.propAsFuncOrValue(specs.overwrites) || {};
		goJoe($.extend({},item_defaults,overwrites),specs);
	};

/*    /!*-------------------------------------------------------------------->
     + | Add Items
     <--------------------------------------------------------------------*!/
    this.createItem = function(schema,defaults,title){
        var specs = {type:schema,itemtype:schema};
        var defaults = defaults||{};
        var joespecs = {schema:schema};
        if(title){
            joespecs.title = title;
        }
        goJoe($.extend(specs,defaults),joespecs)
    };*/

    function _defaultUpdateCallback(data){
        self.showMessage(data.name +' updated successfully');
    }

    this.Object.validate = this.validateObject = function(obj){
        var req_fields = [];
        var validate_fields = [];
        _joe.current.fields.filter(f=>{
            return (f.validation && typeof f.validation == "function");
        })


        //_joe.current.fields.where({required:true});
        req_fields = _joe.current.fields.filter(function reqCheck(prop) {

            //if (prop.required && (typeof prop.required != 'function' && prop.required) || (typeof prop.required == 'function' && prop.required(self.current.object))) {
            if(self.propAsFuncOrValue(prop.required)){
                return true;
            }
            return false;
        });

        function olistVal(olistObj) {
            var vals = '';
            for (var i in olistObj) {
                vals += olistObj[i];
            }
            vals.replace(/ /g, '');
            return vals;
        }

        var required_missed = [];
        req_fields.map(function (f) {
            if (!obj[f.name] ||
                ($.type(obj[f.name] == "array") && f.type != "objectList" && obj[f.name].length == 0 ) ||
                (f.type == "objectList" && obj[f.name].filter(olistVal).length == 0)
            ) {

                required_missed.push(f);
                return false;
            }
        });
        $('.joe-object-field').removeClass('joe-highlighted');
        if (required_missed.length) {
            //f.display|| f.label|| f.name
            var req_array = [];
            required_missed.map(function (of) {
                req_array.push(of.display || of.name);
                $('.joe-object-field[data-name=' + of.name + ']').addClass('joe-highlighted');
            });
            req_array = req_array.condense(true);
            self.showMessage("There are <b>" + req_array.length + "</b> required fields currently missing. <br/>"+req_array.join());//<br/>"+required_missed.join(', ')
            
            // self.panel.addClass('show-required');
            return false;
        }

        return true;
    };

    this.updateObjectAsync = function(ajaxURL,specs){

        var specs = $.extend({
            oldObj:$.extend({},self.current.object),
            callback:function(data){
                self.showMessage(data.name +' updated successfully');
            },
            ajaxObj: self.constructObjectFromFields(self.joe_index),
            overwrites:{},
            skipValidation:false
        },(specs||{}));
        var newObj = $.extend(
            ajaxObj.newObj,
            specs.overwrites,
            {joeUpdated:new Date()}
        );

        var skipVal = _joe.propAsFuncOrValue(self.skipValidation);
        if(!skipVal) {
            var valid = self.validateObject(obj);
            if(!valid){return false;}
        }

        $.ajax({
            url:specs.ajaxObj,
            data:newObj,
            dataType:'jsonp',
            success:specs.callback
        })


    };
    this.Object.findAndModify = function(id,modifications,specs,callback){
        /*|{
         featured:true,
         tags:'save,modify',
         description:'updates any currently indexed object (by id) using validation and the callback from the schema',
         specs:'schema,goto,'
         }|*/
        var specs = specs ||{};
        var modifications = $.extend({joeUpdated:new Date()},(modifications||{}))
        var schemaObj = (specs.schema && (_joe.schemas[specs.schema] || specs.schema));
        var idprop = specs.idprop 
            || (schemaObj && schemaObj.idprop)
            || '_id';
    
        var object = _joe.Indexes[idprop][id];
        if(!schemaObj && object.itemtype && _joe.schemas[object.itemtype] ){
            schemaObj = _joe.schemas[object.itemtype];
        }
        var oldObj = $.extend({},object);
        $.extend(object,modifications);


    //update UI
        function defaultCallback(data){
			self.showMessage((data.name||data.itemtype) +' modified successfully');
		}
        var callback = specs.callback || (schemaObj.schema && schemaObj.callback) || defaultCallback;
        var mode = _joe.getMode();
        if(mode == "details" && self.current.object == object){
            goJoe(self.current.object,{schema:self.current.object.itemtype,force:true})
        }else if(mode == "list" && self.current.list.indexOf(object) != -1){
            _joe.reload();
        }
        //run callback

		logit((obj.name||'object')+' updated');

        if(specs.goto){
            if(typeof specs.goto == string){
                goJoe(object,{schema:schemaObj})
            }else{
                var gotoObject = _joe.search(specs.goto)[0]||false;
                gotoObject && goJoe(gotoObject,{schema:gotoObject.itemtype})
            }
        }
        callback(obj,oldObj,oldObj.changes(obj));
    }
    this.Object.update = this.updateObject = function(dom,callback,stayOnItem,overwrites,skipValidation){
        /*|{
         featured:true,
         tags:'save',
         description:'updates the current object using validation and the callback from the schema'
         }|*/
         self.Cache.clear();
        var oldObj = $.extend({},self.current.object);
		function defaultCallback(data){
			self.showMessage(data.name +' updated successfully');
		}
		var callback = callback || self.current.callback || (self.current.schema && self.current.schema.callback) || defaultCallback; //logit;
		var newObj = self.constructObjectFromFields(self.joe_index);
		newObj.joeUpdated = new Date();
        overwrites = overwrites || {};
		var obj = $.extend(newObj,overwrites);

//check required fields()
        var skipVal = _joe.propAsFuncOrValue(skipValidation);
        if(!skipVal) {
            var valid = self.validateObject(obj);
            if(!valid){return false;}

        }
    //end validation


        obj = $.extend(self.current.object,newObj);
        var itemtype = (self.current.object.itemtype && self.Data[self.current.object.itemtype])?self.current.object.itemtype:null;
    //update object list
        var index = (self.current.list && self.current.list.indexOf(obj));
        if(self.current.list && (index == -1 || index == undefined)){
          //  object not in current list
            if(itemtype && self.current.list[0] && self.current.list[0].itemtype == itemtype ){
                //self.current.list.push(obj);
            }else if(!itemtype){
                self.current.list.push(obj);
            }
        }

    //update object in dataset
        var dsname = itemtype || self.current.schema.__schemaname;
        var idprop = self.getIDProp();
        if(self.Data[dsname]){
            if(self.getDataItem(obj[idprop],dsname)) {

            }else{
                self.Data[dsname].push(obj);
            }
            logit('item matches current schema, updating...');
        }
    //run callback

		logit((obj.name||'object')+' updated');
        callback(obj,oldObj,oldObj.changes(obj));
		if(!stayOnItem){self.goBack(obj);}

	};


	this.deleteObject = function(callback){
        var obj = self.current.object;
        if(!confirm('Are you sure you want to DELETE\n'+obj.name+'\n'+obj[self.getIDProp()])){
            return false;
        }
		var callback = self.current.callback || (self.current.schema && self.current.schema.callback) || logit;

        var deleted_prop = self.getCascadingProp('deleted') || '_deleted';
        obj[deleted_prop] = new Date().toISOString();
		if(!self.current.list || !obj || self.current.list.indexOf(obj) == -1){
		//no list or no item
			logit('object or list not found');

            callback(obj);
            self.current.changesConfirmed = true;
			self.goBack();
			return;
		}
		var index = self.current.list.indexOf(obj);

		self.current.list.removeAt(index);
		logit('object deleted');

        callback(obj);
        self.current.changesConfirmed = true;
		self.goBack();

	};

    this.Object.duplicate = this.duplicateObject = function(specs){
		//takes fields to be deleted
		specs = specs || {};
		var deletes = specs.deletes || [];
		var itemobj = $.extend({},self.current.object);
        delete itemobj[self.getIDProp()];
        delete itemobj.joeUpdated;
        delete itemobj.created;
		itemobj.name = itemobj.name +' copy';
		deletes.map(function(d){
			delete itemobj[d];
		});

		//self.goBack();
		goJoe(itemobj,self.current.userSpecs);
	};
    this.Object.construct = this.constructObjectFromFields = function(index){
        /*|{
         featured:true,
         tags:'construct',
         alias:'_jco(true)',
         description:'constructs a new object from the current JOE details view'
         }|*/
        var constructBM = new Benchmarker();
		var object = {};
        if(self.current.object){
            object.joeUpdated = self.current.object.joeUpdated;
        }
        //{joeUpdated:new Date()};joeUpdated:self.current.object.joeUpdated||new Date()
        var groups = {};
		var prop;

		//var parentFind = $('.joe-object-field');
		var parentFind = $('.joe-overlay.active');

		if(index){
		parentFind = self.overlay.find('.joe-object-field');

        }
        var fieldsToConstruct = parentFind.find('.joe-field');
        if(!fieldsToConstruct.length && self.current.object){
            return self.current.object;
        }
		//var parentFind = $('.joe-overlay.active');

		parentFind.find('.joe-field').each(function(){
            if($(this).parents('.objectList-field,.objectlist-field').length){
                return true;
            }
            if($(this).parents(_joe.Mini.selector).length){
                return true;
            }
			if(self.current.userSpecs && self.current.userSpecs.multiedit){
				if(!$(this).parents('.joe-object-field').hasClass('multi-selected')){
					return;
				}
			}
			switch($(this).attr('type')){

                case 'checkbox':
                    //IF multiple with same name, add them to array
					prop = $(this).attr('name');
                    if(groups[prop] || this.parentElement.classList.contains('joe-group-item-checkbox')){//checkbox groups
                        //$('input[name='+prop+']').length > 1){
                        groups[prop] = groups[prop] || [];
                        if ($(this).is(':checked')) {
                            groups[prop].push($(this).val());

                        }
                        object[prop] = groups[prop];
                    }else {//single checkboxes
                        if ($(this).is(':checked')) {
                            object[prop] = true;
                        } else {
                            object[prop] = false;

                        }
                    }
				break;

				case 'text':
				default:

					prop = $(this).attr('name');
					if(prop && prop != 'undefined') {//make sure it's suppoesed to be a prop field
                        switch ($(this).data('ftype')) {
                            case 'passthrough':
                                object[prop] = self.propAsFuncOrValue(object[prop]);
                                break;
                            //ace editor
                            case 'ace':
                                var editor = _joe.ace_editors[$(this).data('ace_id')];
                                //$(this).find('.ace_editor');
                                object[prop] = (editor)? editor.getValue():self.current.object[prop];
                                break;
                            case 'ckeditor':
                                var editor = CKEDITOR.instances[$(this).data('ckeditor_id')];
                                //_joe.ace_editors[];
                                //$(this).find('.ace_editor');
                                object[prop] =(editor)? editor.getData():self.current.object[prop];
                                break;
                            case 'tinymce':
                                var editor = tinymce.editors.where({id:$(this).data('texteditor_id')})[0]||false;
                                object[prop] =(editor)? editor.getContent():self.current.object[prop];
                                break;

                            case 'multisorter':
                                var vals = [];
                                $(this).find('.selections-bin').find('li').each(function () {
                                    vals.push($(this).data('id'));
                                });
                                object[prop] = vals;
                                break;
                            case 'uploader':
                                var joe_uploader = self.uploaders[$(this).data('uploader_id')];
                                object[prop] = joe_uploader.files;
                                break;
                            case 'buckets':
                                var vals = [];
                                $(this).find('.selections-bin').each(function () {
                                    vals.push([]);
                                    $(this).find('li').each(function () {
                                        vals[vals.length - 1].push(decodeValueObject(this));
                                    });
                                });
                                /*
                                 $(this).find('.selections-bin').find('li').each(function(){
                                 vals.push($(this).data('id'));
                                 });*/
                                object[prop] = vals;
                                break;
                            case 'objectReference':
                                var vals = [];
                                object[prop] = 'objectReference';
                                var obj;
                                $('.objectReference-field[data-name="'+prop+'"]')
                                    .find('.joe-field-list-item,.joe-field-item')
                                    .each(function(){
                                        var data = $(this).data();
                                        if(data.isobject){
                                            obj = JSON.parse(decodeURI($(this).data().value));
                                            vals.push(obj);
                                        }else{
                                            vals.push($(this).data().value);
                                        }
                                        //JSON.parse(decodeURI($(this).data().value))

                                    });
                                object[prop] = vals.condense();
                                break;
                            default:
                                object[prop] = $(this).val();
                                break;
                        }
                        break;
                    }
			}
            function decodeValueObject(dom) {
                var data = $(dom).data();
                if (data.isobject) {
                    obj = JSON.parse(decodeURI($(dom).data().value));
                    return obj;
                } else {
                    return $(dom).data().value;
                }
            }
		});

    //subObject Field
        ['subobject'].map(function(fieldtype){
            self.Fields[fieldtype].construct(parentFind,object);
        });

    //CONTENT FIELD
        parentFind.find('.joe-object-field.content-field').each(function() {
            prop = $(this).data('name');
            var cfield = self.getField(prop);
            if (cfield.passthrough) {
                if(cfield.passthrough === true) {
                    object[prop] = self.current.object[prop];
                }else{
                    object[prop] = self.propAsFuncOrValue(cfield.passthrough,self.current.object);
                }
            }
        });

    //OBJECT LISTS
        parentFind.find('.objectList-field').each(function(){
            var ol_property = $(this).data('name');
            var ol_array = [];
            var subVal,subInput;
        //get table header rows
            var subprops = [];
            $(this).find('thead').find('th').each(function(i){
               // if($(this).data('subprop')) {//skip handle
                    subprops.push($(this).data('subprop'));
              //  }
            });

        //parse all trs and tds and match values to subprops array
            $(this).find('tbody').find('tr').each(function(){
            //$(this).find('joe-objectlist-item').each(function(){

            //find all tds
                var ol_array_obj = {};
                $(this).find('td').each(function(i){
                    subInput = $(this).find('input,select,textarea');
                    subVal = subInput.val();
                    switch(subInput.attr('type')){
                        case 'checkbox':
                            subVal = subInput.is(':checked');
                        break;
                    }
                    ol_array_obj[subprops[i]] = subVal;
                });
            //store to ol_array
                delete ol_array_obj.undefined;
                ol_array.push(ol_array_obj);
            });


            object[ol_property] = ol_array;
        });

        function evalCheckbox(input){
            return input.is(':checked');
        }

        _bmResponse(constructBM,'----constructed',null,.01);
        var constructed = ((!$c.isEmpty(object) && object)|| false);
        return constructed;
		//return object;
	};

/*-------------------------------------------------------------------->
    EXPORT DATA
 <--------------------------------------------------------------------*/
    this.Export = {}
	
    this.Export.JSON = this.exportJSON = function(object,specs){
        var specs = specs ||{};
		var minify =specs.minify || null;
		var objvar = specs.objvar || '';
		var obobj = (minify)?JSON.stringify(object):JSON.stringify(object,'','    ');

		goJoe('<b>'+(specs.title || (objvar && 'var '+objvar +' = ')|| 'JSON Object')+'</b><br/><pre>'+obobj+'</pre>',{schema:specs.schema});
		logit(obobj);
	};
    this.Export.csv = function(schemaname,specs){
        /*|{
         featured:true,
         tags:'export,io,csv',
         description:'Export a csv of the selected schema, cuids become referned item if found.'
         }|*/
        while(!schemaname){
            schemaname = prompt('schema name (itemtype)?');
            if (schemaname === null) {
                return; //break out of the function early
            }
        }
        var schema = _joe.schemas[schemaname];
        if(!schema){
            alert('no schema "'+schemaname+'" found ');
            return;
        }
        var specs = $.extend({labels:true},
        specs||{});
        var idprop = specs.idprop|| schema.idprop || '_id';
        var arrData = _joe.Data[schemaname].sortBy('name');
        if(specs.filter){
            arrData = arrData.where(specs.filter);
        }
        var CSV = '';    
        
        CSV += schemaname + '\r\n\n';
        if (specs.labels) {
            var row = "";
            for (var index in arrData[0]) {
                
                row += index + ',';
            }

            row = row.slice(0, -1);
            CSV += row + '\r\n';
        }
        for (var i = 0; i < arrData.length; i++) {
            var row = "";
            var colval;
            for (var index in arrData[i]) {
                colval = arrData[i][index];
                if(index != idprop && $c.isCuid(colval)){
                    colval = _joe.Cache.get(colval).name
                }
                row += '"' + colval + '",';
            }

            row.slice(0, row.length - 1);
            
            CSV += row + '\r\n';
        }

        if (CSV == '') {        
            alert("Invalid data");
            return;
        }   
        var fileName = "JOE-"+$c.pluralize(schemaname)+'-'+$c.format(new Date(),'m-d-Y')+'.csv';
        
        
        var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
//        window.open(uri);

        var downloadLink = document.createElement("a");
        //var blob = new Blob(["\ufeff", csv]);
        //var url = URL.createObjectURL(uri);
        downloadLink.href = uri;
        downloadLink.download = fileName;

        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
    }


/*-------------------------------------------------------------------->
    MULTI FUNCTIONS
 <--------------------------------------------------------------------*/

	this.updateMultipleObjects = function(dom,multipleCallback, callback){
		var callback = callback || self.current.onUpdate || self.current.callback || (self.current.schema && (self.current.schema.onUpdate || self.current.schema.callback)) || logit;
		var multipleCallback = multipleCallback || self.current.onMultipleUpdate || self.current.multipleCallback
            || (self.current.schema && (self.current.schema.onMultipleUpdate || self.current.schema.multipleCallback)) || logit;
		var idprop = self.getIDProp();
		var newObj = self.constructObjectFromFields(self.joe_index);
        if(Object && Object.keys && Object.keys(newObj).length == 1 && Object.keys(newObj)[0] == "joeUpdated"){
            self.showMessage('please select at least one property to update.');
            return;
        }
		//newObj.joeUpdated = new Date();

	//clear id from merged object
		delete newObj[idprop];
		delete newObj['id'];
		delete newObj['_id'];

		var haystack = self.current.list;

		var needles = self.current.selectedListItems;
		//var items = [];
        var updatedItems = [];
		haystack.map(function(i){

			if(needles.indexOf(i[idprop]) == -1){//not selected
				return;
			}else{//make updates to items
				//protoItem = merge(protoItem,i);
				$.extend(i,newObj);
				//logit('object updated');
                //itemsUpdated++;
				callback(i);
                updatedItems.push(i);
                //self.showMessage(updatedItems.length +' item(s) updated');
			}
		});

		logit(updatedItems.length +' item(s) updated');
        multipleCallback(updatedItems);

		self.goBack();
        self.showMessage(updatedItems.length +' item(s) updated');
	};

	this.deleteMultipleObjects = function(callback){
		var callback = self.current.callback || (self.current.schema && self.current.schema.callback) || logit;
		var obj = self.current.object;
		if(!self.current.list || !obj || self.current.list.indexOf(obj) == -1){
		//no list or no item
			alert('object or list not found');
			self.hide();
			callback(obj);
			return;
		}
		var index = self.current.list.indexOf(obj);

		self.current.list.removeAt(index);
		logit('object deleted');
		self.hide();
		callback(obj);
	};
	this.selectAllItems = function(){
        /*|{
            featured:true,
            description:'selects all items in the current filtered list',
            tags:'list, select'
        }|*/
        self.current.selectedListItems = [];
        var currentItemIDs =
            currentListItems.map(function(item){return item[self.getIDProp()]})
        self.overlay.addClass('multi-edit');
        self.current.selectedListItems =  currentItemIDs;
        self.overlay.find('.joe-panel-content-option').addClass('selected');
        self.allSelected = true;
        self.updateSelectionVisuals();

    };
    this.deselectAllItems = function(){
        /*|{
            featured:false,
            description:'deselects all items in the current filtered list',
            tags:'list, select'
        }|*/
        self.current.selectedListItems = [];
        self.overlay[0].classList.remove('multi-edit');
        self.overlay.find('.joe-panel-content-option').removeClass('selected');
        self.allSelected = false;
        self.updateSelectionVisuals();
    }

/*-------------------------------------------------------------------->
ANALYSIS, IMPORT AND MERGE
 <--------------------------------------------------------------------*/
    this.analyzeImportMerge = function(newArr,oldArr,idprop,specs){
            self.showMessage('Beginning Merge Analysis of '+(newArr.length + oldArr.length),{timeout:0});
        if(_webworkers){
            //_joeworker = new Worker("../JsonObjectEditor/js/joe-worker.js");
            _joeworker = new Worker(joe_web_dir+"js/joe-worker.js");
            _joeworker.onmessage = function(e){
                //logit(e.data);
                if(!e.data.worker_update) {
                    self.showMessage('Analysis Completed in '+e.data.time+' secs',{timeout:0});
                    var analysisOK = confirm('Run Merge? \n' + JSON.stringify(e.data.results, null, '\n'));
                    if (analysisOK) {
                        specs.callback(e.data);
                    }else{
                        self.showMessage('next step cancelled');
                    }
                }else{
                    if(e.data.worker_update.message){
                        self.showMessage(e.data.worker_update.message, {timeout: 0});
                    }else {
                        self.showMessage('Merged ' + e.data.worker_update.current + '/' + e.data.worker_update.total + ' in ' + e.data.worker_update.time + ' secs', {timeout: 0});
                    }
                    logit('Merged '+e.data.worker_update.current+'/'+e.data.worker_update.total+' in '+e.data.worker_update.time+' secs');
                }
            };
            var safespecs = $.extend({},specs);
            delete safespecs.icallback;
            delete safespecs.callback;
            _joeworker.postMessage({action:'analyzeMerge',data:{
                newArr:newArr,
                oldArr:oldArr,
                idprop:idprop,
                specs:safespecs
            }});

            return;
        }


        var aimBM = new Benchmarker();
        var idprop = idprop || '_id';
        var defs = {
            callback:printResults,
            deleteOld:false,
            dateProp:null,
            icallback:null
            };
        specs = $.extend(defs,specs);
        var data = {
            add:[],
            update:[],
            same:[],
            'delete':[]
        };
        var oldArr = oldArr.sortBy(idprop);
        var newArr = newArr.sortBy(idprop);

        var newObj, oldObj,matchObj,testObj, i = 0,query={};
        var oldIDs = [];
        var newIDs = [];
//create an array of old object ids
        oldArr.map(function(s){oldIDs.push(s[idprop]);});
        newArr.map(function(s){newIDs.push(s[idprop]);});

        var query = {};
        query[idprop] = {$nin:oldIDs};
        //adds
        data.add = newArr.where(query);

        //updates||same
        query[idprop] = {$in:newIDs};
        var existing = oldArr.where(query);

        //for one-to-one, new items in the old array
        query[idprop] = {$in:oldIDs};
        var onetoone = newArr.where(query);


        for(var i = 0, len = existing.length; i < len; i++ ){
            newObj = onetoone[i], oldObj = existing[i];
            if (!newObj) {
                data.same.push(oldObj);//same
            } else {
                if (specs.dateProp
                    && newObj[specs.dateProp]
                    && oldObj[specs.dateProp]
                    && newObj[specs.dateProp] == oldObj[specs.dateProp]) {
                    data.same.push(newObj);//same

                } else {
                    data.update.push(newObj);//update
                }
            }

        }

/*        var newObj, oldObj,matchObj, i = 0,query={};
        while(newObj = newArr[i++]){
            matchObj = oldArr.where(newObj);//is it currently in there in the same form

            if(matchObj.length){//currently there and the same
                data.same.push(newObj);
            }else{
                query[idprop] = newObj[idprop];

                matchObj = oldArr.where(query);
                if(matchObj.length){//is there, not the same
                    if(specs.dateProp ) {
                        if(matchObj[0][specs.dateProp] == newObj[specs.dateProp]) {
                            data.same.push(newObj);
                        }
                    }else {
                        data.update.push(newObj);
                    }
                }else{//not there
                    data.add.push(newObj);

                   }
            }
            if(specs.icallback){
                specs.icallback(i);
            }
        }*//*if(specs.dateProp){
            query[specs.dateProp] = newObj[specs.dateProp];
        }*/
        data.results = {
            add: data.add.length,
            update: data.update.length,
            same: data.same.length

        };
        self._mergeAnalyzeResults = data;

        logit('Joe Analyzed in '+aimBM.stop()+' seconds');

        var analysisOK = confirm('Run Merge? \n'+JSON.stringify(data.results,null,'\n'));
        if(analysisOK) {
            specs.callback(data);
        }
        function printResults(data){
            self.showMiniJoe({title:'Merge results',props:JSON.stringify(data.results,null,'\n'),mode:'text',menu:[]})
        }
    };

    this.analyzeClassObject = function(object,ref,maxDepth,classString){
        /*|
        {
            alias:'_joe.parseAPI, window.joeAPI',
            description:'Takes a js class and creates a joe viewable object from it.',
            tags:'api, analysis'
        }
         |*/
        var classString = classString;
        var maxDepth = maxDepth || 10;
        var data ={
            methods:[],
            properties:[]
        };
        var pReg = /function[\s*]\(([_a-z,A-Z0-9]*)\)/;
        var curProp;
        var params;

        function parseFunction(object,p,parent,depth){

            var parent = parent || '';
            var curProp = object[p];
            params = pReg.exec(object[p]);
            params = (params && params[1]) || '';
            try {

                var comments = /\/\*\|([\s\S]*)?\|\*\//;
                evalString = curProp.toString().match(comments);
                if(evalString && evalString[1]){
                    evalString = eval('('+evalString[1].replace(/\*/g,'')+')');
                }
                //logit(evalString);

            } catch (e) {
                evalString = {error:'Could not evalutate "'+p+'": \n' + e};
                self.Error.add('Could not evalutate "'+p+'": \n' + e,e,{caller:'self.analyzeClassObject'})
            }
            var funcName = parent+p;
            var methodObj = {
                code: object[p],
                name: funcName,
                global_function: false,
                propname:p,
                ref: ref,
                'class':ref,
                args : params,
                parameters:(comments && (comments.params || comments.parameters)) ||null,
                _id:ref+'_'+funcName,
                comments:evalString||{},
                itemtype:'method',
                parent:parent||null
            };

            if(classString){
                try{
                    methodObj.count = (classString.toString().match(new RegExp((parent||'.')+p,'g'))||[]).length;
                }   catch(e){
                    //warn('issue with static class: '+ref);
                    self.Error.add('issue with static class: '+ref,e,{caller:'self.analyzeClassObject'})

                }
            }

            if(methodObj.comments && (methodObj.comments.info||methodObj.comments.description)){
                methodObj.description =methodObj.comments.info||methodObj.comments.description;
            }
            if(methodObj.comments && methodObj.comments.tags){
                methodObj.tags =methodObj.comments.tags.split(',');
            }
            data.methods.push(methodObj);

            return {_id:ref+'_'+funcName};
        }

        function parseProperty(object,p,parent,depth) {
            if(depth > maxDepth){
                return;
            }
            var depth = depth || 0;
            depth++;
            var parent = parent || '';
            var curProp = object[p];

            var propName = parent+p;
            var propObj = {
                name:propName,
                ref:ref,
                'class':ref,
                _id:ref+'_'+propName,
                itemtype:'property',
                parent:parent||null,
                depth:depth,
                propname:p,
            };

            if($.type(curProp) == "object"){

                var subproperties =[];

                for(var sub in curProp) {
                    try {
                        if(curProp.hasOwnProperty(sub) && object != window.document && object != window) {

                                //JSON.stringify(curProp.sub);

                                if ($.type(curProp[sub]) == "function") {
                                    subproperties.push(parseFunction(curProp, sub, propName+'.',depth)._id);
                                } else {
                                    subproperties.push(parseProperty(curProp, sub, propName+'.',depth)._id);
                                }
                        }
                    }catch(e){
                        //console && console.log(sub+': '+e,curProp[sub]);
                        self.Error.add(sub+': '+e+':'+curProp[sub],e,{caller:'parseProperty'})
                    }
                }
                if(subproperties.length){propObj.subproperties = subproperties;}
                //else{
                    propObj.value = object[p];
                //}
            }else{
                propObj.value = object[p];

            }
            data.properties.push(propObj);
            return {_id:ref+'_'+propName};
        }
        for(var p in object){
            curProp = object[p];
            try {
                if ($.type(curProp) == "function") {
                    parseFunction(object,p);
                }else{
                    parseProperty(object,p);
                }
            }catch(e){
                //logit(e);
                self.Error.add(e,e)
            }
        }

        return data;
    };

/*-------------------------------------------------------------------->
	H | HELPERS
<--------------------------------------------------------------------*/
	this.functionName = function(func){
		var name=func.toString();
		var reg=/function ([^\(]*)/;
		return reg.exec(name)[1];
	};

    this.getState = function(){
        /*|{
         featured:true,
         description:'returns an abridged version of the calling joe panel including current object,list (filtered),schema and window title',
         tags:'state, prop, getter',
         returns:'(object)'

         }|*/
        return{
            object:_jco(),
            list:self.current.filteredList,
            schema:(self.current.schema && self.current.schema.name || null),
            title:self.current.title
        }
    };

    this.getCascadingProp = function(propname,schema){
        /*|{
         specs:'propname,schema',
         description:'Gets a prop based off either current user specs, schema spec or joe default spec (respectively)',
         tags:'cascading, prop',
         returns:'(mixed)',
         featured:true
         }|*/
        var schema = (schema && (self.schemas[schema] || schema)) || self.current.schema;
        var prop =
            self.current[propname]
            || (self.current.userSpecs && self.current.userSpecs[propname])
            || (schema &&
                (
                    (listMode && schema.listView && schema.listView[propname])
                    || schema[propname]
                )
            )
            || self.specs[propname];

        return prop;


    };

	this.getIDProp = function(schema){
        /*|{
            featured:true,
            tags:'helper, prop,getter',
            description:'Gets the idprop of the current item, or any item with a passed schemaname.',
            returns:'(string)'
        }|*/

        var prop = (schema && self.schemas[schema] && self.schemas[schema].idprop)
        || (self.current.schema && (self.current.schema.idprop || self.current.schema._listID))
        || '_id';
		// var prop = ( ((schema && self.schemas[schema]) || self.current.schema) && (self.current.schema.idprop || self.current.schema._listID)) || '_id' || 'id';
		return prop;
	};

    this.propAsFuncOrValue = function(prop, toPass, defaultTo,secondToPass){
        /*|{
            featured:true,
            tags:'helper',
            description:'Parses the property passed as a function or value, can be passed an object to be a parameter of the function call.'
         }|*/
        try {
            var toPass = toPass || (!$c.isEmpty(self.current.constructed) && self.current.constructed) || self.current.object;

            /*        if(!toPass.hasOwnProperty(prop)){
             return defaultTo || false;
             }*/
            if (prop && typeof prop == 'function') {
                return prop(toPass,secondToPass);
            }
            return prop;
        }catch(e){
            //logit('JOE: error parsing propAsFunction: '+e);
            self.Error.add('JOE: error parsing propAsFunction: '+e,e,{caller:'self.propAsFuncOrValue'})
            return '';
        }
    };
    this.getFieldValues = function(values,prop){
        var vals = self.propAsFuncOrValue(values) || [];
        if(vals.isString()){
            vals = self.getDataset(vals)||[];
        }
        if(prop){
            var sortby = prop.sortby || prop.sortBy;
            if(sortby){
                vals = vals.sortBy(sortby);
            }
        }
        return vals;
    };
/*-------------------------------------------------------------------->
 I | Hashlink
 <--------------------------------------------------------------------*/
    this.Hash = {

    };
    this.setEditingHashLink = function(bool){
        /*|{
            tags:'hash',
            description: 'toggle to set hash link editing , false means joe did not cause change directly.'
        }|*/
        window._joeEditingHash = bool||false;
    };

    var hash_delimiter = '/';
    if(location.href && location.href.indexOf(':::') != -1){
        hash_delimiter = ':::';
    }
    this.Hash.delimiter = hash_delimiter;

    this.updateHashLink = function(){
        /*|{
         featured:true,
         description:'updates the hashlink based on the calling joe.',
         tags:'hash, SPA',
         returns:'(string)'

         }|*/
        if(!self.specs.useHashlink){
            return;
        }
        var hashInfo ={};

        hashInfo.schema_name =self.current.schema && self.current.schema.__schemaname;
        if(listMode){
            hashInfo.object_id = '';
            hashInfo.subset = (self.current.subset && self.current.subset.name) || '';
        }else{
            hashInfo.subset = '';
            hashInfo.object_id = (self.current.object && self.current.object[self.getIDProp()])||'';
        }

        var hashtemplate = ($.type(specs.useHashlink) == 'string')?
            specs.useHashlink:
            hash_delimiter+'${schema_name}'+((hashInfo.object_id||hashInfo.subset) && hash_delimiter || '')+'${object_id}${subset}';
            // if(self.current.keyword && listMode){
            //     hashtemplate+='@keyword='+self.current.keyword;
            // }
        //$SET({'@!':fillTemplate(hashtemplate,hashInfo)},{noHistory:true});
        //$SET({'@!':fillTemplate(hashtemplate,hashInfo)});
        location.hash = fillTemplate(hashtemplate,hashInfo);
        return location.hash;
    };
    this.Hash.update = this.updateHashLink;

    this.readHashLink = function(hash){
        /*|{
            featured:true,
            description:'reads the page hashlink and parses for collection, item id, subset and details section',
            tags:'hash, SPA',
            returns:'(bool)'

        }|*/
        if(!self.specs.useHashlink){
            return false;
        }
        try {
            var useHash = hash || $GET('!') || location.hash;
            if (!useHash || self.joe_index != 0) {
                return false;
            }
            var useHash = useHash.replace('#','');
            var hashBreakdown = useHash.split(hash_delimiter).condense();
            //hashBreakdown.removeAll('');
            hashBreakdown = hashBreakdown.filter(function(v){return v!=='';});
            //BUGFIX: condense no longer removes blank strings.
            if(!hashBreakdown.length){
                self.closeButtonAction();
                return false;
            }

            var hashInfo = {};
            var hashSchema = self.schemas[hashBreakdown[0]];
            var hashItemID = hashBreakdown[1]||'';

/*            function goHash(item,dataset){
                if(item){
                    goJoe(collectionItem, {schema: hashSchema});
                }
            }*/
            if (hashSchema && (hashSchema.dataset || (!$.isEmptyObject(self.Data) && self.Data[hashSchema.__schemaname]))) {
                var dataset;
                
                if(hashSchema.dataset) {
                    dataset = (typeof(hashSchema.dataset) == "function") ? hashSchema.dataset() : hashSchema.dataset;
                }else{
                    dataset =  self.Data[hashSchema.__schemaname] || [];
                }
                hashInfo.schema = hashSchema;
                /*function variableGoJoe(dataset,item,subset){
                    goJoe((item||), {schema: hashSchema});
                }*/
            
            //PROCESS single item, subset, subquery or keyword
                if(!$.isEmptyObject(self.Data)) {
                    //self.current.keyword = hashInfo.keyword = $GET('keyword');
                    var curkey = self.current.keyword;
                    self.current.keyword = hashInfo.keyword = $GET('keyword');
                    self.current.keyword = self.current.keyword || curkey;
                //SINGLE ITEM
                    if(hashItemID ){
                        var collectionName = hashSchema.__collection || hashSchema.__schemaname;


                    //using standard id
                        if(hashItemID.indexOf('@') != -1){//passed keyword or filters
                            hashItemID=hashItemID.substr(0,hashItemID.indexOf('@'))
                            var metas ={};
                            

                        }
                        
                        if(hashItemID.indexOf(':') != -1) {//2nd param is key/value pair
                            var key = hashItemID.split(':')[0];
                            var value = hashItemID.split(':')[1];

                            //var collectionName = hashSchema.__collection || hashSchema.__schemaname;
                            if (collectionName) {
                                var collectionItem = self.getDataItem(value, collectionName,key);
                                if (collectionItem) {
                                    goJoe(collectionItem, hashInfo);
                                }
                            }else {//SHOW LIST, NO item
                                hashInfo.subset = hashItemID;
                                goJoe(dataset, hashInfo);
                            }
                        }
                        else{

                            if (collectionName) {
                                var collectionItem = self.getDataItem(hashItemID, collectionName);
                               // goHash(collectionItem,);
                                if (collectionItem) {
                                    goJoe(collectionItem, hashInfo);
                                }else {//SHOW LIST, NO item
                                    hashInfo.subset = hashItemID;
                                    goJoe(dataset, hashInfo);
                                }
                            }
                        }
                    }else {//SHOW LIST, NO item
                      //  goJoe(dataset, {schema: hashSchema,subset:hashItemID});
                        goJoe(dataset, hashInfo);

                    }
                    var section = $GET('section')||hashBreakdown[2]||false;
                    if(!section) {
                    }else{
                        //$DEL('section');
                        //self.updateHashLink();
                        self.gotoSection(section);
                    }

                }
            }
            else{
                throw(error,'dataset for "'+hashSchema+'" not found');
            }
            //logit('hashInfo',hashInfo);
        }catch(e){
            //logit('error reading hashlink:'+e);
            self.Error.add('error reading hashlink:'+e,e,{useHash:useHash||''})

            return false;
        }

        __gotoJoeSection = self.gotoSection;
        return true;
    };
    this.Hash.read = this.readHashLink;

    this.isNewItem = function(){
        if(listMode){
            return false;
        }
        var idprop = self.getIDProp();
        if(!self.current.object
            || !self.current.object[idprop]
            || !self.search(self.current.object[idprop]).length
        ){
            return true;
        }
        return false;
    };

/*<-----------------s-------------------------------------------->*/

/*-------------------------------------------------------------------->
 I.2 | HashCHangeHandler
 <--------------------------------------------------------------------*/

/*-------------------------------------------------------------------->
 J | RESPOND
 <--------------------------------------------------------------------*/
    this.respond = function(e){
        // if($c.isMobile() && !_joe.current.object && !_joe.current.list){
            
        //     return;
        // }
        // if (document.activeElement.tagName == "INPUT") {
            
        //     return;
        // }
        var w = self.overlay.width();
        var h = self.overlay.height();
        var curPanel = self.panel[0];
        //logit(w);
        var sizeClass='';
        if(curPanel.className.indexOf('-size') == -1){
            curPanel.className += ' large-size ';
        }
        if(w<600){
            sizeClass='small-size';
        }else{
            sizeClass='large-size';
        }

        curPanel.className = curPanel.className.replace(/[a-z]*-size/,sizeClass);
        htmlRef.className = htmlRef.className.replace(/[a-z]*-size/,'joe'+sizeClass);
        self.sizeClass = sizeClass;
/*
        logit(e);
        logit($(window).height())*/
    };

this._renderGotoButton = function(id,dataset,schema){
    var item = {};
    item._id = id || this._id;
    item.dataset = dataset || this.itemtype;
    item.schema = schema || item.dataset;
    var idprop = self.getIDProp(schema);
    return fillTemplate(
        '<div class="joe-block-button goto-icon" ' +
            //'onclick="goJoe(getNPCDataItem(\'${_id}\',\'${dataset}\'),' +
        'onclick="goJoe(_joe.getDataItem(\'${'+idprop+'}\',\'${dataset}\'),' +
        '{schema:\'${schema}\'})"></div>',item);

};

    window._renderGotoButton = this._renderGotoButton;

/*-------------------------------------------------------------------->
K | Smart Schema Values
 <--------------------------------------------------------------------*/
    this.smartSave = function(item,newItem,schema){
        //find schema
        //look for save function

    };

/*-------------------------------------------------------------------->
 L | API
<--------------------------------------------------------------------*/
    this.parseAPI = function(jsObj,libraryname){
        goJoe(self.analyzeClassObject(jsObj,libraryname).methods,
            {schema:'method',title:libraryname+' Methods'})
    };
    window.joeAPI = this.parseAPI;

    this.jsonp = function(url,dataset,callback,data){
        callback = callback || logit;
        $.ajax({
            url:url,
            data:data,
            dataType:'jsonp',
            callback:function(data){
                callback(data,dataset);
            }
        })/*
        ajax({url:url,
            onsuccess:function(data){
                callback(data,dataset);
            },
            dataType:'jsonp',
        query:data
        })*/
    };

/*-------------------------------------------------------------------->
 N | ITEM NAVIGATION
 <--------------------------------------------------------------------*/
    this.next = function(){
        navigateItems(1);
    };
    this.previous = function(){
        navigateItems(-1);
    };
    function navigateItems(goto){
        var dataset,currentObjectIndex,gotoIndex,gotoObject;
        var objIndex = self.getObjectIndex();
        var dataset = objIndex.dataset;
        if(!dataset){return alert('no dataset set');}
        currentObjectIndex = objIndex.index;
        gotoIndex = currentObjectIndex + goto;
        if(gotoIndex > dataset.length-1){
            gotoIndex = 0;
        }else if(gotoIndex < 0){
            gotoIndex = dataset.length-1;
        }
        gotoObject = dataset[gotoIndex];
        self.show(gotoObject, {schema:self.current.schema.__schemaname});
    }

    this.getObjectIndex = function(object){
        var object = object || self.current.item;
        var dataset,currentObjectIndex,gotoIndex,gotoObject,idprop = self.getIDProp();
        if(self.current.filteredList){
            dataset = self.current.filteredList;
        }else{
            if(!self.current.schema){return alert('no schema set');}
            dataset = self.getDataset(self.current.schema.__schemaname)
                .sortBy(self.current.schema.sorter || ['name']);
        }
        if(!dataset){return {index:-1,dataset:null}}

        return {index:dataset.indexOf(self.current.object),dataset:dataset};

        //if(currentObjectIndex == -1){return alert('object not found in current dataset');}
    };
    var getSelfStr = 'getJoe('+self.joe_index+')';

/*-------------------------------------------------------------------->
 IO | Import / Export
 <--------------------------------------------------------------------*/
    this.IO = {Export:{}};
    this.IO.Export.button = __exportBtn__;
    this.IO.Export.schema = {fields:[{name:'code',type:'code'}]};
    this.IO.Export.json = self.exportJSON;
    this.IO.Export.csv = self.Export.csv;
    this.IO.export = function(item,json,specs){
        var specs = $.extend({
            minify:false,
            schema:self.IO.Export.schema,
            objvar:self.current.schema.__schemaname
        },specs);
        var item = item || _jco();
        if(json){
            self.exportJSON(item,specs);
            return 
        }

        var item_spec = {code:JSON.stringify(item).replace(/,"/i,',\n"')};
        self.show(item_spec,{schema:self.IO.Export.schema});
    }
/*-------------------------------------------------------------------->
 Co | Components
 <--------------------------------------------------------------------*/
 this.Components = {loaded:{},styleTag:'joeWebComponentStyles'};
 this.Components.load = function(coName){
    if(!document.getElementById(self.Components.styleTag)){
        var c = document.createElement('style');
        c.id = self.Components.styleTag;
        document.body.appendChild(c);
    }
    if(!self.Components.loaded[coName]){
        var script=document.createElement('script');
        script.type='text/javascript';
        var wd = window.web_dir || '/JsonObjectEditor/'
        script.src=`${wd}web-components/${coName}.js`;
        document.body.appendChild(script);
        self.Components.loaded[coName] = true;
    }
 }
 this.Components.appendStyles = function(styles){
    var styleTag = document.getElementById(_joe.Components.styleTag);
    if(styleTag){
     styleTag.innerHTML+=styles;
    }
 }
 this.Components.getAttributes = function(comp){
    var d = {};
    for (var a = 0; a < comp.attributes.length; a++) {
      d[comp.attributes[a].name] = comp.attributes[a].value;
    }
    return d;
 }
 this.Components.init = function(){
    self.Components.load('joe-button');
    self.Components.load('joe-field');
    self.Components.load('joe-list-item');
    self.Components.load('joe-autocomplete');
    self.Components.load('joe-user-cube');
    self.Components.load('joe-card');
    self.Components.load('capp-components');
 }
/*-------------------------------------------------------------------->
    //BUTTONS
<--------------------------------------------------------------------*/

    this.buttons = {
        create:__createBtn__,
        quickSave:__quicksaveBtn__,
        save:__saveBtn__,
        'delete':__deleteBtn__,
        multiSave:__multisaveBtn__,
        selectAll:__selectAllBtn__,
        duplicate:__duplicateBtn__,
        export:self.IO.Export.Button

    };
    this.buttons.next = {name:'next', display:'next >',css:'joe-fright', action:getSelfStr+'.next()'};
    this.buttons.previous = {name:'previous',display:'< prev', css:'joe-fleft', action:getSelfStr+'.previous()'};

/*-------------------------------------------------------------------->
    //SPEECH RECOGNITION
<--------------------------------------------------------------------*/
//_joe.showMiniJoe({object:'hello',title:'Audio',menu:[{name:'confirm'},{name:'cancel'}]})
    this.Speech = {
        init : function(force){
          if(force || self.specs.speechRecognition){
            self.addStylesheet(joe_web_dir+'js/plugins/microphone/microphone.min.css',{name:'microphone-css'})
            self.addScript(joe_web_dir+'js/plugins/microphone/microphone.min.js',{name:'microphone-js'})
          }
        },
        confirm: function(){
          self.Mini.show({object:'hello',title:'Audio',menu:[{name:'confirm'},{name:'cancel'}]})
        },
        initMic:function(){
          var mic = self.Speech.mic = new Wit.Microphone(document.getElementById('speech-button-'+self.joe_index));
          var info = function (msg) {
            logit(msg);
            //document.getElementById("info").innerHTML = msg;
          };
          var error = function (msg) {
            logit(msg);
            //document.getElementById("error").innerHTML = msg;
          };
          mic.onready = function () {
            info("Microphone is ready to record");
          };
          mic.onaudiostart = function () {
            info("Recording started");
            error("");
          };
          mic.onaudioend = function () {
            info("Recording stopped, processing started");
          };
          mic.onresult = function (intent, entities) {
logit(intent)
            /*var r = kv("intent", intent);

            for (var k in entities) {
              var e = entities[k];

              if (!(e instanceof Array)) {
                r += kv(k, e.value);
              } else {
                for (var i = 0; i < e.length; i++) {
                  r += kv(k, e[i].value);
                }
              }
            }

            document.getElementById("result").innerHTML = r;*/
          };
          mic.onerror = function (err) {
            error("Error: " + err);
          };
          mic.onconnecting = function () {
            info("Microphone is connecting");
          };
          mic.ondisconnected = function () {
            info("Microphone is not connected");
          };

          mic.connect("3J7E3INLVHRRZYVTX6VBXWUXXGMRDEPS");
        }

    };
/*--------------------------------> TITLE <---------------------------------*/
    this.TITLE = {
        dom:function(){
            return self.panel && self.panel.find('.joe-panel-title') || null;
        },
        get:function(){return self.TITLE.dom.html()},
        set:function(value,specs){
            var specs = $.extend({
                before:'',
                after:''
            },specs||{});
            var value = self.propAsFuncOrValue(value);
            if(value){
                self.TITLE.dom().html(specs.before+value+specs.after);
                return value;
            }
            var tempObj = self.constructObjectFromFields()
            var titleObj = $.extend({},tempObj,
                {_listCount:(currentListItems||[]).length||'0'});
			var cTitle = self.propAsFuncOrValue(self.current.title,tempObj);
            var titleStr = fillTemplate(cTitle,titleObj);
            self.TITLE.dom().html(specs.before+titleStr+specs.after);
            return titleStr;
        }
    };


    /*--------------------------------> TITLE <---------------------------------*/
/*-------------------------> CONNECT <--------------------------------------*/
 var iceServers = [{
        urls: 'stun:stun.l.google.com:19302'
    }, {
        urls: 'stun:webrtcweb.com:7788',
        username: 'muazkh',
        credential: 'muazkh'
    }, {
        urls: 'turn:webrtcweb.com:7788',
        username: 'muazkh',
        credential: 'muazkh'
    }, {
        urls: 'turn:webrtcweb.com:8877',
        username: 'muazkh',
        credential: 'muazkh'
    }, {
        urls: 'turns:webrtcweb.com:7788',
        username: 'muazkh',
        credential: 'muazkh'
    }, {
        urls: 'turns:webrtcweb.com:8877',
        username: 'muazkh',
        credential: 'muazkh'
    }, {
        urls: 'stun:webrtcweb.com:4455',
        username: 'muazkh',
        credential: 'muazkh'
    }, {
        urls: 'turns:webrtcweb.com:4455',
        username: 'muazkh',
        credential: 'muazkh'
    }, {
        urls: 'turn:webrtcweb.com:5544?transport=tcp',
        username: 'muazkh',
        credential: 'muazkh'
    }];

    var rtcConfig = {
        iceServers: iceServers,
        sdpSemantics:'plan-b'
    };
    var useRTC = false;
    this.CONNECT = {
        gotStream:function(stream) {
            logit('Received local stream');
            
            if(self.CONNECT.videos.local){
                //self.CONNECT.videos.local.src = window.URL.createObjectURL(stream);
                self.CONNECT.videos.local.srcObject = stream;
            }
            // Add localStream to global scope so it's accessible from the browser console
            self.CONNECT.streams.local = stream;
            self.CONNECT.RTC.addStream(self.CONNECT.streams.local);
        },
        RTC:(useRTC && window.RTCPeerConnection && new RTCPeerConnection(rtcConfig) || null),
        newConnection:function(){
            self.CONNECT.RTC = new RTCPeerConnection(rtcConfig)
            self.CONNECT.init();
        },
        init:function(){
            self.CONNECT.RTC.oniceconnectionstatechange = function(){
                if(self.CONNECT.RTC.iceConnectionState == "completed"){
                    logit('ICE state: '+self.CONNECT.RTC.iceConnectionState);
                    if(!self.CONNECT.offerinprogress){
                        if(!confirm('connect request received')){
                            self.CONNECT.end(self.CONNECT.userid,true);
                        }
                    }
                    logit('stopping CONNECT interval -init');
                    clearInterval(self.CONNECT.offerinterval);
                    self.CONNECT.offerinprogress = false;
                }
            }
            self.CONNECT.RTC.onaddstream = function (evt) {
                    logit('stream');
                    self.CONNECT.streams.remote = evt.stream;
                    //self.CONNECT.videos.remote.src = window.URL.createObjectURL(self.CONNECT.streams.remote);
                    self.CONNECT.videos.remote.srcObject = self.CONNECT.streams.remote;
                    //elem.srcObject = stream
                    self.CONNECT.videos.remote.play();
            };
            self.CONNECT.RTC.onremovestream = function(evt){
                logit('stream ended:',evt);
            }
            self.CONNECT.RTC.onicecandidate = function (evt) {
                    //logit('candidate',evt);
                    //alert('candidate')
            };
        },
        videos:{},
        streams:{},
        start:function(userid,makeoffer){
            if(self.CONNECT.RTC.signalingState == "closed"){
                self.CONNECT.RTC = new RTCPeerConnection(iceServers);
            }
            if(!userid){
                _joe.showMiniJoe({
                    title:'CONNECT w/',
                    list:self.Data.user.where({_id:{$ne:self.User._id}}),
                    template:function(user){
                        var template =
                            '<joe-title>${name}</joe-title><joe-subtitle>${fullname}</joe-subtitle>'
                            +'<joe-subtext>${info}</joe-subtext>';
                        if(user.image_url){
                            template = '<joe-listitem-icon style="background-image:url(${image_url});"></joe-listitem-icon>'+template;
                        }
                        return template;
                    },
                    idprop:'_id',
                    menu:[],
                    
                    callback:function(item_id){
                        self.CONNECT.start(item_id);
                    }
                })
                return;
            }
            self.CONNECT.end(userid);
            //x select user
            var userobj = self.getDataItem(userid,'user');
            //x open popup
            var popup = self.CONNECT.createPopup(userobj);
            //get my video data
            self.CONNECT.videos.local = $('#'+popup.domid).find('.joe-connect-myvideo')[0];
            self.CONNECT.videos.remote = $('#'+popup.domid).find('.joe-connect-video')[0];
            // self.CONNECT.videos.local.addEventListener('loadedmetadata', function() {
            // logit('Local video videoWidth: ' + this.videoWidth +
            //     'px,  videoHeight: ' + this.videoHeight + 'px');
            // });
            // self.CONNECT.videos.remote.addEventListener('loadedmetadata', function() {
            // logit('Local video videoWidth: ' + this.videoWidth +
            //     'px,  videoHeight: ' + this.videoHeight + 'px');
            // });
            // var gum = ((navigator.mediaDevices && navigator.mediaDevices.getUserMedia) || navigator.getUserMedia);
            navigator.mediaDevices.getUserMedia({
                audio: true,
                video: true,
                //mandatory: {chromeMediaSource: 'screen'}
                // or desktop-Capturing
                //mandatory: {chromeMediaSource: 'desktop'}
            })
            .then(self.CONNECT.gotStream)
            .catch(function(e) {
                alert('getUserMedia() error: ' + e.name);
                console.log(e);
            });
            //send invite (socket)

            //var RTC = self.CONNECT.RTC = new RTCPeerConnection(RTCconfig);
            if(makeoffer){
                var RTC = self.CONNECT.RTC;
                
                RTC.createOffer()
                .then(
                    function(offer){
                        return RTC.setLocalDescription(offer);
                    },
                    function(data){alert('error in RTC'+data); logit(data);}
                ).then(function(){
                    self.SERVER.socket.emit('connect_offer',{
                        user:_joe.User._id,
                        target:userid,
                        sdp:RTC.localDescription
                    })
                }).catch(function(reason){console.log(reason)});
                
            }
        },
        sendOfferToServer:function(offer){

        },
        end:function(userid,clear){
            //cleanup backend
            //close popup
            capp.Popup.delete('connect_'+userid);
            if(clear){
                logit('stopping CONNECT interval - end');
                clearInterval(self.CONNECT.offerinterval);
                self.CONNECT.offerinprogress = false;
                self.CONNECT.userid = null;
                // self.CONNECT.streams.local.getAudioTracks()[0].stop();
                // self.CONNECT.streams.local.getVideoTracks()[0].stop();
                // self.CONNECT.streams.remote.getAudioTracks()[0].stop();
                // self.CONNECT.streams.remote.getVideoTracks()[0].stop();
                self.CONNECT.RTC.close();
                self.CONNECT.streams.remote && self.CONNECT.streams.remote.getTracks().forEach(function (track) {
                    track.stop();
                });
                self.CONNECT.streams.local && self.CONNECT.streams.local.getTracks().forEach(function (track) {
                    track.stop();
                });
            }
        },
        template:function(userobj){
            var h = 
                '<joe-video-holder>'
                    +'<video autoplay class="joe-connect-video"></video>'
                    +'<video autoplay  class="joe-connect-myvideo"></video>'
                +'</joe-video-holder>'
                +userobj.name;
               // +'<div onclick="_joe.CONNECT.end(\''+userobj._id+'\',true);"> close </div>';
            return h;
        },
        createPopup:function(userobj,specs){
            //create panel
            return capp.Popup.add('connect w/ <b>'+(userobj.fullname || userobj.name)+'</b>',
            self.propAsFuncOrValue(self.CONNECT.template,userobj),
            {id:'connect_'+userobj._id,active:true,
            close_action:'_joe.CONNECT.end(\''+userobj._id+'\',true);'
            });
        }
    }
    useRTC && window.RTCPeerConnection && this.CONNECT.init();
/*-------------------------> end CONNECT <--------------------------------------*/
/*-------------------------> SERVER <--------------------------------------*/
    this.SERVER = {
        Plugins:{
            awsFileUpload:function(data,callback){
                var file = data.file;
                var field = data.field;
                var bucket = field.bucketname || (_joe.Data.setting.where({name:'AWS_BUCKETNAME'})[0] || {value:''}).value;
                if(!bucket){
                    callback(err,file.base64);
                    return;
                }
                var filename = file.filename; 
                var directory = _joe.current.object._id;
                var url = (field && self.propAsFuncOrValue(field.server_url)) || 
                (location.port && '//'+__jsc.hostname+':'+__jsc.port+'/API/plugin/awsConnect/') || 
                '//'+__jsc.hostname+'/API/plugin/awsConnect/';
                
                
                var Key = directory+'/'+filename;
                $.ajax({
                    url:url,
                    type:'POST',
                    data:{
                        Key:Key,
                        base64:file.base64,
                        extension:file.extension,
                        ACL:'public-read'
                    },
                    success:function(response){
                        var url,filename;
                        if(!response.error){
                            logit(response);
                            var bucket = response.bucket || (_joe.Data.setting.where({name:'AWS_BUCKETNAME'})[0] || {value:''}).value;
                            var region = (_joe.Utils.Settings('AWS_S3CONFIG') ||{}).region;
                            // var prefix = 'https://s3.'+((region && region +'.')||'')+'amazonaws.com/'+bucket+'/';
                            // var url = prefix+response.Key;
                            var prefix = (_joe.Data.setting.where({name:'AWS_S3PREFIX'})[0] || {value:''}).value || 'https://s3.'+((region && region +'.')||'')+'amazonaws.com/'+bucket+'/';
                            var url = prefix+response.Key;
                            var filename = response.Key.replace(self.current.object._id+'/','');
                            //TODO: change file status in list.

                            
                        }
                            
                        callback(response.error,url,filename);
                    }
                })
            },
            awsConnect:function(data,callback){
                var bucket = data.field.bucketname || (_joe.Data.setting.where({name:'AWS_BUCKETNAME'})[0] || {value:''}).value;
                if(!bucket){
                    callback(err,data.base64);
                    return;
                }
                var filename = _joe.current.object._id+data.extension; 
                var directory = _joe.current.object.itemtype;
                var url = (field && self.propAsFuncOrValue(field.server_url)) ||  
                (location.port && '//'+__jsc.hostname+':'+__jsc.port+'/API/plugin/awsConnect/') || 
                '//'+__jsc.hostname+'/API/plugin/awsConnect/';
                var Key = directory+'/'+filename;
                var field = data.field;
                $.ajax({
                    url:url,
                    type:'POST',
                    data:{
                        Key:Key,
                        base64:data.base64,
                        extension:data.extension,
                        ACL:'public-read'
                    },
                    success:function(data){
                        if(!data.error){
                            logit(data);
                            var bucket = data.bucket || (_joe.Data.setting.where({name:'AWS_BUCKETNAME'})[0] || {value:''}).value;
                            
                            //var prefix = 'https://s3.amazonaws.com/'+bucket+'/';
                            var prefix = 'https://'+bucket+'.s3.amazonaws.com/';
                            
                            var url = prefix+data.Key;
                            if(field.url_field){
                                var nprop = {};
                                _joe.current.object[field.url_field] = url;
                                nprop[field.url_field] = url;
                                /*$('.joe-field[name="'+field.url_field+'"]').val(url)*/
                                _joe.Fields.rerender(field.url_field,nprop);
                            }
                            callback(null,url);
                        }else{
                            callback(data.error);
                        }
                    }
                })
            }
        },
        ready:{
            schemas:false,
            datasets:false,
            localstorage:false,
            user:false,
            app:false
        },
        Cache:{
            reload:function(collections){
            /*|{
                featured:true,
                description:'refreshes the specified collections or all in the server cache',
                tags:'cache'
            }|*/
            $.ajax({
                url:'/API/cache/update/'+(collections||''),
                datatype:'jsonp',
                success:function(data){
                    if(data.error){
                        alert(data.error);
                    }else{
                        location.reload();
                    }
                }
            })
            }
        },
        History:{
          get:function(id,callback){
              var id = id || (self.current.item && self.current.item[self.getIDProp()]);
              if(!id){return;}
              //TODO: if callback == undefined
              var callback = callback || logit;
              
              $.ajax({
                  url:'/API/history/itemid/'+id,
                  dataType:'jsonp',
                  success:function(data){
                      if(data.error){
                          return alert(error);
                      }
                      callback(data);
                  }
              })
          },
          show:function(history_response,specs){
              var results = history_response.results;
              var item = self.search(history_response.query.itemid)[0]||{name:'not found'};
              goJoe(results,{schema:self.SERVER.History.schema,title:'History of '
                    +'['+item.itemtype+']' + ' '+item.name})
          },
          schema:{
              title:'[${collection} history] ${this.historical.name}',
              sorter:['!timestamp'],
              //TODO: wtf ${${this.changes.__length} || 0} __or
              listTitle:function(item){
                return '<joe-title>'
                    +((item.changes && item.changes.__length) || 0)
                    +' changes by ${this.user.name}</joe-title>'+
                '<joe-subtitle>${RUN[_joe.Utils.prettyPrintDTS;${timestamp}]}</joe-subtitle>';
            },
            checkChanges:false,
            fields:[
                'itemid:cuid',
                'collection',
                'timestamp',
                {name:'user',type:'subObject',locked:true},
                {name:'historical',type:'subObject',locked:true},
                {name:'changes',type:'subObject',locked:true}
            ]
          },
          button:{
              name:'history',
              label:'&nbsp;', 
              title:'Item History', 
              action:'_joe.SERVER.History.get(\'${_id}\',_joe.SERVER.History.show)', 
              css:'joe-history-button joe-left-button'
            }
        },
        User:{
            get:function(){
                $.ajax({
                    url:'/API/user/current',
                    dataType:'jsonp',
                    success:function(data){
                        if(data.error){
                            alert('error getting user:\n'+data.error);

                        }
                        else if(data.user){
                            _joe.User = data.user;
                            self.SERVER.User.store();
                            self.SERVER.User.signin();
                            logit('logged in as '+_joe.User.name);
                        }
                        self.SERVER.ready.user = true;
                        self.SERVER.onLoadComplete();
                    }
                })
            },
            store:function(){
                var now = new Date();
                var time = now.getTime();
                var expireTime = time + 10000*36000;
                now.setTime(expireTime);
                $('#cappUser').html(_joe.User.name);
                document.cookie = '_j_user='+_joe.User.name+';expires='+now.toGMTString()+';path=/';
                document.cookie = '_j_token='+_joe.User.token+';expires='+now.toGMTString()+';path=/';
            },
            signin:function(obj,oldObj,changes) {
                self.SERVER.socket.emit('signin', {user: _joe.User});
                self.SERVER.socket.on('user_info',function(data){
                    if(data.error || !data.connected_users 
                    || !$c.itemCount(data.connected_users)
                    ){
                        
                        self.Error.add('improper user data returned',null,data);
                        logit('user info',data);
                        return;
                    }
                    logit('user info',data);
                    var userObj = data.connected_users[_joe.User._id];
                    if(!userObj || !userObj.sockets){
                        return;
                    }
                    var html = '';
                    var imageHtml='';
                    if(_joe.User && _joe.User.image){
                        imageHtml = '<img class="capp-user-icon" src="'+_joe.User.image+'" />';
                        $('#cappUser').addClass('with-image'); 
                    }
                    $('#cappUser').html(imageHtml+'<capp-user-name><b>'+_joe.User.name+'</b>'
                        +((userObj.sockets.length > 1 && ('<small> x'+userObj.sockets.length+'</small>')) || '')+
                    '</capp-user-name>');
                    $('#cappUserMenu').find('.user-info').remove();
                    var user,user_html = '',apps='apps';
                    for ( var u in data.connected_users){
                        if(u != _joe.User._id) {
                            user = data.connected_users[u];
                            user_html+= '<capp-menu-content class="user-info"><b onclick="_joe.CONNECT.start(\''+user.user._id+'\',true);">'+user.user.name+'</b>'
                                +fillTemplate('<span onclick="window.open(\'${url}\')"> ${app}</span>',user.sockets)
                                +'</capp-menu-content>';
                        }
                    }
                    $('#cappUserMenu capp-menu-panel').append(user_html);
                });
            },
            Render:{
                cubes:function(ids,cssclass,user_dataset){
                    if(typeof ids == 'string'){ids = ids.split(',');}
                    var users = _joe.Data[(user_dataset || 'user')].where({_id:{$in:ids}});
                    var html = '';
                    users.map(function(user){
                        html += self.SERVER.User.Render.cube(user);
                    })
                    return html;
                },
                cube:function(userobj,cssclass){
                        var css = cssclass||'fright';
                        var u = userobj;
                        var html ='';
                        var name = u.fullname || u.name;
                        initials = name[0]+ (((name.indexOf(' ') > 0) && name[name.indexOf(' ')+1])||'');
                        html += '<joe-user-cube title="'+ name+'" style="background-color:'+userobj.color+'" class="joe-initials '+css+'">'
                            +initials
                            +' <span>'+ name+'</span>'
                        +'</joe-user-cube>';

                        return html;
                }
            }
        },
        handleUpdate:function(payload){
            try{
                var new_item,itemtype,_id,old_item;
                var idprop = '_id';
                for(var r = 0,tot = payload.results.length; r<tot;r++){

                    new_item = payload.results[r];
                    var schemaname = new_item.itemtype;
                    if(schemaname && self.schemas[schemaname] && self.schemas[schemaname].onload){
                        //run schema onload
                        self.schemas[schemaname].onload([new_item]);
                    }
                    if(!_joe.Data[new_item.itemtype]){
                        _joe.Data[new_item.itemtype] = [];
                    }
                    old_item = _joe.Data[new_item.itemtype].where({_id:new_item[idprop]})[0] || false;
                    //logit(JSON.stringify(payload));
                    if(old_item){
                        $.extend(old_item,new_item);
                        if(_joe.current.list && _joe.current.list.indexOf(old_item) != -1 && listMode){
                            _joe.reload(true);
                        }

                    }else{
                        _joe.Data[new_item.itemtype].push(new_item);
                        if(_joe.current.list && _joe.current.list.indexOf(new_item) != -1 && listMode){
                            _joe.reload(true);
                        }
                    }
                }
                for(var sc in self.Data){
                    capp && capp.Button.update(sc+'SchemaBtn',{counter:self.Data[sc].length});
                }
                if(specs.onServerUpdate){
                    specs.onServerUpdate();
                }
            }catch(e){
                self.Error.add('error handling update',e);
            }

        },
        save:function(obj,oldObj,changes){
            self.SERVER.socket.emit('save',{item:obj});
        },
        delete:function(obj,oldObj,changes){
            self.SERVER.socket.emit('delete',{item:obj});
        },
        save_ajax:function(obj,oldObj,changes){
              ajax({
                  url:'/API/save/',
                  dataType:'jsonp',
                  onsuccess:function(data){
                      if(data.error){
                          alert('Error Saving:\n'+data.error);
                        //TODO:ROLLBACK
                      }
                      else{
                          if($.type(data.results) == "object"){
                              var result = data.results;
                              var itemtype = (result.itemtype || self.current.schema.name);
                              self.showMessage( itemtype+ ', ' + result.name+'<br/><b>saved successfully</b>');
                          }
                      }
                  },
                  data:obj
              })
        },
        updateObject:function(ajaxURL,specs){

            var specs = $.extend({
                oldObj:$.extend({},self.current.object),
                callback:function(data){
                    self.showMessage(data.name +' updated successfully');
                },
                ajaxObj: self.constructObjectFromFields(self.joe_index),
                overwrites:{},
                skipValidation:false
            },(specs||{}));
            var newObj = $.extend(
                ajaxObj.newObj,
                specs.overwrites,
                {joeUpdated:new Date().toISOString()}
            );

            var skipVal = _joe.propAsFuncOrValue(self.skipValidation);
            if(!skipVal) {
                var valid = self.validateObject(obj);
                if(!valid){return false;}
            }

            $.ajax({
                url:specs.ajaxObj,
                data:newObj,
                dataType:'jsonp',
                success:specs.callback
            })


        },
        loadSchema:function(schemaName,schemaObj){
            let sobj;
            sobj = $.extend({},getDefaultSchema(schemaName),schemaObj);
            unstringifyFunctions(sobj);

            if(!sobj.error) {
                    self.Schema.add(schemaName, sobj);
            }else{
                self.Error.add(sobj.error,null);
            }
            return sobj;
        },
        loadSchemas:function(collectionsSTR,callback){
            if($.type(collectionsSTR) == "Array"){
                collectionsSTR = collectionsSTR.join(',');
            }
            ajax({url:'/API/schema/'+collectionsSTR,dataType:'jsonp',onsuccess:function(data){

                if(data.error){
                    alert(data.error);
                }else{
                    
                    //var html = '';

                    var defSchema = function(schema){
                        var menu = [];
                        if(__jsu && ['super','admin','editor'].indexOf(__jsu.role) != -1 
                        || $c.isEmpty(self.Data.user)){
                            menu = [
                                __exportBtn__,
                                _joe.SERVER.History.button,
                                __quicksaveBtn__,
                                __deleteBtn__,
                                __duplicateBtn__
                            ];
                        }
                        return{
                            subtitle:'${_subsetName}',
                            callback:self.SERVER.save,
                            onsave:function(item){
                                let current = _jco(true);
                                if(/*item.status && */current && item[self.getIDProp(item.itemtype)] == current._id){
                                   
                                    self.Fields.rerender('status,updated,timestamp');
                                }
                                
                            },
                            new:function(){return {
                                _id:cuid(),
                                itemtype:schema,
                                created:(new Date).toISOString()
                            };},
                            menu:menu
                        }
                    };
                    var sobj;
                    var default_schemas = [];
                    for(var schema in data.schemas){
                        sobj = self.SERVER.loadSchema(schema,data.schemas[schema])
                        if(!sobj.error) {
                            if(sobj.default_schema){
                                default_schemas.push(sobj);
                                
                            }else{
                                capp.Button.addFromSchema(self.Schema.add(schema, sobj));
                            }
                        }else{
                            self.Error.add(sobj.error,null);
                        }
                        // sobj = $.extend({},getDefaultSchema(schema),data.schemas[schema]);
                        // unstringifyFunctions(sobj);

                        // if(!sobj.error) {
                        //     if(sobj.default_schema){
                        //         self.Schema.add(schema, sobj);
                        //         default_schemas.push(sobj);
                                
                        //     }else{
                        //         capp.Button.addFromSchema(self.Schema.add(schema, sobj));
                        //     }
                        // }else{
                        //     self.Error.add(sobj.error,null);
                        // }
                    }
                    /*TODO: add menu for additional schemas */
                    capp.Menu.add('<capp-button-icon >'+_joe.SVG.icon.menu+' </capp-button-icon><capp-menu-icon-label>default Schemas</capp-menu-icon-label> ',
                    default_schemas,'capp-panel',{
                        labelcss:'no-padding',
                        cssclass:'default-schemas-menu',
                        template:function(sc_btn){
                            var so = self.schemas[sc_btn.name];
                            //return 't';
                            return capp.Button.addFromSchema(so,{container:false});
                        }
                    });
                    
                    if(data.fields){
                        unstringifyFunctions(data.fields);
                        self.specs.fields = self.fields = data.fields;
                    }


                }
                self.SERVER.ready.schemas = true;
                self.SERVER.onLoadComplete();
                callback && callback();
            }});
        },
        loadDatasets:function(collectionsSTR){

            if (self.specs.localStorage && typeof(Storage) !== "undefined") {
                self.SERVER.LocalStorage.loadData();
                var toget = [];
                APPINFO.collections.map(function(coll){
                    if(!_joe.Data[coll]){
                        toget.push(coll);
                    }
                });
                self.SERVER.LocalStorage.getUpdates();
                if(toget.length){
                    logit('retrieving '+toget.length +' datasets ');
                    self.SERVER.getDatasets(toget);
                    
                }else{
                    self.SERVER.ready.datasets = true;
                    self.SERVER.onLoadComplete();
                }
            }else{
                self.SERVER.ready.localstorage = true;
                self.SERVER.getDatasets(collectionsSTR);
            }
        },
        loadAdditionalItems:function(items){
            if(!items || !items.length){
                self.SERVER.ready.additionalItems = true;
                self.SERVER.onLoadComplete();
                return;
            }
            var query = {_id:{$in:items}};
            //get all the items from the server
            ajax({url:'/API/search/',
                data:{query:query},
                dataType:'jsonp',
            onsuccess:function(data){
                var items = data.results || [];
                var schemas = [];
                var datasets = {}
                items.map(i=>{
                    if(__jsu.schemas.indexOf(i.itemtype) == -1){
                        datasets[i.itemtype] = datasets[i.itemtype] || [];
                        datasets[i.itemtype].push(i);
                        schemas.push(i.itemtype);
                    }
                })
                    schemas = Array.from(new Set(schemas));

                
                //get their schemas
                ajax({url:'/API/schema/'+schemas.join(','),
                    dataType:'jsonp',
                    data:{mode:'additionalItems'},
                    onsuccess:function(data){

                        if(data.error){
                            alert(data.error);
                        }else{
                            
                        
                            var sobj;
                            for(var schema in data.schemas){
                                sobj = self.SERVER.loadSchema(schema,data.schemas[schema])
                                //add datasets to the system
                                if(!self.Data[schema] || !self.Data[schema].length){
                                    self.addDataset(schema,datasets[schema]);
                                }
                            }
                        }


                        self.SERVER.ready.additionalItems = true;
                        self.SERVER.onLoadComplete();
                    }});

            }})
        },
        loadApp:function(){

        },
        getDatasets:function(collectionsSTR){
            if($.type(collectionsSTR) == "Array"){
                collectionsSTR = collectionsSTR.join(',');
            }
            ajax({url:'/API/datasets/'+collectionsSTR,dataType:'jsonp',onsuccess:function(data){
                if(data.error){
                    alert(data.error);
                }else{

                    for(var dataset in data.datasets){
                        self.addDataset(dataset,data.datasets[dataset]);
                        capp && capp.Button.update(dataset+'SchemaBtn',{counter:data.datasets[dataset].length});
                        //(data.datasets[dataset].length && dataset+' - '+data.datasets[dataset].length)||'');
                    }
                }
                self.SERVER.ready.datasets = true;
                self.SERVER.onLoadComplete();
            }});
        },
        LocalStorage:{
            saveData:function(){
                localStorage.joeData = JSON.stringify(_joe.Data);
                localStorage.timestamp = new Date().toISOString();
            },
            loadData:function(){
                if(localStorage.joeData){
                    var allData = JSON.parse(localStorage.joeData);
                    APPINFO.collections.map(function(coll){
                    if(coll == 'user' || (__jsu.schemas && __jsu.schemas.indexOf(coll) != -1)){
                            self.addDataset(coll,allData[coll]);
                        }
                    })
                }
            },
            
            getUpdates:function(dateISO){
                var dateISO = dateISO 
                || ((typeof(Storage) !== "undefined") && localStorage.timestamp) 
                || null;//new Date().toISOString();
                var query = {itemtype:{$in:APPINFO.collections}};
                if(dateISO){query.joeUpdated={"$gte":dateISO};}
                $.ajax({
                    url:'/API/search/',
                    dataType:'jsonp',
                    data:{query:query}, 
                    success:self.SERVER.LocalStorage.handleUpdates
                })
            },
            handleUpdates:function(data){
                
                if(data.error){
                    alert(data.error);
                }
                logit(data.results.length +' updates in '+data.benchmark);
                self.SERVER.handleUpdate(data);
                self.SERVER.ready.localstorage = true;
                self.SERVER.onLoadComplete();
            }
        },
        onLoadComplete:function(){


            if(self.SERVER.ready.datasets && self.SERVER.ready.schemas && self.SERVER.ready.user && self.SERVER.ready.localstorage ) {
                if(!self.SERVER.ready.additionalItems){
                    self.SERVER.loadAdditionalItems(__jsu.items);
                    return
                }
                for(var sc in self.Data){
                    capp && capp.Button.update(sc+'SchemaBtn',{counter:self.Data[sc].length});
                }
                self.SERVER.socket.on('message',function(message){
                    if(message.sender){
                        alert(message.sender+' -\n'+message.message);
                    }else{
                        alert(message);
                    }
                    
                });
                self.SERVER.socket.on('comments_updated',function(info){
                    if(self.current.object && 
                    info.itemid == self.current.object[self.getIDProp()]){
                        JOE.Fields.comments.ready()
                    }
                });
                self.SERVER.socket.on('log_data',function(data,name){
                    var name = name ||'';
                    console.log(name,data);
                });
                self.SERVER.socket.on('item_updated',_joe.SERVER.handleUpdate);
                self.SERVER.socket.on('save_successful',function(data){
                    var result = data.results;
                    if($.type(result) == "array" && result.length == 1) {
                        result = result[0];
                        self.showMessage(result.itemtype + ', ' + result.name + '<br/><b>saved successfully</b>');
                    }else{
                        self.showMessage(result.itemtype + ', ' + result.name + '<br/><b>saved successfully</b>');
                    }
                    if(self.schemas[result.itemtype] && self.schemas[result.itemtype].onsave){
                        self.schemas[result.itemtype].onsave(result);
                    }
                    
                });
               
                self.SERVER.socket.on('connect_answer',function(data){
                    logit('answer received');
                    var userid = data.user;
                    if(!self.CONNECT.offerinprogress){
                        logit('REREQUESTING: '+userid);
                        self.CONNECT.offerinprogress = true;
                        self.CONNECT.offerinterval = setInterval(function(){
                            logit('ICE state: '+self.CONNECT.RTC.iceConnectionState)
                            if(_joe.CONNECT.RTC.iceConnectionState !="completed"){
                                _joe.CONNECT.start(userid,true);
                            }else{
                                logit('stopping CONNECT interval - interval');
                                clearInterval(self.CONNECT.offerinterval);
                                self.CONNECT.offerinprogress = false;
                            }

                        },3000)
                    }
                    self.CONNECT.RTC.setRemoteDescription(new RTCSessionDescription(data.sdp));
                    //self.CONNECT.RTC.addStream(self.CONNECT.streams.local);
                }); 
                self.SERVER.socket.on('connect_offer',function(data){
                    var RTC = self.CONNECT.RTC;
                    self.CONNECT.userid = data.user;
                    // if(!confirm('connect request received')){
                    //     return;
                    // }
                    _joe.CONNECT.start(data.user,false)
                    logit(data);
                    RTC.setRemoteDescription(new RTCSessionDescription(data.sdp))
                    .then(function(){
                        RTC.createAnswer().then(
                            function(answer){
                                //logit(answer);
                                RTC.setLocalDescription(new RTCSessionDescription(answer));
                                self.SERVER.socket.emit('connect_answer',{
                                    user:_joe.User._id,
                                    target:data.user,
                                    sdp:answer,
                                    socketid:data.socketid
                                })
                            },
                            function(e){ console.log(e); alert(e)}
                        ),
                        function(e){ console.log(e); alert(e)}
                    })
                });
                _joe.Hash.read();
                if(specs.onServerLoadComplete){
                    specs.onServerLoadComplete();
                }
                if (self.specs.localStorage && typeof(Storage) !== "undefined") {  
                    self.SERVER.LocalStorage.saveData();
                }
            }
        },
        socket:(self.specs.socket && io(self.specs.socket,{rejectUnauthorized: false}) || null)
    };

/*-------------------------> SERVER <--------------------------------------*/

/*-------------------------------------------------------------------->
    //ASYNC File loading
<--------------------------------------------------------------------*/
    this.addStylesheet = function(href,specs,cancelAppend){
      var s = document.createElement('link');
      var specs = $.extend({rel:'stylesheet'},specs || {})
      $.extend(s,specs);
      s.href = href;

      if(!cancelAppend){
        document.head.appendChild(s);
      }
      return s;
    };
    this.addScript = function(src,specs,cancelAppend){
      var s = document.createElement('script');
      var specs = $.extend({type:'text/javascript'},specs || {})
      $.extend(s,specs);
      s.src = src;

      if(!cancelAppend){
        document.body.appendChild(s);
      }
      return s;
    };

/*-------------------------------------------------------------------->
     //Utils
 <--------------------------------------------------------------------*/
    this.nowISO = new Date().toISOString();
    var nd = new Date();
    this.times = {
    };
    nd.setHours(0,0,0,0);
    this.times.SOD = nd.toISOString();
    nd.setHours(24,0,0,0);
    this.times.EOD = nd.toISOString();
    this.Utils = {
        /*|{
        featured:true,
        description:'get the names setting, returns eval'd code or a function,
        specs:'raw,params,object'
        tags:'utils,settings'
    }|*/
        cleanString:function(str){
            return str.replace(/[^A-Z0-9]+/ig, "_");
        },
        stopPropagation:function(e){
            var e = e || window.event;
            if (e.stopPropagation) e.stopPropagation();
            if (e.preventDefault) e.preventDefault();
        },
        getPossibleValues:function(propertyName,schema,specs){
            //TODO:
            //array format
            //sort array
            var specs = $.extend({
                format:'object'
            },specs||{});

            if(specs.static && _joe.Cache.static[specs.static]){
                return _joe.Cache.static[specs.static];
            }
            var types = {},propval;
           // _joe.Data[schema].map(function(a){
            for(var i=0;i<_joe.Data[schema].length;i++){
                let a = _joe.Data[schema][i];
                propval = a[propertyName];
                if(specs.allowNulls || propval != null){
                    types[propval] = types[propval] || 0;
                    types[propval]++;
                }
            }
            //})
            if(specs.static){
                _joe.Cache.static[specs.static] = types;
            }
            return types;
        },
        Settings:function(settingName,specs){
            var specs = specs ||{};
            var s = (self.Data.setting||[]).where({name:settingName})[0];
            var setting = (s||{});
            if(specs.object){return setting;}
            if(setting.setting_type =="code" && !specs.raw){
                if(setting.value.trim().indexOf('function') == 0 && specs.params){
                    var p = tryEval(s.value);
                    if(!p){
                        return specs.params;
                    }
                     return p(specs.params)
                }
                return tryEval(s.value)
                
            }
            return setting.value;
        },
        toDollars:function(amount){
            return parseFloat(amount).toFixed(2);
        },
        joeState:self.getState,
        nextDay:function(x){
            var now = new Date();    
            now.setDate(now.getDate() + (x+(7-now.getDay())) % 7);
            return now;
        },
        toDateInput:function(date){
            var specs = specs || {};
            var dt = date || new Date();
            if(typeof dt == 'string'){
                dt = new Date(dt);
            }
            console.log(dt,date);
            var dts_str = '';
                var monthstr = (dt.getUTCMonth()+1);
                monthstr = (monthstr < 10 ? '0' + monthstr : monthstr);
                var dayStr =('0'+dt.getUTCDate()).slice(-2);
                var yearStr = dt.getUTCFullYear().toString();
                return `${yearStr}-${monthstr}-${dayStr}`;

        },
        toDateString:function(dt,specs){
            if(specs == "Y-m-d"){
                return self.Utils.toDateInput(dt);
            }
            var specs = specs || {};
            var dt = dt || new Date();
            if(typeof dt == 'string'){
                dt = new Date(dt);
            }
            var dts_str = '';
                var monthstr = (dt.getUTCMonth()+1);
                var dayStr =('0'+dt.getUTCDate()).slice(-2);
                var yearStr = dt.getUTCFullYear().toString();
                dts_str += (monthstr < 10 ? '0' + monthstr : monthstr)
                +'/'+dayStr
                +'/'+((specs.fullyear && yearStr)||yearStr.substr(2,4));
                return dts_str;
        },
        prettyPrintDTS:function(dts,specs){
            var specs = $.extend({
                showtime:true,
                showdate:true
            },(specs||{}));
            var dts = dts || new Date();
            if(typeof dts == 'string'){
                dts = new Date(dts);
            }
            var dts_str = '';
            if(specs.showtime){
                var ampm = 'a';
                var dmins= dts.getMinutes().toString();
                var dhours = dts.getHours();
                if(dhours > 12){
                    ampm = 'p';
                    dhours -= 12;
                }
                if(!dhours){
                    dhours = 12;
                }
                dts_str += dhours
                +':'+((dmins.length == 2)?dmins:'0'+dmins)+ampm;
            }

            if(specs.showdate && specs.showtime){
                dts_str +=' ';
            }
            if(specs.showdate){
                dts_str += (dts.getMonth()+1)
                +'/'+dts.getDate()
                +'/'+dts.getUTCFullYear().toString().substr(2,4);
            }
            return dts_str;
        },
        getRandomColor:function() {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++ ) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        },
        benchmarkit:function(torun,name){
            var bm = new Benchmarker();
            var t = torun();
            _bmResponse(bm,'benchmarker',name||'');
            return t;
        }
    };
   /*-------------------------------------------------------------------->
     //SVGs
 <--------------------------------------------------------------------*/
 this.SVG = {
     icon:{
        'favorite':'<?xml version="1.0" encoding="UTF-8"?><svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M0 0h24v24H0z" fill="none"/><path d="M4 2h16a1 1 0 0 1 1 1v19.276a.5.5 0 0 1-.704.457L12 19.03l-8.296 3.702A.5.5 0 0 1 3 22.276V3a1 1 0 0 1 1-1zm8 11.5l2.939 1.545-.561-3.272 2.377-2.318-3.286-.478L12 6l-1.47 2.977-3.285.478 2.377 2.318-.56 3.272L12 13.5z"/></svg>',
         'left':'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-5 -5 36 36"><path d="M11.3 21C7.1 18.2 3.3 13.8 3.2 13.6 3.1 13.5 3 13.2 3 13 3 12.8 3.1 12.5 3.2 12.3 3.3 12.1 7.2 7.7 11.3 4.9 11.6 4.7 12 4.7 12.4 4.8 12.8 5.1 13 5.4 13 5.8L13 9.5C13 9.5 21.7 10 22.1 10.3 22.7 10.7 23 12 23 13 23 14.1 22.6 15.2 22.1 15.6 21.7 15.9 13 16.5 13 16.5L13 20.2C13 20.6 12.8 20.9 12.4 21.1 12.1 21.3 11.7 21.2 11.3 21Z"/></svg>',
         'cancel':'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-5 -5 36 36"><path d="M13 0.2C5.9 0.2 0.2 5.9 0.2 13 0.2 20.1 5.9 25.8 13 25.8 20.1 25.8 25.8 20.1 25.8 13 25.8 5.9 20.1 0.2 13 0.2ZM18.8 17.4L17.4 18.8C17.1 19 16.7 19 16.5 18.8L13 15.3 9.5 18.8C9.3 19 8.9 19 8.6 18.8L7.2 17.4C7 17.1 7 16.7 7.2 16.5L10.7 13 7.2 9.5C7 9.3 7 8.9 7.2 8.6L8.6 7.2C8.9 7 9.3 7 9.5 7.2L13 10.7 16.5 7.2C16.7 7 17.1 7 17.4 7.2L18.8 8.6C19 8.9 19 9.3 18.8 9.5L15.3 13 18.8 16.5C19 16.7 19 17.1 18.8 17.4Z"/></svg>',
         close:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-6 -6 44 44"><path d="M26 22.3L22.3 26c0 0-5.9-6.3-6.3-6.3C15.6 19.7 9.7 26 9.7 26L6 22.3c0 0 6.3-5.8 6.3-6.3C12.3 15.5 6 9.7 6 9.7l3.7-3.7c0 0 5.9 6.3 6.3 6.3 0.4 0 6.3-6.3 6.3-6.3L26 9.7c0 0-6.3 5.9-6.3 6.3C19.7 16.4 26 22.3 26 22.3z"/></svg>',
         unsaved:
            '<svg xmlns="http://www.w3.org/2000/svg" viewBox="-8 -8 48 48"><g data-name="Layer 2"><path d="M24.4 3.4h-21V28.6H28.6V7.5Zm-5 2.8v4.5H16.6V6.1Zm6.5 19.7H6.1V6.1h3.5v7.2H22.2V6.1h1.1l2.6 2.5Z"/><polygon points="20.9 22.5 18 19.6 20.9 16.7 18.9 14.7 16 17.6 13.1 14.7 11.1 16.7 14.1 19.6 11.1 22.5 13.1 24.5 16 21.5 18.9 24.5 20.9 22.5"/></g></svg>',
         'sidebar-right':
            '<svg xmlns="http://www.w3.org/2000/svg" viewBox="-4 -4 48 48"><g><path d="M38,36H2V4H38ZM26,6H4V34H26Z"/></g></svg>',
         'sidebar-left':
            '<svg xmlns="http://www.w3.org/2000/svg" viewBox="-4 -4 48 48"><path d="M2 4H38V36H2ZM14 34H36V6H14Z"/></svg>',
         'menu':
            '<svg xmlns="http://www.w3.org/2000/svg" viewBox="-5 -5 42 42"><path d="M16 6C14.9 6 14 6.9 14 8 14 9.1 14.9 10 16 10 17.1 10 18 9.1 18 8 18 6.9 17.1 6 16 6zM16 14C14.9 14 14 14.9 14 16 14 17.1 14.9 18 16 18 17.1 18 18 17.1 18 16 18 14.9 17.1 14 16 14zM16 22C14.9 22 14 22.9 14 24 14 25.1 14.9 26 16 26 17.1 26 18 25.1 18 24 18 22.9 17.1 22 16 22z"/></svg>',
         'sections':
            '<svg xmlns="http://www.w3.org/2000/svg" viewBox="-10 -10 120 120"><polygon points="50 90 35 75 65 75 50 90"/><polygon points="50 10 65 25 35 25 50 10"/><circle cx="50" cy="50" r="10"/></svg>',
        'help':
        '<svg viewBox="-384 -384 1792 1792" style=""><g><path d="M448 768h128v-128h-128v128zM512 256c-96 0-192 96-192 192h128c0-32 32-64 64-64s64 32 64 64c0 64-128 64-128 128h128c64-22 128-64 128-160s-96-160-192-160zM512 0c-283 0-512 229-512 512s229 512 512 512 512-229 512-512-229-512-512-512zM512 896c-212 0-384-172-384-384s172-384 384-384 384 172 384 384-172 384-384 384z"></path></g></svg>'
     }
 };



/*-------------------------------------------------------------------->
     //JCO - current object
<--------------------------------------------------------------------*/
    self.getCurrentObject = function(construct,force){
        /*|{
        featured:true,
        description:'gets the current object JOE is editing, if construct, has current user updates.',
        alias:'_jco(construct)'
        }|*/
        if(construct){
            if(self.current.constructed){
                return self.current.constructed;
            }
            var obj = self.constructObjectFromFields();
            //self.current.construct = obj;
            if(!obj[self.getIDProp()] && !force){
                return self.current.object;
            }
            return obj;
        }
        return self.current.object;
    };
   // if(window){
        window._jco = self.getCurrentObject;

   // }
/*-------------------------------------------------------------------->
     //Colors
<--------------------------------------------------------------------*/
    this.Colors={}
    this.Colors.priority = {
        1:'#cc4500',
        2:'#FFee33',
        3:'#acf'
    }

/*-------------------------------------------------------------------->
     //Snippets
<--------------------------------------------------------------------*/
this.Snippets={}
this.Snippets.priorityStripeColor = function(item){
    if(item.priority && item.priority < 100){
        return {
            title:`P${item.priority}`,
            color:_joe.Colors.priority[item.priority]
        };
    }
},
/*-------------------------------------------------------------------->
     //GOOGLE - GOOGLE CLIENT
<--------------------------------------------------------------------*/
    this.Google = {
        Calendar:{
            list:function(){
                if(!self.Google.Calendar.calendars){
                    self.Google.loadCalendarApi();
                    self.Google.Calendar.list();
                    /*
                gapi.auth.authorize({
                            'client_id': self.Google.client_id,
                            'scope': self.Google.scopes.join(' '),
                            'immediate': false
                        }, function(authResult){
                                if (authResult && !authResult.error) {
                                    console.log(authResult);
                                    self.Google.loadCalendarApi();
                                    self.Google.Calendar.list();
                                }else{
                                    alert('error authorizing calendars');
                                    self.Error.add('google client:'+authResult.error+','+authResult.error_subtype,
                                    authResult.error+','+authResult.error_subtype,{c:client_id,g:gapi,auth:gapi.auth,authResult})
                                }
                        })*/
                return;
            }
                goJoe(_joe.Google.Calendar.calendars,{schema:{
                    idprop:'id',
                    listWindowTitle:'Google Calendars',
                    title:'gcal: ${summary}',
                    listTitle:'<joe-title>${summary}</joe-title><joe-subtitle>${id}</joe-subtitle>',
                    itemMenu:function(item){
                        var buttons = [{name:'view',action:'window.open(\'https://calendar.google.com/calendar/embed?mode=WEEK&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=${id}&amp;color=%236B3304&amp;ctz=America%2FChicago\');'}];
                        return buttons;
                    },
                    sorter:['summary'],
                    checkChanges:false,
                    stripeColor:function(cal){
                        return cal.backgroundColor;
                    },
                    bgColor:function(cal){
                        if(_joe.Google.Calendar && _joe.Google.Calendar.active){
                            if(_joe.Google.Calendar.active.id == cal.id){
                                return 'goldenrod';
                            }
                        }
                        return '';
                    }
                }})
            }
        },
        client_id:null,
        init:function(){
            console.log('google authed',self.Google.access_token);
            self.Google.listCalendars();
        },
        scopes:["https://www.googleapis.com/auth/calendar"],
        loadCalendarApi:function(access_token,callback){
            if(access_token){
                self.Google.access_token = access_token;
            }
            var callback = callback ||self.Google.init;
            gapi.client.load('calendar', 'v3', callback);
        },
        listCalendars:function(){
            var request = gapi.client.calendar.calendarList.list();
            request.execute(function(resp){
                    self.Google.Calendar.calendars = resp.items;
                    self.Google.Calendar.active  = self.Utils.Settings('GOOGLE_CALENDARID') && resp.items.where({id:self.Utils.Settings('GOOGLE_CALENDARID')})[0]
                        || resp.items.where({primary:true})[0] || false;
                        
                    capp && capp.Reload.all();
            });
        }
    };
    this.Google.authorize = function(immediate,callback,hidePopup){
        var client_id = self.Google.client_id = self.Utils.Settings('GOOGLE_CLIENTID');
        if(self.Google.access_token){
            self.Google.loadCalendarApi();
            return;
        }
        if(client_id && gapi && gapi.auth ){
            
            gapi.auth.authorize(
            {
                'client_id': self.Google.client_id,
                'scope': self.Google.scopes.join(' '),
                'immediate': true
            }, function(authResult){
                    if (authResult && !authResult.error) {
                        self.Google.loadCalendarApi(authResult.access_token);
                    } else {
                        if(JACmode){
                            var doit = confirm('jac mode enabled, login to google?');
                            if(!doit) return;
                        }
                        gapi.auth.authorize({
                            'client_id': self.Google.client_id,
                            'scope': self.Google.scopes.join(' '),
                            'immediate': false
                        }, function(authResult){
                                if (authResult && !authResult.error) {
                                    
                                    self.Google.loadCalendarApi(authResult.access_token);
                                    
                                }else{
                                    self.Error.add('google client:'+authResult.error+','+authResult.error_subtype,
                                    authResult.error+','+authResult.error_subtype,{c:client_id,g:gapi,auth:gapi.auth,authResult:authResult})
                                }
                        })
                    }
            });
        }else{
            self.Error.add('error in google client api',null,{c:client_id,g:gapi,auth:gapi.auth})
        }
    };
    function parseDateTimeForGoogle(){

    }
    this.Google.Calendar.Event = {
        buildResource:function(event){
            var template = 
                self.Utils.Settings('GOOGLE_EVENT_TEMPLATE',{params:event})
                ||'${info} \r\n ${description}';

            // var descriptionString =__removeTags(fillTemplate(template,$.extend({
            //     URL:'//'+__jsc.hostname+':'+__jsc.port+location.pathname+location.hash
            // },event)));
            var descriptionString =fillTemplate(template,$.extend({
                URL:'//'+__jsc.hostname+':'+__jsc.port+location.pathname+location.hash
            },event));
            var res = {
                sendNotifications:event.sendNotifications || false,
                summary:event.name||event.address+' '+(event.event_type||''),
                description:descriptionString,
                location: event.location || fillTemplate('${street_address} ${city} ${state} ${zip}',event),
                "start": {
                    "dateTime": $c.format(new Date(event.date+' '+event.start_time),'Y-m-d')
                    +'T'
                    +$c.format(new Date(event.date+' '+event.start_time),'H:i:00.000')
                    +event.timezone_offset
                },
                "end": {
                    "dateTime": $c.format(new Date(event.date+' '+event.end_time),'Y-m-d')
                    +'T'
                    +$c.format(new Date(event.date+' '+event.end_time),'H:i:00.000')
                    +event.timezone_offset
                },
                id:event._id.replace(/-/g,''),
                attendees:[]
            };
            var lead_person = event.leader || event.lead;
            if(lead_person){
                var leader = self.Cache.get(lead_person);
                res.attendees.push({
                    name:(leader.fullname || (leader.first && (leader.first+' '+leader.last))||leader.name),
                    id:leader._id,
                    email:leader.email
                });
            }
            (event.attendees || event.team_members|| []).map(function(att){
                var attendee = self.Cache.get(att);
                res.attendees.push({
                    name:(attendee.fullname 
                        || (attendee.first && (attendee.first+' '+attendee.last))
                        || attendee.name),
                    id:attendee._id,
                    email:attendee.email
                });
            });
            return res;
        },
        getObject:function(eventid){
            if(self.current.object && self.current.object._id == eventid){
                return _jco(true);
            }else{
                return self.getDataItem(eventid,'event');
            }
        },
        add:function(eventid,force,saveFirst){
            if(!self.Google.access_token || !self.Google.Calendar.calendars){
                alert('please login to your google account and then try again.')
                self.Google.authorize(true);
                return;
            }
            try{
                var event = self.Google.Calendar.Event.getObject(eventid);
                
                if(!event){
                    alert('event not found: '+eventid);
                    return;
                }
                if(!event.published && !force){
                    alert('event not published: '+event.name);
                    return;
                }
                var resource = self.Google.Calendar.Event.buildResource(event);
                
                var request = gapi.client.calendar.events.insert({
                    'calendarId': self.Google.Calendar.active.id,
                    'resource': resource,
                    'sendNotifications':resource.sendNotifications
                });
                request.sendNotifications =resource.sendNotifications;
                request.execute(function(resp) {
                    if(resp.error && resp.error.code == 409){
                        logit('updating');
                        var ovr = {status:'confirmed'};
                        if(!event.published){
                            ovr.status = "cancelled"
                        }
                        ovr.save = saveFirst;
                        self.Google.Calendar.Event.update(eventid,ovr)
                    }else{
                        if(saveFirst){
                            _joe.current.object.published_ts = new Date(); 
                            self.updateObject(null,null,true);
                            _joe.rerenderField('published_ts');
                        }
                    }
                    logit(resp);
                });
            }catch(e){
                self.Error.add('error adding event',e,eventid);
                alert('error adding event \n'+e);
            }
        },
        update:function(eventid,specs){
            try{
                var saveFirst = specs.save;
                //var event = self.getDataItem(eventid,'event');
                var event = self.Google.Calendar.Event.getObject(eventid);
                var resource = $.extend(self.Google.Calendar.Event.buildResource(event),(specs||{status:'confirmed'}));
                var request = gapi.client.calendar.events.patch({
                    'calendarId': self.Google.Calendar.active.id,
                    'resource': resource,
                    'eventId':eventid.replace(/-/g,''),
                    'sendNotifications':resource.sendNotifications
                });
                request.sendNotifications =resource.sendNotifications;
                request.execute(function(resp) {
                    if(resp.error){
                        alert(resp.error.message || resp.error);
                    }else{
                        if(saveFirst){
                            _joe.current.object.published_ts = new Date(); 
                            self.updateObject(null,null,true);
                            _joe.rerenderField('published_ts');
                        }
                    }
                    logit(resp);
                });
            }catch(e){
                self.Error.add('error updating event',e,eventid);
            }
        },
        publish:function(query){
            if(!self.Google.Calendar.calendars || !self.Google.access_token){
                gapi.auth.authorize({
                            'client_id': self.Google.client_id,
                            'scope': self.Google.scopes.join(' '),
                            'immediate': false
                        }, function(authResult){
                                if (authResult && !authResult.error) {
                                    self.Google.loadCalendarApi();
                                    self.Google.Calendar.Event.publish();
                                }else{
                                    alert('error authorizing calendars');
                                    self.Error.add('google client:'+authResult.error+','+authResult.error_subtype,
                                    authResult.error+','+authResult.error_subtype,{c:client_id,g:gapi,auth:gapi.auth,authResult:authResult})
                                }
                        })
                return;
            }
            var query = query || {published:true};
            if(self.Data.event){
                var topublish = self.Data.event.where(query);
                var notpublish = self.Data.event.where(function(){ return !this.published;});
                if(confirm('publish '+topublish.length+' events?\n you have '+notpublish.length+'/'+self.Data.event.length+' unpublished events')){
                    topublish.map(function(ev){
                        self.Google.Calendar.Event.add(ev._id);
                    })
                }
            }   
        }
    }

   /*-------------------------------------------------------------------->
     //AUTOINIT
 <--------------------------------------------------------------------*/

	if(self.specs.autoInit){
		self.init();
	}
	return this;
}
function unstringifyFunctions(propObject){
    for(var p in propObject){
        if(typeof propObject[p] == 'string' && propObject[p].indexOf('(function') ==0 ){
            propObject[p] = tryEval('('+propObject[p].replace(/;r\n/g,';\n').replace(/<br \/>/g,'')+')');
           //propObject[p] = eval('('+propObject[p].replace(/<br \/>/g,'')+')');

        }else if(typeof propObject[p] == "object"){
            unstringifyFunctions(propObject[p]);
        }
    }
    return propObject;
}

var __gotoJoeSection;
var __clearDiv__ = '<div class="clear"></div>';

// var __createBtn__ = {name:'create',label:'Create', action:'_joe.createObject();', css:'joe-orange-button joe-add-button joe-iconed-button'};
var __createBtn__ = {name:'create', icon:'plus', action:'create', color:"orange", 
schema:function(){
    return (_joe.current.schema && _joe.current.schema.name);
}, css:' joe-add-button joe-iconed-button'};

var __quicksaveBtn__ = {name:'quicksave',label:'Save', action:'_joe.updateObject(this,null,true);', css:'joe-quicksave-button joe-confirm-button joe-iconed-button'};
var __saveBtn__ = {name:'save',label:'Save', action:'_joe.updateObject(this);', css:'joe-save-button joe-confirm-button joe-iconed-button'};
var __deleteBtn__ = {name:'delete',label:'Delete',action:'_joe.deleteObject(this);',
    css:'joe-delete-button joe-iconed-button joe-red-button',
    condition:function(){
        return (_joe.isNewItem && !_joe.isNewItem());
    }};
var __multisaveBtn__ = {name:'save_multi',label:'Multi Save', action:'_joe.updateMultipleObjects(this);', css:'joe-save-button joe-confirm-button joe-multi-only'};
var __multideleteBtn__ = {name:'delete_multi',label:'Multi Delete',action:'_joe.deleteMultipleObjects(this);', css:'joe-delete-button joe-multi-only'};
var __selectAllBtn__ = {name:'select_all',label:'select all',action:'_joe.selectAllItems();', css:'joe-left-button joe-multi-always'};

var __replaceBtn__ = {name:'replace',label:'Replace', action:'_joe.updateRendering(this);', css:'joe-replace-button joe-confirm-button'};
var __duplicateBtn__ = {name:'duplicate',label:'Duplicate', action:'_joe.duplicateObject();', css:'joe-left-button joe-iconed-button joe-duplicate-button'};
var __exportBtn__ = {name:'export', display:'<b>{&#8593;}</b>',condition:function(){
    return (_joe.User && _joe.User.role == "super");
},
css:'joe-export-button joe-left-button joe-grey-button joe-orangegrey-button', action:'_joe.IO.export(null,true)'};

var __systemFieldsSection = [
    {section_start:'system', collapsed:true},
    '_id','created','itemtype',
    {section_end:'system'}
];
var __defaultButtons = [__createBtn__];
var __defaultObjectButtons = [__deleteBtn__,__saveBtn__];
var __defaultMultiButtons = [__multisaveBtn__,__multideleteBtn__];
function __removeTags(str){
    if(!str){return '';}
    return str.replace(/<(?:.|\n)*?>/gm, '');
}

function _COUNT(array){
    if(!array){
        return 0;
    }
	if(array.isArray()) {
		return array.length;
	}else if(array.isString()){
        return array.length;
    }else if(array.isFunction){
        return array.toString().length;
    }
	return 0;
};

function showJOEWarnings(){
    goJoe(_joe.current.benchmarkers._warnings,{
        schema:{
            title:'benchmarker warnings',
            listTitle:'<joe-title>${name}</joe-title><joe-subtitle>${info}</joe-subtitle>',
            checkChanges:false
        }
    });
}
function _bmResponse(benchmarker,message,bmname,threshhold){
    var t = benchmarker.stop();
    var m = message + ' in ' + t + ' secs';
    _joe.current.benchmarkers = _joe.current.benchmarkers || {};
    var name = (bmname || message);
    var payload = {
        info:t,
        name:name,
        _id:cuid()
    };
    _joe.current.benchmarkers._warnings = _joe.current.benchmarkers._warnings ||[];
    _joe.current.benchmarkers[name] = payload;
    if($c.DEBUG_MODE && window.console && window.console.warn){
        if(t > .4){
            window.console.warn(m);
            _joe.current.benchmarkers[name].warn = true;
            _joe.current.benchmarkers._warnings.push(payload);
        }else {
            if(!threshhold || t > threshhold){
                logit(m);
            }
        }
    }else {
        logit(m);
    }
}

function beginLogGroup(name,expanded){
    if(!$c.DEBUG_MODE)return;
    if(expanded){
        window.console && window.console.group && console.group(name);
    }else{
        window.console && window.console.groupCollapsed && console.groupCollapsed(name);
    }
}
function endLogGroup(){
    if(!$c.DEBUG_MODE)return;
    window.console && window.console.groupEnd &&console.groupEnd();
}

function _renderUserCubes(user,cssclass,user_dataset){
    var css = cssclass||'fleft';

    var u = user;
    var html ='';
    var title,initials;
    var color = user.color || '#cbcbcb';
    if(u.first && u.last){
        title = u.first+' '+u.last;
        initials = u.first[0]+u.last[0];
    }else{
        var name = u.fullname || u.name;
        title = name;
        initials = name[0]+ (((name.indexOf(' ') > 0) && name[name.indexOf(' ')+1])||'');
    }
    html += '<joe-user-cube title="'+ title+'" class="'+css+'" style="background-color:'+color+'">'+initials+' <span>'+ title+'</span></joe-user-cube>';

    return html;

}
/*-------------------------------------------------------------------->
 Watch Polyfill
 <--------------------------------------------------------------------*/

/*
 * object.watch polyfill
 *
 * 2012-04-03
 *
 * By Eli Grey, http://eligrey.com
 * Public Domain.
 * NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.
 */
/*
// object.watch
if (!Object.prototype.observeProp && Object.defineProperty) {
    Object.defineProperty(Object.prototype, "watch", {
        enumerable: false
        , configurable: true
        , writable: false
        , value: function (prop, handler) {
            var
                oldval = this[prop]
                , newval = oldval
                , getter = function () {
                    return newval;
                }
                , setter = function (val) {
                    oldval = newval;
                    handler = handler || logit;
                    var specs = {object:this, prop:prop, oldval:oldval, val:val};
                    handler(specs);
                    return newval = val;
                    //return
                }
                ;

            if (delete this[prop]) { // can't watch constants
                Object.defineProperty(this, prop, {
                    get: getter
                    , set: setter
                    , enumerable: true
                    , configurable: true
                });
            }
        }
    });
}

// object.unwatch
if (!Object.prototype.observeProp && Object.defineProperty) {
    Object.defineProperty(Object.prototype, "unwatch", {
        enumerable: false
        , configurable: true
        , writable: false
        , value: function (prop) {
            var val = this[prop];
            delete this[prop]; // remove accessors
            this[prop] = val;
        }
    });
}*/
/*-------------------------------------------------------------------->
CRAYDENT UPDATES
 <--------------------------------------------------------------------*/

//UNTIL VERBOSE IS REMOVED


function warn(message){
    console && console.warn && console.warn(message);
}

function formatMoney(number, places, symbol, thousand, decimal) {
    number = number || 0;
    places = !isNaN(places = Math.abs(places)) ? places : 2;
    symbol = symbol !== undefined ? symbol : "$";
    thousand = thousand || ",";
    decimal = decimal || ".";
    var negative = number < 0 ? "-" : "",
        i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
}/*
 Leaflet, a JavaScript library for mobile-friendly interactive maps. http://leafletjs.com
 (c) 2010-2013, Vladimir Agafonkin
 (c) 2010-2011, CloudMade
*/
!function(t,e,i){var n=t.L,o={};o.version="0.7.2","object"==typeof module&&"object"==typeof module.exports?module.exports=o:"function"==typeof define&&define.amd&&define(o),o.noConflict=function(){return t.L=n,this},t.L=o,o.Util={extend:function(t){var e,i,n,o,s=Array.prototype.slice.call(arguments,1);for(i=0,n=s.length;n>i;i++){o=s[i]||{};for(e in o)o.hasOwnProperty(e)&&(t[e]=o[e])}return t},bind:function(t,e){var i=arguments.length>2?Array.prototype.slice.call(arguments,2):null;return function(){return t.apply(e,i||arguments)}},stamp:function(){var t=0,e="_leaflet_id";return function(i){return i[e]=i[e]||++t,i[e]}}(),invokeEach:function(t,e,i){var n,o;if("object"==typeof t){o=Array.prototype.slice.call(arguments,3);for(n in t)e.apply(i,[n,t[n]].concat(o));return!0}return!1},limitExecByInterval:function(t,e,i){var n,o;return function s(){var a=arguments;return n?void(o=!0):(n=!0,setTimeout(function(){n=!1,o&&(s.apply(i,a),o=!1)},e),void t.apply(i,a))}},falseFn:function(){return!1},formatNum:function(t,e){var i=Math.pow(10,e||5);return Math.round(t*i)/i},trim:function(t){return t.trim?t.trim():t.replace(/^\s+|\s+$/g,"")},splitWords:function(t){return o.Util.trim(t).split(/\s+/)},setOptions:function(t,e){return t.options=o.extend({},t.options,e),t.options},getParamString:function(t,e,i){var n=[];for(var o in t)n.push(encodeURIComponent(i?o.toUpperCase():o)+"="+encodeURIComponent(t[o]));return(e&&-1!==e.indexOf("?")?"&":"?")+n.join("&")},template:function(t,e){return t.replace(/\{ *([\w_]+) *\}/g,function(t,n){var o=e[n];if(o===i)throw new Error("No value provided for variable "+t);return"function"==typeof o&&(o=o(e)),o})},isArray:Array.isArray||function(t){return"[object Array]"===Object.prototype.toString.call(t)},emptyImageUrl:"data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="},function(){function e(e){var i,n,o=["webkit","moz","o","ms"];for(i=0;i<o.length&&!n;i++)n=t[o[i]+e];return n}function i(e){var i=+new Date,o=Math.max(0,16-(i-n));return n=i+o,t.setTimeout(e,o)}var n=0,s=t.requestAnimationFrame||e("RequestAnimationFrame")||i,a=t.cancelAnimationFrame||e("CancelAnimationFrame")||e("CancelRequestAnimationFrame")||function(e){t.clearTimeout(e)};o.Util.requestAnimFrame=function(e,n,a,r){return e=o.bind(e,n),a&&s===i?void e():s.call(t,e,r)},o.Util.cancelAnimFrame=function(e){e&&a.call(t,e)}}(),o.extend=o.Util.extend,o.bind=o.Util.bind,o.stamp=o.Util.stamp,o.setOptions=o.Util.setOptions,o.Class=function(){},o.Class.extend=function(t){var e=function(){this.initialize&&this.initialize.apply(this,arguments),this._initHooks&&this.callInitHooks()},i=function(){};i.prototype=this.prototype;var n=new i;n.constructor=e,e.prototype=n;for(var s in this)this.hasOwnProperty(s)&&"prototype"!==s&&(e[s]=this[s]);t.statics&&(o.extend(e,t.statics),delete t.statics),t.includes&&(o.Util.extend.apply(null,[n].concat(t.includes)),delete t.includes),t.options&&n.options&&(t.options=o.extend({},n.options,t.options)),o.extend(n,t),n._initHooks=[];var a=this;return e.__super__=a.prototype,n.callInitHooks=function(){if(!this._initHooksCalled){a.prototype.callInitHooks&&a.prototype.callInitHooks.call(this),this._initHooksCalled=!0;for(var t=0,e=n._initHooks.length;e>t;t++)n._initHooks[t].call(this)}},e},o.Class.include=function(t){o.extend(this.prototype,t)},o.Class.mergeOptions=function(t){o.extend(this.prototype.options,t)},o.Class.addInitHook=function(t){var e=Array.prototype.slice.call(arguments,1),i="function"==typeof t?t:function(){this[t].apply(this,e)};this.prototype._initHooks=this.prototype._initHooks||[],this.prototype._initHooks.push(i)};var s="_leaflet_events";o.Mixin={},o.Mixin.Events={addEventListener:function(t,e,i){if(o.Util.invokeEach(t,this.addEventListener,this,e,i))return this;var n,a,r,h,l,u,c,d=this[s]=this[s]||{},p=i&&i!==this&&o.stamp(i);for(t=o.Util.splitWords(t),n=0,a=t.length;a>n;n++)r={action:e,context:i||this},h=t[n],p?(l=h+"_idx",u=l+"_len",c=d[l]=d[l]||{},c[p]||(c[p]=[],d[u]=(d[u]||0)+1),c[p].push(r)):(d[h]=d[h]||[],d[h].push(r));return this},hasEventListeners:function(t){var e=this[s];return!!e&&(t in e&&e[t].length>0||t+"_idx"in e&&e[t+"_idx_len"]>0)},removeEventListener:function(t,e,i){if(!this[s])return this;if(!t)return this.clearAllEventListeners();if(o.Util.invokeEach(t,this.removeEventListener,this,e,i))return this;var n,a,r,h,l,u,c,d,p,_=this[s],m=i&&i!==this&&o.stamp(i);for(t=o.Util.splitWords(t),n=0,a=t.length;a>n;n++)if(r=t[n],u=r+"_idx",c=u+"_len",d=_[u],e){if(h=m&&d?d[m]:_[r]){for(l=h.length-1;l>=0;l--)h[l].action!==e||i&&h[l].context!==i||(p=h.splice(l,1),p[0].action=o.Util.falseFn);i&&d&&0===h.length&&(delete d[m],_[c]--)}}else delete _[r],delete _[u],delete _[c];return this},clearAllEventListeners:function(){return delete this[s],this},fireEvent:function(t,e){if(!this.hasEventListeners(t))return this;var i,n,a,r,h,l=o.Util.extend({},e,{type:t,target:this}),u=this[s];if(u[t])for(i=u[t].slice(),n=0,a=i.length;a>n;n++)i[n].action.call(i[n].context,l);r=u[t+"_idx"];for(h in r)if(i=r[h].slice())for(n=0,a=i.length;a>n;n++)i[n].action.call(i[n].context,l);return this},addOneTimeEventListener:function(t,e,i){if(o.Util.invokeEach(t,this.addOneTimeEventListener,this,e,i))return this;var n=o.bind(function(){this.removeEventListener(t,e,i).removeEventListener(t,n,i)},this);return this.addEventListener(t,e,i).addEventListener(t,n,i)}},o.Mixin.Events.on=o.Mixin.Events.addEventListener,o.Mixin.Events.off=o.Mixin.Events.removeEventListener,o.Mixin.Events.once=o.Mixin.Events.addOneTimeEventListener,o.Mixin.Events.fire=o.Mixin.Events.fireEvent,function(){var n="ActiveXObject"in t,s=n&&!e.addEventListener,a=navigator.userAgent.toLowerCase(),r=-1!==a.indexOf("webkit"),h=-1!==a.indexOf("chrome"),l=-1!==a.indexOf("phantom"),u=-1!==a.indexOf("android"),c=-1!==a.search("android [23]"),d=-1!==a.indexOf("gecko"),p=typeof orientation!=i+"",_=t.navigator&&t.navigator.msPointerEnabled&&t.navigator.msMaxTouchPoints&&!t.PointerEvent,m=t.PointerEvent&&t.navigator.pointerEnabled&&t.navigator.maxTouchPoints||_,f="devicePixelRatio"in t&&t.devicePixelRatio>1||"matchMedia"in t&&t.matchMedia("(min-resolution:144dpi)")&&t.matchMedia("(min-resolution:144dpi)").matches,g=e.documentElement,v=n&&"transition"in g.style,y="WebKitCSSMatrix"in t&&"m11"in new t.WebKitCSSMatrix&&!c,P="MozPerspective"in g.style,L="OTransition"in g.style,x=!t.L_DISABLE_3D&&(v||y||P||L)&&!l,w=!t.L_NO_TOUCH&&!l&&function(){var t="ontouchstart";if(m||t in g)return!0;var i=e.createElement("div"),n=!1;return i.setAttribute?(i.setAttribute(t,"return;"),"function"==typeof i[t]&&(n=!0),i.removeAttribute(t),i=null,n):!1}();o.Browser={ie:n,ielt9:s,webkit:r,gecko:d&&!r&&!t.opera&&!n,android:u,android23:c,chrome:h,ie3d:v,webkit3d:y,gecko3d:P,opera3d:L,any3d:x,mobile:p,mobileWebkit:p&&r,mobileWebkit3d:p&&y,mobileOpera:p&&t.opera,touch:w,msPointer:_,pointer:m,retina:f}}(),o.Point=function(t,e,i){this.x=i?Math.round(t):t,this.y=i?Math.round(e):e},o.Point.prototype={clone:function(){return new o.Point(this.x,this.y)},add:function(t){return this.clone()._add(o.point(t))},_add:function(t){return this.x+=t.x,this.y+=t.y,this},subtract:function(t){return this.clone()._subtract(o.point(t))},_subtract:function(t){return this.x-=t.x,this.y-=t.y,this},divideBy:function(t){return this.clone()._divideBy(t)},_divideBy:function(t){return this.x/=t,this.y/=t,this},multiplyBy:function(t){return this.clone()._multiplyBy(t)},_multiplyBy:function(t){return this.x*=t,this.y*=t,this},round:function(){return this.clone()._round()},_round:function(){return this.x=Math.round(this.x),this.y=Math.round(this.y),this},floor:function(){return this.clone()._floor()},_floor:function(){return this.x=Math.floor(this.x),this.y=Math.floor(this.y),this},distanceTo:function(t){t=o.point(t);var e=t.x-this.x,i=t.y-this.y;return Math.sqrt(e*e+i*i)},equals:function(t){return t=o.point(t),t.x===this.x&&t.y===this.y},contains:function(t){return t=o.point(t),Math.abs(t.x)<=Math.abs(this.x)&&Math.abs(t.y)<=Math.abs(this.y)},toString:function(){return"Point("+o.Util.formatNum(this.x)+", "+o.Util.formatNum(this.y)+")"}},o.point=function(t,e,n){return t instanceof o.Point?t:o.Util.isArray(t)?new o.Point(t[0],t[1]):t===i||null===t?t:new o.Point(t,e,n)},o.Bounds=function(t,e){if(t)for(var i=e?[t,e]:t,n=0,o=i.length;o>n;n++)this.extend(i[n])},o.Bounds.prototype={extend:function(t){return t=o.point(t),this.min||this.max?(this.min.x=Math.min(t.x,this.min.x),this.max.x=Math.max(t.x,this.max.x),this.min.y=Math.min(t.y,this.min.y),this.max.y=Math.max(t.y,this.max.y)):(this.min=t.clone(),this.max=t.clone()),this},getCenter:function(t){return new o.Point((this.min.x+this.max.x)/2,(this.min.y+this.max.y)/2,t)},getBottomLeft:function(){return new o.Point(this.min.x,this.max.y)},getTopRight:function(){return new o.Point(this.max.x,this.min.y)},getSize:function(){return this.max.subtract(this.min)},contains:function(t){var e,i;return t="number"==typeof t[0]||t instanceof o.Point?o.point(t):o.bounds(t),t instanceof o.Bounds?(e=t.min,i=t.max):e=i=t,e.x>=this.min.x&&i.x<=this.max.x&&e.y>=this.min.y&&i.y<=this.max.y},intersects:function(t){t=o.bounds(t);var e=this.min,i=this.max,n=t.min,s=t.max,a=s.x>=e.x&&n.x<=i.x,r=s.y>=e.y&&n.y<=i.y;return a&&r},isValid:function(){return!(!this.min||!this.max)}},o.bounds=function(t,e){return!t||t instanceof o.Bounds?t:new o.Bounds(t,e)},o.Transformation=function(t,e,i,n){this._a=t,this._b=e,this._c=i,this._d=n},o.Transformation.prototype={transform:function(t,e){return this._transform(t.clone(),e)},_transform:function(t,e){return e=e||1,t.x=e*(this._a*t.x+this._b),t.y=e*(this._c*t.y+this._d),t},untransform:function(t,e){return e=e||1,new o.Point((t.x/e-this._b)/this._a,(t.y/e-this._d)/this._c)}},o.DomUtil={get:function(t){return"string"==typeof t?e.getElementById(t):t},getStyle:function(t,i){var n=t.style[i];if(!n&&t.currentStyle&&(n=t.currentStyle[i]),(!n||"auto"===n)&&e.defaultView){var o=e.defaultView.getComputedStyle(t,null);n=o?o[i]:null}return"auto"===n?null:n},getViewportOffset:function(t){var i,n=0,s=0,a=t,r=e.body,h=e.documentElement;do{if(n+=a.offsetTop||0,s+=a.offsetLeft||0,n+=parseInt(o.DomUtil.getStyle(a,"borderTopWidth"),10)||0,s+=parseInt(o.DomUtil.getStyle(a,"borderLeftWidth"),10)||0,i=o.DomUtil.getStyle(a,"position"),a.offsetParent===r&&"absolute"===i)break;if("fixed"===i){n+=r.scrollTop||h.scrollTop||0,s+=r.scrollLeft||h.scrollLeft||0;break}if("relative"===i&&!a.offsetLeft){var l=o.DomUtil.getStyle(a,"width"),u=o.DomUtil.getStyle(a,"max-width"),c=a.getBoundingClientRect();("none"!==l||"none"!==u)&&(s+=c.left+a.clientLeft),n+=c.top+(r.scrollTop||h.scrollTop||0);break}a=a.offsetParent}while(a);a=t;do{if(a===r)break;n-=a.scrollTop||0,s-=a.scrollLeft||0,a=a.parentNode}while(a);return new o.Point(s,n)},documentIsLtr:function(){return o.DomUtil._docIsLtrCached||(o.DomUtil._docIsLtrCached=!0,o.DomUtil._docIsLtr="ltr"===o.DomUtil.getStyle(e.body,"direction")),o.DomUtil._docIsLtr},create:function(t,i,n){var o=e.createElement(t);return o.className=i,n&&n.appendChild(o),o},hasClass:function(t,e){if(t.classList!==i)return t.classList.contains(e);var n=o.DomUtil._getClass(t);return n.length>0&&new RegExp("(^|\\s)"+e+"(\\s|$)").test(n)},addClass:function(t,e){if(t.classList!==i)for(var n=o.Util.splitWords(e),s=0,a=n.length;a>s;s++)t.classList.add(n[s]);else if(!o.DomUtil.hasClass(t,e)){var r=o.DomUtil._getClass(t);o.DomUtil._setClass(t,(r?r+" ":"")+e)}},removeClass:function(t,e){t.classList!==i?t.classList.remove(e):o.DomUtil._setClass(t,o.Util.trim((" "+o.DomUtil._getClass(t)+" ").replace(" "+e+" "," ")))},_setClass:function(t,e){t.className.baseVal===i?t.className=e:t.className.baseVal=e},_getClass:function(t){return t.className.baseVal===i?t.className:t.className.baseVal},setOpacity:function(t,e){if("opacity"in t.style)t.style.opacity=e;else if("filter"in t.style){var i=!1,n="DXImageTransform.Microsoft.Alpha";try{i=t.filters.item(n)}catch(o){if(1===e)return}e=Math.round(100*e),i?(i.Enabled=100!==e,i.Opacity=e):t.style.filter+=" progid:"+n+"(opacity="+e+")"}},testProp:function(t){for(var i=e.documentElement.style,n=0;n<t.length;n++)if(t[n]in i)return t[n];return!1},getTranslateString:function(t){var e=o.Browser.webkit3d,i="translate"+(e?"3d":"")+"(",n=(e?",0":"")+")";return i+t.x+"px,"+t.y+"px"+n},getScaleString:function(t,e){var i=o.DomUtil.getTranslateString(e.add(e.multiplyBy(-1*t))),n=" scale("+t+") ";return i+n},setPosition:function(t,e,i){t._leaflet_pos=e,!i&&o.Browser.any3d?t.style[o.DomUtil.TRANSFORM]=o.DomUtil.getTranslateString(e):(t.style.left=e.x+"px",t.style.top=e.y+"px")},getPosition:function(t){return t._leaflet_pos}},o.DomUtil.TRANSFORM=o.DomUtil.testProp(["transform","WebkitTransform","OTransform","MozTransform","msTransform"]),o.DomUtil.TRANSITION=o.DomUtil.testProp(["webkitTransition","transition","OTransition","MozTransition","msTransition"]),o.DomUtil.TRANSITION_END="webkitTransition"===o.DomUtil.TRANSITION||"OTransition"===o.DomUtil.TRANSITION?o.DomUtil.TRANSITION+"End":"transitionend",function(){if("onselectstart"in e)o.extend(o.DomUtil,{disableTextSelection:function(){o.DomEvent.on(t,"selectstart",o.DomEvent.preventDefault)},enableTextSelection:function(){o.DomEvent.off(t,"selectstart",o.DomEvent.preventDefault)}});else{var i=o.DomUtil.testProp(["userSelect","WebkitUserSelect","OUserSelect","MozUserSelect","msUserSelect"]);o.extend(o.DomUtil,{disableTextSelection:function(){if(i){var t=e.documentElement.style;this._userSelect=t[i],t[i]="none"}},enableTextSelection:function(){i&&(e.documentElement.style[i]=this._userSelect,delete this._userSelect)}})}o.extend(o.DomUtil,{disableImageDrag:function(){o.DomEvent.on(t,"dragstart",o.DomEvent.preventDefault)},enableImageDrag:function(){o.DomEvent.off(t,"dragstart",o.DomEvent.preventDefault)}})}(),o.LatLng=function(t,e,n){if(t=parseFloat(t),e=parseFloat(e),isNaN(t)||isNaN(e))throw new Error("Invalid LatLng object: ("+t+", "+e+")");this.lat=t,this.lng=e,n!==i&&(this.alt=parseFloat(n))},o.extend(o.LatLng,{DEG_TO_RAD:Math.PI/180,RAD_TO_DEG:180/Math.PI,MAX_MARGIN:1e-9}),o.LatLng.prototype={equals:function(t){if(!t)return!1;t=o.latLng(t);var e=Math.max(Math.abs(this.lat-t.lat),Math.abs(this.lng-t.lng));return e<=o.LatLng.MAX_MARGIN},toString:function(t){return"LatLng("+o.Util.formatNum(this.lat,t)+", "+o.Util.formatNum(this.lng,t)+")"},distanceTo:function(t){t=o.latLng(t);var e=6378137,i=o.LatLng.DEG_TO_RAD,n=(t.lat-this.lat)*i,s=(t.lng-this.lng)*i,a=this.lat*i,r=t.lat*i,h=Math.sin(n/2),l=Math.sin(s/2),u=h*h+l*l*Math.cos(a)*Math.cos(r);return 2*e*Math.atan2(Math.sqrt(u),Math.sqrt(1-u))},wrap:function(t,e){var i=this.lng;return t=t||-180,e=e||180,i=(i+e)%(e-t)+(t>i||i===e?e:t),new o.LatLng(this.lat,i)}},o.latLng=function(t,e){return t instanceof o.LatLng?t:o.Util.isArray(t)?"number"==typeof t[0]||"string"==typeof t[0]?new o.LatLng(t[0],t[1],t[2]):null:t===i||null===t?t:"object"==typeof t&&"lat"in t?new o.LatLng(t.lat,"lng"in t?t.lng:t.lon):e===i?null:new o.LatLng(t,e)},o.LatLngBounds=function(t,e){if(t)for(var i=e?[t,e]:t,n=0,o=i.length;o>n;n++)this.extend(i[n])},o.LatLngBounds.prototype={extend:function(t){if(!t)return this;var e=o.latLng(t);return t=null!==e?e:o.latLngBounds(t),t instanceof o.LatLng?this._southWest||this._northEast?(this._southWest.lat=Math.min(t.lat,this._southWest.lat),this._southWest.lng=Math.min(t.lng,this._southWest.lng),this._northEast.lat=Math.max(t.lat,this._northEast.lat),this._northEast.lng=Math.max(t.lng,this._northEast.lng)):(this._southWest=new o.LatLng(t.lat,t.lng),this._northEast=new o.LatLng(t.lat,t.lng)):t instanceof o.LatLngBounds&&(this.extend(t._southWest),this.extend(t._northEast)),this},pad:function(t){var e=this._southWest,i=this._northEast,n=Math.abs(e.lat-i.lat)*t,s=Math.abs(e.lng-i.lng)*t;return new o.LatLngBounds(new o.LatLng(e.lat-n,e.lng-s),new o.LatLng(i.lat+n,i.lng+s))},getCenter:function(){return new o.LatLng((this._southWest.lat+this._northEast.lat)/2,(this._southWest.lng+this._northEast.lng)/2)},getSouthWest:function(){return this._southWest},getNorthEast:function(){return this._northEast},getNorthWest:function(){return new o.LatLng(this.getNorth(),this.getWest())},getSouthEast:function(){return new o.LatLng(this.getSouth(),this.getEast())},getWest:function(){return this._southWest.lng},getSouth:function(){return this._southWest.lat},getEast:function(){return this._northEast.lng},getNorth:function(){return this._northEast.lat},contains:function(t){t="number"==typeof t[0]||t instanceof o.LatLng?o.latLng(t):o.latLngBounds(t);var e,i,n=this._southWest,s=this._northEast;return t instanceof o.LatLngBounds?(e=t.getSouthWest(),i=t.getNorthEast()):e=i=t,e.lat>=n.lat&&i.lat<=s.lat&&e.lng>=n.lng&&i.lng<=s.lng},intersects:function(t){t=o.latLngBounds(t);var e=this._southWest,i=this._northEast,n=t.getSouthWest(),s=t.getNorthEast(),a=s.lat>=e.lat&&n.lat<=i.lat,r=s.lng>=e.lng&&n.lng<=i.lng;return a&&r},toBBoxString:function(){return[this.getWest(),this.getSouth(),this.getEast(),this.getNorth()].join(",")},equals:function(t){return t?(t=o.latLngBounds(t),this._southWest.equals(t.getSouthWest())&&this._northEast.equals(t.getNorthEast())):!1},isValid:function(){return!(!this._southWest||!this._northEast)}},o.latLngBounds=function(t,e){return!t||t instanceof o.LatLngBounds?t:new o.LatLngBounds(t,e)},o.Projection={},o.Projection.SphericalMercator={MAX_LATITUDE:85.0511287798,project:function(t){var e=o.LatLng.DEG_TO_RAD,i=this.MAX_LATITUDE,n=Math.max(Math.min(i,t.lat),-i),s=t.lng*e,a=n*e;return a=Math.log(Math.tan(Math.PI/4+a/2)),new o.Point(s,a)},unproject:function(t){var e=o.LatLng.RAD_TO_DEG,i=t.x*e,n=(2*Math.atan(Math.exp(t.y))-Math.PI/2)*e;return new o.LatLng(n,i)}},o.Projection.LonLat={project:function(t){return new o.Point(t.lng,t.lat)},unproject:function(t){return new o.LatLng(t.y,t.x)}},o.CRS={latLngToPoint:function(t,e){var i=this.projection.project(t),n=this.scale(e);return this.transformation._transform(i,n)},pointToLatLng:function(t,e){var i=this.scale(e),n=this.transformation.untransform(t,i);return this.projection.unproject(n)},project:function(t){return this.projection.project(t)},scale:function(t){return 256*Math.pow(2,t)},getSize:function(t){var e=this.scale(t);return o.point(e,e)}},o.CRS.Simple=o.extend({},o.CRS,{projection:o.Projection.LonLat,transformation:new o.Transformation(1,0,-1,0),scale:function(t){return Math.pow(2,t)}}),o.CRS.EPSG3857=o.extend({},o.CRS,{code:"EPSG:3857",projection:o.Projection.SphericalMercator,transformation:new o.Transformation(.5/Math.PI,.5,-.5/Math.PI,.5),project:function(t){var e=this.projection.project(t),i=6378137;return e.multiplyBy(i)}}),o.CRS.EPSG900913=o.extend({},o.CRS.EPSG3857,{code:"EPSG:900913"}),o.CRS.EPSG4326=o.extend({},o.CRS,{code:"EPSG:4326",projection:o.Projection.LonLat,transformation:new o.Transformation(1/360,.5,-1/360,.5)}),o.Map=o.Class.extend({includes:o.Mixin.Events,options:{crs:o.CRS.EPSG3857,fadeAnimation:o.DomUtil.TRANSITION&&!o.Browser.android23,trackResize:!0,markerZoomAnimation:o.DomUtil.TRANSITION&&o.Browser.any3d},initialize:function(t,e){e=o.setOptions(this,e),this._initContainer(t),this._initLayout(),this._onResize=o.bind(this._onResize,this),this._initEvents(),e.maxBounds&&this.setMaxBounds(e.maxBounds),e.center&&e.zoom!==i&&this.setView(o.latLng(e.center),e.zoom,{reset:!0}),this._handlers=[],this._layers={},this._zoomBoundLayers={},this._tileLayersNum=0,this.callInitHooks(),this._addLayers(e.layers)},setView:function(t,e){return e=e===i?this.getZoom():e,this._resetView(o.latLng(t),this._limitZoom(e)),this},setZoom:function(t,e){return this._loaded?this.setView(this.getCenter(),t,{zoom:e}):(this._zoom=this._limitZoom(t),this)},zoomIn:function(t,e){return this.setZoom(this._zoom+(t||1),e)},zoomOut:function(t,e){return this.setZoom(this._zoom-(t||1),e)},setZoomAround:function(t,e,i){var n=this.getZoomScale(e),s=this.getSize().divideBy(2),a=t instanceof o.Point?t:this.latLngToContainerPoint(t),r=a.subtract(s).multiplyBy(1-1/n),h=this.containerPointToLatLng(s.add(r));return this.setView(h,e,{zoom:i})},fitBounds:function(t,e){e=e||{},t=t.getBounds?t.getBounds():o.latLngBounds(t);var i=o.point(e.paddingTopLeft||e.padding||[0,0]),n=o.point(e.paddingBottomRight||e.padding||[0,0]),s=this.getBoundsZoom(t,!1,i.add(n)),a=n.subtract(i).divideBy(2),r=this.project(t.getSouthWest(),s),h=this.project(t.getNorthEast(),s),l=this.unproject(r.add(h).divideBy(2).add(a),s);return s=e&&e.maxZoom?Math.min(e.maxZoom,s):s,this.setView(l,s,e)},fitWorld:function(t){return this.fitBounds([[-90,-180],[90,180]],t)},panTo:function(t,e){return this.setView(t,this._zoom,{pan:e})},panBy:function(t){return this.fire("movestart"),this._rawPanBy(o.point(t)),this.fire("move"),this.fire("moveend")},setMaxBounds:function(t){return t=o.latLngBounds(t),this.options.maxBounds=t,t?(this._loaded&&this._panInsideMaxBounds(),this.on("moveend",this._panInsideMaxBounds,this)):this.off("moveend",this._panInsideMaxBounds,this)},panInsideBounds:function(t,e){var i=this.getCenter(),n=this._limitCenter(i,this._zoom,t);return i.equals(n)?this:this.panTo(n,e)},addLayer:function(t){var e=o.stamp(t);return this._layers[e]?this:(this._layers[e]=t,!t.options||isNaN(t.options.maxZoom)&&isNaN(t.options.minZoom)||(this._zoomBoundLayers[e]=t,this._updateZoomLevels()),this.options.zoomAnimation&&o.TileLayer&&t instanceof o.TileLayer&&(this._tileLayersNum++,this._tileLayersToLoad++,t.on("load",this._onTileLayerLoad,this)),this._loaded&&this._layerAdd(t),this)},removeLayer:function(t){var e=o.stamp(t);return this._layers[e]?(this._loaded&&t.onRemove(this),delete this._layers[e],this._loaded&&this.fire("layerremove",{layer:t}),this._zoomBoundLayers[e]&&(delete this._zoomBoundLayers[e],this._updateZoomLevels()),this.options.zoomAnimation&&o.TileLayer&&t instanceof o.TileLayer&&(this._tileLayersNum--,this._tileLayersToLoad--,t.off("load",this._onTileLayerLoad,this)),this):this},hasLayer:function(t){return t?o.stamp(t)in this._layers:!1},eachLayer:function(t,e){for(var i in this._layers)t.call(e,this._layers[i]);return this},invalidateSize:function(t){if(!this._loaded)return this;t=o.extend({animate:!1,pan:!0},t===!0?{animate:!0}:t);var e=this.getSize();this._sizeChanged=!0,this._initialCenter=null;var i=this.getSize(),n=e.divideBy(2).round(),s=i.divideBy(2).round(),a=n.subtract(s);return a.x||a.y?(t.animate&&t.pan?this.panBy(a):(t.pan&&this._rawPanBy(a),this.fire("move"),t.debounceMoveend?(clearTimeout(this._sizeTimer),this._sizeTimer=setTimeout(o.bind(this.fire,this,"moveend"),200)):this.fire("moveend")),this.fire("resize",{oldSize:e,newSize:i})):this},addHandler:function(t,e){if(!e)return this;var i=this[t]=new e(this);return this._handlers.push(i),this.options[t]&&i.enable(),this},remove:function(){this._loaded&&this.fire("unload"),this._initEvents("off");try{delete this._container._leaflet}catch(t){this._container._leaflet=i}return this._clearPanes(),this._clearControlPos&&this._clearControlPos(),this._clearHandlers(),this},getCenter:function(){return this._checkIfLoaded(),this._initialCenter&&!this._moved()?this._initialCenter:this.layerPointToLatLng(this._getCenterLayerPoint())},getZoom:function(){return this._zoom},getBounds:function(){var t=this.getPixelBounds(),e=this.unproject(t.getBottomLeft()),i=this.unproject(t.getTopRight());return new o.LatLngBounds(e,i)},getMinZoom:function(){return this.options.minZoom===i?this._layersMinZoom===i?0:this._layersMinZoom:this.options.minZoom},getMaxZoom:function(){return this.options.maxZoom===i?this._layersMaxZoom===i?1/0:this._layersMaxZoom:this.options.maxZoom},getBoundsZoom:function(t,e,i){t=o.latLngBounds(t);var n,s=this.getMinZoom()-(e?1:0),a=this.getMaxZoom(),r=this.getSize(),h=t.getNorthWest(),l=t.getSouthEast(),u=!0;i=o.point(i||[0,0]);do s++,n=this.project(l,s).subtract(this.project(h,s)).add(i),u=e?n.x<r.x||n.y<r.y:r.contains(n);while(u&&a>=s);return u&&e?null:e?s:s-1},getSize:function(){return(!this._size||this._sizeChanged)&&(this._size=new o.Point(this._container.clientWidth,this._container.clientHeight),this._sizeChanged=!1),this._size.clone()},getPixelBounds:function(){var t=this._getTopLeftPoint();return new o.Bounds(t,t.add(this.getSize()))},getPixelOrigin:function(){return this._checkIfLoaded(),this._initialTopLeftPoint},getPanes:function(){return this._panes},getContainer:function(){return this._container},getZoomScale:function(t){var e=this.options.crs;return e.scale(t)/e.scale(this._zoom)},getScaleZoom:function(t){return this._zoom+Math.log(t)/Math.LN2},project:function(t,e){return e=e===i?this._zoom:e,this.options.crs.latLngToPoint(o.latLng(t),e)},unproject:function(t,e){return e=e===i?this._zoom:e,this.options.crs.pointToLatLng(o.point(t),e)},layerPointToLatLng:function(t){var e=o.point(t).add(this.getPixelOrigin());return this.unproject(e)},latLngToLayerPoint:function(t){var e=this.project(o.latLng(t))._round();return e._subtract(this.getPixelOrigin())},containerPointToLayerPoint:function(t){return o.point(t).subtract(this._getMapPanePos())},layerPointToContainerPoint:function(t){return o.point(t).add(this._getMapPanePos())},containerPointToLatLng:function(t){var e=this.containerPointToLayerPoint(o.point(t));return this.layerPointToLatLng(e)},latLngToContainerPoint:function(t){return this.layerPointToContainerPoint(this.latLngToLayerPoint(o.latLng(t)))},mouseEventToContainerPoint:function(t){return o.DomEvent.getMousePosition(t,this._container)},mouseEventToLayerPoint:function(t){return this.containerPointToLayerPoint(this.mouseEventToContainerPoint(t))},mouseEventToLatLng:function(t){return this.layerPointToLatLng(this.mouseEventToLayerPoint(t))},_initContainer:function(t){var e=this._container=o.DomUtil.get(t);if(!e)throw new Error("Map container not found.");if(e._leaflet)throw new Error("Map container is already initialized.");e._leaflet=!0},_initLayout:function(){var t=this._container;o.DomUtil.addClass(t,"leaflet-container"+(o.Browser.touch?" leaflet-touch":"")+(o.Browser.retina?" leaflet-retina":"")+(o.Browser.ielt9?" leaflet-oldie":"")+(this.options.fadeAnimation?" leaflet-fade-anim":""));var e=o.DomUtil.getStyle(t,"position");"absolute"!==e&&"relative"!==e&&"fixed"!==e&&(t.style.position="relative"),this._initPanes(),this._initControlPos&&this._initControlPos()},_initPanes:function(){var t=this._panes={};this._mapPane=t.mapPane=this._createPane("leaflet-map-pane",this._container),this._tilePane=t.tilePane=this._createPane("leaflet-tile-pane",this._mapPane),t.objectsPane=this._createPane("leaflet-objects-pane",this._mapPane),t.shadowPane=this._createPane("leaflet-shadow-pane"),t.overlayPane=this._createPane("leaflet-overlay-pane"),t.markerPane=this._createPane("leaflet-marker-pane"),t.popupPane=this._createPane("leaflet-popup-pane");var e=" leaflet-zoom-hide";this.options.markerZoomAnimation||(o.DomUtil.addClass(t.markerPane,e),o.DomUtil.addClass(t.shadowPane,e),o.DomUtil.addClass(t.popupPane,e))},_createPane:function(t,e){return o.DomUtil.create("div",t,e||this._panes.objectsPane)},_clearPanes:function(){this._container.removeChild(this._mapPane)},_addLayers:function(t){t=t?o.Util.isArray(t)?t:[t]:[];for(var e=0,i=t.length;i>e;e++)this.addLayer(t[e])},_resetView:function(t,e,i,n){var s=this._zoom!==e;n||(this.fire("movestart"),s&&this.fire("zoomstart")),this._zoom=e,this._initialCenter=t,this._initialTopLeftPoint=this._getNewTopLeftPoint(t),i?this._initialTopLeftPoint._add(this._getMapPanePos()):o.DomUtil.setPosition(this._mapPane,new o.Point(0,0)),this._tileLayersToLoad=this._tileLayersNum;var a=!this._loaded;this._loaded=!0,a&&(this.fire("load"),this.eachLayer(this._layerAdd,this)),this.fire("viewreset",{hard:!i}),this.fire("move"),(s||n)&&this.fire("zoomend"),this.fire("moveend",{hard:!i})},_rawPanBy:function(t){o.DomUtil.setPosition(this._mapPane,this._getMapPanePos().subtract(t))},_getZoomSpan:function(){return this.getMaxZoom()-this.getMinZoom()},_updateZoomLevels:function(){var t,e=1/0,n=-1/0,o=this._getZoomSpan();for(t in this._zoomBoundLayers){var s=this._zoomBoundLayers[t];isNaN(s.options.minZoom)||(e=Math.min(e,s.options.minZoom)),isNaN(s.options.maxZoom)||(n=Math.max(n,s.options.maxZoom))}t===i?this._layersMaxZoom=this._layersMinZoom=i:(this._layersMaxZoom=n,this._layersMinZoom=e),o!==this._getZoomSpan()&&this.fire("zoomlevelschange")},_panInsideMaxBounds:function(){this.panInsideBounds(this.options.maxBounds)},_checkIfLoaded:function(){if(!this._loaded)throw new Error("Set map center and zoom first.")},_initEvents:function(e){if(o.DomEvent){e=e||"on",o.DomEvent[e](this._container,"click",this._onMouseClick,this);var i,n,s=["dblclick","mousedown","mouseup","mouseenter","mouseleave","mousemove","contextmenu"];for(i=0,n=s.length;n>i;i++)o.DomEvent[e](this._container,s[i],this._fireMouseEvent,this);this.options.trackResize&&o.DomEvent[e](t,"resize",this._onResize,this)}},_onResize:function(){o.Util.cancelAnimFrame(this._resizeRequest),this._resizeRequest=o.Util.requestAnimFrame(function(){this.invalidateSize({debounceMoveend:!0})},this,!1,this._container)},_onMouseClick:function(t){!this._loaded||!t._simulated&&(this.dragging&&this.dragging.moved()||this.boxZoom&&this.boxZoom.moved())||o.DomEvent._skipped(t)||(this.fire("preclick"),this._fireMouseEvent(t))},_fireMouseEvent:function(t){if(this._loaded&&!o.DomEvent._skipped(t)){var e=t.type;if(e="mouseenter"===e?"mouseover":"mouseleave"===e?"mouseout":e,this.hasEventListeners(e)){"contextmenu"===e&&o.DomEvent.preventDefault(t);var i=this.mouseEventToContainerPoint(t),n=this.containerPointToLayerPoint(i),s=this.layerPointToLatLng(n);this.fire(e,{latlng:s,layerPoint:n,containerPoint:i,originalEvent:t})}}},_onTileLayerLoad:function(){this._tileLayersToLoad--,this._tileLayersNum&&!this._tileLayersToLoad&&this.fire("tilelayersload")},_clearHandlers:function(){for(var t=0,e=this._handlers.length;e>t;t++)this._handlers[t].disable()},whenReady:function(t,e){return this._loaded?t.call(e||this,this):this.on("load",t,e),this},_layerAdd:function(t){t.onAdd(this),this.fire("layeradd",{layer:t})},_getMapPanePos:function(){return o.DomUtil.getPosition(this._mapPane)},_moved:function(){var t=this._getMapPanePos();return t&&!t.equals([0,0])},_getTopLeftPoint:function(){return this.getPixelOrigin().subtract(this._getMapPanePos())},_getNewTopLeftPoint:function(t,e){var i=this.getSize()._divideBy(2);return this.project(t,e)._subtract(i)._round()},_latLngToNewLayerPoint:function(t,e,i){var n=this._getNewTopLeftPoint(i,e).add(this._getMapPanePos());return this.project(t,e)._subtract(n)},_getCenterLayerPoint:function(){return this.containerPointToLayerPoint(this.getSize()._divideBy(2))},_getCenterOffset:function(t){return this.latLngToLayerPoint(t).subtract(this._getCenterLayerPoint())},_limitCenter:function(t,e,i){if(!i)return t;var n=this.project(t,e),s=this.getSize().divideBy(2),a=new o.Bounds(n.subtract(s),n.add(s)),r=this._getBoundsOffset(a,i,e);return this.unproject(n.add(r),e)},_limitOffset:function(t,e){if(!e)return t;var i=this.getPixelBounds(),n=new o.Bounds(i.min.add(t),i.max.add(t));return t.add(this._getBoundsOffset(n,e))},_getBoundsOffset:function(t,e,i){var n=this.project(e.getNorthWest(),i).subtract(t.min),s=this.project(e.getSouthEast(),i).subtract(t.max),a=this._rebound(n.x,-s.x),r=this._rebound(n.y,-s.y);return new o.Point(a,r)},_rebound:function(t,e){return t+e>0?Math.round(t-e)/2:Math.max(0,Math.ceil(t))-Math.max(0,Math.floor(e))},_limitZoom:function(t){var e=this.getMinZoom(),i=this.getMaxZoom();return Math.max(e,Math.min(i,t))}}),o.map=function(t,e){return new o.Map(t,e)},o.Projection.Mercator={MAX_LATITUDE:85.0840591556,R_MINOR:6356752.314245179,R_MAJOR:6378137,project:function(t){var e=o.LatLng.DEG_TO_RAD,i=this.MAX_LATITUDE,n=Math.max(Math.min(i,t.lat),-i),s=this.R_MAJOR,a=this.R_MINOR,r=t.lng*e*s,h=n*e,l=a/s,u=Math.sqrt(1-l*l),c=u*Math.sin(h);c=Math.pow((1-c)/(1+c),.5*u);var d=Math.tan(.5*(.5*Math.PI-h))/c;return h=-s*Math.log(d),new o.Point(r,h)},unproject:function(t){for(var e,i=o.LatLng.RAD_TO_DEG,n=this.R_MAJOR,s=this.R_MINOR,a=t.x*i/n,r=s/n,h=Math.sqrt(1-r*r),l=Math.exp(-t.y/n),u=Math.PI/2-2*Math.atan(l),c=15,d=1e-7,p=c,_=.1;Math.abs(_)>d&&--p>0;)e=h*Math.sin(u),_=Math.PI/2-2*Math.atan(l*Math.pow((1-e)/(1+e),.5*h))-u,u+=_;
return new o.LatLng(u*i,a)}},o.CRS.EPSG3395=o.extend({},o.CRS,{code:"EPSG:3395",projection:o.Projection.Mercator,transformation:function(){var t=o.Projection.Mercator,e=t.R_MAJOR,i=.5/(Math.PI*e);return new o.Transformation(i,.5,-i,.5)}()}),o.TileLayer=o.Class.extend({includes:o.Mixin.Events,options:{minZoom:0,maxZoom:18,tileSize:256,subdomains:"abc",errorTileUrl:"",attribution:"",zoomOffset:0,opacity:1,unloadInvisibleTiles:o.Browser.mobile,updateWhenIdle:o.Browser.mobile},initialize:function(t,e){e=o.setOptions(this,e),e.detectRetina&&o.Browser.retina&&e.maxZoom>0&&(e.tileSize=Math.floor(e.tileSize/2),e.zoomOffset++,e.minZoom>0&&e.minZoom--,this.options.maxZoom--),e.bounds&&(e.bounds=o.latLngBounds(e.bounds)),this._url=t;var i=this.options.subdomains;"string"==typeof i&&(this.options.subdomains=i.split(""))},onAdd:function(t){this._map=t,this._animated=t._zoomAnimated,this._initContainer(),t.on({viewreset:this._reset,moveend:this._update},this),this._animated&&t.on({zoomanim:this._animateZoom,zoomend:this._endZoomAnim},this),this.options.updateWhenIdle||(this._limitedUpdate=o.Util.limitExecByInterval(this._update,150,this),t.on("move",this._limitedUpdate,this)),this._reset(),this._update()},addTo:function(t){return t.addLayer(this),this},onRemove:function(t){this._container.parentNode.removeChild(this._container),t.off({viewreset:this._reset,moveend:this._update},this),this._animated&&t.off({zoomanim:this._animateZoom,zoomend:this._endZoomAnim},this),this.options.updateWhenIdle||t.off("move",this._limitedUpdate,this),this._container=null,this._map=null},bringToFront:function(){var t=this._map._panes.tilePane;return this._container&&(t.appendChild(this._container),this._setAutoZIndex(t,Math.max)),this},bringToBack:function(){var t=this._map._panes.tilePane;return this._container&&(t.insertBefore(this._container,t.firstChild),this._setAutoZIndex(t,Math.min)),this},getAttribution:function(){return this.options.attribution},getContainer:function(){return this._container},setOpacity:function(t){return this.options.opacity=t,this._map&&this._updateOpacity(),this},setZIndex:function(t){return this.options.zIndex=t,this._updateZIndex(),this},setUrl:function(t,e){return this._url=t,e||this.redraw(),this},redraw:function(){return this._map&&(this._reset({hard:!0}),this._update()),this},_updateZIndex:function(){this._container&&this.options.zIndex!==i&&(this._container.style.zIndex=this.options.zIndex)},_setAutoZIndex:function(t,e){var i,n,o,s=t.children,a=-e(1/0,-1/0);for(n=0,o=s.length;o>n;n++)s[n]!==this._container&&(i=parseInt(s[n].style.zIndex,10),isNaN(i)||(a=e(a,i)));this.options.zIndex=this._container.style.zIndex=(isFinite(a)?a:0)+e(1,-1)},_updateOpacity:function(){var t,e=this._tiles;if(o.Browser.ielt9)for(t in e)o.DomUtil.setOpacity(e[t],this.options.opacity);else o.DomUtil.setOpacity(this._container,this.options.opacity)},_initContainer:function(){var t=this._map._panes.tilePane;if(!this._container){if(this._container=o.DomUtil.create("div","leaflet-layer"),this._updateZIndex(),this._animated){var e="leaflet-tile-container";this._bgBuffer=o.DomUtil.create("div",e,this._container),this._tileContainer=o.DomUtil.create("div",e,this._container)}else this._tileContainer=this._container;t.appendChild(this._container),this.options.opacity<1&&this._updateOpacity()}},_reset:function(t){for(var e in this._tiles)this.fire("tileunload",{tile:this._tiles[e]});this._tiles={},this._tilesToLoad=0,this.options.reuseTiles&&(this._unusedTiles=[]),this._tileContainer.innerHTML="",this._animated&&t&&t.hard&&this._clearBgBuffer(),this._initContainer()},_getTileSize:function(){var t=this._map,e=t.getZoom()+this.options.zoomOffset,i=this.options.maxNativeZoom,n=this.options.tileSize;return i&&e>i&&(n=Math.round(t.getZoomScale(e)/t.getZoomScale(i)*n)),n},_update:function(){if(this._map){var t=this._map,e=t.getPixelBounds(),i=t.getZoom(),n=this._getTileSize();if(!(i>this.options.maxZoom||i<this.options.minZoom)){var s=o.bounds(e.min.divideBy(n)._floor(),e.max.divideBy(n)._floor());this._addTilesFromCenterOut(s),(this.options.unloadInvisibleTiles||this.options.reuseTiles)&&this._removeOtherTiles(s)}}},_addTilesFromCenterOut:function(t){var i,n,s,a=[],r=t.getCenter();for(i=t.min.y;i<=t.max.y;i++)for(n=t.min.x;n<=t.max.x;n++)s=new o.Point(n,i),this._tileShouldBeLoaded(s)&&a.push(s);var h=a.length;if(0!==h){a.sort(function(t,e){return t.distanceTo(r)-e.distanceTo(r)});var l=e.createDocumentFragment();for(this._tilesToLoad||this.fire("loading"),this._tilesToLoad+=h,n=0;h>n;n++)this._addTile(a[n],l);this._tileContainer.appendChild(l)}},_tileShouldBeLoaded:function(t){if(t.x+":"+t.y in this._tiles)return!1;var e=this.options;if(!e.continuousWorld){var i=this._getWrapTileNum();if(e.noWrap&&(t.x<0||t.x>=i.x)||t.y<0||t.y>=i.y)return!1}if(e.bounds){var n=e.tileSize,o=t.multiplyBy(n),s=o.add([n,n]),a=this._map.unproject(o),r=this._map.unproject(s);if(e.continuousWorld||e.noWrap||(a=a.wrap(),r=r.wrap()),!e.bounds.intersects([a,r]))return!1}return!0},_removeOtherTiles:function(t){var e,i,n,o;for(o in this._tiles)e=o.split(":"),i=parseInt(e[0],10),n=parseInt(e[1],10),(i<t.min.x||i>t.max.x||n<t.min.y||n>t.max.y)&&this._removeTile(o)},_removeTile:function(t){var e=this._tiles[t];this.fire("tileunload",{tile:e,url:e.src}),this.options.reuseTiles?(o.DomUtil.removeClass(e,"leaflet-tile-loaded"),this._unusedTiles.push(e)):e.parentNode===this._tileContainer&&this._tileContainer.removeChild(e),o.Browser.android||(e.onload=null,e.src=o.Util.emptyImageUrl),delete this._tiles[t]},_addTile:function(t,e){var i=this._getTilePos(t),n=this._getTile();o.DomUtil.setPosition(n,i,o.Browser.chrome),this._tiles[t.x+":"+t.y]=n,this._loadTile(n,t),n.parentNode!==this._tileContainer&&e.appendChild(n)},_getZoomForUrl:function(){var t=this.options,e=this._map.getZoom();return t.zoomReverse&&(e=t.maxZoom-e),e+=t.zoomOffset,t.maxNativeZoom?Math.min(e,t.maxNativeZoom):e},_getTilePos:function(t){var e=this._map.getPixelOrigin(),i=this._getTileSize();return t.multiplyBy(i).subtract(e)},getTileUrl:function(t){return o.Util.template(this._url,o.extend({s:this._getSubdomain(t),z:t.z,x:t.x,y:t.y},this.options))},_getWrapTileNum:function(){var t=this._map.options.crs,e=t.getSize(this._map.getZoom());return e.divideBy(this._getTileSize())._floor()},_adjustTilePoint:function(t){var e=this._getWrapTileNum();this.options.continuousWorld||this.options.noWrap||(t.x=(t.x%e.x+e.x)%e.x),this.options.tms&&(t.y=e.y-t.y-1),t.z=this._getZoomForUrl()},_getSubdomain:function(t){var e=Math.abs(t.x+t.y)%this.options.subdomains.length;return this.options.subdomains[e]},_getTile:function(){if(this.options.reuseTiles&&this._unusedTiles.length>0){var t=this._unusedTiles.pop();return this._resetTile(t),t}return this._createTile()},_resetTile:function(){},_createTile:function(){var t=o.DomUtil.create("img","leaflet-tile");return t.style.width=t.style.height=this._getTileSize()+"px",t.galleryimg="no",t.onselectstart=t.onmousemove=o.Util.falseFn,o.Browser.ielt9&&this.options.opacity!==i&&o.DomUtil.setOpacity(t,this.options.opacity),o.Browser.mobileWebkit3d&&(t.style.WebkitBackfaceVisibility="hidden"),t},_loadTile:function(t,e){t._layer=this,t.onload=this._tileOnLoad,t.onerror=this._tileOnError,this._adjustTilePoint(e),t.src=this.getTileUrl(e),this.fire("tileloadstart",{tile:t,url:t.src})},_tileLoaded:function(){this._tilesToLoad--,this._animated&&o.DomUtil.addClass(this._tileContainer,"leaflet-zoom-animated"),this._tilesToLoad||(this.fire("load"),this._animated&&(clearTimeout(this._clearBgBufferTimer),this._clearBgBufferTimer=setTimeout(o.bind(this._clearBgBuffer,this),500)))},_tileOnLoad:function(){var t=this._layer;this.src!==o.Util.emptyImageUrl&&(o.DomUtil.addClass(this,"leaflet-tile-loaded"),t.fire("tileload",{tile:this,url:this.src})),t._tileLoaded()},_tileOnError:function(){var t=this._layer;t.fire("tileerror",{tile:this,url:this.src});var e=t.options.errorTileUrl;e&&(this.src=e),t._tileLoaded()}}),o.tileLayer=function(t,e){return new o.TileLayer(t,e)},o.TileLayer.WMS=o.TileLayer.extend({defaultWmsParams:{service:"WMS",request:"GetMap",version:"1.1.1",layers:"",styles:"",format:"image/jpeg",transparent:!1},initialize:function(t,e){this._url=t;var i=o.extend({},this.defaultWmsParams),n=e.tileSize||this.options.tileSize;i.width=i.height=e.detectRetina&&o.Browser.retina?2*n:n;for(var s in e)this.options.hasOwnProperty(s)||"crs"===s||(i[s]=e[s]);this.wmsParams=i,o.setOptions(this,e)},onAdd:function(t){this._crs=this.options.crs||t.options.crs,this._wmsVersion=parseFloat(this.wmsParams.version);var e=this._wmsVersion>=1.3?"crs":"srs";this.wmsParams[e]=this._crs.code,o.TileLayer.prototype.onAdd.call(this,t)},getTileUrl:function(t){var e=this._map,i=this.options.tileSize,n=t.multiplyBy(i),s=n.add([i,i]),a=this._crs.project(e.unproject(n,t.z)),r=this._crs.project(e.unproject(s,t.z)),h=this._wmsVersion>=1.3&&this._crs===o.CRS.EPSG4326?[r.y,a.x,a.y,r.x].join(","):[a.x,r.y,r.x,a.y].join(","),l=o.Util.template(this._url,{s:this._getSubdomain(t)});return l+o.Util.getParamString(this.wmsParams,l,!0)+"&BBOX="+h},setParams:function(t,e){return o.extend(this.wmsParams,t),e||this.redraw(),this}}),o.tileLayer.wms=function(t,e){return new o.TileLayer.WMS(t,e)},o.TileLayer.Canvas=o.TileLayer.extend({options:{async:!1},initialize:function(t){o.setOptions(this,t)},redraw:function(){this._map&&(this._reset({hard:!0}),this._update());for(var t in this._tiles)this._redrawTile(this._tiles[t]);return this},_redrawTile:function(t){this.drawTile(t,t._tilePoint,this._map._zoom)},_createTile:function(){var t=o.DomUtil.create("canvas","leaflet-tile");return t.width=t.height=this.options.tileSize,t.onselectstart=t.onmousemove=o.Util.falseFn,t},_loadTile:function(t,e){t._layer=this,t._tilePoint=e,this._redrawTile(t),this.options.async||this.tileDrawn(t)},drawTile:function(){},tileDrawn:function(t){this._tileOnLoad.call(t)}}),o.tileLayer.canvas=function(t){return new o.TileLayer.Canvas(t)},o.ImageOverlay=o.Class.extend({includes:o.Mixin.Events,options:{opacity:1},initialize:function(t,e,i){this._url=t,this._bounds=o.latLngBounds(e),o.setOptions(this,i)},onAdd:function(t){this._map=t,this._image||this._initImage(),t._panes.overlayPane.appendChild(this._image),t.on("viewreset",this._reset,this),t.options.zoomAnimation&&o.Browser.any3d&&t.on("zoomanim",this._animateZoom,this),this._reset()},onRemove:function(t){t.getPanes().overlayPane.removeChild(this._image),t.off("viewreset",this._reset,this),t.options.zoomAnimation&&t.off("zoomanim",this._animateZoom,this)},addTo:function(t){return t.addLayer(this),this},setOpacity:function(t){return this.options.opacity=t,this._updateOpacity(),this},bringToFront:function(){return this._image&&this._map._panes.overlayPane.appendChild(this._image),this},bringToBack:function(){var t=this._map._panes.overlayPane;return this._image&&t.insertBefore(this._image,t.firstChild),this},setUrl:function(t){this._url=t,this._image.src=this._url},getAttribution:function(){return this.options.attribution},_initImage:function(){this._image=o.DomUtil.create("img","leaflet-image-layer"),this._map.options.zoomAnimation&&o.Browser.any3d?o.DomUtil.addClass(this._image,"leaflet-zoom-animated"):o.DomUtil.addClass(this._image,"leaflet-zoom-hide"),this._updateOpacity(),o.extend(this._image,{galleryimg:"no",onselectstart:o.Util.falseFn,onmousemove:o.Util.falseFn,onload:o.bind(this._onImageLoad,this),src:this._url})},_animateZoom:function(t){var e=this._map,i=this._image,n=e.getZoomScale(t.zoom),s=this._bounds.getNorthWest(),a=this._bounds.getSouthEast(),r=e._latLngToNewLayerPoint(s,t.zoom,t.center),h=e._latLngToNewLayerPoint(a,t.zoom,t.center)._subtract(r),l=r._add(h._multiplyBy(.5*(1-1/n)));i.style[o.DomUtil.TRANSFORM]=o.DomUtil.getTranslateString(l)+" scale("+n+") "},_reset:function(){var t=this._image,e=this._map.latLngToLayerPoint(this._bounds.getNorthWest()),i=this._map.latLngToLayerPoint(this._bounds.getSouthEast())._subtract(e);o.DomUtil.setPosition(t,e),t.style.width=i.x+"px",t.style.height=i.y+"px"},_onImageLoad:function(){this.fire("load")},_updateOpacity:function(){o.DomUtil.setOpacity(this._image,this.options.opacity)}}),o.imageOverlay=function(t,e,i){return new o.ImageOverlay(t,e,i)},o.Icon=o.Class.extend({options:{className:""},initialize:function(t){o.setOptions(this,t)},createIcon:function(t){return this._createIcon("icon",t)},createShadow:function(t){return this._createIcon("shadow",t)},_createIcon:function(t,e){var i=this._getIconUrl(t);if(!i){if("icon"===t)throw new Error("iconUrl not set in Icon options (see the docs).");return null}var n;return n=e&&"IMG"===e.tagName?this._createImg(i,e):this._createImg(i),this._setIconStyles(n,t),n},_setIconStyles:function(t,e){var i,n=this.options,s=o.point(n[e+"Size"]);i=o.point("shadow"===e?n.shadowAnchor||n.iconAnchor:n.iconAnchor),!i&&s&&(i=s.divideBy(2,!0)),t.className="leaflet-marker-"+e+" "+n.className,i&&(t.style.marginLeft=-i.x+"px",t.style.marginTop=-i.y+"px"),s&&(t.style.width=s.x+"px",t.style.height=s.y+"px")},_createImg:function(t,i){return i=i||e.createElement("img"),i.src=t,i},_getIconUrl:function(t){return o.Browser.retina&&this.options[t+"RetinaUrl"]?this.options[t+"RetinaUrl"]:this.options[t+"Url"]}}),o.icon=function(t){return new o.Icon(t)},o.Icon.Default=o.Icon.extend({options:{iconSize:[25,41],iconAnchor:[12,41],popupAnchor:[1,-34],shadowSize:[41,41]},_getIconUrl:function(t){var e=t+"Url";if(this.options[e])return this.options[e];o.Browser.retina&&"icon"===t&&(t+="-2x");var i=o.Icon.Default.imagePath;if(!i)throw new Error("Couldn't autodetect L.Icon.Default.imagePath, set it manually.");return i+"/marker-"+t+".png"}}),o.Icon.Default.imagePath=function(){var t,i,n,o,s,a=e.getElementsByTagName("script"),r=/[\/^]leaflet[\-\._]?([\w\-\._]*)\.js\??/;for(t=0,i=a.length;i>t;t++)if(n=a[t].src,o=n.match(r))return s=n.split(r)[0],(s?s+"/":"")+"images"}(),o.Marker=o.Class.extend({includes:o.Mixin.Events,options:{icon:new o.Icon.Default,title:"",alt:"",clickable:!0,draggable:!1,keyboard:!0,zIndexOffset:0,opacity:1,riseOnHover:!1,riseOffset:250},initialize:function(t,e){o.setOptions(this,e),this._latlng=o.latLng(t)},onAdd:function(t){this._map=t,t.on("viewreset",this.update,this),this._initIcon(),this.update(),this.fire("add"),t.options.zoomAnimation&&t.options.markerZoomAnimation&&t.on("zoomanim",this._animateZoom,this)},addTo:function(t){return t.addLayer(this),this},onRemove:function(t){this.dragging&&this.dragging.disable(),this._removeIcon(),this._removeShadow(),this.fire("remove"),t.off({viewreset:this.update,zoomanim:this._animateZoom},this),this._map=null},getLatLng:function(){return this._latlng},setLatLng:function(t){return this._latlng=o.latLng(t),this.update(),this.fire("move",{latlng:this._latlng})},setZIndexOffset:function(t){return this.options.zIndexOffset=t,this.update(),this},setIcon:function(t){return this.options.icon=t,this._map&&(this._initIcon(),this.update()),this._popup&&this.bindPopup(this._popup),this},update:function(){if(this._icon){var t=this._map.latLngToLayerPoint(this._latlng).round();this._setPos(t)}return this},_initIcon:function(){var t=this.options,e=this._map,i=e.options.zoomAnimation&&e.options.markerZoomAnimation,n=i?"leaflet-zoom-animated":"leaflet-zoom-hide",s=t.icon.createIcon(this._icon),a=!1;s!==this._icon&&(this._icon&&this._removeIcon(),a=!0,t.title&&(s.title=t.title),t.alt&&(s.alt=t.alt)),o.DomUtil.addClass(s,n),t.keyboard&&(s.tabIndex="0"),this._icon=s,this._initInteraction(),t.riseOnHover&&o.DomEvent.on(s,"mouseover",this._bringToFront,this).on(s,"mouseout",this._resetZIndex,this);var r=t.icon.createShadow(this._shadow),h=!1;r!==this._shadow&&(this._removeShadow(),h=!0),r&&o.DomUtil.addClass(r,n),this._shadow=r,t.opacity<1&&this._updateOpacity();var l=this._map._panes;a&&l.markerPane.appendChild(this._icon),r&&h&&l.shadowPane.appendChild(this._shadow)},_removeIcon:function(){this.options.riseOnHover&&o.DomEvent.off(this._icon,"mouseover",this._bringToFront).off(this._icon,"mouseout",this._resetZIndex),this._map._panes.markerPane.removeChild(this._icon),this._icon=null},_removeShadow:function(){this._shadow&&this._map._panes.shadowPane.removeChild(this._shadow),this._shadow=null},_setPos:function(t){o.DomUtil.setPosition(this._icon,t),this._shadow&&o.DomUtil.setPosition(this._shadow,t),this._zIndex=t.y+this.options.zIndexOffset,this._resetZIndex()},_updateZIndex:function(t){this._icon.style.zIndex=this._zIndex+t},_animateZoom:function(t){var e=this._map._latLngToNewLayerPoint(this._latlng,t.zoom,t.center).round();this._setPos(e)},_initInteraction:function(){if(this.options.clickable){var t=this._icon,e=["dblclick","mousedown","mouseover","mouseout","contextmenu"];o.DomUtil.addClass(t,"leaflet-clickable"),o.DomEvent.on(t,"click",this._onMouseClick,this),o.DomEvent.on(t,"keypress",this._onKeyPress,this);for(var i=0;i<e.length;i++)o.DomEvent.on(t,e[i],this._fireMouseEvent,this);o.Handler.MarkerDrag&&(this.dragging=new o.Handler.MarkerDrag(this),this.options.draggable&&this.dragging.enable())}},_onMouseClick:function(t){var e=this.dragging&&this.dragging.moved();(this.hasEventListeners(t.type)||e)&&o.DomEvent.stopPropagation(t),e||(this.dragging&&this.dragging._enabled||!this._map.dragging||!this._map.dragging.moved())&&this.fire(t.type,{originalEvent:t,latlng:this._latlng})},_onKeyPress:function(t){13===t.keyCode&&this.fire("click",{originalEvent:t,latlng:this._latlng})},_fireMouseEvent:function(t){this.fire(t.type,{originalEvent:t,latlng:this._latlng}),"contextmenu"===t.type&&this.hasEventListeners(t.type)&&o.DomEvent.preventDefault(t),"mousedown"!==t.type?o.DomEvent.stopPropagation(t):o.DomEvent.preventDefault(t)},setOpacity:function(t){return this.options.opacity=t,this._map&&this._updateOpacity(),this},_updateOpacity:function(){o.DomUtil.setOpacity(this._icon,this.options.opacity),this._shadow&&o.DomUtil.setOpacity(this._shadow,this.options.opacity)},_bringToFront:function(){this._updateZIndex(this.options.riseOffset)},_resetZIndex:function(){this._updateZIndex(0)}}),o.marker=function(t,e){return new o.Marker(t,e)},o.DivIcon=o.Icon.extend({options:{iconSize:[12,12],className:"leaflet-div-icon",html:!1},createIcon:function(t){var i=t&&"DIV"===t.tagName?t:e.createElement("div"),n=this.options;return i.innerHTML=n.html!==!1?n.html:"",n.bgPos&&(i.style.backgroundPosition=-n.bgPos.x+"px "+-n.bgPos.y+"px"),this._setIconStyles(i,"icon"),i},createShadow:function(){return null}}),o.divIcon=function(t){return new o.DivIcon(t)},o.Map.mergeOptions({closePopupOnClick:!0}),o.Popup=o.Class.extend({includes:o.Mixin.Events,options:{minWidth:50,maxWidth:300,autoPan:!0,closeButton:!0,offset:[0,7],autoPanPadding:[5,5],keepInView:!1,className:"",zoomAnimation:!0},initialize:function(t,e){o.setOptions(this,t),this._source=e,this._animated=o.Browser.any3d&&this.options.zoomAnimation,this._isOpen=!1},onAdd:function(t){this._map=t,this._container||this._initLayout();var e=t.options.fadeAnimation;e&&o.DomUtil.setOpacity(this._container,0),t._panes.popupPane.appendChild(this._container),t.on(this._getEvents(),this),this.update(),e&&o.DomUtil.setOpacity(this._container,1),this.fire("open"),t.fire("popupopen",{popup:this}),this._source&&this._source.fire("popupopen",{popup:this})},addTo:function(t){return t.addLayer(this),this},openOn:function(t){return t.openPopup(this),this},onRemove:function(t){t._panes.popupPane.removeChild(this._container),o.Util.falseFn(this._container.offsetWidth),t.off(this._getEvents(),this),t.options.fadeAnimation&&o.DomUtil.setOpacity(this._container,0),this._map=null,this.fire("close"),t.fire("popupclose",{popup:this}),this._source&&this._source.fire("popupclose",{popup:this})},getLatLng:function(){return this._latlng},setLatLng:function(t){return this._latlng=o.latLng(t),this._map&&(this._updatePosition(),this._adjustPan()),this},getContent:function(){return this._content},setContent:function(t){return this._content=t,this.update(),this},update:function(){this._map&&(this._container.style.visibility="hidden",this._updateContent(),this._updateLayout(),this._updatePosition(),this._container.style.visibility="",this._adjustPan())},_getEvents:function(){var t={viewreset:this._updatePosition};return this._animated&&(t.zoomanim=this._zoomAnimation),("closeOnClick"in this.options?this.options.closeOnClick:this._map.options.closePopupOnClick)&&(t.preclick=this._close),this.options.keepInView&&(t.moveend=this._adjustPan),t},_close:function(){this._map&&this._map.closePopup(this)},_initLayout:function(){var t,e="leaflet-popup",i=e+" "+this.options.className+" leaflet-zoom-"+(this._animated?"animated":"hide"),n=this._container=o.DomUtil.create("div",i);this.options.closeButton&&(t=this._closeButton=o.DomUtil.create("a",e+"-close-button",n),t.href="#close",t.innerHTML="&#215;",o.DomEvent.disableClickPropagation(t),o.DomEvent.on(t,"click",this._onCloseButtonClick,this));var s=this._wrapper=o.DomUtil.create("div",e+"-content-wrapper",n);o.DomEvent.disableClickPropagation(s),this._contentNode=o.DomUtil.create("div",e+"-content",s),o.DomEvent.disableScrollPropagation(this._contentNode),o.DomEvent.on(s,"contextmenu",o.DomEvent.stopPropagation),this._tipContainer=o.DomUtil.create("div",e+"-tip-container",n),this._tip=o.DomUtil.create("div",e+"-tip",this._tipContainer)},_updateContent:function(){if(this._content){if("string"==typeof this._content)this._contentNode.innerHTML=this._content;else{for(;this._contentNode.hasChildNodes();)this._contentNode.removeChild(this._contentNode.firstChild);this._contentNode.appendChild(this._content)}this.fire("contentupdate")}},_updateLayout:function(){var t=this._contentNode,e=t.style;e.width="",e.whiteSpace="nowrap";var i=t.offsetWidth;i=Math.min(i,this.options.maxWidth),i=Math.max(i,this.options.minWidth),e.width=i+1+"px",e.whiteSpace="",e.height="";var n=t.offsetHeight,s=this.options.maxHeight,a="leaflet-popup-scrolled";s&&n>s?(e.height=s+"px",o.DomUtil.addClass(t,a)):o.DomUtil.removeClass(t,a),this._containerWidth=this._container.offsetWidth},_updatePosition:function(){if(this._map){var t=this._map.latLngToLayerPoint(this._latlng),e=this._animated,i=o.point(this.options.offset);e&&o.DomUtil.setPosition(this._container,t),this._containerBottom=-i.y-(e?0:t.y),this._containerLeft=-Math.round(this._containerWidth/2)+i.x+(e?0:t.x),this._container.style.bottom=this._containerBottom+"px",this._container.style.left=this._containerLeft+"px"}},_zoomAnimation:function(t){var e=this._map._latLngToNewLayerPoint(this._latlng,t.zoom,t.center);o.DomUtil.setPosition(this._container,e)},_adjustPan:function(){if(this.options.autoPan){var t=this._map,e=this._container.offsetHeight,i=this._containerWidth,n=new o.Point(this._containerLeft,-e-this._containerBottom);this._animated&&n._add(o.DomUtil.getPosition(this._container));var s=t.layerPointToContainerPoint(n),a=o.point(this.options.autoPanPadding),r=o.point(this.options.autoPanPaddingTopLeft||a),h=o.point(this.options.autoPanPaddingBottomRight||a),l=t.getSize(),u=0,c=0;s.x+i+h.x>l.x&&(u=s.x+i-l.x+h.x),s.x-u-r.x<0&&(u=s.x-r.x),s.y+e+h.y>l.y&&(c=s.y+e-l.y+h.y),s.y-c-r.y<0&&(c=s.y-r.y),(u||c)&&t.fire("autopanstart").panBy([u,c])}},_onCloseButtonClick:function(t){this._close(),o.DomEvent.stop(t)}}),o.popup=function(t,e){return new o.Popup(t,e)},o.Map.include({openPopup:function(t,e,i){if(this.closePopup(),!(t instanceof o.Popup)){var n=t;t=new o.Popup(i).setLatLng(e).setContent(n)}return t._isOpen=!0,this._popup=t,this.addLayer(t)},closePopup:function(t){return t&&t!==this._popup||(t=this._popup,this._popup=null),t&&(this.removeLayer(t),t._isOpen=!1),this}}),o.Marker.include({openPopup:function(){return this._popup&&this._map&&!this._map.hasLayer(this._popup)&&(this._popup.setLatLng(this._latlng),this._map.openPopup(this._popup)),this},closePopup:function(){return this._popup&&this._popup._close(),this},togglePopup:function(){return this._popup&&(this._popup._isOpen?this.closePopup():this.openPopup()),this},bindPopup:function(t,e){var i=o.point(this.options.icon.options.popupAnchor||[0,0]);return i=i.add(o.Popup.prototype.options.offset),e&&e.offset&&(i=i.add(e.offset)),e=o.extend({offset:i},e),this._popupHandlersAdded||(this.on("click",this.togglePopup,this).on("remove",this.closePopup,this).on("move",this._movePopup,this),this._popupHandlersAdded=!0),t instanceof o.Popup?(o.setOptions(t,e),this._popup=t):this._popup=new o.Popup(e,this).setContent(t),this},setPopupContent:function(t){return this._popup&&this._popup.setContent(t),this},unbindPopup:function(){return this._popup&&(this._popup=null,this.off("click",this.togglePopup,this).off("remove",this.closePopup,this).off("move",this._movePopup,this),this._popupHandlersAdded=!1),this},getPopup:function(){return this._popup},_movePopup:function(t){this._popup.setLatLng(t.latlng)}}),o.LayerGroup=o.Class.extend({initialize:function(t){this._layers={};var e,i;if(t)for(e=0,i=t.length;i>e;e++)this.addLayer(t[e])},addLayer:function(t){var e=this.getLayerId(t);return this._layers[e]=t,this._map&&this._map.addLayer(t),this},removeLayer:function(t){var e=t in this._layers?t:this.getLayerId(t);return this._map&&this._layers[e]&&this._map.removeLayer(this._layers[e]),delete this._layers[e],this},hasLayer:function(t){return t?t in this._layers||this.getLayerId(t)in this._layers:!1},clearLayers:function(){return this.eachLayer(this.removeLayer,this),this},invoke:function(t){var e,i,n=Array.prototype.slice.call(arguments,1);for(e in this._layers)i=this._layers[e],i[t]&&i[t].apply(i,n);return this},onAdd:function(t){this._map=t,this.eachLayer(t.addLayer,t)},onRemove:function(t){this.eachLayer(t.removeLayer,t),this._map=null},addTo:function(t){return t.addLayer(this),this},eachLayer:function(t,e){for(var i in this._layers)t.call(e,this._layers[i]);return this},getLayer:function(t){return this._layers[t]},getLayers:function(){var t=[];for(var e in this._layers)t.push(this._layers[e]);return t},setZIndex:function(t){return this.invoke("setZIndex",t)},getLayerId:function(t){return o.stamp(t)}}),o.layerGroup=function(t){return new o.LayerGroup(t)},o.FeatureGroup=o.LayerGroup.extend({includes:o.Mixin.Events,statics:{EVENTS:"click dblclick mouseover mouseout mousemove contextmenu popupopen popupclose"},addLayer:function(t){return this.hasLayer(t)?this:("on"in t&&t.on(o.FeatureGroup.EVENTS,this._propagateEvent,this),o.LayerGroup.prototype.addLayer.call(this,t),this._popupContent&&t.bindPopup&&t.bindPopup(this._popupContent,this._popupOptions),this.fire("layeradd",{layer:t}))},removeLayer:function(t){return this.hasLayer(t)?(t in this._layers&&(t=this._layers[t]),t.off(o.FeatureGroup.EVENTS,this._propagateEvent,this),o.LayerGroup.prototype.removeLayer.call(this,t),this._popupContent&&this.invoke("unbindPopup"),this.fire("layerremove",{layer:t})):this},bindPopup:function(t,e){return this._popupContent=t,this._popupOptions=e,this.invoke("bindPopup",t,e)},openPopup:function(t){for(var e in this._layers){this._layers[e].openPopup(t);break}return this},setStyle:function(t){return this.invoke("setStyle",t)},bringToFront:function(){return this.invoke("bringToFront")},bringToBack:function(){return this.invoke("bringToBack")},getBounds:function(){var t=new o.LatLngBounds;return this.eachLayer(function(e){t.extend(e instanceof o.Marker?e.getLatLng():e.getBounds())}),t},_propagateEvent:function(t){t=o.extend({layer:t.target,target:this},t),this.fire(t.type,t)}}),o.featureGroup=function(t){return new o.FeatureGroup(t)},o.Path=o.Class.extend({includes:[o.Mixin.Events],statics:{CLIP_PADDING:function(){var e=o.Browser.mobile?1280:2e3,i=(e/Math.max(t.outerWidth,t.outerHeight)-1)/2;return Math.max(0,Math.min(.5,i))}()},options:{stroke:!0,color:"#0033ff",dashArray:null,lineCap:null,lineJoin:null,weight:5,opacity:.5,fill:!1,fillColor:null,fillOpacity:.2,clickable:!0},initialize:function(t){o.setOptions(this,t)},onAdd:function(t){this._map=t,this._container||(this._initElements(),this._initEvents()),this.projectLatlngs(),this._updatePath(),this._container&&this._map._pathRoot.appendChild(this._container),this.fire("add"),t.on({viewreset:this.projectLatlngs,moveend:this._updatePath},this)},addTo:function(t){return t.addLayer(this),this},onRemove:function(t){t._pathRoot.removeChild(this._container),this.fire("remove"),this._map=null,o.Browser.vml&&(this._container=null,this._stroke=null,this._fill=null),t.off({viewreset:this.projectLatlngs,moveend:this._updatePath},this)},projectLatlngs:function(){},setStyle:function(t){return o.setOptions(this,t),this._container&&this._updateStyle(),this},redraw:function(){return this._map&&(this.projectLatlngs(),this._updatePath()),this}}),o.Map.include({_updatePathViewport:function(){var t=o.Path.CLIP_PADDING,e=this.getSize(),i=o.DomUtil.getPosition(this._mapPane),n=i.multiplyBy(-1)._subtract(e.multiplyBy(t)._round()),s=n.add(e.multiplyBy(1+2*t)._round());this._pathViewport=new o.Bounds(n,s)}}),o.Path.SVG_NS="http://www.w3.org/2000/svg",o.Browser.svg=!(!e.createElementNS||!e.createElementNS(o.Path.SVG_NS,"svg").createSVGRect),o.Path=o.Path.extend({statics:{SVG:o.Browser.svg},bringToFront:function(){var t=this._map._pathRoot,e=this._container;return e&&t.lastChild!==e&&t.appendChild(e),this},bringToBack:function(){var t=this._map._pathRoot,e=this._container,i=t.firstChild;return e&&i!==e&&t.insertBefore(e,i),this},getPathString:function(){},_createElement:function(t){return e.createElementNS(o.Path.SVG_NS,t)},_initElements:function(){this._map._initPathRoot(),this._initPath(),this._initStyle()},_initPath:function(){this._container=this._createElement("g"),this._path=this._createElement("path"),this.options.className&&o.DomUtil.addClass(this._path,this.options.className),this._container.appendChild(this._path)},_initStyle:function(){this.options.stroke&&(this._path.setAttribute("stroke-linejoin","round"),this._path.setAttribute("stroke-linecap","round")),this.options.fill&&this._path.setAttribute("fill-rule","evenodd"),this.options.pointerEvents&&this._path.setAttribute("pointer-events",this.options.pointerEvents),this.options.clickable||this.options.pointerEvents||this._path.setAttribute("pointer-events","none"),this._updateStyle()},_updateStyle:function(){this.options.stroke?(this._path.setAttribute("stroke",this.options.color),this._path.setAttribute("stroke-opacity",this.options.opacity),this._path.setAttribute("stroke-width",this.options.weight),this.options.dashArray?this._path.setAttribute("stroke-dasharray",this.options.dashArray):this._path.removeAttribute("stroke-dasharray"),this.options.lineCap&&this._path.setAttribute("stroke-linecap",this.options.lineCap),this.options.lineJoin&&this._path.setAttribute("stroke-linejoin",this.options.lineJoin)):this._path.setAttribute("stroke","none"),this.options.fill?(this._path.setAttribute("fill",this.options.fillColor||this.options.color),this._path.setAttribute("fill-opacity",this.options.fillOpacity)):this._path.setAttribute("fill","none")},_updatePath:function(){var t=this.getPathString();t||(t="M0 0"),this._path.setAttribute("d",t)},_initEvents:function(){if(this.options.clickable){(o.Browser.svg||!o.Browser.vml)&&o.DomUtil.addClass(this._path,"leaflet-clickable"),o.DomEvent.on(this._container,"click",this._onMouseClick,this);for(var t=["dblclick","mousedown","mouseover","mouseout","mousemove","contextmenu"],e=0;e<t.length;e++)o.DomEvent.on(this._container,t[e],this._fireMouseEvent,this)}},_onMouseClick:function(t){this._map.dragging&&this._map.dragging.moved()||this._fireMouseEvent(t)},_fireMouseEvent:function(t){if(this.hasEventListeners(t.type)){var e=this._map,i=e.mouseEventToContainerPoint(t),n=e.containerPointToLayerPoint(i),s=e.layerPointToLatLng(n);this.fire(t.type,{latlng:s,layerPoint:n,containerPoint:i,originalEvent:t}),"contextmenu"===t.type&&o.DomEvent.preventDefault(t),"mousemove"!==t.type&&o.DomEvent.stopPropagation(t)}}}),o.Map.include({_initPathRoot:function(){this._pathRoot||(this._pathRoot=o.Path.prototype._createElement("svg"),this._panes.overlayPane.appendChild(this._pathRoot),this.options.zoomAnimation&&o.Browser.any3d?(o.DomUtil.addClass(this._pathRoot,"leaflet-zoom-animated"),this.on({zoomanim:this._animatePathZoom,zoomend:this._endPathZoom})):o.DomUtil.addClass(this._pathRoot,"leaflet-zoom-hide"),this.on("moveend",this._updateSvgViewport),this._updateSvgViewport())
},_animatePathZoom:function(t){var e=this.getZoomScale(t.zoom),i=this._getCenterOffset(t.center)._multiplyBy(-e)._add(this._pathViewport.min);this._pathRoot.style[o.DomUtil.TRANSFORM]=o.DomUtil.getTranslateString(i)+" scale("+e+") ",this._pathZooming=!0},_endPathZoom:function(){this._pathZooming=!1},_updateSvgViewport:function(){if(!this._pathZooming){this._updatePathViewport();var t=this._pathViewport,e=t.min,i=t.max,n=i.x-e.x,s=i.y-e.y,a=this._pathRoot,r=this._panes.overlayPane;o.Browser.mobileWebkit&&r.removeChild(a),o.DomUtil.setPosition(a,e),a.setAttribute("width",n),a.setAttribute("height",s),a.setAttribute("viewBox",[e.x,e.y,n,s].join(" ")),o.Browser.mobileWebkit&&r.appendChild(a)}}}),o.Path.include({bindPopup:function(t,e){return t instanceof o.Popup?this._popup=t:((!this._popup||e)&&(this._popup=new o.Popup(e,this)),this._popup.setContent(t)),this._popupHandlersAdded||(this.on("click",this._openPopup,this).on("remove",this.closePopup,this),this._popupHandlersAdded=!0),this},unbindPopup:function(){return this._popup&&(this._popup=null,this.off("click",this._openPopup).off("remove",this.closePopup),this._popupHandlersAdded=!1),this},openPopup:function(t){return this._popup&&(t=t||this._latlng||this._latlngs[Math.floor(this._latlngs.length/2)],this._openPopup({latlng:t})),this},closePopup:function(){return this._popup&&this._popup._close(),this},_openPopup:function(t){this._popup.setLatLng(t.latlng),this._map.openPopup(this._popup)}}),o.Browser.vml=!o.Browser.svg&&function(){try{var t=e.createElement("div");t.innerHTML='<v:shape adj="1"/>';var i=t.firstChild;return i.style.behavior="url(#default#VML)",i&&"object"==typeof i.adj}catch(n){return!1}}(),o.Path=o.Browser.svg||!o.Browser.vml?o.Path:o.Path.extend({statics:{VML:!0,CLIP_PADDING:.02},_createElement:function(){try{return e.namespaces.add("lvml","urn:schemas-microsoft-com:vml"),function(t){return e.createElement("<lvml:"+t+' class="lvml">')}}catch(t){return function(t){return e.createElement("<"+t+' xmlns="urn:schemas-microsoft.com:vml" class="lvml">')}}}(),_initPath:function(){var t=this._container=this._createElement("shape");o.DomUtil.addClass(t,"leaflet-vml-shape"+(this.options.className?" "+this.options.className:"")),this.options.clickable&&o.DomUtil.addClass(t,"leaflet-clickable"),t.coordsize="1 1",this._path=this._createElement("path"),t.appendChild(this._path),this._map._pathRoot.appendChild(t)},_initStyle:function(){this._updateStyle()},_updateStyle:function(){var t=this._stroke,e=this._fill,i=this.options,n=this._container;n.stroked=i.stroke,n.filled=i.fill,i.stroke?(t||(t=this._stroke=this._createElement("stroke"),t.endcap="round",n.appendChild(t)),t.weight=i.weight+"px",t.color=i.color,t.opacity=i.opacity,t.dashStyle=i.dashArray?o.Util.isArray(i.dashArray)?i.dashArray.join(" "):i.dashArray.replace(/( *, *)/g," "):"",i.lineCap&&(t.endcap=i.lineCap.replace("butt","flat")),i.lineJoin&&(t.joinstyle=i.lineJoin)):t&&(n.removeChild(t),this._stroke=null),i.fill?(e||(e=this._fill=this._createElement("fill"),n.appendChild(e)),e.color=i.fillColor||i.color,e.opacity=i.fillOpacity):e&&(n.removeChild(e),this._fill=null)},_updatePath:function(){var t=this._container.style;t.display="none",this._path.v=this.getPathString()+" ",t.display=""}}),o.Map.include(o.Browser.svg||!o.Browser.vml?{}:{_initPathRoot:function(){if(!this._pathRoot){var t=this._pathRoot=e.createElement("div");t.className="leaflet-vml-container",this._panes.overlayPane.appendChild(t),this.on("moveend",this._updatePathViewport),this._updatePathViewport()}}}),o.Browser.canvas=function(){return!!e.createElement("canvas").getContext}(),o.Path=o.Path.SVG&&!t.L_PREFER_CANVAS||!o.Browser.canvas?o.Path:o.Path.extend({statics:{CANVAS:!0,SVG:!1},redraw:function(){return this._map&&(this.projectLatlngs(),this._requestUpdate()),this},setStyle:function(t){return o.setOptions(this,t),this._map&&(this._updateStyle(),this._requestUpdate()),this},onRemove:function(t){t.off("viewreset",this.projectLatlngs,this).off("moveend",this._updatePath,this),this.options.clickable&&(this._map.off("click",this._onClick,this),this._map.off("mousemove",this._onMouseMove,this)),this._requestUpdate(),this._map=null},_requestUpdate:function(){this._map&&!o.Path._updateRequest&&(o.Path._updateRequest=o.Util.requestAnimFrame(this._fireMapMoveEnd,this._map))},_fireMapMoveEnd:function(){o.Path._updateRequest=null,this.fire("moveend")},_initElements:function(){this._map._initPathRoot(),this._ctx=this._map._canvasCtx},_updateStyle:function(){var t=this.options;t.stroke&&(this._ctx.lineWidth=t.weight,this._ctx.strokeStyle=t.color),t.fill&&(this._ctx.fillStyle=t.fillColor||t.color)},_drawPath:function(){var t,e,i,n,s,a;for(this._ctx.beginPath(),t=0,i=this._parts.length;i>t;t++){for(e=0,n=this._parts[t].length;n>e;e++)s=this._parts[t][e],a=(0===e?"move":"line")+"To",this._ctx[a](s.x,s.y);this instanceof o.Polygon&&this._ctx.closePath()}},_checkIfEmpty:function(){return!this._parts.length},_updatePath:function(){if(!this._checkIfEmpty()){var t=this._ctx,e=this.options;this._drawPath(),t.save(),this._updateStyle(),e.fill&&(t.globalAlpha=e.fillOpacity,t.fill()),e.stroke&&(t.globalAlpha=e.opacity,t.stroke()),t.restore()}},_initEvents:function(){this.options.clickable&&(this._map.on("mousemove",this._onMouseMove,this),this._map.on("click",this._onClick,this))},_onClick:function(t){this._containsPoint(t.layerPoint)&&this.fire("click",t)},_onMouseMove:function(t){this._map&&!this._map._animatingZoom&&(this._containsPoint(t.layerPoint)?(this._ctx.canvas.style.cursor="pointer",this._mouseInside=!0,this.fire("mouseover",t)):this._mouseInside&&(this._ctx.canvas.style.cursor="",this._mouseInside=!1,this.fire("mouseout",t)))}}),o.Map.include(o.Path.SVG&&!t.L_PREFER_CANVAS||!o.Browser.canvas?{}:{_initPathRoot:function(){var t,i=this._pathRoot;i||(i=this._pathRoot=e.createElement("canvas"),i.style.position="absolute",t=this._canvasCtx=i.getContext("2d"),t.lineCap="round",t.lineJoin="round",this._panes.overlayPane.appendChild(i),this.options.zoomAnimation&&(this._pathRoot.className="leaflet-zoom-animated",this.on("zoomanim",this._animatePathZoom),this.on("zoomend",this._endPathZoom)),this.on("moveend",this._updateCanvasViewport),this._updateCanvasViewport())},_updateCanvasViewport:function(){if(!this._pathZooming){this._updatePathViewport();var t=this._pathViewport,e=t.min,i=t.max.subtract(e),n=this._pathRoot;o.DomUtil.setPosition(n,e),n.width=i.x,n.height=i.y,n.getContext("2d").translate(-e.x,-e.y)}}}),o.LineUtil={simplify:function(t,e){if(!e||!t.length)return t.slice();var i=e*e;return t=this._reducePoints(t,i),t=this._simplifyDP(t,i)},pointToSegmentDistance:function(t,e,i){return Math.sqrt(this._sqClosestPointOnSegment(t,e,i,!0))},closestPointOnSegment:function(t,e,i){return this._sqClosestPointOnSegment(t,e,i)},_simplifyDP:function(t,e){var n=t.length,o=typeof Uint8Array!=i+""?Uint8Array:Array,s=new o(n);s[0]=s[n-1]=1,this._simplifyDPStep(t,s,e,0,n-1);var a,r=[];for(a=0;n>a;a++)s[a]&&r.push(t[a]);return r},_simplifyDPStep:function(t,e,i,n,o){var s,a,r,h=0;for(a=n+1;o-1>=a;a++)r=this._sqClosestPointOnSegment(t[a],t[n],t[o],!0),r>h&&(s=a,h=r);h>i&&(e[s]=1,this._simplifyDPStep(t,e,i,n,s),this._simplifyDPStep(t,e,i,s,o))},_reducePoints:function(t,e){for(var i=[t[0]],n=1,o=0,s=t.length;s>n;n++)this._sqDist(t[n],t[o])>e&&(i.push(t[n]),o=n);return s-1>o&&i.push(t[s-1]),i},clipSegment:function(t,e,i,n){var o,s,a,r=n?this._lastCode:this._getBitCode(t,i),h=this._getBitCode(e,i);for(this._lastCode=h;;){if(!(r|h))return[t,e];if(r&h)return!1;o=r||h,s=this._getEdgeIntersection(t,e,o,i),a=this._getBitCode(s,i),o===r?(t=s,r=a):(e=s,h=a)}},_getEdgeIntersection:function(t,e,i,n){var s=e.x-t.x,a=e.y-t.y,r=n.min,h=n.max;return 8&i?new o.Point(t.x+s*(h.y-t.y)/a,h.y):4&i?new o.Point(t.x+s*(r.y-t.y)/a,r.y):2&i?new o.Point(h.x,t.y+a*(h.x-t.x)/s):1&i?new o.Point(r.x,t.y+a*(r.x-t.x)/s):void 0},_getBitCode:function(t,e){var i=0;return t.x<e.min.x?i|=1:t.x>e.max.x&&(i|=2),t.y<e.min.y?i|=4:t.y>e.max.y&&(i|=8),i},_sqDist:function(t,e){var i=e.x-t.x,n=e.y-t.y;return i*i+n*n},_sqClosestPointOnSegment:function(t,e,i,n){var s,a=e.x,r=e.y,h=i.x-a,l=i.y-r,u=h*h+l*l;return u>0&&(s=((t.x-a)*h+(t.y-r)*l)/u,s>1?(a=i.x,r=i.y):s>0&&(a+=h*s,r+=l*s)),h=t.x-a,l=t.y-r,n?h*h+l*l:new o.Point(a,r)}},o.Polyline=o.Path.extend({initialize:function(t,e){o.Path.prototype.initialize.call(this,e),this._latlngs=this._convertLatLngs(t)},options:{smoothFactor:1,noClip:!1},projectLatlngs:function(){this._originalPoints=[];for(var t=0,e=this._latlngs.length;e>t;t++)this._originalPoints[t]=this._map.latLngToLayerPoint(this._latlngs[t])},getPathString:function(){for(var t=0,e=this._parts.length,i="";e>t;t++)i+=this._getPathPartStr(this._parts[t]);return i},getLatLngs:function(){return this._latlngs},setLatLngs:function(t){return this._latlngs=this._convertLatLngs(t),this.redraw()},addLatLng:function(t){return this._latlngs.push(o.latLng(t)),this.redraw()},spliceLatLngs:function(){var t=[].splice.apply(this._latlngs,arguments);return this._convertLatLngs(this._latlngs,!0),this.redraw(),t},closestLayerPoint:function(t){for(var e,i,n=1/0,s=this._parts,a=null,r=0,h=s.length;h>r;r++)for(var l=s[r],u=1,c=l.length;c>u;u++){e=l[u-1],i=l[u];var d=o.LineUtil._sqClosestPointOnSegment(t,e,i,!0);n>d&&(n=d,a=o.LineUtil._sqClosestPointOnSegment(t,e,i))}return a&&(a.distance=Math.sqrt(n)),a},getBounds:function(){return new o.LatLngBounds(this.getLatLngs())},_convertLatLngs:function(t,e){var i,n,s=e?t:[];for(i=0,n=t.length;n>i;i++){if(o.Util.isArray(t[i])&&"number"!=typeof t[i][0])return;s[i]=o.latLng(t[i])}return s},_initEvents:function(){o.Path.prototype._initEvents.call(this)},_getPathPartStr:function(t){for(var e,i=o.Path.VML,n=0,s=t.length,a="";s>n;n++)e=t[n],i&&e._round(),a+=(n?"L":"M")+e.x+" "+e.y;return a},_clipPoints:function(){var t,e,i,n=this._originalPoints,s=n.length;if(this.options.noClip)return void(this._parts=[n]);this._parts=[];var a=this._parts,r=this._map._pathViewport,h=o.LineUtil;for(t=0,e=0;s-1>t;t++)i=h.clipSegment(n[t],n[t+1],r,t),i&&(a[e]=a[e]||[],a[e].push(i[0]),(i[1]!==n[t+1]||t===s-2)&&(a[e].push(i[1]),e++))},_simplifyPoints:function(){for(var t=this._parts,e=o.LineUtil,i=0,n=t.length;n>i;i++)t[i]=e.simplify(t[i],this.options.smoothFactor)},_updatePath:function(){this._map&&(this._clipPoints(),this._simplifyPoints(),o.Path.prototype._updatePath.call(this))}}),o.polyline=function(t,e){return new o.Polyline(t,e)},o.PolyUtil={},o.PolyUtil.clipPolygon=function(t,e){var i,n,s,a,r,h,l,u,c,d=[1,4,2,8],p=o.LineUtil;for(n=0,l=t.length;l>n;n++)t[n]._code=p._getBitCode(t[n],e);for(a=0;4>a;a++){for(u=d[a],i=[],n=0,l=t.length,s=l-1;l>n;s=n++)r=t[n],h=t[s],r._code&u?h._code&u||(c=p._getEdgeIntersection(h,r,u,e),c._code=p._getBitCode(c,e),i.push(c)):(h._code&u&&(c=p._getEdgeIntersection(h,r,u,e),c._code=p._getBitCode(c,e),i.push(c)),i.push(r));t=i}return t},o.Polygon=o.Polyline.extend({options:{fill:!0},initialize:function(t,e){o.Polyline.prototype.initialize.call(this,t,e),this._initWithHoles(t)},_initWithHoles:function(t){var e,i,n;if(t&&o.Util.isArray(t[0])&&"number"!=typeof t[0][0])for(this._latlngs=this._convertLatLngs(t[0]),this._holes=t.slice(1),e=0,i=this._holes.length;i>e;e++)n=this._holes[e]=this._convertLatLngs(this._holes[e]),n[0].equals(n[n.length-1])&&n.pop();t=this._latlngs,t.length>=2&&t[0].equals(t[t.length-1])&&t.pop()},projectLatlngs:function(){if(o.Polyline.prototype.projectLatlngs.call(this),this._holePoints=[],this._holes){var t,e,i,n;for(t=0,i=this._holes.length;i>t;t++)for(this._holePoints[t]=[],e=0,n=this._holes[t].length;n>e;e++)this._holePoints[t][e]=this._map.latLngToLayerPoint(this._holes[t][e])}},setLatLngs:function(t){return t&&o.Util.isArray(t[0])&&"number"!=typeof t[0][0]?(this._initWithHoles(t),this.redraw()):o.Polyline.prototype.setLatLngs.call(this,t)},_clipPoints:function(){var t=this._originalPoints,e=[];if(this._parts=[t].concat(this._holePoints),!this.options.noClip){for(var i=0,n=this._parts.length;n>i;i++){var s=o.PolyUtil.clipPolygon(this._parts[i],this._map._pathViewport);s.length&&e.push(s)}this._parts=e}},_getPathPartStr:function(t){var e=o.Polyline.prototype._getPathPartStr.call(this,t);return e+(o.Browser.svg?"z":"x")}}),o.polygon=function(t,e){return new o.Polygon(t,e)},function(){function t(t){return o.FeatureGroup.extend({initialize:function(t,e){this._layers={},this._options=e,this.setLatLngs(t)},setLatLngs:function(e){var i=0,n=e.length;for(this.eachLayer(function(t){n>i?t.setLatLngs(e[i++]):this.removeLayer(t)},this);n>i;)this.addLayer(new t(e[i++],this._options));return this},getLatLngs:function(){var t=[];return this.eachLayer(function(e){t.push(e.getLatLngs())}),t}})}o.MultiPolyline=t(o.Polyline),o.MultiPolygon=t(o.Polygon),o.multiPolyline=function(t,e){return new o.MultiPolyline(t,e)},o.multiPolygon=function(t,e){return new o.MultiPolygon(t,e)}}(),o.Rectangle=o.Polygon.extend({initialize:function(t,e){o.Polygon.prototype.initialize.call(this,this._boundsToLatLngs(t),e)},setBounds:function(t){this.setLatLngs(this._boundsToLatLngs(t))},_boundsToLatLngs:function(t){return t=o.latLngBounds(t),[t.getSouthWest(),t.getNorthWest(),t.getNorthEast(),t.getSouthEast()]}}),o.rectangle=function(t,e){return new o.Rectangle(t,e)},o.Circle=o.Path.extend({initialize:function(t,e,i){o.Path.prototype.initialize.call(this,i),this._latlng=o.latLng(t),this._mRadius=e},options:{fill:!0},setLatLng:function(t){return this._latlng=o.latLng(t),this.redraw()},setRadius:function(t){return this._mRadius=t,this.redraw()},projectLatlngs:function(){var t=this._getLngRadius(),e=this._latlng,i=this._map.latLngToLayerPoint([e.lat,e.lng-t]);this._point=this._map.latLngToLayerPoint(e),this._radius=Math.max(this._point.x-i.x,1)},getBounds:function(){var t=this._getLngRadius(),e=this._mRadius/40075017*360,i=this._latlng;return new o.LatLngBounds([i.lat-e,i.lng-t],[i.lat+e,i.lng+t])},getLatLng:function(){return this._latlng},getPathString:function(){var t=this._point,e=this._radius;return this._checkIfEmpty()?"":o.Browser.svg?"M"+t.x+","+(t.y-e)+"A"+e+","+e+",0,1,1,"+(t.x-.1)+","+(t.y-e)+" z":(t._round(),e=Math.round(e),"AL "+t.x+","+t.y+" "+e+","+e+" 0,23592600")},getRadius:function(){return this._mRadius},_getLatRadius:function(){return this._mRadius/40075017*360},_getLngRadius:function(){return this._getLatRadius()/Math.cos(o.LatLng.DEG_TO_RAD*this._latlng.lat)},_checkIfEmpty:function(){if(!this._map)return!1;var t=this._map._pathViewport,e=this._radius,i=this._point;return i.x-e>t.max.x||i.y-e>t.max.y||i.x+e<t.min.x||i.y+e<t.min.y}}),o.circle=function(t,e,i){return new o.Circle(t,e,i)},o.CircleMarker=o.Circle.extend({options:{radius:10,weight:2},initialize:function(t,e){o.Circle.prototype.initialize.call(this,t,null,e),this._radius=this.options.radius},projectLatlngs:function(){this._point=this._map.latLngToLayerPoint(this._latlng)},_updateStyle:function(){o.Circle.prototype._updateStyle.call(this),this.setRadius(this.options.radius)},setLatLng:function(t){return o.Circle.prototype.setLatLng.call(this,t),this._popup&&this._popup._isOpen&&this._popup.setLatLng(t),this},setRadius:function(t){return this.options.radius=this._radius=t,this.redraw()},getRadius:function(){return this._radius}}),o.circleMarker=function(t,e){return new o.CircleMarker(t,e)},o.Polyline.include(o.Path.CANVAS?{_containsPoint:function(t,e){var i,n,s,a,r,h,l,u=this.options.weight/2;for(o.Browser.touch&&(u+=10),i=0,a=this._parts.length;a>i;i++)for(l=this._parts[i],n=0,r=l.length,s=r-1;r>n;s=n++)if((e||0!==n)&&(h=o.LineUtil.pointToSegmentDistance(t,l[s],l[n]),u>=h))return!0;return!1}}:{}),o.Polygon.include(o.Path.CANVAS?{_containsPoint:function(t){var e,i,n,s,a,r,h,l,u=!1;if(o.Polyline.prototype._containsPoint.call(this,t,!0))return!0;for(s=0,h=this._parts.length;h>s;s++)for(e=this._parts[s],a=0,l=e.length,r=l-1;l>a;r=a++)i=e[a],n=e[r],i.y>t.y!=n.y>t.y&&t.x<(n.x-i.x)*(t.y-i.y)/(n.y-i.y)+i.x&&(u=!u);return u}}:{}),o.Circle.include(o.Path.CANVAS?{_drawPath:function(){var t=this._point;this._ctx.beginPath(),this._ctx.arc(t.x,t.y,this._radius,0,2*Math.PI,!1)},_containsPoint:function(t){var e=this._point,i=this.options.stroke?this.options.weight/2:0;return t.distanceTo(e)<=this._radius+i}}:{}),o.CircleMarker.include(o.Path.CANVAS?{_updateStyle:function(){o.Path.prototype._updateStyle.call(this)}}:{}),o.GeoJSON=o.FeatureGroup.extend({initialize:function(t,e){o.setOptions(this,e),this._layers={},t&&this.addData(t)},addData:function(t){var e,i,n,s=o.Util.isArray(t)?t:t.features;if(s){for(e=0,i=s.length;i>e;e++)n=s[e],(n.geometries||n.geometry||n.features||n.coordinates)&&this.addData(s[e]);return this}var a=this.options;if(!a.filter||a.filter(t)){var r=o.GeoJSON.geometryToLayer(t,a.pointToLayer,a.coordsToLatLng,a);return r.feature=o.GeoJSON.asFeature(t),r.defaultOptions=r.options,this.resetStyle(r),a.onEachFeature&&a.onEachFeature(t,r),this.addLayer(r)}},resetStyle:function(t){var e=this.options.style;e&&(o.Util.extend(t.options,t.defaultOptions),this._setLayerStyle(t,e))},setStyle:function(t){this.eachLayer(function(e){this._setLayerStyle(e,t)},this)},_setLayerStyle:function(t,e){"function"==typeof e&&(e=e(t.feature)),t.setStyle&&t.setStyle(e)}}),o.extend(o.GeoJSON,{geometryToLayer:function(t,e,i,n){var s,a,r,h,l="Feature"===t.type?t.geometry:t,u=l.coordinates,c=[];switch(i=i||this.coordsToLatLng,l.type){case"Point":return s=i(u),e?e(t,s):new o.Marker(s);case"MultiPoint":for(r=0,h=u.length;h>r;r++)s=i(u[r]),c.push(e?e(t,s):new o.Marker(s));return new o.FeatureGroup(c);case"LineString":return a=this.coordsToLatLngs(u,0,i),new o.Polyline(a,n);case"Polygon":if(2===u.length&&!u[1].length)throw new Error("Invalid GeoJSON object.");return a=this.coordsToLatLngs(u,1,i),new o.Polygon(a,n);case"MultiLineString":return a=this.coordsToLatLngs(u,1,i),new o.MultiPolyline(a,n);case"MultiPolygon":return a=this.coordsToLatLngs(u,2,i),new o.MultiPolygon(a,n);case"GeometryCollection":for(r=0,h=l.geometries.length;h>r;r++)c.push(this.geometryToLayer({geometry:l.geometries[r],type:"Feature",properties:t.properties},e,i,n));return new o.FeatureGroup(c);default:throw new Error("Invalid GeoJSON object.")}},coordsToLatLng:function(t){return new o.LatLng(t[1],t[0],t[2])},coordsToLatLngs:function(t,e,i){var n,o,s,a=[];for(o=0,s=t.length;s>o;o++)n=e?this.coordsToLatLngs(t[o],e-1,i):(i||this.coordsToLatLng)(t[o]),a.push(n);return a},latLngToCoords:function(t){var e=[t.lng,t.lat];return t.alt!==i&&e.push(t.alt),e},latLngsToCoords:function(t){for(var e=[],i=0,n=t.length;n>i;i++)e.push(o.GeoJSON.latLngToCoords(t[i]));return e},getFeature:function(t,e){return t.feature?o.extend({},t.feature,{geometry:e}):o.GeoJSON.asFeature(e)},asFeature:function(t){return"Feature"===t.type?t:{type:"Feature",properties:{},geometry:t}}});var a={toGeoJSON:function(){return o.GeoJSON.getFeature(this,{type:"Point",coordinates:o.GeoJSON.latLngToCoords(this.getLatLng())})}};o.Marker.include(a),o.Circle.include(a),o.CircleMarker.include(a),o.Polyline.include({toGeoJSON:function(){return o.GeoJSON.getFeature(this,{type:"LineString",coordinates:o.GeoJSON.latLngsToCoords(this.getLatLngs())})}}),o.Polygon.include({toGeoJSON:function(){var t,e,i,n=[o.GeoJSON.latLngsToCoords(this.getLatLngs())];if(n[0].push(n[0][0]),this._holes)for(t=0,e=this._holes.length;e>t;t++)i=o.GeoJSON.latLngsToCoords(this._holes[t]),i.push(i[0]),n.push(i);return o.GeoJSON.getFeature(this,{type:"Polygon",coordinates:n})}}),function(){function t(t){return function(){var e=[];return this.eachLayer(function(t){e.push(t.toGeoJSON().geometry.coordinates)}),o.GeoJSON.getFeature(this,{type:t,coordinates:e})}}o.MultiPolyline.include({toGeoJSON:t("MultiLineString")}),o.MultiPolygon.include({toGeoJSON:t("MultiPolygon")}),o.LayerGroup.include({toGeoJSON:function(){var e,i=this.feature&&this.feature.geometry,n=[];if(i&&"MultiPoint"===i.type)return t("MultiPoint").call(this);var s=i&&"GeometryCollection"===i.type;return this.eachLayer(function(t){t.toGeoJSON&&(e=t.toGeoJSON(),n.push(s?e.geometry:o.GeoJSON.asFeature(e)))}),s?o.GeoJSON.getFeature(this,{geometries:n,type:"GeometryCollection"}):{type:"FeatureCollection",features:n}}})}(),o.geoJson=function(t,e){return new o.GeoJSON(t,e)},o.DomEvent={addListener:function(t,e,i,n){var s,a,r,h=o.stamp(i),l="_leaflet_"+e+h;return t[l]?this:(s=function(e){return i.call(n||t,e||o.DomEvent._getEvent())},o.Browser.pointer&&0===e.indexOf("touch")?this.addPointerListener(t,e,s,h):(o.Browser.touch&&"dblclick"===e&&this.addDoubleTapListener&&this.addDoubleTapListener(t,s,h),"addEventListener"in t?"mousewheel"===e?(t.addEventListener("DOMMouseScroll",s,!1),t.addEventListener(e,s,!1)):"mouseenter"===e||"mouseleave"===e?(a=s,r="mouseenter"===e?"mouseover":"mouseout",s=function(e){return o.DomEvent._checkMouse(t,e)?a(e):void 0},t.addEventListener(r,s,!1)):"click"===e&&o.Browser.android?(a=s,s=function(t){return o.DomEvent._filterClick(t,a)},t.addEventListener(e,s,!1)):t.addEventListener(e,s,!1):"attachEvent"in t&&t.attachEvent("on"+e,s),t[l]=s,this))},removeListener:function(t,e,i){var n=o.stamp(i),s="_leaflet_"+e+n,a=t[s];return a?(o.Browser.pointer&&0===e.indexOf("touch")?this.removePointerListener(t,e,n):o.Browser.touch&&"dblclick"===e&&this.removeDoubleTapListener?this.removeDoubleTapListener(t,n):"removeEventListener"in t?"mousewheel"===e?(t.removeEventListener("DOMMouseScroll",a,!1),t.removeEventListener(e,a,!1)):"mouseenter"===e||"mouseleave"===e?t.removeEventListener("mouseenter"===e?"mouseover":"mouseout",a,!1):t.removeEventListener(e,a,!1):"detachEvent"in t&&t.detachEvent("on"+e,a),t[s]=null,this):this},stopPropagation:function(t){return t.stopPropagation?t.stopPropagation():t.cancelBubble=!0,o.DomEvent._skipped(t),this},disableScrollPropagation:function(t){var e=o.DomEvent.stopPropagation;return o.DomEvent.on(t,"mousewheel",e).on(t,"MozMousePixelScroll",e)},disableClickPropagation:function(t){for(var e=o.DomEvent.stopPropagation,i=o.Draggable.START.length-1;i>=0;i--)o.DomEvent.on(t,o.Draggable.START[i],e);return o.DomEvent.on(t,"click",o.DomEvent._fakeStop).on(t,"dblclick",e)},preventDefault:function(t){return t.preventDefault?t.preventDefault():t.returnValue=!1,this},stop:function(t){return o.DomEvent.preventDefault(t).stopPropagation(t)},getMousePosition:function(t,e){if(!e)return new o.Point(t.clientX,t.clientY);var i=e.getBoundingClientRect();return new o.Point(t.clientX-i.left-e.clientLeft,t.clientY-i.top-e.clientTop)},getWheelDelta:function(t){var e=0;return t.wheelDelta&&(e=t.wheelDelta/120),t.detail&&(e=-t.detail/3),e},_skipEvents:{},_fakeStop:function(t){o.DomEvent._skipEvents[t.type]=!0},_skipped:function(t){var e=this._skipEvents[t.type];return this._skipEvents[t.type]=!1,e},_checkMouse:function(t,e){var i=e.relatedTarget;if(!i)return!0;try{for(;i&&i!==t;)i=i.parentNode}catch(n){return!1}return i!==t},_getEvent:function(){var e=t.event;if(!e)for(var i=arguments.callee.caller;i&&(e=i.arguments[0],!e||t.Event!==e.constructor);)i=i.caller;return e},_filterClick:function(t,e){var i=t.timeStamp||t.originalEvent.timeStamp,n=o.DomEvent._lastClick&&i-o.DomEvent._lastClick;return n&&n>100&&1e3>n||t.target._simulatedClick&&!t._simulated?void o.DomEvent.stop(t):(o.DomEvent._lastClick=i,e(t))}},o.DomEvent.on=o.DomEvent.addListener,o.DomEvent.off=o.DomEvent.removeListener,o.Draggable=o.Class.extend({includes:o.Mixin.Events,statics:{START:o.Browser.touch?["touchstart","mousedown"]:["mousedown"],END:{mousedown:"mouseup",touchstart:"touchend",pointerdown:"touchend",MSPointerDown:"touchend"},MOVE:{mousedown:"mousemove",touchstart:"touchmove",pointerdown:"touchmove",MSPointerDown:"touchmove"}},initialize:function(t,e){this._element=t,this._dragStartTarget=e||t},enable:function(){if(!this._enabled){for(var t=o.Draggable.START.length-1;t>=0;t--)o.DomEvent.on(this._dragStartTarget,o.Draggable.START[t],this._onDown,this);this._enabled=!0}},disable:function(){if(this._enabled){for(var t=o.Draggable.START.length-1;t>=0;t--)o.DomEvent.off(this._dragStartTarget,o.Draggable.START[t],this._onDown,this);this._enabled=!1,this._moved=!1}},_onDown:function(t){if(this._moved=!1,!(t.shiftKey||1!==t.which&&1!==t.button&&!t.touches||(o.DomEvent.stopPropagation(t),o.Draggable._disabled||(o.DomUtil.disableImageDrag(),o.DomUtil.disableTextSelection(),this._moving)))){var i=t.touches?t.touches[0]:t;this._startPoint=new o.Point(i.clientX,i.clientY),this._startPos=this._newPos=o.DomUtil.getPosition(this._element),o.DomEvent.on(e,o.Draggable.MOVE[t.type],this._onMove,this).on(e,o.Draggable.END[t.type],this._onUp,this)}},_onMove:function(t){if(t.touches&&t.touches.length>1)return void(this._moved=!0);var i=t.touches&&1===t.touches.length?t.touches[0]:t,n=new o.Point(i.clientX,i.clientY),s=n.subtract(this._startPoint);(s.x||s.y)&&(o.DomEvent.preventDefault(t),this._moved||(this.fire("dragstart"),this._moved=!0,this._startPos=o.DomUtil.getPosition(this._element).subtract(s),o.DomUtil.addClass(e.body,"leaflet-dragging"),o.DomUtil.addClass(t.target||t.srcElement,"leaflet-drag-target")),this._newPos=this._startPos.add(s),this._moving=!0,o.Util.cancelAnimFrame(this._animRequest),this._animRequest=o.Util.requestAnimFrame(this._updatePosition,this,!0,this._dragStartTarget))},_updatePosition:function(){this.fire("predrag"),o.DomUtil.setPosition(this._element,this._newPos),this.fire("drag")},_onUp:function(t){o.DomUtil.removeClass(e.body,"leaflet-dragging"),o.DomUtil.removeClass(t.target||t.srcElement,"leaflet-drag-target");for(var i in o.Draggable.MOVE)o.DomEvent.off(e,o.Draggable.MOVE[i],this._onMove).off(e,o.Draggable.END[i],this._onUp);o.DomUtil.enableImageDrag(),o.DomUtil.enableTextSelection(),this._moved&&this._moving&&(o.Util.cancelAnimFrame(this._animRequest),this.fire("dragend",{distance:this._newPos.distanceTo(this._startPos)})),this._moving=!1}}),o.Handler=o.Class.extend({initialize:function(t){this._map=t},enable:function(){this._enabled||(this._enabled=!0,this.addHooks())},disable:function(){this._enabled&&(this._enabled=!1,this.removeHooks())},enabled:function(){return!!this._enabled}}),o.Map.mergeOptions({dragging:!0,inertia:!o.Browser.android23,inertiaDeceleration:3400,inertiaMaxSpeed:1/0,inertiaThreshold:o.Browser.touch?32:18,easeLinearity:.25,worldCopyJump:!1}),o.Map.Drag=o.Handler.extend({addHooks:function(){if(!this._draggable){var t=this._map;this._draggable=new o.Draggable(t._mapPane,t._container),this._draggable.on({dragstart:this._onDragStart,drag:this._onDrag,dragend:this._onDragEnd},this),t.options.worldCopyJump&&(this._draggable.on("predrag",this._onPreDrag,this),t.on("viewreset",this._onViewReset,this),t.whenReady(this._onViewReset,this))}this._draggable.enable()},removeHooks:function(){this._draggable.disable()},moved:function(){return this._draggable&&this._draggable._moved},_onDragStart:function(){var t=this._map;t._panAnim&&t._panAnim.stop(),t.fire("movestart").fire("dragstart"),t.options.inertia&&(this._positions=[],this._times=[])},_onDrag:function(){if(this._map.options.inertia){var t=this._lastTime=+new Date,e=this._lastPos=this._draggable._newPos;this._positions.push(e),this._times.push(t),t-this._times[0]>200&&(this._positions.shift(),this._times.shift())}this._map.fire("move").fire("drag")},_onViewReset:function(){var t=this._map.getSize()._divideBy(2),e=this._map.latLngToLayerPoint([0,0]);this._initialWorldOffset=e.subtract(t).x,this._worldWidth=this._map.project([0,180]).x},_onPreDrag:function(){var t=this._worldWidth,e=Math.round(t/2),i=this._initialWorldOffset,n=this._draggable._newPos.x,o=(n-e+i)%t+e-i,s=(n+e+i)%t-e-i,a=Math.abs(o+i)<Math.abs(s+i)?o:s;this._draggable._newPos.x=a},_onDragEnd:function(t){var e=this._map,i=e.options,n=+new Date-this._lastTime,s=!i.inertia||n>i.inertiaThreshold||!this._positions[0];if(e.fire("dragend",t),s)e.fire("moveend");else{var a=this._lastPos.subtract(this._positions[0]),r=(this._lastTime+n-this._times[0])/1e3,h=i.easeLinearity,l=a.multiplyBy(h/r),u=l.distanceTo([0,0]),c=Math.min(i.inertiaMaxSpeed,u),d=l.multiplyBy(c/u),p=c/(i.inertiaDeceleration*h),_=d.multiplyBy(-p/2).round();_.x&&_.y?(_=e._limitOffset(_,e.options.maxBounds),o.Util.requestAnimFrame(function(){e.panBy(_,{duration:p,easeLinearity:h,noMoveStart:!0})})):e.fire("moveend")}}}),o.Map.addInitHook("addHandler","dragging",o.Map.Drag),o.Map.mergeOptions({doubleClickZoom:!0}),o.Map.DoubleClickZoom=o.Handler.extend({addHooks:function(){this._map.on("dblclick",this._onDoubleClick,this)},removeHooks:function(){this._map.off("dblclick",this._onDoubleClick,this)},_onDoubleClick:function(t){var e=this._map,i=e.getZoom()+(t.originalEvent.shiftKey?-1:1);"center"===e.options.doubleClickZoom?e.setZoom(i):e.setZoomAround(t.containerPoint,i)}}),o.Map.addInitHook("addHandler","doubleClickZoom",o.Map.DoubleClickZoom),o.Map.mergeOptions({scrollWheelZoom:!0}),o.Map.ScrollWheelZoom=o.Handler.extend({addHooks:function(){o.DomEvent.on(this._map._container,"mousewheel",this._onWheelScroll,this),o.DomEvent.on(this._map._container,"MozMousePixelScroll",o.DomEvent.preventDefault),this._delta=0},removeHooks:function(){o.DomEvent.off(this._map._container,"mousewheel",this._onWheelScroll),o.DomEvent.off(this._map._container,"MozMousePixelScroll",o.DomEvent.preventDefault)},_onWheelScroll:function(t){var e=o.DomEvent.getWheelDelta(t);this._delta+=e,this._lastMousePos=this._map.mouseEventToContainerPoint(t),this._startTime||(this._startTime=+new Date);var i=Math.max(40-(+new Date-this._startTime),0);clearTimeout(this._timer),this._timer=setTimeout(o.bind(this._performZoom,this),i),o.DomEvent.preventDefault(t),o.DomEvent.stopPropagation(t)},_performZoom:function(){var t=this._map,e=this._delta,i=t.getZoom();e=e>0?Math.ceil(e):Math.floor(e),e=Math.max(Math.min(e,4),-4),e=t._limitZoom(i+e)-i,this._delta=0,this._startTime=null,e&&("center"===t.options.scrollWheelZoom?t.setZoom(i+e):t.setZoomAround(this._lastMousePos,i+e))}}),o.Map.addInitHook("addHandler","scrollWheelZoom",o.Map.ScrollWheelZoom),o.extend(o.DomEvent,{_touchstart:o.Browser.msPointer?"MSPointerDown":o.Browser.pointer?"pointerdown":"touchstart",_touchend:o.Browser.msPointer?"MSPointerUp":o.Browser.pointer?"pointerup":"touchend",addDoubleTapListener:function(t,i,n){function s(t){var e;if(o.Browser.pointer?(_.push(t.pointerId),e=_.length):e=t.touches.length,!(e>1)){var i=Date.now(),n=i-(r||i);h=t.touches?t.touches[0]:t,l=n>0&&u>=n,r=i}}function a(t){if(o.Browser.pointer){var e=_.indexOf(t.pointerId);if(-1===e)return;_.splice(e,1)}if(l){if(o.Browser.pointer){var n,s={};for(var a in h)n=h[a],s[a]="function"==typeof n?n.bind(h):n;h=s}h.type="dblclick",i(h),r=null}}var r,h,l=!1,u=250,c="_leaflet_",d=this._touchstart,p=this._touchend,_=[];t[c+d+n]=s,t[c+p+n]=a;var m=o.Browser.pointer?e.documentElement:t;return t.addEventListener(d,s,!1),m.addEventListener(p,a,!1),o.Browser.pointer&&m.addEventListener(o.DomEvent.POINTER_CANCEL,a,!1),this},removeDoubleTapListener:function(t,i){var n="_leaflet_";return t.removeEventListener(this._touchstart,t[n+this._touchstart+i],!1),(o.Browser.pointer?e.documentElement:t).removeEventListener(this._touchend,t[n+this._touchend+i],!1),o.Browser.pointer&&e.documentElement.removeEventListener(o.DomEvent.POINTER_CANCEL,t[n+this._touchend+i],!1),this}}),o.extend(o.DomEvent,{POINTER_DOWN:o.Browser.msPointer?"MSPointerDown":"pointerdown",POINTER_MOVE:o.Browser.msPointer?"MSPointerMove":"pointermove",POINTER_UP:o.Browser.msPointer?"MSPointerUp":"pointerup",POINTER_CANCEL:o.Browser.msPointer?"MSPointerCancel":"pointercancel",_pointers:[],_pointerDocumentListener:!1,addPointerListener:function(t,e,i,n){switch(e){case"touchstart":return this.addPointerListenerStart(t,e,i,n);case"touchend":return this.addPointerListenerEnd(t,e,i,n);case"touchmove":return this.addPointerListenerMove(t,e,i,n);default:throw"Unknown touch event type"}},addPointerListenerStart:function(t,i,n,s){var a="_leaflet_",r=this._pointers,h=function(t){o.DomEvent.preventDefault(t);for(var e=!1,i=0;i<r.length;i++)if(r[i].pointerId===t.pointerId){e=!0;break}e||r.push(t),t.touches=r.slice(),t.changedTouches=[t],n(t)};if(t[a+"touchstart"+s]=h,t.addEventListener(this.POINTER_DOWN,h,!1),!this._pointerDocumentListener){var l=function(t){for(var e=0;e<r.length;e++)if(r[e].pointerId===t.pointerId){r.splice(e,1);
break}};e.documentElement.addEventListener(this.POINTER_UP,l,!1),e.documentElement.addEventListener(this.POINTER_CANCEL,l,!1),this._pointerDocumentListener=!0}return this},addPointerListenerMove:function(t,e,i,n){function o(t){if(t.pointerType!==t.MSPOINTER_TYPE_MOUSE&&"mouse"!==t.pointerType||0!==t.buttons){for(var e=0;e<a.length;e++)if(a[e].pointerId===t.pointerId){a[e]=t;break}t.touches=a.slice(),t.changedTouches=[t],i(t)}}var s="_leaflet_",a=this._pointers;return t[s+"touchmove"+n]=o,t.addEventListener(this.POINTER_MOVE,o,!1),this},addPointerListenerEnd:function(t,e,i,n){var o="_leaflet_",s=this._pointers,a=function(t){for(var e=0;e<s.length;e++)if(s[e].pointerId===t.pointerId){s.splice(e,1);break}t.touches=s.slice(),t.changedTouches=[t],i(t)};return t[o+"touchend"+n]=a,t.addEventListener(this.POINTER_UP,a,!1),t.addEventListener(this.POINTER_CANCEL,a,!1),this},removePointerListener:function(t,e,i){var n="_leaflet_",o=t[n+e+i];switch(e){case"touchstart":t.removeEventListener(this.POINTER_DOWN,o,!1);break;case"touchmove":t.removeEventListener(this.POINTER_MOVE,o,!1);break;case"touchend":t.removeEventListener(this.POINTER_UP,o,!1),t.removeEventListener(this.POINTER_CANCEL,o,!1)}return this}}),o.Map.mergeOptions({touchZoom:o.Browser.touch&&!o.Browser.android23,bounceAtZoomLimits:!0}),o.Map.TouchZoom=o.Handler.extend({addHooks:function(){o.DomEvent.on(this._map._container,"touchstart",this._onTouchStart,this)},removeHooks:function(){o.DomEvent.off(this._map._container,"touchstart",this._onTouchStart,this)},_onTouchStart:function(t){var i=this._map;if(t.touches&&2===t.touches.length&&!i._animatingZoom&&!this._zooming){var n=i.mouseEventToLayerPoint(t.touches[0]),s=i.mouseEventToLayerPoint(t.touches[1]),a=i._getCenterLayerPoint();this._startCenter=n.add(s)._divideBy(2),this._startDist=n.distanceTo(s),this._moved=!1,this._zooming=!0,this._centerOffset=a.subtract(this._startCenter),i._panAnim&&i._panAnim.stop(),o.DomEvent.on(e,"touchmove",this._onTouchMove,this).on(e,"touchend",this._onTouchEnd,this),o.DomEvent.preventDefault(t)}},_onTouchMove:function(t){var e=this._map;if(t.touches&&2===t.touches.length&&this._zooming){var i=e.mouseEventToLayerPoint(t.touches[0]),n=e.mouseEventToLayerPoint(t.touches[1]);this._scale=i.distanceTo(n)/this._startDist,this._delta=i._add(n)._divideBy(2)._subtract(this._startCenter),1!==this._scale&&(e.options.bounceAtZoomLimits||!(e.getZoom()===e.getMinZoom()&&this._scale<1||e.getZoom()===e.getMaxZoom()&&this._scale>1))&&(this._moved||(o.DomUtil.addClass(e._mapPane,"leaflet-touching"),e.fire("movestart").fire("zoomstart"),this._moved=!0),o.Util.cancelAnimFrame(this._animRequest),this._animRequest=o.Util.requestAnimFrame(this._updateOnMove,this,!0,this._map._container),o.DomEvent.preventDefault(t))}},_updateOnMove:function(){var t=this._map,e=this._getScaleOrigin(),i=t.layerPointToLatLng(e),n=t.getScaleZoom(this._scale);t._animateZoom(i,n,this._startCenter,this._scale,this._delta)},_onTouchEnd:function(){if(!this._moved||!this._zooming)return void(this._zooming=!1);var t=this._map;this._zooming=!1,o.DomUtil.removeClass(t._mapPane,"leaflet-touching"),o.Util.cancelAnimFrame(this._animRequest),o.DomEvent.off(e,"touchmove",this._onTouchMove).off(e,"touchend",this._onTouchEnd);var i=this._getScaleOrigin(),n=t.layerPointToLatLng(i),s=t.getZoom(),a=t.getScaleZoom(this._scale)-s,r=a>0?Math.ceil(a):Math.floor(a),h=t._limitZoom(s+r),l=t.getZoomScale(h)/this._scale;t._animateZoom(n,h,i,l)},_getScaleOrigin:function(){var t=this._centerOffset.subtract(this._delta).divideBy(this._scale);return this._startCenter.add(t)}}),o.Map.addInitHook("addHandler","touchZoom",o.Map.TouchZoom),o.Map.mergeOptions({tap:!0,tapTolerance:15}),o.Map.Tap=o.Handler.extend({addHooks:function(){o.DomEvent.on(this._map._container,"touchstart",this._onDown,this)},removeHooks:function(){o.DomEvent.off(this._map._container,"touchstart",this._onDown,this)},_onDown:function(t){if(t.touches){if(o.DomEvent.preventDefault(t),this._fireClick=!0,t.touches.length>1)return this._fireClick=!1,void clearTimeout(this._holdTimeout);var i=t.touches[0],n=i.target;this._startPos=this._newPos=new o.Point(i.clientX,i.clientY),n.tagName&&"a"===n.tagName.toLowerCase()&&o.DomUtil.addClass(n,"leaflet-active"),this._holdTimeout=setTimeout(o.bind(function(){this._isTapValid()&&(this._fireClick=!1,this._onUp(),this._simulateEvent("contextmenu",i))},this),1e3),o.DomEvent.on(e,"touchmove",this._onMove,this).on(e,"touchend",this._onUp,this)}},_onUp:function(t){if(clearTimeout(this._holdTimeout),o.DomEvent.off(e,"touchmove",this._onMove,this).off(e,"touchend",this._onUp,this),this._fireClick&&t&&t.changedTouches){var i=t.changedTouches[0],n=i.target;n&&n.tagName&&"a"===n.tagName.toLowerCase()&&o.DomUtil.removeClass(n,"leaflet-active"),this._isTapValid()&&this._simulateEvent("click",i)}},_isTapValid:function(){return this._newPos.distanceTo(this._startPos)<=this._map.options.tapTolerance},_onMove:function(t){var e=t.touches[0];this._newPos=new o.Point(e.clientX,e.clientY)},_simulateEvent:function(i,n){var o=e.createEvent("MouseEvents");o._simulated=!0,n.target._simulatedClick=!0,o.initMouseEvent(i,!0,!0,t,1,n.screenX,n.screenY,n.clientX,n.clientY,!1,!1,!1,!1,0,null),n.target.dispatchEvent(o)}}),o.Browser.touch&&!o.Browser.pointer&&o.Map.addInitHook("addHandler","tap",o.Map.Tap),o.Map.mergeOptions({boxZoom:!0}),o.Map.BoxZoom=o.Handler.extend({initialize:function(t){this._map=t,this._container=t._container,this._pane=t._panes.overlayPane,this._moved=!1},addHooks:function(){o.DomEvent.on(this._container,"mousedown",this._onMouseDown,this)},removeHooks:function(){o.DomEvent.off(this._container,"mousedown",this._onMouseDown),this._moved=!1},moved:function(){return this._moved},_onMouseDown:function(t){return this._moved=!1,!t.shiftKey||1!==t.which&&1!==t.button?!1:(o.DomUtil.disableTextSelection(),o.DomUtil.disableImageDrag(),this._startLayerPoint=this._map.mouseEventToLayerPoint(t),void o.DomEvent.on(e,"mousemove",this._onMouseMove,this).on(e,"mouseup",this._onMouseUp,this).on(e,"keydown",this._onKeyDown,this))},_onMouseMove:function(t){this._moved||(this._box=o.DomUtil.create("div","leaflet-zoom-box",this._pane),o.DomUtil.setPosition(this._box,this._startLayerPoint),this._container.style.cursor="crosshair",this._map.fire("boxzoomstart"));var e=this._startLayerPoint,i=this._box,n=this._map.mouseEventToLayerPoint(t),s=n.subtract(e),a=new o.Point(Math.min(n.x,e.x),Math.min(n.y,e.y));o.DomUtil.setPosition(i,a),this._moved=!0,i.style.width=Math.max(0,Math.abs(s.x)-4)+"px",i.style.height=Math.max(0,Math.abs(s.y)-4)+"px"},_finish:function(){this._moved&&(this._pane.removeChild(this._box),this._container.style.cursor=""),o.DomUtil.enableTextSelection(),o.DomUtil.enableImageDrag(),o.DomEvent.off(e,"mousemove",this._onMouseMove).off(e,"mouseup",this._onMouseUp).off(e,"keydown",this._onKeyDown)},_onMouseUp:function(t){this._finish();var e=this._map,i=e.mouseEventToLayerPoint(t);if(!this._startLayerPoint.equals(i)){var n=new o.LatLngBounds(e.layerPointToLatLng(this._startLayerPoint),e.layerPointToLatLng(i));e.fitBounds(n),e.fire("boxzoomend",{boxZoomBounds:n})}},_onKeyDown:function(t){27===t.keyCode&&this._finish()}}),o.Map.addInitHook("addHandler","boxZoom",o.Map.BoxZoom),o.Map.mergeOptions({keyboard:!0,keyboardPanOffset:80,keyboardZoomOffset:1}),o.Map.Keyboard=o.Handler.extend({keyCodes:{left:[37],right:[39],down:[40],up:[38],zoomIn:[187,107,61,171],zoomOut:[189,109,173]},initialize:function(t){this._map=t,this._setPanOffset(t.options.keyboardPanOffset),this._setZoomOffset(t.options.keyboardZoomOffset)},addHooks:function(){var t=this._map._container;-1===t.tabIndex&&(t.tabIndex="0"),o.DomEvent.on(t,"focus",this._onFocus,this).on(t,"blur",this._onBlur,this).on(t,"mousedown",this._onMouseDown,this),this._map.on("focus",this._addHooks,this).on("blur",this._removeHooks,this)},removeHooks:function(){this._removeHooks();var t=this._map._container;o.DomEvent.off(t,"focus",this._onFocus,this).off(t,"blur",this._onBlur,this).off(t,"mousedown",this._onMouseDown,this),this._map.off("focus",this._addHooks,this).off("blur",this._removeHooks,this)},_onMouseDown:function(){if(!this._focused){var i=e.body,n=e.documentElement,o=i.scrollTop||n.scrollTop,s=i.scrollLeft||n.scrollLeft;this._map._container.focus(),t.scrollTo(s,o)}},_onFocus:function(){this._focused=!0,this._map.fire("focus")},_onBlur:function(){this._focused=!1,this._map.fire("blur")},_setPanOffset:function(t){var e,i,n=this._panKeys={},o=this.keyCodes;for(e=0,i=o.left.length;i>e;e++)n[o.left[e]]=[-1*t,0];for(e=0,i=o.right.length;i>e;e++)n[o.right[e]]=[t,0];for(e=0,i=o.down.length;i>e;e++)n[o.down[e]]=[0,t];for(e=0,i=o.up.length;i>e;e++)n[o.up[e]]=[0,-1*t]},_setZoomOffset:function(t){var e,i,n=this._zoomKeys={},o=this.keyCodes;for(e=0,i=o.zoomIn.length;i>e;e++)n[o.zoomIn[e]]=t;for(e=0,i=o.zoomOut.length;i>e;e++)n[o.zoomOut[e]]=-t},_addHooks:function(){o.DomEvent.on(e,"keydown",this._onKeyDown,this)},_removeHooks:function(){o.DomEvent.off(e,"keydown",this._onKeyDown,this)},_onKeyDown:function(t){var e=t.keyCode,i=this._map;if(e in this._panKeys){if(i._panAnim&&i._panAnim._inProgress)return;i.panBy(this._panKeys[e]),i.options.maxBounds&&i.panInsideBounds(i.options.maxBounds)}else{if(!(e in this._zoomKeys))return;i.setZoom(i.getZoom()+this._zoomKeys[e])}o.DomEvent.stop(t)}}),o.Map.addInitHook("addHandler","keyboard",o.Map.Keyboard),o.Handler.MarkerDrag=o.Handler.extend({initialize:function(t){this._marker=t},addHooks:function(){var t=this._marker._icon;this._draggable||(this._draggable=new o.Draggable(t,t)),this._draggable.on("dragstart",this._onDragStart,this).on("drag",this._onDrag,this).on("dragend",this._onDragEnd,this),this._draggable.enable(),o.DomUtil.addClass(this._marker._icon,"leaflet-marker-draggable")},removeHooks:function(){this._draggable.off("dragstart",this._onDragStart,this).off("drag",this._onDrag,this).off("dragend",this._onDragEnd,this),this._draggable.disable(),o.DomUtil.removeClass(this._marker._icon,"leaflet-marker-draggable")},moved:function(){return this._draggable&&this._draggable._moved},_onDragStart:function(){this._marker.closePopup().fire("movestart").fire("dragstart")},_onDrag:function(){var t=this._marker,e=t._shadow,i=o.DomUtil.getPosition(t._icon),n=t._map.layerPointToLatLng(i);e&&o.DomUtil.setPosition(e,i),t._latlng=n,t.fire("move",{latlng:n}).fire("drag")},_onDragEnd:function(t){this._marker.fire("moveend").fire("dragend",t)}}),o.Control=o.Class.extend({options:{position:"topright"},initialize:function(t){o.setOptions(this,t)},getPosition:function(){return this.options.position},setPosition:function(t){var e=this._map;return e&&e.removeControl(this),this.options.position=t,e&&e.addControl(this),this},getContainer:function(){return this._container},addTo:function(t){this._map=t;var e=this._container=this.onAdd(t),i=this.getPosition(),n=t._controlCorners[i];return o.DomUtil.addClass(e,"leaflet-control"),-1!==i.indexOf("bottom")?n.insertBefore(e,n.firstChild):n.appendChild(e),this},removeFrom:function(t){var e=this.getPosition(),i=t._controlCorners[e];return i.removeChild(this._container),this._map=null,this.onRemove&&this.onRemove(t),this},_refocusOnMap:function(){this._map&&this._map.getContainer().focus()}}),o.control=function(t){return new o.Control(t)},o.Map.include({addControl:function(t){return t.addTo(this),this},removeControl:function(t){return t.removeFrom(this),this},_initControlPos:function(){function t(t,s){var a=i+t+" "+i+s;e[t+s]=o.DomUtil.create("div",a,n)}var e=this._controlCorners={},i="leaflet-",n=this._controlContainer=o.DomUtil.create("div",i+"control-container",this._container);t("top","left"),t("top","right"),t("bottom","left"),t("bottom","right")},_clearControlPos:function(){this._container.removeChild(this._controlContainer)}}),o.Control.Zoom=o.Control.extend({options:{position:"topleft",zoomInText:"+",zoomInTitle:"Zoom in",zoomOutText:"-",zoomOutTitle:"Zoom out"},onAdd:function(t){var e="leaflet-control-zoom",i=o.DomUtil.create("div",e+" leaflet-bar");return this._map=t,this._zoomInButton=this._createButton(this.options.zoomInText,this.options.zoomInTitle,e+"-in",i,this._zoomIn,this),this._zoomOutButton=this._createButton(this.options.zoomOutText,this.options.zoomOutTitle,e+"-out",i,this._zoomOut,this),this._updateDisabled(),t.on("zoomend zoomlevelschange",this._updateDisabled,this),i},onRemove:function(t){t.off("zoomend zoomlevelschange",this._updateDisabled,this)},_zoomIn:function(t){this._map.zoomIn(t.shiftKey?3:1)},_zoomOut:function(t){this._map.zoomOut(t.shiftKey?3:1)},_createButton:function(t,e,i,n,s,a){var r=o.DomUtil.create("a",i,n);r.innerHTML=t,r.href="#",r.title=e;var h=o.DomEvent.stopPropagation;return o.DomEvent.on(r,"click",h).on(r,"mousedown",h).on(r,"dblclick",h).on(r,"click",o.DomEvent.preventDefault).on(r,"click",s,a).on(r,"click",this._refocusOnMap,a),r},_updateDisabled:function(){var t=this._map,e="leaflet-disabled";o.DomUtil.removeClass(this._zoomInButton,e),o.DomUtil.removeClass(this._zoomOutButton,e),t._zoom===t.getMinZoom()&&o.DomUtil.addClass(this._zoomOutButton,e),t._zoom===t.getMaxZoom()&&o.DomUtil.addClass(this._zoomInButton,e)}}),o.Map.mergeOptions({zoomControl:!0}),o.Map.addInitHook(function(){this.options.zoomControl&&(this.zoomControl=new o.Control.Zoom,this.addControl(this.zoomControl))}),o.control.zoom=function(t){return new o.Control.Zoom(t)},o.Control.Attribution=o.Control.extend({options:{position:"bottomright",prefix:'<a href="http://leafletjs.com" title="A JS library for interactive maps">Leaflet</a>'},initialize:function(t){o.setOptions(this,t),this._attributions={}},onAdd:function(t){this._container=o.DomUtil.create("div","leaflet-control-attribution"),o.DomEvent.disableClickPropagation(this._container);for(var e in t._layers)t._layers[e].getAttribution&&this.addAttribution(t._layers[e].getAttribution());return t.on("layeradd",this._onLayerAdd,this).on("layerremove",this._onLayerRemove,this),this._update(),this._container},onRemove:function(t){t.off("layeradd",this._onLayerAdd).off("layerremove",this._onLayerRemove)},setPrefix:function(t){return this.options.prefix=t,this._update(),this},addAttribution:function(t){return t?(this._attributions[t]||(this._attributions[t]=0),this._attributions[t]++,this._update(),this):void 0},removeAttribution:function(t){return t?(this._attributions[t]&&(this._attributions[t]--,this._update()),this):void 0},_update:function(){if(this._map){var t=[];for(var e in this._attributions)this._attributions[e]&&t.push(e);var i=[];this.options.prefix&&i.push(this.options.prefix),t.length&&i.push(t.join(", ")),this._container.innerHTML=i.join(" | ")}},_onLayerAdd:function(t){t.layer.getAttribution&&this.addAttribution(t.layer.getAttribution())},_onLayerRemove:function(t){t.layer.getAttribution&&this.removeAttribution(t.layer.getAttribution())}}),o.Map.mergeOptions({attributionControl:!0}),o.Map.addInitHook(function(){this.options.attributionControl&&(this.attributionControl=(new o.Control.Attribution).addTo(this))}),o.control.attribution=function(t){return new o.Control.Attribution(t)},o.Control.Scale=o.Control.extend({options:{position:"bottomleft",maxWidth:100,metric:!0,imperial:!0,updateWhenIdle:!1},onAdd:function(t){this._map=t;var e="leaflet-control-scale",i=o.DomUtil.create("div",e),n=this.options;return this._addScales(n,e,i),t.on(n.updateWhenIdle?"moveend":"move",this._update,this),t.whenReady(this._update,this),i},onRemove:function(t){t.off(this.options.updateWhenIdle?"moveend":"move",this._update,this)},_addScales:function(t,e,i){t.metric&&(this._mScale=o.DomUtil.create("div",e+"-line",i)),t.imperial&&(this._iScale=o.DomUtil.create("div",e+"-line",i))},_update:function(){var t=this._map.getBounds(),e=t.getCenter().lat,i=6378137*Math.PI*Math.cos(e*Math.PI/180),n=i*(t.getNorthEast().lng-t.getSouthWest().lng)/180,o=this._map.getSize(),s=this.options,a=0;o.x>0&&(a=n*(s.maxWidth/o.x)),this._updateScales(s,a)},_updateScales:function(t,e){t.metric&&e&&this._updateMetric(e),t.imperial&&e&&this._updateImperial(e)},_updateMetric:function(t){var e=this._getRoundNum(t);this._mScale.style.width=this._getScaleWidth(e/t)+"px",this._mScale.innerHTML=1e3>e?e+" m":e/1e3+" km"},_updateImperial:function(t){var e,i,n,o=3.2808399*t,s=this._iScale;o>5280?(e=o/5280,i=this._getRoundNum(e),s.style.width=this._getScaleWidth(i/e)+"px",s.innerHTML=i+" mi"):(n=this._getRoundNum(o),s.style.width=this._getScaleWidth(n/o)+"px",s.innerHTML=n+" ft")},_getScaleWidth:function(t){return Math.round(this.options.maxWidth*t)-10},_getRoundNum:function(t){var e=Math.pow(10,(Math.floor(t)+"").length-1),i=t/e;return i=i>=10?10:i>=5?5:i>=3?3:i>=2?2:1,e*i}}),o.control.scale=function(t){return new o.Control.Scale(t)},o.Control.Layers=o.Control.extend({options:{collapsed:!0,position:"topright",autoZIndex:!0},initialize:function(t,e,i){o.setOptions(this,i),this._layers={},this._lastZIndex=0,this._handlingClick=!1;for(var n in t)this._addLayer(t[n],n);for(n in e)this._addLayer(e[n],n,!0)},onAdd:function(t){return this._initLayout(),this._update(),t.on("layeradd",this._onLayerChange,this).on("layerremove",this._onLayerChange,this),this._container},onRemove:function(t){t.off("layeradd",this._onLayerChange).off("layerremove",this._onLayerChange)},addBaseLayer:function(t,e){return this._addLayer(t,e),this._update(),this},addOverlay:function(t,e){return this._addLayer(t,e,!0),this._update(),this},removeLayer:function(t){var e=o.stamp(t);return delete this._layers[e],this._update(),this},_initLayout:function(){var t="leaflet-control-layers",e=this._container=o.DomUtil.create("div",t);e.setAttribute("aria-haspopup",!0),o.Browser.touch?o.DomEvent.on(e,"click",o.DomEvent.stopPropagation):o.DomEvent.disableClickPropagation(e).disableScrollPropagation(e);var i=this._form=o.DomUtil.create("form",t+"-list");if(this.options.collapsed){o.Browser.android||o.DomEvent.on(e,"mouseover",this._expand,this).on(e,"mouseout",this._collapse,this);var n=this._layersLink=o.DomUtil.create("a",t+"-toggle",e);n.href="#",n.title="Layers",o.Browser.touch?o.DomEvent.on(n,"click",o.DomEvent.stop).on(n,"click",this._expand,this):o.DomEvent.on(n,"focus",this._expand,this),o.DomEvent.on(i,"click",function(){setTimeout(o.bind(this._onInputClick,this),0)},this),this._map.on("click",this._collapse,this)}else this._expand();this._baseLayersList=o.DomUtil.create("div",t+"-base",i),this._separator=o.DomUtil.create("div",t+"-separator",i),this._overlaysList=o.DomUtil.create("div",t+"-overlays",i),e.appendChild(i)},_addLayer:function(t,e,i){var n=o.stamp(t);this._layers[n]={layer:t,name:e,overlay:i},this.options.autoZIndex&&t.setZIndex&&(this._lastZIndex++,t.setZIndex(this._lastZIndex))},_update:function(){if(this._container){this._baseLayersList.innerHTML="",this._overlaysList.innerHTML="";var t,e,i=!1,n=!1;for(t in this._layers)e=this._layers[t],this._addItem(e),n=n||e.overlay,i=i||!e.overlay;this._separator.style.display=n&&i?"":"none"}},_onLayerChange:function(t){var e=this._layers[o.stamp(t.layer)];if(e){this._handlingClick||this._update();var i=e.overlay?"layeradd"===t.type?"overlayadd":"overlayremove":"layeradd"===t.type?"baselayerchange":null;i&&this._map.fire(i,e)}},_createRadioElement:function(t,i){var n='<input type="radio" class="leaflet-control-layers-selector" name="'+t+'"';i&&(n+=' checked="checked"'),n+="/>";var o=e.createElement("div");return o.innerHTML=n,o.firstChild},_addItem:function(t){var i,n=e.createElement("label"),s=this._map.hasLayer(t.layer);t.overlay?(i=e.createElement("input"),i.type="checkbox",i.className="leaflet-control-layers-selector",i.defaultChecked=s):i=this._createRadioElement("leaflet-base-layers",s),i.layerId=o.stamp(t.layer),o.DomEvent.on(i,"click",this._onInputClick,this);var a=e.createElement("span");a.innerHTML=" "+t.name,n.appendChild(i),n.appendChild(a);var r=t.overlay?this._overlaysList:this._baseLayersList;return r.appendChild(n),n},_onInputClick:function(){var t,e,i,n=this._form.getElementsByTagName("input"),o=n.length;for(this._handlingClick=!0,t=0;o>t;t++)e=n[t],i=this._layers[e.layerId],e.checked&&!this._map.hasLayer(i.layer)?this._map.addLayer(i.layer):!e.checked&&this._map.hasLayer(i.layer)&&this._map.removeLayer(i.layer);this._handlingClick=!1,this._refocusOnMap()},_expand:function(){o.DomUtil.addClass(this._container,"leaflet-control-layers-expanded")},_collapse:function(){this._container.className=this._container.className.replace(" leaflet-control-layers-expanded","")}}),o.control.layers=function(t,e,i){return new o.Control.Layers(t,e,i)},o.PosAnimation=o.Class.extend({includes:o.Mixin.Events,run:function(t,e,i,n){this.stop(),this._el=t,this._inProgress=!0,this._newPos=e,this.fire("start"),t.style[o.DomUtil.TRANSITION]="all "+(i||.25)+"s cubic-bezier(0,0,"+(n||.5)+",1)",o.DomEvent.on(t,o.DomUtil.TRANSITION_END,this._onTransitionEnd,this),o.DomUtil.setPosition(t,e),o.Util.falseFn(t.offsetWidth),this._stepTimer=setInterval(o.bind(this._onStep,this),50)},stop:function(){this._inProgress&&(o.DomUtil.setPosition(this._el,this._getPos()),this._onTransitionEnd(),o.Util.falseFn(this._el.offsetWidth))},_onStep:function(){var t=this._getPos();return t?(this._el._leaflet_pos=t,void this.fire("step")):void this._onTransitionEnd()},_transformRe:/([-+]?(?:\d*\.)?\d+)\D*, ([-+]?(?:\d*\.)?\d+)\D*\)/,_getPos:function(){var e,i,n,s=this._el,a=t.getComputedStyle(s);if(o.Browser.any3d){if(n=a[o.DomUtil.TRANSFORM].match(this._transformRe),!n)return;e=parseFloat(n[1]),i=parseFloat(n[2])}else e=parseFloat(a.left),i=parseFloat(a.top);return new o.Point(e,i,!0)},_onTransitionEnd:function(){o.DomEvent.off(this._el,o.DomUtil.TRANSITION_END,this._onTransitionEnd,this),this._inProgress&&(this._inProgress=!1,this._el.style[o.DomUtil.TRANSITION]="",this._el._leaflet_pos=this._newPos,clearInterval(this._stepTimer),this.fire("step").fire("end"))}}),o.Map.include({setView:function(t,e,n){if(e=e===i?this._zoom:this._limitZoom(e),t=this._limitCenter(o.latLng(t),e,this.options.maxBounds),n=n||{},this._panAnim&&this._panAnim.stop(),this._loaded&&!n.reset&&n!==!0){n.animate!==i&&(n.zoom=o.extend({animate:n.animate},n.zoom),n.pan=o.extend({animate:n.animate},n.pan));var s=this._zoom!==e?this._tryAnimatedZoom&&this._tryAnimatedZoom(t,e,n.zoom):this._tryAnimatedPan(t,n.pan);if(s)return clearTimeout(this._sizeTimer),this}return this._resetView(t,e),this},panBy:function(t,e){if(t=o.point(t).round(),e=e||{},!t.x&&!t.y)return this;if(this._panAnim||(this._panAnim=new o.PosAnimation,this._panAnim.on({step:this._onPanTransitionStep,end:this._onPanTransitionEnd},this)),e.noMoveStart||this.fire("movestart"),e.animate!==!1){o.DomUtil.addClass(this._mapPane,"leaflet-pan-anim");var i=this._getMapPanePos().subtract(t);this._panAnim.run(this._mapPane,i,e.duration||.25,e.easeLinearity)}else this._rawPanBy(t),this.fire("move").fire("moveend");return this},_onPanTransitionStep:function(){this.fire("move")},_onPanTransitionEnd:function(){o.DomUtil.removeClass(this._mapPane,"leaflet-pan-anim"),this.fire("moveend")},_tryAnimatedPan:function(t,e){var i=this._getCenterOffset(t)._floor();return(e&&e.animate)===!0||this.getSize().contains(i)?(this.panBy(i,e),!0):!1}}),o.PosAnimation=o.DomUtil.TRANSITION?o.PosAnimation:o.PosAnimation.extend({run:function(t,e,i,n){this.stop(),this._el=t,this._inProgress=!0,this._duration=i||.25,this._easeOutPower=1/Math.max(n||.5,.2),this._startPos=o.DomUtil.getPosition(t),this._offset=e.subtract(this._startPos),this._startTime=+new Date,this.fire("start"),this._animate()},stop:function(){this._inProgress&&(this._step(),this._complete())},_animate:function(){this._animId=o.Util.requestAnimFrame(this._animate,this),this._step()},_step:function(){var t=+new Date-this._startTime,e=1e3*this._duration;e>t?this._runFrame(this._easeOut(t/e)):(this._runFrame(1),this._complete())},_runFrame:function(t){var e=this._startPos.add(this._offset.multiplyBy(t));o.DomUtil.setPosition(this._el,e),this.fire("step")},_complete:function(){o.Util.cancelAnimFrame(this._animId),this._inProgress=!1,this.fire("end")},_easeOut:function(t){return 1-Math.pow(1-t,this._easeOutPower)}}),o.Map.mergeOptions({zoomAnimation:!0,zoomAnimationThreshold:4}),o.DomUtil.TRANSITION&&o.Map.addInitHook(function(){this._zoomAnimated=this.options.zoomAnimation&&o.DomUtil.TRANSITION&&o.Browser.any3d&&!o.Browser.android23&&!o.Browser.mobileOpera,this._zoomAnimated&&o.DomEvent.on(this._mapPane,o.DomUtil.TRANSITION_END,this._catchTransitionEnd,this)}),o.Map.include(o.DomUtil.TRANSITION?{_catchTransitionEnd:function(t){this._animatingZoom&&t.propertyName.indexOf("transform")>=0&&this._onZoomTransitionEnd()},_nothingToAnimate:function(){return!this._container.getElementsByClassName("leaflet-zoom-animated").length},_tryAnimatedZoom:function(t,e,i){if(this._animatingZoom)return!0;if(i=i||{},!this._zoomAnimated||i.animate===!1||this._nothingToAnimate()||Math.abs(e-this._zoom)>this.options.zoomAnimationThreshold)return!1;var n=this.getZoomScale(e),o=this._getCenterOffset(t)._divideBy(1-1/n),s=this._getCenterLayerPoint()._add(o);return i.animate===!0||this.getSize().contains(o)?(this.fire("movestart").fire("zoomstart"),this._animateZoom(t,e,s,n,null,!0),!0):!1},_animateZoom:function(t,e,i,n,s,a){this._animatingZoom=!0,o.DomUtil.addClass(this._mapPane,"leaflet-zoom-anim"),this._animateToCenter=t,this._animateToZoom=e,o.Draggable&&(o.Draggable._disabled=!0),this.fire("zoomanim",{center:t,zoom:e,origin:i,scale:n,delta:s,backwards:a})},_onZoomTransitionEnd:function(){this._animatingZoom=!1,o.DomUtil.removeClass(this._mapPane,"leaflet-zoom-anim"),this._resetView(this._animateToCenter,this._animateToZoom,!0,!0),o.Draggable&&(o.Draggable._disabled=!1)}}:{}),o.TileLayer.include({_animateZoom:function(t){this._animating||(this._animating=!0,this._prepareBgBuffer());var e=this._bgBuffer,i=o.DomUtil.TRANSFORM,n=t.delta?o.DomUtil.getTranslateString(t.delta):e.style[i],s=o.DomUtil.getScaleString(t.scale,t.origin);e.style[i]=t.backwards?s+" "+n:n+" "+s},_endZoomAnim:function(){var t=this._tileContainer,e=this._bgBuffer;t.style.visibility="",t.parentNode.appendChild(t),o.Util.falseFn(e.offsetWidth),this._animating=!1},_clearBgBuffer:function(){var t=this._map;!t||t._animatingZoom||t.touchZoom._zooming||(this._bgBuffer.innerHTML="",this._bgBuffer.style[o.DomUtil.TRANSFORM]="")},_prepareBgBuffer:function(){var t=this._tileContainer,e=this._bgBuffer,i=this._getLoadedTilesPercentage(e),n=this._getLoadedTilesPercentage(t);return e&&i>.5&&.5>n?(t.style.visibility="hidden",void this._stopLoadingImages(t)):(e.style.visibility="hidden",e.style[o.DomUtil.TRANSFORM]="",this._tileContainer=e,e=this._bgBuffer=t,this._stopLoadingImages(e),void clearTimeout(this._clearBgBufferTimer))},_getLoadedTilesPercentage:function(t){var e,i,n=t.getElementsByTagName("img"),o=0;for(e=0,i=n.length;i>e;e++)n[e].complete&&o++;return o/i},_stopLoadingImages:function(t){var e,i,n,s=Array.prototype.slice.call(t.getElementsByTagName("img"));for(e=0,i=s.length;i>e;e++)n=s[e],n.complete||(n.onload=o.Util.falseFn,n.onerror=o.Util.falseFn,n.src=o.Util.emptyImageUrl,n.parentNode.removeChild(n))}}),o.Map.include({_defaultLocateOptions:{watch:!1,setView:!1,maxZoom:1/0,timeout:1e4,maximumAge:0,enableHighAccuracy:!1},locate:function(t){if(t=this._locateOptions=o.extend(this._defaultLocateOptions,t),!navigator.geolocation)return this._handleGeolocationError({code:0,message:"Geolocation not supported."}),this;var e=o.bind(this._handleGeolocationResponse,this),i=o.bind(this._handleGeolocationError,this);return t.watch?this._locationWatchId=navigator.geolocation.watchPosition(e,i,t):navigator.geolocation.getCurrentPosition(e,i,t),this},stopLocate:function(){return navigator.geolocation&&navigator.geolocation.clearWatch(this._locationWatchId),this._locateOptions&&(this._locateOptions.setView=!1),this},_handleGeolocationError:function(t){var e=t.code,i=t.message||(1===e?"permission denied":2===e?"position unavailable":"timeout");this._locateOptions.setView&&!this._loaded&&this.fitWorld(),this.fire("locationerror",{code:e,message:"Geolocation error: "+i+"."})},_handleGeolocationResponse:function(t){var e=t.coords.latitude,i=t.coords.longitude,n=new o.LatLng(e,i),s=180*t.coords.accuracy/40075017,a=s/Math.cos(o.LatLng.DEG_TO_RAD*e),r=o.latLngBounds([e-s,i-a],[e+s,i+a]),h=this._locateOptions;if(h.setView){var l=Math.min(this.getBoundsZoom(r),h.maxZoom);this.setView(n,l)}var u={latlng:n,bounds:r,timestamp:t.timestamp};for(var c in t.coords)"number"==typeof t.coords[c]&&(u[c]=t.coords[c]);this.fire("locationfound",u)}})}(window,document);/*! esri-leaflet-geocoder - v0.0.1-beta.3 - 2014-02-27
*   Copyright (c) 2014 Environmental Systems Research Institute, Inc.
*   Apache 2.0 License */
!function(a){function b(a,b){b=b||window;for(var c,d=b,e=a.split(".");c=e.shift();)d[c]||(d[c]={}),d=d[c];return d}function c(a){var b="?";for(var c in a)if(a.hasOwnProperty(c)){var d=c,e=a[c];b+=encodeURIComponent(d),b+="=",b+=encodeURIComponent(e),b+="&"}return b.substring(0,b.length-1)}function d(b){var c=new a.LatLng(b.ymin,b.xmin),d=new a.LatLng(b.ymax,b.xmax);return new a.LatLngBounds(c,d)}b("L.esri.Services.Geocoding"),b("L.esri.Controls.Geosearch"),a.esri.Services.Geocoding=a.Class.extend({includes:a.Mixin.Events,options:{url:"https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/",outFields:"Subregion, Region, PlaceName, Match_addr, Country, Addr_type, City, Place_addr"},initialize:function(b){a.Util.setOptions(this,b)},request:function(b,d,e){var f="c"+(1e9*Math.random()).toString(36).replace(".","_");d.f="json",d.callback="L.esri.Services.Geocoding._callback."+f;var g=document.createElement("script");g.type="text/javascript",g.src=b+c(d),g.id=f,this.fire("loading"),a.esri.Services.Geocoding._callback[f]=a.Util.bind(function(b){this.fire("load"),e(b),document.body.removeChild(g),delete a.esri.Services.Geocoding._callback[f]},this),document.body.appendChild(g)},geocode:function(b,c,d){var e={outFields:this.options.outFields},f=a.extend(e,c);f.text=b,this.request(this.options.url+"find",f,d)},suggest:function(a,b,c){var d=b||{};d.text=a,this.request(this.options.url+"suggest",d,c)}}),a.esri.Services.geocoding=function(b){return new a.esri.Services.Geocoding(b)},a.esri.Services.Geocoding._callback={},a.esri.Controls.Geosearch=a.Control.extend({includes:a.Mixin.Events,options:{position:"topleft",zoomToResult:!0,useMapBounds:11,collapseAfterResult:!0,expanded:!1,maxResults:25},initialize:function(b){a.Util.setOptions(this,b),this._service=new a.esri.Services.Geocoding},_processMatch:function(b,c){var e=c.feature.attributes,f=d(c.extent);return{text:b,bounds:f,latlng:new a.LatLng(c.feature.geometry.y,c.feature.geometry.x),name:e.PlaceName,match:e.Addr_type,country:e.Country,region:e.Region,subregion:e.Subregion,city:e.City,address:e.Place_addr?e.Place_addr:e.Match_addr}},_geocode:function(b,c){var d={};if(c)d.magicKey=c;else{var e=this._map.getBounds(),f=e.getCenter(),g=e.getNorthWest();d.bbox=e.toBBoxString(),d.maxLocations=this.options.maxResults,d.location=f.lng+","+f.lat,d.distance=Math.min(Math.max(f.distanceTo(g),2e3),5e4)}a.DomUtil.addClass(this._input,"geocoder-control-loading"),this.fire("loading"),this._service.geocode(b,d,a.Util.bind(function(c){if(c.error)this.fire("error",{code:c.error.code,message:c.error.messsage});else if(c.locations.length){var d,e=[],f=new a.LatLngBounds;for(d=c.locations.length-1;d>=0;d--)e.push(this._processMatch(b,c.locations[d]));for(d=e.length-1;d>=0;d--)f.extend(e[d].bounds);this.fire("results",{results:e,bounds:f,latlng:f.getCenter()}),this.options.zoomToResult&&this._map.fitBounds(f)}else this.fire("results",{results:[],bounds:null,latlng:null,text:b});a.DomUtil.removeClass(this._input,"geocoder-control-loading"),this.fire("load"),this.clear(),this._input.blur()},this))},_suggest:function(b){a.DomUtil.addClass(this._input,"geocoder-control-loading");var c={};if(this.options.useMapBounds===!0||this._map.getZoom()>=this.options.useMapBounds){var d=this._map.getBounds(),e=d.getCenter(),f=d.getNorthWest();c.location=e.lng+","+e.lat,c.distance=Math.min(Math.max(e.distanceTo(f),2e3),5e4)}this._service.suggest(b,c,a.Util.bind(function(b){if(this._input.value){if(this._suggestions.innerHTML="",this._suggestions.style.display="none",b.suggestions){this._suggestions.style.display="block";for(var c=0;c<b.suggestions.length;c++){var d=a.DomUtil.create("li","geocoder-control-suggestion",this._suggestions);d.innerHTML=b.suggestions[c].text,d["data-magic-key"]=b.suggestions[c].magicKey}}a.DomUtil.removeClass(this._input,"geocoder-control-loading")}},this))},clear:function(){this._suggestions.innerHTML="",this._suggestions.style.display="none",this._input.value="",this.options.collapseAfterResult&&a.DomUtil.removeClass(this._container,"geocoder-control-expanded")},onAdd:function(b){return this._map=b,b.attributionControl?b.attributionControl.addAttribution("Geocoding by Esri"):a.control.attribution().addAttribution("Geocoding by Esri").addTo(b),this._container=a.DomUtil.create("div","geocoder-control"+(this.options.expanded?" geocoder-control-expanded":"")),this._input=a.DomUtil.create("input","geocoder-control-input leaflet-bar",this._container),this._suggestions=a.DomUtil.create("ul","geocoder-control-suggestions leaflet-bar",this._container),a.DomEvent.addListener(this._input,"focus",function(){a.DomUtil.addClass(this._container,"geocoder-control-expanded")},this),a.DomEvent.addListener(this._container,"click",function(){a.DomUtil.addClass(this._container,"geocoder-control-expanded"),this._input.focus()},this),a.DomEvent.addListener(this._suggestions,"mousedown",function(a){var b=a.target||a.srcElement;this._geocode(b.innerHTML,b["data-magic-key"]),this.clear()},this),a.DomEvent.addListener(this._input,"blur",function(){this.clear()},this),a.DomEvent.addListener(this._input,"keydown",function(b){var c=this._suggestions.querySelectorAll(".geocoder-control-selected")[0];switch(b.keyCode){case 13:c?(this._geocode(c.innerHTML,c["data-magic-key"]),this.clear()):this.options.allowMultipleResults?this._geocode(this._input.value):a.DomUtil.addClass(this._suggestions.childNodes[0],"geocoder-control-selected"),this.clear(),a.DomEvent.preventDefault(b);break;case 38:c&&a.DomUtil.removeClass(c,"geocoder-control-selected"),c&&c.previousSibling?a.DomUtil.addClass(c.previousSibling,"geocoder-control-selected"):a.DomUtil.addClass(this._suggestions.childNodes[this._suggestions.childNodes.length-1],"geocoder-control-selected"),a.DomEvent.preventDefault(b);break;case 40:c&&a.DomUtil.removeClass(c,"geocoder-control-selected"),c&&c.nextSibling?a.DomUtil.addClass(c.nextSibling,"geocoder-control-selected"):a.DomUtil.addClass(this._suggestions.childNodes[0],"geocoder-control-selected"),a.DomEvent.preventDefault(b)}},this),a.DomEvent.addListener(this._input,"keyup",function(a){var b=a.which||a.keyCode,c=(a.target||a.srcElement).value;return c.length<2?void 0:27===b?(this._suggestions.innerHTML="",void(this._suggestions.style.display="none")):void(13!==b&&38!==b&&40!==b&&this._suggest(c))},this),a.DomEvent.disableClickPropagation(this._container),this._container},onRemove:function(a){a.attributionControl.removeAttribution("Geocoding by Esri")}}),a.esri.Controls.geosearch=function(b){return new a.esri.Controls.Geosearch(b)}}(L);!function(t){"use strict";"function"==typeof define&&define.amd?define(["jquery"],t):t("object"==typeof exports?require("jquery"):jQuery)}(function(t){"use strict";t.Zebra_DatePicker=function(e,s){var i,n,a,r,o,d,c,l,h,_,g,u,f,p,b,y,v,m,w,k,D,A,M,F,C,Y,P,S,Z,x,z,I,N,O,W,j,T,J,L,H={always_visible:!1,container:t("body"),custom_classes:!1,days:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],days_abbr:!1,default_position:"above",direction:0,disabled_dates:!1,enabled_dates:!1,first_day_of_week:1,format:"Y-m-d",header_captions:{days:"F, Y",months:"Y",years:"Y1 - Y2"},header_navigation:["&#171;","&#187;"],icon_position:"right",inside:!0,lang_clear_date:"Clear date",months:["January","February","March","April","May","June","July","August","September","October","November","December"],months_abbr:!1,offset:[5,-5],open_icon_only:!1,pair:!1,readonly_element:!0,select_other_months:!1,show_clear_date:0,show_icon:!0,show_other_months:!0,show_select_today:"Today",show_week_number:!1,start_date:!1,strict:!1,view:"days",weekend_days:[0,6],zero_pad:!1,onChange:null,onClear:null,onOpen:null,onClose:null,onSelect:null},V={},$=this;$.settings={};var q=t(e),E=function(e){var A,S,Z,x,E={days:["d","j","D"],months:["F","m","M","n","t"],years:["o","Y","y"]},Q=!1,U=!1,X=!1,G=null;if(T=Math.floor(65536*(1+Math.random())).toString(16),!e){$.settings=t.extend({},H,s),V.readonly=q.attr("readonly"),V.style=q.attr("style");for(A in q.data())0===A.indexOf("zdp_")&&(A=A.replace(/^zdp\_/,""),void 0!==H[A]&&($.settings[A]="pair"===A?t(q.data("zdp_"+A)):q.data("zdp_"+A)))}$.settings.readonly_element&&q.attr("readonly","readonly");for(G in E)t.each(E[G],function(t,e){$.settings.format.indexOf(e)>-1&&("days"===G?Q=!0:"months"===G?U=!0:"years"===G&&(X=!0))});z=Q&&U&&X?["years","months","days"]:!Q&&U&&X?["years","months"]:Q&&U&&!X?["months","days"]:Q||U||!X?Q||!U||X?["years","months","days"]:["months"]:["years"],-1===t.inArray($.settings.view,z)&&($.settings.view=z[z.length-1]),D=[],k=[],J={},L=[];for(Z in $.settings.custom_classes)$.settings.custom_classes.hasOwnProperty(Z)&&L.push(Z);for(x=0;x<2+L.length;x++)S=0===x?$.settings.disabled_dates:1===x?$.settings.enabled_dates:$.settings.custom_classes[L[x-2]],t.isArray(S)&&S.length>0&&t.each(S,function(){var e,s,i,n,a=this.split(" ");for(e=0;e<4;e++){for(a[e]||(a[e]="*"),a[e]=a[e].indexOf(",")>-1?a[e].split(","):new Array(a[e]),s=0;s<a[e].length;s++)if(a[e][s].indexOf("-")>-1&&null!==(n=a[e][s].match(/^([0-9]+)\-([0-9]+)/))){for(i=ct(n[1]);i<=ct(n[2]);i++)-1===t.inArray(i,a[e])&&a[e].push(i+"");a[e].splice(s,1)}for(s=0;s<a[e].length;s++)a[e][s]=isNaN(ct(a[e][s]))?a[e][s]:ct(a[e][s])}0===x?D.push(a):1===x?k.push(a):(void 0===J[L[x-2]]&&(J[L[x-2]]=[]),J[L[x-2]].push(a))});var K,tt,et=new Date,nt=$.settings.reference_date?$.settings.reference_date:q.data("zdp_reference_date")&&void 0!==q.data("zdp_reference_date")?q.data("zdp_reference_date"):et;if(M=void 0,F=void 0,u=nt.getMonth(),h=et.getMonth(),f=nt.getFullYear(),_=et.getFullYear(),p=nt.getDate(),g=et.getDate(),!0===$.settings.direction)M=nt;else if(!1===$.settings.direction)P=(F=nt).getMonth(),Y=F.getFullYear(),C=F.getDate();else if(!t.isArray($.settings.direction)&&it($.settings.direction)&&ct($.settings.direction)>0||t.isArray($.settings.direction)&&((K=R($.settings.direction[0]))||!0===$.settings.direction[0]||it($.settings.direction[0])&&$.settings.direction[0]>0)&&((tt=R($.settings.direction[1]))||!1===$.settings.direction[1]||it($.settings.direction[1])&&$.settings.direction[1]>=0))M=K||new Date(f,u,p+ct(t.isArray($.settings.direction)?!0===$.settings.direction[0]?0:$.settings.direction[0]:$.settings.direction)),u=M.getMonth(),f=M.getFullYear(),p=M.getDate(),tt&&+tt>=+M?F=tt:!tt&&!1!==$.settings.direction[1]&&t.isArray($.settings.direction)&&(F=new Date(f,u,p+ct($.settings.direction[1]))),F&&(P=F.getMonth(),Y=F.getFullYear(),C=F.getDate());else if(!t.isArray($.settings.direction)&&it($.settings.direction)&&ct($.settings.direction)<0||t.isArray($.settings.direction)&&(!1===$.settings.direction[0]||it($.settings.direction[0])&&$.settings.direction[0]<0)&&((K=R($.settings.direction[1]))||it($.settings.direction[1])&&$.settings.direction[1]>=0))F=new Date(f,u,p+ct(t.isArray($.settings.direction)?!1===$.settings.direction[0]?0:$.settings.direction[0]:$.settings.direction)),P=F.getMonth(),Y=F.getFullYear(),C=F.getDate(),K&&+K<+F?M=K:!K&&t.isArray($.settings.direction)&&(M=new Date(Y,P,C-ct($.settings.direction[1]))),M&&(u=M.getMonth(),f=M.getFullYear(),p=M.getDate());else if(t.isArray($.settings.disabled_dates)&&$.settings.disabled_dates.length>0)for(var ot in D)if("*"===D[ot][0]&&"*"===D[ot][1]&&"*"===D[ot][2]&&"*"===D[ot][3]){var ht=[];if(t.each(k,function(){var t=this;"*"!==t[2][0]&&ht.push(parseInt(t[2][0]+("*"===t[1][0]?"12":dt(t[1][0],2))+("*"===t[0][0]?"*"===t[1][0]?"31":new Date(t[2][0],t[1][0],0).getDate():dt(t[0][0],2)),10))}),ht.sort(),ht.length>0){var gt=(ht[0]+"").match(/([0-9]{4})([0-9]{2})([0-9]{2})/);f=parseInt(gt[1],10),u=parseInt(gt[2],10)-1,p=parseInt(gt[3],10)}break}if(st(f,u,p)){for(;st(f);)M?(f++,u=0):(f--,u=11);for(;st(f,u);)M?(u++,p=1):(u--,p=new Date(f,u+1,0).getDate()),u>11?(f++,u=0,p=1):u<0&&(f--,u=11,p=new Date(f,u+1,0).getDate());for(;st(f,u,p);)M?p++:p--,et=new Date(f,u,p),f=et.getFullYear(),u=et.getMonth(),p=et.getDate();et=new Date(f,u,p),f=et.getFullYear(),u=et.getMonth(),p=et.getDate()}var ut=R(q.val()||($.settings.start_date?$.settings.start_date:""));if(ut&&$.settings.strict&&st(ut.getFullYear(),ut.getMonth(),ut.getDate())&&q.val(""),e||void 0===M&&void 0===ut||lt(void 0!==ut?ut:M),!$.settings.always_visible){if(!e){if($.settings.show_icon){"firefox"===_t.name&&q.is('input[type="text"]')&&"inline"===q.css("display")&&q.css("display","inline-block");var ft=t('<span class="Zebra_DatePicker_Icon_Wrapper"></span>').css({display:q.css("display"),position:"static"===q.css("position")?"relative":q.css("position"),float:q.css("float"),top:q.css("top"),right:q.css("right"),bottom:q.css("bottom"),left:q.css("left")});"block"===q.css("display")&&ft.css("width",q.outerWidth(!0)),q.wrap(ft).css({position:"relative",top:"auto",right:"auto",bottom:"auto",left:"auto"}),a=t('<button type="button" class="Zebra_DatePicker_Icon'+("disabled"===q.attr("disabled")?" Zebra_DatePicker_Icon_Disabled":"")+'">Pick a date</button>'),$.icon=a,I=$.settings.open_icon_only?a:a.add(q)}else I=q;I.on("click.Zebra_DatePicker_"+T,function(t){t.preventDefault(),q.attr("disabled")||(n.hasClass("dp_visible")?$.hide():$.show())}),!$.settings.readonly_element&&$.settings.pair&&q.on("blur.Zebra_DatePicker_"+T,function(){var e;(e=R(t(this).val()))&&!st(e.getFullYear(),e.getMonth(),e.getDate())&&lt(e)}),void 0!==a&&a.insertAfter(q)}if(void 0!==a){a.attr("style",""),$.settings.inside&&a.addClass("Zebra_DatePicker_Icon_Inside_"+("right"===$.settings.icon_position?"Right":"Left"));var pt=q.outerWidth(),bt=q.outerHeight(),yt=parseInt(q.css("marginLeft"),10)||0,vt=parseInt(q.css("marginTop"),10)||0,mt=(a.outerWidth(),a.outerHeight()),wt=parseInt(a.css("marginLeft"),10)||0;parseInt(a.css("marginRight"),10);$.settings.inside?(a.css("top",vt+(bt-mt)/2),"right"===$.settings.icon_position?a.css("right",0):a.css("left",0)):a.css({top:vt+(bt-mt)/2,left:yt+pt+wt}),a.removeClass(" Zebra_DatePicker_Icon_Disabled"),"disabled"===q.attr("disabled")&&a.addClass("Zebra_DatePicker_Icon_Disabled")}}if(W=!1!==$.settings.show_select_today&&t.inArray("days",z)>-1&&!st(_,h,g)&&$.settings.show_select_today,e)return t(".dp_previous",n).html($.settings.header_navigation[0]),t(".dp_next",n).html($.settings.header_navigation[1]),t(".dp_clear",n).html($.settings.lang_clear_date),void t(".dp_today",n).html($.settings.show_select_today);t(window).on("resize.Zebra_DatePicker_"+T+", orientationchange.Zebra_DatePicker_"+T,function(){$.hide(),void 0!==a&&(clearTimeout(j),j=setTimeout(function(){$.update()},100))});var kt='<div class="Zebra_DatePicker"><table class="dp_header"><tr><td class="dp_previous">'+$.settings.header_navigation[0]+'</td><td class="dp_caption">&#032;</td><td class="dp_next">'+$.settings.header_navigation[1]+'</td></tr></table><table class="dp_daypicker"></table><table class="dp_monthpicker"></table><table class="dp_yearpicker"></table><table class="dp_footer"><tr><td class="dp_today"'+(!1!==$.settings.show_clear_date?' style="width:50%"':"")+">"+W+'</td><td class="dp_clear"'+(!1!==W?' style="width:50%"':"")+">"+$.settings.lang_clear_date+"</td></tr></table></div>";n=t(kt),$.datepicker=n,r=t("table.dp_header",n),o=t("table.dp_daypicker",n),d=t("table.dp_monthpicker",n),c=t("table.dp_yearpicker",n),O=t("table.dp_footer",n),N=t("td.dp_today",O),l=t("td.dp_clear",O),$.settings.always_visible?q.attr("disabled")||($.settings.always_visible.append(n),$.show()):$.settings.container.append(n),n.delegate("td:not(.dp_disabled, .dp_weekend_disabled, .dp_not_in_month, .dp_week_number)","mouseover",function(){t(this).addClass("dp_hover")}).delegate("td:not(.dp_disabled, .dp_weekend_disabled, .dp_not_in_month, .dp_week_number)","mouseout",function(){t(this).removeClass("dp_hover")}),B(t("td",r)),t(".dp_previous",r).on("click",function(){"months"===i?y--:"years"===i?y-=12:--b<0&&(b=11,y--),at()}),t(".dp_caption",r).on("click",function(){i="days"===i?t.inArray("months",z)>-1?"months":t.inArray("years",z)>-1?"years":"days":"months"===i?t.inArray("years",z)>-1?"years":t.inArray("days",z)>-1?"days":"months":t.inArray("days",z)>-1?"days":t.inArray("months",z)>-1?"months":"years",at()}),t(".dp_next",r).on("click",function(){"months"===i?y++:"years"===i?y+=12:12==++b&&(b=0,y++),at()}),o.delegate("td:not(.dp_disabled, .dp_weekend_disabled, .dp_not_in_month, .dp_week_number)","click",function(){var e;$.settings.select_other_months&&t(this).attr("class")&&null!==(e=t(this).attr("class").match(/date\_([0-9]{4})(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])/))?rt(e[1],e[2]-1,e[3],"days",t(this)):rt(y,b,ct(t(this).html()),"days",t(this))}),d.delegate("td:not(.dp_disabled)","click",function(){var e=t(this).attr("class").match(/dp\_month\_([0-9]+)/);b=ct(e[1]),-1===t.inArray("days",z)?rt(y,b,1,"months",t(this)):(i="days",$.settings.always_visible&&q.val(""),at())}),c.delegate("td:not(.dp_disabled)","click",function(){y=ct(t(this).html()),-1===t.inArray("months",z)?rt(y,1,1,"years",t(this)):(i="months",$.settings.always_visible&&q.val(""),at())}),t(N).on("click",function(e){e.preventDefault(),rt(_,h,g,"days",t(".dp_current",o)),$.settings.always_visible&&$.show(),$.hide()}),t(l).on("click",function(e){e.preventDefault(),q.val(""),$.settings.always_visible?(v=null,m=null,w=null,t("td.dp_selected",n).removeClass("dp_selected")):(v=null,m=null,w=null,b=null,y=null),$.hide(),$.settings.onClear&&"function"==typeof $.settings.onClear&&$.settings.onClear.call(q,q)}),$.settings.always_visible||(t(document).on("mousedown.Zebra_DatePicker_"+T+", touchstart.Zebra_DatePicker_"+T,function(e){if(n.hasClass("dp_visible")){if($.settings.show_icon&&t(e.target).get(0)===a.get(0))return!0;0===t(e.target).parents().filter(".Zebra_DatePicker").length&&$.hide()}}),t(document).on("keyup.Zebra_DatePicker_"+T,function(t){n.hasClass("dp_visible")&&27===t.which&&$.hide()})),at()};$.clear_date=function(){t(l).trigger("click")},$.destroy=function(){void 0!==$.icon&&$.icon.remove(),$.datepicker.remove(),$.settings.show_icon&&!$.settings.always_visible&&q.unwrap(),q.off("click.Zebra_DatePicker_"+T),q.off("blur.Zebra_DatePicker_"+T),t(document).off("keyup.Zebra_DatePicker_"+T),t(document).off("mousedown.Zebra_DatePicker_"+T),t(window).off("resize.Zebra_DatePicker_"+T),t(window).off("orientationchange.Zebra_DatePicker_"+T),q.removeData("Zebra_DatePicker"),q.attr("readonly",V.readonly),q.attr("style",V.style?V.style:"")},$.hide=function(){$.settings.always_visible||(et("hide"),n.removeClass("dp_visible").addClass("dp_hidden"),$.settings.onClose&&"function"==typeof $.settings.onClose&&$.settings.onClose.call(q,q))},$.set_date=function(t){var e;(e=R(t))&&!st(e.getFullYear(),e.getMonth(),e.getDate())&&(q.val(t),lt(e))},$.show=function(){i=$.settings.view;var e=R(q.val()||($.settings.start_date?$.settings.start_date:""));if(e?(m=e.getMonth(),b=e.getMonth(),w=e.getFullYear(),y=e.getFullYear(),v=e.getDate(),st(w,m,v)&&($.settings.strict&&q.val(""),b=u,y=f)):(b=u,y=f),at(),$.settings.always_visible)n.removeClass("dp_hidden").addClass("dp_visible");else{if($.settings.container.is("body")){var s=n.outerWidth(),r=n.outerHeight(),o=(void 0!==a?a.offset().left+a.outerWidth(!0):q.offset().left+q.outerWidth(!0))+$.settings.offset[0],d=(void 0!==a?a.offset().top:q.offset().top)-r+$.settings.offset[1],c=t(window).width(),l=t(window).height(),h=t(window).scrollTop(),_=t(window).scrollLeft();"below"===$.settings.default_position&&(d=(void 0!==a?a.offset().top:q.offset().top)+$.settings.offset[1]),o+s>_+c&&(o=_+c-s),o<_&&(o=_),d+r>h+l&&(d=h+l-r),d<h&&(d=h),n.css({left:o,top:d})}else n.css({left:0,top:0});n.removeClass("dp_hidden").addClass("dp_visible"),et()}$.settings.onOpen&&"function"==typeof $.settings.onOpen&&$.settings.onOpen.call(q,q)},$.update=function(e){$.original_direction&&($.original_direction=$.direction),$.settings=t.extend($.settings,e),E(!0)};var R=function(e){if(e+="",""!==t.trim(e)){for(var s=Q($.settings.format),i=["d","D","j","l","N","S","w","F","m","M","n","Y","y"],n=[],a=[],r=null,o=null,d=0;d<i.length;d++)(r=s.indexOf(i[d]))>-1&&n.push({character:i[d],position:r});if(n.sort(function(t,e){return t.position-e.position}),t.each(n,function(t,e){switch(e.character){case"d":a.push("0[1-9]|[12][0-9]|3[01]");break;case"D":a.push("[a-z]{3}");break;case"j":a.push("[1-9]|[12][0-9]|3[01]");break;case"l":a.push("[a-z]+");break;case"N":a.push("[1-7]");break;case"S":a.push("st|nd|rd|th");break;case"w":a.push("[0-6]");break;case"F":a.push("[a-z]+");break;case"m":a.push("0[1-9]|1[012]+");break;case"M":a.push("[a-z]{3}");break;case"n":a.push("[1-9]|1[012]");break;case"Y":a.push("[0-9]{4}");break;case"y":a.push("[0-9]{2}")}}),a.length&&(n.reverse(),t.each(n,function(t,e){s=s.replace(e.character,"("+a[a.length-t-1]+")")}),a=new RegExp("^"+s+"$","ig"),o=a.exec(e))){var c,l=new Date,h=1,_=l.getMonth()+1,g=l.getFullYear(),u=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],f=["January","February","March","April","May","June","July","August","September","October","November","December"],p=!0;if(n.reverse(),t.each(n,function(e,s){if(!p)return!0;switch(s.character){case"m":case"n":_=ct(o[e+1]);break;case"d":case"j":h=ct(o[e+1]);break;case"D":case"l":case"F":case"M":c="D"===s.character||"l"===s.character?$.settings.days:$.settings.months,p=!1,t.each(c,function(t,i){if(p)return!0;if(o[e+1].toLowerCase()===i.substring(0,"D"===s.character||"M"===s.character?3:i.length).toLowerCase()){switch(s.character){case"D":o[e+1]=u[t].substring(0,3);break;case"l":o[e+1]=u[t];break;case"F":o[e+1]=f[t],_=t+1;break;case"M":o[e+1]=f[t].substring(0,3),_=t+1}p=!0}});break;case"Y":g=ct(o[e+1]);break;case"y":g="19"+ct(o[e+1])}}),p){var b=new Date(g,(_||1)-1,h||1);if(b.getFullYear()===g&&b.getDate()===(h||1)&&b.getMonth()===(_||1)-1)return b}}return!1}},B=function(t){"firefox"===_t.name?t.css("MozUserSelect","none"):"explorer"===_t.name?t.on("selectstart",function(){return!1}):t.mousedown(function(){return!1})},Q=function(t){return t.replace(/([-.,*+?^${}()|[\]\/\\])/g,"\\$1")},U=function(e){var s,i,n="",a=e.getDate(),r=e.getDay(),o=$.settings.days[r],d=e.getMonth()+1,c=$.settings.months[d-1],l=e.getFullYear()+"";for(s=0;s<$.settings.format.length;s++)switch(i=$.settings.format.charAt(s)){case"y":l=l.substr(2);case"Y":n+=l;break;case"m":d=dt(d,2);case"n":n+=d;break;case"M":c=t.isArray($.settings.months_abbr)&&void 0!==$.settings.months_abbr[d-1]?$.settings.months_abbr[d-1]:$.settings.months[d-1].substr(0,3);case"F":n+=c;break;case"d":a=dt(a,2);case"j":n+=a;break;case"D":o=t.isArray($.settings.days_abbr)&&void 0!==$.settings.days_abbr[r]?$.settings.days_abbr[r]:$.settings.days[r].substr(0,3);case"l":n+=o;break;case"N":r++;case"w":n+=r;break;case"S":n+=a%10==1&&"11"!==a?"st":a%10==2&&"12"!==a?"nd":a%10==3&&"13"!==a?"rd":"th";break;default:n+=i}return n},X=function(){var e,s,i,n,a,r,d,c,l,u,f=new Date(y,b+1,0).getDate(),p=new Date(y,b,1).getDay(),k=new Date(y,b,0).getDate(),D=p-$.settings.first_day_of_week;for(D=D<0?7+D:D,nt($.settings.header_captions.days),s="<tr>",$.settings.show_week_number&&(s+="<th>"+$.settings.show_week_number+"</th>"),e=0;e<7;e++)s+="<th>"+(t.isArray($.settings.days_abbr)&&void 0!==$.settings.days_abbr[($.settings.first_day_of_week+e)%7]?$.settings.days_abbr[($.settings.first_day_of_week+e)%7]:$.settings.days[($.settings.first_day_of_week+e)%7].substr(0,2))+"</th>";for(s+="</tr><tr>",e=0;e<42;e++)e>0&&e%7==0&&(s+="</tr><tr>"),e%7==0&&$.settings.show_week_number&&(s+='<td class="dp_week_number">'+ht(new Date(y,b,e-D+1))+"</td>"),i=e-D+1,$.settings.select_other_months&&(e<D||i>f)&&(a=(n=new Date(y,b,i)).getFullYear(),r=n.getMonth(),d=n.getDate(),n=a+dt(r+1,2)+dt(d,2)),e<D?s+='<td class="'+($.settings.select_other_months&&!st(a,r,d)?"dp_not_in_month_selectable date_"+n:"dp_not_in_month")+'">'+($.settings.select_other_months||$.settings.show_other_months?dt(k-D+e+1,$.settings.zero_pad?2:0):"&nbsp;")+"</td>":i>f?s+='<td class="'+($.settings.select_other_months&&!st(a,r,d)?"dp_not_in_month_selectable date_"+n:"dp_not_in_month")+'">'+($.settings.select_other_months||$.settings.show_other_months?dt(i-f,$.settings.zero_pad?2:0):"&nbsp;")+"</td>":(c=($.settings.first_day_of_week+e)%7,l="",u=tt(y,b,i),st(y,b,i)?(t.inArray(c,$.settings.weekend_days)>-1?l="dp_weekend_disabled":l+=" dp_disabled",b===h&&y===_&&g===i&&(l+=" dp_disabled_current"),""!==u&&(l+=" "+u+"_disabled")):(t.inArray(c,$.settings.weekend_days)>-1&&(l="dp_weekend"),b===m&&y===w&&v===i&&(l+=" dp_selected"),b===h&&y===_&&g===i&&(l+=" dp_current"),""!==u&&(l+=" "+u)),s+="<td"+(""!==l?' class="'+t.trim(l)+'"':"")+">"+(($.settings.zero_pad?dt(i,2):i)||"&nbsp;")+"</td>");s+="</tr>",o.html(t(s)),$.settings.always_visible&&(S=t("td:not(.dp_disabled, .dp_weekend_disabled, .dp_not_in_month, .dp_week_number)",o)),o.show()},G=function(){nt($.settings.header_captions.months);var e,s,i="<tr>";for(e=0;e<12;e++)e>0&&e%3==0&&(i+="</tr><tr>"),s="dp_month_"+e,st(y,e)?s+=" dp_disabled":!1!==m&&m===e&&y===w?s+=" dp_selected":h===e&&_===y&&(s+=" dp_current"),i+='<td class="'+t.trim(s)+'">'+(t.isArray($.settings.months_abbr)&&void 0!==$.settings.months_abbr[e]?$.settings.months_abbr[e]:$.settings.months[e].substr(0,3))+"</td>";i+="</tr>",d.html(t(i)),$.settings.always_visible&&(Z=t("td:not(.dp_disabled)",d)),d.show()},K=function(){nt($.settings.header_captions.years);var e,s,i="<tr>";for(e=0;e<12;e++)e>0&&e%3==0&&(i+="</tr><tr>"),s="",st(y-7+e)?s+=" dp_disabled":w&&w===y-7+e?s+=" dp_selected":_===y-7+e&&(s+=" dp_current"),i+="<td"+(""!==t.trim(s)?' class="'+t.trim(s)+'"':"")+">"+(y-7+e)+"</td>";i+="</tr>",c.html(t(i)),$.settings.always_visible&&(x=t("td:not(.dp_disabled)",c)),c.show()},tt=function(e,s,i){var n,a,r;void 0!==s&&(s+=1);for(a in L)if(n=L[a],r=!1,t.isArray(J[n])&&t.each(J[n],function(){if(!r){var a,o=this;if((t.inArray(e,o[2])>-1||t.inArray("*",o[2])>-1)&&(void 0!==s&&t.inArray(s,o[1])>-1||t.inArray("*",o[1])>-1)&&(void 0!==i&&t.inArray(i,o[0])>-1||t.inArray("*",o[0])>-1)){if("*"===o[3])return r=n;if(a=new Date(e,s-1,i).getDay(),t.inArray(a,o[3])>-1)return r=n}}}),r)return r;return r||""},et=function(e){var s,i;if("explorer"===_t.name&&6===_t.version)switch(A||(s=ct(n.css("zIndex"))-1,A=t("<iframe>",{src:'javascript:document.write("")',scrolling:"no",frameborder:0,css:{zIndex:s,position:"absolute",top:-1e3,left:-1e3,width:n.outerWidth(),height:n.outerHeight(),filter:"progid:DXImageTransform.Microsoft.Alpha(opacity=0)",display:"none"}}),t("body").append(A)),e){case"hide":A.hide();break;default:i=n.offset(),A.css({top:i.top,left:i.left,display:"block"})}},st=function(e,s,i){var n,a,r,o;if(!(void 0!==e&&!isNaN(e)||void 0!==s&&!isNaN(s)||void 0!==i&&!isNaN(i)))return!1;if(e<1e3)return!0;if(t.isArray($.settings.direction)||0!==ct($.settings.direction)){if(n=ct(ot(e,void 0!==s?dt(s,2):"",void 0!==i?dt(i,2):"")),8===(a=(n+"").length)&&(void 0!==M&&n<ct(ot(f,dt(u,2),dt(p,2)))||void 0!==F&&n>ct(ot(Y,dt(P,2),dt(C,2)))))return!0;if(6===a&&(void 0!==M&&n<ct(ot(f,dt(u,2)))||void 0!==F&&n>ct(ot(Y,dt(P,2)))))return!0;if(4===a&&(void 0!==M&&n<f||void 0!==F&&n>Y))return!0}return void 0!==s&&(s+=1),r=!1,o=!1,t.isArray(D)&&D.length&&t.each(D,function(){if(!r){var n,a=this;if((t.inArray(e,a[2])>-1||t.inArray("*",a[2])>-1)&&(void 0!==s&&t.inArray(s,a[1])>-1||t.inArray("*",a[1])>-1)&&(void 0!==i&&t.inArray(i,a[0])>-1||t.inArray("*",a[0])>-1)){if("*"===a[3])return r=!0;if(n=new Date(e,s-1,i).getDay(),t.inArray(n,a[3])>-1)return r=!0}}}),k&&t.each(k,function(){if(!o){var n,a=this;if((t.inArray(e,a[2])>-1||t.inArray("*",a[2])>-1)&&(o=!0,void 0!==s))if(o=!0,t.inArray(s,a[1])>-1||t.inArray("*",a[1])>-1){if(void 0!==i)if(o=!0,t.inArray(i,a[0])>-1||t.inArray("*",a[0])>-1){if("*"===a[3])return o=!0;if(n=new Date(e,s-1,i).getDay(),t.inArray(n,a[3])>-1)return o=!0;o=!1}else o=!1}else o=!1}}),(!k||!o)&&!(!D||!r)},it=function(t){return(t+"").match(/^\-?[0-9]+$/)},nt=function(e){!isNaN(parseFloat(b))&&isFinite(b)&&(e=e.replace(/\bm\b|\bn\b|\bF\b|\bM\b/,function(e){switch(e){case"m":return dt(b+1,2);case"n":return b+1;case"F":return $.settings.months[b];case"M":return t.isArray($.settings.months_abbr)&&void 0!==$.settings.months_abbr[b]?$.settings.months_abbr[b]:$.settings.months[b].substr(0,3);default:return e}})),!isNaN(parseFloat(y))&&isFinite(y)&&(e=e.replace(/\bY\b/,y).replace(/\by\b/,(y+"").substr(2)).replace(/\bY1\b/i,y-7).replace(/\bY2\b/i,y+4)),t(".dp_caption",r).html(e)},at=function(){var e,s,a;""===o.text()||"days"===i?(""===o.text()?($.settings.always_visible||n.css("left",-1e3),n.css("visibility","visible"),X(),e=o.outerWidth(),s=o.outerHeight(),d.css({width:e,height:s}),c.css({width:e,height:s}),r.css("width",e),O.css("width",e),n.css("visibility","").addClass("dp_hidden")):X(),d.hide(),c.hide()):"months"===i?(G(),o.hide(),c.hide()):"years"===i&&(K(),o.hide(),d.hide()),$.settings.onChange&&"function"==typeof $.settings.onChange&&void 0!==i&&((a="days"===i?o.find("td:not(.dp_disabled, .dp_weekend_disabled, .dp_not_in_month)"):"months"===i?d.find("td:not(.dp_disabled, .dp_weekend_disabled, .dp_not_in_month)"):c.find("td:not(.dp_disabled, .dp_weekend_disabled, .dp_not_in_month)")).each(function(){var e;"days"===i?t(this).hasClass("dp_not_in_month_selectable")?(e=t(this).attr("class").match(/date\_([0-9]{4})(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])/),t(this).data("date",e[1]+"-"+e[2]+"-"+e[3])):t(this).data("date",y+"-"+dt(b+1,2)+"-"+dt(ct(t(this).text()),2)):"months"===i?(e=t(this).attr("class").match(/dp\_month\_([0-9]+)/),t(this).data("date",y+"-"+dt(ct(e[1])+1,2))):t(this).data("date",ct(t(this).text()))}),$.settings.onChange.call(q,i,a,q)),O.show(),!0===$.settings.show_clear_date||0===$.settings.show_clear_date&&""!==q.val()||$.settings.always_visible&&!1!==$.settings.show_clear_date?(l.show(),W?(N.css("width","50%"),l.css("width","50%")):(N.hide(),l.css("width","100%"))):(l.hide(),W?N.show().css("width","100%"):O.hide())},rt=function(t,e,s,i,n){var a=new Date(t,e,s,12,0,0),r="days"===i?S:"months"===i?Z:x,o=U(a);q.val(o),$.settings.always_visible&&(m=a.getMonth(),b=a.getMonth(),w=a.getFullYear(),y=a.getFullYear(),v=a.getDate(),r.removeClass("dp_selected"),n.addClass("dp_selected"),"days"===i&&n.hasClass("dp_not_in_month_selectable")&&$.show()),$.hide(),lt(a),$.settings.onSelect&&"function"==typeof $.settings.onSelect&&$.settings.onSelect.call(q,o,t+"-"+dt(e+1,2)+"-"+dt(s,2),a,q,ht(a)),q.focus()},ot=function(){var t,e="";for(t=0;t<arguments.length;t++)e+=arguments[t]+"";return e},dt=function(t,e){for(t+="";t.length<e;)t="0"+t;return t},ct=function(t){return parseInt(t,10)},lt=function(e){$.settings.pair&&t.each($.settings.pair,function(){var s,i=t(this);i.data&&i.data("Zebra_DatePicker")?((s=i.data("Zebra_DatePicker")).update({reference_date:e,direction:0===s.settings.direction?1:s.settings.direction}),s.settings.always_visible&&s.show()):i.data("zdp_reference_date",e)})},ht=function(t){var e,s,i,n,a,r,o,d=t.getFullYear(),c=t.getMonth()+1,l=t.getDate();return c<3?(i=(s=((e=d-1)/4|0)-(e/100|0)+(e/400|0))-(((e-1)/4|0)-((e-1)/100|0)+((e-1)/400|0)),n=0,a=l-1+31*(c-1)):(n=(i=(s=((e=d)/4|0)-(e/100|0)+(e/400|0))-(((e-1)/4|0)-((e-1)/100|0)+((e-1)/400|0)))+1,a=l+((153*(c-3)+2)/5|0)+58+i),r=(e+s)%7,l=(a+r-n)%7,o=a+3-l,o<0?53-((r-i)/5|0):o>364+i?1:1+(o/7|0)},_t={init:function(){this.name=this.searchString(this.dataBrowser)||"",this.version=this.searchVersion(navigator.userAgent)||this.searchVersion(navigator.appVersion)||""},searchString:function(t){var e,s,i;for(e=0;e<t.length;e++)if(s=t[e].string,i=t[e].prop,this.versionSearchString=t[e].versionSearch||t[e].identity,s){if(-1!==s.indexOf(t[e].subString))return t[e].identity}else if(i)return t[e].identity},searchVersion:function(t){var e=t.indexOf(this.versionSearchString);if(-1!==e)return parseFloat(t.substring(e+this.versionSearchString.length+1))},dataBrowser:[{string:navigator.userAgent,subString:"Firefox",identity:"firefox"},{string:navigator.userAgent,subString:"MSIE",identity:"explorer",versionSearch:"MSIE"}]};_t.init(),E()},t.fn.Zebra_DatePicker=function(e){return this.each(function(){void 0!==t(this).data("Zebra_DatePicker")&&t(this).data("Zebra_DatePicker").destroy();var s=new t.Zebra_DatePicker(this,e);t(this).data("Zebra_DatePicker",s)})}});!function(e){"object"==typeof module&&"object"==typeof module.exports?e(require("jquery"),window,document):"undefined"!=typeof jQuery&&e(jQuery,window,document)}(function(e,t,i,n){!function(){function t(e,t,i){return new Array(i+1-e.length).join(t)+e}function n(){if(1===arguments.length){var t=arguments[0];return"string"==typeof t&&(t=e.fn.timepicker.parseTime(t)),new Date(0,0,0,t.getHours(),t.getMinutes(),t.getSeconds())}return 3===arguments.length?new Date(0,0,0,arguments[0],arguments[1],arguments[2]):2===arguments.length?new Date(0,0,0,arguments[0],arguments[1],0):new Date(0,0,0)}e.TimePicker=function(){var t=this;t.container=e(".ui-timepicker-container"),t.ui=t.container.find(".ui-timepicker"),0===t.container.length&&(t.container=e("<div></div>").addClass("ui-timepicker-container").addClass("ui-timepicker-hidden ui-helper-hidden").appendTo("body").hide(),t.ui=e("<div></div>").addClass("ui-timepicker").addClass("ui-widget ui-widget-content ui-menu").addClass("ui-corner-all").appendTo(t.container),t.viewport=e("<ul></ul>").addClass("ui-timepicker-viewport").appendTo(t.ui),e.fn.jquery>="1.4.2"&&t.ui.delegate("a","mouseenter.timepicker",function(){t.activate(!1,e(this).parent())}).delegate("a","mouseleave.timepicker",function(){t.deactivate(!1)}).delegate("a","click.timepicker",function(i){i.preventDefault(),t.select(!1,e(this).parent())}))},e.TimePicker.count=0,e.TimePicker.instance=function(){return e.TimePicker._instance||(e.TimePicker._instance=new e.TimePicker),e.TimePicker._instance},e.TimePicker.prototype={keyCode:{ALT:18,BLOQ_MAYUS:20,CTRL:17,DOWN:40,END:35,ENTER:13,HOME:36,LEFT:37,NUMPAD_ENTER:108,PAGE_DOWN:34,PAGE_UP:33,RIGHT:39,SHIFT:16,TAB:9,UP:38},_items:function(t,i){var r,a,o=this,s=e("<ul></ul>"),c=null;for(-1===t.options.timeFormat.indexOf("m")&&t.options.interval%60!==0&&(t.options.interval=60*Math.max(Math.round(t.options.interval/60),1)),r=i?n(i):t.options.startTime?n(t.options.startTime):n(t.options.startHour,t.options.startMinutes),a=new Date(r.getTime()+864e5);a>r;)o._isValidTime(t,r)&&(c=e("<li>").addClass("ui-menu-item").appendTo(s),e("<a>").addClass("ui-corner-all").text(e.fn.timepicker.formatTime(t.options.timeFormat,r)).appendTo(c),c.data("time-value",r)),r=new Date(r.getTime()+60*t.options.interval*1e3);return s.children()},_isValidTime:function(e,t){var i=null,r=null;return t=n(t),null!==e.options.minTime?i=n(e.options.minTime):null===e.options.minHour&&null===e.options.minMinutes||(i=n(e.options.minHour,e.options.minMinutes)),null!==e.options.maxTime?r=n(e.options.maxTime):null===e.options.maxHour&&null===e.options.maxMinutes||(r=n(e.options.maxHour,e.options.maxMinutes)),null!==i&&null!==r?t>=i&&r>=t:null!==i?t>=i:null!==r?r>=t:!0},_hasScroll:function(){var e="undefined"!=typeof this.ui.prop?"prop":"attr";return this.ui.height()<this.ui[e]("scrollHeight")},_move:function(e,t,i){var n=this;if(n.closed()&&n.open(e),!n.active)return void n.activate(e,n.viewport.children(i));var r=n.active[t+"All"](".ui-menu-item").eq(0);r.length?n.activate(e,r):n.activate(e,n.viewport.children(i))},register:function(t,i){var n=this,r={};r.element=e(t),r.element.data("TimePicker")||(r.options=e.metadata?e.extend({},i,r.element.metadata()):e.extend({},i),r.widget=n,e.extend(r,{next:function(){return n.next(r)},previous:function(){return n.previous(r)},first:function(){return n.first(r)},last:function(){return n.last(r)},selected:function(){return n.selected(r)},open:function(){return n.open(r)},close:function(){return n.close(r)},closed:function(){return n.closed(r)},destroy:function(){return n.destroy(r)},parse:function(e){return n.parse(r,e)},format:function(e,t){return n.format(r,e,t)},getTime:function(){return n.getTime(r)},setTime:function(e,t){return n.setTime(r,e,t)},option:function(e,t){return n.option(r,e,t)}}),n._setDefaultTime(r),n._addInputEventsHandlers(r),r.element.data("TimePicker",r))},_setDefaultTime:function(t){"now"===t.options.defaultTime?t.setTime(n(new Date)):t.options.defaultTime&&t.options.defaultTime.getFullYear?t.setTime(n(t.options.defaultTime)):t.options.defaultTime&&t.setTime(e.fn.timepicker.parseTime(t.options.defaultTime))},_addInputEventsHandlers:function(t){var i=this;t.element.bind("keydown.timepicker",function(e){switch(e.which||e.keyCode){case i.keyCode.ENTER:case i.keyCode.NUMPAD_ENTER:e.preventDefault(),i.closed()?t.element.trigger("change.timepicker"):i.select(t,i.active);break;case i.keyCode.UP:t.previous();break;case i.keyCode.DOWN:t.next();break;default:i.closed()||t.close(!0)}}).bind("focus.timepicker",function(){t.open()}).bind("blur.timepicker",function(){setTimeout(function(){t.element.data("timepicker-user-clicked-outside")&&t.close()})}).bind("change.timepicker",function(){t.closed()&&t.setTime(e.fn.timepicker.parseTime(t.element.val()))})},select:function(t,i){var n=this,r=t===!1?n.instance:t;n.setTime(r,e.fn.timepicker.parseTime(i.children("a").text())),n.close(r,!0)},activate:function(e,t){var i=this,n=e===!1?i.instance:e;if(n===i.instance){if(i.deactivate(),i._hasScroll()){var r=t.offset().top-i.ui.offset().top,a=i.ui.scrollTop(),o=i.ui.height();0>r?i.ui.scrollTop(a+r):r>=o&&i.ui.scrollTop(a+r-o+t.height())}i.active=t.eq(0).children("a").addClass("ui-state-hover").attr("id","ui-active-item").end()}},deactivate:function(){var e=this;e.active&&(e.active.children("a").removeClass("ui-state-hover").removeAttr("id"),e.active=null)},next:function(e){return(this.closed()||this.instance===e)&&this._move(e,"next",".ui-menu-item:first"),e.element},previous:function(e){return(this.closed()||this.instance===e)&&this._move(e,"prev",".ui-menu-item:last"),e.element},first:function(e){return this.instance===e?this.active&&0===this.active.prevAll(".ui-menu-item").length:!1},last:function(e){return this.instance===e?this.active&&0===this.active.nextAll(".ui-menu-item").length:!1},selected:function(e){return this.instance===e&&this.active?this.active:null},open:function(t){var n=this,r=t.getTime(),a=t.options.dynamic&&r;if(!t.options.dropdown)return t.element;switch(t.element.data("timepicker-event-namespace",Math.random()),e(i).bind("click.timepicker-"+t.element.data("timepicker-event-namespace"),function(e){t.element.get(0)===e.target?t.element.data("timepicker-user-clicked-outside",!1):t.element.data("timepicker-user-clicked-outside",!0).blur()}),(t.rebuild||!t.items||a)&&(t.items=n._items(t,a?r:null)),(t.rebuild||n.instance!==t||a)&&(e.fn.jquery<"1.4.2"?(n.viewport.children().remove(),n.viewport.append(t.items),n.viewport.find("a").bind("mouseover.timepicker",function(){n.activate(t,e(this).parent())}).bind("mouseout.timepicker",function(){n.deactivate(t)}).bind("click.timepicker",function(i){i.preventDefault(),n.select(t,e(this).parent())})):(n.viewport.children().detach(),n.viewport.append(t.items))),t.rebuild=!1,n.container.removeClass("ui-helper-hidden ui-timepicker-hidden ui-timepicker-standard ui-timepicker-corners").show(),t.options.theme){case"standard":n.container.addClass("ui-timepicker-standard");break;case"standard-rounded-corners":n.container.addClass("ui-timepicker-standard ui-timepicker-corners")}n.container.hasClass("ui-timepicker-no-scrollbar")||t.options.scrollbar||(n.container.addClass("ui-timepicker-no-scrollbar"),n.viewport.css({paddingRight:40}));var o=n.container.outerHeight()-n.container.height(),s=t.options.zindex?t.options.zindex:t.element.offsetParent().css("z-index"),c=t.element.offset();n.container.css({top:c.top+t.element.outerHeight(),left:c.left}),n.container.show(),n.container.css({left:t.element.offset().left,height:n.ui.outerHeight()+o,width:t.element.outerWidth(),zIndex:s,cursor:"default"});var u=n.container.width()-(n.ui.outerWidth()-n.ui.width());return n.ui.css({width:u}),n.viewport.css({width:u}),t.items.css({width:u}),n.instance=t,r?t.items.each(function(){var i,a=e(this);return i=e.fn.jquery<"1.4.2"?e.fn.timepicker.parseTime(a.find("a").text()):a.data("time-value"),i.getTime()===r.getTime()?(n.activate(t,a),!1):!0}):n.deactivate(t),t.element},close:function(t){var n=this;return n.instance===t&&(n.container.addClass("ui-helper-hidden ui-timepicker-hidden").hide(),n.ui.scrollTop(0),n.ui.children().removeClass("ui-state-hover")),e(i).unbind("click.timepicker-"+t.element.data("timepicker-event-namespace")),t.element},closed:function(){return this.ui.is(":hidden")},destroy:function(e){var t=this;return t.close(e,!0),e.element.unbind(".timepicker").data("TimePicker",null)},parse:function(t,i){return e.fn.timepicker.parseTime(i)},format:function(t,i,n){return n=n||t.options.timeFormat,e.fn.timepicker.formatTime(n,i)},getTime:function(t){var i=this,n=e.fn.timepicker.parseTime(t.element.val());return n instanceof Date&&!i._isValidTime(t,n)?null:n instanceof Date&&t.selectedTime?t.format(n)===t.format(t.selectedTime)?t.selectedTime:n:n instanceof Date?n:null},setTime:function(t,i,r){var a=this,o=t.selectedTime;if("string"==typeof i&&(i=t.parse(i)),i&&i.getMinutes&&a._isValidTime(t,i)){if(i=n(i),t.selectedTime=i,t.element.val(t.format(i,t.options.timeFormat)),r)return t}else t.selectedTime=null;return null===o&&null===t.selectedTime||(t.element.trigger("time-change",[i]),e.isFunction(t.options.change)&&t.options.change.apply(t.element,[i])),t.element},option:function(t,i,n){if("undefined"==typeof n)return t.options[i];var r,a,o=t.getTime();"string"==typeof i?(r={},r[i]=n):r=i,a=["minHour","minMinutes","minTime","maxHour","maxMinutes","maxTime","startHour","startMinutes","startTime","timeFormat","interval","dropdown"],e.each(r,function(i){t.options[i]=r[i],t.rebuild=t.rebuild||e.inArray(i,a)>-1}),t.rebuild&&t.setTime(o)}},e.TimePicker.defaults={timeFormat:"hh:mm p",minHour:null,minMinutes:null,minTime:null,maxHour:null,maxMinutes:null,maxTime:null,startHour:null,startMinutes:null,startTime:null,interval:30,dynamic:!0,theme:"standard",zindex:null,dropdown:!0,scrollbar:!1,change:function(){}},e.TimePicker.methods={chainable:["next","previous","open","close","destroy","setTime"]},e.fn.timepicker=function(t){if("string"==typeof t){var i,n,r=Array.prototype.slice.call(arguments,1);return i="option"===t&&arguments.length>2?"each":-1!==e.inArray(t,e.TimePicker.methods.chainable)?"each":"map",n=this[i](function(){var i=e(this),n=i.data("TimePicker");return"object"==typeof n?n[t].apply(n,r):void 0}),"map"===i&&1===this.length?e.makeArray(n).shift():"map"===i?e.makeArray(n):n}if(1===this.length&&this.data("TimePicker"))return this.data("TimePicker");var a=e.extend({},e.TimePicker.defaults,t);return this.each(function(){e.TimePicker.instance().register(this,a)})},e.fn.timepicker.formatTime=function(e,i){var n=i.getHours(),r=n%12,a=i.getMinutes(),o=i.getSeconds(),s={hh:t((0===r?12:r).toString(),"0",2),HH:t(n.toString(),"0",2),mm:t(a.toString(),"0",2),ss:t(o.toString(),"0",2),h:0===r?12:r,H:n,m:a,s:o,p:n>11?"PM":"AM"},c=e,u="";for(u in s)s.hasOwnProperty(u)&&(c=c.replace(new RegExp(u,"g"),s[u]));return c=c.replace(new RegExp("a","g"),n>11?"pm":"am")},e.fn.timepicker.parseTime=function(){var t=[[/^(\d+)$/,"$1"],[/^:(\d)$/,"$10"],[/^:(\d+)/,"$1"],[/^(\d):([7-9])$/,"0$10$2"],[/^(\d):(\d\d)$/,"$1$2"],[/^(\d):(\d{1,})$/,"0$1$20"],[/^(\d\d):([7-9])$/,"$10$2"],[/^(\d\d):(\d)$/,"$1$20"],[/^(\d\d):(\d*)$/,"$1$2"],[/^(\d{3,}):(\d)$/,"$10$2"],[/^(\d{3,}):(\d{2,})/,"$1$2"],[/^(\d):(\d):(\d)$/,"0$10$20$3"],[/^(\d{1,2}):(\d):(\d\d)/,"$10$2$3"]],i=t.length;return function(r){var a=n(new Date),o=!1,s=!1,c=!1,u=!1,l=!1;if("undefined"==typeof r||!r.toLowerCase)return null;r=r.toLowerCase(),o=/a/.test(r),s=o?!1:/p/.test(r),r=r.replace(/[^0-9:]/g,"").replace(/:+/g,":");for(var m=0;i>m;m+=1)if(t[m][0].test(r)){r=r.replace(t[m][0],t[m][1]);break}return r=r.replace(/:/g,""),1===r.length?c=r:2===r.length?c=r:3===r.length||5===r.length?(c=r.substr(0,1),u=r.substr(1,2),l=r.substr(3,2)):(4===r.length||r.length>5)&&(c=r.substr(0,2),u=r.substr(2,2),l=r.substr(4,2)),r.length>0&&r.length<5&&(r.length<3&&(u=0),l=0),c===!1||u===!1||l===!1?!1:(c=parseInt(c,10),u=parseInt(u,10),l=parseInt(l,10),o&&12===c?c=0:s&&12>c&&(c+=12),c>24?r.length>=6?e.fn.timepicker.parseTime(r.substr(0,5)):e.fn.timepicker.parseTime(r+"0"+(o?"a":"")+(s?"p":"")):(a.setHours(c,u,l),a))}}()}()});
