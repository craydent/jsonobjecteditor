function selectMenu(name){
    document.querySelectorAll(`.active[data-panel]`).forEach(e=>{
        e.classList.remove('active');
    })
    var doms = document.querySelectorAll(`[data-panel="${name}"`);
    doms.forEach(d=>{
        d.classList.add('active');
    })
}



function init(){
    document.querySelectorAll('menu-nav-option').forEach(b=>{
        b.addEventListener('click',a=>{selectMenu(b.dataset.panel)})
        })

    var startOn = 'available';
        if(
    $('card-panel[data-panel="pending"').find('card-row').length){
        startOn = "pending";
    }
    selectMenu(startOn);
}


init();

function takeOnTask(itemid,type,name){
    var object;
    $.ajax({
        //url: `/API/save/3c298a37-519e-4066-9727-91ac865cb4e2`, 
        url: `/API/item/${type}/_id/${itemid}`,
        async: false,
        success: function(result){
            object = result.item
        }
    });
    //console.log('object',object);
    var data = {
        itemtype:'instance',
        //name:encodeURIComponent(object.name)+' &#8520;',
        name:object.name+' &#8520;',
        reference:[itemid],
        instance_type:type,
        date:today,
        created:new Date(),
        _id:cuid(),
        points:object.points,
        members:[user._id],
        approved:false,
        joeUpdated:new Date()
    }
    if(confirm(`Take ${type}\n${object.name}?`)){
        
        createInstance(data)
    }
    //console.log(data);
}
function createInstance(data){

    $.ajax({
        //url: `/API/save/3c298a37-519e-4066-9727-91ac865cb4e2`, 
        url: `/API/save/`,
        data:data, 
        success: function(result){
            window.location = window.location;
        }
    });
    //http://localhost:8099/API/save/3c298a37-519e-4066-9727-91ac865cb4e2
}

function getCookie(key) {
    var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
    return keyValue ? keyValue[2] : null;
}