

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*/---------------------------------------------------------
    Craydent LLC
	Copyright 2014 (http://craydent.com/joe)
    Dual licensed under the MIT or GPL Version 2 licenses.
	(http://craydent.com/license)
---------------------------------------------------------/*/
//render field - self.renderObjectPropFieldUI
//render list item - self.renderListItems()
//single field list item -renderFieldListItem()
if ($c.isMobile()) {
  document.getElementsByTagName('html')[0].className += ' touch joesmall-size';
} else {
  document.getElementsByTagName('html')[0].className += ' no-touch joelarge-size';
}

window._joeEditingHash = false;
var _webworkers = false;

var _joeworker;

if (!!window.Worker) {
  _webworkers = true;
}

var joe_web_dir = '//' + location.hostname + ':' + (location.port || (location.protocol == "https:" ? 443 : 80)) + "/JsonObjectEditor/";

if (location && location.origin == 'file://') {
  joe_web_dir = location.href.slice(0, location.href.lastIndexOf('/') + 1);
}

var  __docWriter = function __docWriter(src, type, cancelWrite) {
  var type = type || 'js';
  var html = '';

  switch (type.toLowerCase()) {
    case 'js':
    case 'javascript':
      html += '<script type="text/javascript" src="' + src + '"></script>';
      break;

    case 'css':
    case 'stylesheet':
      html += '<link href="' + src + '" rel="stylesheet" type="text/css">';
      break;
  }

  if (!cancelWrite) {
    document.write(html);
  }

  return html;
};

var __loadAdditionalFiles = function __loadAdditionalFiles() {
  var bonuses = '';

  if (typeof jQuery == 'undefined') {
    //console.log('loading jquery');

    /* bonuses+=
         __docWriter(joe_web_dir+"js/libs/jquery-1.11.3.min.js",'js')+
         __docWriter(joe_web_dir+"js/libs/jquery-ui.min.js");*/
    __docWriter(joe_web_dir + "js/libs/jquery-1.11.3.min.js", 'js') + __docWriter(joe_web_dir + "js/libs/jquery-ui.min.js");
  }

  bonuses += __docWriter(joe_web_dir + "js/libs/jquery.ui.touch-punch.min.js");

  if (typeof Craydent == 'undefined' || !Craydent.VERSION || Craydent.VERSION < '1.7.37') {
    bonuses += __docWriter(joe_web_dir + "js/libs/craydent-1.8.1.js", 'js', true);
  }
  /*
      bonuses+=
          __docWriter(joe_web_dir+'css/joe.css','css','css',true)
          +__docWriter(joe_web_dir+'js/ace/ace.js','js',true)
          +__docWriter(joe_web_dir+'js/plugins/tinymce.min.js','js',true);
  */


  document.write(bonuses);
};

function __require(file, callback) {
  var head = document.getElementsByTagName("head")[0];
  var script = document.createElement('script');
  script.src = file;
  script.type = 'text/javascript'; //real browsers

  script.onload = callback; //Internet explorer

  script.onreadystatechange = function () {
    if (this.readyState == 'complete') {
      callback();
    }
  };

  head.appendChild(script);
}
/*var __joeFieldTypes = [
    'text',
    'select',
    'code',
    'rendering',
    'date',
    'boolean',
    'geo',
    'image',
    'url',
    'objectList',
    'objectReference',
    'group',
    'content'
];*/
//__loadAdditionalFiles();


function JsonObjectEditor(specs) {
  var self = this;
  var htmlRef = document.getElementsByTagName('html')[0];
  self.initialDocumentTitle = document.title;
  $c.TEMPLATE_VARS.push({
    variable: '/textarea',
    value: '</textarea>'
  }, {
    variable: 'textarea',
    value: '<textarea>'
  }, {
    variable: 'SERVER',
    value: '//' + $c.SERVER
  });
  var initialized = false;
  var colCount = 1;
  var listMode = false;
  var gridMode = false;
  var tableMode = false,
      tableSpecs;
  var multiEdit = false;
  this.VERSION = '1.0.1';
  window._joes = window._joes || [];
  this.joe_index = window._joes.length;

  if (!window._joes.length) {
    window._joe = this;
  }

  this.Error = {
    log: [],
    add: function add(message, error, data) {
      var payload = {
        caller: arguments.callee.caller,
        callerargs: arguments.callee.caller.arguments,
        message: message,
        error: error,
        stack: new Error(error).stack,
        data: data,
        timestamp: new Date().toISOString(),
        _id: cuid()
      };
      self.Error.log.push(payload);
      logit('[error]: ' + message);
    },
    show: function show() {
      self.show(self.Error.log, {
        schema: {
          title: '${message}',
          idprop: '_id',
          listView: {
            title: '<joe-title>${message}</joe-title><joe-subtitle>${timestamp}</joe-subtitle>',
            listWindowTitle: 'Errors'
          },
          fields: ['error:code', 'message', 'stack:code', {
            name: 'data',
            type: 'content',
            run: function run(data, obj) {
              return '<div><pre>' + tryEval(data) + '</pre></div>';
            }
          }, 'callerargs:code', 'timestamp:guid', '_id']
        }
      });
    }
  };

  window._joes.push(this);

  this.Cache = {
    "static": {},
    list: [],
    lookup: {},
    clear: function clear() {
      self.Cache.lookup = {};
      self.Cache.list = [];
    },
    remove: function remove(id) {//self.cache.
    },
    get: function get(id, specs) {
      var cacheitem = self.Cache.lookup[id];

      if (cacheitem && typeof cacheitem.value == "function" && cacheitem.type == "callback") {
        return cacheitem.value(cacheitem.data, cacheitem.id);
      }

      if (!cacheitem) {
        //if(autoadd){
        var obj = self.search(id)[0] || false;

        if (obj) {
          self.Cache.add(obj, {
            id: id
          });
          return obj;
        } //}


        return false;
      }

      return cacheitem.value;
    },
    callback: function callback(id) {
      var cacheitem = self.Cache.lookup[id];

      if (typeof cacheitem.value == "function") {
        return cacheitem.value(cacheitem.data, cacheitem.id);
      } else {
        logit('cache item is not a function');
      }
    },
    add: function add(value, specs) {
      var specs = specs || {};
      var obj = $.extend({
        id: specs.id || cuid(),
        value: value,
        parent: self.joe_index
      }, specs);
      self.Cache.list.push(obj.id);
      self.Cache.lookup[obj.id] = obj;
      return obj;
    }
  };
  this.Cache.set = this.Cache.add;
  this.history = [];
  /*-------------------------------------------------------------------->
  	0 | CONFIG
  <--------------------------------------------------------------------*/

  var defaults = {
    localStorage: false,
    container: 'body',
    joeprofile: {
      lockedFields: ['joeUpdated'],
      hiddenFields: []
    },
    profiles: {},
    fields: {},
    schemas: {
      'rendering': {
        title: 'HTML Rendering',
        callback: function callback() {
          alert('yo');
        } //fields:['id','name','thingType','legs','species','weight','color','gender'],
        //_listID:'id',
        //_listTitle:'${name} ${species}'

      }
    },
    compact: false,
    useControlEnter: true,
    //to save
    useEscapeKey: false,
    //to close window
    autoInit: false,
    autosave: 2000,
    dynamicDisplay: $(window).height() < 800 && $c.isMobile() ? 12 : 20,
    sans: false,
    listSubMenu: true,
    documentTitle: false,
    //tmplate to set document title
    style: {
      inset: true,
      cards: false
    },
    speechRecognition: false
  };
  this.specs = $.extend({}, defaults, specs || {});

  if (this.specs.localStorage && typeof Storage === "undefined") {
    alert('no local storage');
    this.specs.LocalStorage = false;
  }

  this.Indexes = {
    _add: function _add(idprop, dataitem) {
      self.Indexes[idprop] = self.Indexes[idprop] || {};
      self.Indexes[idprop][dataitem[idprop]] = dataitem;
    },
    _usage: 0
  };
  this.Printer = {
    init: function init() {
      if (!document.querySelector('print-joe')) {
        document.body.appendChild(document.createElement("print-joe"));
      }
    },
    print: function print() {
      self.Printer.init();
      var elmnt = document.getElementsByTagName("joe-panel-content")[0];
      var cln = elmnt.cloneNode(true);
      document.querySelector('print-joe').innerHTML = '';
      document.querySelector('print-joe').appendChild(cln);
      var panelContent = document.querySelector('print-joe joe-panel-content');
      panelContent.className = 'joe-panel-content';
      var activeNodes = document.querySelectorAll('print-joe joe-panel-content .joe-content-section.active');

      if (activeNodes.length) {
        panelContent.innerHTML = '';

        for (var i = 0; i < activeNodes.length; i++) {
          panelContent.appendChild(activeNodes[i]);
        }
      }

      window.print();
      document.querySelector('print-joe').innerHTML = '';
    }
  };
  this.Data = {};
  this.Render = {}; //TODO: make current.clear a function
  //TODO: check for class/id selector

  this.container = $(this.specs.container);
  this.fields = this.specs.fields;
  this.Fields = {
    reset: function reset(propname) {
      var field = self.Fields.get(propname);
      self.current.object[propname] = field.reset;
      self.Fields.rerender(propname);
    }
  }; //configure schemas

  this.schemas = this.specs.schemas;
  /*
      this.schemas._function = {
          idprop:'name'
      };*/

  for (var s in _joe.schemas) {
    _joe.schemas[s].__schemaname = _joe.schemas[s].name = s;
    _joe.schemas[s]._id = cuid();
  }

  this.current = {}; //filters:{},

  this.current.clear = function () {
    /*|{
     featured:true,
     description:'cleans up variables that currently exist between a single JOE showings.',
     tags:'cleanup, reset',
     category:'core'
     }|*/
    self.current.list = null;
    self.current.subsets = null;
    self.current.subset = null;
    self.current.filters = {};
    self.current.fields = [];
    self.current.schema = null;
    self.current.object = null;
    self.current.reset = {};
    self.current.cache = {};
    self.current.title = null;
    self.current.keyword = '';

    if (_joes.length == 1) {
      document.title = self.initialDocumentTitle;
    }
  }; //profile


  this.defaultProfile = this.specs.defaultProfile || this.specs.joeprofile;
  this.current.profile = this.defaultProfile;
  this.ace_editors = {};

  this.Render.stripeColor = function (opt, colorfunc) {
    var color = self.propAsFuncOrValue(colorfunc || opt.stripecolor, opt, null, _jco());
    var title = '';

    if (color && $c.isObject(color)) {
      title = ' title="' + color.title + '" ';
      color = color.color;
    }

    var h = '<joe-stripe-color ' + title + (color && 'style="background-color:' + color + ';"' || '') + '></joe-stripe-color>';
    return h;
  };

  this.Render.bgColor = function (opt, colorfunc) {
    var color = self.propAsFuncOrValue(colorfunc || opt.bgcolor, opt, null, _jco());

    if (color && $c.isObject(color)) {
      color = color.color;
    }

    var h = '<joe-bg-color ' + (color && 'style="background-color:' + color + ';"' || '') + '></joe-bg-color>';
    return h;
  };

  this.Render.itemCheckbox = function (listItem, schema, specs) {
    /*|{
        description:'renders a checkbox for a lsititem',
        specs:,expander,gotoButton,itemMenu,schemaprop,nonclickable,bgcolor,stripecolor,action
        tags:'render, field list item, checkbox',
    }|*/
    var specs = specs || {};
    var checkbox = self.propAsFuncOrValue(self.getCascadingProp('checkbox', schema));
    var idprop = specs.idprop || self.getIDProp();

    if (checkbox) {
      var cbox_prop = checkbox;
      var cbox_percentage = null;
      var cbox_title = '';

      if ($.type(checkbox) == "object") {
        cbox_prop = checkbox.prop;
        cbox_title = checkbox.title || cbox_prop;
        cbox_percentage = self.propAsFuncOrValue(checkbox.percentage, listItem);
      }

      var cb_label = self.propAsFuncOrValue(checkbox.label, listItem);
      var checked = [true, 'true'].contains(listItem[cbox_prop]) ? 'checked' : '';
      checkbox = '<joe-checkbox title="' + cbox_title + '" ' + 'class="' + checked + '"' + 'onclick="_joe.checkItem(\'' + listItem[idprop] + '\',\'' + cbox_prop + '\',null,this)">' + (cbox_percentage !== false && checkbox.hasOwnProperty('percentage') ? '<joe-checkbox-percentage>' + Math.round(cbox_percentage * 100) + '%</joe-checkbox-percentage>' : '') + (cb_label && '<joe-checkbox-label>' + cb_label + '</joe-checkbox-label>' || '') + '</joe-checkbox>';
    } else {
      checkbox = '';
    }

    return checkbox;
  };
  /*-------------------------------------------------------------------->
  	1 | INIT
  <--------------------------------------------------------------------*/


  this.init = function (callback) {
    /*|{
     featured:true,
     description:'primary entry point into JOE, sets up the UI necessary',
     tags:'init, framework',
     category:'core'
     }|*/
    if (initialized) {
      return false;
    }

    beginLogGroup('JOE init');
    self.current.clear();
    var html = self.renderFramework(self.renderEditorHeader() + self.renderEditorContent() + self.renderEditorFooter());
    self.container.append(html);
    self.overlay = $('.joe-overlay[data-joeindex=' + self.joe_index + ']');
    self.panel = self.overlay.find('.joe-overlay-panel');
    self.initKeyHandlers();
    self.Speech.init(); //self.Autosave.init();

    self.readHashLink();
    window.addEventListener("hashchange", function (newH, oldH) {
      var useHash = $GET('!') || location.hash;

      if (!useHash || self.joe_index != 0 || !specs.useHashlink) {
        return false;
      }

      if (!window._joeEditingHash) {
        self.readHashLink();
      }
    }, false);
    var respond_timeout;
    $(window).on('resize', function () {
      if (self.resizeOk()) {
        clearTimeout(respond_timeout);
        respond_timeout = setTimeout(self.respond, 200);
      }
    });
    initialized = true;
    self.respond();
    endLogGroup();
    self.Components.init();
    callback && callback();
  };

  this.resizeOk = function () {
    if (!$c.isMobile()) {
      return true;
    }

    if (document.activeElement.tagName == "INPUT") {
      return false;
    }

    return true;
  };
  /*-------------------------------------------------------------------->
       INIT KEY HANDLERS
   <--------------------------------------------------------------------*/


  this.initKeyHandlers = function () {
    if (self.specs.useBackButton) {
      window.onkeydown = function (e) {
        var code = e.keyCode;
        var nonBackElements = ['input', 'select', 'textarea'];
        var isInputElement = nonBackElements.indexOf(e.target.tagName.toLowerCase()) != -1;

        if (code == 8) {
          //BACKBUTTON PRESSED
          if (isInputElement) {//return false;
          } else {
            self.goBack();
            return false;
          }
        } else if ([37, 39, 38, 40, 13, 16, 17, 27].indexOf(code) == -1) {
          //set focus for alphanumeric keys
          if (e.altKey) {
            //alt control
            switch (code) {
              case 70:
                //QUICKFIND
                self.quickFind();
                if (e.stopPropagation) e.stopPropagation();
                if (e.preventDefault) e.preventDefault();
                break;

              case 78:
                //QUICKADD
                self.quickAdd();
                if (e.stopPropagation) e.stopPropagation();
                if (e.preventDefault) e.preventDefault();
                break;

              case 80:
                //PRINT
                self.Printer.print();
                if (e.stopPropagation) e.stopPropagation();
                if (e.preventDefault) e.preventDefault();
                break;
            }
          } else {
            if (listMode) {
              var inSearchfield = false;

              if ($(document.activeElement) && $(document.activeElement)[0] != $('.joe-submenu-search-field')[0]) {
                self.overlay.find('.joe-submenu-search-field').focus();
                inSearchfield = true;
                $('.joe-panel-content-option.keyboard-selected').removeClass('keyboard-selected');
              }
            } else {
              //NOT LIST MODE, DETAILS MODE
              //is control key down
              if (e.ctrlKey || e.metaKey) {
                switch (code) {
                  case 83:
                    //look for control save
                    if (self.container.find('.joe-button.joe-quicksave-button').length) {
                      self.updateObject(null, null, true);
                      if (e.stopPropagation) e.stopPropagation();
                      if (e.preventDefault) e.preventDefault();
                    }

                    break;
                }
              }
            }
          }
        } else {
          var keyboardSelectOption = function keyboardSelectOption(selector, top) {
            $(selector + '.keyboard-selected').toggleClass('keyboard-selected');
            var el = $(selector).eq(keyboardSelectedIndex);
            el.addClass('keyboard-selected');
            self.overlay.find('.joe-submenu-search-field').blur(); // $('.joe-panel-content').scrollTop($('.joe-panel-content-option.keyboard-selected').offset().top);

            el[0].scrollIntoView(top); //var panel_content = self.overlay.find('.joe-panel-content');
            //panel_content.animate({ scrollTop: panel_content.scrollTop()-10 });
            //panel_content.scrollTop(panel_content.scrollTop()-10);
          };

          //38 up, 40 dn,13 enter,37 left, 39 right
          var autocompleteField = $('.joe-text-autocomplete.active').length;

          if (autocompleteField) {
            var sel = '.joe-text-autocomplete-option.visible' + '.keyboard-selected'; //$('.joe-text-autocomplete-option.visible').length();

            var keyboardSelected = $(sel)[0];
            var keyboardSelectedIndex = $(sel).length ? $(sel).index() : -1;

            switch (code) {
              case 38:
                //up
                keyboardSelectedIndex--;

                if (keyboardSelectedIndex > -1) {
                  keyboardSelectOption('.joe-text-autocomplete-option.visible');
                }

                break;

              case 40:
                //down
                keyboardSelectedIndex++;

                if (keyboardSelectedIndex < $('.joe-text-autocomplete-option.visible').length) {
                  keyboardSelectOption('.joe-text-autocomplete-option.visible');
                }

                break;

              case 13:
                //enter
                if (keyboardSelectedIndex != -1) {
                  keyboardSelected.click();
                }

                break;
            }
          }

          if (listMode) {
            if (e.altKey) {
              //alt control
              //get current index and add 1
              if (self.current.subsets) {
                var subindex = self.current.subsets.indexOf(self.current.subset) + 1; //$('joe-subset-option.active').index()+1;

                switch (code) {
                  case 38:
                    //up
                    $('joe-subset-option').eq(subindex - 1).click();
                    break;

                  case 40:
                    //down
                    $('joe-subset-option').eq(subindex + 1).click();
                    break;
                }
              }
            } else {
              var keyboardSelectedIndex = $('.joe-panel-content-option.keyboard-selected').length ? $('.joe-panel-content-option.keyboard-selected').index() : -1; //logit(keyboardSelectedIndex);

              switch (code) {
                case 38:
                  //up
                  keyboardSelectedIndex--;

                  if (keyboardSelectedIndex > -1) {
                    keyboardSelectOption('.joe-panel-content-option', top);
                  }

                  break;

                case 40:
                  //down
                  keyboardSelectedIndex++;

                  if (keyboardSelectedIndex < currentListItems.length) {
                    keyboardSelectOption('.joe-panel-content-option', top);
                  }

                  break;

                case 13:
                  //enter
                  if (keyboardSelectedIndex != -1) {
                    $('.joe-panel-content-option.keyboard-selected').find('.joe-panel-content-option-content').click();
                  }

                  break;
              }
            }
          } else {
            //DETAILS MODE
            if (e.altKey) {
              switch (code) {
                case 219: //left

                case 221:
                  //right
                  var sside = code == 219 ? 'left' : 'right';

                  if (self.current.sidebars[sside].content && !self.current.sidebars[sside].hidden) {
                    self.toggleSidebar(sside);
                  }

                  break;
              }
            } else if (e.ctrlKey) {
              switch (code) {
                case 37:
                  //left
                  self.previous();
                  break;

                case 39:
                  //right
                  self.next();
                  break;
              }
            } else if (code == 13) {
              //add another row on enter click
              var ae = $(document.activeElement);

              if (ae && ae.is('input')) {
                var fieldobj = ae.parents('.joe-object-field');

                if (fieldobj.length) {
                  try {
                    fieldobj.find('.joe-plus-button').click();
                  } catch (e) {
                    self.Error.add('adding new row to objectlist', e);
                  }
                }
              }
            }
          }
        }
      };
    }
  };
  /*-------------------------------------------------------------------->
  	2 | FRAMEWORK START
  <--------------------------------------------------------------------*/


  this.getMode = function () {
    if (listMode) {
      if (self.current.list) {
        return 'list';
      }
    }

    if (self.current.object) {
      return 'details';
    }

    if (self.current.list) {
      return 'list';
    } else {
      return false;
    }
    /*
    if(listMode){return 'list';}
    return 'details';*/

  };

  this.renderFramework = function (content) {
    var style = 'style-variant1';
    var html = '<joe-overlay class="joe-overlay sans cols-' + colCount + ' ' + style + ' ' + (self.specs.compact && ' compact ' || '') + (self.specs.sans && ' sans ' || '') + '" data-joeindex="' + this.joe_index + '">' + '<joe-panel class="joe-overlay-panel">' + (content || '') + '</joe-panel>' + //mini
    '<div class="joe-mini-panel">' + '</div>' + '</joe-overlay>';
    return html;
  };

  this.populateFramework = function (data, setts) {
    /*|{
     featured:true,
     description:'primary function for populating and rendering JOE, called by goJoe and joe.show',
     tags:'populate, framework, core'
     }|*/
    self.current.cache = {};
    self.overlay.removeClass('multi-edit');
    var joePopulateBenchmarker = new Benchmarker();
    self.current.reset = self.current.reset || {};
    beginLogGroup('JOE population'); //logit('------Beginning joe population');

    var specs = setts || {};
    self.current.specs = setts;
    self.current.data = data; //clean copy for later;

    self.current.userSpecs = $.extend({}, setts);
    gridMode = self.current.specs.viewMode == 'grid' ? true : false;
    tableMode = self.current.specs.viewMode == 'table' ? true : false; //update history 1/2

    if (!self.current.specs.noHistory) {
      self.history.push({
        /*			_joeHistoryTitle:self.overlay.find('.joe-panel-title').html(),
        */
        specs: self.current.userSpecs,
        data: self.current.data
      });
    }

    var schema = setts.schema || '';
    var profile = setts.profile || null;
    var callback = setts.callback || null;
    var datatype = setts.datatype || '';
    var title = setts.title || ''; //callback

    if (callback) {
      self.current.callback = callback;
    } else {
      self.current.callback = null;
    }
    /*-------------------------
     String data
     -------------------------*/


    if ($.type(data) == 'string' && datatype != "string" && self.getDataset(data, {
      "boolean": true
    })) {
      if (!specs.schema && self.schemas[data]) {
        schema = data;
      }

      data = self.getDataset(data);
    }
    /*setup schema*/


    specs.schema = this.setSchema(schema);
    /*-------------------------
    Column Count
    -------------------------*/

    colCount = self.current.specs.colCount || specs.schema && specs.schema.colCount
    /*|| colCount*/
    || 1;

    if (self.sizeClass == "small-size") {
      colCount = 1;
    }
    /*-------------------------
        Preformat Functions
    -------------------------*/


    specs.preformat = specs.schema && specs.schema.preformat || specs.preformat || function (d) {
      return d;
    };

    data = specs.preformat(data);
    /*-------------------------
    	Object
    -------------------------*/
    //when object passed in

    if ($.type(data) == 'object' || datatype == 'object') {
      specs.object = data;
      specs.menu = specs.menu || specs.schema && specs.schema.menu || self.specs.menu || specs.multiedit && __defaultMultiButtons || __defaultObjectButtons;
      specs.mode = "object";
      self.current.object = data;
    }
    /*-------------------------
     MultiEdit (Arrays)
     -------------------------*/


    self.toggleMultiEditMode(specs, data);
    /*-------------------------
    	Lists (Arrays)
    -------------------------*/
    //when array passed in

    listMode = false;

    if ($.type(data) == 'array' || datatype == 'array') {
      listMode = true;
      specs.list = data;
      specs.menu = specs.listMenu || specs.schema && specs.schema.listMenu || __defaultButtons; //__defaultMultiButtons;

      specs.mode = "list"; //TODO: filter list items here.

      self.current.list = data;
      /*-------------------------
       Subsets
       -------------------------*/

      var currentSubsets; //setup subsets

      currentSubsets = setts.subsets || specs.schema && specs.schema.subsets || null;

      if (typeof currentSubsets == 'function') {
        currentSubsets = currentSubsets();
      } //a current subset selected


      if (self.current.specs.subset && currentSubsets &&
      /*currentSubsets.where({name:specs.subset})*/
      currentSubsets.where({
        $or: [{
          name: specs.subset
        }, {
          group_start: specs.subset
        }]
      }).length) {
        self.current.subset = currentSubsets.where({
          $or: [{
            name: specs.subset
          }, {
            group_start: specs.subset
          }]
        })[0] || false;
      } else {
        //all selected
        if (self.current.specs.subset == "All") {
          self.current.subset = {
            name: "All",
            filter: {}
          };
        } else {
          //select default subset if it exists
          self.current.subset = currentSubsets && currentSubsets.where({
            'default': true
          })[0] || null;
        }
      }

      self.current.subsets = currentSubsets;
      /*-------------------------
       Sorting
       -------------------------*/
      //setup sorting

      self.current.sorter = setts.sorter || self.current.subset && self.current.subset.sorter || specs.schema && specs.schema.sorter || 'name';

      if ($.type(self.current.sorter) == 'string') {
        self.current.sorter = [self.current.sorter];
      } //self.current.object = null;

    }
    /*-------------------------
     Submenu
     -------------------------*/


    if (specs.mode == 'list') {
      self.current.submenu = self.current.specs.listsubmenu || self.current.specs.submenu || specs.schema && specs.schema.listSubMenu || self.specs.listSubMenu;
    } else {
      self.current.submenu = self.current.specs.submenu || specs.schema && specs.schema.subMenu || self.specs.subMenu;
    }

    if (self.current.submenu == 'none') {
      self.current.submenu = null;
    }
    /*-------------------------
    	Rendering
    -------------------------*/
    //when rendering passed in


    if ($.type(data) == 'string' && datatype == 'rendering') {
      specs.rendering = data;
      specs.menu = [__replaceBtn__];
      specs.mode = "rendering";
      self.current.rendering = specs.rendering; //specs.schema
    }
    /*-------------------------
    	String
    -------------------------*/
    //when string passed in
    else if ($.type(data) == 'string' || datatype == 'string') {
        specs.text = data;
        specs.menu = __defaultButtons; //specs.menu = [{name:'save',label:'Save Object',action:'_joe.updateObject()'}];

        specs.mode = "text";
        self.current.text = specs.text;
      } //setup window title
    //specs.title = title || (specs.schema)? specs.schema._title : "Viewing "+specs.mode.capitalize();


    specs.listWindowTitle = specs.list && (specs._listMenuTitle || //specs.listWindowTitle ||
    specs._listWindowTitle || self.getCascadingProp('listWindowTitle') || getProperty('specs.list.windowTitle') || specs.schema && (specs.schema._listMenuTitle || specs.schema._listWindowTitle)) || false;
    specs.title = specs.listWindowTitle || title || specs.schema && (specs.schema.title || specs.schema._title) || "Viewing " + (self.current.schema && self.current.schema.__schemaname || typeof self.current.userSpecs.schema == 'string' && self.current.userSpecs.schema || specs.mode).capitalize(); //setup profile

    specs.profile = profile ? self.specs.profiles[profile] || self.specs.joeprofile : self.specs.joeprofile;
    self.current.profile = specs.profile; //cleanup variables

    self.cleanUp();
    /*-------------------------------------------------------------------->
     Set global view mode specs
     <--------------------------------------------------------------------*/

    if (self.current.schema && (self.current.schema.table || self.current.schema.tableView)) {
      tableSpecs = $.extend({
        cols: ['name', self.getIDProp()]
      }, self.current.schema && (self.current.schema.table || self.current.schema.tableView) // &&(self.current.schema.table||self.current.schema.tableView).cols
      || {});
    } else {
      tableSpecs = null;
    }
    /*-------------------------------------------------------------------->
     Framework Rendering
     <--------------------------------------------------------------------*/


    var contentBM = new Benchmarker();
    beginLogGroup('Content');
    var content = self.renderEditorContent(specs);
    endLogGroup();

    _bmResponse(contentBM, 'JOE [Content]');

    var chromeBM = new Benchmarker();
    var html = self.renderEditorHeader(specs) + self.renderEditorSubmenu(specs) + content + self.renderEditorFooter(specs) + self.renderMessageContainer();

    _bmResponse(chromeBM, 'JOE [overlay-chrome]');

    self.overlay.find('.joe-overlay-panel').html(html); //$('.joe-overlay-panel').html(html);
    //update history 2/2	- add title

    if (!self.current.specs.noHistory && self.history.length) {
      $.extend(self.history[self.history.length - 1], {
        _joeHistoryTitle: self.overlay.find('.joe-panel-title').html()
      });
    } //clear ace_editors

    /*        for (var p in _joe.ace_editors){
                _joe.ace_editors[p].destroy();
            }
            _joe.ace_editors = {};*/
    //update hashlink


    self.updateHashLink(); //logit('Joe Populated in '+joePopulateBenchmarker.stop()+' seconds');

    _bmResponse(joePopulateBenchmarker, '----Joe Populated');

    endLogGroup();
    return html;
  };
  /*-------------------------------------------------------------------->
   2e | FRAMEWORK END
   <--------------------------------------------------------------------*/


  this.toggleMultiEditMode = function (specs, data) {
    multiEdit = self.current.userSpecs && self.current.userSpecs.multiedit || false;
  };
  /*----------------------------->
  	A | Header
  <-----------------------------*/


  function createTitleObject(specs) {
    var specs = specs || {};
    var titleObj = $.extend({}, self.current.object);
    var list = specs.list || self.current.list;

    if (list) {
      var lcount = list.length;

      if (self.current.subset) {
        lcount = list.where(self.current.subset.filter).length;
      }

      titleObj._listCount = lcount || '0';
      titleObj._subsetName = self.current.subset && self.current.subset.name + ' ' || '';
    }

    self.current.title = specs.title || self.getCascadingProp('title') || self.current.schema && "new " + self.current.schema.__schemaname || 'Json Object Editor';
    self.current.subtitle = specs.subtitle || self.getCascadingProp('subtitle');
    var title = fillTemplate(self.propAsFuncOrValue(self.current.title), titleObj);
    var subtitle = fillTemplate(self.propAsFuncOrValue(self.current.subtitle), titleObj);
    titleObj.docTitle = title;
    titleObj.subTitle = subtitle;
    return titleObj;
  }

  this.Header = {};

  this.toggleHelpMenu = function (show, target) {};

  this.Header.Render = this.renderEditorHeader = function (specs) {
    var BM = new Benchmarker();
    var specs = specs || {};
    var titleObj = createTitleObject(specs);
    var title = titleObj.docTitle || self.current.schema && "new " + self.current.schema.__schemaname;
    var subtitle = titleObj.subTitle; //show doctitle

    if (self.specs.documentTitle) {
      var doctitle = self.specs.documentTitle === true ? self.current.title : self.propAsFuncOrValue(self.specs.documentTitle, self.current.title);
      document.title = fillTemplate(self.propAsFuncOrValue(doctitle), titleObj);
    }

    var close_action = specs.close_action || 'onclick="getJoe(' + self.joe_index + ').closeButtonAction()"';
    var reload_action = specs.reload_action || 'onclick="getJoe(' + self.joe_index + ').reload()"';

    function renderHeaderBackButton() {
      var html = '';

      if (self.history.length > 1
      /*&& specs.useHeaderBackBtn*/
      ) {
          html += '<div class="jif-header-back-btn jif-panel-header-button standard-button" onclick="window.history.back()" title="back">' + self.SVG.icon.left //+'<span class="jif-arrow-left"></span>'
          + '</div>';
        }

      return html;
    }

    function renderHelpButton() {
      var html = '';

      if (specs.minimode) {
        return '';
      }

      html += '<div class="jif-panel-header-button joe-panel-help" onclick="_joe.toggleHelpMenu()" title="help">' + self.SVG.icon.help + '</div>';
      return html;
    }

    var help_button = renderHelpButton();

    function renderUnsavedIcon() {
      var html = '';

      if (specs.minimode) {
        return '';
      }

      html += '<div class="jif-panel-header-button joe-panel-unsaved" onclick="_joe.updateObject(this,null,true);" title="unsaved changes">' + self.SVG.icon.unsaved + '</div>';
      return html;
    }

    var unsaved_icon = renderUnsavedIcon();
    var speech_action = specs.speech_action || 'onclick="getJoe(' + self.joe_index + ').Speech()"';
    var reload_button = specs.minimode ? '' : '<div class="jif-panel-header-button joe-panel-reload" title="reload" ' + reload_action + '><span class="jif-reload"></span></div>';
    var mic_button = !self.specs.speechRecognition || specs.minimode ? '' : '<div class="jif-panel-header-button joe-panel-speech" title="speech" id="speech-button-' + self.joe_index + '">M</div>';
    var close_button = '<div class="jif-panel-header-button joe-panel-close" title="close" ' + close_action + '>' + self.SVG.icon.close //+'<span class="jif-close"></span>'+
    + '</div>'; // var schema_button = (!specs.minimode && (self.current.schema && self.current.schema.menuicon && 
    //         '<joe-schema-icon class="clickable" title="'+self.current.schema.__schemaname+'" onclick="goJoe(_joe.getDataset(\''+self.current.schema.__schemaname+'\'),{schema:\''+self.current.schema.__schemaname+'\'})">'+self.current.schema.menuicon+'</joe-schema-icon>')||'');

    var schema_button = !specs.minimode && self.current.schema && self.current.schema.menuicon && '<joe-schema-icon class="clickable" title="' + self.current.schema.__schemaname + '" onclick="goJoe(\'' + self.current.schema.__schemaname + '\')">' + self.current.schema.menuicon + '</joe-schema-icon>' || '';
    var back_button = !specs.minimode && renderHeaderBackButton() || '';
    var left_buttons = [back_button, schema_button];
    var right_buttons = [close_button, mic_button, reload_button, //help_button, TODO: add help button.
    unsaved_icon];
    var html = //'<div class="joe-panel-header">'+
    '<joe-panel-header >' + (specs.schema && specs.schema.subsets && self.renderSubsetselector(specs.schema) || specs.subsets && self.renderSubsetselector(specs) || '') + //'<joe-panel-header-buttons class="left-side">'+
    left_buttons.join(' ') + //'</joe-panel-header>'+
    '<div class="joe-vcenter joe-panel-title-holder"><span class="joe-panel-title">' + (('<div>' + title + '</div>').toDomElement().innerText || title || 'Json Object Editor') + '</span></div>' + //'<div class="joe-panel-reload joe-panel-header-button" title="reload" '+reload_action+'></div>'+
    '<joe-panel-header-buttons class="right-side">' + right_buttons.join(' ') + '<div class="clear"></div>' + '</joe-panel-header-buttons>' + '<div class="clear"></div>' + '</joe-panel-header>';

    _bmResponse(BM, '[Header] rendered');

    return html;
  }; //What happens when the user clicks the close button.


  this.closeButtonAction = function (prechecked) {
    if (!this.checkChanges()) {
      return;
    }

    self.history = [];
    self.panel.addClass('centerscreen-collapse');
    self.hide(500);
    self.current.clear();
    $(self.container).trigger({
      type: "hideJoe",
      index: self.joe_index
      /*,
      schema: self.current.specs.schema,
      subset: self.current.specs.subset*/

    });
    self.updateHashLink();
    var closeAction = self.getCascadingProp('onPanelHide');

    if (closeAction) {
      closeAction(self.getState());
    }

    self.Autosave.deactivate();
  };

  var goingBackFromID;
  var goingBackQuery;

  this.goBack = function (obj) {
    if (!this.checkChanges()) {
      return;
    } //go back to last item and highlight


    if (self.current.object) {
      var gobackItem = self.current.object[self.getIDProp()];

      if (gobackItem) {
        goingBackFromID = gobackItem; // logit(goingBackFromID);
      }
    } //clearTimeout(self.searchTimeout );


    self.history.pop();
    var joespecs = self.history.pop();

    if (!joespecs) {
      self.closeButtonAction(true);
      return;
    } else {
      if (obj && $c.isArray(joespecs.data)) {
        var objid = obj[self.getIDProp()];

        if (objid) {
          var query = {};
          query[self.getIDProp()] = objid;
          var found = joespecs.data.where(query);
          found.length && $.extend(found[0], obj);
        }
      }

      if (joespecs.keyword) {
        goingBackQuery = joespecs.keyword;
      }
    }

    var specs = $.extend({}, joespecs.specs);
    specs.filters = joespecs.filters;
    self.show(joespecs.data, specs);
  };
  /*    this.clearAuxiliaryData = function(){
          /!*|{
           featured:true,
           description:'cleans up variables that currently exist between a single JOE showings.',
           tags:'cleanup, reset',
           category:'core'
           }|*!/
          self.current.list = null;
          self.current.subsets = null;
          self.current.subset = null;
          self.current.filters = {};
          self.current.fields = [];
          self.current.schema = null;
          self.current.object = null;
          self.current.reset = {};
  
      };*/
  //this.current.clear = this.clearAuxiliaryData;


  this.cleanUp = function () {
    /*|{
     featured:true,
     description:'cleans up variables that currently exist between a single JOE showings.',
     tags:'cleanup, reset',
     category:'core'
     }|*/

    /*        if(!self._reloading){
                self.current.reset = {};
            }*/
    self.current.fields = [];
    self.shiftSelecting = false;
    self.allSelected = false;
    /*        for (var p in _joe.ace_editors){
                _joe.ace_editors[p].destroy();
            }
            _joe.ace_editors = {};*/

    if (self.current.userSpecs && self.current.userSpecs.multiedit) {
      self.overlay.addClass('multi-edit');
    } else {
      self.overlay.removeClass('multi-edit');
    }
  };
  /*----------------------------->
   B | SubMenu
   <-----------------------------*/


  function renderSubmenuButtons(buttons) {
    var html = ''; //<div class="joe-submenu-buttons">';

    var buttons = self.propAsFuncOrValue(buttons || listMode && self.getCascadingProp('headerListMenu') || !listMode && self.getCascadingProp('headerMenu'));
    var content;

    if (buttons) {
      var h = self.renderMenuButtons(buttons, 'joe-submenu-button');

      if (h) {
        content = true;
      }

      html += h;
    }

    return {
      html: html,
      content: content
    }; // return {html:html +'</div>',content:content};
  }

  ; //self.current.submenuButtons;

  this.renderEditorSubmenu = function (specs) {
    beginLogGroup('Submenu');
    var BM = new Benchmarker();
    var sectionAnchors = renderSectionAnchors(); //submenu buttons

    var submenuButtons = renderSubmenuButtons();

    if (!self.current.submenu && !sectionAnchors.count && !listMode && !self.getCascadingProp('headerMenu')) {
      return '';
    }

    var showFilters = $c.getProperty(self.current, 'userSpecs.filters') || $c.getProperty(self.current, 'schema.filters') || $c.getProperty(self.current, 'schema.subsets') || false;
    var subSpecs = {
      search: true,
      itemcount: true,
      filters: showFilters,
      numCols: self.current.schema.columns || 0
    };
    var userSubmenu = $.type(self.current.submenu) != 'object' ? {} : self.current.submenu;
    $.extend(subSpecs, userSubmenu);

    if (specs.mode == 'list') {
      var filters_obj = self.Filter.getCached();
      var submenu = '<div class="joe-panel-submenu">' //right side
      +self.Submenu.Aggregator.renderToggleButton()
      + self.renderViewModeButtons(subSpecs) + self.renderColumnCountSelector(subSpecs.numCols) + self.renderSorter() //left side
      + (subSpecs.filters && self.renderSubmenuFilters(subSpecs.filter) || '') + (subSpecs.search && self.renderSubmenuSearch(subSpecs.search) || '') + (subSpecs.itemcount && self.renderSubmenuItemcount(subSpecs.itemcount) || '') + submenuButtons.html + '</div>' + "<div class='joe-filters-holder'>" + renderSubsetsDiv() + (filters_obj && renderFiltersDiv() || '') // +'<span class="jif-arrow-left"></span>'
      + "</div>";
    } else {
      var submenu = '<div class="joe-panel-submenu">' + renderSidebarToggle('left') + renderSidebarToggle('right') + submenuButtons.html + (sectionAnchors.count && sectionAnchors.code || '') + '</div>';
      /*
      + "<div class='joe-filters-holder'>"
      // + renderSubsetsDiv()
      //+ (self.current.schema && (self.propAsFuncOrValue(self.current.schema.filters) && renderFiltersDiv()) || '')
      // +'<span class="jif-arrow-left"></span>'
      + "</div>";*/
    }

    function renderOptStripeColor(opt) {
      var color = self.propAsFuncOrValue(opt.stripecolor || opt.stripeColor, null, null, opt);
      var title = '';

      if (color && $c.isObject(color.title)) {
        title = ' title="' + color.title + '" ';
        color = color.color;
      }

      var h = '<joe-stripe-color ' + title + (color && 'style="background-color:' + color + ';"' || '') + '></joe-stripe-color>';
      return h;
    }

    function renderOptBGColor(opt) {
      var color = self.propAsFuncOrValue(opt.bgcolor || opt.bgColor, null, null, opt);

      if (color && $c.isObject(color.title)) {
        color = color.color;
      }

      var h = '<joe-bg-color ' + (color && 'style="background-color:' + color + ';"' || '') + '></joe-bg-color>';
      return h;
    }

    function renderOptBorderColor(opt) {
      var color = self.propAsFuncOrValue(opt.bordercolor || opt.borderColor, null, null, opt);

      if (color && $c.isObject(color.title)) {
        color = color.color;
      }

      var h = '<joe-border-color ' + (color && 'style="border:1px solid ' + color + ';"' || '') + '></joe-border-color>';
      return h;
    }

    function renderOptionGroupStart(opt, childDom) {
      var collapsed = self.propAsFuncOrValue(opt.collapsed) ? 'collapsed' : '';

      if (!opt.hasOwnProperty('filter')) {
        var html = '<joe-option-group id="' + opt.group_start + '-group" class="' + collapsed + '"><joe-menu-label onclick="$(this).parent().toggleClass(\'collapsed\');">' + (opt.display || opt.name || opt.group_start) + '</joe-menu-label>';
        return html;
      } else {
        var optdisplay = opt.display || opt.name || opt.group_start || '';
        var optname = opt.id || opt.name || opt.group_start || '';
        var cbox = '',
            act,
            onclick;
        var dataname;

        if (childDom == 'joe-filter-option') {
          dataname = ' data-filter="' + optname + '" ';
          cbox = '<span class ="joe-option-checkbox"></span>';
          act = self.current.filters && self.current.filters[optname] ? 'active' : '';
          onclick = 'getJoe(' + self.joe_index + ').toggleFilter(\'' + optname + '\',this);_joe.Utils.stopPropagation();';
        } else {
          dataname = ' data-subset="' + optname + '" ';
          act = self.current.subset && self.current.subset.name == optname || !self.current.subset && opt.name == "All" ? 'active' : '';
          onclick = 'getJoe(' + self.joe_index + ').selectSubset(\'' + optname.toString().replace(/\'/g, "\\'") + '\');';
        }

        var optiondiv = '<' + childDom + '  ' + dataname + ' class="' + childDom + ' joe-group-option ' + act + ' " onclick=' + onclick + '>' + renderOptBorderColor(opt) + renderOptBGColor(opt) + renderOptStripeColor(opt) + cbox + '<span style="position:relative;">' + optdisplay + '</span>' + '</' + childDom + '>';
        var html = '<joe-option-group id="' + opt.group_start + '-group" class="' + collapsed + ' ">' + '<joe-menu-label onclick="$(this).parent().toggleClass(\'collapsed\');">' + optiondiv + '</joe-menu-label>';
        return html;
      }
    } //TODO:move to subsets filter rendering function


    function renderSubsetsDiv() {
      var sh = '<div><joe-menu-label>Subsets</joe-menu-label>';
      var act;
      var starting = [];

      if (!self.current.schema || !self.current.schema.hideAllSubset) {
        starting.push({
          name: 'All',
          filter: {}
        });
      }

      var group = null;
      starting.concat(_joe.current.subsets || []).map(function (opt) {
        if (opt.group_start) {
          group = opt.group_start;
          sh += renderOptionGroupStart(opt, 'joe-subset-option');
        } else if (opt.group_end) {
          sh += '</joe-option-group>';
          group = null;
        } else {
          var idval = opt.id || opt.name || ''; //if(!opt.condition || (typeof opt.condition == 'function' && opt.condition(self.current.object)) || (typeof opt.condition != 'function' && opt.condition)) {

          if (!opt.condition || self.propAsFuncOrValue(opt.condition)) {
            act = self.current.subset && self.current.subset.name == opt.name || !self.current.subset && opt.name == "All" ? 'active' : '';

            if (act == 'active' && group) {
              sh = sh.replace('<joe-option-group id="' + group + '-group" class="collapsed', '<joe-option-group id="' + group + '-group" class="');
            } // sh += '<joe-subset-option  data-subset="'+idval+'" class="joe-subset-option ' + act + ' " onclick="getJoe(' + self.joe_index + ').selectSubset(\'' 
            // + (opt.id || opt.name || '').toString().replace(/\'/g,"\\'") + '\');">' 
            // +renderOptBorderColor(opt)
            // +renderOptBGColor(opt)
            // +renderOptStripeColor(opt)
            // + '<span style="position:relative;">'+opt.name+'</span>' 
            // + '</joe-subset-option>';


            sh += "<joe-subset-option  data-subset=\"" + idval + "\" class=\"joe-subset-option " + act + " \" \n                            onclick=\"getJoe(" + self.joe_index + ").selectSubset('" + (opt.id || opt.name || '').toString().replace(/\'/g, "\\'") + "');\">" + renderOptBorderColor(opt) + renderOptBGColor(opt) + renderOptStripeColor(opt) + '<span style="position:relative;">' + opt.name + '</span>' + '</joe-subset-option>';
          }
        }
      }); //+fillTemplate('<div class="joe-filters-subset">${name}</div>',_joe.current.subsets||[])

      sh += '</div>';

      _bmResponse(BM, 'subsets rendered');

      return sh;
    }

    function renderFiltersDiv() {
      _bmResponse(BM, 'filters start');

      var fh = '<div><joe-menu-label>Filters</joe-menu-label>';
      var filters = self.current.schema.filters;
      filters = self.propAsFuncOrValue(filters);
      var group = null;
      (filters || []).map(function (opt) {
        if (opt.group_start) {
          group = opt.group_start;
          fh += renderOptionGroupStart(opt, 'joe-filter-option');
        } else if (opt.group_end) {
          fh += '</joe-option-group>';
          group = null;
        } else {
          var idval = opt.id || opt.name || '';

          if (!opt.condition || self.propAsFuncOrValue(opt.condition)) {
            act = '';

            if (self.current.filters && self.current.filters[idval]) {
              act = 'active';
            } else if (opt.hasOwnProperty('default')) {
              act = self.propAsFuncOrValue(opt["default"]) ? 'active' : '';
              self.toggleFilter(opt.name);
            }

            if (act == 'active' && group) {
              fh = fh.replace('<joe-option-group id="' + group + '-group" class="collapsed">', '<joe-option-group id="' + group + '-group" class="">');
            }

            fh += '<joe-filter-option data-filter="' + idval + '" class="joe-filter-option ' + act + '" onclick="getJoe(' + self.joe_index + ').toggleFilter(\'' + idval + '\',this);">' + renderOptBorderColor(opt) + renderOptBGColor(opt) + renderOptStripeColor(opt) + '<span class ="joe-option-checkbox"></span>' + '<span style="position:relative;">' + opt.name + '</span>' + '</joe-filter-option>';
          }
        }
      });
      fh += '<joe-subset-option class="joe-subset-option clear-filters" onclick="' + getSelfStr + '.clearFilters()">clear filters</joe-subset-option>';
      fh += '</div>';

      _bmResponse(BM, 'filters rendered');

      return fh;
    }

    endLogGroup();

    _bmResponse(BM, '[Submenu] rendered');

    return submenu;
  };

  function renderSectionAnchors() {
    var anchorhtml = '<joe-submenu-section-anchors>' + '<joe-panel-button class="sections-toggle" onclick="' + getSelfStr + '.Sections.toggle(this);">' + self.SVG.icon.sections + ' <joe-button-label>' + (self.current.sections && self.current.sections.count() || '') + ' sections</joe-button-label></joe-panel-button>';
    anchorhtml += '<div class="joe-submenu-section" onclick="_joe.Sections.gotoTop(' + self.joe_index + ')">^ top</div>';
    var scount = 0;
    var onclick = 'onclick="getJoe(' + self.joe_index + ').gotoSection(\'${id}\');"';
    var ondblclick = 'ondblclick="getJoe(' + self.joe_index + ').gotoSection(\'${id}\',{dblclickdom:this});"';
    var template = //'<div class="joe-submenu-section" onclick="$(\'.joe-content-section[data-section=${id}]\').removeClass(\'collapsed\')[0].scrollIntoView()">${name}</div>';
    '<joe-submenu-section class="joe-submenu-section f${renderTo}" data-section="${name}" data-panel="${renderTo}" ' + onclick + ' ' + ondblclick + '>${${anchor}||${name}}</joe-submenu-section>';
    var section;

    for (var secname in self.current.sections) {
      section = _getSection(secname);

      if (!section.hidden) {
        anchorhtml += fillTemplate(template, section);
        scount++;
      }
    }

    anchorhtml += "<div class='clear'></div></joe-submenu-section-anchors>";
    return {
      count: scount,
      code: anchorhtml
    };
  }

  self.gotoSection = function (section, specs) {
    var specs = $.extend({}, specs || {});

    if (section) {
      var i = specs.index || self.joe_index;
      var sectionDom = $('.joe-overlay[data-joeindex=' + i + ']').find('.joe-content-section[data-section=\'' + section + '\']');

      if (specs.dblclickdom) {
        var tabbedmode,
            parent_dom = sectionDom.parents('.joe-content-sidebar,joe-panel-content');
        $(specs.dblclickdom).toggleClass('active');
        sectionDom.toggleClass('active');
        tabbedmode = $(specs.dblclickdom).parent().children('.active[data-panel=' + $(specs.dblclickdom).data('panel') + ']').length > 0; // if(tabbedmode){

        parent_dom.toggleClass('joe-tabbed-content', tabbedmode); // }
      }

      sectionDom.removeClass('collapsed')[0].scrollIntoView();
      var sidebar = sectionDom.parents('.joe-content-sidebar');

      if (sidebar && sidebar.length) {
        var s = sidebar.data('side');
        self.toggleSidebar(s, true);
        sectionDom.siblings('.joe-content-section').addClass('collapsed');
      } else if (self.sizeClass == "small-size") {
        self.toggleSidebar('left', false);
        self.toggleSidebar('right', false);
      } //collapse in mobile view


      $('joe-submenu-section-anchors.expanded').removeClass('expanded');
    }
  };

  self.Sections = {
    renderAchors: renderSectionAnchors,
    "goto": self.gotoSection,
    gotoTop: function gotoTop(index) {
      var ajoe = getJoe(index);
      $('.joe-overlay[data-joeindex=' + index + ']').find('.joe-panel-content').scrollTop(0);

      if (ajoe.panel.hasClass('small-size')) {
        ajoe.toggleSidebar('right', false);
        ajoe.toggleSidebar('left', false);
      }

      $('joe-submenu-section-anchors.expanded').removeClass('expanded');
    }
  };

  self.Sections.toggle = function (dom) {
    if (dom) {
      $(dom).parents('joe-submenu-section-anchors').toggleClass('expanded');
    }
  };
  /*------------------>
      Subset
  <------------------*/


  this.Subset = {
    getCached: function getCached(list) {
      var list = list || self.current.list;
      var cache_query = self.current.schema.name + ':' + self.current.subset.name;
      var subcache = self.Cache.get(cache_query);

      if (!subcache) {
        subcache = list.where(self.current.subset.filter);
        self.Cache.set(subcache, {
          id: cache_query
        });
      }

      return subcache;
    }
  };
  /*------------------>
      Filter
  <------------------*/

  this.Filter = {
    refilter: function refilter() {
      if (!$c.isEmpty(self.current.filters)) {
        self.filterListFromSubmenu(null, true);
      }
    },
    getCached: function getCached(list) {
      var BM = new Benchmarker();

      if (!self.current.schema || !self.current.schema.filters) {
        _bmResponse(BM, 'filters gotten');

        return false;
      }

      var list = list || self.current.list;
      var cache_query = self.current.schema.name + ':filters';
      var filtercache = self.Cache.get(cache_query);

      if (!filtercache) {
        filtercache = self.propAsFuncOrValue(self.current.schema.filters, self.current.list);
        self.Cache.set(filtercache, {
          id: cache_query
        });
      }

      _bmResponse(BM, 'filters gotten');

      return filtercache;
    }
  };

  this.renderSubmenuFilters = function (s) {
    if (!(s || self.current.subsets || self.current.schema && self.propAsFuncOrValue(self.current.schema.filters))) {
      return '';
    }

    var action = ' onclick="getJoe(' + self.joe_index + ').toggleFiltersMenu();" ';
    var active = self.current.subset ? 'active' : '';
    var count = $c.itemCount(self.current.filters);
    var indicator = active || count ? '<joe-count-indicator class="' + active + (count && ' count ' || '') + '">' + (count || '') + '</joe-count-indicator>' : '';
    var html = "<div class='jif-panel-submenu-button joe-filters-toggle ' " + action + ">" + indicator + "</div>";
    return html;
  };

  var leftMenuShowing = false;
  var rightMenuShowing = false;

  this.toggleFiltersMenu = function () {
    leftMenuShowing = !leftMenuShowing;
    self.panel.toggleClass('show-filters', leftMenuShowing);
  };

  this.toggleAggregateMenu = function () {
    rightMenuShowing = !rightMenuShowing;
    self.panel.toggleClass('show-aggregator', rightMenuShowing);
  };

  this.Filter.Options = {
    status: function status(schemaname, sortby, specs) {
      var specs = specs || {};

      if ($.type(schemaname) == "object") {
        specs = schemaname;
      }

      var schemaname = specs.schemaname || self.current.schema && self.current.schema.name || '';
      var stats = [];
      var stat;
      self.Data.status.sortBy(specs.sortby || 'index').where({
        datasets: {
          $in: [schemaname]
        }
      }).map(function (status) {
        stat = {
          name: status.name,
          filter: {
            status: status._id
          },
          title: self.propAsFuncOrValue(specs.title) || status.name
        };
        stat[specs.color || 'bgcolor'] = status.color;
        stats.push(stat);
      });
      return stats;
    },
    tags: function tags(specs) {
      var specs = specs || {};
      var schemaname = specs.schemaname || self.current.schema && self.current.schema.name || '';
      var showUntagged = specs.untagged || false;
      var schemaname = specs.schemaname || self.current.schema && self.current.schema.name || '';
      var filters = [];
      var groupname = typeof specs.group == 'string' ? specs.group : 'tags';

      if (specs.group) {
        filters.push({
          group_start: groupname,
          collapsed: specs.collapsed
        });
      }

      var tag;

      var tags = _joe.Data.tag.sortBy(specs.sortby || 'name').filter(function (tag) {
        if (tag.datasets.indexOf(schemaname) != -1) {
          filters.push({
            name: tag.name,
            filter: {
              tags: {
                $in: [tag._id]
              }
            }
          });
        }
      });

      if (specs.untagged) {
        filters.push({
          name: 'untagged',
          filter: {
            $or: [{
              tags: {
                $size: 0
              }
            }, {
              tags: {
                $exists: false
              }
            }]
          }
        });
      }

      if (specs.group) {
        filters.push({
          group_end: groupname
        });
      }

      return filters;
    }
  };

  this.Filter.generateQuery = this.generateFiltersQuery = function () {
    //comgine all active fitlers into a query
    var query = {};
    var filterinfo, filterobj, addFilter; //var qval, fval;

    for (var f in self.current.filters) {
      filterinfo = (self.propAsFuncOrValue(self.current.schema.filters) || []).where({
        name: f
      })[0] || self.current.filters[f] || {};
      addFilter = !filterinfo.schema || !self.current.schema || !self.current.schema.__schemaname || filterinfo.schema == (self.current.schema.__Schemaname || self.current.schema.__schemaname);

      if (addFilter) {
        filterobj = filterinfo.filter;

        for (var ff in filterobj) {
          if (query[ff]) {
            //TODO: do crazy smart stuff here.
            switch ($.type(filterobj[ff])) {
              case 'number':
              case 'string':
              case 'object':
                if ($.type(query[ff]) == 'string' || $.type(query[ff]) == 'number') {
                  query[ff] = {
                    $in: [query[ff], filterobj[ff]]
                  };
                } else if ($.type(query[ff]) == 'object') {
                  if (query[ff].hasOwnProperty('$in')) {
                    query[ff]['$in'] = query[ff]['$in'].concat(filterobj[ff]['$in'] || [filterobj[ff]]); //query[ff]['$in'] = query[ff]['$in'].concat(filterobj[ff]['$in']);
                  }
                }

                break;
            }
          } else {
            //not there, add it
            query[ff] = filterobj[ff];
          }
        }
      } else {
        //remove from current filters list
        delete self.current.filters[filterinfo.name];
      }
    }

    logit(query);
    return query;
  };
  /*------------------>
   Search
   <------------------*/


  self.Search = {
    schema: {
      title: 'Search Results',
      name: 'search',
      listTitle: function listTitle(item) {
        var html = '<joe-full-right>' + '<joe-subtext>updated</joe-subtext>' + '<joe-subtitle>' + self.Utils.toDateString(item.joeUpdated) + '</joe-subtitle>' + '</joe-full-right>' + (_joe.schemas[item.itemtype].menuicon && '<joe-icon class="icon-40 icon-grey fleft">' + _joe.schemas[item.itemtype].menuicon + '</joe-icon>' || '') + '<joe-subtext>${itemtype}</joe-subtext>' + '<joe-title>${name}</joe-title><joe-subtitle>${info}</joe-subtitle>' + '<joe-subtext>${_id}</joe-subtext>';
        return html;
      },
      listmenu: [],
      sorter: ['name', {
        name: 'updated',
        value: '!joeUpdated'
      }, 'itemtype'],
      filters: function filters() {
        var f = [];

        __appCollections.concat().sort().map(function (s) {
          f.push({
            name: s,
            filter: {
              itemtype: s
            }
          });
        });

        return f;
      },
      listAction: function listAction(item) {
        return 'goJoeItem(\'' + item._id + '\',\'' + item.itemtype + '\')'; //    return 'goJoe(_joe.search(\''+item._id+'\')[0],{schema:\''+item.itemtype+'\'});';
      }
    }
  };

  this.renderSubmenuSearch = function (s) {
    var action = ' onkeyup="_joe.filterListFromSubmenu(this.value);" ';
    var keyword = self.current.keyword || $GET('keyword') || '';
    var submenusearch = "<div class='joe-submenu-search '>" + '<input class="joe-submenu-search-field" ' + action + ' placeholder="search list" value="' + keyword + '"/>' + "</div>";
    return submenusearch;
  };

  this.searchTimeout;
  var searchTimeoutTime = $c.isMobile() ? 750 : 500;

  this.filterListFromSubmenu = function (keyword, now) {
    /*|{
        featured:true,
        tags:'filter,list',
        description:'Filters the list based on keywords,subsets and filters'
    }|*/
    clearTimeout(self.searchTimeout);
    self.overlay.removeClass('.multi-edit');

    if (!now) {
      self.searchTimeout = setTimeout(function () {
        searchFilter(keyword, true);
      }, searchTimeoutTime);
    } else {
      searchFilter(keyword);
    }

    function searchFilter(keyword, frominput) {
      var searchBM = new Benchmarker();

      if (self.current.keyword != keyword) {
        keyword = keyword || $('.joe-submenu-search-field').val() || '';

        if (!frominput && !keyword && self.current.keyword) {
          keyword = self.current.keyword;
          $('.joe-submenu-search-field').val(keyword);
        }

        var value = keyword.toLowerCase();
        var keywords = value.replace(/,/g, ' ').split(' ');
        var filters = self.generateFiltersQuery();
        _joe.history[_joe.history.length - 1].keyword = value;
        _joe.history[_joe.history.length - 1].filters = self.current.filters;
        var testable;
        var idprop = self.getIDProp();
        var id;
        var listables = self.current.list;

        if (self.current.subset) {
          listables = self.Subset.getCached();
        }

        listables = cleanDeleted(listables);
        var searchables = self.current.schema && self.current.schema.searchables;

        _bmResponse(searchBM, 'search where');

        self.current.filteredList = currentListItems = listables.where(filters).filter(function (i) {
          if (!keyword) {
            return true;
          }

          id = i[idprop];
          testable = '';

          if (searchables) {
            //use searchable array
            searchables.map(function (s) {
              testable += i[s] + ' ';
            });
            testable = testable.toLowerCase() + id;

            for (var k = 0, tot = keywords.length; k < tot; k++) {
              if (testable.indexOf(keywords[k]) == -1) {
                return false;
              }
            } //return (testable.toLowerCase().indexOf(value) != -1);
            //return (testable+' '+i[idprop].toLowerCase().indexOf(value) != -1);

          } else {
            if (tableMode) {
              testable = self.renderTableItem(i, true);
            } else if (gridMode) {//testable = self.renderListItem(i,true);
            } else {
              testable = self.renderListItem(i, true);
            }

            testable = testable.toLowerCase() + id;

            for (var k = 0, tot = keywords.length; k < tot; k++) {
              if (testable.indexOf(keywords[k]) == -1) {
                return false;
              }
            } //return ((__removeTags(testable)+id).toLowerCase().indexOf(value) != -1);

          }

          return true;
        });

        _bmResponse(searchBM, 'search \'' + keyword + '\' filtered ' + currentListItems.length + '/' + self.current.list.length + ' items [' + listables.length + ']');

        self.overlay.find('.joe-submenu-itemcount').html(currentListItems.length + ' item' + (currentListItems.length > 1 && 's' || ''));
        self.panel.find('.joe-panel-content').html(self.renderListItems(currentListItems, 0, self.specs.dynamicDisplay)); //var titleObj = $.extend({},self.current.object,{_listCount:currentListItems.length||'0',_subsetName:(self.current.subset && self.current.subset.name ||'')});
        //self.panel.find('.joe-panel-title').html(fillTemplate(self.current.title,titleObj));
      }

      self.current.keyword = keyword;
      self.current.search_schema;
      var titleObj = createTitleObject();

      if (titleObj.subTitle) {
        self.panel.find('.joe-panel-title').addClass('subtitled').html(titleObj.docTitle + '<joe-subtext>' + titleObj.subTitle + '</joe-subtext>');
      } else {
        self.panel.find('.joe-panel-title').addClass('subtitled').html(titleObj.docTitle);
      } //update filters icons


      self.panel.find('.joe-filters-toggle').replaceWith(self.renderSubmenuFilters());
    }
  };
  /*------------------>
   Count
   <------------------*/


  this.renderSubmenuItemcount = function (s) {
    var submenuitem = "<div class='joe-submenu-itemcount'>items</div>";
    return submenuitem;
  };
  /*------------------------------------------------------>
      //SUBMENU SELECTORS
   <-----------------------------------------------------*/


  function renderSubmenuSelectors(specs) {
    var specs = $.extend({
      options: [],
      content: '',
      action: 'nothing',
      buttonTemplate: "<div class='selection-label'>${label}</div>${name}",
      label: 'label',
      cssclass: ''
    }, specs || {});
    var selectionTemplate = "<div data-colcount='${name}' " + "onclick='getJoe(" + self.joe_index + ")" + "." + specs.action + "(${name});' " + "class='jif-panel-button selector-button-${name} joe-selector-button " + "'>" + specs.buttonTemplate + '</div>';
    var onclick_action = "onclick='$(this).parent().toggleClass(\"expanded\")'";
    var content = specs.content || "<div class='joe-selector-button selector-label' " + onclick_action + ">" + specs.label + "</div>" + "<div class='joe-selector-button selector-close joe-close-button' " + onclick_action + ">&nbsp;</div>" + '<joe-submenu-selector-options>' + fillTemplate(selectionTemplate, specs.options) + '</joe-submenu-selector-options>'; // var width = 40*specs.options.length + 40;

    var html = "<div class='joe-submenu-selector' >" + content + "</div>";
    /*function checkSubmenuSelector(value){
        var submenuValue = value || this.value;
        return ''
    }*/

    return html;
  }

  this.nothing = function (nothing) {
    alert(value);
  };
  /*------------------>
  View Mode Buttons
  <------------------*/


  this.renderViewModeButtons = function (subspecs) {
    var gridspecs = self.current.schema && (self.current.schema.grid || self.current.schema.gridView);
    var tablespecs = tableSpecs; //self.current.schema && (self.current.schema.table||self.current.schema.tableView);

    if (!gridspecs && !tablespecs) {
      return '';
    }

    var modes = [{
      name: 'list'
    }];

    if (gridspecs) {
      modes.push({
        name: 'grid'
      });
    }

    if (tablespecs) {
      modes.push({
        name: 'table'
      });
    }

    var modeTemplate = "<div data-view='${name}' " + "onclick='getJoe(" + self.joe_index + ")" + ".setViewMode(\"${name}\");' " + "class='jif-panel-button joe-viewmode-button ${name}-button joe-selector-button'>&nbsp;</div>";
    var onclick_action = "onclick='$(this).parent().toggleClass(\"expanded\")'";
    var submenuitem = "<div class='joe-submenu-selector joe-viewmode-selector opts-" + modes.length + "' >" + //onhover='$(this).toggleClass(\"expanded\")'
    "<div class='joe-selector-button selector-label' " + onclick_action + ">view</div>" + "<div class='joe-selector-button selector-close joe-close-button' " + onclick_action + ">&nbsp;</div>" + '<joe-submenu-selector-options>' + fillTemplate(modeTemplate, modes) + '</joe-submenu-selector-options>' + "</div>"; //return '';

    return submenuitem;
  };

  this.setViewMode = function (mode) {
    self.reload(true, {
      viewMode: mode
    });
  };
  /*------------------------------------------------------>
   Column Count Buttons
   <-----------------------------------------------------*/


  this.renderColumnCountSelector = function (subspecs) {
    if (!subspecs) {
      return '';
    }

    var modes = [{
      name: '1'
    }, {
      name: '2'
    }, {
      name: '3'
    }, {
      name: '4'
    }, {
      name: '5'
    }];

    if (typeof eval(subspecs) == "number") {
      modes = modes.slice(0, eval(subspecs));
    }
    /*        var modeTemplate="<div data-colcount='${name}' " +
                "onclick='getJoe("+self.joe_index+")" + ".setColumnCount(${name});' " +
                "class='jif-panel-button selector-button-${name} joe-selector-button'>" +
                "<div class='selection-label'>cols</div>${name}</div>";
            var submenuitem =
                "<div class='joe-submenu-selector opts-"+modes.length+"'' >"+
                fillTemplate(modeTemplate,modes)+
                "</div>";*/


    var h = renderSubmenuSelectors({
      options: modes,
      //buttonTemplate:"<div class='selection-label'>cols</div>${name}",
      label: 'cols',
      value: colCount,
      action: 'setColumnCount'
    });
    return h; //return submenuitem;
  };

  this.setColumnCount = function (mode) {
    self.overlay[0].className = self.overlay[0].className.replace(/cols-[0-9]/, 'cols-' + mode);

    if (mode) {
      colCount = mode;
    }

    var multi = mode && mode > 1 ? true : false;
    self.overlay.toggleClass('multi-col', multi); //self.reload(true,{colCount:mode});
  };
  /*------------------------------------------------------>
      Submenu Sorter
   <-----------------------------------------------------*/


  this.Sorter = {};

  this.Sorter.render = this.renderSorter = function (subspecs) {
    var sorter = self.current.subset && self.current.subset.sorter || self.current.schema && self.current.schema.sorter || 'name';

    if ($.type(sorter) == 'string') {
      sorter = sorter.split(',');
    }

    if (sorter.indexOf('name') == -1 && sorter.indexOf('!name') == -1 && !sorter.where({
      field: 'name'
    }).length) {
      sorter.push('name');
    }

    if (sorter.length == 1) {
      return '';
    }

    var newsorter = subspecs;
    var current;
    return '<label for="joe-' + self.joe_index + '-sorter" class="joe-list-sorter" >' + '<svg onclick="getJoe(' + self.joe_index + ').Sorter.reverse();" class="joe-list-sorter-icon" title="reverse sort" xmlns="http://www.w3.org/2000/svg" viewBox="-20 6 40 40"><path d="M-4 16L-9.5 21.5 -8 23 -5 20 -5 28 -3 28 -3 20 0 23 1.5 21.5 -4 16zM3 16L3 18 5 18 5 16 3 16zM3 20L3 22 5 22 5 20 3 20zM3 24L3 32 0 29 -1.5 30.5 4 36 9.5 30.5 8 29 5 32 5 24 3 24zM-5 30L-5 32 -3 32 -3 30 -5 30zM-5 34L-5 36 -3 36 -3 34 -5 34z"/></svg>' + '<select title="sort" name="joe-' + self.joe_index + '-sorter" onchange="getJoe(' + self.joe_index + ').Sorter.resort(this.value);">' + sorter.map(function (so) {
      var s = so,
          display = so;

      if (!$c.isString(so)) {
        s = so.field || so.value;
        display = so.display || so.name || s;
      }

      current = s == (self.current.sorter[0].field || self.current.sorter[0].value || self.current.sorter[0]) ? 'selected' : '';
      /* if(self.current.sorter[0] == s){
       return '<option value="!'+s+'">!'+s+'</option>';
       }*/

      return '<option ' + current + ' value="' + s + '">' + display + '</option>';
    }) + '</select></label>';
  };

  this.Sorter.resort = function (sorter) {
    self.reload(true, {
      sorter: sorter
    });
  };

  this.Sorter.reverse = function (sorter) {
    var curSorter = self.current.sorter;

    if ($.type(curSorter) == "array") {
      curSorter = [].concat(curSorter);

      if ($.type(curSorter[0]) == "string") {
        if (curSorter[0].indexOf('!') == 0) {
          curSorter[0] = curSorter[0].substr(1);
        } else {
          curSorter[0] = '!' + curSorter[0];
        }
      } else if ($.type(curSorter[0]) == "object" && curSorter[0].value) {
        if (curSorter[0].value.indexOf('!') == 0) {
          curSorter[0].value = curSorter[0].value.substr(1);
        } else {
          curSorter[0].value = '!' + curSorter[0].value;
        }
      }

      self.reload(true, {
        sorter: curSorter
      });
    }
  };

  /*------------------------------------------------------>
      Submenu Aggregator
  <-----------------------------------------------------*/
  this.Submenu = this.Submenu || {};
  this.Submenu.Aggregator = {};
  this.Submenu.Aggregator.renderToggleButton = function(){
    
    return ``;
  }
  this.Submenu.Aggregator.renderAggregatorHolder = function(content){
    "<div class='joe-filters-holder'>" + renderSubsetsDiv() + (filters_obj && renderFiltersDiv() || '') // +'<span class="jif-arrow-left"></span>'
      + "</div>"
    return ``;
  }

  /*----------------------------->
      C | Editor Content
  <-----------------------------*/


  function renderEditorStyles() {
    var style_str = (self.specs.style && self.specs.style.inset && 'joe-style-inset ' || '') + (self.specs.style && self.specs.style.cards && 'joe-style-cards ' || '');
    return style_str;
  }

  this.renderEditorContent = function (specs) {
    self.current.sidebars = {
      left: {
        collapsed: false
      },
      right: {
        collapsed: false
      }
    }; //specs = specs || {};

    var content;

    if (!specs) {
      specs = {
        mode: 'text',
        //text,list,single
        text: 'No object or list selected'
      };
    }

    var mode = specs.mode;

    switch (mode) {
      case 'text':
        content = self.renderTextContent(specs);
        break;

      case 'rendering':
        content = self.renderHTMLContent(specs);
        break;

      case 'list':
        content = self.renderListContent(specs);
        break;

      case 'object':
        content = self.renderObjectContent(specs);
        break;

      default:
        content = content || '';
        break;
    }

    var submenu = '';

    if (!specs.minimode) {
      if (mode == 'list' && self.current.submenu || self.current.submenu || renderSectionAnchors().count || renderSubmenuButtons().content) {
        submenu = ' with-submenu ';
      }
    } //submenu=(self.current.submenu || renderSectionAnchors().count)?' with-submenu ':'';


    var scroll = 'onscroll="getJoe(' + self.joe_index + ').onListContentScroll(this);"';
    var rightC = content.right || '';
    var leftC = content.left || ''; //var content_class='joe-panel-content joe-inset ' +submenu;

    var content_class = 'joe-panel-content ' + renderEditorStyles() + submenu;
    var html = '<joe-panel-content class="' + content_class + '" ' + (listMode && scroll || '') + '>' + (content.main || content) + '</joe-panel-content>' + self.renderSideBar('left', leftC, {
      css: submenu
    }) + self.renderSideBar('right', rightC, {
      css: submenu
    });
    self.current.sidebars.left.content = leftC;
    self.current.sidebars.right.content = rightC;
    return html;
  };

  this.renderSideBar = function (side, content, specs) {
    var side = side || 'right';
    var expanded = ''; //(content && ' expanded ') ||'';

    var specs = specs || {};
    var addCss = specs.css || '';
    var html = "<div class='joe-content-sidebar " + renderEditorStyles() + " joe-absolute " + side + "-side " + expanded + addCss + "' data-side='" + side + "'>" + (content || '') + __clearDiv__ + "</div>";
    return html;
  };

  this.toggleSidebar = function (side, hardset) {
    if (['right', 'left'].indexOf(side) == -1) {
      return false;
    }

    self.panel.toggleClass(side + '-sidebar', hardset);
    /*        $('.joe-panel-content').toggleClass(side+'-sidebar',hardset)
     $('.joe-content-sidebar.'+side+'-side').toggleClass('expanded',hardset)*/
  };

  function renderSidebarToggle(side) {
    if (self.current.sidebars[side].hidden) {
      return '';
    }

    var html = '<div class="jif-panel-submenu-button joe-sidebar-button joe-sidebar_' + side + '-button" ' + 'title="toggle" onclick="getJoe(' + self.joe_index + ').toggleSidebar(\'' + side + '\')">' + self.SVG.icon['sidebar-' + side] + '</div>';
    return html;
  }

  this.renderTextContent = function (specs) {
    specs = specs || {};
    var text = specs.text || specs.object || '';
    var html = '<div class="joe-text-content">' + text + '</div>';
    return html;
  };

  this.renderHTMLContent = function (specs) {
    specs = specs || {};
    var html = '<textarea class="joe-rendering-field">' + (specs.rendering || '') + '</textarea>';
    return html;
  };
  /*--
  //LIST
  --*/


  var currentListItems;

  this.Render.listContent = this.renderListContent = function (specs) {
    var wBM = new Benchmarker();

    if (specs.minimode) {
      return self.renderMiniListContent(specs);
    }

    currentListItems = [];
    self.current.selectedListItems = [];
    self.current.anchorListItem = null;
    specs = specs || {};
    var schema = specs.schema;
    var list = specs.list || [];
    var html = '';
    var filteredList;
    list = cleanDeleted(list);
    var numItemsToRender;
    currentListItems = list;

    if (self.current.subset) {
      filteredList = currentListItems = self.Subset.getCached(list);

      _bmResponse(wBM, 'list subsetted');
    } //SORT LIST


    var primer = null;

    if (self.current.sorter && ($.type(self.current.sorter) != 'array' || self.current.sorter.length)) {
      var sorterValues = self.current.sorter;

      if ($c.isArray(self.current.sorter) && self.current.sorter[0].field) {
        sorterValues = [];

        for (var i = 0, len = self.current.sorter.length; i < len; i++) {
          sorterValues.push(self.current.sorter[i].field || self.current.sorter[i]);
          primer = self.current.sorter[i].primer || primer || null;
        }
      } else {
        primer = self.current.sorter[0].primer || null;
      }

      list = list.sortBy(sorterValues, null, primer);
    } //list = list.sortBy(self.current.sorter);


    _bmResponse(wBM, 'list sort complete'); //FILTER LIST


    if (!$c.isEmpty(_joe.current.filters)) {
      filteredList = currentListItems.where(self.Filter.generateQuery());
      currentListItems = filteredList;

      _bmResponse(wBM, 'list filtered');
    }
    /*
    if(!self.current.subset){
        currentListItems = list;
    }
    else{
        filteredList = list.where(self.current.subset.filter);
        currentListItems = filteredList;
        _bmResponse(wBM,'list where complete');
    }*/


    _bmResponse(wBM, 'list prerender');

    numItemsToRender = self.specs.dynamicDisplay || currentListItems.length;
    html += self.renderListItems(currentListItems, 0, numItemsToRender);

    _bmResponse(wBM, 'list complete');

    return html;
  };

  function cleanDeleted(list, schema) {
    var deleted_prop = self.getCascadingProp('deleted') || '_deleted';
    var query = {};
    query[deleted_prop] = {
      $in: [false, 'false', undefined]
    };
    var activeList = list.where(query);
    return activeList;
  }

  this.renderMiniListContent = function (specs) {
    var wBM = new Benchmarker();
    specs = specs || {};
    var schema = specs.schema;
    var list = specs.object || [];
    var idprop = specs.idprop || '_id';
    var html = '';
    var sorter = specs.sorter || 'name'; //var click = specs.click || 'alert(\'${name}\')';

    list = list.sortBy(sorter);
    var click = 'getJoe(' + self.joe_index + ').minis[\'' + specs.minimode + '\'].callback(\'${' + idprop + '}\')';
    var template; // var template = self.propAsFuncOrValue(specs.template) || '<joe-title>${name}</joe-title><joe-subtitle>${'+idprop+'}</joe-subtitle>';
    //     template ='<joe-list-item schema="'+(schema.name || schema)+'" class="joe-field-list-item" onclick="'+click+'">'+template+'</joe-list-item>';
    //TODO: lazy-render images

    for (var li = 0, tot = list.length; li < tot; li++) {
      var item = list[li];
      var schemaName = item.itemtype || item.schema || item.name;
      template = self.propAsFuncOrValue(specs.template, item) || '<joe-title>${name}</joe-title><joe-subtext>${' + idprop + '}</joe-subtext>';
      template = '<joe-list-item schema="' + (schemaName || schema.name || schema) + '" class="joe-field-list-item clickable" onclick="' + click + '">' + template + '</joe-list-item>';
      html += fillTemplate(template, item);
    }

    _bmResponse(wBM, 'minilist complete');

    return html;
  };

  this.Render.listItems = this.renderListItems = function (items, start, stop) {
    var html = '';
    var listItem;
    var items = items || currentListItems;
    var simple_sorter = [];
    self.current.sorter.map(function (s) {
      simple_sorter.push(s.field || s.value || s);
    });
    items = items.sortBy(simple_sorter);
    var start = start || 0;
    var stop = stop || currentListItems.length - 1;

    if (gridMode) {
      var gridspecs = self.current.schema && (self.current.schema.grid || self.current.schema.gridView) || {};
      var columns = self.propAsFuncOrValue(gridspecs.cols) || [];
      var rows = self.propAsFuncOrValue(gridspecs.rows) || [];
      var cells = [];
      var cell, colinfo, rowinfo, col_filter, row_filter, subitems;
      /*var template = self.propAsFuncOrValue(gridspecs.template) 
          || '<joe-option class=" joe-panel-content-option"><joe-title></joe-title>${name}<joe-content>${info}</joe-content></joe-option>';
      */

      for (var c = 0; c < columns.length; c++) {
        colinfo = columns[c];
        col_filter = colinfo.filter;
        subitems = items.where(col_filter);

        if (rows.length) {//create array cells with merged filters
        } else {
          //create array with single cell in it
          cell_filter = colinfo.filter;
          cell = {
            name: colinfo.name,
            filter: col_filter,
            html: '',
            items: []
          };
          subitems.map(function (i) {
            cell.items.push(i);
            cell.html += self.Render.gridItem(i);
          });
          cells.push([cell]);
        }
      }

      html += '<table class="joe-grid-table"><thead>';
      columns.map(function (c) {
        if ($c.isString(c)) {
          html += '<th>' + c + '</th>';
        } else if ($c.isObject(c)) {
          html += '<th>' + (c.header || c.display || c.name) + '</th>';
        }
      });
      html += '</thead><tbody>';
      var row_number = 0;
      var row_count = rows.length || 1; // cells.map(function(cell_list){
      //     cell_list.map(function(cell){
      //     })
      // })

      while (row_number < row_count) {
        html += '<tr>';
        cells.map(function (cell_list) {
          html += '<td class="joe-grid-cell">' + cell_list[row_number].html + '</td>';
        });
        html += '</tr>';
        row_number++;
      }
      /*
      for (var i = start; i < stop; i++) {
          listItem = items[i];
          if (listItem) {
              //html += self.renderListItem(listItem, false, i + 1);
              html += self.renderGridItem(listItem, false, i + 1);
            }
      }*/


      html += '</tbody></table>';
      return html;
    } else if (tableMode) {
      /*var tableSpecs = $.extend({cols:['name',self.getIDProp()]},
          (self.current.schema
              &&(self.current.schema.table||self.current.schema.tableView)
             // &&(self.current.schema.table||self.current.schema.tableView).cols
          ) ||{});*/
      html += '<table class="joe-item-table" cellspacing="0"><thead class="joe-table-head"><th>&nbsp;</th>';
      tableSpecs.cols.map(function (c) {
        if ($c.isString(c)) {
          html += '<th>' + c + '</th>';
        } else if ($c.isObject(c)) {
          html += '<th>' + (c.header || c.display) + '</th>';
        }
      });
      html += '</thead><tbody>';
      stop = currentListItems.length;

      for (var i = start; i < stop; i++) {
        listItem = items[i];

        if (listItem) {
          //html += self.renderListItem(listItem, false, i + 1);
          html += self.renderTableItem(listItem, false, i + 1);
        }
      }

      html += '</tbody></table>';
      return html;
    } else {
      for (var i = start; i < stop; i++) {
        listItem = items[i];

        if (listItem) {
          html += self.renderListItem(listItem, false, i + 1); //html += $GET('table') ? self.renderGridItem(listItem, false, i + 1) : self.renderListItem(listItem, false, i + 1);
        }
      }

      return html;
    }
  };

  this.onListContentScroll = function (domObj) {
    // logit(domObj);
    var listItem = self.panel.find('.joe-panel-content-option').last()[0];
    var currentItemCount = self.panel.find('.joe-panel-content-option').length;

    if (currentItemCount == currentListItems.length) {
      // logit('all items showing');
      return;
    } //$('.app-list-group-content').not('.events-group').not('.collapsed').find('.app-list-divider-count').prev('.app-list-item');
    //var listItem;


    var viewPortHeight = self.panel.find('.joe-panel-content').height();
    var html = '';

    try {
      if (listItem.getBoundingClientRect().bottom - 500 < viewPortHeight) {
        //self.generateGroupContent(groupIndex);
        // logit('more content coming');
        if (gridMode) {}

        if (tableMode) {} else {
          html += self.renderListItems(null, currentItemCount, currentItemCount + self.specs.dynamicDisplay);
          self.panel.find('.joe-panel-content').append(html);
        }
      }
    } catch (e) {
      self.Error.add('error scrolling for more content: \n' + e, e, {
        caller: 'self.onListContentScroll',
        domObj: domObj
      });
    }
  };
  /*--
       //OBJECT
   --*/
  //TODO: Add configed listeneres via jquery not strings


  var renderFieldTo;

  this.renderObjectContent = function (specs) {
    renderFieldTo = 'main';
    specs = specs || {};
    var object = specs.object;
    var fields = {
      main: '',
      left: '',
      right: ''
    };
    var propObj;
    var fieldProp;

    if (!specs.minimode) {
      self.current.fields = [];
      self.current.sections = {};
    }

    if (specs.schema && typeof specs.schema == 'string') {
      specs.schema = self.schemas[specs.schema];
    }

    var schemaFields = specs.schema ? self.propAsFuncOrValue(specs.schema.fields) : false;

    if (!specs.schema || !schemaFields) {
      //no schema use items as own schema
      for (var prop in object) {
        if (object.hasOwnProperty(prop)) {
          propObj = $.extend({
            name: prop,
            type: 'text',
            //type:($.type(object[prop]) == 'object')?'rendering':'text',
            value: object[prop]
          }, self.fields[prop], //overwrite with value
          {
            value: object[prop]
          });
          fields.main += self.renderObjectField(propObj);
        }
      }
    } else {
      var fhtml; //self.current.constructed = self.constructObjectFromFields();

      (schemaFields || []).map(function (prop) {
        //logit(renderFieldTo);
        fhtml = self.renderObjectPropFieldUI(prop, specs);
        fields[renderFieldTo] += fhtml;
      }); //end map
    }

    var html = '<div class="joe-object-content">' + fields.main + '<div class="clear"></div></div>';
    return fields;
  };

  var rerenderingField = false;

  self.Fields.rerender = this.rerenderField = function (fieldname, valueObj) {
    /*|{
        featured:true,
        description:'rerenders a single or list of fields, pass an optional object to overwrite values',
        tags:'rendering,field'
    }|*/
    if (!fieldname) {
      return;
    }

    self.current.constructed = self.constructObjectFromFields();

    if (!self.current.constructed || $c.isEmpty(self.current.constructed)) {
      return;
    }

    $.extend(self.current.constructed, valueObj || {});
    rerenderingField = true;

    if ($c.isArray(fieldname)) {
      fieldname = fieldname.join(',');
    }

    var ftype, field;
    fieldname.split(',').condense().map(function (f) {
      var fields = self.renderObjectPropFieldUI(f);
      field = self.getField(f);
      ftype = self.propAsFuncOrValue(field.type, self.current.constructed).toLowerCase(); //            ftype = self.propAsFuncOrValue(field.type,self.current.constructed);

      $('.joe-object-field[data-name=' + f + ']').parent().replaceWith(fields);

      if (self.Fields[ftype] && self.Fields[ftype].ready) {
        self.Fields[ftype].ready(self.current.constructed);
      }
    });
    rerenderingField = false;
    self.current.constructed = null;
  };

  self.Set = function (field, value) {
    /*|{
        featured:true,
        description:'changes the value of a single field to the passed value',
        tags:'rendering,field,setter'
    }|*/
    if (listMode) {
      logit('in list mode');
      return;
    }

    var field = self.propAsFuncOrValue(field);
    var value = self.propAsFuncOrValue(value);
    var vals = {};
    vals[field] = value;
    self.Fields.rerender(field, vals);
    return value;
  };

  self.Get = function (field) {
    /*|{
        featured:true,
        description:'returns the current value of a field',
        tags:'rendering,field,getter'
    }|*/
    if (listMode) {
      logit('in list mode');
      return;
    }

    var obj = self.constructObjectFromFields();
    return obj[field];
  };

  self.renderObjectPropFieldUI = function (prop, specs) {
    /*|{
        featured:true,
        description:'entry point to field rendering. takes the prop to be rendered and additional spec overwrites.',
        tags:'rendering,field'
    }|*/
    //var prop = fieldname;
    var fields = '';

    if ($.type(prop) == "string" && prop.contains(':')) {
      var ps = prop.split(':');
      prop = {
        name: ps[0],
        type: ps[1]
      };

      if (ps[2]) {
        if (ps[2].contains('%') && parseInt(ps[2])) {
          prop.width = ps[2];
        } else {
          prop.display = ps[2];
        }
      }
    }

    var schemaspec = specs && specs.schema || self.current.schema;
    var existingProp = (self.propAsFuncOrValue(schemaspec.fields) || []).filter(function (s) {
      return s.name == prop || s.extend == prop;
    })[0];

    if (existingProp) {
      prop = existingProp;
    } //construct a working copy of the object to use for values.


    var constructed = self.current.constructed || self.constructObjectFromFields();

    if ($.type(prop) == "string") {
      //var fieldProp = self.fields[prop] || {};
      var propObj = self.fields[prop] || self.current.fields.filter(function (f) {
        return f.name == prop;
      })[0] || {};
      var fieldProp = $.extend({}, propObj); //merge all the items

      var propObj = extendField(prop);

      if (constructed[prop] !== undefined) {
        fieldProp.value = constructed[prop];
      }

      fields += self.renderObjectField(propObj);
    } else if ($.type(prop) == "object" && prop.name) {
      var fieldProp = $.extend({}, prop || {}); //merge all the items

      var propObj = extendField(prop.name);

      if (constructed[prop.name] !== undefined) {
        fieldProp.value = constructed[prop.name];
      }

      fields += self.renderObjectField(propObj);
    } else if (prop && prop.extend) {
      var fieldProp = $.extend({}, self.fields[prop.extend] || {}, prop.specs || {});
      var propObj = extendField(prop.extend);

      if (constructed[prop.extend] !== undefined) {
        fieldProp.value = constructed[prop.extend];
      } //renderFieldTo = propObj.sidebar_side || propObj.side || 'main';


      fields += self.renderObjectField(propObj);
    } else if ($.type(prop) == "object") {
      if (prop.component) {
        fields += self.renderPropComponent(prop);
      } else if (prop.label) {
        fields += self.renderContentLabel(prop);
      } else if (prop.section_start) {
        fields += self.renderPropSectionStart(prop); //renderFieldTo = prop.sidebar_side || prop.side || renderFieldTo;
      } else if (prop.section_end) {
        fields += self.renderPropSectionEnd(prop); //renderFieldTo = 'main';
      } else if (prop.sidebar_start) {
        //fields += self.renderPropSectionStart(prop);
        renderFieldTo = prop.sidebar_side || prop.sidebar_start || 'main';

        if (renderFieldTo != 'main') {
          if (prop.hasOwnProperty('collapsed')) {
            self.current.sidebars[renderFieldTo].collapsed = self.propAsFuncOrValue(prop.collapsed);
          }

          if (prop.hasOwnProperty('hidden')) {
            self.current.sidebars[renderFieldTo].hidden = self.propAsFuncOrValue(prop.hidden);

            if (self.current.sidebars[renderFieldTo].hidden) {
              self.current.sidebars[renderFieldTo].collapsed = true;
            }
          }
        }
      } else if (prop.sidebar_end) {
        renderFieldTo = 'main';
      }
    }

    function extendField(propname) {
      var propname = propname || prop;
      var cobj = self.current.constructed || self.current.object;
      var rerenderObj = {};

      if (rerenderingField && (fieldProp.run || fieldProp.value)) {
        if (fieldProp.run) {// rerenderObj.value = fieldProp.run(cobj,propname)
        } else {
          rerenderObj.value = fieldProp.value;
        } // let rerenderVal = self.propAsFuncOrValue((fieldProp.run||fieldProp.value));
        // rerenderObj.value = rerenderVal;

      }

      return $.extend({
        name: propname,
        type: 'text'
      }, {
        onblur: schemaspec.onblur,
        onchange: schemaspec.onchange,
        onkeypress: schemaspec.onkeypress,
        onkeyup: schemaspec.onkeyup
      }, fieldProp, //overwrite with value, possibly constructed
      rerenderObj, {
        value: cobj[propname]
      });
    }

    return fields;
  }; //PROP COMPONENT


  self.renderPropComponent = function (prop) {
    var html;
    var componentName = self.propAsFuncOrValue(prop.component);
    var cobj = self.current.constructed || self.current.object;

    if (prop.specs) {
      var _specs = self.propAsFuncOrValue(prop.specs);

      var specHtml = Object.keys(_specs).map(function (spec) {
        return " " + _specs + "=\"" + _specs[spec] + "\"";
      });
      html = "<" + componentName + " item_id=\"" + cobj._id + "\" " + specHtml + " >" + componentName + "</" + componentName + ">";
    } else {
      html = "<" + componentName + " item_id=\"" + cobj._id + "\">" + componentName + "</" + componentName + ">";
    }

    return html;
  }; //PROP LABELS


  self.renderContentLabel = function (specs) {
    if (specs.hasOwnProperty('condition') && !_joe.propAsFuncOrValue(specs.condition)) {
      return '';
    }

    if (specs.hasOwnProperty('hidden') && _joe.propAsFuncOrValue(specs.hidden)) {
      return '';
    }

    var html = "<div class='joe-content-label'>" + fillTemplate(specs.label, self.current.object) + "</div>";
    return html;
  }; //prop sections


  self.renderPropSectionStart = function (prop) {
    var show = '';
    var hidden = false;

    if (prop.condition && !prop.condition(self.current.object)) {
      show = ' no-section ';
      hidden = true;
    }

    if (self.current.sidebars[renderFieldTo] && self.current.sidebars[renderFieldTo].hidden) {
      show = ' no-section ';
      hidden = true;
    }

    var secname = fillTemplate(prop.display || prop.section_label || prop.section_start, self.current.object);
    var secID = prop.section_start;

    if (!secname || !secID) {
      return '';
    }

    var collapsed = self.propAsFuncOrValue(prop.collapsed) ? 'collapsed' : '';
    var toggle_action = "onclick='$(this).parent().toggleClass(\"collapsed\");'";
    var dblclick = ' ondblclick="$(\'joe-submenu-section[data-section=' + secID + ']\').dblclick();" ';
    var section_html = '<div class="joe-content-section ' + show + ' ' + collapsed + '" data-section="' + secID + '">' + '<joe-content-section-label class="joe-content-section-label" ' + toggle_action + dblclick + '>' + secname + '</joe-content-section-label>' + '<div class="joe-content-section-content">'; //add to current sections

    self.current.sections[secID] = {
      open: true,
      name: secname,
      id: secID,
      hidden: hidden,
      renderTo: renderFieldTo,
      anchor: prop.anchor
    };
    return section_html;
  };

  self.renderPropSectionEnd = function (prop) {
    var secID = prop.section_end;

    var section = _getSection(secID);

    if (!secID || !(section && section.open)) {
      return '';
    }

    var section_html = __clearDiv__ + '</div></div>';
    section.open = false;
    return section_html;
  };

  function _getSection(secname) {
    return self.current.sections[secname];
  }
  /*----------------------------->
  	D | Footer
  <-----------------------------*/


  this.renderEditorFooter = function (specs) {
    var fBM = new Benchmarker();
    var customMenu = true;
    specs = specs || this.specs || {};
    var menu = specs.minimenu || listMode && specs.schema && specs.schema.listmenu || specs.listmenu || //list mode
    multiEdit && (specs.multimenu || specs.schema && specs.schema.multimenu || __defaultMultiButtons) || specs.menu || null;

    if (!menu) {
      menu = __defaultObjectButtons;
      customMenu = false;
    }
    /*if(typeof menu =='function'){
    	menu = menu();
    }*/


    menu = self.propAsFuncOrValue(menu);
    var html = '<div class="joe-panel-footer">' + '<div class="joe-panel-menu">';
    menu.map(function (m) {
      html += self.renderFooterMenuItem(m);
    }, this); //add default list buttons.

    if (!specs.minimenu && self.current.list && $.type(self.current.data) == 'array') {
      html += self.renderFooterMenuItem(__selectAllBtn__);
      html += '<div class="joe-selection-indicator"></div>'; //remove multiedit if menu passed.

      if (!customMenu) {
        html += self.renderFooterMenuItem({
          label: 'Multi-Edit',
          name: 'multiEdit',
          css: 'joe-multi-only',
          action: 'getJoe(' + self.joe_index + ').editMultiple()'
        });
      }
    }

    html += __clearDiv__ + '</div>' + '</div>';

    _bmResponse(fBM, '[Footer] rendered');

    return html;
  };

  this.renderFooterMenuItem = function (m) {
    //passes a menu item
    return self.renderMenuButtons(m, 'joe-footer-button');
  };

  this.renderMenuButtons = function (button, menucss) {
    /*|{
     description:'function for rendering standar menu buttons',
     tags:'render, menu, button',
     category:'core'
     }|*/
    if (!button) {
      return '';
    }

    var button = $.type(button) == "array" ? button : [button];
    var menucss = menucss || '';
    var display,
        action,
        html = '',
        m;

    for (var b = 0, tot = button.length; b < tot; b++) {
      m = button[b];
      var schema = self.propAsFuncOrValue(m.schema);

      if (!m) {
        logit('error loading menu button');
        continue; //return '';
      }

      if (m.condition && !self.propAsFuncOrValue(m.condition)) {
        //return '';
        continue;
      }

      display = fillTemplate(self.propAsFuncOrValue(m.label || m.display || m.name), self.current.object);

      if (!m.callback) {
        action = fillTemplate(self.propAsFuncOrValue(m.action), self.current.object) || 'alert(\'' + display + '\')';
      } else {
        action = 'getJoe(' + self.joe_index + ').Cache.callback(\'' + m.callback + '\');';
      } // html += '<div class="joe-button ' + menucss + ' ' + (m.css || '') + '" onclick="' + action + '" data-btnid="' + (m.id||m.name) + '" title="' + (m.title || '') + '">' + display + '</div>';


      html += "<joe-button class=\"joe-button  " + menucss + " " + (m.css || '') + "\" action=\"" + action + "\" \n            data-btnid=\"" + (m.id || m.name) + "\" \n            " + (schema && "schema=\"" + schema + "\"" || '') + " \n            " + (m.color && "color=\"" + m.color + "\"" || '') + " \n            title=\"" + (m.title || display || '') + "\">\n            " + display + "</joe-button>";
    }

    return html;
  };
  /*-------------------------------------------------------------------->
  	3 | OBJECT FORM
  <--------------------------------------------------------------------*/


  var preProp;
  var prePropWidths = 0;

  this.renderObjectField = function (prop, mini) {
    /*|{
        featured:true,
        tags:''
    }|*/
    if (prop.hasOwnProperty('condition') && !self.propAsFuncOrValue(prop.condition)) {
      return '';
    }

    var joeFieldBenchmarker = new Benchmarker(); //field requires {name,type}
    //if(!mini){

    self.current.fields.push(prop); //}
    //value

    prop.value = prop.asFunction ? prop.value : self.propAsFuncOrValue(prop.value); //format

    if (prop.format && $.type(prop.format == 'function')) {
      prop.value = prop.format(prop.value, prop, self.current.item);
    } //set default value


    if (prop.value == undefined && prop['default'] != undefined) {
      prop.value = prop['default'];
    } //reset


    if (!self.current.reset[prop.name]) {
      self.current.reset[prop.name] = _typeof(prop.value) == "object" ? $.extend({}, prop.value) : prop.value;
    }

    prop.reset = self.current.reset[prop.name]; // || prop.value;
    //hidden

    var hidden = '';
    var qHidden = self.propAsFuncOrValue(prop.hidden);

    if (qHidden) {
      if (parseBoolean(qHidden) === undefined) {
        var negated = qHidden[0] == '!';
        var hiddenPropVal = $.extend({}, self.current.object, self.constructObjectFromFields())[qHidden.replace('!', '')];
        hidden = negated ^ !!hiddenPropVal ? 'hidden' : '';
      } else {
        hidden = 'hidden';
      }
    } //Locked


    prop.locked = self.propAsFuncOrValue(prop.locked); //required

    var required = '';

    if (self.propAsFuncOrValue(prop.required)) {
      required = 'joe-required';
    }

    var html = ''; //propdside

    var propdside = prop.sidebar_side || prop.side || renderFieldTo; //add clear div if the previous fields are floated.

    if (preProp) {
      //TODO:deal with 50,50,50,50 four way float
      //prePropWidths
      if (preProp.width && !prop.width) {
        //if((preProp.width && !prop.width)||){
        html += '<div class="clear"></div>';
      }
    }

    var containercolor = self.propAsFuncOrValue(prop.containercolor, null, null, _jco(true)[prop.name]) || '';

    if (containercolor) {
      containercolor = ' background-color:' + containercolor + ';';
    }

    var sizestyles = " ";

    if (prop.height) {
      sizestyles += "min-height:" + prop.height + '; ';
    }

    if (prop.width) {
      html += '<joe-field-container class="joe-field-container joe-fleft" style="width:' + prop.width + '; ' + sizestyles + containercolor + '" data-side="' + propdside + '">';
    } else {
      html += '<joe-field-container field="' + prop.name + '" class="joe-field-container" data-side="' + propdside + '" style="' + sizestyles + containercolor + '">';
    }

    var proptype = self.propAsFuncOrValue(prop.type, self.current.constructed);
    var fieldlabel = self.propAsFuncOrValue(prop.display || prop.label || prop.name);
    var icon = self.propAsFuncOrValue(prop.icon);

    if (icon && self.schemas[icon] && self.schemas[icon].menuicon) {
      icon = self.schemas[icon].menuicon;
    }

    var hiddenlabel = prop.label === false ? ' hide-label ' : '';
    html += '<joe-field class="joe-object-field ' + hidden + ' ' + required + ' ' + proptype + '-field ' + hiddenlabel + '" data-type="' + proptype + '" data-name="' + prop.name + '">' + renderFieldAttribute('before') + '<label class="joe-field-label ' + (icon && 'iconed' || '') + ' " title="' + prop.name + '">' + (icon || '') + fillTemplate(fieldlabel, self.current.object) + (required && '*' || '') + self.renderFieldTooltip(prop) + '</label>'; //render comment

    html += self.renderFieldComment(prop); //add multi-edit checkbox

    if (self.current.userSpecs && self.current.userSpecs.multiedit) {
      html += '<div class="joe-field-multiedit-toggle" onclick="$(this).parent().toggleClass(\'multi-selected\')"></div>';
    }

    html += self.selectAndRenderFieldType(prop);
    html += self.renderGotoLink(prop);
    html += renderFieldAttribute('after');
    html += '</joe-field>'; //close object field;
    //if(prop.width){
    //close field container

    html += '</joe-field-container>'; //	}

    function renderFieldAttribute(attribute) {
      var obj = rerenderingField ? self.current.constructed : self.current.object;
      var propval = prop[attribute];
      return propval && '<joe-field-attribute class="jfa-' + attribute + '">' + fillTemplate(self.propAsFuncOrValue(propval), obj) + '</joe-field-attribute>' || '';
    }

    preProp = prop;

    _bmResponse(joeFieldBenchmarker, 'field ' + prop.name + ' ' + prop.type);

    return html;
  };

  this.renderGotoLink = function (prop, style) {
    var _goto = self.propAsFuncOrValue(prop["goto"]);

    if (_goto) {
      var action = '';

      if ($.type(_goto) == "string") {
        action = _goto; //action = 'getJoe('+self.joe_index+').gotoItemByProp(\''+prop.name+'\',$(this).siblings().val(),\''+goto+'\');';

        action = 'getJoe(' + self.joe_index + ').gotoFieldItem(this,\'' + _goto + '\');';
      } else {//var values= self.getField(prop.name);
      }

      var btnHtml = '';

      switch (style || prop.type) {
        case 'select':
          btnHtml += '<div class="joe-button inline joe-view-button joe-iconed-button" onclick="' + action + '" title="view ' + _goto + '">view</div>';
          break;
      }

      return btnHtml;
    } else {
      return '';
    }
  };

  this.gotoFieldItem = function (dom, schema) {
    var fieldname = $(dom).parents('.joe-object-field').data('name');
    var field = self.getField(fieldname);
    var idprop = field.idprop || self.getIDProp(schema); //var values = self.propAsFuncOrValue(field.values);

    var values = self.getFieldValues(field.values, field);
    var id = $(dom).siblings('.joe-field').val();
    var object = values.filter(function (v) {
      return v[idprop] == id;
    })[0] || false;

    if (!object) {
      return false;
    }

    self.show(object, {
      schema: schema
    });
  };

  this.renderFieldComment = function (prop) {
    var comment = self.propAsFuncOrValue(prop.comment);

    if (!comment) {
      return '';
    } //var comment = ($.type(prop.comment) == "function")?prop.comment():prop.comment;


    var comment_html = '<div class="joe-field-comment">' + comment + '</div>';
    return comment_html;
  };

  this.renderFieldTooltip = function (prop) {
    if (!prop.tooltip) {
      return '';
    } //var tooltip_html = '<p class="joe-tooltip">'+prop.tooltip+'</p>';


    var tooltip_html = '<span class="joe-field-tooltip" title="' + __removeTags(prop.tooltip) + '">i</span>';
    return tooltip_html;
  };

  this.selectAndRenderFieldType = function (prop) {
    //var joeFieldBenchmarker = new Benchmarker();
    var html = '';
    var proptype = (self.propAsFuncOrValue(prop.type) || '').toLowerCase();

    if (!proptype) {
      console.error(prop);
      self.propAsFuncOrValue(prop.type);
    }

    switch (proptype) {
      case 'select':
        html += self.renderSelectField(prop);
        break;

      case 'multisort':
      case 'multisorter':
        html += self.renderMultisorterField(prop);
        break;

      /*case 'sorter':
      	html+= self.renderSorterField(prop);
      	break;*/

      /*			case 'multi-select':
       html+= self.renderMultiSelectField(prop);*/
      //break;

      case 'guid':
        html += self.renderGuidField(prop);
        break;

      case 'number':
        html += self.renderNumberField(prop);
        break;

      case 'int':
        html += self.renderIntegerField(prop);
        break;

      /*            case 'textarea':
       prop.type = 'rendering';*/

      /*case 'code':
      	html+= self.renderCodeField(prop);
      	break;*/

      /*case 'rendering':
      	html+= self.renderRenderingField(prop);
      	break;*/

      case 'date':
        html += self.renderDateField(prop);
        break;

      case 'boolean':
        html += self.renderBooleanField(prop);
        break;

      case 'geo':
      case 'map':
        html += self.renderGeoField(prop);
        break;

      case 'image':
      case 'img':
        html += self.renderImageField(prop);
        break;

      case 'buckets':
        html += self.renderBucketsField(prop);
        break;

      case 'content':
        html += self.renderContentField(prop);
        break;

      case 'url':
        html += self.renderURLField(prop);
        break;

      /*case 'objectlist':
      	html+= self.renderObjectListField(prop);
      	break;*/

      /*case 'objectreference':
          html+= self.renderObjectReferenceField(prop);
          break;*/

      case 'tags':
        html += self.renderTagsField(prop);
        break;

      case 'color':
        html += self.renderColorField(prop);
        break;

      case 'group':
        html += self.renderCheckboxGroupField(prop);
        break;

      case 'uploader':
        html += self.renderUploaderField(prop);
        break;

      case 'preview':
        html += self.renderPreviewField(prop);
        break;

      /*            case 'wysiwyg':
                      html+= self.renderTinyMCEField(prop);
                      //html+= self.renderCKEditorField(prop);
                      break;*/

      case 'passthrough':
        html += self.renderPassthroughField(prop);
        break;

      default:
        if (self.Fields[proptype] && self.Fields[proptype].render) {
          html += self.Fields[proptype].render(prop);
        } else {
          html += self.renderTextField(prop);
        }

        break;
    } //_bmResponse(joeFieldBenchmarker,'Joe rendered '+(prop.name||"a field"));
    //logit('Joe rendered '+(prop.name||"a field")+' in '+joeFieldBenchmarker.stop()+' seconds');


    return html;
  };
  /*----------------------------->
  	0 | Event Handlers
  <-----------------------------*/


  this.getActionString = function (evt, prop) {
    var evt = prop[evt];

    if (!evt) {
      return '';
    }

    if ($.type(evt) == "string") {
      return evt;
    }

    var str = prop[evt] ? ' ' + self.functionName(prop[evt]) + '(this); ' : '';
    return str;
  };

  function _getPropPlaceholder(prop) {
    var placeholder = self.propAsFuncOrValue(prop.placeholder || prop.display || prop.name) || '';

    if (prop.placeholder === false) {
      return '';
    }

    return placeholder && ' placeholder="' + placeholder + '"' || ''; //return ((placeholder && 'placeholder="'+placeholder+'"')||'');
  }

  this.renderFieldAttributes = function (prop, evts) {
    evts = evts || {};
    var bluraction = ''; //var updateaction = '';

    var changeaction = '';
    var keypressaction = '';
    var keyupaction = '';
    var rerender = prop.rerender ? ' getJoe(' + self.joe_index + ').rerenderField(\'' + prop.rerender + '\'); ' : '';
    var profile = self.current.profile;
    var disabled = profile.lockedFields.indexOf(prop.name) == -1 ? '' : 'disabled';

    if (evts.onblur || prop.onblur) {
      bluraction = 'onblur="' + (evts.onblur || '') + ' ' + self.getActionString('onblur', prop) + '"';
    }

    if (evts.onchange || prop.onchange || rerender) {
      changeaction = 'onchange="' + rerender + (evts.onchange || '') + ' ' + self.getActionString('onchange', prop) + '"';
    }

    if (evts.onkeypress || prop.onkeypress) {
      keypressaction = 'onkeypress="' + (evts.onkeypress || '') + ' ' + self.getActionString('onkeypress', prop) + '"';
    }

    if (evts.onkeyup || prop.onkeyup) {
      keyupaction = 'onkeyup="' + (evts.onkeyup || '') + ' ' + self.getActionString('onkeyup', prop) + '"';
    }

    return ' ' + keyupaction + ' ' + keypressaction + ' ' + bluraction + ' ' + changeaction + ' ' + disabled + ' ';
  };

  function _disableField(prop) {
    return prop.hasOwnProperty('locked') && self.propAsFuncOrValue(prop.locked) && ' disabled ' || '';
  }
  /*----------------------------->
  	A | Text Input
  <-----------------------------*/


  function cleanString(value) {
    return ((value || '') + '').replace(/\"/g, "&quot;");
  }

  this.renderTextField = function (prop) {
    /*|{featured:true,
        tags:'field, render,text',
        description:'This renders a text field control. This is the default field type if none specified'
    }|*/
    var autocomplete;

    if (prop.autocomplete && prop.values) {
      if (typeof prop.values == "function") {
        prop.values = prop.values(self.current.object);
      }

      if ($.type(prop.values) == 'string' && self.getDataset(prop.values)) {
        prop.values = self.getDataset(prop.values);
      }

      if ($.type(prop.values) != 'array') {
        prop.values = [prop.values];
      }

      autocomplete = prop.autocomplete;
    } //TODO: Use jquery ui autocomplete


    var disabled = _disableField(prop); //(prop.locked &&'disabled')||'';


    var fieldtype = prop.ftype && "data-ftype='" + prop.ftype + "'" || '';

    var placeholder = _getPropPlaceholder(prop);

    var ac_function = "document.querySelector('joe-autocomplete[field='" + prop.name + "']').search(this.val)";
    var html =
    /*((autocomplete && '<div class="joe-text-autocomplete-label"></div>')||'')+*/
    '<input class="joe-text-field joe-field ' + (prop.skip && 'skip-prop' || '') + '" ' + placeholder + //add placeholder
    'type="text"  ' + disabled + ' name="' + prop.name + '" value="' + cleanString(prop.value || '') + '" maxlength="' + (prop.maxlength || prop.max || '') + '" ' + self.renderFieldAttributes(prop) + (autocomplete && //' onblur="getJoe('+self.joe_index+').hideTextFieldAutoComplete($(this));"'
    //' onkeyup="getJoe('+self.joe_index+').showTextFieldAutoComplete($(this));"'
    " onkeyup='this.nextSibling.search(this.value)'" || '') + ' ' + fieldtype + '/>';

    if (autocomplete) {
      /*html+='<div class="joe-text-autocomplete">';
               var ac_opt;
               var ac_template = autocomplete.template||'${name}';
               var ac_id, ac_title;
      for(var v = 0, len = prop.values.length; v < len; v++){
                   ac_opt = ($.type(prop.values[v]) == "object")?
                       prop.values[v]:
                       {id:prop.values[v],name:prop.values[v]};
                   ac_title = fillTemplate(self.propAsFuncOrValue(ac_template,ac_opt,null,self.current.object),ac_opt);
                   ac_id = (autocomplete.value && fillTemplate(self.propAsFuncOrValue(autocomplete.value,ac_opt),ac_opt))
                       ||(autocomplete.idprop && ac_opt[autocomplete.idprop])
                       ||ac_opt._id||ac_opt.id||ac_opt.name;
      	html+='<div class="joe-text-autocomplete-option" '
      		+'onclick="getJoe('+self.joe_index+').autocompleteTextFieldOptionClick(this);" '
                       +'data-value="'+ac_id+'">'+ac_title+'</div>';
      }
                 html+='</div>';*/
      html += "<joe-autocomplete class=\"joe-text-autocomplete\" \n                field=\"" + prop.name + "\" \n                itemId=\"" + self.current.object._id + "\" \n                schema=\"" + (self.current.object.itemType || self.current.schema && self.current.schema.name || '') + "\" \n                joe-index=" + self.joe_index + "\n                \">\n            </joe-autocomplete>";
    } //add onblur: hide panel


    return html;
  };

  var textFieldAutocompleteHandler = function textFieldAutocompleteHandler(e) {
    var dom = e.target; //if($(e.target).parents('.joe-text-autocomplete'));

    $('.joe-text-autocomplete').removeClass('active');
    $('body').unbind("click", textFieldAutocompleteHandler);
  };

  this.showTextFieldAutoComplete = function (dom) {
    $('body').unbind("click", textFieldAutocompleteHandler).bind("click", textFieldAutocompleteHandler);
    var autocomplete = dom.next('.joe-text-autocomplete');
    var content, show;
    needles = dom.val().toLowerCase().replace(/,/, ' ').split(' '); //needles.removeAll('');

    autocomplete.find('.joe-text-autocomplete-option').each(function (i, obj) {
      content = obj.textContent === undefined ? obj.innerText : obj.textContent; //content = obj.innerHTML;
      //self.checkAutocompleteValue(dom.val().toLowerCase(),content,obj);

      show = self.checkAutocompleteValue(needles, content.replace(/,/, ' ').toLowerCase(), obj);
      $(obj).toggleClass('visible', show);
    });
    autocomplete.addClass('active');
  };

  this.hideTextFieldAutoComplete = function (dom) {
    var autocomplete = dom.next('.joe-text-autocomplete');
    autocomplete.removeClass('active');
  };
  /*	this.autocompleteTextFieldOptionClick = function(dom){
          var value = ($(dom).data('value'));
  		$(dom).parent().prev('.joe-text-field').val(value);
  		$(dom).parent().removeClass('active');
          $(dom).parent().siblings('.joe-reference-add-button').click();
  		//$(dom).previous('.joe-text-field').val($(dom).html());
      };*/


  this.autocompleteTextFieldOptionClick = function (dom) {
    var value = $(dom).data('value');
    var parent = $(dom).parents('joe-autocomplete');
    parent.prev('.joe-text-field').val(value);
    parent.removeClass('active');
    parent.siblings('.joe-reference-add-button').click(); //$(dom).previous('.joe-text-field').val($(dom).html());
  };

  this.checkAutocompleteValue = function (needles, haystack, dom, additive) {
    var d = $(dom); //var needles = needle.replace( /,/,' ').split(' ');

    if (!needles.length) {
      return true; //d.addClass('visible');
    }

    for (var n = 0, tot = needles.length; n < tot; n++) {
      if (haystack.indexOf(needles[n]) == -1) {
        //needle not in haystack
        return false;
      } //d.removeClass('visible');

    }

    return true; //d.addClass('visible');

    /*        var overlap = needles.filter(function(n) {
                return haystacks.indexOf(n) != -1
            });
            if(overlap.length || !needle){
                d.addClass('visible');
            }else{
                d.removeClass('visible');
            }*/

    /* if(haystack.toLowerCase().indexOf(needle) != -1 || !needle){
    d.addClass('visible');
    }else{
    d.removeClass('visible');
    }*/
  };

  encapsulateFieldType('text', self.renderTextField);
  /*----------------------------->
  	A.2 | Password
  <-----------------------------*/

  this.renderPasswordField = function (prop) {
    /*|{
    description:'forces a float value',
     tags:'render,field,password'
     }|*/
    var disabled = _disableField(prop);

    var html = '<input class="joe-password-field joe-field" type="password" ' + disabled + ' name="' + prop.name + '" value="' + (prop.value || '') + '"  ' + self.renderFieldAttributes(prop) + ' />';
    return html;
  };

  encapsulateFieldType('password', self.renderPasswordField);
  /*----------------------------->
  	B | Number/Int Input
  <-----------------------------*/

  this.renderNumberField = function (prop) {
    /*|{
    description:'forces a float value',
    params:{step:'how many decimals, use any'},
     tags:'render,field,number'
     }|*/
    var step = prop.step || "any";

    var disabled = _disableField(prop);

    var placeholder = _getPropPlaceholder(prop);

    var html = '<input class="joe-number-field joe-field" type="number" ' + disabled + ' ' + placeholder + ' step="' + step + '" name="' + prop.name + '" value="' + (prop.value || '') + '"  ' + self.renderFieldAttributes(prop, {
      onblur: 'getJoe(' + self.joe_index + ').returnNumber(this);'
    }) + ' />';
    return html;
  };

  this.returnNumber = function (dom) {
    if (!$(dom).val()) {
      return;
    }

    $(dom).val(parseFloat($(dom).val()));
  };

  encapsulateFieldType('number', self.renderNumberField);
  /*----------------------------->
   B.2
   <-----------------------------*/

  this.renderIntegerField = function (prop) {
    /*|{
     tags:'render,field,integer'
     }|*/

    /*var disabled = (profile.lockedFields.indexOf(prop.name) != -1 || prop.locked)?
    	'disabled':'';
    */
    //bluraction
    //var bluraction = 'onblur="_joe.returnInt(this); '+self.getActionString('onblur',prop)+'"';
    var placeholder = _getPropPlaceholder(prop);

    var html = '<input class="joe-int-field joe-field" type="text" ' + placeholder + ' name="' + prop.name + '" value="' + (prop.value || '') + '"  ' + self.renderFieldAttributes(prop, {
      onblur: 'getJoe(' + self.joe_index + ').returnInt(this);'
    }) + ' />';
    return html;
  };

  this.returnInt = function (dom) {
    if (!$(dom).val()) {
      return;
    }

    $(dom).val(parseInt($(dom).val()));
  };

  encapsulateFieldType('integer', self.renderIntegerField);
  /*----------------------------->
  	C | Select
  <-----------------------------*/

  this.renderSelectField = function (prop, subObject) {
    /*|{
     tags:'render,field,select'
     }|*/
    var disabled = _disableField(prop);

    var subObject = subObject || {}; //(prop.hasOwnProperty(prop.locked) && self.propAsFuncOrValue(prop.locked) &&'disabled')||'';

    /*var values = ($.type(prop.values) == 'function')?prop.values(self.current.object):prop.values || [prop.value];*/

    var values = self.getFieldValues(prop.values, prop);
    var valObjs = [];

    if (prop.blank) {
      switch ($.type(prop.blank)) {
        case 'string':
          valObjs.push({
            name: prop.blank || ''
          });
          break;

        case 'object':
          valObjs.push({
            name: prop.blank.name || '',
            value: prop.blank.value || ''
          });
          break;

        default:
          valObjs.push({
            name: ''
          });
          break;
      }
    }

    values.map(function (v) {
      if ($.type(v) != 'object') {
        valObjs.push({
          name: v
        });
      } else {
        valObjs.push(v);
      }
    });
    /*		if($.type(values[0]) != 'object'){
    			values.map(function(v){
    				valObjs.push({name:v});
    			});
    		}
    		else{
    			valObjs = values;
    		}*/

    var mul = prop.multiple || prop.allowMultiple;
    var selected;
    var multiple = mul ? ' multiple ' : '';
    var selectSize = prop.size || valObjs.length * .5 > 10 ? 10 : valObjs.length / 2;

    if (!prop.size && !mul) {
      selectSize = 1;
    }

    var style = 'style="';

    if (prop.minwidth) {
      style += 'min-width:' + self.propAsFuncOrValue(prop.minwidth);
    }

    style += '"';
    var html = '<select ' + disabled + ' class="joe-select-field joe-field" name="' + prop.name + '" ' + style + ' value="' + (prop.value || '') + '" size="' + selectSize + '"' + self.renderFieldAttributes(prop) + multiple + ' >';
    var template; // = self.propAsFuncOrValue(prop.template) || '';

    var val;
    var optionVal;
    var propertyValue;
    var prop_idprop;
    valObjs.map(function (v) {
      propertyValue = subObject[prop.name] || self.propAsFuncOrValue(prop.value, null, null, v);
      template = self.propAsFuncOrValue(prop.template, null, null, v) || '';
      optionVal = template ? fillTemplate(template, v) : v.display || v.label || v.name;
      prop_idprop = prop.idprop && self.propAsFuncOrValue(prop.idprop, v, null, _jco()) || null;
      val = prop_idprop && v.hasOwnProperty(prop_idprop) ? v[prop_idprop] : v._id || v.value || v.name || '';

      if ($.type(propertyValue) == 'array') {
        selected = '';
        selected = prop.value.indexOf(val) != -1 ? 'selected' : '';
        /*prop.value.map(function(pval){
        	if(pval.indexOf)
        });*/
      } else {
        selected = propertyValue == val ? 'selected' : '';
      }

      html += '<option value="' + val + '" ' + selected + (self.propAsFuncOrValue(v.disabled) && 'disabled="disabled"' || '') + '>' + optionVal + '</option>';
    });
    html += '</select>';
    return html;
  };

  encapsulateFieldType('select', self.renderSelectField);
  /*----------------------------->
  	D | Date Field
  <-----------------------------*/

  this.renderDateField = function (prop, subObject) {
    /*|{
     tags:'render,field,date'
     }|*/
    var subObject = subObject || {};

    var placeholder = _getPropPlaceholder(prop);

    var html = '<input class="joe-date-field joe-field" type="text"  ' + placeholder + ' ' + _disableField(prop) + ' name="' + prop.name + '" value="' + (subObject[prop.name] || prop.value || '') + '" ' + self.renderFieldAttributes(prop) + ' />';
    html += '<joe-datepicker-holder id="' + prop.name + '"></joe-datepicker-holder>';
    return html;
  };

  this.readyDateField = function (prop) {
    var inObjectList;
    var field;
    self.overlay.find('.joe-date-field').each(function (i, dom) {
      field = _joe.getField(dom.name);

      if ($(dom).parents('.joe-objectlist-row').length) {
        $(dom).Zebra_DatePicker({
          // in objectlist-row
          //always_visible:$('joe-datepicker-holder#'+$(dom).attr('name')),
          //offset:[5,20],
          format: 'm/d/Y',
          first_day_of_week: 0,
          readonly_element: false,
          //open_on_focus:true,
          start_date: new Date().format('m/d/y') //container:$('joe-datepicker-holder#'+$(dom).attr('name'))

        });
      } else {
        //not in objectlist
        $(dom).Zebra_DatePicker({
          always_visible: self.propAsFuncOrValue(field.always_visible) && $('joe-datepicker-holder#' + $(dom).attr('name')) || false,

          /*offset:[5,20],*/
          format: 'm/d/Y',
          first_day_of_week: 0,
          readonly_element: false,
          //open_on_focus:true,
          container: $('joe-datepicker-holder#' + $(dom).attr('name'))
        });
      }
    });
  };

  encapsulateFieldType('date', self.renderDateField, {
    ready: self.readyDateField
  });
  /*----------------------------->
  D.2 | Time Field
  <-----------------------------*/

  /*	this.renderTimeField = function(prop){
  
          var html=
  		'<input class="joe-time-field joe-field" type="time"  '+_disableField(prop)+' name="'+prop.name+'" value="'+(prop.value || '')+'" '+
  			self.renderFieldAttributes(prop)+
  		' />';
  
  		return html;
  	};*/

  this.renderTimeField = function (prop) {
    /*|{
     tags:'render,field,time',
     specs:''
     }|*/
    var html = '<input class="joe-time-field joe-field" type="text" ' + _disableField(prop) + ' name="' + prop.name + '" value="' + (prop.value || '') + '" ' + self.renderFieldAttributes(prop) + ' />';
    return html;
  };

  this.readyTimeField = function (prop) {
    var inObjectList;
    var field;
    self.overlay.find('.joe-time-field').each(function (i, dom) {
      field = _joe.getField(dom.name);
      $(dom).timepicker({
        timeFormat: 'h:mm p',
        minTime: field.minTime,
        maxTime: field.maxTime,
        startTime: field.startTime,
        endTime: field.endTime,
        dropdown: false
      });
    });
  };

  encapsulateFieldType('time', self.renderTimeField, {
    ready: self.readyTimeField
  });
  /*----------------------------->
  	E | Geo Field
  <-----------------------------*/

  this.renderGeoField = function (prop) {
    /*|{
        tags:'render,field,geo',
        specs:'center:[lat,long],hideAttribution:bool,geometry:"point/polygon"'
    }|*/
    var propLocation;

    try {
      propLocation = prop.value && eval(prop.value);
    } catch (e) {
      self.Error.add('error finding geo for ' + (prop.name || ''), e, prop.value);
    }

    var zoom = prop.zoom || 4; //map.setView([37.85750715625203,-96.15234375],3)
    //var map = L.map('map').setView(center, zoom);

    var mapDiv = 'joeGEO_' + prop.name;
    var geoval = tryEval(prop.value); //

    var val = prop.value || '';
    var geometrytype = prop.geometry || 'point';

    if (geoval && geoval[0] && $.type(geoval[0]) == "array") {
      var center = prop.center || [40.513, -96.020];
      geometrytype = "polygon";
    } else {
      var center = propLocation || prop.center || [40.513, -96.020];
    }

    var geofield = geometrytype == "polygon" ? '<textarea placeholder="enter geometry here" onchange="_joe.addMapPolygon(\'' + mapDiv + '\',this.value)" class="joe-geo-field joe-field"  name="' + prop.name + '" ' + _disableField(prop) + '>' + val.replace(/\n/g, '') + '</textarea>' : '<input class="joe-geo-field joe-field" type="text" value="' + val + '" name="' + prop.name + '" ' + _disableField(prop) + '/>';
    var html = '<div class="joe-geo-map joe-field" name="' + prop.name + '" id="' + mapDiv + '" ' + 'data-center="' + JSON.stringify(center) + '" data-zoom="' + zoom + '" ' + 'data-value="' + val + '" ' + 'data-geometrytype="' + geometrytype + '"' + 'data-hideattribution="' + (prop.hideAttribution || '') + '" ' + 'onload="getJoe(' + self.joe_index + ').initGeoMap(this);"></div>' + geofield + '<script type="text/javascript">setTimeout(function(){getJoe(' + self.joe_index + ').initGeoMap("' + mapDiv + '");},100)</script>';
    return html;
  };

  this.initGeoMap = function (id) {
    var mapspecs = $('#' + id).data();
    var fieldname = $('#' + id).attr('name');
    self.current.cache.leaflet = self.current.cache.leaflet || {};
    var map = self.current.cache.leaflet[id] = L.map(id).setView(mapspecs.center, mapspecs.zoom); //var map = L.map(id).setView([51.505, -0.09], 13);

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {//attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map); //add geocoder

    if (mapspecs.geometrytype == 'point') {
      var searchControl = new L.esri.Controls.Geosearch().addTo(map);
      searchControl.on("results", function (data) {
        var latlng = data.latlng;
        var map = self.current.cache.leaflet[$(data.target._input).parents('.joe-geo-map').attr('id')];

        if (map.marker) {
          map.marker.setLatLng(latlng);
        } else {
          self.addMapIcon(map, latlng);
        }

        $('input[name=' + map.prop + ']').val('[' + latlng.lat + ',' + latlng.lng + ']');
      });
      map.on('click', self.onMapClick);

      if (mapspecs.value) {
        self.addMapIcon(map, mapspecs.value);
      }
    } else if (mapspecs.geometrytype == 'polygon') {
      var geoval = tryEval(mapspecs.value);
      self.addMapPolygon(map, geoval);
    } //hide leaflet and esri attribution


    if (mapspecs.hideattribution) {
      $('.leaflet-control-attribution').hide();
    }

    map.prop = $('#' + id).attr('name');
  };

  this.onMapClick = function (e, s, d) {
    var map = e.type == "click" ? e.target : e.target.map || self.current.leaflet; //map.setView(e.latlng);

    var ll = e && e.latlng && (e.latlng.latlng || e.latlng) || this.getLatLng();
    map.setView(ll);

    if (e.type == "dragend") {} else if (map.marker) {
      map.marker.setLatLng(ll);
    } else {
      self.addMapIcon(map, ll);
    }

    $('input[name=' + map.prop + ']').val('[' + ll.lat + ',' + ll.lng + ']');
  };

  this.addMapIcon = function (map, latlng, specs) {
    try {
      if (tryEval(latlng) === null) {
        throw 'could not eval' + latlng;
      }

      specs = specs || {};
      var myIcon = L.icon({
        iconUrl: specs.icon || '/JsonObjectEditor/img/mapstar.png',
        iconSize: [30, 30] //iconAnchor: [22, 94],
        //popupAnchor: [-3, -76],
        //shadowUrl: 'my-icon-shadow.png',
        //shadowRetinaUrl: 'my-icon-shadow@2x.png',
        //shadowSize: [68, 95],
        //shadowAnchor: [22, 94]

      });
      map.marker = L.marker(latlng, {
        draggable: true,
        icon: myIcon
      }).addTo(map);
      map.marker.map = map;
      map.marker.on('dragend', self.onMapClick);
    } catch (e) {
      self.Error.add('error adding point to map', e, {
        latlng: latlng
      });

      if (map.marker) {
        map.removeLayer(map.marker);
      }
    }
  };

  this.addMapPolygon = function (map, geometry, specs) {
    var map = self.getMapFieldMap(map);

    try {
      geometry = tryEval(geometry);
      /* if(tryEval(geometry) === null){
           throw('could not eval'+geometry);
       }*/
      //eval('('+geometry+')');

      specs = specs || {};
      var myPoly = L.polygon(geometry);
      map.polygon = myPoly.addTo(map);
      map.polygon.map = map;
      map.fitBounds(myPoly.getBounds());
      $('textarea[name=' + map.prop + ']').val($('textarea[name=' + map.prop + ']').val().replace(/\n/g, ''));
    } catch (e) {
      self.Error.add('error adding polygon to map', e, {
        geometry: geometry
      });

      if (map.polygon) {
        map.removeLayer(map.polygon);
      }
    }
  };

  this.getMapFieldMap = function (id) {
    if (_typeof(id) == "object") {
      return id;
    }

    if (id.indexOf('joeGEO_') == -1) {
      id = 'joeGEO_' + id;
    }

    return self.current.cache.leaflet[id];
  };

  encapsulateFieldType('geo', self.renderGeoField, {
    ready: self.initGeoMap,
    addPoly: this.addMapPolygon
  });
  /*----------------------------->
  	F | Boolean
  <-----------------------------*/

  this.renderBooleanField = function (prop) {
    /*|{
        tags:'render,field,boolean'
    }|*/
    var profile = self.current.profile;
    var html = '<label class="joe-boolean-label" for="joe_checkbox-' + prop.name + '">' + '<input class="joe-boolean-field joe-field" type="checkbox" name="' + prop.name + '" id="joe_checkbox-' + prop.name + '" ' + ([true, 'true'].contains(prop.value) && 'checked' || '') + self.renderFieldAttributes(prop) + ' ' + _disableField(prop) + '/> <small>' + (prop.label || '') + '</small></label>';
    return html;
  };

  encapsulateFieldType('guid', self.renderGuidField);
  /*----------------------------->
  	G | Guid
  <-----------------------------*/

  this.renderGuidField = function (prop) {
    var profile = self.current.profile;

    var placeholder = _getPropPlaceholder(prop);

    var html = '<input class="joe-guid-field joe-field" type="text" ' + placeholder + ' name="' + prop.name + '" value="' + (prop.value || cuid()) + '"  disabled />';
    return html;
  };

  encapsulateFieldType('guid', self.renderGuidField);
  /*----------------------------->
  	H | Create
  <-----------------------------*/

  this.renderCreateField = function (prop) {
    var schema = self.schemas[prop.schema] || prop.schema;
    var schemaTitle = '';

    if (schema && schema.name) {
      schemaTitle = schema.name.toUpperCase();
    } else {
      return 'schema not found';
    }

    var action = '_joe.Fields.create.go(\'' + prop.name + '\')';
    html = "<joe-button schema=\"" + prop.schema + "\" icon=\"plus\" color=\"orangegrey\" action=\"_joe.Fields.create.go('" + prop.name + "')\" style=\"float:none\">\n        new <b>" + schemaTitle + "</b>\n        </joe-button>";
    return html;
  };

  this.gotoCreatedItem = function (propname) {
    var prop = self.getField(propname);

    if (!prop.schema) {
      return 'no schema selected';
    }

    if (self.isNewItem()) {
      if (prop.autosave) {
        self.updateObject();
      } else {
        alert('please save this item before creating another');
        return;
      }
    }

    var schema = self.schemas[prop.schema] || prop.schema;
    var overwrites = self.propAsFuncOrValue(prop["new"] || prop.overwrites) || {};

    var newitem = _joe.propAsFuncOrValue(schema["new"]);

    var item = $.extend({}, newitem, overwrites);
    goJoe(item, {
      schema: schema
    });
  };

  encapsulateFieldType('create', self.renderCreateField, {
    go: self.gotoCreatedItem
  });
  /*----------------------------->
  	I | Image
  <-----------------------------*/

  this.renderImageField = function (prop) {
    /*|{
        tags:'render,field,image',
        specs:'hidefield'
    }|*/
    var hidefield = self.propAsFuncOrValue(prop.hidefield);
    var html = '';

    if (!hidefield) {
      html += '<input class="joe-image-field joe-field" type="text" name="' + prop.name + '" value="' + (prop.value || '') + '" ' + self.renderFieldAttributes(prop) + ' onkeyup="_joe.updateImageFieldImage(this);" ' + _disableField(prop) + '/>';
    }

    html += '<img class="joe-image-field-image" src="' + (prop.value || '') + '" />' + '<span class="joe-image-field-size"></span>'; // +'<script>_joe.updateImageFieldImage($(\'input[name=thumbnail]\'),2000)</script>';

    return html;
  };

  this.updateImageFieldImage = function (dom, timeout) {
    /*if($(dom).is('img')) {//img
        var src = $(dom).parent().find('input.joe-image-field').val();
        //var img = $(dom).next('.joe-image-field-image');
        var img = $(dom).parent().find('.joe-image-field-image');
        img.attr('src', src);
    }else{//input*/
    logit('img update');
    var src = $(dom).val(); //var img = $(dom).next('.joe-image-field-image');

    var img = $(dom).parent().find('.joe-image-field-image');
    self.updateImageSize(img);
    img.attr('src', src); //   }
  };

  this.updateImageSize = function (img) {
    var jqImg = $(img); //if (!jqImg.hasClass('loaded')) {

    jqImg.siblings('.joe-image-field-size').html(jqImg.width() + 'w x ' + jqImg.height() + 'h'); //jqImg.addClass('loaded');
    // }
  };

  encapsulateFieldType('image', self.renderImageField);
  /*----------------------------->
  	J | Multisorter
  <-----------------------------*/

  this.renderMultisorterField = function (prop) {
    //var values = ($.type(prop.values) == 'function')?prop.values(self.current.object):prop.values||[];
    var values = self.getFieldValues(prop.values, prop);
    var valObjs = []; //sort values into selected or option

    var val;
    var optionsHtml = '';
    var selectionsHtml = '';
    var idprop = prop['idprop'] || 'id' || '_id';
    var template = prop.template || '${name} (${' + idprop + '})';
    var value = prop.value || [];
    var selectionsArray = Array(value.length);
    var li;

    function renderMultisorterOption(v) {
      var html = '<li data-id="' + v[idprop] + '" ondblclick="_joe.toggleMultisorterBin(this);">' + fillTemplate(template, v) + '</li>';
      return html;
    } //render selected list

    /*	values.map(function(v){
    	//li = renderMultisorterOption(v);//'<li data-id="'+v[idprop]+'" onclick="_joe.toggleMultisorterBin(this);">'+fillTemplate(template,v)+'</li>';
    	selectionsHtml += renderMultisorterOption(v);
    	});*/


    var val_index; //render options list

    if (!values) {
      values = []; //todo: do some error reporting here.
    }

    values.map(function (v) {
      li = renderMultisorterOption(v); //'<li data-id="'+v[idprop]+'" onclick="_joe.toggleMultisorterBin(this);">'+fillTemplate(template,v)+'</li>';

      val_index = value.indexOf(v[idprop]);

      if (val_index != -1) {
        //currently selected
        //selectionsHtml += li;
        selectionsArray[val_index] = li;
      } else {
        optionsHtml += li;
      }
    });
    var selectionsHtml = selectionsArray.join('');
    var height = prop.height && "style='max-height:" + prop.height + ";'" || '';
    var html = '<div class="joe-multisorter-field joe-field"  name="' + prop.name + '" data-ftype="multisorter" data-multiple="' + (prop.allowMultiple || 'false') + '">' + '<div class="joe-filter-field-holder"><input type="text"class="" onkeyup="_joe.filterSorterOptions(this);"/></div>' + '<p class="joe-tooltip"> double click or drag item to switch columns.</p>' + '<ul class="joe-multisorter-bin options-bin" ' + height + '>' + optionsHtml + '</ul>' + '<ul class="joe-multisorter-bin selections-bin" ' + height + '>' + selectionsHtml + '</ul>' + __clearDiv__ + '</div>';
    return html;
  };

  this.filterSorterOptions = function (dom) {
    var sorterBM = new Benchmarker();
    var query = $(dom).val().toLowerCase(); //$(dom).parent().next('.joe-multisorter-bin').find('li').each(function(){$(this).toggle($(this).html().toLowerCase().indexOf(query) != -1 );});

    $(dom).parents('.joe-field').find('.joe-multisorter-bin.options-bin').find('li').each(function () {
      $(this).toggle($(this).html().toLowerCase().indexOf(query) != -1);
    });

    _bmResponse(sorterBM, 'found results for: ' + query);
  };

  this.toggleMultisorterBin = function (dom) {
    var id = $(dom).data('id');
    var parent = $(dom).parents('.joe-multisorter-bin');
    var multisorter = parent.parents('.joe-multisorter-field');
    var target = parent.siblings('.joe-multisorter-bin');
    var newDom = parent.find('li[data-id=' + id + ']').detach(); //detach if no multiples allowed.

    /* if (!multisorter.data('multiple')) {
         newDom
     }*/

    target.prepend(newDom);
  };

  encapsulateFieldType('multisorter', self.renderMultisorterField);
  /*----------------------------->
  	K | Buckets
  <-----------------------------*/
  //TODO: progressively render bucket options.

  this.renderBucketsField = function (prop) {
    /*|{
    description:'renders buckets field',
    tags:'buckets,field,render',
    specs:['idprop','values','allowMultiple','template','bucketCount','bucketNames','bucketWidth']
     }|*/

    /*var values = ($.type(prop.values) == 'function')?prop.values(self.current.object):prop.values||[];*/
    var values = self.getFieldValues(prop.values, prop);
    var valObjs = [];
    var bucketNames = self.propAsFuncOrValue(prop.bucketNames) || []; //sort values into selected or option

    var val;
    var bucketCount = self.propAsFuncOrValue(prop.bucketCount) || 3; //(typeof prop.bucketCount =="function")?prop.bucketCount():prop.bucketCount || 3;

    var optionsHtml = '';
    var bucketsHtml = [];
    var value = prop.value || [[], [], []]; //add arrays to values

    for (var i = 0; i < bucketCount; i++) {
      bucketsHtml.push('');

      if (!value[i]) {
        value.push([]);
      }
    }

    var idprop = prop.idprop || '_id' || 'id';
    var template = prop.template || '${name} <br/><small>(${' + idprop + '})</small>';
    var lihtml;
    var selected; //populate selected buckets

    var foundItem;
    var bucketItem;
    var itemCss = 'joe-bucket-delete cui-block joe-icon';
    var selectedIDs = [];

    for (var i = 0; i < bucketCount; i++) {
      for (var li = 0; li < value[i].length; li++) {
        bucketItem = value[i][li];

        if ($.type(bucketItem) == "string") {
          //find object in values
          foundItem = values.filter(function (v) {
            return v[idprop] == bucketItem;
          })[0] || false;

          if (!foundItem) {
            foundItem = {};
            foundItem[idprop] = li;
          }

          selectedIDs.push(bucketItem);
          lihtml = self.renderBucketItem(foundItem, prop, false); //'<li data-value="'+foundItem[idprop]+'" >'+fillTemplate(template,foundItem)+'<div class="'+itemCss+'" onclick="$(this).parent().remove()"></div></li>';
        } else if ($.type(bucketItem) == "object") {
          foundItem = bucketItem;
          lihtml = self.renderBucketItem(foundItem, prop, true); //'<li data-isobject=true data-value="'+encodeURI(JSON.stringify(bucketItem))+'">'
          // +fillTemplate(template,bucketItem)
          //+'<div class="'+itemCss+'" onclick="$(this).parent().remove()"></div></li>';
        }

        bucketsHtml[i] += lihtml;
      }
    } //populate options list


    var optionTemplate = prop.optionTemplate || template;
    values.map(function (v) {
      lihtml = //self.renderBucketItem(foundItem,template,idprop,false);
      '<li data-value="' + v[idprop] + '" >' + fillTemplate(optionTemplate, v) + '<div class="joe-bucket-delete cui-block joe-icon" onclick="$(this).parent().remove()"></div></li>';
      selected = false; //loop over buckets

      /*if(!prop.allowMultiple){
          for(var i = 0; i < bucketCount; i++){
              if(value[i].indexOf(v[idprop]) != -1){//currently selected
                  bucketsHtml[i] += lihtml;
              }
          }
      }*/

      if (selectedIDs.indexOf(v[idprop]) == -1 || prop.allowMultiple) {
        optionsHtml += lihtml;
      }
    });

    function renderBucket(id, name, width) {
      var widthStr = width ? 'style="width:' + width + '"' : '';
      var nameStr = name ? '<div class="joe-bucket-label">' + name + '</div>' : '';
      return '<ul class="joe-buckets-bin selections-bin" ' + widthStr + '>' + nameStr + bucketsHtml[id] + '</ul>';
    } //renderHTML


    var html = '<div class="joe-buckets-field joe-field" name="' + prop.name + '" data-ftype="buckets">' + '<div class="joe-filter-field-holder"><input type="text"class="" onkeyup="_joe.filterBucketOptions(this);"/></div>' + '<div class="joe-buckets-field-holder" style="width:25%;">' + '<ul class="joe-buckets-bin options-bin ' + (prop.allowMultiple && 'allow-multiple' || '') + '">' + optionsHtml + '</ul>' + '</div>' + '<div class="joe-buckets-field-holder" style="width:75%;">';
    bucketsHtml.map(function (b, i) {
      html += renderBucket(i, bucketNames[i] || '', prop.bucketWidth);
    }); //+'<ul class="joe-buckets-bin selections-bin">'+bucketsHtml+'</ul>'

    html += __clearDiv__ + '</div>' //end buckets field holder
    + __clearDiv__ + '</div>';
    return html;
  };

  this.filterBucketOptions = function (dom) {
    var query = $(dom).val().toLowerCase();
    $(dom).parents('.joe-buckets-field').find('.options-bin').find('li').each(function () {
      $(this).toggle($(this).html().toLowerCase().indexOf(query) != -1);
    });
    logit(query);
  };

  this.readyBinFields = function () {
    try {
      self.overlay.find('.joe-multisorter-bin').sortable({
        connectWith: '.joe-multisorter-bin',
        placeholder: "joe-sortable-highlight",
        items: "li"
      });
      self.overlay.find('.joe-buckets-bin').sortable({
        connectWith: '.joe-buckets-bin',
        placeholder: "joe-sortable-highlight",
        items: "li",
        start: function start(event, ui) {
          sortable_index = ui.item.index();
          ui.item.parents();
        },
        update: function update(event, ui) {
          if (ui.sender && ui.sender.hasClass('options-bin') && ui.sender.hasClass('allow-multiple')) {
            //add the element back into the list.
            ui.sender.find('li').eq(sortable_index).before(ui.item.clone()); //ui.item.parents('.joe-buckets-bin');
          }
        }
      });
    } catch (e) {
      //logit('Error creating sortables:\n'+e);
      self.Error.add('Error creating sortables:\n' + e, e, {
        caller: 'self.readyBinFields'
      });
    }
  };

  this.renderBucketItem = function (item, fieldnameorobject, specs) {
    /*|{
     description:'renders a single bucket item, either from an id string or a passed object. set specs.isobject to true to use an object. pass the fielname as second parameter.',
     tags:'buckets,item,render',
     specs:['isobject','idprop','template']
     }|*/
    var fieldObj = $.type(fieldnameorobject) == "object" ? fieldnameorobject : self.getField(fieldnameorobject) || {};

    if (specs === true || specs === false) {
      specs = {
        isobject: specs
      };
    }

    var idprop = fieldObj.idprop || specs.idprop || '_id';
    var template = fieldObj.template || specs.template || '${name} <br/><small>(${' + idprop + '})</small>';
    var itemCss = 'joe-bucket-delete cui-block joe-icon';
    var value = fieldObj.isobject || specs.isobject ? 'data-isobject=true data-value="' + encodeURI(JSON.stringify(item)) + '"' : 'data-value="' + item[idprop] + '"';
    var bucketHtml = '<li ' + value + '>' + fillTemplate(template, item) + '<div class="' + itemCss + '" onclick="$(this).parent().remove()"></div></li>';
    return bucketHtml;
  };

  encapsulateFieldType('buckets', self.renderBucketsField, {
    create: self.renderBucketItem,
    ready: self.readyBinFields
  });
  /*----------------------------->
   L | Content
   <-----------------------------*/

  this.renderContentField = function (prop) {
    /*
     description:'renders content field',
     tags:'content,field,render',
     specs:['idprop','template','run','value']
     */
    var html = '';
    var itemObj = self.current.object;
    var idProp = prop.idprop || self.getIDProp();
    var constructedItem = self.current.constructed || {};

    if (!self.current.object[idProp] || constructedItem[idProp] && constructedItem[idProp] == self.current.object[idProp]) {
      itemObj = constructedItem;

      if (self.current.object[idProp] && constructedItem[idProp] == self.current.object[idProp]) {
        if (rerenderingField) {
          itemObj = $.extend({}, self.current.object, constructedItem);
        } else {
          itemObj = $.extend({}, constructedItem, self.current.object);
        }
      }
    }

    try {
      if (prop.run) {
        html += prop.run(itemObj, prop) || '';
      } else if (prop.template) {
        html += fillTemplate(self.propAsFuncOrValue(prop.template), itemObj);
      } else if (prop.value) {
        html += self.propAsFuncOrValue(prop.value);
      }
    } catch (e) {
      self.Error.add('error rendering field:' + e, e, {
        caller: 'self.renderContentField'
      });
      return 'error rendering field:' + e;
    }

    return html;
  };

  encapsulateFieldType('content', self.renderContentField);

  this.Render.fieldListItem = this.renderFieldListItem = function (item, contentTemplate, schema, specs) {
    /*|{
        description:'renders a preformated list item for use in itemexpanders, details page, etc',
        featured:true,
        specs:isObject,deleteButton,expander,gotoButton,itemMenu,schemaprop,nonclickable,bgcolor,stripecolor,action
        tags:'render, field list item',
        cssclass:'.joe-field-list-item || .joe-field-item'
    }|*/
    var specs = $.extend({
      isObject: false,
      deleteButton: false,
      expander: null,
      gotoButton: false,
      itemMenu: false,
      schemaprop: false,
      nonclickable: false,
      action: false,
      stripecolor: false,
      bgcolor: false,
      checkbox: false,
      value: ''
    }, specs);
    var schemaprop = specs.schemaprop && self.propAsFuncOrValue(specs.schemaprop, item);
    var schema = schemaprop && item[schemaprop] || schema || item.itemtype || item.type;
    var schemaobj = self.schemas[schema];
    var contentTemplate = self.propAsFuncOrValue(contentTemplate, null, null, item) || self.propAsFuncOrValue(schemaobj && (schemaobj.listTitle || schemaobj.listView && schemaobj.listView.title), item) || '<joe-title>${name}</joe-title><joe-subtext>${itemtype}</joe-subtext>';
    var idprop = specs.idprop || schemaobj && schemaobj.idprop || '_id';
    var hasMenu = specs.itemMenu && specs.itemMenu.length;
    var action = specs.action || ' onclick="goJoe(_joe.search(\'${' + idprop + '}\')[0],{schema:\'' + schema + '\'})" ';
    var nonclickable = self.propAsFuncOrValue(specs.nonclickable, item);
    var clickablelistitem = !specs.gotoButton && !nonclickable
    /* && !hasMenu*/
    ; //var clickablelistitem = !!clickable && (!specs.gotoButton && !hasMenu);

    var deleteHandler = specs.fieldname && '_joe.Fields.objectreference.remove(\'' + specs.fieldname + '\');' || '';
    var deleteButton = '<div class="joe-delete-button joe-block-button right" ' + 'onclick="$(this).parent().remove();' + deleteHandler + '">&nbsp;</div>';
    var expanderContent = renderItemExpander(item, specs.expander);
    var click = !clickablelistitem ? '' : action;
    var value = specs.isObject ? 'data-value="' + encodeURI(JSON.stringify(specs.value)) + '" data-isObject=true' : 'data-value="' + specs.value + '"';
    var html = fillTemplate("<joe-list-item schema=\"" + schema + "\" \n            itemId=\"" + item[idprop] + "\"\n            idprop=\"" + idprop + "\"\n            " + (specs.icon && "icon=\"" + specs.icon + "\"" || '') + "\n            class=\"" + (clickablelistitem && 'joe-field-list-item clickable' || 'joe-field-item') + (specs.deleteButton && ' deletable' || '') + (specs.gotoButton && ' gotobutton' || '') + (specs.itemMenu && ' itemmenu' || '') + (specs.stripecolor && ' striped' || '') + (specs.checkbox && ' checkboxed' || '') + (specs.expander && ' expander expander-collapsed' || '') + '" ' + (value || '') + '>' //if(clickableListItem){
    + (hasMenu && renderItemMenu(item, specs.itemMenu) || '') + self.Render.bgColor(item, specs.bgcolor) + (specs.deleteButton && deleteButton || '') + self.Render.itemCheckbox(item, schemaobj) + '<div class="joe-field-item-content ' + (nonclickable && 'nonclickable' || '') + '" ' + click + ' >' + self.propAsFuncOrValue(contentTemplate + __clearDiv__, item) + '</div>' + self.Render.stripeColor(item, specs.stripecolor) + self._renderExpanderButton(expanderContent, item)
    /* }else {
         //+click
         +((hasMenu && renderItemMenu(item, specs.itemMenu)) || '')
         + '<div class="joe-field-item-content" ' + click + '>'
         + (specs.deleteButton && deleteButton || '')
         + self._renderExpanderButton(expanderContent, item)
         + self.propAsFuncOrValue(contentTemplate, item)
         + '</div>'
     }*/
    + (specs.gotoButton && '${RUN[_renderGotoButton]}' || '') + expanderContent + '</joe-list-item>', item);
    return html;
  };
  /*----------------------------->
   M | URL
   <-----------------------------*/


  this.renderURLField = function (prop) {
    /*|{
          tags:'render,field,URL'
    }|*/
    var profile = self.current.profile;

    var disabled = _disableField(prop); // (prop.locked &&'disabled')||'';


    var prefix = self.propAsFuncOrValue(prop.prefix);
    prefix = fillTemplate(prefix || '', self.current.object);

    var html = prefix + ("<joe-button schema=\"" + (prop.schema || '') + "\" action=\"view\" class=\"joe-button\" color=\"green\" prefix=\"" + prefix + "\" ></joe-button>") + '<input class="joe-url-field joe-field" type="text" ' + self.renderFieldAttributes(prop) + 'name="' + prop.name + '" value="' + (prop.value || '') + '"  ' + disabled + ' />' + __clearDiv__;

    return html;
  };

  this.gotoFieldURL = function (dom, prefix) {
    var url = $(dom).siblings('.joe-url-field').val();
    window.open((prefix || '') + url);
  };

  encapsulateFieldType('url', self.renderURLField);
  /*----------------------------->
   N | Color
   <-----------------------------*/

  this.renderColorField = function (prop) {
    /*|{
        tags:'render,field,color',
        specs:''
    }|*/
    var html = '<input class="joe-color-field joe-field" type="text"  name="' + prop.name + '" value="' + (prop.value || '') + '" ' + self.renderFieldAttributes(prop) + ' />' + //add onblur: hide panel
    '<script>' + '_joe._colorFieldListener($(\'input.joe-color-field[name=' + prop.name + ']\')[0]);' + '$(\'input.joe-color-field[name=' + prop.name + ']\').keyup(_joe._colorFieldListener);' + '</script>';
    return html;
  };

  this._colorFieldListener = function (e) {
    var field = e.delegateTarget || e;
    var color = field.value;
    $(field).parents('.joe-object-field').css('background-color', color);
  };

  encapsulateFieldType('color', self.renderColorField);
  /*----------------------------->
   O | Object List Field
   <-----------------------------*/

  function getObjectlistSubProperty(prop) {
    /*|{
    description:'renders an object list field',
        tags:'render,field,objectList'
    }|*/
    var subprop = prop;

    if ($.type(prop) == "string") {
      subprop = {
        name: prop
      };
      subprop.name = subprop.name || prop;
    } else {
      if (subprop.extend && self.fields[subprop.extend]) {
        subprop = $.extend({}, subprop, self.fields[subprop.extend], subprop.specs || {});
        subprop.name = subprop.name || subprop.extend;
      }
    }

    return subprop;
  }

  this.renderObjectListField = function (prop) {
    /*|{
        tags:'render,field,objectlist',
        specs:'new,locked,max,value,properties,standard_field_properties'
    }|*/
    var hiddenHeading = prop.hideHeadings ? " hidden-heading " : '';
    var sortable = prop.sortable === false ? "" : " sortable ";
    var html = "<table class='joe-objectlist-table " + hiddenHeading + sortable + "' >" + self.renderObjectListHeaderProperties(prop) + self.renderObjectListObjects(prop) //render a (table/divs) of properties
    //cross reference with array properties.
    + "</table>";
    var max = prop.max;

    if ((!max || !prop.value || prop.value.length < max) && !prop.locked) {
      var addaction = 'onclick="getJoe(' + self.joe_index + ').addObjectListItem(\'' + prop.name + '\')"';
      html += '<div style="text-align:center;">';
      var doneaction = 'onclick="_joe.Fields.objectlist.editHandler($(this).parents(\'.joe-object-field\').find(\'.row-template\').eq(0),true)"';

      if (prop.hasOwnProperty('template')) {
        html += '<joe-button style="" class=" joe-done-button  joe-button joe-grey-button" ' + doneaction + '> - </joe-button>';
      }

      html += '<div class="joe-button joe-iconed-button joe-plus-button joe-orangegrey-button" ' + addaction + '> ' + (prop.label || "add item") + '</div>' + __clearDiv__ + '</div>';
    }

    return html;
  };

  this.objectlistdefaultproperties = ['name', '_id']; //render headers

  this.renderObjectListHeaderProperties = function (prop) {
    var properties = self.propAsFuncOrValue(prop.properties) || self.objectlistdefaultproperties;
    var property;
    var subprop;
    var width;
    var sortable = prop.sortable === false ? false : true;
    var onclick;
    var html = '<thead><tr>' + (sortable && '<th class="joe-objectlist-object-row-handle-header" ></th>' || '') + '<th class="joe-objectlist-object-row-template-header"></th>';

    for (var p = 0, tot = properties.length; p < tot; p++) {
      subprop = getObjectlistSubProperty(properties[p]);
      /*            if($.type(properties[p]) == "string"){
                      subprop = self.fields[subprop]||{name:properties[p]}
                      subprop.name = subprop.name || properties[p];
                  }*/

      property = {
        name: subprop.display || subprop.name,
        type: subprop.type || 'text'
      };

      if (sortable) {
        onclick = 'title="sort by ' + property.name + '" onclick="_joe.Fields.objectlist.sortBy(\'' + prop.name + '\',\'' + subprop.name + '\');"';
      } else {
        onclick = '';
      }

      width = subprop.width ? 'width="' + subprop.width + '"' : '';
      html += "<th " + onclick + " data-subprop='" + subprop.name + "' " + width + ">" + (subprop.display || subprop.name) + "</th>";
    }

    html += "<th class='joe-objectlist-delete-header'></th>";
    html += "</tr></thead>";
    return html;
  }; //render objects


  this.renderObjectListObjects = function (prop) {
    var objects = prop.value || (self.current.constructed || self.current.object)[prop.name] || prop['default'] || [];
    var properties = self.propAsFuncOrValue(prop.properties) || self.objectlistdefaultproperties;
    var html = '<tbody>';
    var obj;

    for (var o = 0, objecttot = objects.length; o < objecttot; o++) {
      obj = objects[o];
      html += self.renderObjectListRow(obj, properties, o, prop); //parse across properties
    }

    html += "</tbody>";
    return html;
  };

  function _objectlistTemplateClickHandler(templateDom, stopEditing) {
    var fieldname = $(templateDom).parents('.joe-object-field').data('name');
    var fieldObj = self.getField(fieldname);
    var templateContent;
    var subobj;

    var constructed = _jco(true);

    (stopEditing && $(templateDom).parents('.joe-object-field').find('.editing') || $(templateDom).parent().siblings('.editing')).each(function (index, dom) {
      subobj = constructed[fieldname][$(this).index()];
      templateContent = fillTemplate(self.propAsFuncOrValue(fieldObj.template, null, null, subobj), subobj);
      $(this).find('.row-template').html(templateContent);
      $(this).removeClass('editing');
    });

    if (!stopEditing) {
      $(templateDom).parents('.joe-objectlist-row').addClass('editing');
      $(templateDom).parents('.joe-objectlist-row').find('.joe-text-field,.joe-integer-field,.joe-number-field,select,textarea').eq(0)[0].focus();
    }
  }

  this.renderObjectListRow = function (object, objectListProperties, index, prop, editing) {
    var properties = self.propAsFuncOrValue(objectListProperties) || self.objectlistdefaultproperties;
    var prop, property;
    var sortable = prop.sortable === false ? false : true;
    var html = ''; //"<joe-objectlist-item>";
    //var html = "<tr class='joe-object-list-row' data-index='"+index+"'><td class='joe-objectlist-object-row-handle'>|||</td>";

    var templateContent = fillTemplate(self.propAsFuncOrValue(prop.template, null, null, object), object);
    html += "<tr class='joe-objectlist-row " + ((templateContent || editing) && 'templated' || '') + " " + (editing && 'editing' || '') + "' data-index='" + index + "'>" + (prop.locked ? "<td class='template-visible'></td>" : sortable && "<td class='sort-handle template-visible'><div class='joe-panel-button joe-objectlist-object-row-handle'>|||</div></td>" || '');
    var onclick = 'onclick="_joe.Fields.objectlist.editHandler(this);"';
    html += "<td class='row-template template-visible' " + onclick + " colspan='" + (properties.length + 1) + "'>" + templateContent + "</td>";
    var renderInput = {
      'text': self.renderTextField,
      'select': self.renderSelectField,
      'date': self.renderDateField,
      'content': self.renderContentField,
      'rendering': self.renderRenderingField,
      'boolean': self.renderBooleanField,
      'url': self.Fields.url.render
    };
    var subprop; //show all properties

    for (var p = 0, tot = properties.length; p < tot; p++) {
      subprop = getObjectlistSubProperty(properties[p]); //prop = ($.type(properties[p]) == "string")?{name:properties[p]}:prop;

      property = $.extend({
        name: subprop.name,
        type: subprop.type || 'text',
        value: object[subprop.name] || ''
      }, subprop);
      self.Fields['objectlist'].toReady.push(property.type);
      html += "<td>" + (renderInput[property.type] || self.Fields[property.type].render)(property, object) + "</td>";
    }

    var delaction = "onclick='getJoe(" + self.joe_index + ")._oldeleteaction(this);'";
    html += prop.locked ? "<td class='template-visible'></td>" : "<td class='template-visible'><div class='jif-panel-button joe-delete-button' " + delaction + ">&nbsp;</div></td>";
    html += '</tr>'; //</joe-objectlist-item>';

    return html;
  };

  this.addObjectListItem = function (fieldname, specs) {
    self.Fields['objectlist'].toReady = [];
    var fieldobj = self.getField(fieldname);
    var index = $('.joe-object-field[data-name=' + fieldname + ']').find('.joe-objectlist-row').length; //$('.joe-object-field[data-name='+fieldname+']').find('.joe-objectlist-row.editing').removeClass('editing');

    var content = self.renderObjectListRow(self.propAsFuncOrValue(fieldobj["new"]) || {}, fieldobj.properties, index, fieldobj, fieldobj.hasOwnProperty('template'));
    $('.joe-object-field[data-name=' + fieldname + ']').find('tbody').append(content);
    var lastinputfield;
    $('.joe-object-field[data-name=' + fieldname + '] .joe-objectlist-row:last').find('.joe-text-field,.joe-integer-field,.joe-number-field,select,textarea').eq(0)[0].focus(); //ready all newly created fields

    self.Fields['objectlist'].toReady.map(function (fieldtype) {
      self.Fields[fieldtype] && self.Fields[fieldtype].ready && self.Fields[fieldtype].ready();
    });

    if (fieldobj.hasOwnProperty('template')) {
      _objectlistTemplateClickHandler($('.joe-object-field[data-name=' + fieldname + '] .joe-objectlist-row:last .row-template'));
    }
  };

  this._oldeleteaction = function (dom) {
    $(dom).parents('.joe-objectlist-row').remove();
  };

  this.removeObjectListItem = function (fieldname, index) {
    var fieldobj = self.getField(fieldname);
    $('.joe-object-field[data-name=module_fields]').find('tbody').append(content);
  };

  function _sortObjectListRows(property, sortBy) {
    try {
      var overwrites = {};
      overwrites[property] = _jco(true)[property].sortBy(sortBy);

      _joe.Fields.rerender(property, overwrites);
    } catch (e) {
      self.Error.add('error sorting objectlist', e, {
        property: property,
        sortBy: sortBy
      });
    }
  }

  function readyObjectListFields() {
    function olHelper(e, tr) {
      var $originals = tr.children();
      var $helper = tr.clone();

      if ($helper.hasClass('templated')) {
        $helper.children().each(function (index) {
          $(this).toggle($(this).hasClass('template-visible')).width($originals.eq(index).width());
        });
      } else {
        $helper.width(tr.width());
        $helper.children().each(function (index) {
          // Set helper cell sizes to match the original sizes
          $(this).toggle(!$(this).hasClass('row-template'));
          $(this).width($originals.eq(index).width());
        });
      }

      return $helper;
    }

    self.overlay.find('.joe-objectlist-table.sortable').each(function () {
      $(this).find('tbody').sortable({
        axis: 'y',
        handle: '.joe-objectlist-object-row-handle',
        helper: olHelper
      });
    });
  }

  encapsulateFieldType('objectlist', self.renderObjectListField, {
    add: self.addObjectListItem,
    ready: readyObjectListFields,
    toReady: [],
    editHandler: _objectlistTemplateClickHandler,
    sortBy: _sortObjectListRows
  });
  /*----------------------------->
   P | Render Checkbox Group
   <-----------------------------*/

  this.renderCheckboxGroupField = function (prop) {
    /*|{
        tags:'render,field,checkbox,group',
        specs:'values,idprop,standard_field_properties'
    }|*/
    //var profile = self.current.profile;

    /* var values = ($.type(prop.values) == 'function')?prop.values(self.current.object):prop.values||[];*/
    var values = self.getFieldValues(prop.values, prop);
    var locked = self.propAsFuncOrValue(prop.locked) ? ' disabled ' : '';
    var html = '';
    var checked;
    var itemid;
    var propval = self.propAsFuncOrValue(prop.value);

    if ($.type(propval) != "array") {
      propval = [propval];
    }

    var idprop = prop.idprop || '_id';
    var cols = prop.cols || prop.numCols || 2;
    values.map(function (value) {
      if ($.type(value) != 'object') {
        var tempval = {
          name: value
        };
        tempval[idprop] = value;
        value = tempval;
      }

      itemid = 'joe_checkbox-' + prop.name;
      checked = (propval || []).indexOf(value[idprop]) != -1 ? ' checked ' : '';
      html += '<div class="joe-group-item ' + locked + ' cols-' + cols + '"><label >' + '<div class="joe-group-item-checkbox">' + '<input class="joe-field" ' + locked + ' type="checkbox" name="' + prop.name + '" ' + checked + self.renderFieldAttributes(prop) + 'value="' + value[idprop] + '" ' + _disableField(prop) + '/></div>' + (prop.template && fillTemplate(prop.template, value) || value.label || value.name || '') + '</label></div>';
    });
    html += __clearDiv__;
    return html;
  };

  encapsulateFieldType('group', self.renderCheckboxGroupField);
  /*----------------------------->
   Q | Code Field
   <-----------------------------*/

  this.renderCodeField = function (prop) {
    /*|{
        tags:'render,field,code',
        specs:'value,language,standard_field_properties'
    }|*/
    var profile = self.current.profile;
    var height = prop.height ? 'style="height:' + prop.height + ';"' : '';
    var code_language = (self.propAsFuncOrValue(prop.language) || 'html').toLowerCase();
    var editor_id = cuid();
    var html = '<div class="joe-ace-holder joe-rendering-field joe-field" ' + height + ' data-ace_id="' + editor_id + '" data-ftype="ace" name="' + prop.name + '">' + '<textarea class=""  id="' + editor_id + '"  >' + (prop.value || "") + '</textarea>' + '</div>' + '<script>' + 'var editor = ace.edit("' + editor_id + '");\
			editor.setTheme("ace/theme/tomorrow");\
			editor.getSession().setUseWrapMode(true);\
			editor.getSession().setMode("ace/mode/' + code_language + '");\
			editor.setOptions({\
				enableBasicAutocompletion: true,\
				enableLiveAutocompletion: false\
			});\
			_joe.ace_editors["' + editor_id + '"] = editor;' + ' </script>';
    return html;
  };

  encapsulateFieldType('code', self.renderCodeField);
  /*----------------------------->
   Q.2 | QR Field
   <-----------------------------*/

  this.renderQRCodeField = function (prop) {
    /*|{
        tags:'render,field,qr,qrcode',
        specs:'value,size,template,url,standard_field_properties,showContent'
    }|*/
    var height = prop.height ? 'style="height:' + prop.height + ';"' : '';
    var size = prop.size || 280;

    var idprop = _joe.getIDProp();

    var template = self.propAsFuncOrValue(prop.template) || '{\"${itemtype}\":\"${' + idprop + '}\"}';
    var content = fillTemplate;
    template = fillTemplate(template, self.current.object);
    var qrcode_url = prop.url || 'http://www.qr-code-generator.com/phpqrcode/getCode.php?cht=qr&chl=' + encodeURIComponent(template) + '&chs=' + size + 'x' + size + '&choe=UTF-8&chld=L|0';
    var html = '<a href="' + qrcode_url + '" target="_blank"><img class="joe-qrcode-image" src="' + qrcode_url + '" /></a>';

    if (self.propAsFuncOrValue(prop.showContent)) {
      html += '<joe-subtext>' + template + '</joe-subtext>';
    }

    return html;
  };

  encapsulateFieldType('qrcode', self.renderQRCodeField);
  /*----------------------------->
  	R | Rendering Field
  <-----------------------------*/

  this.renderRenderingField = function (prop) {
    /*|{
        tags:'render,field,rendering,textarea',
        specs:'value,standard_field_properties'
    }|*/
    var locked = self.propAsFuncOrValue(prop.locked) ? ' disabled ' : '';
    var profile = self.current.profile;
    var height = prop.height ? 'style="height:' + prop.height + ';"' : '';
    var placeholder = self.propAsFuncOrValue(prop.placeholder) || '';
    var html = '<textarea placeholder="' + placeholder + '" class="joe-rendering-field joe-field" ' + height + ' ' + locked + 'name="' + prop.name + '" >' + (prop.value || "") + '</textarea>';
    return html;
  };

  encapsulateFieldType('rendering', self.renderRenderingField);
  /*----------------------------->
   T | Tags Field
   <-----------------------------*/

  this.renderTagsField = function (prop) {
    /*|{
        tags:'render,field,tag',
        specs:'value,autocomplete,standard_field_properties'
    }|*/
    var profile = self.current.profile;
    var height = prop.height ? 'style="height:' + prop.height + ';"' : '';
    var specs = $.extend({}, prop, {
      autocomplete: true
    }); //,{onblur:'_joe.showMessage($(this).val());'})

    var html = '<div class="joe-tags-container">' + self.renderTextField(specs) + '<div class="joe-text-input-button">add</div>' + '</div>';
    return html;
  };
  /*----------------------------->
   U | Uploader Field
   <-----------------------------*/


  this.uploaders = {};

  this.renderUploaderField = function (prop) {
    /*|{
        tags:'render,field,upload,uploader',
        specs:'field,url_field,onupload(file,callback,base64),onConfirm(data,callback),server_url'
    }|*/
    var uploader_id = cuid();

    if (typeof AWS == 'undefined') {
      $('body').append('<script>function none(){return; }</script>' + '<script type="text/javascript" src="https://sdk.amazonaws.com/js/aws-sdk-2.1.34.min.js"></script>' + //'<script src="http://webapps-cdn.esri.com/CDN/jslibs/craydent-1.7.37.js"></script>'+
      '<script src="' + joe_web_dir + 'js/libs/craydent-upload-2.0.0.js" type="text/javascript"></script>' + '');
    }

    var dz_message = '<div style="line-height:100px;">drag files here to upload</div>';
    var files = prop.value || [];

    if (files && !$c.isArray(files)) {
      files = [files];
    }

    if (prop.field && self.current.object[prop.field]) {
      var kfield = self.getField(prop.field);
      var preview = self.current.object[prop.field];

      if (kfield.prefix) {
        preview = fillTemplate(kfield.prefix, self.current.object) + preview;
      }

      dz_message = '<img src="' + preview + '"/>';
    } else if (files) {
      if (files.length) {
        dz_message = '';
      }

      dz_message += _renderUploaderFilePreviews(files, uploader_id);
    }

    var html = '<div class="joe-uploader joe-field" data-ftype="uploader" name="' + prop.name + '" data-uploader_id="' + uploader_id + '">' + '<div class="joe-uploader-dropzone">' + '<div class="joe-uploader-preview">' + dz_message + '</div>' + '</div>' + '<div class="joe-uploader-message">add a file</div>' + '<div class="joe-button joe-green-button joe-upload-cofirm-button hidden"  onclick="_joe.uploaderConfirm(\'' + uploader_id + '\',\'' + prop.name + '\');">Upload File</div>' + __clearDiv__ + '</div>'; //var idprop = prop.idprop || '_id';
    //html+= __clearDiv__;

    var uploader_obj = {
      cuid: uploader_id,
      prop: prop.name,
      files: files,
      max: prop.max || 0
    };
    self.uploaders[uploader_id] = uploader_obj;
    return html;
  };

  function _removeUploaderFile(id, filename) {
    //TODO:finish cleanup
    var joe_uploader = self.uploaders[id];
    file = joe_uploader.files.where({
      filename: filename
    })[0];

    if (file) {
      joe_uploader.files.remove(file);
      joe_uploader.preview.html(_renderUploaderFilePreviews(joe_uploader.files, joe_uploader.cuid));
    }
  }

  function _renderUploaderFilePreviews(files, cuid) {
    var jup_template,
        html = '';
    var alink = "<a href='${url}${base64}' class='file-link' target='_blank'></a>";
    var delete_btn = '<joe-uploader-file-delete-btn class="svg-shadow" onclick="_joe.Fields.uploader.remove(\'' + cuid + '\',\'${filename}\');">' + self.SVG.icon.close + '</joe-uploader-delete-btn>';
    var label = '<joe-uploader-file-label>${filename}</joe-uploader-file-label>';
    files.map(function (file) {
      if (!$c.isObject(file)) {
        file = {
          url: file,
          filename: file.substr(file.lastIndexOf('/') > 0 && file.lastIndexOf('/') || 0)
        };
      }

      if (file.type && file.type.contains('image') || !file.type && (file.url || file.base64).split('.').contains(['jpg', 'jpeg', 'png', 'gif'])) {
        jup_template = '<joe-uploader-file class="' + (file.uploaded && 'uploaded' || '') + '" style="background-image:url(${url}${base64})">' + alink + label + delete_btn + '</joe-uploader-file>';
      } else {
        jup_template = '<joe-uploader-file class="' + (file.uploaded && 'uploaded' || '') + '">' + '<joe-uploader-file-extension >.' + (file.type.split('/')[1] || '???') + '</joe-uploader-file-extension>' + alink + label + delete_btn + '</joe-uploader-file>';
      }

      html += fillTemplate(jup_template, file);
    });
    return html + '<div class="clear"></div>';
  }

  this.onUserUpload = function (file, base64) {
    var dom = $(this.dropZone).parent();
    var joe_uploader = self.uploaders[dom.data('uploader_id')];

    var field = _joe.getField(joe_uploader.prop);

    if (field.use_legacy) {
      if (file) {
        joe_uploader.file = file;
        joe_uploader.base64 = base64;
        $(joe_uploader.dropzone).find('img,div').replaceWith('<img src="' + base64 + '">'); //joe_uploader.dropzone.html('<img src="' + base64 + '">');

        joe_uploader.message.html('<b>' + file.name + '</b> selected');
        joe_uploader.confirmBtn.removeClass('hidden');
      } else {
        results.innerHTML = 'Nothing to upload.';
      }
    } else {
      if (file) {
        var fname = file.name.replace(/ /g, '_');

        if (joe_uploader.files.where({
          filename: fname
        }).length) {
          var ext = fname.substr(fname.lastIndexOf('.'));
          fname = fname.substr(0, fname.lastIndexOf('.'));

          if (fname.lastIndexOf('_') > 0) {
            var fint = parseInt(fname.substr(fname.lastIndexOf('_') + 1));
            fint += 1;
            fname = fname.substr(0, fname.lastIndexOf('_') + 1) + fint;
          } else {
            fname = fname + '_1';
          }

          fname += ext;
        }

        var temp_file = {
          uploaded: false,
          filename: fname,
          extension: file.type || file.name.substr(file.name.lastIndexOf('.')),
          size: file.size,
          type: file.type,
          base64: base64,
          added: new Date()
        };
        joe_uploader.files.push(temp_file);
        $(joe_uploader.preview).html(_renderUploaderFilePreviews(joe_uploader.files, joe_uploader.cuid));
        joe_uploader.confirmBtn.removeClass('hidden');
      } else {
        results.innerHTML = 'Nothing to upload.';
      }
    }
  }; //setup AWS


  this.uploaderConfirm = function (uploader_id) {
    var joe_uploader = self.uploaders[uploader_id];

    var field = _joe.getField(joe_uploader.prop);

    if (field.use_legacy) {
      var file = joe_uploader.file;
      var base64 = joe_uploader.base64;
      var extension =
      /* joe_uploader.file.type || */
      file.name.substr(file.name.lastIndexOf('.'));

      var callback = function callback(err, url) {
        if (err) {
          joe_uploader.message.append('<div>' + (err.message || err) + '<br/>' + err + '</div>');
          alert('error uploading');
        } else {
          joe_uploader.message.html('<div>uploaded</div>' + url);
          var nprop = {};

          if (field.field) {
            nprop[field.field] = url;
          }

          if (field.url_field) {
            nprop[field.url_field] = url;
          }

          self.rerenderField([field.field, field.url_field], nprop);
          self.updateObject(null, null, true); // self.panel.find('.joe-panel-menu').find('.joe-quicksave-button').click();
        }
      };

      if (field.onConfirm) {
        if (file) {
          joe_uploader.message.append('<div>pending</div>');
          field.onConfirm({
            extension: extension,
            file: file,
            base64: base64,
            field: field
          }, callback);
        }
      } else {
        var uploadFunction = field.upload || field.onupload || function (file, callback, base64_url) {
          //alert('file cached, set upload callback for transmission');
          var err = null;
          callback(err, base64_url);
        };

        if (file) {
          uploadFunction(file, callback, base64);
        }
      }
    } else {
      //new version
      var files = joe_uploader.files.where({
        uploaded: false
      });

      var callback = function callback(err, url, filename) {
        self.upload_progress_counter--;

        if (err) {
          joe_uploader.message.append('<div>' + (err.message || err) + '<br/>' + err + '</div>');
          alert('error uploading');
        } else {
          var fileobj = joe_uploader.files.where({
            filename: filename
          })[0];
          fileobj.uploaded = new Date();
          fileobj.url = url;
          delete fileobj.base64;
        }

        if (self.upload_progress_counter == 0) {
          joe_uploader.message.html('<div>uploaded ' + files.length + ' files</div>');
          joe_uploader.preview.html(_renderUploaderFilePreviews(joe_uploader.files, joe_uploader.cuid));
          self.updateObject(null, null, true); //TODO:add onchange callback
        }
      };

      if (field.onConfirm) {
        if (files && files.length) {
          joe_uploader.message.append('<div>' + files.length + ' pending</div>');
          self.upload_progress_counter = files.length;
          files.map(function (file) {
            field.onConfirm({
              file: file,
              field: field
            }, callback);
          });
        }
      } else {
        var uploadFunction = field.upload || field.onupload || function (file, callback, base64_url) {
          //alert('file cached, set upload callback for transmission');
          var err = null;
          callback(err, base64_url);
        };

        if (file) {
          uploadFunction(file, callback, base64);
        }
      }
    }
  };

  this.readyUploaders = function () {
    self.panel.find('.joe-uploader').each(function () {
      var id = $(this).data('uploader_id'); //$(this).find('.joe-uploader-message').html('awaiting file');

      var fieldObj = self.Fields.get($(this).parents('.joe-object-field').data('name'));
      var uploader = new Upload({
        useInputBox: true,
        target: $(this).find('.joe-uploader-dropzone')[0],
        deferUpload: true,
        onerror: function onerror(data, status, response) {
          var message = data.message || data || "Server responded with error code: " + status;
          message = message.indexOf('bytes') != -1 ? 'file is too small, needs to be larger than 1MB' : message;
          alert(message); //objUpload['uploadFaces'].clear();

          logit(data); //$('divDropZone').innerHTML = data;
        },
        onfileselected: function onfileselected(inputDom) {
          console.log(inputDom.val);
          self.uploaders[id].uploader.upload();
        },
        //uploadUrl:
        onafterfileready: self.onUserUpload
      });
      self.uploaders[id].ready = true;
      self.uploaders[id].uploader = uploader;
      self.uploaders[id].message = $(this).find('.joe-uploader-message').html('awaiting file, previewing current');
      self.uploaders[id].preview = $(this).find('.joe-uploader-preview');
      self.uploaders[id].dropzone = $(this).find('.joe-uploader-dropzone');
      self.uploaders[id].confirmBtn = $(this).find('.joe-upload-cofirm-button');
    });
  };

  encapsulateFieldType('uploader', self.renderUploaderField, {
    ready: self.readyUploaders,
    remove: _removeUploaderFile
  });
  /*----------------------------->
   V | Object Reference
   <-----------------------------*/

  this.renderObjectReferenceField = function (prop) {
    /*|{
    featured:true,
     tags:'render,field,objectReference,callback',
     specs:'values,label(forbutton),autocomplete_template,reference_template,template,idprop,placeholder,sortable,callback,reference_specs'
     }|*/
    //var values = self.propAsFuncOrValue(prop.values) || [];
    if (prop.values && prop.values.async) {
      if (prop.values.search) {
        $.ajax('/API/search/', {
          data: prop.values.query || {},
          success: function success(data) {
            /* do something with the data */
          },
          error: function error(err) {
            /* do something when an error happens */
          }
        });
      }
    } else {
      var values = self.getFieldValues(prop.values, prop);
      if (values) var value = (rerenderingField ? self.current.constructed : self.current.object)[prop.name]; //self.current.object[prop.name] ||

      prop.value || !self.current.object.hasOwnProperty(prop.name) && prop['default'] || [];

      if ($.type(value) != 'array') {
        value = value != null ? [value] : [];
      }

      var disabled = _disableField(prop);

      var idprop = prop.idprop || self.getIDProp();
      var template = prop.autocomplete_template || prop.template //|| (self.schemas[prop.values] && (self.schemas[prop.values].listView && self.schemas[prop.values].listView.title) || self.schemas[prop.values].listTitle)
      || "<joe-subtitle>${name}</joe-subtitle><joe-subtext>${${info} || ${" + idprop + "}}</joe-subtext>"; //prop.template ||
      //var values = ($.type(prop.values) == 'function')?prop.values(self.current.object):prop.values||[];

      var html = "Object Reference";
      var specs = $.extend({}, {
        autocomplete: {
          idprop: prop.idprop,
          template: template
        },
        values: values,
        skip: true,
        name: prop.name,
        ftype: 'objectReference',
        placeholder: prop.placeholder
      }); //,{onblur:'_joe.showMessage($(this).val());'})

      var sortable = true;

      if (prop.hasOwnProperty('sortable')) {
        sortable = prop.sortable;
      }

      var html = '';

      if (!disabled) {
        html += '<div class="joe-references-container">' + self.renderTextField(specs) + '<div class="joe-text-input-button joe-reference-add-button" data-fieldname="' + prop.name + '"' + 'onclick="getJoe(' + self.joe_index + ').addObjectReferenceHandler(this);">add</div>' + '</div>';
      }

      html += __clearDiv__ + '<div class ="joe-object-references-holder ' + disabled + (sortable && !disabled && 'sortable' || '') + '" data-field="' + prop.name + '">';
      var refSpecs = $.extend({
        deleteButton: !disabled
      }, prop.reference_specs || {});
      value.map(function (v) {
        if ($c.isObject(v)) {
          html += self.createObjectReferenceItem(v, null, prop.name, refSpecs);
        } else if ($c.isString(v)) {
          html += self.createObjectReferenceItem(null, v, prop.name, refSpecs);
        }
      });
      html += '</div>'; //html+= self.renderTextField(specs);

      return html;
    }
  };

  this.addObjectReferenceHandler = function (btn) {
    var id = $(btn).siblings('input').val();

    if (!id) {
      return false;
    }

    $(btn).siblings('input').val('');
    var fieldname = $(btn).data().fieldname;

    var field = _getField(fieldname);

    $('.joe-object-references-holder[data-field=' + fieldname + ']').append(self.createObjectReferenceItem(null, id, fieldname, field.reference_specs));

    if (field.callback) {
      field.callback(_jco(true)[fieldname]);
    }
  };

  this.removeObjectReferenceHandler = function (fieldname) {
    var field = _getField(fieldname);

    if (field.callback) {
      field.callback(_jco(true)[fieldname]);
    }
  };

  this.createObjectReferenceItem = function (item, id, fieldname, specs) {
    var field = _getField(fieldname);

    var deletable = true;
    var specs = $.extend({
      expander: field.expander || field.itemExpander,
      gotoButton: field.gotoButton || field["goto"],
      itemMenu: field.itemMenu,
      schemaprop: field.schemaprop || 'itemtype',
      idprop: field.idprop,
      template: field.reference_template || field.template,
      deleteButton: true,
      nonclickable: field.nonclickable,
      fieldname: fieldname,
      bgcolor: field.bgcolor,
      value: '${_id}'
    }, specs || {});

    if (item && _typeof(item) == "object") {
      specs.isObject = true;
      specs.value = item;
    }

    var idprop = field.idprop || '_id';

    if (!item) {
      var values = self.getFieldValues(field.values, field);
      var item;

      for (var i = 0, tot = values.length; i < tot; i++) {
        if (values[i][idprop] == id) {
          item = values[i];
          break;
        }
      }
    }

    if (!item) {
      var deleteButton = '<div class="joe-delete-button joe-block-button left" ' + 'onclick="$(this).parent().remove();_joe.Fields.objectreference.remove(\'' + fieldname + '\');">&nbsp;</div>';
      return '<div class="joe-field-item deletable" data-value="' + id + '">' + deleteButton + "<div>REFERENCE NOT FOUND</div><span class='subtext'>" + id + "</span>" + '</div>';
    }

    var template = self.propAsFuncOrValue(specs.template, item, null, self.current.object) //||  "<joe-title>${name}</joe-title><joe-subtext>${${info} || ${"+idprop+"}}</joe-subtext>";
    || "<joe-title>${name}</joe-title><joe-subtext>${" + idprop + "}</joe-subtext>";
    /*        var specs = {
                deleteButton:specs.deleteButton,
                expander:specs.expander,
                gotoButton:specs.gotoButton,
                itemMenu:specs.itemMenu,
                value:specs.value ||'${_id}',
                idprop:specs.idprop,
                schemaprop:specs.schemaprop,
                isObject:specs.isObject
            };*/

    var schema = item.itemtype && _joe.schemas[item.itemtype] && item.itemtype || '';
    return self.renderFieldListItem(item, template, schema, specs);
  };

  encapsulateFieldType('objectreference', self.renderObjectReferenceField, {
    create: self.createObjectReferenceItem,
    add: self.createObjectReferenceItem,
    remove: self.removeObjectReferenceHandler
  });
  /*----------------------------->
   W | Preview Field
   <-----------------------------*/

  this.renderPreviewField = function (prop) {
    /*|{
     tags:'render,field,preview'
     }|*/
    //var locked = self.propAsFuncOrValue(prop.locked)?' disabled ':'';
    //var profile = self.current.profile;
    var height = prop.height || '600px';
    var url = self.propAsFuncOrValue(prop.url) || joe_web_dir + 'pages/joe-preview.html';

    var construct = _joe.constructObjectFromFields();

    obj = construct[self.getIDProp()] && construct || self.current.object;
    var content = self.propAsFuncOrValue(prop.content, obj) || '';
    var bodycontent = self.propAsFuncOrValue(prop.bodycontent, obj) || '';
    var previewid = obj[self.getIDProp()] + '_' + prop.name;

    window.__PreviewTest = function () {
      alert('test alert');
    };

    window.__previews = window.__previews || {};
    window.__previews[previewid] = {
      content: content,
      bodycontent: bodycontent
    };
    url += '?pid=' + previewid;
    var html = '<div class="joe-button joe-reload-button joe-iconed-button" onclick="getJoe(' + self.joe_index + ').rerenderField(\'' + prop.name + '\');">Reload</div>' + '<div class="joe-preview-iframe-holder" style="height:' + height + '">' + '<iframe class="joe-preview-field joe-field joe-preview-iframe" width="100%" height="100%" name="' + prop.name + '" ' + 'src="' + url + '"></iframe>' + '</div>' //+ '<a href="'+url+'" target="_blank"> view fullscreen preview</a><p>' + url.length + ' chars</p>';
    + '<div class="joe-button joe-iconed-button joe-view-button multiline" onclick="window.open(\'' + url + '\',\'joe-preview-' + previewid + '\').joeparent = window;"> view fullscreen preview <p class="joe-subtext">' + url.length + ' chars</p></div>' + __clearDiv__;
    return html;
  };

  encapsulateFieldType('preview', self.renderPreviewField);
  /*----------------------------->
   X //TextEditor Field
   <-----------------------------*/

  var tinyconfig = {};

  this.renderTinyMCEField = function (prop) {
    /*|{
        tags:'render,field,wysiwyg'
    }|*/
    var locked = self.propAsFuncOrValue(prop.locked) ? ' disabled ' : '';
    var height = prop.height ? 'style="height:' + prop.height + ';"' : '';
    var editor_id = cuid();
    var html = '<div class="joe-tinymce-holder joe-texteditor-field joe-field" '
    /*+height*/
    + ' data-texteditor_id="' + editor_id + '" data-ftype="tinymce" name="' + prop.name + '">' + '<div id="' + editor_id + '" class="joe-tinymce">' + (prop.value || "") + '</div>' + '</div>';
    return html;
  };

  this.readyTinyMCEs = function () {
    tinymce.init({
      selector: ".joe-tinymce",
      //max_height: 300,
      // height:500,
      plugins: ['table wordcount link image code searchreplace autoresize fullscreen'],
      menu: {
        // this is the complete default configuration
        edit: {
          title: 'Edit',
          items: 'undo redo | cut copy paste pastetext | selectall'
        },
        format: {
          title: 'Format',
          items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'
        },
        table: {
          title: 'Table',
          items: 'inserttable tableprops deletetable | cell row column'
        },
        tools: {
          title: 'Tools',
          items: 'spellchecker code link image'
        }
      },
      toolbar: 'fullscreen | undo redo | bold italic removeformat | bullist numlist outdent indent' //  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'

      /*  plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],*/

      /*toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"*/

    });
    $('.joe-tinymce').each(function () {
      var field = self.getField($(this).parents('.wysiwyg-field').data('name'));

      if (field.height) {
        $('.mce-edit-area').height(field.height);
      }
    });
  };

  self.texteditors = [];
  this.readyTextEditors = this.readyTinyMCEs;
  encapsulateFieldType('wysiwyg', self.renderTinyMCEField, {
    ready: self.readyTextEditors
  });
  /*----------------------------->
  Y | Object Field
   <-----------------------------*/

  this.renderSubObjectField = function (prop) {
    /*|{
     tags:'render,field,subobject',
     specs:'locked',
     value:'(object)'
     }|*/
    var action = 'getJoe(' + self.joe_index + ').Fields.subobject.openMini(\'' + prop.name + '\');';
    var html = prop.locked ? '' : '<div class="joe-button joe-view-button joe-iconed-button" onclick="' + action + '">Edit</div>'; //textarea

    html += '<joe-subobject-output><pre>' + JSON.stringify(prop.value, '', '\t') + '</pre></joe-subobject-output>';
    return html;
  };

  encapsulateFieldType('subobject', self.renderSubObjectField, {
    openMini: function openMini(propname) {
      var field = self.getField(propname);
      var callback = self.Cache.add(function (data, id) {
        var field = self.getField(data.prop);
        self.current.object[data.prop] = self.Mini.construct();
        self.Fields.rerender(data.prop);
      }, {
        data: {
          prop: propname
        },
        type: 'callback'
      });
      var specs = {
        object: field.value,
        title: field.display || field.name,
        menu: [{
          name: 'update',
          callback: callback.id
        }],
        schema: field.schema,
        name: self.joe_index + '-' + propname
      };

      _joe.showMiniJoe(specs);
    },
    construct: function construct(parentFind, outputObject) {
      var propname;
      parentFind.find('.subobject-field').each(function () {
        propname = $(this).data('name');
        outputObject[propname] = self.current.object[propname];
      });
    }
  });
  /*----------------------------->
   Z | Passthrough
   <-----------------------------*/

  this.renderPassthroughField = function (prop) {
    var html = 'passthrough';
    return html;
  };
  /*----------------------------->
   27 | Comments
   <-----------------------------*/


  this.renderCommentsField = function (prop) {
    var item = self.propAsFuncOrValue(prop.item) || self.current.object;
    var idprop = self.getIDProp();
    var user = self.propAsFuncOrValue(prop.user) || JOE.User || {};
    var usertemplate = prop.usertemplate || '<joe-subtext><b>${name}</b> says: </joe-subtext>';
    var html = "<joe-comments-add>" + fillTemplate(usertemplate, user) + "<textarea></textarea>" + self.renderMenuButtons({
      css: 'joe-iconed-button joe-comments-button fright',
      name: 'comment',
      //action:'_joe.Fields.comments.add(this,\''+item[idprop]+'\');'
      action: '_joe.Fields.comments.add(this);'
    }) + //<div class="joe-button joe-iconed-button joe-comments-button fright" onclick="addCommentHandler(this);">Comment</div>
    //        "<div class='joe-button fright' onclick='addCommentHandler(this);'>Add Comment</div>"  
    __clearDiv__ + "</joe-comments-add>" + "<joe-comments-view data-itemid='" + item[idprop] + "'>" + "loading" + "</joe-comments-view>";
    return html;
  };

  function _readyCommentsField(fieldname) {
    $('joe-comments-view').each(function () {
      $.ajax({
        url: '/API/comments/item/' + $(this).data().itemid,
        dataType: 'jsonp',
        success: updateCommentsField
      });
    });

    function updateCommentsField(payload) {
      var html = '';

      if (payload.error) {
        html += 'error:' + payload.error;
      } else {
        html += fillTemplate('<joe-comments-entry>' + '<joe-comments-user>${this.user.name}</joe-comments-user>' + '<joe-subtext>${RUN[_joe.Utils.prettyPrintDTS;${timestamp}]}</joe-subtext>' + '<joe-content>${comment}</joe-content>' + '</joe-comments-entry>', payload.comments.reverse());
      }

      $('joe-comments-view').html(html);
    }
  }

  function _commentsAddHandler(dom, itemid) {
    var comment = $(dom).siblings('textarea').val().trim();
    $(dom).siblings('textarea').val('');

    if (comment) {
      try {
        var item = self.current.object;
        var fieldName = $(dom).parents('.joe-object-field').data('name');

        var fieldObj = _joe.getField(fieldName);

        var user = self.propAsFuncOrValue(fieldObj.user) || JOE.User || {};
        var flag = self.propAsFuncOrValue(fieldObj.flag) || false;
        var payload = {
          user: {
            _id: user._id,
            name: user.name
          },
          item: {
            _id: item._id,
            name: item.name,
            type: item.itemtype
          },
          comment: comment,
          flag: flag,
          timestamp: new Date().toISOString()
        };

        var action = fieldObj.action || function (payload) {
          JOE.SERVER.socket.emit('save_comment', payload);
        };

        action(payload);
      } catch (e) {
        self.Error.add('saving comment', e, {
          user: user,
          itemid: itemid,
          comment: comment,
          flag: flag
        });
      } //NPC.saveComment(_joe.current.object, comment);
      // if(fieldObj && fieldObj.callback && $c.isFunction(fieldObj.callback)){
      //     try {
      //         fieldObj.callback(comment, USER, _joe.current.object, fieldObj);
      //     }catch(error){
      //         alert('error in comment callback:\n  error');
      //     }
      // }

    }
  }

  encapsulateFieldType('comments', self.renderCommentsField, {
    add: _commentsAddHandler,
    ready: _readyCommentsField
  });
  /*----------------------------->
  Field Rendering Helpers
   <-----------------------------*/

  function encapsulateFieldType(name, render, additionalFunctions) {
    //render = render
    if (!render) {
      return;
    }

    self.Fields[name] = {
      render: render
    };
    $.extend(self.Fields[name], additionalFunctions || {});
  }

  function _getField(fieldname) {
    /*|{
     featured:true,
     description:'gets a currently registered field as a spec object',
     alias:'window._getField',
     tags:'field,helper'
     }|*/
    var fieldobj;

    for (var f = 0, tot = self.current.fields.length; f < tot; f++) {
      fieldobj = self.current.fields[f];

      if (fieldobj.name == fieldname) {
        return fieldobj;
      }
    }

    return false;
  }

  this.getField = this.Fields.get = _getField;
  /*-------------------------------------------------------------------->
  	4 | OBJECT LISTS
  <--------------------------------------------------------------------*/
  //ITEM MENU

  function renderItemMenu(item, buttons) {
    if (!buttons) {
      return '';
    }

    buttons = self.propAsFuncOrValue(buttons, item);

    if (!buttons) {
      return '';
    }

    var btn_template = '<td class="joe-option-menu-button" onclick="${action}"> ${name}</td>';
    var html = '<item-menu class="joe-panel-content-option-menu"><table class=""><tbody><tr>';
    var oc;
    buttons.map(function (b) {
      if (!b.hasOwnProperty('condition') || self.propAsFuncOrValue(b.condition, item)) {
        oc = b.url && "window.open(\'" + fillTemplate(b.url, item) + "\')" || b.action || "alert('" + b.name + "');";
        html += fillTemplate('<td class="joe-option-menu-button ' + (self.propAsFuncOrValue(b.css, item) || '') + '" onclick="' + oc + '">' + b.name + '</td>', item);
      }
    });
    html += '</tr></tbody></table></item-menu>';
    return html;
  } //ITEM EXPANDER


  this._renderExpanderButton = function (expanderContent, item) {
    if (!expanderContent) {
      return '';
    }

    return '<div class="joe-panel-content-option-expander-button" onclick="_joe.toggleItemExpander(this);"></div>';
  }; //window._renderExpanderButton = this._renderExpanderButton;


  this.toggleItemExpander = function (dom, itemid) {
    if (dom) {
      var expander = $(dom).siblings('.joe-panel-content-option-expander');

      if (expander.hasClass('on-demand') && !expander.hasClass('demanded')) {
        var id = expander.data('itemid');
        var schemaname = expander.data('schema');

        var item = _joe.getDataItem(id, schemaname);

        var content = self.propAsFuncOrValue(self.schemas[schemaname].itemExpander, item);
        expander.addClass('demanded').html(content);
      }

      $(dom).closest('.expander').toggleClass('expander-collapsed').toggleClass('expander-expanded');
    }
  };

  function renderItemExpander(item, contentVal, specs) {
    var specs = specs || {};
    var content = fillTemplate(self.propAsFuncOrValue(contentVal, item, null, self.current.object), item);

    if (!content) {
      return '';
    }

    if (specs.onDemand) {
      var html = '<div class="joe-panel-content-option-expander on-demand" data-itemid="' + item[self.getIDProp(specs.schema)] + '" data-schema="' + (specs.schema || item.itemtype || self.current.schema) + '"></div>';
    } else {
      var html = '<div class="joe-panel-content-option-expander">' + content + '</div>';
    }

    return html;
  }

  this.renderItemExpander = renderItemExpander;

  this.renderTableItem = function (listItem, quick, index) {
    //var tableSpecs = tSpecs || tableSpecs;
    var idprop = self.getIDProp(); // listSchema._listID;

    var id = listItem[idprop] || null;
    var colprop;

    if (quick) {
      var searchable = '';
      tableSpecs.cols.map(function (c) {
        if ($c.isString(c)) {
          searchable += (listItem[c] || '') + ' ';
        } else if ($c.isObject(c)) {
          colprop = c.property || c.name;
          searchable += (c.template && self.propAsFuncOrValue(c.template, listItem[colprop], null, listItem) || listItem[colprop] || colprop.indexOf('${') != -1 && colprop || '') + ' '; // html += '<th>' + (c.display || c.header) + '</th>';
        }

        searchable = fillTemplate(searchable, listItem);
      });
      return searchable;
    }

    var action; //var action = 'onclick="_joe.editObjectFromList(\''+id+'\');"';

    var schemaprop = self.current.schema && self.current.schema.listView && self.current.schema.listView.schemaprop;

    if (schemaprop) {
      action = 'onclick="getJoe(' + self.joe_index + ').listItemClickHandler({dom:this,id:\'' + id + '\',schema:\'' + schemaprop + '\'});"';
    } else {
      action = 'onclick="getJoe(' + self.joe_index + ').listItemClickHandler({dom:this,id:\'' + id + '\'});"';
    }

    var ghtml = '<tr class="joe-panel-content-option trans-bgcol" ' + action + '>';
    ghtml += '<td class="joe-table-checkbox">' + '<label>' + index + (tableSpecs.multiselect && '<input type="checkbox" />' || '') + '</label></td>'; //ghtml +='<td>'+index+'</td>';

    tableSpecs.cols.map(function (c) {
      if ($c.isString(c)) {
        ghtml += fillTemplate('<td>' + (listItem[c] || '') + '</td>', listItem);
      } else if ($c.isObject(c)) {
        colprop = c.property || c.name;
        ghtml += fillTemplate('<td>' + (c.template && self.propAsFuncOrValue(c.template, listItem[colprop], null, listItem) || listItem[colprop] || colprop.indexOf('${') != -1 && colprop || '') + '</td>', listItem);
      }
    });
    ghtml += '</tr>';
    return ghtml;
  }; // this.renderGridItem = function(listItem,quick,index,specs) {
  //     var ghtml = '<tr class="joe-panel-content-option trans-bgcol">';
  //     ghtml +='<td class="joe-grid-checkbox"><label><input type="checkbox"></label></td>';
  //     ghtml +='<td>'+index+'</td>';
  //     ghtml +='<td>'+listItem[self.getIDProp()]+'</td>';
  //     ghtml +='</tr>';
  //     return ghtml;
  // };


  this.Render.gridItem = this.renderGridItem = function (item, quick, index, specs) {
    /*|{
        description:'renders a preformated list item for use in itemexpanders, details page, etc',
        featured:true,
        specs:,expander,gotoButton,itemMenu,schemaprop,nonclickable,bgcolor,stripecolor,action
        tags:'render, field list item',
        cssclass:'.joe-field-list-item || .joe-field-item'
    }|*/
    var specs = $.extend({
      template: '<joe-title>${name}</joe-title><joe-content>${info}</joe-content>',
      bgcolor: '#fff'
    }, self.current.schema, self.current.schema.gridView, specs);
    var schema = specs.__schemaname;
    var schemaobj = self.schemas[schema];
    var contentTemplate = self.propAsFuncOrValue(specs.template, item) || '<joe-title>${name}</joe-title><joe-subtext>${itemtype}</joe-subtext>';
    var idprop = specs.idprop || schemaobj && schemaobj.idprop || '_id';
    var hasMenu = specs.itemMenu && specs.itemMenu.length;
    var action = specs.action || ' onclick="goJoe(_joe.search(\'${' + idprop + '}\')[0],{schema:\'' + schema + '\'})" ';
    var nonclickable = self.propAsFuncOrValue(specs.nonclickable, item);
    var clickablelistitem = !specs.gotoButton && !nonclickable
    /* && !hasMenu*/
    ; //var clickablelistitem = !!clickable && (!specs.gotoButton && !hasMenu);

    var expanderContent = renderItemExpander(item, specs.expander);
    var click = !clickablelistitem ? '' : action;
    var html = fillTemplate('<joe-option class="joe-content-option ' + (specs.itemMenu && ' itemmenu' || '') + (specs.stripecolor && ' striped' || '') + (specs.expander && ' expander expander-collapsed' || '') + '" ' + '>' + (hasMenu && renderItemMenu(item, specs.itemMenu) || '') + self.Render.bgColor(item, specs.bgcolor) + '<div class="joe-panel-content-option-content' + (nonclickable && 'nonclickable' || '') + '" ' + click + ' >' + self.propAsFuncOrValue(contentTemplate + __clearDiv__, item) + '</div>' + self.Render.stripeColor(item, specs.stripecolor) + self._renderExpanderButton(expanderContent, item) + expanderContent + '</joe-option>', item);
    return html;
  };

  this.renderListItem = function (listItem, quick, index) {
    var listSchema = $.extend({
      _listID: 'id',
      stripeColor: null
    }, self.current.schema, self.current.specs);
    var action;
    var idprop = self.getIDProp(); // listSchema._listID;

    var id = listItem[idprop] || null; //var action = 'onclick="_joe.editObjectFromList(\''+id+'\');"';

    var customAction = self.current.schema && (self.current.schema.listView && self.current.schema.listView.action || self.current.schema.listAction);

    if (customAction) {
      action = 'onclick="' + self.propAsFuncOrValue(customAction, listItem) + '"';
    } else {
      var schemaprop = self.current.schema && (self.current.schema.schemaprop || self.current.schema.listView && self.current.schema.listView.schemaprop);

      if (schemaprop) {
        action = 'onclick="getJoe(' + self.joe_index + ')' + '.listItemClickHandler({dom:this,id:\'' + id + '\',schema:\'' + listItem[schemaprop] + '\',idprop:\'_id\'});"';
      } else {
        action = 'onclick="getJoe(' + self.joe_index + ')' + '.listItemClickHandler({dom:this,id:\'' + id + '\'});"';
      }
    } //add stripe color
    //var stripeColor = ($.type(listSchema.stripeColor)=='function')?listSchema.stripeColor(listItem):fillTemplate(listSchema.stripeColor,listItem);


    var stripeColor = self.propAsFuncOrValue(listSchema.stripeColor || listSchema.stripecolor, listItem);
    var stripeHTML = '';
    var stripeTitle = '';

    if (stripeColor) {
      if (_typeof(stripeColor) != "object") {
        stripeColor = {
          color: stripeColor
        };
      }

      if (stripeColor.title || stripeColor.name) {
        stripeTitle = ' title="' + (stripeColor.title || stripeColor.name) + '" ';
      }

      stripeHTML = 'style="background-color:' + stripeColor.color + ';" ' + stripeTitle;
    } //add background color


    var bgColor = self.propAsFuncOrValue(listSchema.bgColor || listSchema.bgcolor, listItem); //($.type(listSchema.bgColor)=='function')?listSchema.bgColor(listItem):fillTemplate(listSchema.bgColor,listItem);

    var bgHTML = '';

    if (bgColor) {
      if (bgColor.color) {
        bgHTML = 'style="background-color:' + bgColor.color + ';"';
      } else {
        bgHTML = 'style="background-color:' + bgColor + ';"';
      }
    }

    var listSchemaObjIndicator = 'listView';
    var template = //getProperty('listSchema.'+listSchemaObjIndicator+'.template')
    listSchema.listView && listSchema.listView.template || listSchema._listTemplate;
    var title = //getProperty('listSchema.'+listSchemaObjIndicator+'.title')
    self.propAsFuncOrValue(listSchema.listView && listSchema.listView.title || listSchema._listTitle || listSchema.listTitle, listItem);

    if (quick) {
      var quicktitle = template || title || '';
      return fillTemplate(quicktitle, listItem);
    } //add bordercolor


    var borderColor = fillTemplate(self.propAsFuncOrValue(listSchema.borderColor || listSchema.bordercolor, listItem));
    var borderHTML = '';

    if (borderColor) {
      borderHTML = 'style="border:1px solid ' + borderColor + ';"';
    } //add item number


    var numberHTML = '';

    if (index && !listSchema.hideNumbers) {
      numberHTML = index;
    }

    if (!template) {
      var title = title || listItem.name || id || 'untitled';
      var menu = listSchema.listView && listSchema.listView.itemMenu || listSchema.itemMenu;
      var listItemMenu = renderItemMenu(listItem, menu); //<div class="joe-panel-content-option-button fleft">#</div><div class="joe-panel-content-option-button fright">#</div>';

      var expander = listSchema.listView && listSchema.listView.itemExpander || listSchema.itemExpander;
      var listItemExpander = renderItemExpander(listItem, expander, {
        onDemand: listSchema.onDemandExpander,
        schema: listSchema.__schemaname
      });

      var listItemExpanderButton = self._renderExpanderButton(listItemExpander, listItem);

      var icon = //self.propAsFuncOrValue(self.getCascadingProp('icon'));
      //getProperty('listSchema.'+listSchemaObjIndicator+'.icon')
      listSchema.listView && listSchema.listView.icon || listSchema.icon || listSchema._icon || ''; //CHECKBOX

      var checkbox = self.Render.itemCheckbox(listItem, listSchema, {
        idprop: idprop
      });
      var user_icon = self.propAsFuncOrValue(icon, listItem);
      var listItemIcon = user_icon && renderIcon(user_icon, listItem) || ''; //list item content

      title = "<div class='joe-panel-content-option-content ' " + action + ">" + checkbox + title + "<div class='clear'></div></div>";
      var html = '<joe-option ' + borderHTML + ' class="' + (self.allSelected && 'selected' || '') + ' joe-panel-content-option trans-bgcol ' + (numberHTML && 'numbered' || '') + ' joe-no-select ' + (stripeColor && 'striped' || '') + ' ' + (listItemExpanderButton && 'expander expander-collapsed' || '') + (listItemMenu && ' item-menu' || '') + '"  data-id="' + id + '" >' + '<div class="joe-panel-content-option-bg" ' + bgHTML + '></div>' + '<div class="joe-panel-content-option-stripe' + (stripeTitle && ' titled' || '') + '" ' + stripeHTML + '></div>' + (numberHTML && '<div class="joe-panel-content-option-number" >' + numberHTML + '</div>' || numberHTML) + (listItem._protected && '<svg xmlns="http://www.w3.org/2000/svg" class="protected-icon" viewBox="-6 -4 36 36"><path d="M12 1C8.7 1 6 3.7 6 7L6 8C4.9 8 4 8.9 4 10L4 20C4 21.1 4.9 22 6 22L18 22C19.1 22 20 21.1 20 20L20 10C20 8.9 19.1 8 18 8L18 7C18 3.7 15.3 1 12 1ZM12 3C14.3 3 16 4.7 16 7L16 8 8 8 8 7C8 4.7 9.7 3 12 3ZM12 13C13.1 13 14 13.9 14 15 14 16.1 13.1 17 12 17 10.9 17 10 16.1 10 15 10 13.9 10.9 13 12 13Z"></path></svg>' || '') + listItemExpanderButton + listItemIcon + listItemMenu + fillTemplate(title, listItem) + listItemExpander + '</joe-option>';
    } //if there is a list template
    else {
        var dup = $c.duplicate(listItem);
        dup.action = action;
        html = fillTemplate(template, dup);
      }

    function renderIcon(icon, listItem) {
      var url = icon.url || icon;

      if (url.indexOf('<svg') == -1) {
        var width = icon.width ? ' width:' + icon.width + '; ' : '';
        var height = icon.height ? ' height:' + icon.height + '; ' : '';
        var iconURL = fillTemplate(url, listItem);
        var iconhtml = '<div style=" background-image:url(\'' + iconURL + '\'); " class="joe-panel-content-option-icon trans-bgcol fleft"  >' + '<img style="' + width + height + '" src="' + iconURL + '"/>' + '</div>';
        return iconhtml;
      } else {
        var iconhtml = '<div  class="joe-panel-content-option-svg fleft"  >' + icon + '</div>';
        return iconhtml;
      }
    }

    return html;
  };

  this.checkItem = function (itemID, prop, schema, dom) {
    //rerender field list item
    var item = self.search(itemID)[0] || false;
    var schema = schema || self.current.schema || item && self.schemas[item.itemtype];
    var checkboxAction;

    if (schema && schema.checkbox && schema.checkbox.action) {
      checkboxAction = schema.checkbox.action;
    }

    if (item) {
      $(dom).toggleClass('checked');
      var overwrite = {
        joeUpdated: new Date()
      };
      var isChecked = $(dom).hasClass('checked');

      if (!checkboxAction) {
        overwrite[prop] = isChecked;
      } else {
        $.extend(overwrite, checkboxAction(item, isChecked, $(dom)) || {});
      }

      $.extend(item, overwrite);
      schema.callback(item);
      window.event && window.event.stopPropagation();
    }
  };

  this.shiftSelecting = false;
  var goBackListIndex;

  this.listItemClickHandler = function (specs) {
    /*|{
    featured:true,
     tags:'list,handler',
     description:'the default function called when a list item is clicked.'
     }|*/
    goBackListIndex = null;

    if (specs && specs.dom) {
      //store index
      goBackListIndex = $(specs.dom).parents('.joe-panel-content-option').index(); //logit('clicked index: '+goBackListIndex);
    }

    self.current.selectedListItems = [];

    if (!window.event) {
      //firefox fix
      self.editObjectFromList(specs);
    } else if (!window.event.shiftKey && !window.event.ctrlKey) {
      self.editObjectFromList(specs);
    } else if (window.event.ctrlKey) {
      if ($(specs.dom).hasClass('joe-panel-content-option')) {
        $(specs.dom).toggleClass('selected');
      } else {
        $(specs.dom).parents('.joe-panel-content-option').toggleClass('selected');
      }

      $('.joe-panel-content-option.selected').map(function (i, listitem) {
        self.current.selectedListItems.push($(listitem).data('id'));
      });
      /*    if(!window.event.shiftKey){
              self.shiftSelecting = false;
          }*/
    } else if (window.event.shiftKey) {
      var shiftIndex;
      var domItem;

      if ($(specs.dom).hasClass('joe-panel-content-option')) {
        /* if(!self.shiftSelecting){
             $(specs.dom).toggleClass('selected');}
         else{
           }*/
        $(specs.dom).addClass('selected');
        shiftIndex = $(specs.dom).index();
      } else {
        $(specs.dom).parents('.joe-panel-content-option').addClass('selected');
        shiftIndex = $(specs.dom).parents('.joe-panel-content-option').index();
        /* if($(specs.dom).parents('.joe-panel-content-option').hasClass('selected')) {
             shiftIndex = $(specs.dom).parents('.joe-panel-content-option').index();
         }*/
      }

      if (self.shiftSelecting !== false) {
        if (shiftIndex < self.shiftSelecting) {
          var t = shiftIndex;
          var p = self.shiftSelecting;
          self.shiftSelecting = t;
          shiftIndex = p;
        }

        $('.joe-panel-content-option').slice(self.shiftSelecting, shiftIndex + 1).addClass('selected');
        $('.joe-panel-content-option').slice(shiftIndex + 1).removeClass('selected');
        $('.joe-panel-content-option').slice(0, self.shiftSelecting).removeClass('selected');
      } else {
        self.shiftSelecting = shiftIndex;
      }

      $('.joe-panel-content-option.selected').map(function (i, listitem) {
        self.current.selectedListItems.push($(listitem).data('id'));
      });
    }

    self.updateSelectionVisuals();
  };

  this.updateSelectionVisuals = function () {
    if (self.current.selectedListItems.length) {
      //  $(specs.dom).parents('.joe-overlay-panel').addClass('multi-edit');
      self.overlay.addClass('multi-edit');
      self.overlay.find('.joe-selection-indicator').html(self.current.selectedListItems.length + ' selected');
    } else {
      //  $(specs.dom).parents('.joe-overlay-panel').removeClass('multi-edit');
      self.overlay.removeClass('multi-edit');
      self.overlay.find('.joe-selection-indicator').html('');
    }
  };

  this.editObjectFromList = function (specs) {
    /*|{
        tags:'list,default',
        description:'the default function called by the list item click handler, overwritten by listAction'
    }|*/
    specs = specs || {};
    self.current.schema = specs.schema || self.current.schema || null;
    var list = specs.list || self.current.list;
    var id = specs.id;
    var idprop = specs.idprop || self.getIDProp(); //(self.current.schema && self.current.schema._listID) || 'id';

    var object = list.filter(function (li) {
      return li[idprop] == id;
    })[0] || false;

    if (!object) {
      alert('error finding object');
      return;
    }

    var setts = {
      schema: specs.schema || self.current.schema,
      callback: specs.callback
    };
    /*self.populateFramework(object,setts);
    self.overlay.addClass('active');*/

    goJoe(object, setts);
  };

  this.showList = function (view, subset, filter) {
    if ($.type(view) == "string") {
      view = {
        collection: view,
        schema: view
      };
    }

    view.subset = subset || null;
    var showJoeListBenchmarker = new Benchmarker();
    self.current.clear();
    var dataList = self.Data[view.collection] || []; //NPC.kovm.Data[view.collection]();

    if (filter) {
      dataList = dataList.where(filter);
    }

    goJoe(dataList, {
      schema: view.schema,
      subset: view.subset
    });

    _bmResponse(showJoeListBenchmarker, 'Joe View: "' + view.collection + '" shown');
  };
  /*----------------------------->
  	List Multi Select
  <-----------------------------*/


  this.editMultiple = function () {
    //create object from shared properties of multiple
    var haystack = self.current.list;
    var idprop = self.getIDProp();
    var needles = self.current.selectedListItems;
    var items = [];
    var protoItem = {};
    haystack.map(function (i) {
      if (needles.indexOf(i[idprop]) == -1) {
        //not selected
        return;
      } else {
        $.extend(protoItem, i);
        items.push(i);
      }
    });
    goJoe(protoItem, {
      title: 'Multi-Edit ' + (self.current.schema.__schemaname || '') + ': ' + items.length + ' items',
      schema: self.current.schema || null,
      multiedit: true
    });
  };
  /*----------------------------->
  	List Filtering
  <-----------------------------*/


  this.filterList = function (list, props, specs) {
    //var list = list || self.current.list;
    specs = specs || {};
    var arr = list || self.current.list;
    var found = arr.filter(function (arrobj) {
      for (var p in props) {
        if (arrobj[p] != props[p]) {
          return false;
        }
      }

      return true;
    });

    if (found.length) {
      if (specs.single) {
        return found[0];
      }

      return found;
    } else {
      return false;
    }
  };
  /*----------------------------->
  	List Subsets
  <-----------------------------*/


  this.renderSubsetselector = function (specs) {
    if (!listMode) {
      return '';
    }

    var html = '<div class="joe-subset-selector" >' + self.renderSubsetSelectorOptions(specs) + '</div>';
    return html;
  };

  this.renderSubsetSelectorOptions = function (specs) {
    var subsets = self.current.subsets;

    if (typeof subsets == 'function') {
      subsets = subsets();
    }

    function renderOption(opt) {
      var html = ''; //if(!opt.condition || (typeof opt.condition == 'function' && opt.condition(self.current.object)) || (typeof opt.condition != 'function' && opt.condition)) {

      if (!opt.condition || self.propAsFuncOrValue(opt.condition)) {
        var html = '<div class="selector-option" onclick="getJoe(' + self.joe_index + ').selectSubset(\'' + (opt.id || opt.name || '') + '\');">' + opt.name + '</div>';
      }

      return html;
    }

    if (self.current.specs.subset) {
      self.current.subset = subsets.filter(function (s) {
        s.id = s.id || s.name;
        return s.id == self.current.specs.subset;
      })[0] || false;
    }

    var subsetlabel = self.current.subset && self.current.subset.name || 'All';
    var html = '<div class="selector-label selector-option" onclick="$(this).parent().toggleClass(\'active\')">' + subsetlabel + '</div>' + '<div class="selector-options">' + renderOption({
      name: 'All',
      filter: {}
    });
    subsets.map(function (s) {
      html += renderOption(s);
    });
    html += '</div>';
    return html;
  };

  this.selectSubset = function (subset) {
    //if (!e) var e = window.event;
    //e.cancelBubble = true;
    //if (e.stopPropagation) e.stopPropagation();
    self.hide();
    var schemaname = self.current.schema && self.current.schema.name;
    goJoe(self.current.list, //        $c.merge(self.current.userSpecs,{subset:subset})
    $.extend({}, self.current.userSpecs, {
      subset: subset,
      schema: schemaname
    }));
  };

  this.toggleFilter = function (filtername, dom) {
    /*|{
        description:'toggles a filter on or off, takes a filtername and dom',
        tags:'filter'
    }|*/
    if (parseFloat(filtername) == filtername) {
      filtername = parseFloat(filtername);
    }

    var filter = (self.current.schema && self.propAsFuncOrValue(self.current.schema.filters) || []).where({
      $or: [{
        name: filtername
      }, {
        id: filtername
      }]
    })[0] || false;

    if (!filter) {
      logit('issue finding filter: ' + filtername);
      return;
    }

    if (self.current.schema && self.current.schema.__schemaname) {
      filter.schema = self.current.schema.__schemaname;
    }

    if (self.current.filters[filtername]) {
      delete self.current.filters[filtername];
    } else {
      self.current.filters[filtername] = filter;
    }

    if (dom) {
      $(dom).toggleClass('active');
    }

    self.filterListFromSubmenu(null, true);
  };

  this.toggleFilterObject = function (filtername, filterObj, wait) {
    /*|{
     description:'toggles a filter on or off, takes a filtername filterobject and whether or not to do it now',
     tags:'filter,custom',
     featured:true
    }|*/
    if (parseFloat(filtername) == filtername) {
      filtername = parseFloat(filtername);
    }

    var filter = self.propAsFuncOrValue(filterObj);

    if (!filter) {
      logit('issue finding filter: ' + filtername);
      return;
    }

    if (self.current.schema && self.current.schema.__schemaname) {
      filter.schema = self.current.schema.__schemaname;
    }

    if (self.current.filters[filtername]) {
      delete self.current.filters[filtername];
    }

    self.current.filters[filtername] = {
      name: filtername,
      filter: filterObj
    };
    self.filterListFromSubmenu(null, !wait);
  };

  this.clearFilters = function () {
    /*|{
     description:'clears all filters',
     tags:'filter'
     }|*/
    self.current.filters = {};
    self.container.find('.joe-filter-option').removeClass('active');
    self.filterListFromSubmenu(null, true);
  };
  /*-------------------------------------------------------------------->
  	5 | HTML Renderings
  <--------------------------------------------------------------------*/


  this.replaceRendering = function (dom, specs) {
    var rendering = dom.toString(); //var data = {rendering:html};

    var specs = {
      datatype: 'rendering',
      compact: false,
      dom: dom
    };
    self.show(rendering, specs);
  };

  this.updateRendering = function (dom, callback) {
    var callback = self.current.callback || self.current.schema && self.current.schema.callback || logit;

    if (!self.current.specs.dom) {
      return false;
    }

    var newVal = $('.joe-rendering-field').val();
    $(self.current.specs.dom).replaceWith(newVal);
    logit('dom updated');
    self.hide();
    callback(newVal);
  };
  /*-------------------------------------------------------------------->
  	MENUS
  <--------------------------------------------------------------------*/

  /*-------------------------------------------------------------------->
  	MUTATE - Adding Properties
  <--------------------------------------------------------------------*/


  this.showPropertyEditor = function (prop) {
    self.current.mutant = prop;
  };

  this.addPropertyToEditor = function (prop) {};

  this.minis = {};
  /*-------------------------------------------------------------------->
  	MINIJOE WIndow
  <-------------------------------------------------------------------*/

  this.showMiniJoe = function (specs, joespecs, data) {
    /*|{
     featured:true,
     tags:'mini, show',
     description:'Shows a miniature joe from specs.(props||list||object||content)',
     specs:'mode,(object||list||content||props),(minimenu||menu),callback, title'
     }|*/
    var mini = {};
    specs = $.extend({
      mode: 'text'
    }, specs || {}, joespecs || {});
    var object = specs.props || specs.object || specs.list || specs.content;

    if (!object) {
      return;
    }

    var title = specs.title || 'Object Focus'; //mini.name=specs.prop.name||specs.prop.id || specs.prop._id;

    mini.id = cuid(); //var html = '<div class="joe-mini-panel joe-panel">';

    var mode = specs.mode || 'object';
    specs.object = object;
    specs.minimenu = specs.minimenu || specs.menu;

    switch ($.type(object)) {
      case 'object':
        mode = 'object';
        break;

      case 'array':
        mode = 'list';
        break;
    }

    specs.mode = mode;
    var minimode = mini.id;
    specs.minimode = minimode;
    var html = self.renderEditorHeader({
      title: title,
      minimode: minimode,
      close_action: 'onclick="getJoe(' + self.joe_index + ').hideMini(\'' + minimode + '\');"'
    });
    html += self.renderMiniEditorContent(specs);
    html += self.renderEditorFooter({
      minimenu: specs.menu,
      minimode: minimode
    });
    var height = specs.height || self.overlay.height() / 2;
    mini.panel = self.overlay.find(self.Mini.selector);

    if (specs.name) {}

    mini.panel.attr('name', specs.name || mini.id);
    mini.panel.addClass('active').addClass('focus').html(html);

    if (!$('.joe-mini-panel .joe-panel-footer').find('.joe-button:visible').length) {
      $('.joe-mini-panel').addClass('no-footer-menu');
    }

    mini.panel.css('height', height);

    if (specs.width) {
      mini.panel.css('width', specs.width);
    }

    mini.panel.draggable({
      handle: 'joe-panel-header',
      snap: '.joe-overlay'
    }).resizable({
      handles: 's'
    });

    mini.callback = specs.callback || function (itemid) {
      alert(itemid);
    };

    mini.data = data || {};
    self.minis[mini.id] = mini;
    self.Mini.id = mini.id;
    self.overlay.toggleClass('mini-active', true);
  };

  this.hideMini = function (miniid) {
    /*|{
        description:'hides the current joes mini.',
        tags:'mini, hide'
    }|*/
    try {
      if (miniid) {
        delete self.minis[miniid];
        $(self.Mini.selector + '.ui-resizable').resizable('destroy').draggable('destroy');
        $(self.Mini.selector).removeClass('active');
      } else {
        $(self.Mini.selector + '.ui-resizable').resizable('destroy').draggable('destroy');
        $(self.Mini.selector).removeClass('active');
      }
    } catch (e) {
      //warn(e);
      self.Error.add('hidemini error: ' + e, e, {
        warning: true
      });
    }

    self.overlay.removeClass('mini-active');
  };

  this.constructObjectFromMiniFields = function () {
    /*|{
        featured:true,
        description:'constructs object from the current minis fields',
        tags:'mini, construct'
     }|*/
    var object = {};
    var prop;
    $('.joe-mini-panel.active .joe-object-field').find('.joe-field').each(function () {
      switch ($(this).attr('type')) {
        case 'checkbox':
          prop = $(this).attr('name');

          if ($(this).is(':checked')) {
            object[prop] = true;
          } else {
            object[prop] = false;
          }

          break;

        case 'text':
        default:
          prop = $(this).attr('name');
          object[prop] = $(this).val();
          break;
      }
    });
    return object;
  };

  this.renderMiniEditorContent = function (specs) {
    self.current.sidebars = {
      left: {
        collapsed: false
      },
      right: {
        collapsed: false
      }
    }; //specs = specs || {};

    var content;

    if (!specs) {
      specs = {
        mode: 'text',
        //text,list,single
        text: 'No object or list selected'
      };
    }

    var mode = specs.mode;

    switch (mode) {
      case 'text':
        content = self.renderTextContent(specs);
        break;

      case 'rendering':
        content = self.renderHTMLContent(specs);
        break;

      case 'list':
        content = self.renderListContent(specs);
        break;

      case 'object':
        content = self.renderObjectContent(specs);
        break;

      default:
        content = content || '';
        break;
    }

    var submenu = '';

    if (!specs.minimode) {
      if (mode == 'list' && self.current.submenu || self.current.submenu || renderSectionAnchors().count) {
        submenu = ' with-submenu ';
      }
    } //submenu=(self.current.submenu || renderSectionAnchors().count)?' with-submenu ':'';


    var scroll = 'onscroll="getJoe(' + self.joe_index + ').onListContentScroll(this);"';
    var rightC = content.right || '';
    var leftC = content.left || '';
    var content_class = 'joe-panel-content ' //+(self.specs.useInset &&'joe-inset '||'') 
    + renderEditorStyles() + submenu;
    var html = '<joe-panel-content class="' + content_class + '" ' + (listMode && scroll || '') + '>' + (content.main || content) + '</joe-panel-content>' + self.renderSideBar('left', leftC, {
      css: submenu
    }) + self.renderSideBar('right', rightC, {
      css: submenu
    });
    self.current.sidebars.left.content = leftC;
    self.current.sidebars.right.content = rightC;
    return html;
  };

  this.Mini = {
    selector: '.joe-mini-panel',
    show: self.showMiniJoe,
    hide: self.hideMini,
    construct: self.constructObjectFromMiniFields,
    get: function get() {
      /*|{
       featured:true,
       description:'get the current joe mini as an object',
       tags:'mini, get'
       }|*/
      return self.minis[self.Mini.id] || false;
    },
    clear: function clear() {
      /*|{
       featured:true,
       description:'clear the current mini',
       tags:'mini, clear'
       }|*/
      self.hideMini();
      self.minis = {};
      delete self.Mini.id;
    }
  };
  /*-------------------------------------------------------------------->
    D | DATA
   <--------------------------------------------------------------------*/

  self.quickAdd = function (itemtype) {
    /*|{
     featured:true,
     tags:'data,search,quick,find',
     description:'Opens a mini joe with a list of datasets that can be created from'
     }|*/
    var itemtype = itemtype;

    if (itemtype !== false) {
      itemtype = itemtype || self.current.schema && self.current.schema.__schemaname;
    }

    if (itemtype) {
      self.createObject(itemtype);
    } else {
      var t = [];
      var sch;

      for (var d in self.Data) {
        sch = _joe.schemas[d];

        if (sch) {
          t.push({
            name: sch.name,
            info: sch.info,
            menuicon: sch.menuicon,
            default_schema: sch.default_schema || false
          });
        }
      }

      t.sortBy('name');
      
      _joe.showMiniJoe({
        title: 'Create a new:',
        list: t,
        template: '<joe-schema-icon>${menuicon}</joe-schema-icon>' + '<joe-title>${name}</joe-title>' + '<joe-subtitle>${info}</joe-subtitle>',
        idprop: 'name',
        sorter: 'default_schema,name',
        menu: [],
        callback: function callback(item_id) {
          self.createObject(item_id);
        }
      });
    }
  };

  self.schemas.search = {
    title: 'Search Results',
    name: 'search',
    __schemaname: 'search',
    listTitle: function listTitle(item) {
      var html = '<joe-full-right>' + '<joe-subtext>updated</joe-subtext>' + '<joe-subtitle>' + self.Utils.toDateString(item.joeUpdated) + '</joe-subtitle>' + '</joe-full-right>' + (_joe.schemas[item.itemtype].menuicon && '<joe-icon class="icon-40 icon-grey fleft">' + _joe.schemas[item.itemtype].menuicon + '</joe-icon>' || '') + '<joe-subtext>${itemtype}</joe-subtext>' + '<joe-title>${name}</joe-title><joe-subtitle>${info}</joe-subtitle>' + '<joe-subtext>${_id}</joe-subtext>';
      return html;
    },
    listmenu: [],
    sorter: ['name', {
      name: 'updated',
      value: '!joeUpdated'
    }, 'itemtype'],
    dataset: function dataset() {
      return _joe.search({
        name: /[a-z0-9 _\-\!\:]*/
      });
    },
    filters: function filters() {
      var f = [];

      __appCollections.concat().sort().map(function (s) {
        f.push({
          name: s,
          filter: {
            itemtype: s
          }
        });
      });

      return f;
    },
    listAction: function listAction(item) {
      return 'goJoeItem(\'' + item._id + '\',\'' + item.itemtype + '\')'; //    return 'goJoe(_joe.search(\''+item._id+'\')[0],{schema:\''+item.itemtype+'\'});';
    }
  };

  self.quickFind = function (phrase) {
    /*|{
     featured:true,
     tags:'data,search,quick,find',
     description:'Pops up list of all data items to quickly search against.'
     }|*/
    goJoe(_joe.search({
      name: /[a-z0-9 _\-\!\:]*/
    }), {
      schema: 'search'
    }, function () {
      self.panel.find('.joe-submenu-search-field').focus();
    });
  };

  self.search = function (id, specs) {
    /*|{
     featured:true,
     tags:'data,search',
     description:'Finds any item in JOE by name or idm can specify additional search params'
     }|*/
    var specs = specs || {};
    var collections = specs.collections || '';
    var properties = specs.props || specs.properties || ['_id', 'id', 'name']; //var idprop = specs.idprop || '_id';

    var haystack = [];

    for (var d in self.Data) {
      haystack = haystack.concat(self.Data[d]);
    }

    var results;

    if ($.type(id) == "string") {
      results = haystack.filter(function (r) {
        return r['_id'] == id;
      });
    } else if ($.type(id) == "object") {
      results = haystack.where(id);
    }

    return results;
  };

  self.addDataset = function (name, values, specs) {
    /*|{
        featured:true,
        tags:'data',
        description:'Adds a dataset to JOE'
    }|*/
    //idprop
    //concatenate
    var values = self.propAsFuncOrValue(values, name);

    if (name && values && $.type(values) == "array") {
      self.Data[name] = values;
      var idprop = self.schemas[name] && self.schemas[name].idprop || '_id';
      self.Data[name].map(function (dataitem) {
        self.Indexes._add(idprop, dataitem);
      });

      if (!self.schemas[name]) {
        return;
      }

      if (self.schemas[name].onload) {
        try {
          console.log(name + ' onload');
          self.schemas[name].onload(self.Data[name]);
        } catch (e) {
          self.Error.add('schema onload error: ' + name, e);
        }
      }
    }
  };

  self.deleteDataset = function (dataset) {
    delete self.Data[dataset];
  };

  self.dit = 0;

  self.getData = function (specs) {
    /*|{
        featured:true,
        description:'gets a list of data objects, filterable sortable, limitable',
        tags:'data, getter'
    }|*/
    var st = new Date().getTime();
    var specs = specs || {};
    var objects = [];

    if (specs.schemas) {
      specs.schemas.map(function (sc) {
        objects = objects.concat(self.Data[sc]);
      });
    } else {
      for (var d in self.Data) {
        objects = objects.concat(self.Data[d]);
      }
    }

    var query = specs.query || specs.filters;

    if (query) {
      objects = objects.where(self.propAsFuncOrValue(query));
    }

    var sortBy = specs.sort || specs.sortBy;

    if (sortBy) {
      objects = objects.sortBy(sortBy);
    }

    if (specs.limit) {
      objects = objects.slice(0, specs.limit);
    }

    return objects;
  };

  self.getDataItem = function (id, datatype, idprop) {
    var st = new Date().getTime();

    function addtoDIT() {
      var el = new Date().getTime() - st;
      self.dit += el;
    }
    /*|{
        featured:true,
        description:'gets a single data item from a data collection',
        tags:'data, getter'
    }|*/


    var item;
    var idprop = idprop || self.schemas[datatype] && self.schemas[datatype].idprop || '_id';

    if (self.Indexes[idprop] && self.Indexes[idprop][id]) {
      addtoDIT();
      self.Indexes._usage++;
      return self.Indexes[idprop][id];
    }

    var dataset = self.Data[datatype];

    if (!self.Data[datatype]) {
      addtoDIT();
      return false;
    }

    for (var i = 0, tot = dataset.length; i < tot; i++) {
      item = dataset[i];

      if (item[idprop] == id) {
        addtoDIT();
        return item;
      }
    }

    addtoDIT();
    return false;
  };

  self.getDataset = function (datatype, specs) {
    specs = specs || {};
    var sortby = specs.sortby || 'name';

    if (self.Data[datatype]) {
      var data = self.Data[datatype].sortBy(sortby);
    } else {
      if (specs["boolean"] = true) {
        return false;
      }

      var data = [];
    }

    if (specs.filter) {
      data = data.where(specs.filter);
    }

    if (specs.reverse) {
      data.reverse();
    }

    if (specs.blank) {
      return [{
        name: '',
        val: ''
      }].concat(data);
    }

    return data;
  };

  self.getDataItemProp = function (id, dataset, prop, specs) {
    var specs = specs || {
      delimiter: ','
    };
    prop = prop || 'name';

    if (!id) {
      return '';
    }

    var vals = '';

    if (!$c.isArray(id)) {
      id = [id];
    }

    id.map(function (item_id, i) {
      var item = self.getDataItem(item_id, dataset);

      if (i > 0) {
        vals += specs.delimiter || '';
      }

      if (item) {
        vals += item[prop];
      }
    });
    return vals;
  };
  /*-------------------------------------------------------------------->
  	SCHEMAS
  <--------------------------------------------------------------------*/


  this.setSchema = function (schemaName) {
    if (!schemaName) {
      return false;
    } //setup schema


    var schema = $.type(schemaName) == 'object' ? schemaName : self.schemas[schemaName] || null; //clear filters, subsets and keyword

    if (schema && self.current.schema && self.current.schema.name && self.current.schema.name != schema.name) {
      if (listMode && self.current.schema && self.current.schema.name == 'search') {} else if (!listMode && schema && schema.name == 'search') {//going from 
      } else {
        self.current.keyword = '';
      }
    }

    self.current.schema = schema;
    return schema;
  };

  this.resetSchema = function (schemaName) {
    var newObj = self.constructObjectFromFields(self.joe_index); //var obj = $.extend(self.current.object,newObj);

    self.show($.extend(self.current.object, newObj), $c.merge(self.current.userSpecs, {
      noHistory: true,
      schema: self.setSchema(schemaName) || self.current.schema
    }));
  };

  this.Schema = {
    add: function add(name, config, specs) {
      self.schemas[name] = self.propAsFuncOrValue(config);
      var schemaObj = self.schemas[name];
      schemaObj.__schemaname = schemaObj.name = name;
      schemaObj._id = cuid();
      return schemaObj;
    },
    set: this.setSchema,
    reset: this.resetSchema
  };
  /*-------------------------------------------------------------------->
  	I | INTERACTIONS
  <--------------------------------------------------------------------*/

  this.toggleOverlay = function (dom) {
    $(dom).parents('.joe-overlay').toggleClass('active');
  };

  this.showItem = function (id, schemaname, specs, show_callback) {
    if (schemaname) {
      var specs = specs || {};
      var item = self.getDataItem(id, schemaname);
      specs = $.extend(specs, {
        schema: schemaname
      });
      self.show(item, specs, show_callback);
    }
  };

  this.show = function (data, specs, show_callback) {
    /*|{
        featured:true,
        description:'Populates and shows the joe editor, takes full configuration overides.',
        specs:'compact',
        alias:'goJoe()',
        params:'data,specs,show_callback'
     }|*/
    var data = data || ''; //profile = profile || null

    var specs = specs || {};

    if (!specs.force && !self.checkChanges()) {
      return;
    }

    self.setEditingHashLink(true);
    self.current.benchmarkers = {};
    self.showBM = new Benchmarker();
    clearTimeout(self.hideTimeout); //handle transition animations.

    /*self.overlay.removeClass('fade-out');
          if(!self.overlay.hasClass('active')){
              self.overlay.
          }
          self.overlay.addClass('fade-in');
          setTimeout(function(){
              self.overlay.removeClass('fade-in');
          },500);*/

    if (specs.container) {
      self.container = $(specs.container);
    }

    self.panel.attr('class', 'joe-overlay-panel');

    if (specs.compact === true) {
      self.overlay.addClass('compact');
    }

    if (specs.compact === false) {
      self.overlay.removeClass('compact');
    }

    self.populateFramework(data, specs);
    self.overlay.removeClass('hidden');
    self.overlay.addClass('active');
    self.current.show_callback = show_callback;
    setTimeout(self.onPanelShow, 0);
    /*        var evt = new Event('JoeEvent');
            evt.initEvent("showJoe",true,true);
    
    // custom param
            evt.schemaname = self.current.schema.__schemaname;
            document.addEventListener("myEvent",function(){alert('finished')},false);
            document.dispatchEvent(evt);*/

    $(self.container).trigger({
      type: "showJoe",
      schema: self.current.specs.schema,
      subset: self.current.specs.subset
    });
  };

  this.hide = function (timeout) {
    timeout = timeout || 0; // self.overlay.removeClass('fade-in');

    self.overlay.addClass('hidden');
    self.hideTimeout = setTimeout(function () {
      self.overlay.removeClass('active');
      self.overlay.removeClass('hidden'); //self.overlay.removeClass('fade-out');
    }, timeout);
  };

  this.reload = function (hideMessage, specs) {
    /*|{
     featured:true,
     description:'Reloads the current window. During reload, the joe._reload flag is set to true.',
     tags:'populate, framework,reload'
     }|*/
    logit('reloading joe ' + self.joe_index);
    self._reloading = true;
    var specs = specs || {};
    var reloadBM = new Benchmarker();
    var info = self.history.pop();

    if ($.type(info.data) == 'object') {
      if (specs.overwrite || specs.overwrites) {
        var obj = $.extend({}, info.data, specs.overwrite || specs.overwrites);
      } else {
        obj = info.data;
      }
    } else {
      var obj = info.data;
    } //delete data overwirtes


    delete specs.overwrite;
    delete specs.overwrites;
    var specs = $.extend({}, info.specs, specs);
    self.show(obj, specs);

    if (!hideMessage) {
      self.showMessage('reloaded in ' + reloadBM.stop() + ' secs');
    }

    self._reloading = false;
  };

  this.compactMode = function (compact) {
    if (compact === true) {
      self.overlay.addClass('compact');
    }

    if (compact === false) {
      self.overlay.removeClass('compact');
    }
  };

  this.printObject = function (obj) {
    goJoe('<pre>' + JSON.stringify(obj, '  ', '    ') + '</pre>');
  };

  if (self.joe_index == 0) {
    window.joeMini = this.showMiniJoe;
    window.goJoe = this.show;
    window.goJoeItem = this.showItem;
    window.listJoe = this.editObjectFromList;
  }

  window.$J = window.$J || {
    _usages: {
      get: 0,
      schema: 0,
      search: 0
    },
    get: function get(itemID, callback) {
      $J._usages.get++;
      var item = self.getDataItem(itemID);

      if (item) {
        callback && callback(item);
        return item;
      }
    },
    schema: function schema(schemaname, callback) {
      $J._usages.schema++;
      var schema = _joe.schemas[schemaname];

      if (schema) {
        callback && callback(schema);
        return schema;
      }
    },
    search: function search(query, callback) {
      $J._usages.search++;

      var results = _joe.search(query);

      callback && callback(results);
      return results;
    }
  };

  window.getJoe = function (index) {
    return window._joes[index] || false;
  };

  $(document).keyup(function (e) {
    var mode = self.getMode();

    if (e.keyCode == 27) {
      //esc key
      if (mode == "list") {
        self.deselectAllItems();
      } else if (mode == "details") {
        if ($('.joe-text-autocomplete.active').length) {
          //close autocoomplete first
          $('.joe-text-autocomplete.active').removeClass('active');
        }
      }

      if (self.specs.useEscapeKey) {
        self.closeButtonAction();
      }
    } // esc
    //TODO:if the joe is also current main joe.
    //ctrl + enter
    else if (e.ctrlKey && e.keyCode == 13 && self.specs.useControlEnter) {
        self.overlay.find('.joe-confirm-button').click();
      }
  });
  var sortable_index;
  this.Autosave = {
    possibleChanges: true,
    activate: function activate() {
      if (!self.specs.autosave) {
        return;
      } // document.addEventListener('blur', function(e){
      //     if(["INPUT","SELECT","TEXTAREA","CHECKBOX"].indexOf(e.target.tagName)!= -1){
      //         self.Autosave.possibleChanges = true;
      //         console.log(e.target.tagName+' blurred')
      //     }
      // },true)
      // $('.joe-object-field').on('click',function(e){
      //     console.log(e.target.dataset().name+' clicked')
      //     possibleChanges = true;
      //     })


      var intercnt = 1000;

      if (typeof self.specs.autosave == "number") {
        intercnt = self.specs.autosave;
      }

      self.Autosave.interval = setInterval(function () {
        if (!self.Autosave.possibleChanges) {
          return;
        }

        var hasChanged = self.checkChanges(true); //document.title = document.title.replace('*','');

        var showUnsaved = hasChanged && self.getMode() == "details";
        document.title = (showUnsaved && '*' || '') + document.title.replace('*', '');
        self.overlay.toggleClass('unsaved-changes', showUnsaved);
      }, intercnt);
    },
    deactivate: function deactivate() {
      if (!self.specs.autosave) {
        return;
      } //document.removeEventListener('blur');


      clearInterval(self.Autosave.interval);
    }
  };

  this.onPanelShow = function () {
    /*|{
        description:'function that initializes any js interactions and calls onPanelShow when it completes',
        tags:'show'
    }|*/
    var BM = new Benchmarker();
    self.respond();
    self.Autosave.activate(); //ready datepicker

    self.Fields.date.ready(); //ready timepicker

    self.Fields.time.ready(); //itemcount

    if (currentListItems && self.overlay.find('.joe-submenu-search-field').length) {
      if (goingBackQuery || !$c.isEmpty(self.current.userSpecs.filters) && listMode) {
        self.overlay.find('.joe-submenu-search-field').val(goingBackQuery);
        self.filterListFromSubmenu(self.overlay.find('.joe-submenu-search-field')[0].value, true);
        goingBackQuery = '';
      }

      self.overlay.find('.joe-submenu-itemcount').html(currentListItems.length + ' item' + (currentListItems.length != 1 && 's' || ''));
    }

    if (!listMode) {
      if (self.isNewItem()) {
        $(".joe-object-field.text-field").eq(0).find('input').focus();
      }
    } //ready multisorter


    self.readyBinFields(); //preview

    $('.joe-preview-iframe-holder').resizable({
      handles: 's'
    }); //imagefield

    self.overlay.find('input.joe-image-field').each(function () {
      _joe.updateImageFieldImage(this);
    }); //object reference

    self.overlay.find('.joe-object-references-holder.sortable').sortable({
      handle: '.joe-field-item-content'
    }); // self.overlay.find('.joe-objectlist-table.sortable').each(function(){
    //     $(this).find('tbody').sortable(
    //         {   axis:'y',
    //         handle:'.joe-objectlist-object-row-handle',
    //         helper:olHelper
    //         }
    //     )
    // });
    //ready objectlist

    self.Fields.objectlist.ready(); //sidebars

    if (self.current.sidebars) {
      var lsb = self.current.sidebars.left;
      var rsb = self.current.sidebars.right;
      self.panel.find('.joe-sidebar_left-button').toggleClass('active', lsb.content != '');
      self.panel.find('.joe-sidebar_right-button').toggleClass('active', rsb.content != '');

      if (self.sizeClass != 'small-size') {
        self.panel.toggleClass('right-sidebar', rsb.content && !rsb.collapsed || false);
      }

      self.panel.toggleClass('left-sidebar', lsb.content && !lsb.collapsed || false);
    } //self.toggleSidebar('right',false);
    //ready comments


    self.Fields.comments.ready(); //ready uploaders

    self.readyUploaders(); //ready editors|

    self.readyTextEditors(); //minis

    self.Mini.clear(); //go back to previous item

    if (goingBackFromID) {
      if (goBackListIndex != null) {
        //logit('rendering until '+goBackListIndex);
        //self.overlay.find('.joe-submenu-itemcount').html(currentListItems.length+' item'+((currentListItems.length > 1 &&'s') ||''));
        if (goBackListIndex > self.specs.dynamicDisplay) {
          self.panel.find('.joe-panel-content').html(self.renderListItems(currentListItems, 0, goBackListIndex + 1));
        }

        goBackListIndex = null;
      }

      try {
        self.overlay.find('.joe-panel-content-option[data-id="' + goingBackFromID + '"]').addClass('keyboard-selected')[0].scrollIntoView(true);
      } catch (e) {
        //logit('go back to item not ready yet:'+goingBackFromID);
        self.Error.add('go back to item not ready yet:' + goingBackFromID, e); //TODO:'render all necessary items to go back'
      } //self.overlay.find('.joe-panel-content').scrollTop(self.overlay.find('.joe-panel-content').scrollTop()-20);
      //$('.joe-panel-content').scrollTop($('.joe-panel-content-option.keyboard-selected').offset().top);[0].scrollIntoView(true);


      goingBackFromID = null;
    }

    self._currentListItems = currentListItems;

    if (listMode) {
      //self.overlay[0].className.replace()
      self.setColumnCount(colCount);
      self.panel.toggleClass('list-mode', listMode);
    }

    if (self.panel.hasClass('small-size') || !(self.current.subsets || self.current.schema && self.propAsFuncOrValue(self.current.schema.filters))) {
      //$c.isEmpty(self.current.filters)
      leftMenuShowing = false;
    }

    self.panel.toggleClass('show-filters', leftMenuShowing && listMode);
    self.panel.toggleClass('show-aggregator', rightMenuShowing && listMode);
    self.specs.speechRecognition && self.Speech.initMic();
    self.setEditingHashLink(false);
    self.current.changesConfirmed = false;
    var panelShowFunction = self.getCascadingProp('onPanelShow');

    try {
      var exectime = (new Date() - self.showBM._start) / 1000;
      panelShowFunction && panelShowFunction(self.getState(), {
        time: exectime,
        warnings: self.current.benchmarkers._warnings
      });
      self.current.show_callback && self.current.show_callback(self.getState(), {
        time: exectime
      });
    } catch (e) {
      //warn(e);
      self.Error.add(e, e, {
        warning: true
      });
    }
    /*        if($(window).height() > 700) {
                self.overlay.find('.joe-submenu-search-field').focus();
            }*/


    _bmResponse(BM, 'on panel show complete');

    _bmResponse(self.showBM, '----Joe Shown');
  };

  this.checkChanges = function (nopopup) {
    /*|{
        tags:'changes,navigation',
        description:'Fires to ask the user if they want to verify changes, returns boolean',
        params:'nopopup:boolean[false]'
    }|*/
    var mode = self.getMode();

    if (!mode || mode == "list") {
      return true;
    }

    var cc_construct = _jco(true);

    if (self.current.schema && self.current.schema.checkChanges === false) {
      return true;
    }

    if (self.current.changesConfirmed === true) {
      return true;
    }

    if (!_joe.current.object || !cc_construct) {
      return true;
    }

    var changes = $c.changes(_joe.current.object, cc_construct);
    var tocheck = [];
    var check_msg = '';

    if (changes && changes.$length) {
      var f, test_info;

      for (var c in changes) {
        f = self.getField(c);

        if (c[0] != '$' && c != 'joeUpdated' && f && f.type != 'content' && !parseBoolean(f.passthrough)) {
          if (['qrcode'].indexOf(f.type) != -1) {} else if ((cc_construct[c] === "" || cc_construct[c] === null || $c.isArray(cc_construct[c]) && !cc_construct[c].length) && (_joe.current.object[c] === undefined || _joe.current.object[c] === null || $c.isArray(_joe.current.object[c]) && !_joe.current.object[c].length)) {
            test_info = 'null to blank';
          } else if (self.current.object.itemtype == "project" && c == 'phases') {
            //TODO HACK: remove when clark fixes
            test_info = 'project phase';
          } else if (cc_construct[c] === false && !_joe.current.object[c]) {
            test_info = 'boolean false';
          } else if (f.type == "wysiwyg" && __removeTags(cc_construct[c]).trim() == __removeTags(_joe.current.object[c]).trim()) {
            test_info = 'wysiwyg formatting';
          } else {
            tocheck.push(c);
            check_msg += c + '\n';
          }
        } // if(test_info)logit(test_info);

      } //self.current.changesConfirmed = false;
      //check adds for blanks

    }

    if (nopopup) {
      $('.joe-object-field.changed').removeClass('changed');
      tocheck.map(function (field) {});
      return tocheck.length > 0;
    }

    if (self.overlay.hasClass('active') && !listMode && !self.current.changesConfirmed && tocheck.length) {
      var confirmed = confirm('Are you sure you want to navigate away from this item?\nYou have unsaved changes:\n' + check_msg);
      self.current.changesConfirmed = confirmed;
      return confirmed;
    }

    return true;
  };
  /*-------------------------------------------------------------------->
   J | MESSAGING
   <--------------------------------------------------------------------*/


  this.renderMessageContainer = function () {
    var mhtml = '<joe-message-holder><div class="joe-message-container left"></div></joe-message-holder>';
    return mhtml;
  };

  var messageTimeouts = Array(4);

  this.showMessage = function (message, specs) {
    /*|{
        featured:true,
        tags:'message',
        description:'Shows the destiny style message on the joe panel.',
        specs:'message,timeout,message_class'
    }|*/
    var mspecs = $.extend({
      timeout: 3,
      message_class: ''
    }, specs || {});
    var message = message || 'JOE Message';
    var attr = 'class';
    var transition_time = 400;

    if (self.overlay.find('.joe-message-container').hasClass('active')) {
      self.overlay.find('.joe-message-container').html('<div class="joe-message-content">' + message + '</div>').attr('class', 'joe-message-container active show-message');
    } else {
      self.overlay.find('.joe-message-container').html('<div class="joe-message-content">' + message + '</div>').attr('class', 'joe-message-container active left');
      self.overlay.find('joe-message-holder').addClass('active');
    } //TODO: don't toggle hidden class if no timeout.


    var target = "getJoe(" + self.joe_index + ").overlay.find('.joe-message-container')";
    var holder = "getJoe(" + self.joe_index + ").overlay.find('joe-message-holder')"; //setTimeout(target+".attr('class','joe-message-container active')",50);

    clearTimeout(messageTimeouts[0]);
    clearTimeout(messageTimeouts[1]);
    clearTimeout(messageTimeouts[2]);
    clearTimeout(messageTimeouts[3]);
    clearTimeout(messageTimeouts[4]);
    messageTimeouts[0] = setTimeout(target + ".attr('class','joe-message-container active show-message')", transition_time);

    if (mspecs.timeout) {
      //only hide if timer is running and active
      messageTimeouts[1] = setTimeout(target + ".attr('class','joe-message-container active ')", mspecs.timeout * 1000 + transition_time - 250);
      messageTimeouts[2] = setTimeout(target + ".attr('class','joe-message-container active right')", mspecs.timeout * 1000 + transition_time);
      messageTimeouts[3] = setTimeout(target + ".attr('class','joe-message-container'); ", mspecs.timeout * 1000 + 2 * transition_time + 50);
      messageTimeouts[4] = setTimeout(holder + ".removeClass('active');", mspecs.timeout * 1000 + 2 * transition_time + 150);
    }
    /*
    		.delay(50)
    			.attr(attr,'joe-message-container active')
    			.delay(mspecs.timeout*1000)
    			.attr(attr,'joe-message-container right')
    			.delay(50)
    			.attr(attr,'joe-message-container');*/

  };
  /*-------------------------------------------------------------------->
  	K | OBJECT
  <--------------------------------------------------------------------*/


  this.Object = {};

  this.Object.create = this.createObject = function (specs, item_defaults) {
    /*|{
     featured:true,
     tags:'object,create',
     description:'Default function called to create a new object. Uses the schema.new as base properties.'
     }|*/
    var specs = specs || {}; //takes fields to be deleted
    //var schema = self.current.schema

    var schema = self.current.schema;

    if (typeof specs == "string") {
      specs = {
        schema: specs
      };
    }

    schema = self.schemas[specs.schema] || self.current.schema;
    var item_defaults = item_defaults || self.propAsFuncOrValue(schema["new"]) || {};
    var specs = $.extend({
      schema: schema
    }, specs || {});
    var overwrites = self.propAsFuncOrValue(specs.overwrites) || {};
    goJoe($.extend({}, item_defaults, overwrites), specs);
  };
  /*    /!*-------------------------------------------------------------------->
       + | Add Items
       <--------------------------------------------------------------------*!/
      this.createItem = function(schema,defaults,title){
          var specs = {type:schema,itemtype:schema};
          var defaults = defaults||{};
          var joespecs = {schema:schema};
          if(title){
              joespecs.title = title;
          }
          goJoe($.extend(specs,defaults),joespecs)
      };*/


  function _defaultUpdateCallback(data) {
    self.showMessage(data.name + ' updated successfully');
  }

  this.Object.validate = this.validateObject = function (obj) {
    var req_fields = []; //_joe.current.fields.where({required:true});

    req_fields = _joe.current.fields.filter(function reqCheck(prop) {
      //if (prop.required && (typeof prop.required != 'function' && prop.required) || (typeof prop.required == 'function' && prop.required(self.current.object))) {
      if (self.propAsFuncOrValue(prop.required)) {
        return true;
      }

      return false;
    });

    function olistVal(olistObj) {
      var vals = '';

      for (var i in olistObj) {
        vals += olistObj[i];
      }

      vals.replace(/ /g, '');
      return vals;
    }

    var required_missed = [];
    req_fields.map(function (f) {
      if (!obj[f.name] || $.type(obj[f.name] == "array") && f.type != "objectList" && obj[f.name].length == 0 || f.type == "objectList" && obj[f.name].filter(olistVal).length == 0) {
        required_missed.push(f);
        return false;
      }
    });
    $('.joe-object-field').removeClass('joe-highlighted');

    if (required_missed.length) {
      //f.display|| f.label|| f.name
      var req_array = [];
      required_missed.map(function (of) {
        req_array.push(of.display || of.name);
        $('.joe-object-field[data-name=' + of.name + ']').addClass('joe-highlighted');
      });
      req_array = req_array.condense(true);
      self.showMessage("There are <b>" + req_array.length + "</b> required fields currently missing. <br/>" + req_array.join()); //<br/>"+required_missed.join(', ')
      // self.panel.addClass('show-required');

      return false;
    }

    return true;
  };

  this.updateObjectAsync = function (ajaxURL, specs) {
    var specs = $.extend({
      oldObj: $.extend({}, self.current.object),
      callback: function callback(data) {
        self.showMessage(data.name + ' updated successfully');
      },
      ajaxObj: self.constructObjectFromFields(self.joe_index),
      overwrites: {},
      skipValidation: false
    }, specs || {});
    var newObj = $.extend(ajaxObj.newObj, specs.overwrites, {
      joeUpdated: new Date()
    });

    var skipVal = _joe.propAsFuncOrValue(self.skipValidation);

    if (!skipVal) {
      var valid = self.validateObject(obj);

      if (!valid) {
        return false;
      }
    }

    $.ajax({
      url: specs.ajaxObj,
      data: newObj,
      dataType: 'jsonp',
      success: specs.callback
    });
  };

  this.Object.findAndModify = function (id, modifications, specs, callback) {
    /*|{
     featured:true,
     tags:'save,modify',
     description:'updates any currently indexed object (by id) using validation and the callback from the schema',
     specs:'schema,goto,'
     }|*/
    var specs = specs || {};
    var modifications = $.extend({
      joeUpdated: new Date()
    }, modifications || {});
    var schemaObj = specs.schema && (_joe.schemas[specs.schema] || specs.schema);
    var idprop = specs.idprop || schemaObj && schemaObj.idprop || '_id';
    var object = _joe.Indexes[idprop][id];

    if (!schemaObj && object.itemtype && _joe.schemas[object.itemtype]) {
      schemaObj = _joe.schemas[object.itemtype];
    }

    var oldObj = $.extend({}, object);
    $.extend(object, modifications); //update UI

    function defaultCallback(data) {
      self.showMessage((data.name || data.itemtype) + ' modified successfully');
    }

    var callback = specs.callback || schemaObj.schema && schemaObj.callback || defaultCallback;

    var mode = _joe.getMode();

    if (mode == "details" && self.current.object == object) {
      goJoe(self.current.object, {
        schema: self.current.object.itemtype,
        force: true
      });
    } else if (mode == "list" && self.current.list.indexOf(object) != -1) {
      _joe.reload();
    } //run callback


    logit((obj.name || 'object') + ' updated');

    if (specs["goto"]) {
      if (_typeof(specs["goto"]) == string) {
        goJoe(object, {
          schema: schemaObj
        });
      } else {
        var gotoObject = _joe.search(specs["goto"])[0] || false;
        gotoObject && goJoe(gotoObject, {
          schema: gotoObject.itemtype
        });
      }
    }

    callback(obj, oldObj, oldObj.changes(obj));
  };

  this.Object.update = this.updateObject = function (dom, callback, stayOnItem, overwrites, skipValidation) {
    /*|{
     featured:true,
     tags:'save',
     description:'updates the current object using validation and the callback from the schema'
     }|*/
    self.Cache.clear();
    var oldObj = $.extend({}, self.current.object);

    function defaultCallback(data) {
      self.showMessage(data.name + ' updated successfully');
    }

    var callback = callback || self.current.callback || self.current.schema && self.current.schema.callback || defaultCallback; //logit;

    var newObj = self.constructObjectFromFields(self.joe_index);
    newObj.joeUpdated = new Date();
    overwrites = overwrites || {};
    var obj = $.extend(newObj, overwrites); //check required fields()

    var skipVal = _joe.propAsFuncOrValue(skipValidation);

    if (!skipVal) {
      var valid = self.validateObject(obj);

      if (!valid) {
        return false;
      }
    } //end validation


    obj = $.extend(self.current.object, newObj);
    var itemtype = self.current.object.itemtype && self.Data[self.current.object.itemtype] ? self.current.object.itemtype : null; //update object list

    var index = self.current.list && self.current.list.indexOf(obj);

    if (self.current.list && (index == -1 || index == undefined)) {
      //  object not in current list
      if (itemtype && self.current.list[0] && self.current.list[0].itemtype == itemtype) {//self.current.list.push(obj);
      } else if (!itemtype) {
        self.current.list.push(obj);
      }
    } //update object in dataset


    var dsname = itemtype || self.current.schema.__schemaname;
    var idprop = self.getIDProp();

    if (self.Data[dsname]) {
      if (self.getDataItem(obj[idprop], dsname)) {} else {
        self.Data[dsname].push(obj);
      }

      logit('item matches current schema, updating...');
    } //run callback


    logit((obj.name || 'object') + ' updated');
    callback(obj, oldObj, oldObj.changes(obj));

    if (!stayOnItem) {
      self.goBack(obj);
    }
  };

  this.deleteObject = function (callback) {
    var obj = self.current.object;

    if (!confirm('Are you sure you want to DELETE\n' + obj.name + '\n' + obj[self.getIDProp()])) {
      return false;
    }

    var callback = self.current.callback || self.current.schema && self.current.schema.callback || logit;
    var deleted_prop = self.getCascadingProp('deleted') || '_deleted';
    obj[deleted_prop] = new Date().toISOString();

    if (!self.current.list || !obj || self.current.list.indexOf(obj) == -1) {
      //no list or no item
      logit('object or list not found');
      callback(obj);
      self.current.changesConfirmed = true;
      self.goBack();
      return;
    }

    var index = self.current.list.indexOf(obj);
    self.current.list.removeAt(index);
    logit('object deleted');
    callback(obj);
    self.current.changesConfirmed = true;
    self.goBack();
  };

  this.Object.duplicate = this.duplicateObject = function (specs) {
    //takes fields to be deleted
    specs = specs || {};
    var deletes = specs.deletes || [];
    var itemobj = $.extend({}, self.current.object);
    delete itemobj[self.getIDProp()];
    delete itemobj.joeUpdated;
    delete itemobj.created;
    itemobj.name = itemobj.name + ' copy';
    deletes.map(function (d) {
      delete itemobj[d];
    }); //self.goBack();

    goJoe(itemobj, self.current.userSpecs);
  };

  this.Object.construct = this.constructObjectFromFields = function (index) {
    /*|{
     featured:true,
     tags:'construct',
     alias:'_jco(true)',
     description:'constructs a new object from the current JOE details view'
     }|*/
    var constructBM = new Benchmarker();
    var object = {};

    if (self.current.object) {
      object.joeUpdated = self.current.object.joeUpdated;
    } //{joeUpdated:new Date()};joeUpdated:self.current.object.joeUpdated||new Date()


    var groups = {};
    var prop; //var parentFind = $('.joe-object-field');

    var parentFind = $('.joe-overlay.active');

    if (index) {
      parentFind = self.overlay.find('.joe-object-field');
    }

    var fieldsToConstruct = parentFind.find('.joe-field');

    if (!fieldsToConstruct.length && self.current.object) {
      return self.current.object;
    } //var parentFind = $('.joe-overlay.active');


    parentFind.find('.joe-field').each(function () {
      if ($(this).parents('.objectList-field,.objectlist-field').length) {
        return true;
      }

      if ($(this).parents(_joe.Mini.selector).length) {
        return true;
      }

      if (self.current.userSpecs && self.current.userSpecs.multiedit) {
        if (!$(this).parents('.joe-object-field').hasClass('multi-selected')) {
          return;
        }
      }

      switch ($(this).attr('type')) {
        case 'checkbox':
          //IF multiple with same name, add them to array
          prop = $(this).attr('name');

          if (groups[prop] || $('input[name=' + prop + ']').length > 1) {
            //checkbox groups
            groups[prop] = groups[prop] || [];

            if ($(this).is(':checked')) {
              groups[prop].push($(this).val());
            }

            object[prop] = groups[prop];
          } else {
            //single checkboxes
            if ($(this).is(':checked')) {
              object[prop] = true;
            } else {
              object[prop] = false;
            }
          }

          break;

        case 'text':
        default:
          prop = $(this).attr('name');

          if (prop && prop != 'undefined') {
            //make sure it's suppoesed to be a prop field
            switch ($(this).data('ftype')) {
              case 'passthrough':
                object[prop] = self.propAsFuncOrValue(object[prop]);
                break;
              //ace editor

              case 'ace':
                var editor = _joe.ace_editors[$(this).data('ace_id')]; //$(this).find('.ace_editor');


                object[prop] = editor ? editor.getValue() : self.current.object[prop];
                break;

              case 'ckeditor':
                var editor = CKEDITOR.instances[$(this).data('ckeditor_id')]; //_joe.ace_editors[];
                //$(this).find('.ace_editor');

                object[prop] = editor ? editor.getData() : self.current.object[prop];
                break;

              case 'tinymce':
                var editor = tinymce.editors.where({
                  id: $(this).data('texteditor_id')
                })[0] || false;
                object[prop] = editor ? editor.getContent() : self.current.object[prop];
                break;

              case 'multisorter':
                var vals = [];
                $(this).find('.selections-bin').find('li').each(function () {
                  vals.push($(this).data('id'));
                });
                object[prop] = vals;
                break;

              case 'uploader':
                var joe_uploader = self.uploaders[$(this).data('uploader_id')];
                object[prop] = joe_uploader.files;
                break;

              case 'buckets':
                var vals = [];
                $(this).find('.selections-bin').each(function () {
                  vals.push([]);
                  $(this).find('li').each(function () {
                    vals[vals.length - 1].push(decodeValueObject(this));
                  });
                });
                /*
                 $(this).find('.selections-bin').find('li').each(function(){
                 vals.push($(this).data('id'));
                 });*/

                object[prop] = vals;
                break;

              case 'objectReference':
                var vals = [];
                object[prop] = 'objectReference';
                var obj;
                $('.objectReference-field[data-name="' + prop + '"]').find('.joe-field-list-item,.joe-field-item').each(function () {
                  var data = $(this).data();

                  if (data.isobject) {
                    obj = JSON.parse(decodeURI($(this).data().value));
                    vals.push(obj);
                  } else {
                    vals.push($(this).data().value);
                  } //JSON.parse(decodeURI($(this).data().value))

                });
                object[prop] = vals.condense();
                break;

              default:
                object[prop] = $(this).val();
                break;
            }

            break;
          }

      }

      function decodeValueObject(dom) {
        var data = $(dom).data();

        if (data.isobject) {
          obj = JSON.parse(decodeURI($(dom).data().value));
          return obj;
        } else {
          return $(dom).data().value;
        }
      }
    }); //subObject Field

    ['subobject'].map(function (fieldtype) {
      self.Fields[fieldtype].construct(parentFind, object);
    }); //CONTENT FIELD

    parentFind.find('.joe-object-field.content-field').each(function () {
      prop = $(this).data('name');
      var cfield = self.getField(prop);

      if (cfield.passthrough) {
        if (cfield.passthrough === true) {
          object[prop] = self.current.object[prop];
        } else {
          object[prop] = self.propAsFuncOrValue(cfield.passthrough, self.current.object);
        }
      }
    }); //OBJECT LISTS

    parentFind.find('.objectList-field').each(function () {
      var ol_property = $(this).data('name');
      var ol_array = [];
      var subVal, subInput; //get table header rows

      var subprops = [];
      $(this).find('thead').find('th').each(function (i) {
        // if($(this).data('subprop')) {//skip handle
        subprops.push($(this).data('subprop')); //  }
      }); //parse all trs and tds and match values to subprops array

      $(this).find('tbody').find('tr').each(function () {
        //$(this).find('joe-objectlist-item').each(function(){
        //find all tds
        var ol_array_obj = {};
        $(this).find('td').each(function (i) {
          subInput = $(this).find('input,select,textarea');
          subVal = subInput.val();

          switch (subInput.attr('type')) {
            case 'checkbox':
              subVal = subInput.is(':checked');
              break;
          }

          ol_array_obj[subprops[i]] = subVal;
        }); //store to ol_array

        delete ol_array_obj.undefined;
        ol_array.push(ol_array_obj);
      });
      object[ol_property] = ol_array;
    });

    function evalCheckbox(input) {
      return input.is(':checked');
    }

    _bmResponse(constructBM, '----constructed', null, .01);

    var constructed = !$c.isEmpty(object) && object || false;
    return constructed; //return object;
  };
  /*-------------------------------------------------------------------->
      EXPORT DATA
   <--------------------------------------------------------------------*/


  this.Export = {};

  this.Export.JSON = this.exportJSON = function (object, specs) {
    var specs = specs || {};
    var minify = specs.minify || null;
    var objvar = specs.objvar || '';
    var obobj = minify ? JSON.stringify(object) : JSON.stringify(object, '', '    ');
    goJoe('<b>' + (specs.title || objvar && 'var ' + objvar + ' = ' || 'JSON Object') + '</b><br/><pre>' + obobj + '</pre>', {
      schema: specs.schema
    });
    logit(obobj);
  };

  this.Export.csv = function (schemaname, specs) {
    /*|{
     featured:true,
     tags:'export,io,csv',
     description:'Export a csv of the selected schema, cuids become referned item if found.'
     }|*/
    while (!schemaname) {
      schemaname = prompt('schema name (itemtype)?');

      if (schemaname === null) {
        return; //break out of the function early
      }
    }

    var schema = _joe.schemas[schemaname];

    if (!schema) {
      alert('no schema "' + schemaname + '" found ');
      return;
    }

    var specs = $.extend({
      labels: true
    }, specs || {});
    var idprop = specs.idprop || schema.idprop || '_id';

    var arrData = _joe.Data[schemaname].sortBy('name');

    if (specs.filter) {
      arrData = arrData.where(specs.filter);
    }

    var CSV = '';
    CSV += schemaname + '\r\n\n';

    if (specs.labels) {
      var row = "";

      for (var index in arrData[0]) {
        row += index + ',';
      }

      row = row.slice(0, -1);
      CSV += row + '\r\n';
    }

    for (var i = 0; i < arrData.length; i++) {
      var row = "";
      var colval;

      for (var index in arrData[i]) {
        colval = arrData[i][index];

        if (index != idprop && $c.isCuid(colval)) {
          colval = _joe.Cache.get(colval).name;
        }

        row += '"' + colval + '",';
      }

      row.slice(0, row.length - 1);
      CSV += row + '\r\n';
    }

    if (CSV == '') {
      alert("Invalid data");
      return;
    }

    var fileName = "JOE-" + $c.pluralize(schemaname) + '-' + $c.format(new Date(), 'm-d-Y') + '.csv';
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV); //        window.open(uri);

    var downloadLink = document.createElement("a"); //var blob = new Blob(["\ufeff", csv]);
    //var url = URL.createObjectURL(uri);

    downloadLink.href = uri;
    downloadLink.download = fileName;
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
  };
  /*-------------------------------------------------------------------->
      MULTI FUNCTIONS
   <--------------------------------------------------------------------*/


  this.updateMultipleObjects = function (dom, multipleCallback, callback) {
    var callback = callback || self.current.onUpdate || self.current.callback || self.current.schema && (self.current.schema.onUpdate || self.current.schema.callback) || logit;
    var multipleCallback = multipleCallback || self.current.onMultipleUpdate || self.current.multipleCallback || self.current.schema && (self.current.schema.onMultipleUpdate || self.current.schema.multipleCallback) || logit;
    var idprop = self.getIDProp();
    var newObj = self.constructObjectFromFields(self.joe_index);

    if (Object && Object.keys && Object.keys(newObj).length == 1 && Object.keys(newObj)[0] == "joeUpdated") {
      self.showMessage('please select at least one property to update.');
      return;
    } //newObj.joeUpdated = new Date();
    //clear id from merged object


    delete newObj[idprop];
    delete newObj['id'];
    delete newObj['_id'];
    var haystack = self.current.list;
    var needles = self.current.selectedListItems; //var items = [];

    var updatedItems = [];
    haystack.map(function (i) {
      if (needles.indexOf(i[idprop]) == -1) {
        //not selected
        return;
      } else {
        //make updates to items
        //protoItem = merge(protoItem,i);
        $.extend(i, newObj); //logit('object updated');
        //itemsUpdated++;

        callback(i);
        updatedItems.push(i); //self.showMessage(updatedItems.length +' item(s) updated');
      }
    });
    logit(updatedItems.length + ' item(s) updated');
    multipleCallback(updatedItems);
    self.goBack();
    self.showMessage(updatedItems.length + ' item(s) updated');
  };

  this.deleteMultipleObjects = function (callback) {
    var callback = self.current.callback || self.current.schema && self.current.schema.callback || logit;
    var obj = self.current.object;

    if (!self.current.list || !obj || self.current.list.indexOf(obj) == -1) {
      //no list or no item
      alert('object or list not found');
      self.hide();
      callback(obj);
      return;
    }

    var index = self.current.list.indexOf(obj);
    self.current.list.removeAt(index);
    logit('object deleted');
    self.hide();
    callback(obj);
  };

  this.selectAllItems = function () {
    /*|{
        featured:true,
        description:'selects all items in the current filtered list',
        tags:'list, select'
    }|*/
    self.current.selectedListItems = [];
    var currentItemIDs = currentListItems.map(function (item) {
      return item[self.getIDProp()];
    });
    self.overlay.addClass('multi-edit');
    self.current.selectedListItems = currentItemIDs;
    self.overlay.find('.joe-panel-content-option').addClass('selected');
    self.allSelected = true;
    self.updateSelectionVisuals();
  };

  this.deselectAllItems = function () {
    /*|{
        featured:false,
        description:'deselects all items in the current filtered list',
        tags:'list, select'
    }|*/
    self.current.selectedListItems = [];
    self.overlay[0].classList.remove('multi-edit');
    self.overlay.find('.joe-panel-content-option').removeClass('selected');
    self.allSelected = false;
    self.updateSelectionVisuals();
  };
  /*-------------------------------------------------------------------->
  ANALYSIS, IMPORT AND MERGE
   <--------------------------------------------------------------------*/


  this.analyzeImportMerge = function (newArr, oldArr, idprop, specs) {
    self.showMessage('Beginning Merge Analysis of ' + (newArr.length + oldArr.length), {
      timeout: 0
    });

    if (_webworkers) {
      //_joeworker = new Worker("../JsonObjectEditor/js/joe-worker.js");
      _joeworker = new Worker(joe_web_dir + "js/joe-worker.js");

      _joeworker.onmessage = function (e) {
        //logit(e.data);
        if (!e.data.worker_update) {
          self.showMessage('Analysis Completed in ' + e.data.time + ' secs', {
            timeout: 0
          });
          var analysisOK = confirm('Run Merge? \n' + JSON.stringify(e.data.results, null, '\n'));

          if (analysisOK) {
            specs.callback(e.data);
          } else {
            self.showMessage('next step cancelled');
          }
        } else {
          if (e.data.worker_update.message) {
            self.showMessage(e.data.worker_update.message, {
              timeout: 0
            });
          } else {
            self.showMessage('Merged ' + e.data.worker_update.current + '/' + e.data.worker_update.total + ' in ' + e.data.worker_update.time + ' secs', {
              timeout: 0
            });
          }

          logit('Merged ' + e.data.worker_update.current + '/' + e.data.worker_update.total + ' in ' + e.data.worker_update.time + ' secs');
        }
      };

      var safespecs = $.extend({}, specs);
      delete safespecs.icallback;
      delete safespecs.callback;

      _joeworker.postMessage({
        action: 'analyzeMerge',
        data: {
          newArr: newArr,
          oldArr: oldArr,
          idprop: idprop,
          specs: safespecs
        }
      });

      return;
    }

    var aimBM = new Benchmarker();
    var idprop = idprop || '_id';
    var defs = {
      callback: printResults,
      deleteOld: false,
      dateProp: null,
      icallback: null
    };
    specs = $.extend(defs, specs);
    var data = {
      add: [],
      update: [],
      same: [],
      'delete': []
    };
    var oldArr = oldArr.sortBy(idprop);
    var newArr = newArr.sortBy(idprop);
    var newObj,
        oldObj,
        matchObj,
        testObj,
        i = 0,
        query = {};
    var oldIDs = [];
    var newIDs = []; //create an array of old object ids

    oldArr.map(function (s) {
      oldIDs.push(s[idprop]);
    });
    newArr.map(function (s) {
      newIDs.push(s[idprop]);
    });
    var query = {};
    query[idprop] = {
      $nin: oldIDs
    }; //adds

    data.add = newArr.where(query); //updates||same

    query[idprop] = {
      $in: newIDs
    };
    var existing = oldArr.where(query); //for one-to-one, new items in the old array

    query[idprop] = {
      $in: oldIDs
    };
    var onetoone = newArr.where(query);

    for (var i = 0, len = existing.length; i < len; i++) {
      newObj = onetoone[i], oldObj = existing[i];

      if (!newObj) {
        data.same.push(oldObj); //same
      } else {
        if (specs.dateProp && newObj[specs.dateProp] && oldObj[specs.dateProp] && newObj[specs.dateProp] == oldObj[specs.dateProp]) {
          data.same.push(newObj); //same
        } else {
          data.update.push(newObj); //update
        }
      }
    }
    /*        var newObj, oldObj,matchObj, i = 0,query={};
            while(newObj = newArr[i++]){
                matchObj = oldArr.where(newObj);//is it currently in there in the same form
    
                if(matchObj.length){//currently there and the same
                    data.same.push(newObj);
                }else{
                    query[idprop] = newObj[idprop];
    
                    matchObj = oldArr.where(query);
                    if(matchObj.length){//is there, not the same
                        if(specs.dateProp ) {
                            if(matchObj[0][specs.dateProp] == newObj[specs.dateProp]) {
                                data.same.push(newObj);
                            }
                        }else {
                            data.update.push(newObj);
                        }
                    }else{//not there
                        data.add.push(newObj);
    
                       }
                }
                if(specs.icallback){
                    specs.icallback(i);
                }
            }*/

    /*if(specs.dateProp){
     query[specs.dateProp] = newObj[specs.dateProp];
    }*/


    data.results = {
      add: data.add.length,
      update: data.update.length,
      same: data.same.length
    };
    self._mergeAnalyzeResults = data;
    logit('Joe Analyzed in ' + aimBM.stop() + ' seconds');
    var analysisOK = confirm('Run Merge? \n' + JSON.stringify(data.results, null, '\n'));

    if (analysisOK) {
      specs.callback(data);
    }

    function printResults(data) {
      self.showMiniJoe({
        title: 'Merge results',
        props: JSON.stringify(data.results, null, '\n'),
        mode: 'text',
        menu: []
      });
    }
  };

  this.analyzeClassObject = function (object, ref, maxDepth, classString) {
    /*|
    {
        alias:'_joe.parseAPI, window.joeAPI',
        description:'Takes a js class and creates a joe viewable object from it.',
        tags:'api, analysis'
    }
     |*/
    var classString = classString;
    var maxDepth = maxDepth || 10;
    var data = {
      methods: [],
      properties: []
    };
    var pReg = /function[\s*]\(([_a-z,A-Z0-9]*)\)/;
    var curProp;
    var params;

    function parseFunction(object, p, parent, depth) {
      var parent = parent || '';
      var curProp = object[p];
      params = pReg.exec(object[p]);
      params = params && params[1] || '';

      try {
        var comments = /\/\*\|([\s\S]*)?\|\*\//;
        evalString = curProp.toString().match(comments);

        if (evalString && evalString[1]) {
          evalString = eval('(' + evalString[1].replace(/\*/g, '') + ')');
        } //logit(evalString);

      } catch (e) {
        evalString = {
          error: 'Could not evalutate "' + p + '": \n' + e
        };
        self.Error.add('Could not evalutate "' + p + '": \n' + e, e, {
          caller: 'self.analyzeClassObject'
        });
      }

      var funcName = parent + p;
      var methodObj = {
        code: object[p],
        name: funcName,
        global_function: false,
        propname: p,
        ref: ref,
        'class': ref,
        args: params,
        parameters: comments && (comments.params || comments.parameters) || null,
        _id: ref + '_' + funcName,
        comments: evalString || {},
        itemtype: 'method',
        parent: parent || null
      };

      if (classString) {
        try {
          methodObj.count = (classString.toString().match(new RegExp((parent || '.') + p, 'g')) || []).length;
        } catch (e) {
          //warn('issue with static class: '+ref);
          self.Error.add('issue with static class: ' + ref, e, {
            caller: 'self.analyzeClassObject'
          });
        }
      }

      if (methodObj.comments && (methodObj.comments.info || methodObj.comments.description)) {
        methodObj.description = methodObj.comments.info || methodObj.comments.description;
      }

      if (methodObj.comments && methodObj.comments.tags) {
        methodObj.tags = methodObj.comments.tags.split(',');
      }

      data.methods.push(methodObj);
      return {
        _id: ref + '_' + funcName
      };
    }

    function parseProperty(object, p, parent, depth) {
      if (depth > maxDepth) {
        return;
      }

      var depth = depth || 0;
      depth++;
      var parent = parent || '';
      var curProp = object[p];
      var propName = parent + p;
      var propObj = {
        name: propName,
        ref: ref,
        'class': ref,
        _id: ref + '_' + propName,
        itemtype: 'property',
        parent: parent || null,
        depth: depth,
        propname: p
      };

      if ($.type(curProp) == "object") {
        var subproperties = [];

        for (var sub in curProp) {
          try {
            if (curProp.hasOwnProperty(sub) && object != window.document && object != window) {
              //JSON.stringify(curProp.sub);
              if ($.type(curProp[sub]) == "function") {
                subproperties.push(parseFunction(curProp, sub, propName + '.', depth)._id);
              } else {
                subproperties.push(parseProperty(curProp, sub, propName + '.', depth)._id);
              }
            }
          } catch (e) {
            //console && console.log(sub+': '+e,curProp[sub]);
            self.Error.add(sub + ': ' + e + ':' + curProp[sub], e, {
              caller: 'parseProperty'
            });
          }
        }

        if (subproperties.length) {
          propObj.subproperties = subproperties;
        } //else{


        propObj.value = object[p]; //}
      } else {
        propObj.value = object[p];
      }

      data.properties.push(propObj);
      return {
        _id: ref + '_' + propName
      };
    }

    for (var p in object) {
      curProp = object[p];

      try {
        if ($.type(curProp) == "function") {
          parseFunction(object, p);
        } else {
          parseProperty(object, p);
        }
      } catch (e) {
        //logit(e);
        self.Error.add(e, e);
      }
    }

    return data;
  };
  /*-------------------------------------------------------------------->
  	H | HELPERS
  <--------------------------------------------------------------------*/


  this.functionName = function (func) {
    var name = func.toString();
    var reg = /function ([^\(]*)/;
    return reg.exec(name)[1];
  };

  this.getState = function () {
    /*|{
     featured:true,
     description:'returns an abridged version of the calling joe panel including current object,list (filtered),schema and window title',
     tags:'state, prop, getter',
     returns:'(object)'
       }|*/
    return {
      object: self.current.item,
      list: self.current.filteredList,
      schema: self.current.schema && self.current.schema.name || null,
      title: self.current.title
    };
  };

  this.getCascadingProp = function (propname, schema) {
    /*|{
     specs:'propname,schema',
     description:'Gets a prop based off either current user specs, schema spec or joe default spec (respectively)',
     tags:'cascading, prop',
     returns:'(mixed)',
     featured:true
     }|*/
    var schema = schema && (self.schemas[schema] || schema) || self.current.schema;
    var prop = self.current[propname] || self.current.userSpecs && self.current.userSpecs[propname] || schema && (listMode && schema.listView && schema.listView[propname] || schema[propname]) || self.specs[propname];
    return prop;
  };

  this.getIDProp = function (schema) {
    /*|{
        featured:true,
        tags:'helper, prop,getter',
        description:'Gets the idprop of the current item, or any item with a passed schemaname.',
        returns:'(string)'
    }|*/
    var prop = schema && self.schemas[schema] && self.schemas[schema].idprop || self.current.schema && (self.current.schema.idprop || self.current.schema._listID) || '_id'; // var prop = ( ((schema && self.schemas[schema]) || self.current.schema) && (self.current.schema.idprop || self.current.schema._listID)) || '_id' || 'id';

    return prop;
  };

  this.propAsFuncOrValue = function (prop, toPass, defaultTo, secondToPass) {
    /*|{
        featured:true,
        tags:'helper',
        description:'Parses the property passed as a function or value, can be passed an object to be a parameter of the function call.'
     }|*/
    try {
      var toPass = toPass || !$c.isEmpty(self.current.constructed) && self.current.constructed || self.current.object;
      /*        if(!toPass.hasOwnProperty(prop)){
       return defaultTo || false;
       }*/

      if (prop && typeof prop == 'function') {
        return prop(toPass, secondToPass);
      }

      return prop;
    } catch (e) {
      //logit('JOE: error parsing propAsFunction: '+e);
      self.Error.add('JOE: error parsing propAsFunction: ' + e, e, {
        caller: 'self.propAsFuncOrValue'
      });
      return '';
    }
  };

  this.getFieldValues = function (values, prop) {
    var vals = self.propAsFuncOrValue(values) || [];

    if (vals.isString()) {
      vals = self.getDataset(vals) || [];
    }

    if (prop) {
      var sortby = prop.sortby || prop.sortBy;

      if (sortby) {
        vals = vals.sortBy(sortby);
      }
    }

    return vals;
  };
  /*-------------------------------------------------------------------->
   I | Hashlink
   <--------------------------------------------------------------------*/


  this.Hash = {};

  this.setEditingHashLink = function (bool) {
    /*|{
        tags:'hash',
        description: 'toggle to set hash link editing , false means joe did not cause change directly.'
    }|*/
    window._joeEditingHash = bool || false;
  };

  var hash_delimiter = '/';

  if (location.href && location.href.indexOf(':::') != -1) {
    hash_delimiter = ':::';
  }

  this.Hash.delimiter = hash_delimiter;

  this.updateHashLink = function () {
    /*|{
     featured:true,
     description:'updates the hashlink based on the calling joe.',
     tags:'hash, SPA',
     returns:'(string)'
       }|*/
    if (!self.specs.useHashlink) {
      return;
    }

    var hashInfo = {};
    hashInfo.schema_name = self.current.schema && self.current.schema.__schemaname;

    if (listMode) {
      hashInfo.object_id = '';
      hashInfo.subset = self.current.subset && self.current.subset.name || '';
    } else {
      hashInfo.subset = '';
      hashInfo.object_id = self.current.object && self.current.object[self.getIDProp()] || '';
    }

    var hashtemplate = $.type(specs.useHashlink) == 'string' ? specs.useHashlink : hash_delimiter + '${schema_name}' + ((hashInfo.object_id || hashInfo.subset) && hash_delimiter || '') + '${object_id}${subset}'; // if(self.current.keyword && listMode){
    //     hashtemplate+='@keyword='+self.current.keyword;
    // }
    //$SET({'@!':fillTemplate(hashtemplate,hashInfo)},{noHistory:true});
    //$SET({'@!':fillTemplate(hashtemplate,hashInfo)});

    location.hash = fillTemplate(hashtemplate, hashInfo);
    return location.hash;
  };

  this.Hash.update = this.updateHashLink;

  this.readHashLink = function (hash) {
    /*|{
        featured:true,
        description:'reads the page hashlink and parses for collection, item id, subset and details section',
        tags:'hash, SPA',
        returns:'(bool)'
      }|*/
    if (!self.specs.useHashlink) {
      return false;
    }

    try {
      var useHash = hash || $GET('!') || location.hash;

      if (!useHash || self.joe_index != 0) {
        return false;
      }

      var useHash = useHash.replace('#', '');
      var hashBreakdown = useHash.split(hash_delimiter).condense(); //hashBreakdown.removeAll('');

      hashBreakdown = hashBreakdown.filter(function (v) {
        return v !== '';
      }); //BUGFIX: condense no longer removes blank strings.

      if (!hashBreakdown.length) {
        self.closeButtonAction();
        return false;
      }

      var hashInfo = {};
      var hashSchema = self.schemas[hashBreakdown[0]];
      var hashItemID = hashBreakdown[1] || '';
      /*            function goHash(item,dataset){
                      if(item){
                          goJoe(collectionItem, {schema: hashSchema});
                      }
                  }*/

      if (hashSchema && (hashSchema.dataset || !$.isEmptyObject(self.Data) && self.Data[hashSchema.__schemaname])) {
        var dataset;

        if (hashSchema.dataset) {
          dataset = typeof hashSchema.dataset == "function" ? hashSchema.dataset() : hashSchema.dataset;
        } else {
          dataset = self.Data[hashSchema.__schemaname] || [];
        }

        hashInfo.schema = hashSchema;
        /*function variableGoJoe(dataset,item,subset){
            goJoe((item||), {schema: hashSchema});
        }*/
        //PROCESS single item, subset, subquery or keyword

        if (!$.isEmptyObject(self.Data)) {
          //self.current.keyword = hashInfo.keyword = $GET('keyword');
          var curkey = self.current.keyword;
          self.current.keyword = hashInfo.keyword = $GET('keyword');
          self.current.keyword = self.current.keyword || curkey; //SINGLE ITEM

          if (hashItemID) {
            var collectionName = hashSchema.__collection || hashSchema.__schemaname; //using standard id

            if (hashItemID.indexOf('@') != -1) {
              //passed keyword or filters
              hashItemID = hashItemID.substr(0, hashItemID.indexOf('@'));
              var metas = {};
            }

            if (hashItemID.indexOf(':') != -1) {
              //2nd param is key/value pair
              var key = hashItemID.split(':')[0];
              var value = hashItemID.split(':')[1]; //var collectionName = hashSchema.__collection || hashSchema.__schemaname;

              if (collectionName) {
                var collectionItem = self.getDataItem(value, collectionName, key);

                if (collectionItem) {
                  goJoe(collectionItem, hashInfo);
                }
              } else {
                //SHOW LIST, NO item
                hashInfo.subset = hashItemID;
                goJoe(dataset, hashInfo);
              }
            } else {
              if (collectionName) {
                var collectionItem = self.getDataItem(hashItemID, collectionName); // goHash(collectionItem,);

                if (collectionItem) {
                  goJoe(collectionItem, hashInfo);
                } else {
                  //SHOW LIST, NO item
                  hashInfo.subset = hashItemID;
                  goJoe(dataset, hashInfo);
                }
              }
            }
          } else {
            //SHOW LIST, NO item
            //  goJoe(dataset, {schema: hashSchema,subset:hashItemID});
            goJoe(dataset, hashInfo);
          }

          var section = $GET('section') || hashBreakdown[2] || false;

          if (!section) {} else {
            //$DEL('section');
            //self.updateHashLink();
            self.gotoSection(section);
          }
        }
      } else {
        throw error, 'dataset for "' + hashSchema + '" not found';
      } //logit('hashInfo',hashInfo);

    } catch (e) {
      //logit('error reading hashlink:'+e);
      self.Error.add('error reading hashlink:' + e, e, {
        useHash: useHash || ''
      });
      return false;
    }

    __gotoJoeSection = self.gotoSection;
    return true;
  };

  this.Hash.read = this.readHashLink;

  this.isNewItem = function () {
    if (listMode) {
      return false;
    }

    var idprop = self.getIDProp();

    if (!self.current.object || !self.current.object[idprop] || !self.search(self.current.object[idprop]).length) {
      return true;
    }

    return false;
  };
  /*<-----------------s-------------------------------------------->*/

  /*-------------------------------------------------------------------->
   I.2 | HashCHangeHandler
   <--------------------------------------------------------------------*/

  /*-------------------------------------------------------------------->
   J | RESPOND
   <--------------------------------------------------------------------*/


  this.respond = function (e) {
    // if($c.isMobile() && !_joe.current.object && !_joe.current.list){
    //     return;
    // }
    // if (document.activeElement.tagName == "INPUT") {
    //     return;
    // }
    var w = self.overlay.width();
    var h = self.overlay.height();
    var curPanel = self.panel[0]; //logit(w);

    var sizeClass = '';

    if (curPanel.className.indexOf('-size') == -1) {
      curPanel.className += ' large-size ';
    }

    if (w < 600) {
      sizeClass = 'small-size';
    } else {
      sizeClass = 'large-size';
    }

    curPanel.className = curPanel.className.replace(/[a-z]*-size/, sizeClass);
    htmlRef.className = htmlRef.className.replace(/[a-z]*-size/, 'joe' + sizeClass);
    self.sizeClass = sizeClass;
    /*
            logit(e);
            logit($(window).height())*/
  };

  this._renderGotoButton = function (id, dataset, schema) {
    var item = {};
    item._id = id || this._id;
    item.dataset = dataset || this.itemtype;
    item.schema = schema || item.dataset;
    var idprop = self.getIDProp(schema);
    return fillTemplate('<div class="joe-block-button goto-icon" ' + //'onclick="goJoe(getNPCDataItem(\'${_id}\',\'${dataset}\'),' +
    'onclick="goJoe(_joe.getDataItem(\'${' + idprop + '}\',\'${dataset}\'),' + '{schema:\'${schema}\'})"></div>', item);
  };

  window._renderGotoButton = this._renderGotoButton;
  /*-------------------------------------------------------------------->
  K | Smart Schema Values
   <--------------------------------------------------------------------*/

  this.smartSave = function (item, newItem, schema) {//find schema
    //look for save function
  };
  /*-------------------------------------------------------------------->
   L | API
  <--------------------------------------------------------------------*/


  this.parseAPI = function (jsObj, libraryname) {
    goJoe(self.analyzeClassObject(jsObj, libraryname).methods, {
      schema: 'method',
      title: libraryname + ' Methods'
    });
  };

  window.joeAPI = this.parseAPI;

  this.jsonp = function (url, dataset, _callback, data) {
    _callback = _callback || logit;
    $.ajax({
      url: url,
      data: data,
      dataType: 'jsonp',
      callback: function callback(data) {
        _callback(data, dataset);
      }
    });
    /*
    ajax({url:url,
      onsuccess:function(data){
          callback(data,dataset);
      },
      dataType:'jsonp',
    query:data
    })*/
  };
  /*-------------------------------------------------------------------->
   N | ITEM NAVIGATION
   <--------------------------------------------------------------------*/


  this.next = function () {
    navigateItems(1);
  };

  this.previous = function () {
    navigateItems(-1);
  };

  function navigateItems(_goto2) {
    var dataset, currentObjectIndex, gotoIndex, gotoObject;
    var objIndex = self.getObjectIndex();
    var dataset = objIndex.dataset;

    if (!dataset) {
      return alert('no dataset set');
    }

    currentObjectIndex = objIndex.index;
    gotoIndex = currentObjectIndex + _goto2;

    if (gotoIndex > dataset.length - 1) {
      gotoIndex = 0;
    } else if (gotoIndex < 0) {
      gotoIndex = dataset.length - 1;
    }

    gotoObject = dataset[gotoIndex];
    self.show(gotoObject, {
      schema: self.current.schema.__schemaname
    });
  }

  this.getObjectIndex = function (object) {
    var object = object || self.current.item;
    var dataset,
        currentObjectIndex,
        gotoIndex,
        gotoObject,
        idprop = self.getIDProp();

    if (self.current.filteredList) {
      dataset = self.current.filteredList;
    } else {
      if (!self.current.schema) {
        return alert('no schema set');
      }

      dataset = self.getDataset(self.current.schema.__schemaname).sortBy(self.current.schema.sorter || ['name']);
    }

    if (!dataset) {
      return {
        index: -1,
        dataset: null
      };
    }

    return {
      index: dataset.indexOf(self.current.object),
      dataset: dataset
    }; //if(currentObjectIndex == -1){return alert('object not found in current dataset');}
  };

  var getSelfStr = 'getJoe(' + self.joe_index + ')';
  /*-------------------------------------------------------------------->
   IO | Import / Export
   <--------------------------------------------------------------------*/

  this.IO = {
    Export: {}
  };
  this.IO.Export.button = __exportBtn__;
  this.IO.Export.schema = {
    fields: [{
      name: 'code',
      type: 'code'
    }]
  };
  this.IO.Export.json = self.exportJSON;
  this.IO.Export.csv = self.Export.csv;

  this.IO["export"] = function (item, json, specs) {
    var specs = $.extend({
      minify: false,
      schema: self.IO.Export.schema,
      objvar: self.current.schema.__schemaname
    }, specs);

    var item = item || _jco();

    if (json) {
      self.exportJSON(item, specs);
      return;
    }

    var item_spec = {
      code: JSON.stringify(item).replace(/,"/i, ',\n"')
    };
    self.show(item_spec, {
      schema: self.IO.Export.schema
    });
  };
  /*-------------------------------------------------------------------->
   Co | Components
   <--------------------------------------------------------------------*/


  this.Components = {
    loaded: {},
    styleTag: 'joeWebComponentStyles'
  };

  this.Components.load = function (coName) {
    if (!document.getElementById(self.Components.styleTag)) {
      var c = document.createElement('style');
      c.id = self.Components.styleTag;
      document.body.appendChild(c);
    }

    if (!self.Components.loaded[coName]) {
      var script = document.createElement('script');
      script.type = 'text/javascript';
      var wd = window.web_dir || '/JsonObjectEditor/';

      script.src = wd + "es5-build/web-components/" + coName + ".js";
      document.body.appendChild(script);
      self.Components.loaded[coName] = true;
    }
  };

  this.Components.appendStyles = function (styles) {
    var styleTag = document.getElementById(_joe.Components.styleTag);

    if (styleTag) {
      styleTag.innerHTML += styles;
    }
  };

  this.Components.getAttributes = function (comp) {
    var d = {};

    for (var a = 0; a < comp.attributes.length; a++) {
      d[comp.attributes[a].name] = comp.attributes[a].value;
    }

    return d;
  };

  this.Components.init = function () {
    self.Components.load('joe-button');
    self.Components.load('joe-field');
    self.Components.load('joe-list-item');
    self.Components.load('joe-autocomplete');
    self.Components.load('joe-user-cube');
    self.Components.load('joe-card');
    self.Components.load('capp-components');
  };
  /*-------------------------------------------------------------------->
      //BUTTONS
  <--------------------------------------------------------------------*/


  this.buttons = {
    create: __createBtn__,
    quickSave: __quicksaveBtn__,
    save: __saveBtn__,
    'delete': __deleteBtn__,
    multiSave: __multisaveBtn__,
    selectAll: __selectAllBtn__,
    duplicate: __duplicateBtn__,
    "export": self.IO.Export.Button
  };
  this.buttons.next = {
    name: 'next',
    display: 'next >',
    css: 'joe-fright',
    action: getSelfStr + '.next()'
  };
  this.buttons.previous = {
    name: 'previous',
    display: '< prev',
    css: 'joe-fleft',
    action: getSelfStr + '.previous()'
  };
  /*-------------------------------------------------------------------->
      //SPEECH RECOGNITION
  <--------------------------------------------------------------------*/
  //_joe.showMiniJoe({object:'hello',title:'Audio',menu:[{name:'confirm'},{name:'cancel'}]})

  this.Speech = {
    init: function init(force) {
      if (force || self.specs.speechRecognition) {
        self.addStylesheet(joe_web_dir + 'js/plugins/microphone/microphone.min.css', {
          name: 'microphone-css'
        });
        self.addScript(joe_web_dir + 'js/plugins/microphone/microphone.min.js', {
          name: 'microphone-js'
        });
      }
    },
    confirm: function confirm() {
      self.Mini.show({
        object: 'hello',
        title: 'Audio',
        menu: [{
          name: 'confirm'
        }, {
          name: 'cancel'
        }]
      });
    },
    initMic: function initMic() {
      var mic = self.Speech.mic = new Wit.Microphone(document.getElementById('speech-button-' + self.joe_index));

      var info = function info(msg) {
        logit(msg); //document.getElementById("info").innerHTML = msg;
      };

      var error = function error(msg) {
        logit(msg); //document.getElementById("error").innerHTML = msg;
      };

      mic.onready = function () {
        info("Microphone is ready to record");
      };

      mic.onaudiostart = function () {
        info("Recording started");
        error("");
      };

      mic.onaudioend = function () {
        info("Recording stopped, processing started");
      };

      mic.onresult = function (intent, entities) {
        logit(intent);
        /*var r = kv("intent", intent);
          for (var k in entities) {
          var e = entities[k];
            if (!(e instanceof Array)) {
            r += kv(k, e.value);
          } else {
            for (var i = 0; i < e.length; i++) {
              r += kv(k, e[i].value);
            }
          }
        }
          document.getElementById("result").innerHTML = r;*/
      };

      mic.onerror = function (err) {
        error("Error: " + err);
      };

      mic.onconnecting = function () {
        info("Microphone is connecting");
      };

      mic.ondisconnected = function () {
        info("Microphone is not connected");
      };

      mic.connect("3J7E3INLVHRRZYVTX6VBXWUXXGMRDEPS");
    }
  };
  /*--------------------------------> TITLE <---------------------------------*/

  this.TITLE = {
    dom: function dom() {
      return self.panel && self.panel.find('.joe-panel-title') || null;
    },
    get: function get() {
      return self.TITLE.dom.html();
    },
    set: function set(value, specs) {
      var specs = $.extend({
        before: '',
        after: ''
      }, specs || {});
      var value = self.propAsFuncOrValue(value);

      if (value) {
        self.TITLE.dom().html(specs.before + value + specs.after);
        return value;
      }

      var tempObj = self.constructObjectFromFields();
      var titleObj = $.extend({}, tempObj, {
        _listCount: (currentListItems || []).length || '0'
      });
      var cTitle = self.propAsFuncOrValue(self.current.title, tempObj);
      var titleStr = fillTemplate(cTitle, titleObj);
      self.TITLE.dom().html(specs.before + titleStr + specs.after);
      return titleStr;
    }
  };
  /*--------------------------------> TITLE <---------------------------------*/

  /*-------------------------> CONNECT <--------------------------------------*/

  var iceServers = [{
    urls: 'stun:stun.l.google.com:19302'
  }, {
    urls: 'stun:webrtcweb.com:7788',
    username: 'muazkh',
    credential: 'muazkh'
  }, {
    urls: 'turn:webrtcweb.com:7788',
    username: 'muazkh',
    credential: 'muazkh'
  }, {
    urls: 'turn:webrtcweb.com:8877',
    username: 'muazkh',
    credential: 'muazkh'
  }, {
    urls: 'turns:webrtcweb.com:7788',
    username: 'muazkh',
    credential: 'muazkh'
  }, {
    urls: 'turns:webrtcweb.com:8877',
    username: 'muazkh',
    credential: 'muazkh'
  }, {
    urls: 'stun:webrtcweb.com:4455',
    username: 'muazkh',
    credential: 'muazkh'
  }, {
    urls: 'turns:webrtcweb.com:4455',
    username: 'muazkh',
    credential: 'muazkh'
  }, {
    urls: 'turn:webrtcweb.com:5544?transport=tcp',
    username: 'muazkh',
    credential: 'muazkh'
  }];
  var rtcConfig = {
    iceServers: iceServers,
    sdpSemantics: 'plan-b'
  };
  this.CONNECT = {
    gotStream: function gotStream(stream) {
      logit('Received local stream');

      if (self.CONNECT.videos.local) {
        //self.CONNECT.videos.local.src = window.URL.createObjectURL(stream);
        self.CONNECT.videos.local.srcObject = stream;
      } // Add localStream to global scope so it's accessible from the browser console


      self.CONNECT.streams.local = stream;
      self.CONNECT.RTC.addStream(self.CONNECT.streams.local);
    },
    RTC: window.RTCPeerConnection && new RTCPeerConnection(rtcConfig) || null,
    newConnection: function newConnection() {
      self.CONNECT.RTC = new RTCPeerConnection(rtcConfig);
      self.CONNECT.init();
    },
    init: function init() {
      self.CONNECT.RTC.oniceconnectionstatechange = function () {
        if (self.CONNECT.RTC.iceConnectionState == "completed") {
          logit('ICE state: ' + self.CONNECT.RTC.iceConnectionState);

          if (!self.CONNECT.offerinprogress) {
            if (!confirm('connect request received')) {
              self.CONNECT.end(self.CONNECT.userid, true);
            }
          }

          logit('stopping CONNECT interval -init');
          clearInterval(self.CONNECT.offerinterval);
          self.CONNECT.offerinprogress = false;
        }
      };

      self.CONNECT.RTC.onaddstream = function (evt) {
        logit('stream');
        self.CONNECT.streams.remote = evt.stream; //self.CONNECT.videos.remote.src = window.URL.createObjectURL(self.CONNECT.streams.remote);

        self.CONNECT.videos.remote.srcObject = self.CONNECT.streams.remote; //elem.srcObject = stream

        self.CONNECT.videos.remote.play();
      };

      self.CONNECT.RTC.onremovestream = function (evt) {
        logit('stream ended:', evt);
      };

      self.CONNECT.RTC.onicecandidate = function (evt) {//logit('candidate',evt);
        //alert('candidate')
      };
    },
    videos: {},
    streams: {},
    start: function start(userid, makeoffer) {
      if (self.CONNECT.RTC.signalingState == "closed") {
        self.CONNECT.RTC = new RTCPeerConnection(iceServers);
      }

      if (!userid) {
        _joe.showMiniJoe({
          title: 'CONNECT w/',
          list: self.Data.user.where({
            _id: {
              $ne: self.User._id
            }
          }),
          template: function template(user) {
            var template = '<joe-title>${name}</joe-title><joe-subtitle>${fullname}</joe-subtitle>' + '<joe-subtext>${info}</joe-subtext>';

            if (user.image_url) {
              template = '<joe-listitem-icon style="background-image:url(${image_url});"></joe-listitem-icon>' + template;
            }

            return template;
          },
          idprop: '_id',
          menu: [],
          callback: function callback(item_id) {
            self.CONNECT.start(item_id);
          }
        });

        return;
      }

      self.CONNECT.end(userid); //x select user

      var userobj = self.getDataItem(userid, 'user'); //x open popup

      var popup = self.CONNECT.createPopup(userobj); //get my video data

      self.CONNECT.videos.local = $('#' + popup.domid).find('.joe-connect-myvideo')[0];
      self.CONNECT.videos.remote = $('#' + popup.domid).find('.joe-connect-video')[0]; // self.CONNECT.videos.local.addEventListener('loadedmetadata', function() {
      // logit('Local video videoWidth: ' + this.videoWidth +
      //     'px,  videoHeight: ' + this.videoHeight + 'px');
      // });
      // self.CONNECT.videos.remote.addEventListener('loadedmetadata', function() {
      // logit('Local video videoWidth: ' + this.videoWidth +
      //     'px,  videoHeight: ' + this.videoHeight + 'px');
      // });
      // var gum = ((navigator.mediaDevices && navigator.mediaDevices.getUserMedia) || navigator.getUserMedia);

      navigator.mediaDevices.getUserMedia({
        audio: true,
        video: true //mandatory: {chromeMediaSource: 'screen'}
        // or desktop-Capturing
        //mandatory: {chromeMediaSource: 'desktop'}

      }).then(self.CONNECT.gotStream)["catch"](function (e) {
        alert('getUserMedia() error: ' + e.name);
        console.log(e);
      }); //send invite (socket)
      //var RTC = self.CONNECT.RTC = new RTCPeerConnection(RTCconfig);

      if (makeoffer) {
        var RTC = self.CONNECT.RTC;
        RTC.createOffer().then(function (offer) {
          return RTC.setLocalDescription(offer);
        }, function (data) {
          alert('error in RTC' + data);
          logit(data);
        }).then(function () {
          self.SERVER.socket.emit('connect_offer', {
            user: _joe.User._id,
            target: userid,
            sdp: RTC.localDescription
          });
        })["catch"](function (reason) {
          console.log(reason);
        });
      }
    },
    sendOfferToServer: function sendOfferToServer(offer) {},
    end: function end(userid, clear) {
      //cleanup backend
      //close popup
      capp.Popup["delete"]('connect_' + userid);

      if (clear) {
        logit('stopping CONNECT interval - end');
        clearInterval(self.CONNECT.offerinterval);
        self.CONNECT.offerinprogress = false;
        self.CONNECT.userid = null; // self.CONNECT.streams.local.getAudioTracks()[0].stop();
        // self.CONNECT.streams.local.getVideoTracks()[0].stop();
        // self.CONNECT.streams.remote.getAudioTracks()[0].stop();
        // self.CONNECT.streams.remote.getVideoTracks()[0].stop();

        self.CONNECT.RTC.close();
        self.CONNECT.streams.remote && self.CONNECT.streams.remote.getTracks().forEach(function (track) {
          track.stop();
        });
        self.CONNECT.streams.local && self.CONNECT.streams.local.getTracks().forEach(function (track) {
          track.stop();
        });
      }
    },
    template: function template(userobj) {
      var h = '<joe-video-holder>' + '<video autoplay class="joe-connect-video"></video>' + '<video autoplay  class="joe-connect-myvideo"></video>' + '</joe-video-holder>' + userobj.name; // +'<div onclick="_joe.CONNECT.end(\''+userobj._id+'\',true);"> close </div>';

      return h;
    },
    createPopup: function createPopup(userobj, specs) {
      //create panel
      return capp.Popup.add('connect w/ <b>' + (userobj.fullname || userobj.name) + '</b>', self.propAsFuncOrValue(self.CONNECT.template, userobj), {
        id: 'connect_' + userobj._id,
        active: true,
        close_action: '_joe.CONNECT.end(\'' + userobj._id + '\',true);'
      });
    }
  };
  window.RTCPeerConnection && this.CONNECT.init();
  /*-------------------------> end CONNECT <--------------------------------------*/

  /*-------------------------> SERVER <--------------------------------------*/

  this.SERVER = {
    Plugins: {
      awsFileUpload: function awsFileUpload(data, callback) {
        var file = data.file;
        var field = data.field;
        var bucket = field.bucketname || (_joe.Data.setting.where({
          name: 'AWS_BUCKETNAME'
        })[0] || {
          value: ''
        }).value;

        if (!bucket) {
          callback(err, file.base64);
          return;
        }

        var filename = file.filename;
        var directory = _joe.current.object._id;
        var url = field && self.propAsFuncOrValue(field.server_url) || location.port && '//' + __jsc.hostname + ':' + __jsc.port + '/API/plugin/awsConnect/' || '//' + __jsc.hostname + '/API/plugin/awsConnect/';
        var Key = directory + '/' + filename;
        $.ajax({
          url: url,
          type: 'POST',
          data: {
            Key: Key,
            base64: file.base64,
            extension: file.extension,
            ACL: 'public-read'
          },
          success: function success(response) {
            var url, filename;

            if (!response.error) {
              logit(response);
              var bucket = response.bucket || (_joe.Data.setting.where({
                name: 'AWS_BUCKETNAME'
              })[0] || {
                value: ''
              }).value;
              var region = (_joe.Utils.Settings('AWS_S3CONFIG') || {}).region;
              var prefix = 'https://s3.' + (region && region + '.' || '') + 'amazonaws.com/' + bucket + '/';
              var url = prefix + response.Key;
              var filename = response.Key.replace(self.current.object._id + '/', ''); //TODO: change file status in list.
            }

            callback(response.error, url, filename);
          }
        });
      },
      awsConnect: function awsConnect(data, callback) {
        var bucket = data.field.bucketname || (_joe.Data.setting.where({
          name: 'AWS_BUCKETNAME'
        })[0] || {
          value: ''
        }).value;

        if (!bucket) {
          callback(err, data.base64);
          return;
        }

        var filename = _joe.current.object._id + data.extension;
        var directory = _joe.current.object.itemtype;
        var url = field && self.propAsFuncOrValue(field.server_url) || location.port && '//' + __jsc.hostname + ':' + __jsc.port + '/API/plugin/awsConnect/' || '//' + __jsc.hostname + '/API/plugin/awsConnect/';
        var Key = directory + '/' + filename;
        var field = data.field;
        $.ajax({
          url: url,
          type: 'POST',
          data: {
            Key: Key,
            base64: data.base64,
            extension: data.extension,
            ACL: 'public-read'
          },
          success: function success(data) {
            if (!data.error) {
              logit(data);
              var bucket = data.bucket || (_joe.Data.setting.where({
                name: 'AWS_BUCKETNAME'
              })[0] || {
                value: ''
              }).value;
              var prefix = 'https://s3.amazonaws.com/' + bucket + '/';
              var url = prefix + data.Key;

              if (field.url_field) {
                var nprop = {};
                _joe.current.object[field.url_field] = url;
                nprop[field.url_field] = url;
                /*$('.joe-field[name="'+field.url_field+'"]').val(url)*/

                _joe.Fields.rerender(field.url_field, nprop);
              }

              callback(null, url);
            } else {
              callback(data.error);
            }
          }
        });
      }
    },
    ready: {
      schemas: false,
      datasets: false,
      localstorage: false,
      user: false,
      app: false
    },
    Cache: {
      reload: function reload(collections) {
        /*|{
            featured:true,
            description:'refreshes the specified collections or all in the server cache',
            tags:'cache'
        }|*/
        $.ajax({
          url: '/API/cache/update/' + (collections || ''),
          datatype: 'jsonp',
          success: function success(data) {
            if (data.error) {
              alert(data.error);
            } else {
              location.reload();
            }
          }
        });
      }
    },
    History: {
      get: function get(id, callback) {
        var id = id || self.current.item && self.current.item[self.getIDProp()];

        if (!id) {
          return;
        } //TODO: if callback == undefined


        var callback = callback || logit;
        $.ajax({
          url: '/API/history/itemid/' + id,
          dataType: 'jsonp',
          success: function success(data) {
            if (data.error) {
              return alert(error);
            }

            callback(data);
          }
        });
      },
      show: function show(history_response, specs) {
        var results = history_response.results;
        var item = self.search(history_response.query.itemid)[0] || {
          name: 'not found'
        };
        goJoe(results, {
          schema: self.SERVER.History.schema,
          title: 'History of ' + '[' + item.itemtype + ']' + ' ' + item.name
        });
      },
      schema: {
        title: '[${collection} history] ${this.historical.name}',
        sorter: ['!timestamp'],
        //TODO: wtf ${${this.changes.__length} || 0} __or
        listTitle: function listTitle(item) {
          return '<joe-title>' + (item.changes && item.changes.__length || 0) + ' changes by ${this.user.name}</joe-title>' + '<joe-subtitle>${RUN[_joe.Utils.prettyPrintDTS;${timestamp}]}</joe-subtitle>';
        },
        checkChanges: false,
        fields: ['itemid:cuid', 'collection', 'timestamp', {
          name: 'user',
          type: 'subObject',
          locked: true
        }, {
          name: 'historical',
          type: 'subObject',
          locked: true
        }, {
          name: 'changes',
          type: 'subObject',
          locked: true
        }]
      },
      button: {
        name: 'history',
        label: '&nbsp;',
        title: 'Item History',
        action: '_joe.SERVER.History.get(\'${_id}\',_joe.SERVER.History.show)',
        css: 'joe-history-button joe-left-button'
      }
    },
    User: {
      get: function get() {
        $.ajax({
          url: '/API/user/current',
          dataType: 'jsonp',
          success: function success(data) {
            if (data.error) {
              alert('error getting user:\n' + data.error);
            } else if (data.user) {
              _joe.User = data.user;
              self.SERVER.User.store();
              self.SERVER.User.signin();
              logit('logged in as ' + _joe.User.name);
            }

            self.SERVER.ready.user = true;
            self.SERVER.onLoadComplete();
          }
        });
      },
      store: function store() {
        var now = new Date();
        var time = now.getTime();
        var expireTime = time + 10000 * 36000;
        now.setTime(expireTime);
        $('#cappUser').html(_joe.User.name);
        document.cookie = '_j_user=' + _joe.User.name + ';expires=' + now.toGMTString() + ';path=/';
        document.cookie = '_j_token=' + _joe.User.token + ';expires=' + now.toGMTString() + ';path=/';
      },
      signin: function signin(obj, oldObj, changes) {
        self.SERVER.socket.emit('signin', {
          user: _joe.User
        });
        self.SERVER.socket.on('user_info', function (data) {
          if (data.error || !data.connected_users || !$c.itemCount(data.connected_users)) {
            self.Error.add('improper user data returned', null, data);
            logit('user info', data);
            return;
          }

          logit('user info', data);
          var userObj = data.connected_users[_joe.User._id];

          if (!userObj || !userObj.sockets) {
            return;
          }

          var html = '';
          var imageHtml = '';

          if (_joe.User && _joe.User.image) {
            imageHtml = '<img class="capp-user-icon" src="' + _joe.User.image + '" />';
            $('#cappUser').addClass('with-image');
          }

          $('#cappUser').html(imageHtml + '<capp-user-name><b>' + _joe.User.name + '</b>' + (userObj.sockets.length > 1 && '<small> x' + userObj.sockets.length + '</small>' || '') + '</capp-user-name>');
          $('#cappUserMenu').find('.user-info').remove();
          var user,
              user_html = '',
              apps = 'apps';

          for (var u in data.connected_users) {
            if (u != _joe.User._id) {
              user = data.connected_users[u];
              user_html += '<capp-menu-content class="user-info"><b onclick="_joe.CONNECT.start(\'' + user.user._id + '\',true);">' + user.user.name + '</b>' + fillTemplate('<span onclick="window.open(\'${url}\')"> ${app}</span>', user.sockets) + '</capp-menu-content>';
            }
          }

          $('#cappUserMenu capp-menu-panel').append(user_html);
        });
      },
      Render: {
        cubes: function cubes(ids, cssclass, user_dataset) {
          if (typeof ids == 'string') {
            ids = ids.split(',');
          }

          var users = _joe.Data[user_dataset || 'user'].where({
            _id: {
              $in: ids
            }
          });

          var html = '';
          users.map(function (user) {
            html += self.SERVER.User.Render.cube(user);
          });
          return html;
        },
        cube: function cube(userobj, cssclass) {
          var css = cssclass || 'fright';
          var u = userobj;
          var html = '';
          var name = u.fullname || u.name;
          initials = name[0] + (name.indexOf(' ') > 0 && name[name.indexOf(' ') + 1] || '');
          html += '<joe-user-cube title="' + name + '" style="background-color:' + userobj.color + '" class="joe-initials ' + css + '">' + initials + ' <span>' + name + '</span>' + '</joe-user-cube>';
          return html;
        }
      }
    },
    handleUpdate: function handleUpdate(payload) {
      try {
        var new_item, itemtype, _id, old_item;

        var idprop = '_id';

        for (var r = 0, tot = payload.results.length; r < tot; r++) {
          new_item = payload.results[r];
          var schemaname = new_item.itemtype;

          if (schemaname && self.schemas[schemaname] && self.schemas[schemaname].onload) {
            //run schema onload
            self.schemas[schemaname].onload([new_item]);
          }

          if (!_joe.Data[new_item.itemtype]) {
            _joe.Data[new_item.itemtype] = [];
          }

          old_item = _joe.Data[new_item.itemtype].where({
            _id: new_item[idprop]
          })[0] || false; //logit(JSON.stringify(payload));

          if (old_item) {
            $.extend(old_item, new_item);

            if (_joe.current.list && _joe.current.list.indexOf(old_item) != -1 && listMode) {
              _joe.reload(true);
            }
          } else {
            _joe.Data[new_item.itemtype].push(new_item);

            if (_joe.current.list && _joe.current.list.indexOf(new_item) != -1 && listMode) {
              _joe.reload(true);
            }
          }
        }

        for (var sc in self.Data) {
          capp && capp.Button.update(sc + 'SchemaBtn', {
            counter: self.Data[sc].length
          });
        }

        if (specs.onServerUpdate) {
          specs.onServerUpdate();
        }
      } catch (e) {
        self.Error.add('error handling update', e);
      }
    },
    save: function save(obj, oldObj, changes) {
      self.SERVER.socket.emit('save', {
        item: obj
      });
    },
    "delete": function _delete(obj, oldObj, changes) {
      self.SERVER.socket.emit('delete', {
        item: obj
      });
    },
    save_ajax: function save_ajax(obj, oldObj, changes) {
      ajax({
        url: '/API/save/',
        dataType: 'jsonp',
        onsuccess: function onsuccess(data) {
          if (data.error) {
            alert('Error Saving:\n' + data.error); //TODO:ROLLBACK
          } else {
            if ($.type(data.results) == "object") {
              var result = data.results;
              var itemtype = result.itemtype || self.current.schema.name;
              self.showMessage(itemtype + ', ' + result.name + '<br/><b>saved successfully</b>');
            }
          }
        },
        data: obj
      });
    },
    updateObject: function updateObject(ajaxURL, specs) {
      var specs = $.extend({
        oldObj: $.extend({}, self.current.object),
        callback: function callback(data) {
          self.showMessage(data.name + ' updated successfully');
        },
        ajaxObj: self.constructObjectFromFields(self.joe_index),
        overwrites: {},
        skipValidation: false
      }, specs || {});
      var newObj = $.extend(ajaxObj.newObj, specs.overwrites, {
        joeUpdated: new Date().toISOString()
      });

      var skipVal = _joe.propAsFuncOrValue(self.skipValidation);

      if (!skipVal) {
        var valid = self.validateObject(obj);

        if (!valid) {
          return false;
        }
      }

      $.ajax({
        url: specs.ajaxObj,
        data: newObj,
        dataType: 'jsonp',
        success: specs.callback
      });
    },
    loadSchemas: function loadSchemas(collectionsSTR) {
      if ($.type(collectionsSTR) == "Array") {
        collectionsSTR = collectionsSTR.join(',');
      }

      ajax({
        url: '/API/schema/' + collectionsSTR,
        dataType: 'jsonp',
        onsuccess: function onsuccess(data) {
          if (data.error) {
            alert(data.error);
          } else {
            var sobj; //var html = '';

            var menu = [];

            if (__jsu && ['super', 'admin', 'editor'].indexOf(__jsu.role) != -1 || $c.isEmpty(self.Data.user)) {
              menu = [__exportBtn__, _joe.SERVER.History.button, __quicksaveBtn__, __deleteBtn__, __duplicateBtn__];
            }

            var defSchema = function defSchema(schema) {
              return {
                subtitle: '${_subsetName}',
                callback: self.SERVER.save,
                onsave: function onsave(item) {
                  var current = _jco(true);

                  if (
                  /*item.status && */
                  current && item[self.getIDProp(item.itemtype)] == current._id) {
                    self.Fields.rerender('status,updated,timestamp');
                  }
                },
                "new": function _new() {
                  return {
                    _id: cuid(),
                    itemtype: schema,
                    created: new Date().toISOString()
                  };
                },
                menu: menu
              };
            };

            var default_schemas = [];

            for (var schema in data.schemas) {
              sobj = $.extend({}, defSchema(schema), data.schemas[schema]);
              unstringifyFunctions(sobj);

              if (!sobj.error) {
                if (sobj.default_schema) {
                  self.Schema.add(schema, sobj);
                  default_schemas.push(sobj);
                } else {
                  capp.Button.addFromSchema(self.Schema.add(schema, sobj));
                }
              } else {
                self.Error.add(sobj.error, null);
              }
            }
            /*TODO: add menu for additional schemas */


            capp.Menu.add('<capp-button-icon >' + _joe.SVG.icon.menu + ' </capp-button-icon><capp-menu-icon-label>default Schemas</capp-menu-icon-label> ', default_schemas, 'capp-panel', {
              labelcss: 'no-padding',
              cssclass: 'default-schemas-menu',
              template: function template(sc_btn) {
                var so = self.schemas[sc_btn.name]; //return 't';

                return capp.Button.addFromSchema(so, {
                  container: false
                });
              }
            });

            if (data.fields) {
              unstringifyFunctions(data.fields);
              self.specs.fields = self.fields = data.fields;
            }
          }

          self.SERVER.ready.schemas = true;
          self.SERVER.onLoadComplete();
        }
      });
    },
    loadDatasets: function loadDatasets(collectionsSTR) {
      if (self.specs.localStorage && typeof Storage !== "undefined") {
        self.SERVER.LocalStorage.loadData();
        var toget = [];
        APPINFO.collections.map(function (coll) {
          if (!_joe.Data[coll]) {
            toget.push(coll);
          }
        });
        self.SERVER.LocalStorage.getUpdates();

        if (toget.length) {
          logit('retrieving ' + toget.length + ' datasets ');
          self.SERVER.getDatasets(toget);
        } else {
          self.SERVER.ready.datasets = true;
          self.SERVER.onLoadComplete();
        }
      } else {
        self.SERVER.ready.localstorage = true;
        self.SERVER.getDatasets(collectionsSTR);
      }
    },
    loadApp: function loadApp() {},
    getDatasets: function getDatasets(collectionsSTR) {
      if ($.type(collectionsSTR) == "Array") {
        collectionsSTR = collectionsSTR.join(',');
      }

      ajax({
        url: '/API/datasets/' + collectionsSTR,
        dataType: 'jsonp',
        onsuccess: function onsuccess(data) {
          if (data.error) {
            alert(data.error);
          } else {
            for (var dataset in data.datasets) {
              self.addDataset(dataset, data.datasets[dataset]);
              capp && capp.Button.update(dataset + 'SchemaBtn', {
                counter: data.datasets[dataset].length
              }); //(data.datasets[dataset].length && dataset+' - '+data.datasets[dataset].length)||'');
            }
          }

          self.SERVER.ready.datasets = true;
          self.SERVER.onLoadComplete();
        }
      });
    },
    LocalStorage: {
      saveData: function saveData() {
        localStorage.joeData = JSON.stringify(_joe.Data);
        localStorage.timestamp = new Date().toISOString();
      },
      loadData: function loadData() {
        if (localStorage.joeData) {
          var allData = JSON.parse(localStorage.joeData);
          APPINFO.collections.map(function (coll) {
            if (coll == 'user' || __jsu.schemas && __jsu.schemas.indexOf(coll) != -1) {
              self.addDataset(coll, allData[coll]);
            }
          });
        }
      },
      getUpdates: function getUpdates(dateISO) {
        var dateISO = dateISO || typeof Storage !== "undefined" && localStorage.timestamp || null; //new Date().toISOString();

        var query = {
          itemtype: {
            $in: APPINFO.collections
          }
        };

        if (dateISO) {
          query.joeUpdated = {
            "$gte": dateISO
          };
        }

        $.ajax({
          url: '/API/search/',
          dataType: 'jsonp',
          data: {
            query: query
          },
          success: self.SERVER.LocalStorage.handleUpdates
        });
      },
      handleUpdates: function handleUpdates(data) {
        if (data.error) {
          alert(data.error);
        }

        logit(data.results.length + ' updates in ' + data.benchmark);
        self.SERVER.handleUpdate(data);
        self.SERVER.ready.localstorage = true;
        self.SERVER.onLoadComplete();
      }
    },
    onLoadComplete: function onLoadComplete() {
      if (self.SERVER.ready.datasets && self.SERVER.ready.schemas && self.SERVER.ready.user && self.SERVER.ready.localstorage) {
        for (var sc in self.Data) {
          capp && capp.Button.update(sc + 'SchemaBtn', {
            counter: self.Data[sc].length
          });
        }

        self.SERVER.socket.on('message', function (message) {
          if (message.sender) {
            alert(message.sender + ' -\n' + message.message);
          } else {
            alert(message);
          }
        });
        self.SERVER.socket.on('comments_updated', function (info) {
          if (self.current.object && info.itemid == self.current.object[self.getIDProp()]) {
            JOE.Fields.comments.ready();
          }
        });
        self.SERVER.socket.on('log_data', function (data, name) {
          var name = name || '';
          console.log(name, data);
        });
        self.SERVER.socket.on('item_updated', _joe.SERVER.handleUpdate);
        self.SERVER.socket.on('save_successful', function (data) {
          var result = data.results;

          if ($.type(result) == "array" && result.length == 1) {
            result = result[0];
            self.showMessage(result.itemtype + ', ' + result.name + '<br/><b>saved successfully</b>');
          } else {
            self.showMessage(result.itemtype + ', ' + result.name + '<br/><b>saved successfully</b>');
          }

          if (self.schemas[result.itemtype] && self.schemas[result.itemtype].onsave) {
            self.schemas[result.itemtype].onsave(result);
          }
        });
        self.SERVER.socket.on('connect_answer', function (data) {
          logit('answer received');
          var userid = data.user;

          if (!self.CONNECT.offerinprogress) {
            logit('REREQUESTING: ' + userid);
            self.CONNECT.offerinprogress = true;
            self.CONNECT.offerinterval = setInterval(function () {
              logit('ICE state: ' + self.CONNECT.RTC.iceConnectionState);

              if (_joe.CONNECT.RTC.iceConnectionState != "completed") {
                _joe.CONNECT.start(userid, true);
              } else {
                logit('stopping CONNECT interval - interval');
                clearInterval(self.CONNECT.offerinterval);
                self.CONNECT.offerinprogress = false;
              }
            }, 3000);
          }

          self.CONNECT.RTC.setRemoteDescription(new RTCSessionDescription(data.sdp)); //self.CONNECT.RTC.addStream(self.CONNECT.streams.local);
        });
        self.SERVER.socket.on('connect_offer', function (data) {
          var RTC = self.CONNECT.RTC;
          self.CONNECT.userid = data.user; // if(!confirm('connect request received')){
          //     return;
          // }

          _joe.CONNECT.start(data.user, false);

          logit(data);
          RTC.setRemoteDescription(new RTCSessionDescription(data.sdp)).then(function () {
            RTC.createAnswer().then(function (answer) {
              //logit(answer);
              RTC.setLocalDescription(new RTCSessionDescription(answer));
              self.SERVER.socket.emit('connect_answer', {
                user: _joe.User._id,
                target: data.user,
                sdp: answer,
                socketid: data.socketid
              });
            }, function (e) {
              console.log(e);
              alert(e);
            }), function (e) {
              console.log(e);
              alert(e);
            };
          });
        });

        _joe.Hash.read();

        if (specs.onServerLoadComplete) {
          specs.onServerLoadComplete();
        }

        if (self.specs.localStorage && typeof Storage !== "undefined") {
          self.SERVER.LocalStorage.saveData();
        }
      }
    },
    socket: self.specs.socket && io(self.specs.socket, {
      rejectUnauthorized: false
    }) || null
  };
  /*-------------------------> SERVER <--------------------------------------*/

  /*-------------------------------------------------------------------->
      //ASYNC File loading
  <--------------------------------------------------------------------*/

  this.addStylesheet = function (href, specs, cancelAppend) {
    var s = document.createElement('link');
    var specs = $.extend({
      rel: 'stylesheet'
    }, specs || {});
    $.extend(s, specs);
    s.href = href;

    if (!cancelAppend) {
      document.head.appendChild(s);
    }

    return s;
  };

  this.addScript = function (src, specs, cancelAppend) {
    var s = document.createElement('script');
    var specs = $.extend({
      type: 'text/javascript'
    }, specs || {});
    $.extend(s, specs);
    s.src = src;

    if (!cancelAppend) {
      document.body.appendChild(s);
    }

    return s;
  };
  /*-------------------------------------------------------------------->
       //Utils
   <--------------------------------------------------------------------*/


  this.nowISO = new Date().toISOString();
  var nd = new Date();
  this.times = {};
  nd.setHours(0, 0, 0, 0);
  this.times.SOD = nd.toISOString();
  nd.setHours(24, 0, 0, 0);
  this.times.EOD = nd.toISOString();
  this.Utils = {
    /*|{
    featured:true,
    description:'get the names setting, returns eval'd code or a function,
    specs:'raw,params,object'
    tags:'utils,settings'
    }|*/
    stopPropagation: function stopPropagation(e) {
      var e = e || window.event;
      if (e.stopPropagation) e.stopPropagation();
      if (e.preventDefault) e.preventDefault();
    },
    getPossibleValues: function getPossibleValues(propertyName, schema, specs) {
      //TODO:
      //array format
      //sort array
      var specs = $.extend({
        format: 'object'
      }, specs || {});

      if (specs["static"] && _joe.Cache["static"][specs["static"]]) {
        return _joe.Cache["static"][specs["static"]];
      }

      var types = {},
          propval;

      _joe.Data[schema].map(function (a) {
        propval = a[propertyName];

        if (specs.allowNulls || propval != null) {
          types[propval] = types[propval] || 0;
          types[propval]++;
        }
      });

      if (specs["static"]) {
        _joe.Cache["static"][specs["static"]] = types;
      }

      return types;
    },
    Settings: function Settings(settingName, specs) {
      var specs = specs || {};
      var s = (self.Data.setting || []).where({
        name: settingName
      })[0];
      var setting = s || {};

      if (specs.object) {
        return setting;
      }

      if (setting.setting_type == "code" && !specs.raw) {
        if (setting.value.trim().indexOf('function') == 0 && specs.params) {
          var p = tryEval(s.value);

          if (!p) {
            return specs.params;
          }

          return p(specs.params);
        }

        return tryEval(s.value);
      }

      return setting.value;
    },
    toDollars: function toDollars(amount) {
      return parseFloat(amount).toFixed(2);
    },
    joeState: self.getState,
    nextDay: function nextDay(x) {
      var now = new Date();
      now.setDate(now.getDate() + (x + (7 - now.getDay())) % 7);
      return now;
    },
    toDateString: function toDateString(dt, specs) {
      var specs = specs || {};
      var dt = dt || new Date();

      if (typeof dt == 'string') {
        dt = new Date(dt);
      }

      var dts_str = '';
      var monthstr = dt.getMonth() + 1;
      var dayStr = ('0' + dt.getDate()).slice(-2);
      var yearStr = dt.getUTCFullYear().toString();
      dts_str += (monthstr < 10 ? '0' + monthstr : monthstr) + '/' + dayStr + '/' + (specs.fullyear && yearStr || yearStr.substr(2, 4));
      return dts_str;
    },
    prettyPrintDTS: function prettyPrintDTS(dts, specs) {
      var specs = $.extend({
        showtime: true,
        showdate: true
      }, specs || {});
      var dts = dts || new Date();

      if (typeof dts == 'string') {
        dts = new Date(dts);
      }

      var dts_str = '';

      if (specs.showtime) {
        var ampm = 'a';
        var dmins = dts.getMinutes().toString();
        var dhours = dts.getHours();

        if (dhours > 12) {
          ampm = 'p';
          dhours -= 12;
        }

        if (!dhours) {
          dhours = 12;
        }

        dts_str += dhours + ':' + (dmins.length == 2 ? dmins : '0' + dmins) + ampm;
      }

      if (specs.showdate && specs.showtime) {
        dts_str += ' ';
      }

      if (specs.showdate) {
        dts_str += dts.getMonth() + 1 + '/' + dts.getDate() + '/' + dts.getUTCFullYear().toString().substr(2, 4);
      }

      return dts_str;
    },
    getRandomColor: function getRandomColor() {
      var letters = '0123456789ABCDEF';
      var color = '#';

      for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
      }

      return color;
    },
    benchmarkit: function benchmarkit(torun, name) {
      var bm = new Benchmarker();
      var t = torun();

      _bmResponse(bm, 'benchmarker', name || '');

      return t;
    }
  };
  /*-------------------------------------------------------------------->
    //SVGs
  <--------------------------------------------------------------------*/

  this.SVG = {
    icon: {
      'left': '<svg xmlns="http://www.w3.org/2000/svg" viewBox="-5 -5 36 36"><path d="M11.3 21C7.1 18.2 3.3 13.8 3.2 13.6 3.1 13.5 3 13.2 3 13 3 12.8 3.1 12.5 3.2 12.3 3.3 12.1 7.2 7.7 11.3 4.9 11.6 4.7 12 4.7 12.4 4.8 12.8 5.1 13 5.4 13 5.8L13 9.5C13 9.5 21.7 10 22.1 10.3 22.7 10.7 23 12 23 13 23 14.1 22.6 15.2 22.1 15.6 21.7 15.9 13 16.5 13 16.5L13 20.2C13 20.6 12.8 20.9 12.4 21.1 12.1 21.3 11.7 21.2 11.3 21Z"/></svg>',
      'cancel': '<svg xmlns="http://www.w3.org/2000/svg" viewBox="-5 -5 36 36"><path d="M13 0.2C5.9 0.2 0.2 5.9 0.2 13 0.2 20.1 5.9 25.8 13 25.8 20.1 25.8 25.8 20.1 25.8 13 25.8 5.9 20.1 0.2 13 0.2ZM18.8 17.4L17.4 18.8C17.1 19 16.7 19 16.5 18.8L13 15.3 9.5 18.8C9.3 19 8.9 19 8.6 18.8L7.2 17.4C7 17.1 7 16.7 7.2 16.5L10.7 13 7.2 9.5C7 9.3 7 8.9 7.2 8.6L8.6 7.2C8.9 7 9.3 7 9.5 7.2L13 10.7 16.5 7.2C16.7 7 17.1 7 17.4 7.2L18.8 8.6C19 8.9 19 9.3 18.8 9.5L15.3 13 18.8 16.5C19 16.7 19 17.1 18.8 17.4Z"/></svg>',
      close: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="-6 -6 44 44"><path d="M26 22.3L22.3 26c0 0-5.9-6.3-6.3-6.3C15.6 19.7 9.7 26 9.7 26L6 22.3c0 0 6.3-5.8 6.3-6.3C12.3 15.5 6 9.7 6 9.7l3.7-3.7c0 0 5.9 6.3 6.3 6.3 0.4 0 6.3-6.3 6.3-6.3L26 9.7c0 0-6.3 5.9-6.3 6.3C19.7 16.4 26 22.3 26 22.3z"/></svg>',
      unsaved: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="-8 -8 48 48"><g data-name="Layer 2"><path d="M24.4 3.4h-21V28.6H28.6V7.5Zm-5 2.8v4.5H16.6V6.1Zm6.5 19.7H6.1V6.1h3.5v7.2H22.2V6.1h1.1l2.6 2.5Z"/><polygon points="20.9 22.5 18 19.6 20.9 16.7 18.9 14.7 16 17.6 13.1 14.7 11.1 16.7 14.1 19.6 11.1 22.5 13.1 24.5 16 21.5 18.9 24.5 20.9 22.5"/></g></svg>',
      'sidebar-right': '<svg xmlns="http://www.w3.org/2000/svg" viewBox="-4 -4 48 48"><g><path d="M38,36H2V4H38ZM26,6H4V34H26Z"/></g></svg>',
      'sidebar-left': '<svg xmlns="http://www.w3.org/2000/svg" viewBox="-4 -4 48 48"><path d="M2 4H38V36H2ZM14 34H36V6H14Z"/></svg>',
      'menu': '<svg xmlns="http://www.w3.org/2000/svg" viewBox="-5 -5 42 42"><path d="M16 6C14.9 6 14 6.9 14 8 14 9.1 14.9 10 16 10 17.1 10 18 9.1 18 8 18 6.9 17.1 6 16 6zM16 14C14.9 14 14 14.9 14 16 14 17.1 14.9 18 16 18 17.1 18 18 17.1 18 16 18 14.9 17.1 14 16 14zM16 22C14.9 22 14 22.9 14 24 14 25.1 14.9 26 16 26 17.1 26 18 25.1 18 24 18 22.9 17.1 22 16 22z"/></svg>',
      'sections': '<svg xmlns="http://www.w3.org/2000/svg" viewBox="-10 -10 120 120"><polygon points="50 90 35 75 65 75 50 90"/><polygon points="50 10 65 25 35 25 50 10"/><circle cx="50" cy="50" r="10"/></svg>',
      'help': '<svg viewBox="-384 -384 1792 1792" style=""><g><path d="M448 768h128v-128h-128v128zM512 256c-96 0-192 96-192 192h128c0-32 32-64 64-64s64 32 64 64c0 64-128 64-128 128h128c64-22 128-64 128-160s-96-160-192-160zM512 0c-283 0-512 229-512 512s229 512 512 512 512-229 512-512-229-512-512-512zM512 896c-212 0-384-172-384-384s172-384 384-384 384 172 384 384-172 384-384 384z"></path></g></svg>'
    }
  };
  /*-------------------------------------------------------------------->
       //JCO - current object
  <--------------------------------------------------------------------*/

  self.getCurrentObject = function (construct, force) {
    /*|{
    featured:true,
    description:'gets the current object JOE is editing, if construct, has current user updates.',
    alias:'_jco(construct)'
    }|*/
    if (construct) {
      if (self.current.constructed) {
        return self.current.constructed;
      }

      var obj = self.constructObjectFromFields(); //self.current.construct = obj;

      if (!obj[self.getIDProp()] && !force) {
        return self.current.object;
      }

      return obj;
    }

    return self.current.object;
  }; // if(window){


  window._jco = self.getCurrentObject; // }

  /*-------------------------------------------------------------------->
       //GOOGLE - GOOGLE CLIENT
  <--------------------------------------------------------------------*/

  this.Google = {
    Calendar: {
      list: function list() {
        if (!self.Google.Calendar.calendars) {
          self.Google.loadCalendarApi();
          self.Google.Calendar.list();
          /*
          gapi.auth.authorize({
                  'client_id': self.Google.client_id,
                  'scope': self.Google.scopes.join(' '),
                  'immediate': false
              }, function(authResult){
                      if (authResult && !authResult.error) {
                          console.log(authResult);
                          self.Google.loadCalendarApi();
                          self.Google.Calendar.list();
                      }else{
                          alert('error authorizing calendars');
                          self.Error.add('google client:'+authResult.error+','+authResult.error_subtype,
                          authResult.error+','+authResult.error_subtype,{c:client_id,g:gapi,auth:gapi.auth,authResult})
                      }
              })*/

          return;
        }

        goJoe(_joe.Google.Calendar.calendars, {
          schema: {
            idprop: 'id',
            listWindowTitle: 'Google Calendars',
            title: 'gcal: ${summary}',
            listTitle: '<joe-title>${summary}</joe-title><joe-subtitle>${id}</joe-subtitle>',
            itemMenu: function itemMenu(item) {
              var buttons = [{
                name: 'view',
                action: 'window.open(\'https://calendar.google.com/calendar/embed?mode=WEEK&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=${id}&amp;color=%236B3304&amp;ctz=America%2FChicago\');'
              }];
              return buttons;
            },
            sorter: ['summary'],
            checkChanges: false,
            stripeColor: function stripeColor(cal) {
              return cal.backgroundColor;
            },
            bgColor: function bgColor(cal) {
              if (_joe.Google.Calendar && _joe.Google.Calendar.active) {
                if (_joe.Google.Calendar.active.id == cal.id) {
                  return 'goldenrod';
                }
              }

              return '';
            }
          }
        });
      }
    },
    client_id: null,
    init: function init() {
      console.log('google authed', self.Google.access_token);
      self.Google.listCalendars();
    },
    scopes: ["https://www.googleapis.com/auth/calendar"],
    loadCalendarApi: function loadCalendarApi(access_token, callback) {
      if (access_token) {
        self.Google.access_token = access_token;
      }

      var callback = callback || self.Google.init;
      gapi.client.load('calendar', 'v3', callback);
    },
    listCalendars: function listCalendars() {
      var request = gapi.client.calendar.calendarList.list();
      request.execute(function (resp) {
        self.Google.Calendar.calendars = resp.items;
        self.Google.Calendar.active = self.Utils.Settings('GOOGLE_CALENDARID') && resp.items.where({
          id: self.Utils.Settings('GOOGLE_CALENDARID')
        })[0] || resp.items.where({
          primary: true
        })[0] || false;
        capp && capp.Reload.all();
      });
    }
  };

  this.Google.authorize = function (immediate, callback, hidePopup) {
    var client_id = self.Google.client_id = self.Utils.Settings('GOOGLE_CLIENTID');

    if (self.Google.access_token) {
      self.Google.loadCalendarApi();
      return;
    }

    if (client_id && gapi && gapi.auth) {
      gapi.auth.authorize({
        'client_id': self.Google.client_id,
        'scope': self.Google.scopes.join(' '),
        'immediate': true
      }, function (authResult) {
        if (authResult && !authResult.error) {
          self.Google.loadCalendarApi(authResult.access_token);
        } else {
          if (JACmode) {
            var doit = confirm('jac mode enabled, login to google?');
            if (!doit) return;
          }

          gapi.auth.authorize({
            'client_id': self.Google.client_id,
            'scope': self.Google.scopes.join(' '),
            'immediate': false
          }, function (authResult) {
            if (authResult && !authResult.error) {
              self.Google.loadCalendarApi(authResult.access_token);
            } else {
              self.Error.add('google client:' + authResult.error + ',' + authResult.error_subtype, authResult.error + ',' + authResult.error_subtype, {
                c: client_id,
                g: gapi,
                auth: gapi.auth,
                authResult: authResult
              });
            }
          });
        }
      });
    } else {
      self.Error.add('error in google client api', null, {
        c: client_id,
        g: gapi,
        auth: gapi.auth
      });
    }
  };

  function parseDateTimeForGoogle() {}

  this.Google.Calendar.Event = {
    buildResource: function buildResource(event) {
      var template = self.Utils.Settings('GOOGLE_EVENT_TEMPLATE', {
        params: event
      }) || '${info} \r\n ${description}'; // var descriptionString =__removeTags(fillTemplate(template,$.extend({
      //     URL:'//'+__jsc.hostname+':'+__jsc.port+location.pathname+location.hash
      // },event)));

      var descriptionString = fillTemplate(template, $.extend({
        URL: '//' + __jsc.hostname + ':' + __jsc.port + location.pathname + location.hash
      }, event));
      var res = {
        sendNotifications: event.sendNotifications || false,
        summary: event.name || event.address + ' ' + (event.event_type || ''),
        description: descriptionString,
        location: event.location || fillTemplate('${street_address} ${city} ${state} ${zip}', event),
        "start": {
          "dateTime": $c.format(new Date(event.date + ' ' + event.start_time), 'Y-m-d') + 'T' + $c.format(new Date(event.date + ' ' + event.start_time), 'H:i:00.000') + event.timezone_offset
        },
        "end": {
          "dateTime": $c.format(new Date(event.date + ' ' + event.end_time), 'Y-m-d') + 'T' + $c.format(new Date(event.date + ' ' + event.end_time), 'H:i:00.000') + event.timezone_offset
        },
        id: event._id.replace(/-/g, ''),
        attendees: []
      };
      var lead_person = event.leader || event.lead;

      if (lead_person) {
        var leader = self.Cache.get(lead_person);
        res.attendees.push({
          name: leader.fullname || leader.first && leader.first + ' ' + leader.last || leader.name,
          id: leader._id,
          email: leader.email
        });
      }

      (event.attendees || event.team_members || []).map(function (att) {
        var attendee = self.Cache.get(att);
        res.attendees.push({
          name: attendee.fullname || attendee.first && attendee.first + ' ' + attendee.last || attendee.name,
          id: attendee._id,
          email: attendee.email
        });
      });
      return res;
    },
    getObject: function getObject(eventid) {
      if (self.current.object && self.current.object._id == eventid) {
        return _jco(true);
      } else {
        return self.getDataItem(eventid, 'event');
      }
    },
    add: function add(eventid, force, saveFirst) {
      if (!self.Google.access_token || !self.Google.Calendar.calendars) {
        alert('please login to your google account and then try again.');
        self.Google.authorize(true);
        return;
      }

      try {
        var event = self.Google.Calendar.Event.getObject(eventid);

        if (!event) {
          alert('event not found: ' + eventid);
          return;
        }

        if (!event.published && !force) {
          alert('event not published: ' + event.name);
          return;
        }

        var resource = self.Google.Calendar.Event.buildResource(event);
        var request = gapi.client.calendar.events.insert({
          'calendarId': self.Google.Calendar.active.id,
          'resource': resource,
          'sendNotifications': resource.sendNotifications
        });
        request.sendNotifications = resource.sendNotifications;
        request.execute(function (resp) {
          if (resp.error && resp.error.code == 409) {
            logit('updating');
            var ovr = {
              status: 'confirmed'
            };

            if (!event.published) {
              ovr.status = "cancelled";
            }

            ovr.save = saveFirst;
            self.Google.Calendar.Event.update(eventid, ovr);
          } else {
            if (saveFirst) {
              _joe.current.object.published_ts = new Date();
              self.updateObject(null, null, true);

              _joe.rerenderField('published_ts');
            }
          }

          logit(resp);
        });
      } catch (e) {
        self.Error.add('error adding event', e, eventid);
        alert('error adding event \n' + e);
      }
    },
    update: function update(eventid, specs) {
      try {
        var saveFirst = specs.save; //var event = self.getDataItem(eventid,'event');

        var event = self.Google.Calendar.Event.getObject(eventid);
        var resource = $.extend(self.Google.Calendar.Event.buildResource(event), specs || {
          status: 'confirmed'
        });
        var request = gapi.client.calendar.events.patch({
          'calendarId': self.Google.Calendar.active.id,
          'resource': resource,
          'eventId': eventid.replace(/-/g, ''),
          'sendNotifications': resource.sendNotifications
        });
        request.sendNotifications = resource.sendNotifications;
        request.execute(function (resp) {
          if (resp.error) {
            alert(resp.error.message || resp.error);
          } else {
            if (saveFirst) {
              _joe.current.object.published_ts = new Date();
              self.updateObject(null, null, true);

              _joe.rerenderField('published_ts');
            }
          }

          logit(resp);
        });
      } catch (e) {
        self.Error.add('error updating event', e, eventid);
      }
    },
    publish: function publish(query) {
      if (!self.Google.Calendar.calendars || !self.Google.access_token) {
        gapi.auth.authorize({
          'client_id': self.Google.client_id,
          'scope': self.Google.scopes.join(' '),
          'immediate': false
        }, function (authResult) {
          if (authResult && !authResult.error) {
            self.Google.loadCalendarApi();
            self.Google.Calendar.Event.publish();
          } else {
            alert('error authorizing calendars');
            self.Error.add('google client:' + authResult.error + ',' + authResult.error_subtype, authResult.error + ',' + authResult.error_subtype, {
              c: client_id,
              g: gapi,
              auth: gapi.auth,
              authResult: authResult
            });
          }
        });
        return;
      }

      var query = query || {
        published: true
      };

      if (self.Data.event) {
        var topublish = self.Data.event.where(query);
        var notpublish = self.Data.event.where(function () {
          return !this.published;
        });

        if (confirm('publish ' + topublish.length + ' events?\n you have ' + notpublish.length + '/' + self.Data.event.length + ' unpublished events')) {
          topublish.map(function (ev) {
            self.Google.Calendar.Event.add(ev._id);
          });
        }
      }
    }
  };
  /*-------------------------------------------------------------------->
    //AUTOINIT
  <--------------------------------------------------------------------*/

  if (self.specs.autoInit) {
    self.init();
  }

  return this;
}

function unstringifyFunctions(propObject) {
  for (var p in propObject) {
    if (typeof propObject[p] == 'string' && propObject[p].indexOf('(function') == 0) {
      propObject[p] = tryEval('(' + propObject[p].replace(/;r\n/g, ';\n').replace(/<br \/>/g, '') + ')');
      
    } else if (_typeof(propObject[p]) == "object") {
      unstringifyFunctions(propObject[p]);
    }
  }

  return propObject;
}

var __gotoJoeSection;

var __clearDiv__ = '<div class="clear"></div>'; // var __createBtn__ = {name:'create',label:'Create', action:'_joe.createObject();', css:'joe-orange-button joe-add-button joe-iconed-button'};

var __createBtn__ = {
  name: 'create',
  icon: 'plus',
  action: 'create',
  color: "orange",
  schema: function schema() {
    return _joe.current.schema && _joe.current.schema.name;
  },
  css: ' joe-add-button joe-iconed-button'
};
var __quicksaveBtn__ = {
  name: 'quicksave',
  label: 'Save',
  action: '_joe.updateObject(this,null,true);',
  css: 'joe-quicksave-button joe-confirm-button joe-iconed-button'
};
var __saveBtn__ = {
  name: 'save',
  label: 'Save',
  action: '_joe.updateObject(this);',
  css: 'joe-save-button joe-confirm-button joe-iconed-button'
};
var __deleteBtn__ = {
  name: 'delete',
  label: 'Delete',
  action: '_joe.deleteObject(this);',
  css: 'joe-delete-button joe-iconed-button joe-red-button',
  condition: function condition() {
    return _joe.isNewItem && !_joe.isNewItem();
  }
};
var __multisaveBtn__ = {
  name: 'save_multi',
  label: 'Multi Save',
  action: '_joe.updateMultipleObjects(this);',
  css: 'joe-save-button joe-confirm-button joe-multi-only'
};
var __multideleteBtn__ = {
  name: 'delete_multi',
  label: 'Multi Delete',
  action: '_joe.deleteMultipleObjects(this);',
  css: 'joe-delete-button joe-multi-only'
};
var __selectAllBtn__ = {
  name: 'select_all',
  label: 'select all',
  action: '_joe.selectAllItems();',
  css: 'joe-left-button joe-multi-always'
};
var __replaceBtn__ = {
  name: 'replace',
  label: 'Replace',
  action: '_joe.updateRendering(this);',
  css: 'joe-replace-button joe-confirm-button'
};
var __duplicateBtn__ = {
  name: 'duplicate',
  label: 'Duplicate',
  action: '_joe.duplicateObject();',
  css: 'joe-left-button joe-iconed-button joe-duplicate-button'
};
var __exportBtn__ = {
  name: 'export',
  display: '<b>{&#8593;}</b>',
  condition: function condition() {
    return _joe.User && _joe.User.role == "super";
  },
  css: 'joe-export-button joe-left-button joe-grey-button joe-orangegrey-button',
  action: '_joe.IO.export(null,true)'
};
var __systemFieldsSection = [{
  section_start: 'system',
  collapsed: true
}, '_id', 'created', 'itemtype', {
  section_end: 'system'
}];
var __defaultButtons = [__createBtn__];
var __defaultObjectButtons = [__deleteBtn__, __saveBtn__];
var __defaultMultiButtons = [__multisaveBtn__, __multideleteBtn__];

function __removeTags(str) {
  if (!str) {
    return '';
  }

  return str.replace(/<(?:.|\n)*?>/gm, '');
}

function _COUNT(array) {
  if (!array) {
    return 0;
  }

  if (array.isArray()) {
    return array.length;
  } else if (array.isString()) {
    return array.length;
  } else if (array.isFunction) {
    return array.toString().length;
  }

  return 0;
}

;

function showJOEWarnings() {
  goJoe(_joe.current.benchmarkers._warnings, {
    schema: {
      title: 'benchmarker warnings',
      listTitle: '<joe-title>${name}</joe-title><joe-subtitle>${info}</joe-subtitle>',
      checkChanges: false
    }
  });
}

function _bmResponse(benchmarker, message, bmname, threshhold) {
  var t = benchmarker.stop();
  var m = message + ' in ' + t + ' secs';
  _joe.current.benchmarkers = _joe.current.benchmarkers || {};
  var name = bmname || message;
  var payload = {
    info: t,
    name: name,
    _id: cuid()
  };
  _joe.current.benchmarkers._warnings = _joe.current.benchmarkers._warnings || [];
  _joe.current.benchmarkers[name] = payload;

  if ($c.DEBUG_MODE && window.console && window.console.warn) {
    if (t > .4) {
      window.console.warn(m);
      _joe.current.benchmarkers[name].warn = true;

      _joe.current.benchmarkers._warnings.push(payload);
    } else {
      if (!threshhold || t > threshhold) {
        logit(m);
      }
    }
  } else {
    logit(m);
  }
}

function beginLogGroup(name, expanded) {
  if (!$c.DEBUG_MODE) return;

  if (expanded) {
    window.console && window.console.group && console.group(name);
  } else {
    window.console && window.console.groupCollapsed && console.groupCollapsed(name);
  }
}

function endLogGroup() {
  if (!$c.DEBUG_MODE) return;
  window.console && window.console.groupEnd && console.groupEnd();
}

function _renderUserCubes(user, cssclass, user_dataset) {
  var css = cssclass || 'fleft';
  var u = user;
  var html = '';
  var title, initials;
  var color = user.color || '#cbcbcb';

  if (u.first && u.last) {
    title = u.first + ' ' + u.last;
    initials = u.first[0] + u.last[0];
  } else {
    var name = u.fullname || u.name;
    title = name;
    initials = name[0] + (name.indexOf(' ') > 0 && name[name.indexOf(' ') + 1] || '');
  }

  html += '<joe-user-cube title="' + title + '" class="' + css + '" style="background-color:' + color + '">' + initials + ' <span>' + title + '</span></joe-user-cube>';
  return html;
}
/*-------------------------------------------------------------------->
 Watch Polyfill
 <--------------------------------------------------------------------*/

/*
 * object.watch polyfill
 *
 * 2012-04-03
 *
 * By Eli Grey, http://eligrey.com
 * Public Domain.
 * NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.
 */

/*
// object.watch
if (!Object.prototype.observeProp && Object.defineProperty) {
    Object.defineProperty(Object.prototype, "watch", {
        enumerable: false
        , configurable: true
        , writable: false
        , value: function (prop, handler) {
            var
                oldval = this[prop]
                , newval = oldval
                , getter = function () {
                    return newval;
                }
                , setter = function (val) {
                    oldval = newval;
                    handler = handler || logit;
                    var specs = {object:this, prop:prop, oldval:oldval, val:val};
                    handler(specs);
                    return newval = val;
                    //return
                }
                ;

            if (delete this[prop]) { // can't watch constants
                Object.defineProperty(this, prop, {
                    get: getter
                    , set: setter
                    , enumerable: true
                    , configurable: true
                });
            }
        }
    });
}

// object.unwatch
if (!Object.prototype.observeProp && Object.defineProperty) {
    Object.defineProperty(Object.prototype, "unwatch", {
        enumerable: false
        , configurable: true
        , writable: false
        , value: function (prop) {
            var val = this[prop];
            delete this[prop]; // remove accessors
            this[prop] = val;
        }
    });
}*/

/*-------------------------------------------------------------------->
CRAYDENT UPDATES
 <--------------------------------------------------------------------*/
//UNTIL VERBOSE IS REMOVED


function warn(message) {
  console && console.warn && console.warn(message);
}

function formatMoney(number, places, symbol, thousand, decimal) {
  number = number || 0;
  places = !isNaN(places = Math.abs(places)) ? places : 2;
  symbol = symbol !== undefined ? symbol : "$";
  thousand = thousand || ",";
  decimal = decimal || ".";
  var negative = number < 0 ? "-" : "",
      i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
      j = (j = i.length) > 3 ? j % 3 : 0;
  return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
}