"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _wrapNativeSuper(Class) { var _cache = typeof Map === "function" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null || !_isNativeFunction(Class)) return Class; if (typeof Class !== "function") { throw new TypeError("Super expression must either be null or a function"); } if (typeof _cache !== "undefined") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }

function isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _construct(Parent, args, Class) { if (isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }

function _isNativeFunction(fn) { return Function.toString.call(fn).indexOf("[native code]") !== -1; }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var AccountInfo =
/*#__PURE__*/
function (_HTMLElement) {
  _inherits(AccountInfo, _HTMLElement);

  function AccountInfo() {
    _classCallCheck(this, AccountInfo);

    return _possibleConstructorReturn(this, _getPrototypeOf(AccountInfo).call(this));
  }

  _createClass(AccountInfo, [{
    key: "connectedCallback",
    value: function connectedCallback() {
      this.plaidid = this.getAttribute('plaid-id');
      this.start_date = new Date(this.getAttribute('start-date')).format('Y-m-d');
      this.end_date = new Date(this.getAttribute('end-date')).format('Y-m-d');
      this.acctName = this.getAttribute('acct-name');
      this.acctId = this.getAttribute('acct-id');
      this.joe_id = this.getAttribute('joe-id');
      this.render();
      var styles = "\n      account-info {\n        display: block;\n        padding: 0;\n        background: #fff;\n        /* border: 1px solid #eee; */\n        margin: 5px auto;\n        border-bottom: 1px solid #ccc;\n        padding-bottom: 5px;\n      }\n      acct-row {\n        display: block;\n        font-weight: bold;\n        font-size: 14px;\n        padding: 4px;\n      }\n      acct-row.charge{background: rgba(255,0,0,.1);}\n      acct-row.payment{background: rgba(0,255,0,.1);}\n      acct-row span.trans-count {\n        float: right;\n        font-weight: normal;\n      }\n    \n    ";
      document.getElementById(_joe.Components.styleTag).innerHTML += styles;
    }
  }, {
    key: "render",
    value: function render() {
      var self = this;
      var data = {
        bank: self.plaidid,
        start_date: self.start_date,
        end_date: self.end_date
      };
      self.acctId && (data.account_ids = self.acctId);
      $.ajax({
        url: '/API/plugin/money/transactions?',
        data: data,
        success: function success(data) {
          var transUrl = "/API/plugin/money/transactions?bank=" + self.plaidid + "&start_date=" + self.start_date + "&end_date=" + self.end_date + (self.acctId && "&account_ids=" + self.acctId || '');

          if (data.error) {
            this.innerHTML = data.error;
            return;
          }

          var transactions = data.transactions;

          if (self.acctId && self.acctId != "undefined") {
            transactions = data.transactions.filter(function (t) {
              return t.account_id == self.acctId;
            });
          }

          var summary = {
            total: {
              count: 0,
              sum: 0
            },
            charge: {
              count: 0,
              sum: 0
            },
            payment: {
              count: 0,
              sum: 0
            }
          };
          transactions.map(function (tran) {
            summary.total.count++;
            summary.total.sum += tran.amount;

            if (tran.amount > 0) {
              summary.charge.count++;
              summary.charge.sum += tran.amount;
            } else {
              summary.payment.count++;
              summary.payment.sum += tran.amount;
            }
          });

          function renderRow(obj, label) {
            var money = ('$' + summary[obj].sum.toFixed(2)).replace('$-', '-$');
            return "<acct-row class=\"" + obj + "\">" + money + "\n          <span class=\"trans-count\">" + summary[obj].count + " " + (label || 'transactions') + "</span>\n          </acct-row>";
          }

          self.innerHTML = "<joe-subtitle>" + self.acctName + "</joe-subtitle>\n        <joe-subtext>" + self.joe_id + "</joe-subtext>\n        " + renderRow('charge', 'charges') + "\n        " + renderRow('payment', 'payments') + "\n        " + renderRow('total', 'transactions') + "\n        <a href=\"" + transUrl + "\" target=\"_blank\" class=\"joe-subtext\">view transactions json</a>\n      ";
        }
      });
    }
  }, {
    key: "attributeChangedCallback",
    value: function attributeChangedCallback(attr, oldValue, newValue) {
      this.render();
    }
  }, {
    key: "disconnectedCallback",
    value: function disconnectedCallback() {}
  }], [{
    key: "observedAttributes",
    get: function get() {
      return [];
    }
  }]);

  return AccountInfo;
}(_wrapNativeSuper(HTMLElement)); // window.addEventListener('load', function(){


window.customElements.define("account-info", AccountInfo); // })