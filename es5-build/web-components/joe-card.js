"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _wrapNativeSuper(Class) { var _cache = typeof Map === "function" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null || !_isNativeFunction(Class)) return Class; if (typeof Class !== "function") { throw new TypeError("Super expression must either be null or a function"); } if (typeof _cache !== "undefined") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }

function isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _construct(Parent, args, Class) { if (isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }

function _isNativeFunction(fn) { return Function.toString.call(fn).indexOf("[native code]") !== -1; }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var JoeCard =
/*#__PURE__*/
function (_HTMLElement) {
  _inherits(JoeCard, _HTMLElement);

  function JoeCard() {
    _classCallCheck(this, JoeCard);

    return _possibleConstructorReturn(this, _getPrototypeOf(JoeCard).call(this));
  }

  _createClass(JoeCard, [{
    key: "connectedCallback",
    value: function connectedCallback() {
      var atts = _joe.Components.getAttributes(this);

      var styles = "\n      joe-card{\n        display:block;\n        position:relative;\n        box-sizing:border-box;\n        padding:5px;\n        border:1px solid #eee;\n        background-color:#fff;\n      }\n      .joe-panel-content-option-expander joe-card{\n        margin-bottom:5px;\n        background:#fcfcfc;\n      }\n      joe-card > joe-title {\n        border-bottom: 1px solid #f2f2f2;\n        margin: 0 -5px;\n        padding: 0px 5px 5px 5px;\n        background:#fff;\n      }\n    ";

      _joe.Components.appendStyles(styles);

      this.item = this.getJoeItem(atts.item || atts.itemId || atts.item_id); //var schemaname = atts.schema || this.item.itemtype;

      this.schema = _joe.schemas[atts.schema || this.item.itemtype];
      this.render();
      this.classList.add("joe-card");
    }
  }, {
    key: "getJoeItem",
    value: function getJoeItem(itemid) {
      return _joe.getDataItem(itemid);
    }
  }, {
    key: "render",
    value: function render() {
      var atts = _joe.Components.getAttributes(this);

      if (!this.schema) {
        return;
      }

      var card_template = "<joe-title>" + this.item.name + "</joe-title>";
      this.cardTemplate = this.schema.card && _joe.propAsFuncOrValue(this.schema.card, this.item);

      if (this.innerHTML) {}
    }
  }, {
    key: "attributeChangedCallback",
    value: function attributeChangedCallback(attr, oldValue, newValue) {
      this.render();
    }
  }, {
    key: "disconnectedCallback",
    value: function disconnectedCallback() {}
  }], [{
    key: "observedAttributes",
    get: function get() {
      return [];
    }
  }]);

  return JoeCard;
}(_wrapNativeSuper(HTMLElement)); // window.addEventListener('load', function(){


window.customElements.define("joe-card", JoeCard); // })