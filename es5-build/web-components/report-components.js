"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _wrapNativeSuper(Class) { var _cache = typeof Map === "function" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null || !_isNativeFunction(Class)) return Class; if (typeof Class !== "function") { throw new TypeError("Super expression must either be null or a function"); } if (typeof _cache !== "undefined") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }

function isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _construct(Parent, args, Class) { if (isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }

function _isNativeFunction(fn) { return Function.toString.call(fn).indexOf("[native code]") !== -1; }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function getComponentAttributes(comp) {
  var d = {};

  for (var a = 0; a < comp.attributes.length; a++) {
    d[comp.attributes[a].name] = comp.attributes[a].value;
  }

  return d;
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');

  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];

    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }

    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }

  return "";
}

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  var expires = "expires=" + d.toUTCString();
  var cookie_string = cname + "=" + cvalue + ";" + expires + ";path=" + location.pathname + location.search; //var cookie_string = cname + "=" + cvalue + ";" + expires + ";";

  document.cookie = cookie_string;
}

var ReportListItem =
/*#__PURE__*/
function (_HTMLElement) {
  _inherits(ReportListItem, _HTMLElement);

  function ReportListItem() {
    _classCallCheck(this, ReportListItem);

    return _possibleConstructorReturn(this, _getPrototypeOf(ReportListItem).call(this));
  }

  _createClass(ReportListItem, [{
    key: "connectedCallback",
    value: function connectedCallback() {
      this.classList.add('report-list-item');
      this.container = this.parentElement;
      this.render();
    }
  }, {
    key: "render",
    value: function render() {
      var atts = getComponentAttributes(this);

      if (this.hasAttribute('toggleable')) {
        this.onclick = this.toggleItem;

        if (window && window.localStorage) {
          var completed = (window.localStorage.getItem('completeItems') || '').split(',');

          if (completed.indexOf(this.id) != -1) {
            this.classList.add('strikethrough');
          }
        }
      }
    }
  }, {
    key: "toggleItem",
    value: function toggleItem() {
      this.classList.toggle('strikethrough');
      var completed = [];
      document.querySelectorAll('report-list-item.strikethrough').map(function (dom) {
        completed.push(dom.id);
      }); // Create or update the cookie:

      if (window && window.localStorage) {
        window.localStorage.setItem('completeItems', completed.join(','));
      }
    }
  }, {
    key: "attributeChangedCallback",
    value: function attributeChangedCallback(attr, oldValue, newValue) {
      this.render();
    }
  }, {
    key: "disconnectedCallback",
    value: function disconnectedCallback() {}
  }], [{
    key: "observedAttributes",
    get: function get() {
      return [];
    }
  }]);

  return ReportListItem;
}(_wrapNativeSuper(HTMLElement));

window.customElements.define("report-list-item", ReportListItem);