"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _wrapNativeSuper(Class) { var _cache = typeof Map === "function" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null || !_isNativeFunction(Class)) return Class; if (typeof Class !== "function") { throw new TypeError("Super expression must either be null or a function"); } if (typeof _cache !== "undefined") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }

function isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _construct(Parent, args, Class) { if (isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }

function _isNativeFunction(fn) { return Function.toString.call(fn).indexOf("[native code]") !== -1; }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var JoeAutocomplete =
/*#__PURE__*/
function (_HTMLElement) {
  _inherits(JoeAutocomplete, _HTMLElement);

  function JoeAutocomplete() {
    _classCallCheck(this, JoeAutocomplete);

    return _possibleConstructorReturn(this, _getPrototypeOf(JoeAutocomplete).call(this));
  }

  _createClass(JoeAutocomplete, [{
    key: "connectedCallback",
    value: function connectedCallback() {
      var _this = this;

      var st = new Date().getTime();
      var self = this;
      this.classList.add('joe-autocomplete');
      this.schemaObj = _joe.schemas[this.getAttribute('schema')];
      this.field = this.getAttribute('field');
      this.itemId = this.getAttribute('itemId');
      this.idprop = this.getAttribute('idprop') || this.schemaObj.idprop;
      this.propObj = _joe.getField(this.field);
      this.joeIndex = this.getAttribute('joe-index');
      this.values = this.propObj.values;

      if (typeof this.values == "function") {
        this.values = this.values(_joe.current.object);
      }

      if ($.type(this.values) == 'string' && _joe.getDataset(this.values)) {
        this.values = _joe.getDataset(this.values);
      }

      this.lookup = {};
      this.autocompleteSpecs = this.propObj.autocomplete || {
        template: this.propObj.autocomplete_template || this.propObj.template || '<joe-title>${name}</joe-title><joe-subtext>${info}</joe-subtext>'
      };
      this.template = this.autocompleteSpecs.template;
      this.values.map(function (v) {
        var schema = v.itemtype && _joe.schemas[v.itemtype] || _this.schemaObj;
        var searchable = schema.searchable || _this.autocompleteSpecs.searchable || ['name', 'id', 'info'];
        var haystack = searchable.map(function (s) {
          return v[s];
        }).join(' ').replace(/,/, ' ').toLowerCase();
        _this.lookup[v[_this.idprop]] = haystack; //`${v.name} ${v.id} ${v.info} `.replace( /,/,' ').toLowerCase();
      }); // _joe.propAsFuncOrValue(this.propObj.values);

      var el = new Date().getTime() - st;
      this.innerHTML = "<joe-subtext>" + this.values.length + " options [" + el + " ms]</joe-subtext><autocomplete-options></autocomplete-options>";
      this.autocompleteOptions = this.querySelector('autocomplete-options');
    }
  }, {
    key: "search",
    value: function search(query) {
      var _this2 = this;

      //find elements in values that match
      var needles = query.toLowerCase().replace(/,/, ' ').split(' ');
      this.show();
      var opts = this.values.filter(function (v) {
        if (!needles.length) {
          return true;
        }

        for (var n = 0, tot = needles.length; n < tot; n++) {
          if (_this2.lookup[v[_this2.idprop]].indexOf(needles[n]) == -1) {
            //needle not in haystack
            return false;
          }
        }

        return true;
      });
      this.renderOptions(opts);
      this.classList.add('active');
    }
  }, {
    key: "renderOptions",
    value: function renderOptions(opts) {
      var autocomplete = this.autocompleteSpecs;
      var html = '';
      var ac_opt;
      var ac_id, ac_title;
      var max = Math.min(20, opts.length);

      for (var v = 0, len = max; v < len; v++) {
        //ac_opt = ($.type(this.values[v]) == "object")?
        // this.values[v]:
        //     {id:this.values[v],name:this.values[v]};
        // ac_title = fillTemplate(_joe.propAsFuncOrValue(autocomplete.template,ac_opt,null,_joe.current.object),ac_opt);
        // ac_id = (autocomplete.value && fillTemplate(_joe.propAsFuncOrValue(autocomplete.value,ac_opt),ac_opt))
        //     ||(autocomplete.idprop && ac_opt[autocomplete.idprop])
        //     ||ac_opt._id||ac_opt.id||ac_opt.name;
        ac_opt = opts[v];
        var schema = ac_opt.itemtype && _joe.schemas[ac_opt.itemtype] || this.schemaObj;
        ac_id = ac_opt[this.idprop];
        ac_title = this.lookup[ac_id];
        html += "<div class=\"joe-text-autocomplete-option\"\n          onclick=\"getJoe(" + this.joeIndex + ").autocompleteTextFieldOptionClick(this);\"\n          data-value=\"" + ac_id + "\">\n          " + (_joe.schemas[schema.name] && _joe.schemas[schema.name].menuicon) + "\n          " + fillTemplate(_joe.propAsFuncOrValue(this.template, ac_opt), ac_opt) + "\n          </div>";
      }

      this.autocompleteOptions.innerHTML = html;
    }
  }, {
    key: "attributeChangedCallback",
    value: function attributeChangedCallback(attr, oldValue, newValue) {}
  }, {
    key: "disconnectedCallback",
    value: function disconnectedCallback() {}
  }], [{
    key: "observedAttributes",
    get: function get() {
      return [];
    }
  }]);

  return JoeAutocomplete;
}(_wrapNativeSuper(HTMLElement)); // window.addEventListener('load', function(){


window.customElements.define("joe-autocomplete", JoeAutocomplete); // })