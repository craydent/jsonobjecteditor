"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _wrapNativeSuper(Class) { var _cache = typeof Map === "function" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null || !_isNativeFunction(Class)) return Class; if (typeof Class !== "function") { throw new TypeError("Super expression must either be null or a function"); } if (typeof _cache !== "undefined") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }

function isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _construct(Parent, args, Class) { if (isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }

function _isNativeFunction(fn) { return Function.toString.call(fn).indexOf("[native code]") !== -1; }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var JoeListItem =
/*#__PURE__*/
function (_HTMLElement) {
  _inherits(JoeListItem, _HTMLElement);

  function JoeListItem() {
    _classCallCheck(this, JoeListItem);

    return _possibleConstructorReturn(this, _getPrototypeOf(JoeListItem).call(this)); //   var styles = `
    //   swipe-option{
    //     position:absolute;
    //     top:0;
    //     width:100px;
    //     bottom:0;
    //     backgorund:#ccc;
    //     display:none;
    //   }
    //   .ui-draggable-dragging swipe-option{
    //     display:block;
    //   }
    // `;
    // document.getElementById(_joe.Components.styleTag).innerHTML+=styles;
  }

  _createClass(JoeListItem, [{
    key: "renderIcon",
    value: function renderIcon() {
      this.icon = this.getAttribute('icon');

      if (_joe.schemas[this.icon] && _joe.schemas[this.icon].menuicon) {
        this.content.innerHTML = _joe.schemas[this.icon].menuicon + this.content.innerHTML; //return _joe.schemas[this.icon].menuicon;
      }
    }
  }, {
    key: "connectedCallback",
    value: function connectedCallback() {
      var self = this;
      this.schemaObj = _joe.schemas[this.getAttribute('schema')];
      this.itemId = this.getAttribute('itemId');
      this.idprop = this.getAttribute('idprop');
      this.content = this.querySelector('.joe-field-item-content');
      this.initial_onclick = this.content.getAttribute('onclick');
      this.checkbox = this.querySelector('joe-checkbox');
      this.renderIcon();

      if (this.schemaObj && this.schemaObj.onswipe) {
        var rightOption = this.schemaObj.onswipe.left;
        var leftOption = this.schemaObj.onswipe.right;
        var l, r;

        if (leftOption) {
          l = document.createElement("swipe-option");
          l.className = "left-side";
          l.innerText = leftOption.name;
          l.style.backgroundColor = leftOption.color || '#ccc';
          this.appendChild(l);
        }

        if (rightOption) {
          r = document.createElement("swipe-option");
          r.className = "right-side";
          r.innerText = rightOption.name;
          r.style.backgroundColor = rightOption.color || '#ccc';
          this.appendChild(r);
        } // this.innerHTML+=`
        //     <swipe-option class="left"></swipe-option>
        //     <swipe-option class="left"></swipe-option>
        // `;


        $(this).draggable({
          axis: 'x',
          revert: true,
          scroll: false,
          start: function start(e, t) {
            self.content.removeAttribute('onclick'); //self.content.removeEventListener('click',this.initial_onclick)
          },
          drag: function drag(e, t) {
            var drag = t.position.left;
            var threshold = this.schemaObj.onswipe.threshold || .6;

            if (Math.abs(drag) > 20) {
              self.beingSwiped = true;
            }

            if (Math.abs(drag) > e.target.width() * threshold) {
              self.classList.add('swiping');
            } else {
              self.classList.remove('swiping');
            }

            console.log(drag); //self.content.removeEventListener('click',this.initial_onclick)
          },
          stop: function stop(e, t) {
            var drag = t.position.left;
            var dir = '';
            var threshold = this.schemaObj.onswipe.threshold || .6;

            if (Math.abs(drag) < 20 && !self.beingSwiped) {
              switch (_typeof(this.initial_onclick)) {
                case "string":
                  eval(this.initial_onclick);
                  break;

                case "function":
                  this.initial_onclick(e.target);
                  break;
              }
            } else if (Math.abs(drag) > e.target.width() * threshold) {
              dir = drag > 0 ? 'right' : 'left';
              this.schemaObj.onswipe[dir] && this.schemaObj.onswipe[dir].action && this.schemaObj.onswipe[dir].action(_joe.Indexes[self.idprop][self.itemId], self);
              logit(dir + ' ' + drag);
            }

            self.content.setAttribute('onclick', this.initial_onclick);
            self.beingSwiped = false;
            self.classList.remove('swiping'); //self.content.addEventListener('click',this.initial_onclick);
          }
        });
      }

      this.container = this.parentElement;
      this.render();
    }
  }, {
    key: "render",
    value: function render() {
      var atts = _joe.Components.getAttributes(this);
    }
  }, {
    key: "attributeChangedCallback",
    value: function attributeChangedCallback(attr, oldValue, newValue) {
      this.render();
    }
  }, {
    key: "disconnectedCallback",
    value: function disconnectedCallback() {}
  }], [{
    key: "observedAttributes",
    get: function get() {
      return [];
    }
  }]);

  return JoeListItem;
}(_wrapNativeSuper(HTMLElement)); // window.addEventListener('load', function(){


window.customElements.define("joe-list-item", JoeListItem); // })