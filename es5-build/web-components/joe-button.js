"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _wrapNativeSuper(Class) { var _cache = typeof Map === "function" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null || !_isNativeFunction(Class)) return Class; if (typeof Class !== "function") { throw new TypeError("Super expression must either be null or a function"); } if (typeof _cache !== "undefined") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }

function isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _construct(Parent, args, Class) { if (isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }

function _isNativeFunction(fn) { return Function.toString.call(fn).indexOf("[native code]") !== -1; }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var JoeButton =
/*#__PURE__*/
function (_HTMLElement) {
  _inherits(JoeButton, _HTMLElement);

  function JoeButton() {
    _classCallCheck(this, JoeButton);

    return _possibleConstructorReturn(this, _getPrototypeOf(JoeButton).call(this));
  }

  _createClass(JoeButton, [{
    key: "connectedCallback",
    value: function connectedCallback() {
      this.schema = this.getAttribute('schema');
      this.classList.add('joe-button');
      this.initialHTML = this.innerHTML.trim(); //var initialText = this.innerText.trim();
      // this.newText = this.innerText.trim() && `<jb-text>${this.innerText.trim()}</jb-text>` ||'';

      this.render();
    }
  }, {
    key: "render",
    value: function render() {
      var _this$classList;

      var atts = _joe.Components.getAttributes(this);

      var classesToAdd = [];
      var icon;
      var newHTML = '';

      if (atts.schema) {
        this.schema = atts.schema;
        icon = _joe.schemas[this.schema] && _joe.schemas[this.schema].menuicon;
        icon && classesToAdd.push("joe-svg-button");
      }

      atts.color && classesToAdd.push("joe-" + atts.color + "-button");
      atts.icon && classesToAdd.push('joe-iconed-button', "joe-" + atts.icon + "-button");
      var actionPreset = false;

      if (atts.action) {
        var actString = '';

        switch (atts.action) {
          case 'create':
          case 'new':
            var actKey = "create"; //this.classList.add(`joe-${atts.action}-button`)

            classesToAdd.push("joe-" + actKey + "-button");

            if (atts.schema) {
              actString = "_joe.Object." + actKey + "('" + atts.schema + "')";
              newHTML = "new <b>" + atts.schema.toUpperCase() + "</b>";
              classesToAdd.push('joe-iconed-button', "joe-plus-button");
            }

            actionPreset = true;
            break;

          case 'preview':
          case 'view':
            var prefix = atts.prefix || '';
            actString = "_joe.gotoFieldURL(this,'" + prefix + "');"; //actString="_joe.gotoFieldURL(this,\''+prefix+'\');"

            newHTML = "<jb-text>" + atts.action + " <b>" + atts.schema.toUpperCase() + "</b></jb-text>";
            classesToAdd.push('joe-iconed-button', "joe-view-button");
            actionPreset = true;
            break;

          default:
            actString = atts.action;
            break;
        }

        this.setAttribute('onclick', actString);
      }

      var guts = actionPreset ? newHTML || this.initialHTML : this.initialHTML || newHTML;
      this.innerHTML = (icon || '') + guts;
      /*this.innerText = '<button-text>'+this.innerText+'</button-text>';*/

      (_this$classList = this.classList).add.apply(_this$classList, classesToAdd);
    }
  }, {
    key: "attributeChangedCallback",
    value: function attributeChangedCallback(attr, oldValue, newValue) {
      this.render();
    }
  }, {
    key: "disconnectedCallback",
    value: function disconnectedCallback() {}
  }], [{
    key: "observedAttributes",
    get: function get() {
      return [];
    }
  }]);

  return JoeButton;
}(_wrapNativeSuper(HTMLElement)); // window.addEventListener('load', function(){


window.customElements.define("joe-button", JoeButton); // })