var site_status = {

};
var apps = function(){
    var systemStatsCard = function(cssclass,left){
        var cssclass = cssclass || '';
        var left = left || 0;
        return {type:'Card',
                    config:{/*DEPRECATED?*/
                        title:'<small>JOE Platform v'+JOE.VERSION+'</small>',
                        content:function(){
                            if(!_joe.Data || !_joe.Data.user){return 'loading data';}
                            var message = 
                            _joe.Data.user.length+' users <br/>'+
                            
                            $c.itemCount(_joe.Data)+' schemas <br/>'+
                            (_joe.search({}).length)+ ' objects';
                            if(_joe.User && _joe.User.role == 'super'){
                                message+='<br/><br/><joe-button onclick="_joe.SERVER.Cache.reload()" class="joe-button joe-iconed-button  joe-grey-button joe-red-button joe-reload-button"> reload cache </joe-button>';
                            }
                            return message;
                        },
                        cssclass:cssclass,
                        left:left
                    }
                };
    };
    var default_schemas = JOE.webconfig.default_schemas;
    return{
        joe:{
            title:'Platform',
            info:'This is the master Data Model Management System (DMMS) admin page. It has access to all of your schemas and datasets.',
            description:'JOE is the Json Object Editor. The Platform app has access to all your schemas and datasets.',
            plugins:['auth.js','formBuilder.js','reportbuilder.js','callbackTester.js','memberRegistry.js','awsConnect.js','notifier.js','calendar.js','inventory.js','money.js'],
            collections:((default_schemas.concat(['schema','group',
                'site','page','post','layout','block','include','event',
                'project','board','task',
                'transaction','budget','ledger',
                'location','device','notification',
                'visitor','question','form','submission','session','touchpoint','member']))).sort(),
                dashboard:[
                    JOE.Apps.Cards.appHome({cssclass:'w3 h4'}),
                    JOE.Apps.Cards.recentlyUpdated({left:3},
                    {limit:20}),
                    JOE.Apps.Cards.systemStats({top:0,left:6}),
                {type:'Card',
                    config:{
                        title:'Calendars',
                        top:0,left:8,
                        content:function(){
                            var html = 
                            ((_joe.Data && _joe.Data.event && _joe.Data.event.length)||0)+" events";
                            html+='<joe-button class="joe-button joe-grey-button" onclick="window.open(\'/API/plugin/calendar/events?events=all&mode=feed\')">view  feed</joe-button>';
                            if(_joe.User && ['super','admin'].contains(_joe.User.role)){
                               // if(_joe.Google && _joe.Google.Calendar && _joe.Google.Calendar.active){
                                html+='<hr style="margin:10px 0"/>google';
                                html+='<joe-button class="joe-button joe-grey-button" onclick="_joe.Google.Calendar.list()">List calendars</joe-button><hr/>';

                                html+='<joe-button class="joe-button joe-orangegrey-button" onclick="_joe.Google.Calendar.Event.publish()">PUBLISH events</joe-button>';
                                //}
                            }
                        return html;
                    },
                        cssclass:'w1 h2'
                    }
                },
                {type:'Card',
                    config:{
                        title:'JSC',
                        top:1,left:6,
                        content:function(){
                            var html = '<pre>'+JSON.stringify(__jsc,'','\n\r')+'</pre>';
                        return html;
                    },
                        cssclass:'w2 h2'
                    }
                },
                //systemStatsCard(null,3)
            ]
        }
    }
};

module.exports = apps();