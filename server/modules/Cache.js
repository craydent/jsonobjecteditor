function Cache(updateNow) {
    var modulename = JOE.Utils.color('[cache]','module');
    var self = this;
    this.settings = {};
    this.lookup ={};

    this.update =  function(callback,collections){//updates the app cache of information that should be quickly accessible.
        var uBM = new Benchmarker();
        var collections = collections || JOE.Apps.collections || JOE.Apps.get('joe').collections;// || ['trend', 'company', 'eki', 'template', 'doc','capability'];
        var callback = callback || function(){logit('cache updated')};
        var coll;
        var collected = 0;
        self.settings = self.settings || {};
        self.list = self.list || [];
        self.queries = [];
        function loadCollection(collection,callb){
            //logit('[caching] '+collection);
//            JOE.Mongo.get(collection,null,function(err,results){
            JOE.Storage.load(collection,null,function(err,results){
                collected++;
                JOE.Data[collection] = results;
                
                if(collection == "setting" && JOE.Data[collection]){
                    JOE.Data[collection].map(function(setting){
                        self.settings[setting.name] = setting.value;
                    })
                }
                let coll = (JOE.Data[collection]||[]);
                //coll.map(function(item){
                for(var i =0;i<coll.length;i++){    
                    let item = coll[i];
                    self.lookup[item._id] = item;
                }
                //})
                //console.log(Object.keys(self.lookup).length+' keys in lookup');
                //TODO: make list update modular
                try{
                   // let prevList = self.list.filter(function(li){return (li.itemtype != collection);});
                    self.list = self.list.filter(function(li){return (li.itemtype != collection);}).concat(JOE.Data[collection]);
                    //Object.keys(JOE.Data).map(k=>{

                    //})
                }catch(e){
                    console.log(JOE.Utils.color('[error] ','red')+e);
                }
                if(collected == collections.length){
                 logit(`${modulename} ${collected} of ${collections.length} schemas collected, cached ${self.list.length} items in ${uBM.stop()} secs`);   

                    
                    callb(JOE.Data);
                }
            })
        }
        var done = [];
        for (var c = 0, tot = collections.length; c < tot; c++) {
            coll = collections[c];
            if(done.indexOf(coll) == -1){
                loadCollection(coll,callback);
                done.push(coll);
            }
        }




    };
    this.search = function(query){
        //gets a search query and finds within all items
        //TODO: cache queries
        
       // var results = self.list.where(query);
       // logit(query,results);
        return self.list.where(query);
    }
    this.findByID = function(collection,id,specs,returnProp){
        try{
            if($c.isCuid(collection) && !id){
                id = collection;
            }
            if(self.lookup[id]){
                return self.lookup[id];            
            }
            var specs = specs || {};
            var idprop = specs.idprop||'_id';
            var ids = (id||'').split(',');
            //return JOE.Data[collection].where(query)||[];
            var results = (JOE.Data[collection]||[]).filter(function(item){
                return ids.indexOf(item[idprop]+'') != -1;
            });
            if(id.indexOf(',') == -1){
                if(returnProp){
                    return (results[0]||{})[returnProp];
                }
                return results[0]||false;
            }
            return results;
        }catch(e){
            console.log(JOE.Utils.color('[cache]','error')+' error: in findByID('+(id||'')+') '+e);
        }
    };
    if(updateNow && typeof updateNow == "function"){
        this.update(updateNow);
    }
    return this;
};

module.exports = new Cache();
