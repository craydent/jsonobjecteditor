var fs = require('fs');
var utils = function(){
    var self = this;
        this.propAsFuncOrValue = function(prop, toPass, defaultTo){
        try {
            var toPass = toPass || {};

            if (prop && typeof prop == 'function') {
                return prop(toPass);
            }
            return prop;
        }catch(e){
            logit('JOE: error parsing propAsFunction: '+e);
            
            return '';
        }
    };
        this.Settings = function(settingName,specs){
            var specs = specs ||{};
            var s = (JOE.Data.setting||[]).where({name:settingName})[0];
            var setting = (s||{});
            if(specs.object){return setting;}
            if(setting.setting_type =="code" && !specs.raw){
                if(setting.value.trim().indexOf('function') == 0 && specs.params){
                    var p = tryEval(s.value);
                    if(!p){
                        return specs.params;
                    }
                     return p(specs.params)
                }
                return tryEval(s.value)
                
            }
            return setting.value;
        }
        this.setupFileFolder = function(dir,name,watchHandler){
            if (!fs.existsSync(dir)){
                fs.mkdirSync(dir);
            }
            var watchHandler = watchHandler ||
            function(event,filename){
                console.log(name+' dir updated >'+filename||'no filename')
            }
            fs.watch(dir,watchHandler);
            console.log(JOE.Utils.color('['+name+']','module')+' stored in: '+dir);
        }
        this.stringFunctions = function(propObject){
            for(var p in propObject){
                if(typeof propObject[p] == 'function'){
                    //propObject[p] = '(' + propObject[p].toString() + ')';
                    propObject[p] = '(' + propObject[p].toString().replace(/\\n/g,'\\n') + ')';

                }else if(typeof propObject[p] == "object"){
                    JOE.Utils.stringFunctions(propObject[p]);
                }
            }
            return propObject;
        }
        this.color =function(message,styles,noclear){
            
            try{
                var styles = styles.split();
                var chart ={
                    bold:'\x1b[1m',
                    underline:'\x1b[4m',
                    error:'\x1b[31m',
                    reset:'\x1b[0m',
                    blue:'\x1b[34m',
                    system:'\x1b[34m',
                    plugin:'\x1b[32m',
                    green:'\x1b[32m',
                    schema:'\x1b[32m',
                    yellow:'\x1b[33m',
                    module:"\x1b[33m",
                    white:"\x1b[37m",
                    black:'\x1b[30m',
                    gray:'\x1b[90m'
                };
                var messagestring = '';
                styles.map(function(i){
                    if(chart[i]){
                        messagestring+=chart[i];
                    }else{
                        messagestring+=i;
                    }
                })
                
                messagestring+= message;
                if(!noclear){messagestring += chart.reset;}
                return messagestring;
                
            }catch(e){
                console.log(e);
                return message;
            }
        }
        this.propAsFuncOrValue = function(prop, toPass, defaultTo,secondToPass){
        /*|{
            featured:true,
            tags:'helper',
            description:'Parses the property passed as a function or value, can be passed an object to be a parameter of the function call.'
         }|*/
            try {
                var toPass = toPass || null;

                /*        if(!toPass.hasOwnProperty(prop)){
                return defaultTo || false;
                }*/
                if (prop && typeof prop == 'function') {
                    return prop(toPass,secondToPass);
                }
                return prop;
            }catch(e){
                //logit('JOE: error parsing propAsFunction: '+e);
                //self.Error.add('JOE: error parsing propAsFunction: '+e,e,{caller:'self.propAsFuncOrValue'})
                console.log('[JOE error] parsing propAsFunction: '+e);
                if($c.DEBUG_MODE){
                    return ('[JOE error] parsing propAsFunction: '+e);
                }
                return '';
            }
        };

        this.requireFromString = function(src,filename){
            try{
            var Module = module.constructor;
            var m = new Module();
            m._compile(src, filename+'.js');
            }catch(e){
                return {errors:e};
            }
            return m.exports;
        }

        this.ProjectedItemsFromFields = function(fields,data,makeSet){
            fields = fields.split(',');
            var finalItems = [];
            data.map(item=>{
                let copy = {};
                fields.map(f=>{
                    if(f.indexOf('.')!= -1){
                        let fstrings = f.split('.');
                        try{
                            var currentDepthValue =item;
                            for (var i = 0, n = fstrings.length; i < n; ++i) {
                                let fVal = fstrings[i];
                                if(currentDepthValue.hasOwnProperty(fVal)){
                                    currentDepthValue = currentDepthValue[fVal];
                                    if(i == n-1){
                                        copy[f] = currentDepthValue;
                                    }
                                }
                            }
                        }catch(e){
                            copy[f]=e;
                        }
                    }else{
                        if(f){
                            copy[f]=item[f];
                        }
                    }
                })
                finalItems.push(copy);
            });
            if(makeSet){
                var items = [];
                let template = (typeof makeSet == "string" && makeSet)||fields.map(f=>{return '${'+f+'}'}).join('_');
                finalItems.map(fi=>{
                    //get template or use fields with _
                    items.push($c.fillTemplate(template,fi).trim().toLowerCase())
                })
                items = Array.from(new Set(items));
                return items;
            }
            return finalItems;
        }

    return this;
};

module.exports = utils(); 