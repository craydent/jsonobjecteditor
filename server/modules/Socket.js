if(JOE.webconfig.socketPort == JOE.webconfig.port){
    if(JOE.httpsServer){
        var io = JOE.io = require('socket.io')(JOE.httpsServer);
    }else{
        var io = JOE.io = require('socket.io')(JOE.httpServer);
    }

}else{
    var io = JOE.io = require('socket.io')(JOE.webconfig.socketPort);
}

//var io = JOE.io = require('socket.io')(JOE.httpServer);//(JOE.webconfig.socketPort);


var modulename = JOE.Utils.color('[socket]','module');
var moduleerror = JOE.Utils.color('[socket]','error');
console.log(modulename+' listening on port '+JOE.webconfig.socketPort);
var socket_list = [];
var connected_users = {};
io.exit = function(){
    //console.log('io server',io.server);   
        //console.log('socket list',connected_users); 
        io.close && io.close(); 
        for (var s in io.sockets.connected){
            console.log('socket',s);   
            io.sockets.connected[s].disconnect();
        }
}
io.on('connection', function(socket){
    socket.on('disconnect',function(){
        updateSocketRgistry(true);
        });
    socket.on('save',function(data,collection,id){
        try{
        var item = data.item;
        var collection = collection || item.itemtype;
        var id = id || item._id;
        JOE.Storage.save(item,collection,function(err,data){
        //JOE.Mongo.save(item,collection,function(err,data){
            if(!err) {
                delete data.callbacks;
                var results = [data];
                socket.emit('save_successful', {results: results});
                io.emit('item_updated', {results: results});
                //JOE.Cache.update();
            }else{
                console.log(err);
            }
        },{user:socket.user})
        }catch(e){
            socket.emit('error',{error:e});
        }
    });
    socket.on('save_comment',function(payload){
        //console.log('saving comment:'+payload.comment);
            JOE.Comments.add(payload,null,socket);
    });
    socket.on('signin',function(data) {
        socket.user = data.user||{};
        updateSocketRgistry();
    });
    socket.on('signout',function(data) {
        socket.user = {};
        updateSocketRgistry();
    });
    socket.on('global_message',function(message,specs){
        socket.broadcast.emit('message',message);

    });
    socket.on('connect_offer',function(data){

        var target = data.target;
        var target_user = connected_users[target];
        if(!target_user){
            socket.emit('message','that user is not available');
            return;
        }
        //logit(sdp);
        data.socketid = socket.id;
        for(var s = 0; s < target_user.sockets.length;s++){
            //console.log(socket.id);
            socket.broadcast.to(target_user.sockets[s].id).emit('connect_offer',data);
        }
        //console.log('headers',socket.client.request.headers);
        
    });
    socket.on('connect_answer',function(data){
        var sdp = data.sdp;
        var target = data.target;
        var user = data.user;
        var target_user = connected_users[target];
        if(!target_user){
            socket.emit('message','that user is not available');
            return;
        }

        socket.broadcast.to(data.socketid).emit('connect_answer',data);
        // for(var s = 0; s < target_user.sockets.length;s++){
        //     socket.broadcast.to(target_user.sockets[s].id).emit('connect_answer',data);
        // }
        //console.log('headers',socket.client.request.headers);
        //logit(sdp);
    });
    socket.on('get users',function(){
        try{
        socket.emit('log_data',connected_users,'connected users');
    }catch(e){
        console.log(moduleerror+' '+e);
            socket.emit('message',e)
        }
    })
    function updateSocketRgistry(disconnect){
        socket_list = [];
        connected_users = {};
        var userid,sock,headers;
        for (var s in io.sockets.connected){
            sock = io.sockets.connected[s];
            if(sock.user) {
                userid = sock.user._id;
                socket_list.push(io.sockets.sockets[s]);
                if (!connected_users[userid]) {
                    connected_users[userid] = {user: sock.user, sockets: []}
                }

                headers = sock.client.request.headers;
                connected_users[userid].sockets.push({
                    id:s,
                    url:headers.referer,
                    app:(headers.referer||'').replace(headers.origin+'/JOE/','')||'joe'
                });
            }
        }
        //go through sockets and add users to array
        //console.log(socket_list.length);
        var cu_payload = {connected_users:connected_users};
        socket.broadcast.emit('user_info',cu_payload);
        if(!disconnect) {
            socket.emit('user_info', cu_payload);
            //console.log('user ' + socket.user.name + ' connected, '
            //    + $c.itemCount(connected_users) + ' users ' + +$c.itemCount(io.sockets.connected) + ' sockets'
            //);
        }
    }
});//end io on

module.exports = io;

/*
 // sending to sender-client only
 socket.emit('message', "this is a test");

 // sending to all clients, include sender
 io.emit('message', "this is a test");

 // sending to all clients except sender
 socket.broadcast.emit('message', "this is a test");

 // sending to all clients in 'game' room(channel) except sender
 socket.broadcast.to('game').emit('message', 'nice game');

 // sending to all clients in 'game' room(channel), include sender
 io.in('game').emit('message', 'cool game');

 // sending to sender client, only if they are in 'game' room(channel)
 socket.to('game').emit('message', 'enjoy the game');

 // sending to all clients in namespace 'myNamespace', include sender
 io.of('myNamespace').emit('message', 'gg');

 // sending to individual socketid
 socket.broadcast.to(socketid).emit('message', 'for your eyes only');
 */