var restify = require('restify');
var server = restify.createServer({
    name: 'eTechApp',
    version: '1.0.0'
});


//cors//
server.use(restify.authorizationParser());
server.use(restify.CORS());

server.use(function (req, res, next) {
    var users;
    next();
    // if (/* some condition determining whether the resource requires authentication */) {
    //    return next();
    // }

    users = {
        foo: {
            id: 1,
            password: 'bar'
        }
    };

    // Ensure that user is not anonymous; and
    // That user exists; and
    // That user password matches the record in the database.
    if (req.username == 'anonymous' || !users[req.username] || req.authorization.basic.password !== users[req.username].password) {
        // Respond with { code: 'NotAuthorized', message: '' }
        next(new restify.NotAuthorizedError());
    } else {
        next();
    }

    next();
});
//server.use(restify.fullResponse());

server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());
server.use(restify.jsonp());



module.exports = server;