var fs = require('fs');
var serverPath = '../';
function Schemas(){
    var self = this;
    this.schema = {};
    this.schemaList =[];
    this.raw_schemas = {};
    this.updateDelay = 1200;
    //this.core = {};
    var internalSchemasDir = __dirname+'/../schemas';
    //console.log('internal schema dir = '+internalSchemasDir);
    JOE.schemaDir = schemaDir = JOE.appDir+'/'+JOE.webconfig.schemaDir+'/';

    this.load = function(s){

        //console.log('[schema] loading '+s);
        var schema,raw_schema;
        if(s.indexOf('.js') == -1){s+= '.js';}
        try{
            var schemapath = JOE.schemaDir + s;
            fs.accessSync(schemapath, fs.F_OK);
                
        }catch(e){
            var schemapath = serverPath+'schemas/' + s;
        }
        try{
            try{
                delete require.cache[require.resolve(schemapath)];
            }catch(e){
                console.log('[schema][error] '+s+' not deleted');
            }
            schema = require(schemapath);
            
        }catch(e){
            schema = {error:'schema '+s+' not found'};
            console.log(e,schema);
        }

        /*raw_schema = self.raw_schemas[s.replace('.js','')] = $c.merge({},schema);
        if(raw_schema.events){
            raw_schema.events = $c.merge({},raw_schema.events);
        }*/
        raw_schema = self.raw_schemas[s.replace('.js','')] = schema.duplicate(true);
        JOE.Utils.stringFunctions(schema);
        let schemaname = s.replace('.js','')
        self.schema[schemaname] = schema;
        self.schemaList.push(schemaname);
        return schema;
    }
    this.updateTimeout;
    this.update = function(doItNow){
        if(doItNow){
            var lBM = new Benchmarker();
            var schema_to_load = [];
            //go through all the server files, then the schemadir files\
            schema_to_load = schema_to_load.concat(fs.readdirSync(internalSchemasDir));
            //schema_to_load = schema_to_load.concat(fs.readdirSync('server/schemas'));
            schema_to_load = schema_to_load.concat(fs.readdirSync(JOE.schemaDir));
            schema_to_load.map(function(sc){
                self.load(sc);
            })
            logit(JOE.Utils.color('[schema] ','module')+schema_to_load.length+' updated in '+lBM.stop()+' secs'); 
        }else{
            clearTimeout(self.updateTimeout);
            self.updateTimeout = setTimeout(self.update,self.updateDelay,true);
            //logit(JOE.Utils.color('[schema] ','module')+'updating in '+ this.updateDelay/1000); 

        }
    }
    JOE.Utils.setupFileFolder(schemaDir,'schemas',self.update);
    fs.watch(internalSchemasDir,function(){
            self.update();
    });

    this.events = function(item,events,specs){
        try{
            var specs = specs || {};
            var schemaprop = specs.schemaprop || "itemtype";
            var schemaname = item[schemaprop];
            var schema_def = self.raw_schemas[schemaname];
            
            if(schema_def){
                
                if(typeof events == "string"){
                    events = events.split(',');
                }
                events.map(function(event){
                    if(schema_def.events && schema_def.events[event]){
                        if(typeof schema_def.events[event] == "function"){
                            try{
                               
                                schema_def.events[event](item,specs);
                                logit('[event] '+schemaname+' > '+event);
                            }catch(e){
                                console.log(JOE.Utils.color('[schema]event error: ','error')+event+':'+e);
                            }
                        }else{
                            logit('no event'+event+' for '+schemaname );
                        }
                    }
                })
            }else{
                logit('schema "'+schemaname+'" not found');
            }
        }catch(e){
            console.log(JOE.Utils.color('[schema] hook error: ','error')+events+':'+e);
        }
    }
    this.update();
    return self;
}
module.exports = new Schemas();