var mysql = require('mysql');
function MYSQL(db){
    var modulename = JOE.Utils.color('[mysql]','module');
    var mBM = new Benchmarker();
    var self = this;
    var sql_info = db || JOE.webconfig.mysql;
    var connection
    if(sql_info){
        connection = mysql.createConnection(sql_info);
        connection.connect(function(err) {
        if (err) {
            console.log(modulename+'error connecting: ' + err.stack);
            return;
        }
        
            console.log(modulename+'connected as id ' + connection.threadId+' in '+mBM.stop()+' secs');
        });
    }
    
 


    this.get =function(collection,specs/*query,callback*/){
        var specs = specs || {};
        var schema = JOE.Schemas.schema[collection];
        var con;
        if(specs.storage && specs.storage.connection){
            con = mysql.createConnection(specs.storage.connection);
        }else{
            con = connection;
        }

        var callback = specs.callback || function (error, results, fields) {
            if (error) {
                console.log(error);
            }
            try{
                console.log(modulename+': '+results.length)
            }catch(e){
                console.log(e);
            }
        };
        var query = specs.query || {};
            //logit(specs);
        //connection.connect();
        console.log('mysql query',query);
        con.query(query, callback);
        
        if(specs.storage && specs.storage.connection){
            con.end();
        }
    };
    this.save = function(data,collection,callback){


        if(data._id && !data._id.isCuid()){
            //console.log(data._id);
            data._id = mongojs.ObjectId(data._id);
        }
        database = mongojs(db);
        database.collection(collection).save(data,function(err,results){
            if(err){console.log('save error: '+err)}
            JOE.Cache.update();
            (callback||logit)(err,results);
        });
    };

    this.delete =function(data,collection,callback){
      data.deleted = new Date().toISOString();
        JOE.Mongo.save(data,collection,callback);
    };
    this.Connection=connection;

    return this;

};

module.exports = new MYSQL();