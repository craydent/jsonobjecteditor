var Render = {
    page:function(req,res){
        var pageid = req.params.itemid||req.query.pageid;
        if(!pageid){
            res.send('no page selected');
        }
        var page =JOE.Cache.findByID('page',pageid);
        var layout =JOE.Cache.findByID('layout',page.layout);
        var payload = {
            PAGE:page
        };
        var final = fillTemplate(layout.template,payload);
        res.send(final);
    },
    html:function(res,html){
        //res.writeHead(200, {
        //    'Content-Length': Buffer.byteLength(html),
        //    'Content-Type': 'text/html'
        //});
        res.end(html);
    },
    single:function(res,templateID,contentID,specs){
        var specs = specs || {};
        var app = 'etechmax';
        var templateQuery = {_id:templateID};
        var payload = {
            content:{
                SETTING:JOE.Cache.settings
            }
        };
        if(templateID.length != 24){
            templateQuery = {template_code:templateID}
        }
        //var user = basicAuth(req);
        JOE.Mongo.get(app,'template',templateQuery,function(err,template_item){
            var template  = template_item[0]||{template:'no template specified'};
            var contentType = template.dataset || 'company';
            payload.template = template.template || 'no template specified';

            payload.content.TEMPLATE = template;
            JOE.Mongo.get(app,contentType,{_id:contentID},function(err,content_item){
                if(!content_item[0]){
                    return res.send({name:'no content specified'});
                }
                payload.content[contentType.toUpperCase()] = content_item[0]||{name:'no content specified'};
                payload.content.ready = true;
                payload.content.NAME = payload.content[contentType.toUpperCase()].name||'';
                payload.content.CONTENT = payload.content[contentType.toUpperCase()];
                //payload.content.Document =
                if(payload.template && payload.content.ready) {
                    var responseHTML = fillTemplate(payload.template,payload.content);
                    if(responseHTML){
                        Render.html(res, responseHTML);
                    }else{
                        res.send({template:payload.template,content:payload.content});
                    }
                }
            });
        });
    },
    SingleDocument:function(req, res, next) {
        var templateID = req.params.template;
        var contentID = req.params.content;//[req.params.company||''].split();
        //TODO:allow for multiples

        //var template_item =
        var response;
        if(!(templateID && contentID)){
            response ='<html><body>/docs/:content/:template, please specify all'
                +'<pre>'+JSON.stringify(req.params)+'</pre>'
                +'</body></html>';
            Render.html(res,response);
        }
        Render.single(res,templateID,contentID,{});
        //return next();
    }
};

module.exports = Render;