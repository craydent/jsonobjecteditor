function UniversalShorthand(){
    var self = this;
    this._usages={
        get:0,
        schmema:0,
        search:0
    };
        this.get = function(itemID,callback){
            self._usages.get++;
            var item = JOE.Cache.lookup[itemID];
                
            if(item){
                callback && callback(item);
                return item;
            }
        };

        this.search = function(query,callback){
            self._usages.search++;
            var items = JOE.Cache.search(query);
            if(items){
                callback && callback(items);
                return items;
            }
             
        }
        this.schema = function(schemaname,callback){
            self._usages.schema++;
            var schema = JOE.Schemas.raw_schemas[schemaname];
            if(schema){
                callback && callback(schema);
                return schema;
            }
        } 

    return this;

}
global.$J = new UniversalShorthand();
module.exports = global.$J;