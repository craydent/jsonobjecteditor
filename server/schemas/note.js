var schema = {
    title : '${name}',
    default_schema:true,
    menuicon:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"><title>note</title><path d="M42.35,28.1V26h5.86V28.1a2,2,0,0,0,2,2h.5a2,2,0,0,0,2-2V26h5.86V28.1a2,2,0,0,0,2,2h.5a2,2,0,0,0,2-2V26h1.19l3.81,3.74v6h4V28.09L65.91,22H63.08V20a2,2,0,0,0-2-2h-.5a2,2,0,0,0-2,2v2H52.71V20a2,2,0,0,0-2-2h-.5a2,2,0,0,0-2,2v2H42.35V20a2,2,0,0,0-2-2h-.5a2,2,0,0,0-2,2v2H32V20a2,2,0,0,0-2-2h-.51a2,2,0,0,0-2,2v2H18V82H35.7l1-4H22V26h5.47V28.1a2,2,0,0,0,2,2H30a2,2,0,0,0,2-2V26h5.85V28.1a2,2,0,0,0,2,2h.5A2,2,0,0,0,42.35,28.1Z"/><path d="M80.81,48.41,74.4,42a4,4,0,0,0-5.61-.06h0L45.41,65.29l-.47.46-4,16.13,16.14-4,1.51-1.53h0L80.87,54h0A4,4,0,0,0,80.81,48.41Zm-34.22,19,8.83,8.81-8.29,2.07-2.59-2.72ZM57,70.73l-5-5L68.79,49l5,5Z"/></svg>',
    listView:{
        title: function(note){return '<joe-subtext>'+$c.toDateTime(new Date(note.joeUpdated),{format:'n/d/y'})+'</joe-subtext><joe-title>${name}</joe-title><joe-subtitle>${info}</joe-subtitle>'},
        listWindowTitle: 'Notes'
    },
    onDemandExpander:true,
    itemExpander:function(note){
        var h = 
        
        '<joe-card><joe-title>content<joe-subtext class="fright">created <b>'+$c.toDateTime(new Date(note.created),{format:'n/d/y'})+'</b></joe-subtext></joe-title>'
        
        +note.content
        +'</joe-card>';
        return h;
    },
    itemMenu:function(){
        return [{name:'view',action:'window.open(\'/API/plugin/reportbuilder/standard?itemid=${_id}\')'}];
    },
    subsets:function(){
        var filters = [];
        var tags = _joe.Data.tag.filter(function(tag){
            if(tag.datasets.indexOf(_joe.current.schema.__schemaname) != -1){
                filters.push({name:tag.name,
                    filter:{tags:{$in:[tag._id]}}
                });
            }
        });
        filters.push({name:'untagged',
            filter:{$or:[{tags:{$size:0}},{tags:{$exists:false}}]}
        })
        return filters;
    },
    fields:function(){
        return [
        'name',
        'info',
        {name:'content_type',type:'select',values:['wysiwyg','code',{name:'objectList',display:'bullets'}], rerender:'content'},
        {extend:'content',specs:{
            type:function(item){
               /* switch(item.content_type){
                    case 'code':
                        return 'code';
                    break;

                    case 'objectList':
                        return 'objectList';
                    break;

                    case 'wysiwyg':
                    default:
                        return 'wysiwyg';
                    break;
                }*/
                return item.content_type || 'wysiwyg';
            },
            template:function(sub,item){
                return '<${format}>${text}</${format}>';
            },
            properties:['text',{name:'format',type:'select',values:['div','b','i','h2'],width:'100px'}],
            height:null
        }},
        {section_start:'related',collapsed:function(item){
            return !((item.files && item.files.length) && (item.references && item.references.length));
        }},
        'references',
        {name:'files',type:'uploader',allowmultiple:true, height:'300px',comment:'drag files here to upload', onConfirm:_joe.SERVER.Plugins.awsFileUpload},
        
        {section_end:'related'},
        {sidebar_start:'right'},
        'updated',
        'reports',
        {section_start:'details'},
                
                'tags',
        {section_end:'details'},   
        {section_start:'access'},
        '_protected',
        {section_end:'access'},
        {sidebar_end:'right'},    
        {section_start:'system',collapsed:true},
        '_id','created','itemtype',
        {section_end:'system'},
    ];
    },
    idprop : "_id",
    sorter:['!joeUpdated','name','created'],
    report:{
        name:'Note',
        info:'basic note content view',
        template:
            '<report-section>\
                <report-section-label>Content</report-section-label>\
                ${this.NOTE.content}\
            </report-section>'
    }
};

module.exports = schema;