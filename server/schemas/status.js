var schema = {
    title : '${name}',
    info:"Choose status options to track the progress of different panels throughout your dashboard in a workflow (ex: new member flow). Build a report to easily check the status of a workflow.",
    default_schema:true,
    listView:{
        title: 
        '<joe-full-right><joe-subtext>${this.index}</joe-subtext></joe-full-right>'+
        '<joe-title>${name}</joe-title><joe-subtitle>${info}</joe-subtitle>'+
        '<joe-subtext>${ENUM[${datasets}]}</joe-subtext>',
        listWindowTitle: 'Statuses'
    },
    menuicon:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-40 -40 180 180"><path d="M95 65H15V35H95ZM85 45H25V55H85Z"/><rect x="15" y="75" width="60" height="10"/><rect x="15" y="15" width="60" height="10"/><rect x="5.7" width="2" height="100"/><rect x="7" y="49" width="6" height="2"/></svg>',
    bgColor:function(item){
              return item.color;
    },
    stripeColor:function(item){
  
        if(item.default){
            //return 'limegreen';
            return {color:'limegreen',name:'default workflow start'};
        }
        if(item.active){
            return {color:'#90c0c0',name:'active status'};
        }
        if(item.inactive){
            //return 'limegreen';
            return {color:'#bbb',name:'inactive status'};
        }
        if(item.terminal){
            //return 'crimson';
            return {color:'crimson',name:'possible workflow end'};
        }

        return false;

    },
    subsets:function(){
        var schemas = [];
        var subs = [];
        _joe.current.list.map(function(status){
            schemas = schemas.concat(status.datasets||[]);
        });
        (new Set(schemas)).map(function(schema){
            subs.push({name:schema,filter:{datasets:{$in:[schema]}}})
        });
        return subs;
    },
    sorter:['index'],
    fields:[
        'name',
        'info',
        'description',
        'color:color',
        'index:number',
        {section_start:'categorization',collapsed:true},
        {label:'workflow'},
        {name:'default',type:'boolean',display:'default',label:'default status for new items', width:'50%'},
        {name:'terminal',type:'boolean',display:'end state', label:'this is a workflow end state', width:'50%'},
        {name:'active',type:'boolean',display:'active',label:'being edited right now', width:'50%'},
        {name:'inactive',type:'boolean',display:'inactive',label:'objects in this status are not to being actively edited', width:'50%'},

        'datasets',
        {section_end:'categorization'},
        {sidebar_start:'right'},
            {section_start:'usage',collapsed:false},
            {name:'usages',label:false,type:'content',
            run:function(item){
                var usages = _joe.search({status:item._id})
                var html = '<joe-title>'+usages.length+' objects are in this status</joe-title>';
                usages.sortBy('itemtype,name').map(function(usage){
                    html += _joe.renderFieldListItem(usage,function(item){
                    var html = 
                    '<joe-full-right>'
                        +'<joe-subtext>updated</joe-subtext>'
                        +'<joe-subtitle>'+_joe.Utils.toDateString(item.joeUpdated)+'</joe-subtitle>'
                        +'</joe-full-right>'
                    +(_joe.schemas[usage.itemtype].menuicon && '<joe-icon class="icon-50 icon-grey fleft">'
                    +_joe.schemas[usage.itemtype].menuicon+'</joe-icon>' || '')
                    +'<joe-subtext>${itemtype}</joe-subtext>'
                    +'<joe-subtitle>${name}</joe-subtitle><joe-subtext>${info}</joe-subtext>';

                    return html;
                },usage.itemtype);
                });
                return html;
            }},
            {section_end:'usage'},
        {sidebar_end:'right'},
        {section_start:'system',collapsed:true},
        '_id','created','itemtype',
        {section_end:'system'}
    ],
    idprop : "_id"
};

module.exports = schema;