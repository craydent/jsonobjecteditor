var schema = {
    title : '${name}',
    info:"A workflow is automated to track the status of a process you've created (ex: new member flow).",
    default_schema:true,
    listView:{
        title: 
        '<joe-full-right><joe-subtext>${this.index}</joe-subtext></joe-full-right>'+
        '<joe-title>${name}</joe-title><joe-subtitle>${info}</joe-subtitle>'+
        '<joe-subtext>${ENUM[${datasets}]}</joe-subtext>',
        listWindowTitle: 'Workflows'
    },
    menuicon:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-6 -6 44 44"><path d="M12 4L12 5 12 10 12 11 13 11 15 11 15 15 6 15 5 15 5 16 5 21 3 21 2 21 2 22 2 27 2 28 3 28 9 28 10 28 10 27 10 22 10 21 9 21 7 21 7 17 15 17 15 21 13 21 12 21 12 22 12 27 12 28 13 28 19 28 20 28 20 27 20 22 20 21 19 21 17 21 17 17 25 17 25 21 23 21 22 21 22 22 22 27 22 28 23 28 29 28 30 28 30 27 30 22 30 21 29 21 27 21 27 16 27 15 26 15 17 15 17 11 19 11 20 11 20 10 20 5 20 4 19 4 13 4 12 4zM14 6L18 6 18 9 14 9 14 6zM4 23L8 23 8 26 4 26 4 23zM14 23L18 23 18 26 14 26 14 23zM24 23L28 23 28 26 24 26 24 23z"/></svg>',
    itemExpander:function(workflow){
        var h ="";
        workflow.statuses.map(s=>{
            let statusObj = _joe.Indexes._id[s];
            h+= _joe.renderFieldListItem(statusObj,null,'status');
        })
        return h;
    },
    bgColor:function(item){
        return item.color;
    },
    hideNumbers:true,
    subsets:function(){
        var schemas = [];
        var subs = [];
        _joe.current.list.map(function(status){
            schemas = schemas.concat(status.datasets||[]);
        });

        (new Set(schemas)).map(function(schema){
            subs.push({name:schema,filter:{datasets:{$in:[schema]}}})
        });
        return subs;
    },
    methods:{
        getStatusesByName:function(workflowName){
            
        }
    },
    sorter:['index'],
    fields:[
        'name',
        'info',
        'description',
        'workflow_id',
        {name:'statuses',type:'objectReference',values:'status',sortBy:'index,name',schema:'status',
        reference_template:'<joe-title>${name}</joe-title><joe-subtext>${info}</joe-subtext>',
        bgcolor:function(status,item){
            return status.color;
        }
        },
        {sidebar_start:'right'},
        'reports',
            {section_start:'datasets'},
                'datasets',
            {section_end:'datasets'},
        {sidebar_end:'right'},
        '_id','created','itemtype'
    ],
    idprop : "_id",
    reports:[{
        name:'Workflow Standard',
        id:'standard',
        template:''
    }]
};

module.exports = schema;