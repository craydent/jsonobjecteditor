var board = function(){return{
    title : 'Board | ${name}',
    menuinfo:"Check a board to visualize the status of a project, task, visitor, or other object as they move through a workflow.",
    menuicon:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-13 -13 52 52"><path d="M1 0C0.4 0 0 0.4 0 1L0 25C0 25.6 0.4 26 1 26L25 26C25.6 26 26 25.6 26 25L26 1C26 0.4 25.6 0 25 0L1 0zM2 2L6 2 6 6 2 6 2 2zM8 2L12 2 12 6 8 6 8 2zM14 2L18 2 18 6 14 6 14 2zM20 2L24 2 24 6 20 6 20 2zM2 8L6 8 6 12 2 12 2 8zM8 8L12 8 12 12 8 12 8 8zM14 8L18 8 18 12 14 12 14 8zM20 8L24 8 24 12 20 12 20 8zM2 14L6 14 6 18 2 18 2 14zM8 14L12 14 12 18 8 18 8 14zM14 14L18 14 18 18 14 18 14 14zM20 14L24 14 24 18 20 18 20 14zM2 20L6 20 6 24 2 24 2 20zM8 20L12 20 12 24 8 24 8 20zM14 20L18 20 18 24 14 24 14 20zM20 20L24 20 24 24 20 24 20 20z"/></svg>',
    listView:{
        title: '<joe-title>${name}</joe-title><joe-subtitle>${info}</joe-subtitle>',
        listWindowTitle: 'Boards'
    },
    fields:[
        'name',
        'info',
        'description',
        {name:'column_property',type:'code',language:'javascript',comment:'if function, must return an array of possible values'},
        {name:'row_property',type:'code',language:'javascript',comment:'if function, must return an array of possible values'},
        // 'row_property',
        // {name:'columns',type:'objectlist',properties:['name','value','width']},
        'dataset',
        '_id','created','itemtype'
    ],
    idprop : "_id"
}};

module.exports = board();