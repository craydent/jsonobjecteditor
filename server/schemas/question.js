var question ={
    title: 'Q | ${name}',
    info:"The question panel is where you can manage the questions used to build a form.",
    filters:function(item){
        return _joe.Filter.Options.tags({group:true,untagged:true,collapsed:true});
    },
    fields:[
        'name',
        
        {name:'info',width:'50%'},
        {name:'title',width:'50%'},
        
        {name:'fieldname',comment:'used for database storage instead of name field'},
        {name:'question_type',type:'select', rerender:'options,content,question_dataset,option_template',
            values:['oneline','textarea','select','boolean','multiple','content','password','number']},
        {name:'question_dataset',type:'select',width:'33%',
            values:function(){
                return [{name:'none',value:''}].concat(__collectionNames);
            },hidden:function(item){
                return (['multiple','select'].indexOf(item.question_type) == -1);
            }
        },
        {name:'option_template',width:'33%',
            hidden:function(item){
                return (['multiple','select'].indexOf(item.question_type) == -1);
            }
        },
        {name:'value_template',width:'33%',
            hidden:function(item){
                return (['multiple','select'].indexOf(item.question_type) == -1);
            }
        },
        {name:'options',type:'objectList',comment:'label is the wording, value is the code',
            hidden:function(item){
                return (['multiple','select'].indexOf(item.question_type) == -1);
            },
            properties:['label','value']
        },
        {name:'content',type:'code',
            comment:'receives an object of {question,form} when run as function.',
            hidden:function(item){
            return (item.question_type != 'content')
        }},
        'required:boolean',
        {sidebar_start:'right'},
            {section_start:'adv',collapsed:false},
                {name:'canBeId',type:'boolean',tooltip:'can this field be used as form visitor id',display:'use as id',label:'can be used as visitor ID'},
                {name:'canBeDate',type:'boolean',tooltip:'can this field be used as form submission date',display:'use as date',label:'can be used as form date'},
            {section_end:'adv'},
        'tags',
        {sidebar_end:'right'},
        {section_start:'system',collapsed:true},
        '_id','created','itemtype',
        {section_end:'system'}
    ],
    idprop: '_id',
    listView:{
        title: '<joe-subtext>${fieldname}</joe-subtext>'
        +'<joe-title>${name}</joe-title><joe-subtitle>${question_type}</joe-subtitle>',
        listWindowTitle: 'Questions'
    }
};

module.exports = question;