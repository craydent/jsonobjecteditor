var form ={
    title: '${name}',
    info:"Build a form to collect information from your audience. Add questions from the question panel.",
    menuicon:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-8 -8 48 48"><path d="M16 2C14.7 2 13.8 2.9 13.4 4L11 4 6 4 5 4 5 5 5 28 5 29 6 29 26 29 27 29 27 28 27 5 27 4 26 4 21 4 18.6 4C18.2 2.9 17.3 2 16 2zM16 4C16.6 4 17 4.4 17 5L17 6 18 6 20 6 20 8 12 8 12 6 14 6 15 6 15 5C15 4.4 15.4 4 16 4zM7 6L10 6 10 9 10 10 11 10 21 10 22 10 22 9 22 6 25 6 25 27 7 27 7 6zM9 12L9 13 9 16 9 17 10 17 13 17 14 17 14 16 14 13 14 12 13 12 10 12 9 12zM16 13L16 15 23 15 23 13 16 13zM11 14L12 14 12 15 11 15 11 14zM9 19L9 24 14 24 14 19 9 19zM16 20L16 22 23 22 23 20 16 20z"/></svg>',
    fields:[
        'name',
        {   name:'visitor_id_field',
            display:'visitor id field', type:'select',blank:true,width:'50%',
            values:function(){return _joe.Data.question.where({canBeId:true});},
            idprop:function(q){
                var ques = _joe.getDataItem(q._id,'question');
                if(ques.fieldname){return "fieldname";}
                return "name";
            }
        },
        {   name:'submission_date_field',width:'50%',
            display:'submission date field', type:'select',blank:true,
            values:function(){return _joe.Data.question.where({canBeDate:true});},
            idprop:function(q){
                var ques = _joe.getDataItem(q._id,'question');
                if(ques.fieldname){return "fieldname";}
                return "name";
            }
        },
        {section_start:'Content'},
            {name:'sections',type:'objectList',properties:['name','id'],comment:'name will be displayed, id is for css and js'},
        // {name:'questions',type:'objectReference',values:'question'},
            {name:'questions',type:'buckets',values:'question',
            comment:'Drag questions to the section of the form they belong in. <joe-button onclick="_joe.Fields.rerender(\'questions\');" class="joe-reload-button joe-button joe-iconed-button">reload form sections</joe-button>',
            bucketCount:function(item){
                return (item.sections && item.sections.length) || 1;
            },
            bucketNames:function(item){
                var names = [];
                (item.sections ||[]).map(function(s){
                    names.push(s.name);
                })
                return names;
            },
            template:'<joe-subtext>${question_type}</joe-subtext><joe-title>${name}</joe-title><joe-subtitle>${fieldname}</joe-subtitle>'
        
            },
        {section_end:'Content'},
        {section_start:'Submission'},
            {label:'Submit Action'},

            {label:'On Client'},
            {name:'validate',type:'code',language:'javascript',
                comment:'function passed form values as object, <br/> Return true to continue processing form.<br/>Return false to cancel form, return a string to cancel form with message.<br/>ex function(visitor_submission_object){<br/> return true; <br/>}'},
            {name:'callback',type:'code',language:'javascript',
                comment:'function called on server response callback: function(data)'},
                
            {label:'On Server'},
            {name:'server_action',type:'code',language:'javascript',
                comment:'function called on server, function(submission,form_info,request)'},
        {section_end:'Submission'},
        {sidebar_start:'right'},
        'updated',
            {section_start:'Preview'},
            {name:'preview',type:'content',
            template:function(item){
                if(__jsc && __jsc.port){
                    return `<joe-button icon="form" class="joe-orange-button joe-button full-width"
            onclick="window.open('/API/plugin/formBuilder/render?formid=${item._id}')">
                Preview Form</joe-button>`;
                }
                return '<joe-button icon="form" class="joe-orange-button joe-button full-width" \
            onclick="window.open(\'//'+location.hostname+':'+
            __jsc.port+'/API/plugin/formBuilder/render?formid=${_id}\')">\
                Preview Form</joe-button>';}
            },
            {section_end:'Preview'},
            {section_start:'Adv'},
                'form_button_label',
                {name:'hostname',comment:'joe hostname'},
                {name:'save_submission', type:'boolean', display:'save submission',comment:"use standard submission object"},
                {name:'upsert_submission', type:'boolean', display:'upsert submission',
                comment:"if submission exists (visitorID && submisson_date), update it"},
            {section_end:'Adv'},
            {section_start:'includes'},
            'includes',
            {name:'createInclude', type:'create',schema:'include',label:false},
            {section_end:'includes'},
            'tags',
        {sidebar_end:'right'},
        
        {section_start:'system',collapsed:true},
        '_id','created','itemtype',
        {section_end:'system'}
    ],
    idprop: '_id',
    listView:{
        title: function(item){
            
            return '<joe-title>${name}</joe-title><joe-subtitle>${COUNT[${sections}]} sections, </joe-subtitle><joe-subtext>${_id}</joe-subtext>'
        },
        listWindowTitle: 'Forms'
    }
};

module.exports = form;