var schema = {
    title : 'Visitor | ${name}',
    info:"A visitor is anyone who has interacted with your online content.",
    listView:{
        title: '<joe-title>${name}</joe-title><joe-subtitle>${info}</joe-subtitle>',
        listWindowTitle: 'Visitors'
    },
    fields:[
        'name',
        'username',
        'password',
        'email',
        'info',
        'status',
        '_id','created','itemtype'
    ],
    idprop : "_id"
};

module.exports = schema;