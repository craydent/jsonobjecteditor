var schema = {
    title : 'Role | ${name}',
    listView:{
        title:
        '<joe-title>${name}</joe-title>' +
        '<joe-subtitle>${info}</joe-subtitle>',
        listWindowTitle: 'Roles'
    },
    fields:[
        'name',
        'info',
        {name:'group',type:'select',values:['super','admin','editor','viewer']},
        'description',
        
        '_id','created','itemtype'
    ],
    idprop : "_id"
};

module.exports = schema;