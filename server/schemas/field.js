
var schema = {
    title : 'Field | ${name}',
    listWindowTitle: 'Fields',
    idprop:'_id',
    //hideNumbers:true,
    fields:[
        'name',
        'display',
        'label',//false for none
        'type',
        'run',
        'values',
        
        '_id','created','itemtype'
    ]
};

module.exports = schema;