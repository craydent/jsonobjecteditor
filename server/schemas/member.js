var schema = {
    title : '${name}',
    info:"A member is a site visitor who has created an account to access private site content.",
    menuicon:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-6 -6 36 36"><path d="M20 5H4C2.9 5 2 5.9 2 7v10c0 1.1 0.9 2 2 2h16c1.1 0 2-0.9 2-2V7C22 5.9 21.1 5 20 5zM8.5 7C9.9 7 11 8.1 11 9.5c0 1.4-1.1 2.5-2.5 2.5S6 10.9 6 9.5C6 8.1 7.1 7 8.5 7zM13 17H4v-1c0-1.7 1.7-2.9 3-3h3c1.2 0.1 3 1.3 3 3V17zM20 15h-5v-2h5V15zM20 11h-5V9h5V11z"/></svg>',
    listView:{
        title: '<joe-title>${name}</joe-title>'
        +'<joe-subtitle>${first_name} ${last_name}</joe-subtitle><joe-subtext>${email}</joe-subtext>',
        listWindowTitle: 'Members'
    },
    fields:[
        {extend:'name',specs:{comment:'username'}},
        'password:password',
        'info',
        'first_name:text:50%',
        'last_name:text:50%',
        'email',
        'phone',
        'address',
        'postal_code',
        'country',
        'image:image',
        'private:boolean',
        {extend:'description',specs:{comment:'member bio'}},
        {name:'member_info',type:'subObject'},

        {section_start:'system',collapsed:true},
        '_id','created','itemtype','token',
        {section_end:'system'}
    ],
    idprop : "_id"
};

module.exports = schema;