var task = function(){return{
    title : '${name}',
    info:"Create a new reward to be shared as a gift.",
    listWindowTitle: 'Rewards',
    menuicon:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-24 -24 148 148"><path d="M24.1 20.6c0 0 0 0 0 0 0 0 0 0 0 0 0 0.1 0.1 0.2 0.1 0.2 0 0 0 0 0 0 0 0 0 0 0 0.1 0 0 0 0.1 0.1 0.1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0l25 21.1c0 0 0 0 0 0 0.1 0.1 0.1 0.1 0.2 0.1 0 0 0.1 0 0.1 0 0.1 0 0.2 0.1 0.3 0.1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.1 0 0.2 0 0.3-0.1 0 0 0.1 0 0.1 0 0 0 0.1 0 0.1 0 0 0 0 0 0.1-0.1 0 0 0.1 0 0.1 0l25-21.1c0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.1-0.1 0.1-0.1 0 0 0 0 0-0.1 0 0 0 0 0 0 0-0.1 0.1-0.2 0.1-0.2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0-0.1 0-0.2 0-0.2 0 0 0 0 0 0 0 0 0-0.1 0-0.1 0-0.1 0-0.1 0-0.2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0-0.1 0 0 0-0.1 0-0.1L69.7 9.5C69.5 9.2 69.2 9 68.8 9H31.2c-0.4 0-0.7 0.2-0.9 0.5L24.2 19.9c0 0 0 0.1 0 0.1 0 0 0 0 0 0.1 0 0 0 0 0 0 0 0 0 0 0 0 0 0.1 0 0.1 0 0.2 0 0 0 0.1 0 0.1 0 0 0 0 0 0 0 0.1 0 0.2 0 0.2C24.1 20.6 24.1 20.6 24.1 20.6zM27.8 21.4h9.2l9.2 15.5L27.8 21.4zM49 37.8l-9.7-16.5H49V37.8zM53.6 37.2l5.4-9.1c0.3-0.5 0.1-1.1-0.3-1.4s-1.1-0.1-1.4 0.3L51 37.6V21.4h9.8c-0.2 0.4 0 1 0.4 1.2 0.2 0.1 0.3 0.1 0.5 0.1 0.3 0 0.7-0.2 0.9-0.5l0.5-0.9h9.2L53.6 37.2zM73.2 19.4h-8.9l4.5-7.4L73.2 19.4zM67 11l-4.5 7.4L58 11H67zM60.7 19.4h-8.9l4.5-7.4L60.7 19.4zM54.5 11L50 18.4 45.5 11H54.5zM43.2 11l5 8.4h-8.9l2.5-4.1c0.3-0.5 0.1-1.1-0.3-1.4 -0.5-0.3-1.1-0.1-1.4 0.3l-2.5 4.2L33 11H43.2zM31.3 11.9l4.5 7.4h-8.9L31.3 11.9z"/><path d="M90.1 63.3l-6.1-14.4c-0.2-0.4-0.5-0.6-0.9-0.6H37.5 16.9c-0.4 0-0.8 0.2-0.9 0.6L9.9 63.3c-0.1 0.3-0.1 0.7 0.1 0.9s0.5 0.4 0.8 0.4h5.1v22.7C15.9 89.4 17.6 91 19.6 91h17.9H80.4c2 0 3.7-1.6 3.7-3.7V64.7h5.1c0.3 0 0.6-0.2 0.8-0.4S90.2 63.6 90.1 63.3zM16.9 62.7h-4.6l5.3-12.4h18.4l-1.4 3.2c0 0 0 0 0 0.1l-3.9 9.1H16.9zM36.5 89H19.6c-0.9 0-1.7-0.8-1.7-1.7V64.7h13.5c0.4 0 0.8-0.2 0.9-0.6l4.2-9.8c0 0 0 0 0-0.1V89zM82.1 87.3C82.1 88.2 81.3 89 80.4 89H38.5V54.2c0 0 0 0 0 0.1l4.2 9.8c0.2 0.4 0.5 0.6 0.9 0.6h29.9v12.6c0 0.6 0.4 1 1 1s1-0.4 1-1V64.7h6.7V87.3zM83.1 62.7H44.2l-3.9-9.1c0 0 0 0 0-0.1l-1.4-3.2h43.5l5.3 12.4H83.1z"/><path d="M75.4 81.9c0-0.1-0.1-0.1-0.1-0.2 0 0-0.1-0.1-0.1-0.1C75.1 81.5 75 81.5 75 81.5c-0.1 0-0.1-0.1-0.2-0.1 -0.1 0-0.1-0.1-0.2-0.1 -0.1 0-0.3 0-0.4 0 -0.1 0-0.1 0-0.2 0.1 -0.1 0-0.1 0-0.2 0.1C73.8 81.5 73.8 81.5 73.7 81.6c-0.2 0.2-0.3 0.5-0.3 0.7 0 0.3 0.1 0.5 0.3 0.7 0.2 0.2 0.4 0.3 0.7 0.3 0.1 0 0.1 0 0.2 0 0.1 0 0.1 0 0.2-0.1 0.1 0 0.1 0 0.2-0.1 0.1 0 0.1-0.1 0.1-0.1 0.2-0.2 0.3-0.4 0.3-0.7 0-0.1 0-0.1 0-0.2C75.4 82 75.4 82 75.4 81.9z"/></svg>',
    listTitle:function(item){

    return `
    <joe-full-right><joe-subtitle>${((item.points && item.points+' pts' )||'')}</joe-subtitle></joe-full-right>
    <joe-title>${item.name}</joe-title><joe-subtitle>${item.info}</joe-subtitle>
    `
    },
    sorter:[
        'name',
        {field:'!points',display:'points'},
        
    ],
    
    methods:{
       createInstance:function(){}
    },
/*    menu:function(){
        if(__jsu && ['super','admin','editor'].indexOf(__jsu.role) != -1 || $c.isEmpty(self.Data.user)){
            return [
                __exportBtn__,
                _joe.SERVER.History.button,
                {name:'assingme',title:'assign task to me', label:_joe.schemas.user.menuicon+'<button-text>Assign Me</button-text>', action:'_joe.schemas.task.methods.addUser(_joe.User._id,true)', css:'joe-left-button joe-orange-button joe-svg-button'},
                {name:'dotoday',title:'today', label:_joe.schemas.event.menuicon+'<button-text>Today</button-text>', action:'_joe.schemas.task.methods.doToday()', css:'joe-left-button joe-orange-button joe-svg-button'},
                __quicksaveBtn__,
                __deleteBtn__
            ];
        }
        return [];
    },*/
    fields:function(){
        var fields = [
        {section_start:'Overview'},
            'name',
            'info',
            'description',
            {name:'points',type:'number',display:'Reward Cost (in points)'},
        {section_end:'Overview'},

       
       
        {section_start:'related',collapsed:function(item){
            return !(item.files && item.files.length);
        }},
        {name:'files',type:'uploader',allowmultiple:true, height:'300px',comment:'drag files here to upload', onConfirm:_joe.SERVER.Plugins.awsFileUpload},
        'references',
        
        {section_end:'related'},
        'tags',
        {section_start:'activity',collapsed:true},
        {name:'activity',type:'content',value:'TODO, add list of instances of this reward being used. Include date and assignee'},
        {section_end:'activity'},
        {section_start:'system',collapsed:true},
        '_id','created','itemtype',
        {section_end:'system'}
    ];
    return fields;
    },
    
    
    


    idprop : "_id",
    instance:{
        checkbox:function(instance){
            return {prop:'approved'}
        },
        fields:function(){
            return [
                {name:'approved',type:'boolean',display:'APPROVED','label':'click when reward is approved',width:'50%'},
                {name:'date',type:'date',width:'50%',native:true},
                {name:'members',type:'group',label:'completed by -',
                values:function(){
                    return _joe.Data.user.where({custom_roles:{$in:['chore_assignee']}});
                    }
                },
                {name:'points',type:'number'},
                
                

            ];
        }
    }
    
}
};

module.exports = task();