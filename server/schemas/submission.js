var schema ={
    title: 'Submission | ${form_name}',
    info:"A submission is a response to a form. ",
    menuicon:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-19 -19 88 88" transform="rotate(-90)"><path d="M30 3A1 1 0 0 0 29.8 3L8 3 8 47 42 47 42 40.4 40 42 40 45 36.2 45 36 45.1 36 45 10 45 10 5 29 5 29 16 40 16 40 21 42 22.6 42 14.6 30.8 3.4A1 1 0 0 0 30.6 3.2L30.4 3 30.2 3A1 1 0 0 0 30 3zM31 6.4L38.6 14 31 14 31 6.4zM16 21L16 23 34 23 34 21 16 21zM38 22L38 28 31 28 31 35 38 35 38 41 50 31.5 38 22zM40 26.1L46.8 31.5 40 36.9 40 35 40 33 38 33 33 33 33 30 38 30 40 30 40 28 40 26.1zM16 27L16 29 29 29 29 27 16 27zM16 33L16 35 29 35 29 33 16 33z"/></svg>',
    subsets:function(i){
        var subsets = [];
        return subsets;
    },
    fields:[
        'form_name',
        //'form',
        {name:'form',type:'select',values:'form',locked:true,
        /*goto:true,schema:'form'*/
    
    },
        {name:'visitor',locked:true,width:'50%'},
        {name:'submission_date',locked:true,width:'50%'},
        {name:'data',type:"content",passthrough:true,
        run:function(sub){

            return '<pre>'+JSON.stringify(sub.data,'','\n')+'</pre>';
        }},
        'computed:code',
        'source:url',
        {section_start:'system',collapsed:true},
        '_id','created','itemtype',
        {section_end:'system'}
    ],
    idprop: '_id',
    listView:{
        title: function(item){
            var user = $J.get(item.visitor);
            var username = (user && user.name) || item.visitor || '';
            var title =  `<joe-subtext>${username} - ${item.submission_date||''}</joe-subtext>
                <joe-title>${item.form_name}</joe-title>
                <joe-subtitle>${_joe.Utils.prettyPrintDTS(item.created)}</joe-subtitle>`;
                return title;
        },
        listWindowTitle: 'Submissions'
    },
    sorter:['!created','submission_date']
};

module.exports = schema;