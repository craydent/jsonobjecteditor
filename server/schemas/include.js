var user = function(){return{
    title : '${name} (.${filetype})',
    default_schema:true,
    info:"Use an include to store a piece of javascript or css that you can reference throughout your site.",
    menuicon:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-16 -16 82 82"><path d="M29.1 7.3L17.1 41.3 20.9 42.7 32.9 8.7 29.1 7.3zM9.9 13.4L1.3 23.7 0.2 25 1.3 26.3 9.9 36.7 13 34.1 5.4 25 13 15.9 9.9 13.4zM40.1 13.4L37 15.9 44.6 25 37 34.1 40.1 36.6 48.7 26.3 49.8 25 48.7 23.7 40.1 13.4z"/></svg>', 
    listView:{
        title: '<joe-title>${name}</joe-title><joe-subtitle>${info}</joe-subtitle><joe-subtext>${filetype}</joe-subtext>',
        listWindowTitle: 'Includes'
    },
    subsets:[
        {name:'Scripts (.js)',filter:{filetype:'js'}},
        {name:'Stylesheets (.css)',filter:{filetype:'css'}}
    ],
    fields:[
        'name',
        'info',
        {section_start:'content'},
        {name:'content',type:'code', label:false,height:'600px',language:function(item){
            switch(item.filetype){
                case 'js':
                case 'javascript':                
                    return 'javascript';
                break;
                default:
                    return item.filetype;
                break;
            }
        }},
        {section_end:'content'},
        {section_start:'system',collapsed:true},
        '_id','created','itemtype',
        {section_end:'system'},
        {sidebar_start:'right'},
        'updated',
         {name:'preview',type:'content',run:function(item){
            var url = location.origin.replace(__jsc.port,__jsc.sitesPort);
                var path = (url+'/_include/'+item._id);
                return '<joe-subtitle>'+path+'</joe-subtitle><joe-button class="joe-button joe-iconed-button joe-preview-button full-width"  \
                 onclick="window.open(\''+path+'\');">'
                 +'View Include'
                 +'</joe-button>';
            
        }},
            {section_start:'details'},
                {name:'filetype',onchange:'_joe.TITLE.set()',type:'select',values:[
                    {name:'',value:''},
                    {name:'stylesheet',value:'css'},
                    {name:'script',value:'js'}
                ]},
                {name:'fillTemplate',type:'boolean', display:'fill template',label:'run fillTemplate on this'},
                'site',
            {section_end:'details'},
        {sidebar_end:'right'}
    ],
    idprop : "_id"
}};

module.exports = user();