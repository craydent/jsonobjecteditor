var schema = {
    title : '${name}',
    info:'An item to be purchased and added to recipes or eaten as a standalone.',
    listView:{
        title: function(ing){
            var usages = ing.__usagecount == 1? ing.__usagecount+' usage':ing.__usagecount+' usages';
            
            return `
            <joe-title>${ing.name}</joe-title>
            <joe-subtitle>${ing.info}</joe-subtitle>
            <joe-subtext>${usages}</joe-subtext>
            `;
        },
        listWindowTitle: 'Ingredients'
    },
    fields:function(){
        var fieldset = [
            'name','info',
            {section_start:'details'},
            'description',
            {name:'files',type:'uploader',allowmultiple:true, height:'300px',comment:'drag files here to upload', onConfirm:_joe.SERVER.Plugins.awsFileUpload},
            {section_end:'details'},
            {section_start:'usage'},
            {name:'ingredient_usages',type:'content',display:function(i){
                return 'used in '+i.__usagecount+' recipe'+((i.__usagecount == 1)?'':'s');
            },
            run:function(item){
                var html = '';
                item.__usages.map(function(i){
                    let usage = $J.get(i);
                    html += _joe.renderFieldListItem(usage,null,'recipe');
                });
                return html;
              /*  var html = '';
                function returnInstances(itemtype,check){
                    var check = check || function(ins){
                        if(ins.blocks){
                            if(ins.blocks.where({block:item._id}).length){
                                return true;
                            }
                        }
                    };
                    var instances = _joe.Data[itemtype].filter(check)
                    html += '<joe-content>'+instances.length+' '+itemtype+(instances.length != 1 && 's'||'')+'</joe-content>';
                    instances.map(function(i){
                        html += _joe.renderFieldListItem(i,null,itemtype);
                    });
                }
                returnInstances('layout');
                html+='<hr/>';
                returnInstances('page');
                html+='<hr/>';
                returnInstances('block',function(ins){
                    return ins.use_template == item._id;
                });
                return html;*/
            }},
        {section_end:'usage'},
        {sidebar_start:'right'},
            {section_start:'advanced'},
                {name:'mealtype',type:'select',width:'50%',comment:'can this ingredient be a recipe by itself?',display:'standalone recipe?',values:function(){
                    return [{name:'no',value:false}].concat(_joe.schemas.recipe.mealtypes);
                }},
            {section_end:'advanced'},
            {section_start:'tags'},
            {extend:'tags',specs:{labe:false}},
            {section_end:'tags'},
            
        {sidebar_end:'right'}];

        return fieldset.concat(__systemFieldsSection);

    },
    sorter:['name',{name:'usage',value:'!__usagecount'}],
    subsets:function(){
        var subs = [
            {name:'standalone',filter:{mealtype:{$ne:false}}}
        ];

        _joe.schemas.recipe.mealtypes.map(function(mt){
            subs.push({name:mt,filter:{mealtype:mt}});
        })
        return subs;
    },
    filters:function(){
        var stats = _joe.Filter.Options.tags();
        return stats;
    
    },
    idprop : "_id",
    menuicon:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-20 -20 90 90"><path d="M29 0C27.9 0 27 0.9 27 2L27 4C27 5.1 27.9 6 29 6L39 6C40.1 6 41 5.1 41 4L41 2C41 0.9 40.1 0 39 0ZM28.1 7.9C27.2 9.6 26.4 11.9 25.8 13.7L40.5 22 44 22 44 21.8C44 19.5 42.1 12.1 39.8 7.9 39.6 8 39.3 8 39 8L29 8C28.7 8 28.4 7.9 28.1 7.9ZM14.6 13.8C9.6 13.8 5.7 17.2 4.7 22L12.3 22 19.6 15.1C18.1 14.2 16.4 13.8 14.6 13.8ZM23.2 14.7C23 14.7 22.8 14.8 22.6 15L15.2 22 36.5 22 23.8 14.8C23.6 14.7 23.4 14.7 23.2 14.7ZM2.8 24C1.3 24 0 25.3 0 26.8L0 30.2C0 31.7 1.3 33 2.8 33L47.2 33C48.7 33 50 31.7 50 30.2L50 26.8C50 25.3 48.7 24 47.2 24ZM3 35L3 36C3 36.2 3 36.3 6.4 47.4 6.5 47.5 6.5 47.7 6.6 47.7 7.2 48.8 7.8 50 9.2 50L40.8 50C42.5 50 43.1 48.5 43.6 47.4 47 36.3 47 36.2 47 36L47 35 37 35 37 45 34 45 34 35 30 35 30 45 27 45 27 35 23 35 23 45 20 45 20 35 16 35 16 45 13 45 13 35Z"/></svg>',
    onload:function(ingredients){
        var usages = _joe.Cache.get('ingredient_usage');
        if(!usages){
            if(_joe.Data.recipe){
                _joe.schemas.recipe.onload(_joe.Data.recipe);
                usages = _joe.Cache.get('ingredient_usage');
            }else{
                
               // alert('no recipes');
            }
        }
        if(usages){
            ingredients.map(function(ing){
                ing.__usages = usages[ing._id] ||[];
                ing.__usagecount = ing.__usages.length;
            })
        }
    }
};

module.exports = schema;