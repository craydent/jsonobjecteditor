var schema = {
    title : '${name}',
    info:'The ingredients and steps required to prepare a meal.',
    listView:{
        title: function(i){
            return `
            <joe-subtext>${i.mealtype || ''}</joe-subtext>
            <joe-title>${i.name}</joe-title>
            <joe-subtitle>${(i.info && i.info+' -')|| ''} ${((i.ingredients||[]).length)} ingredient(s) </joe-subtitle>`;},
        listWindowTitle: 'Recipes'
    },
    checkbox:{prop:'family_favorite'},
    sorter:['name','mealtype','family_favorite','created'],
    menuicon:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-8 -8 80 80"><rect x="16.2" y="43.3" width="30.1" height="2.3"/><rect x="16.2" y="49.3" width="30.1" height="2.3"/><path d="M9.5 1.9v60.1h45.1V1.9H9.5zM51.5 59.1H12.5V4.9h39.1V59.1z"/><path d="M31 10c-7.7 0-14 6.3-14 14s6.3 14 14 14 14-6.3 14-14S38.7 10 31 10zM29.2 24c0 1.2 0.7 4 0.7 6.7s-1 3.4-1.7 3.4c-0.7 0-1.7-0.5-1.7-3.4s0.7-5.5 0.7-6.7 -2.4-0.8-2.4-3.9c0-2.8 0.4-5.6 0.4-5.6s0.3-0.3 1.2-0.4v5.5c0 0.3 0.2 0.6 0.6 0.6s0.6-0.2 0.6-0.6V14v-0.1c0.1 0 0.3 0 0.4 0 0.2 0 0.4 0 0.7 0 0 0.1 0 0.1 0 0.1v5.5c0 0.3 0.2 0.6 0.6 0.6s0.6-0.2 0.6-0.6v-5.4c0.8 0.2 1.2 0.4 1.2 0.4s0.5 3.8 0.5 5.6C31.6 23.3 29.2 22.8 29.2 24zM36 30.2c0 1.7-0.3 3.9-1.1 3.9 -0.8 0-1.1-1.2-1.1-3.9 0-1.8 0-16.2 0-16.2 2.9 1.4 3.4 4.4 3.4 5.8 0 2.5-1.7 4.5-1.7 6.2C35.5 26.9 36 27.9 36 30.2z"/></svg>',
    subsets:function(){
        var subs = [];
        _joe.schemas.recipe.mealtypes.map(function(mt){
            subs.push({name:mt,filter:{mealtype:mt}});
        })
        return subs;
    },
    mealtypes:['breakfast','lunch','dinner','snack','side','main','dessert'],
    fields:function(){
        return [
            'name','info','url',
            {name:'mealtype',display:'recipe type',type:'select',values:function(){
                return _joe.schemas.recipe.mealtypes;
            }},
        {section_start:'ingredients'},
            {name:'ingredients',label:false,type:'objectList',properties:[
                {name:'id',display:'name',type:'select',values:'ingredient',blank:true},
                {name:'amount', width:'20%'}
            ],template:function(recipe,ing){
                var ingredient = _joe.getDataItem(ing.id,'ingredient');
                var ts = 
                ((ing.amount&&`<joe-subtext>${ing.amount}</joe-subtext>`)||'')+
                `<joe-title>${ingredient.name}</joe-title>`;
                return ts;
            }},
        {section_end:'ingredients'},
        {section_start:'directions'},
        'description',
        {section_end:'directions'},
        {name:'files',type:'uploader',allowmultiple:true, height:'300px',comment:'drag files here to upload', onConfirm:_joe.SERVER.Plugins.awsFileUpload},
        {sidebar_start:'right'},
        'updated',
        'reports',
        'tags',
        {name:'family_favorite',type:'boolean',display:"FAMILY FAVORITE", label:'will show up as option for tasker'},
        
        /*'favorite',*/
        {sidebar_end:'right'},
        
        {section_start:'system',collapsed:true},
        '_id','created','itemtype',
        {section_end:'system'}
    ]},
    idprop : "_id",
    onload:function(recipes){
        //catalog ingredient usage
        var usages ={};
        var id;
        var any = false;
        recipes.map(function(rec){
            (rec.ingredients|| []).map(function(ing){
                any = true;
                id = ing.id;
                usages[id] = usages[id] || [];
                usages[id].push(rec._id);
            })
        })
        _joe.Cache.add(usages,{id:'ingredient_usage'});
        if(!_joe.Cache.get('ingredient_usage')){
            alert('ingredient usage error');
        }
        /*if(_joe.Data.ingredient){
            alert('ingredients');
            _joe.schemas.ingredient.onload(_joe.Data.ingredient);
        }*/
    }
};

module.exports = schema;