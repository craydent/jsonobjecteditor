var site ={
    title: '${name}',
    //info:'The main public facing elements, are made up of pages, includes, layouts and plugins that can display content from anywhere in JOE. Use the <b>sitebuilder</b> app for a more focused view.',
    info:"Manage each individual site bulit with JOE. ",
    listWindowTitle:'Sites',
      listTitle:'<joe-subtitle>${info}</joe-subtitle><joe-title>${name}</joe-title><joe-subtitle>${url}</joe-subtitle>',

    itemMenu:function(item) {
        var viewAction = __jsc.PORT?`window.open('${('/'+item.url).replace('//','/')}')`:'window.open(\'//'+location.hostname+':'+__jsc.sitesPort+'/${url}/\')';
        var menu = [
            {name:'pages',action:'goJoe(\'page\',{subset:\''+item.name+'\'})'},
            {name:'<joe-icon class="joe-view-button" title="view">&nbsp;</joe-icon>',action:viewAction}
        ];
        return menu;
    },

     menuicon:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-150 -150 800 800"><path d="M296 4c0 0 0 0 0 0l0 0C281 2 266 0 250 0s-31 2-46 4l0 0L204 4C88 26 0 128 0 250c0 138 112 250 250 250 138 0 250-112 250-250C500 128 412 26 296 4zM34 268h101c1 26 5 54 11 83H57C44 325 36 297 34 268zM134 231H34c3-29 11-56 24-81h87C139 175 135 202 134 231zM178 150h142c6 24 10 51 11 81H168C169 201 173 174 178 150zM168 268h164c-1 26-5 54-12 83H181C173 321 169 294 168 268zM366 268h101c-2 30-11 58-24 83h-88C361 321 365 294 366 268zM366 231c-1-29-5-56-10-81h87c13 25 21 52 24 81H366zM419 113h-72C338 85 328 62 319 44 358 57 393 82 419 113zM275 34c8 14 23 41 36 79H189c13-38 28-65 37-79 8-1 16-1 25-1C258 33 267 33 275 34zM181 44c-9 18-19 41-27 69H81C107 82 142 57 181 44zM82 387h74c7 23 17 46 28 70C143 444 108 419 82 387zM228 466c-15-27-27-54-36-79h116c-9 26-20 52-35 79 -7 1-15 1-22 1C242 467 235 467 228 466zM316 457c12-24 21-47 28-70h74C392 419 357 444 316 457z" /></svg>',
    fields:[
        //'display',
        'name',
        'info',
        {extend:'url',specs:{schema:'site',prefix:function(item){
            var viewAction = __jsc.PORT?`/`:`//localhost:${JOE.webconfig.sitesPort}`;
            return viewAction;
        },
            tooltip:'leave blank for default site'}},
        {section_start:"includes"},
        'includes',
        {section_end:"includes"},
        {section_start:"datasets",collapsed:function(item){
            if(item.datasets && item.datasets.length){
                return false;
            }
            return true;
        }},
        {extend:'datasets',specs:{comment:'select the datasets to make available to this sites content (pages, blocks, layouts, etc)'}},
        {section_end:"datasets"},
        /*
        {section_start:'ID', section_label:'Includes + Datasets',collapsed:true},
                'includes',
                'datasets',
        {section_end:'ID'},
        */
        {sidebar_start:'right'},
        'updated',
        {section_start:'Pages'},
            {name:'homepage',type:'select',idprop:'_id',
                template:'${name} : ${path}',
                goto:'page',
                //blank:true,
                values:function(item){
                return [{name:'select page',path:'',_id:''}].concat(_joe.Data.page.where({site:item._id}));
            }},
            {name:'addpage',type:'create',schema:'page',overwrites:function(item){
                return {site:item._id};
            }},
            {name:'pages',type:'content',run:function(item){
                var html = '';
                var pages = _joe.Data.page.where({site:item._id}).sortBy(['path']);
                pages.map(function(t){
                    html += _joe.renderFieldListItem(t,
                        '<joe-title>${name}</joe-title>' +
                        '<joe-content>${info}</joe-content>'+
                        '<joe-subtitle>${path}</joe-subtitle>',
                        'page');
                });
                return html;
            }},
        {section_end:'Pages'},
        {sidebar_end:'right'},
        {section_start:'system',collapsed:true},
        '_id','created','itemtype',
        {section_end:'system'}
    ],
    idprop: '_id',
    itemExpander:function(item){
        var pages = _joe.Data.page.where({site:item._id});
/*        if(!pages.length){
            return '';
        }*/
        var html = '<joe-title>Pages</joe-title>';
        pages.map(function(page){
            html+=_joe.renderFieldListItem(page,'<joe-title>${name}</joe-title><joe-subtitle>${info}</joe-subtitle>','page');
            //item,contentTemplate,schema,specs

        });
        return html;
    }
};

module.exports = site;