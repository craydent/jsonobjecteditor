var email ={
    title: 'Tag | ${name}',
    fields:[
        'name',
        'info',
        'sender',
        'recipient',
        'subject',
        'content',
        '_id','created','itemtype'
    ],
    idprop: '_id',
    listView:{
        listWindowTitle: 'Emails'
    }
};

module.exports = email;