const { itemExpander } = require("./goal");

var tag ={
    title: '${name}',
    info:"Assign a tag to categorize different pieces of information so you can easily access information in a category in one place.",
    default_schema:true,
    fields:[
        'name',
        {name:'tag_id',display:'Tag ID',comment:'a memorable ID for this parent tag'},
        {name:'parent_tag',type:'select',values:'tag',idprop:'_id',blank:true},
        'info',
        'datasets',
        {section_start:'usage',collapsed:true},
        {name:'usages',label:false,type:'content',
        run:function(item){
            var usages = _joe.search({tags:{$in:[item._id]}})
            var html = '<joe-title>'+usages.length+' objects use this tag</joe-title>';
            usages.sortBy('itemtype,name').map(function(usage){
                html += _joe.renderFieldListItem(usage,function(item){
                var html = 
                '<joe-full-right>'
                    +'<joe-subtext>updated</joe-subtext>'
                    +'<joe-subtitle>'+_joe.Utils.toDateString(item.joeUpdated)+'</joe-subtitle>'
                    +'</joe-full-right>'
                +(_joe.schemas[usage.itemtype].menuicon && '<joe-icon class="icon-50 icon-grey fleft">'
                +_joe.schemas[usage.itemtype].menuicon+'</joe-icon>' || '')
                +'<joe-subtext>${itemtype}</joe-subtext>'
                +'<joe-subtitle>${name}</joe-subtitle><joe-subtext>${info}</joe-subtext>';

                return html;
            },usage.itemtype);
            });
            return html;
        }},
        {section_end:'usage'},
        '_id','created','itemtype'
    ],
    idprop: '_id',
    itemExpander:function(tg){
        //todo: return list of sub tags

    },
    subsets:function(){
        var schemas = [];
        var subs = [];
        _joe.current.list.map(function(tag){
            schemas = schemas.concat(tag.datasets||[]);
        });

        (new Set(schemas)).map(function(schema){
            subs.push({name:schema,filter:{datasets:{$in:[schema]}}})
        });
        return subs.sortBy('name');
    },
    menuicon:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-200 -200 900 900"><rect x="101.7" y="183.8" transform="matrix(-0.7071 -0.7071 0.7071 -0.7071 264.6132 638.825)" width="325.9" height="161.6" class="a"/><path d="M491.2 310.4L309.1 128.3l-41.9-42 -50.8-50.8L7.4 0.2c-9.4-1.6-7.2 7.2-7.2 7.2l35.3 209 50.8 50.8 42 42 182.1 182.1c11.7 11.7 30.8 11.7 42.4 0L491.3 352.8C502.9 341.2 502.9 322.1 491.2 310.4zM53.7 73.2c-10.7 0-19.5-8.7-19.5-19.5C34.2 42.9 43 34.2 53.7 34.2 64.5 34.2 73.2 42.9 73.2 53.7 73.2 64.5 64.5 73.2 53.7 73.2zM322.7 456.9L72.4 206.5 206.5 72.4l250.3 250.3L322.7 456.9z" /></svg>',
    listView:{
        title: function(tag){
           // var datasets = tag.datasets.split(',');
            return '<joe-fright>${RUN[_joe.getDataItemProp;\'${parent_tag}\',\'tag\']}</joe-fright><joe-title>${name}</joe-title><joe-subtitle>${info}</joe-subtitle>'+
            
        '<joe-subtext>${ENUM[${datasets}]}</joe-subtext>';
            //+(datasets||'');
        },
        listWindowTitle: 'Tags'
    }
};

module.exports = tag;