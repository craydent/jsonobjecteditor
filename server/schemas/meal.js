var schema = {
    title : '${name}',
    info:'A collection of recipes and important information about the preparation of this meal.',
    listView:{
        title: '<joe-title>${name}</joe-title><joe-subtitle>${info}</joe-subtitle>',
        listWindowTitle: 'Meals'
    },
    fields:function(){
        return[
        'name','info',
        {name:'mealtype',type:'select',values:function(){
                return _joe.schemas.recipe.mealtypes;
        }},
        'recipes',
        'description',
        {name:'files',type:'uploader',allowmultiple:true, height:'300px',comment:'drag files here to upload', onConfirm:_joe.SERVER.Plugins.awsFileUpload},
        'tags',
        '_id','created','itemtype'
    ]},
    idprop : "_id",
    menuicon:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-15 -15 80 80"><path d="M25 0C11.2 0 0 11.2 0 25 0 38.8 11.2 50 25 50 38.8 50 50 38.8 50 25 50 11.2 38.8 0 25 0ZM21.7 25C21.7 27.2 23 32.2 23 37 23 41.8 21.3 43 20 43 18.7 43 17 42.1 17 37 17 31.9 18.3 27.2 18.3 25 18.3 22.8 14 23.6 14 18 14 13 14.8 8 14.8 8 14.8 8 15.3 7.5 17 7.2L17 17C17 17.6 17.4 18 18 18 18.6 18 19 17.6 19 17L19 7.1C19 7.1 19 7 19 7 19.2 7 19.5 7 19.8 7 20.2 7 20.6 7 21 7 21 7.1 21 7.1 21 7.1L21 17C21 17.6 21.4 18 22 18 22.6 18 23 17.6 23 17L23 7.3C24.5 7.6 25.1 8 25.1 8 25.1 8 26 14.7 26 18 26 23.7 21.7 22.8 21.7 25ZM34 36C34 39 33.5 43 32 43 30.5 43 30 40.8 30 36 30 32.8 30 7 30 7 35.1 9.5 36 14.8 36 17.4 36 21.8 33 25.5 33 28.4 33 30.1 34 32 34 36Z"/></svg>'
    
};

module.exports = schema;