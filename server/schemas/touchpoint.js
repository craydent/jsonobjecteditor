var schema = {
    title : 'Touchpoint | ${name}',
    info:"Here is a catalogue of every touchpoint you've ever made with each of your visitors.",
    menuicon:'<svg viewBox="-250 -250 1800 1800" xmlns="http://www.w3.org/2000/svg"><path d="M702 994 417 788s-30-20-18-35c1-3 7-10 22-14 31-8 73 16 101 33 28 16 62 31 62 31L441 443l-1-1s-2-6 1-12c3-7 13-14 27-20 13-5 25-6 31-3 5 2 7 6 7 7v1l99 248 43-16-40-100 54-21c4 0 20 0 31 26l27 68 43-16-23-57 49-18 1-1-1 1c5-1 14 4 23 25l27 68 43-16-25-62 44-16h7c6 3 12 10 17 22 0 0 52 136 62 203 10 75 6 171 3 220a543 543 0 0 0 46-55A540 540 0 0 0 514 67a320 320 0 0 1 125 420l-47 18-42-104-3-8a202 202 0 0 0-196-252 202 202 0 1 0 75 390l42 106 2 4A320 320 0 0 1 70 496a541 541 0 0 0 681 622zM399 413c-7 15-6 29-4 38-14 5-28 8-44 8a116 116 0 1 1 114-94l-14 4c-26 10-44 24-52 44z"/></svg>',
    listView:{
        title: '<joe-title>${info}</joe-title><joe-subtitle>${session}</joe-subtitle><joe-subtitle>${visitor}</joe-subtitle>',
        listWindowTitle: 'Touchpoints'
    },
    fields:[
        'visitor',
        'session',
        'info',
        'description',
        '_id','created','itemtype'
    ],
    idprop : "_id"
};

module.exports = schema;