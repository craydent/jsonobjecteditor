var schema ={
    title: '${name}',
    info:"Manage pieces of content you will re-use throughout your site, like a blog post.",
    menuicon:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-9 -9 44 44"><path d="M18.9 0.2C18.6 0.1 18.1 0.4 17.8 0.8L13.1 7.4C10.4 6.6 7.3 7.3 5.2 9.4L16.6 20.8C18.7 18.6 19.4 15.5 18.6 12.8L18.6 12.8 25.2 8.3C25.8 7.8 26 7.2 25.6 6.8L19.3 0.4C19.2 0.3 19.1 0.2 18.9 0.2zM18.9 3.2L20.7 4.9 16.2 9 15.2 8 18.9 3.2zM13 9.3C13.2 9.3 13.3 9.3 13.3 9.3L14.3 10.3C14.3 10.3 13.6 10.4 11.6 12.4L9.8 10.6C11.3 9.5 12.5 9.3 13 9.3zM8.6 14.8L0.2 25.8 11.2 17.3 8.6 14.8z"></path></svg>',
    fields:function(){
        var f = [
        'name',
        'info',
        {section_start:'Content'},
        'content_type',
        {extend:'content',specs:{type:function(item){
            return item.content_type || 'wysiwyg';
        }}},
        {section_end:'Content'},

        {name:'bullets',type:'objectList',properties:['message'],hideHeadings:true},

        {section_start:'files',collapsed:function(item){
            return !(item.files && item.files.length);
        }},
        {name:'files',type:'uploader',allowmultiple:true, height:'300px',comment:'drag files here to upload', onConfirm:_joe.SERVER.Plugins.awsFileUpload},
        {section_end:'files'},

        '_id','created','itemtype',
        {sidebar_start:'right'},
        {section_start:'Adv'},
        'site',
        {name:'post_date',type:'date'},
        'tags',
        {section_end:'Adv'},
        {sidebar_end:'right'}
    ]
        return f;
    },
    idprop: '_id',
    listView:{
        title: '<joe-title>${name}</joe-title><joe-subtitle>${date}</joe-subtitle>',
        listWindowTitle: 'Posts'
    }
};

module.exports = schema;