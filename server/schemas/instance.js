var instance = function(){
    var currentSchema = this;
    return{
    title : '${name}',
    info:"Instances can be used to make a shallow clone (to be modified of a particular) ",
    listWindowTitle: 'Instances',
    default_schema:true,
    searchable:['name','info','description','_id'],
    menuicon:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-16 -16 64 64"><path d="M4 11V6c0-1.1 0.9-2 2-2h4V0H6C2.7 0 0 2.7 0 6v5H4z"/><path d="M22 4h4c1.1 0 2 0.9 2 2v5h4V6c0-3.3-2.7-6-6-6h-4V4z"/><path d="M10 28H6c-1.1 0-2-0.9-2-2v-3H0v3C0 29.3 2.7 32 6 32h4V28z"/><path d="M28 23v3c0 1.1-0.9 2-2 2h-4V32h4c3.3 0 6-2.7 6-6v-3H28z"/></svg>',
    listTitle:function(instance){
        var icon = '';
        if(instance.reference && instance.reference[0]){
            let item = $J.get(instance.reference[0]);
            icon =(_joe.schemas[item.itemtype] && _joe.schemas[item.itemtype].menuicon)||'';
            icon && (icon = `<joe-icon class="fleft icon-grey icon-50 icon-faded">${icon}</joe-icon>`);
        }
        var memberCubes = '';
        if(instance.members){
            memberCubes = `<joe-title>${_joe.SERVER.User.Render.cubes(instance.members)}</joe-title>`;
        }
        /*
                <joe-full-left>
            <joe-subtitle>${((instance.points && instance.points+' pts' )||'')}</joe-subtitle>
        </joe-full-left>
        */
        return `
        <joe-full-right>
            ${memberCubes}
            <joe-title>${((instance.points && instance.points+' pts' )||'')}</joe-title>
        </joe-full-right>

        
        ${icon}
            <joe-title>${instance.name}</joe-title>
            <joe-subtitle>${instance.date || instance.reference || ''}</joe-subtitle        `
    },
    sorter:[
        {field:'!created',display:'created',default:true},
        {field:'!date_str',display:'date'},
        'name',
        {field:'!points',display:'points'},
        
        
    ],
    subsets:function(){
        var sets = []
        //all instance types
        sets = sets.concat(_joe.Filter.Options.getDatasetPropertyValues('instance','instance_type',{group:'type',sortby:'!date_str'}));
        return sets;
    },
    methods:{
       create:function(referenceGuid){}
    },
    onload:function(events_arr){
        events_arr.map(function(event){

            if(event.date){
                event.__date = event.date_str = $c.format(new Date(event.date),'Y-m-d');
            }
            
        })
    },
    filters:function(instance){
        var filters = [];
        //if(['task','reward'].indexOf(instance.instance_type)!= -1){
            filters = filters.concat(
                _joe.Filter.Options.datasetProperty('user','members',{
                group:'assignee',
                collapsed:true,
                query:{custom_roles:{$in:['chore_assignee']}}
            })
            )
        //}

        return filters;
    },
    checkbox:function(instance){
        var itype = instance.instance_type ;
        if(itype
            && _joe.schemas[itype] 
            && _joe.schemas[itype].instance){
                if(_joe.schemas[itype].instance.checkbox){
                    return _joe.propAsFuncOrValue(_joe.schemas[itype].instance.checkbox,instance);
                }
            }
        return false;
        

    },
    fields:function(){
        var fields = [
            {section_start:'overview'},
                'name',
                'info',
                {extend:'reference',specs:{callback:_joe.schemas.instance.methods.setInstanceType}},
                {name:'instance_type',locked:true,type:'select',values:[''].concat(__collectionNames)},
            
            {section_end:'overview'},
            {section_start:'custom'}
        ];
            //custom_fields = [];
        custom_fields = _joe.schemas.instance.methods.getCustomFields();
        end_fields = [
            {section_end:'custom'},

            
            {section_start:'activity',collapsed:true},
            {name:'activity',type:'content',value:'TODO, add list of time stamps of this being edited'},
            {section_end:'activity'},
            {section_start:'system',collapsed:true},
            '_id','created','itemtype',
            {section_end:'system'}
            
        ];
    var final_fields = fields.concat(custom_fields,end_fields)
    return final_fields;
    },
    methods:{
        getCustomFields:function(reference_id){
            //if an item exists, go get it's info
            var refId = reference_id || (_jco().reference && _jco().reference[0]) || null;

            if(!refId){return [];}
            var item = $J.get(refId);
            if(!item){
                return [];
            }else if(item.itemtype && _joe.schemas[item.itemtype] && _joe.schemas[item.itemtype].instance &&_joe.schemas[item.itemtype].instance.fields){
                let instanceFields = _joe.schemas[item.itemtype].instance.fields;
                var customFields = _joe.propAsFuncOrValue(instanceFields) || [];
                customFields = [{label:`${item.itemtype.toUpperCase()} fields`}].concat(customFields);
                return customFields;
            }
            return [];
            return [{name:'custom',type:'content',value:'TODO, add fields that are to be copied by this instance. I think I could add these to the schema or this item.'}];
        },
        setInstanceType:function(value){
            var id = value[0];
            var item;
            id && (item = $J.get(value));
            let type = (item && item.itemtype)||'';
            _joe.Cache.static.todayString = _joe.Cache.static.todayString || $c.format(new Date(),'m/d/Y');
            
            _joe.reload(true,{
                force:true,
                overwrites:{
                reference:value,
                instance_type:type,
                points:(item && item.points|| ''),
                date:_joe.Cache.static.todayString,
                name:((item && item.name+' &#8520;')||'')
            }})
            return;
            //set instance_type field, rerender

            if(item){
                _joe.Fields.set('instance_type',item.itemtype)
                
            }else{
                _joe.Fields.set('instance_type','')
            }
            
            // update custom fields

        }
    },
    idprop : "_id",

    // new:function(){
    //     var payload = {
    //         itemtype:'task',
    //         created:(new Date).toISOString(),
    //         _id:cuid()
    //     };

    //     return payload;
    // },
    //TODO: add events, Look at TASK
    
}
};

module.exports = instance();