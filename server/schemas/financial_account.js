var schema = {
    title : '${name}',
    display:'account',
    menuicon:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-15 -15 80 80"><path d="M23.6 1C22.1 1 21.1 2.2 20.4 3.2 19.7 4.2 18.6 4.2 16.6 4.2 15.5 4.2 14.1 4.3 13.3 4.9 14.6 7 16.2 8.6 17.7 9 18.5 9.3 19.2 9.1 19.8 8.6 20.7 7.7 21.8 6.7 23.1 6.8 24.1 6.9 24.9 7.5 25.9 8.7 26.1 8.9 26.3 9 26.6 9 27.1 9.1 27.8 8.8 28.3 8.3 28.9 7.7 29.6 7.2 30.3 6.6 31.4 5.7 33.5 4.1 33.8 3.3 33.5 3.1 33.1 3 32.5 3 31.7 3 30.9 3.2 30.2 3.4 29.5 3.5 28.9 3.7 28.4 3.7 28 3.7 27.7 3.6 27.4 3.3 25.9 1.7 24.7 1 23.6 1ZM34.3 5.8C33.6 6.6 32.6 7.3 31.5 8.2 30.9 8.7 30.2 9.3 29.7 9.8 28.7 10.6 27.5 11.1 26.4 11 25.6 11 24.9 10.6 24.3 9.9 23.6 9 23.1 8.8 23 8.8 22.5 8.8 21.7 9.5 21.1 10.1 20.3 10.8 19.4 11.1 18.4 11.1 18 11.1 17.6 11.1 17.2 10.9 16.4 10.7 15.6 10.3 14.8 9.7 15.8 10.7 17.2 12.6 17.4 15.2 19.5 16.4 24.3 18.6 29.9 15.4 30.2 15.3 30.6 15 31 14.7 31 14.7 31.1 14.7 31.1 14.7 30.8 10.9 33 8.1 33.3 7.7L33.4 7.6C33.7 7.1 34.1 6.4 34.3 5.8ZM38.2 12C36.3 11.9 34.5 12.6 33.1 13.4 33.1 13.7 33.1 14 33.1 14.3 33.5 14.5 34 14.7 34.4 14.9 36.5 15.9 38.6 17.3 40.4 19 40.5 19 40.5 19 40.5 19 40.7 19 41 18.9 41.2 18.7 41.6 18.3 41.6 17.7 41.2 17.3 39.5 15.5 37.4 14.7 35.3 14.5 36.9 13.9 38.9 13.7 41 14.9 41.5 15.1 42.1 15 42.4 14.5 42.6 14 42.5 13.4 42 13.1 40.6 12.4 39.4 12.1 38.2 12ZM32.4 16.2C32.3 16.2 32.2 16.3 32.1 16.4 31.7 16.7 31.3 16.9 30.9 17.2 28.4 18.5 26.1 19.1 24 19.1 20.4 19.1 17.5 17.6 16 16.7 9.7 19.4 4 27.3 4 34.1 4 45 11.1 47 25 47 38.9 47 46 45 46 34.1 46 26 39.3 18.9 32.4 16.2ZM23.5 22L25.3 22 25.3 24.1C27.9 24.6 28.8 26.4 29.3 28.4L27.5 28.4C27.2 27 26.3 25.8 24.4 25.8 21.7 25.8 21.4 27.6 21.4 28.4 21.4 30.3 23.2 30.7 25.1 31.2 27.5 31.8 29.9 32.6 29.9 35.6 29.9 39.2 26.7 40.2 25.3 40.3L25.3 42 23.5 42 23.5 40.3C22 40 19.3 39.1 19 35.2L20.8 35.2C20.8 36.4 21.6 38.6 24.4 38.6 27.1 38.6 27.8 37.3 27.8 35.8 27.8 31.4 19.4 34.4 19.4 28.6 19.4 25.4 22 24.2 23.5 24Z"/></svg>',

    info:"A financial account like credit, savings, checking or loan.",
    listView:{
        title: '<joe-title>${name}</joe-title><joe-subtitle>${info}</joe-subtitle><joe-subtext>${code}</joe-subtext>',
        listWindowTitle: 'Accounts',
    },
    bgColor:function(fa){
        if(fa.default_account){
            return 'goldenrod';
        }
    },

    subsets:function(acct){
        var sets =[{
            name:'in ledger',
            stripeColor:'green',
            filter:function(i){
                return i.ledger_account;
            }
        }]
        var tts = [];
        sets.push({group_start:'account_type',collapsed:false, name:'account type',filter:{project:{$in:[null,'']}}})
            _joe.schemas.financial_account.account_types.map(function(tt){
                tts.push({name: tt,filter:{financial_account_type: tt}});
                
            });
            sets = sets.concat(tts);
        sets.push({group_end:'account_type'})
        return sets;
    },
    stripeColor:function(acct){
        if(acct.ledger_account){
            return {title:'in ledger',color:'green'};
        }
    },
    methods:{

    },
    fields:[
        {section_start:'general'},
        'name',
        'info',
        {extend:'code',specs:{width:'50%'}},
        {type:'boolean',name:'default_account',width:'50%'},
        'description',

        {section_end:'general'},

        {section_start:'plaid'},
            {name:"plaid_id",tooltip:'must match name in PLAID_ACCESS_TOKENS secret',rerender:'access_token'},
            

            {name:"access_token",display:'access token',tooltip:'only editable by super users', locked:function(){
                return _joe.User.role != "super";
            }
            /*,run:function(ledger){
                try{
                    var plaid = _joe.Data.setting.where({name:"PLAID_ACCESS_TOKENS"})[0];
                    if(!plaid){
                        return 'no plaid spec';
                    }
                    plaid = JSON.parse(plaid.value);
                    var plaid_account = plaid[ledger.plaid_id];
                    if(!plaid_account){
                        return 'plaid account not found';
                    }
                    return plaid_account.access_token;
                }catch(e){
                    return e.message;
                }
            }*/},
            {name:"account_id",display:'account id'},
            {name:"ledger_account",display:'Show account in ledger',type:'boolean',label:"gives realtime running ledger"},

        {section_end:'plaid'},
        
        {section_start:'system',collapsed:true},
        '_id','created','itemtype',
        {section_end:'system'},
        {sidebar_start:'right'},
        'updated',
        'reports',
        {section_start:'details'},
        {display:'account type',name:'financial_account_type',type:'select',
        values:function(){
            return (['']).concat(_joe.schemas.financial_account.account_types);
        },
        required:true},
        'tags',
        'members',
        {section_end:'details'},
        '_protected',
        {sidebar_end:'right'}
        
    ],
    account_types:['checking','savings','credit'],
    idprop : "_id"
};

module.exports = schema;