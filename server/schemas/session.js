var schema = {
    title : 'Session | ${name}',
    info:"A session is a unique visit to your content.",
    listView:{
        title: '<joe-title>${location}</joe-title><joe-subtitle>${visitor}</joe-subtitle>',
        listWindowTitle: 'Visitor Sessions'
    },
    fields:[
        'visitor',
        'location',
        'site',
        '_id','created','itemtype'
    ],
    idprop : "_id"
};

module.exports = schema;