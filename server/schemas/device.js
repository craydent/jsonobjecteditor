var schema = {
    title : 'Device | ${name}',
    info:"Here is every device that has been used to view your content, associated with the person who visited.",
    listView:{
        title:
        '<joe-title>${name}</joe-title>' +
        '<joe-subtitle>${owner}</joe-subtitle>',
        listWindowTitle: 'Devices'
    },
    fields:[
        'name',
        'info',
        'owner',
        'ip',
        'token',
        '_id','created','itemtype'
    ],
    menuicon:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-20 -20 90 90"><path d="M8 4C5.8 4 4 5.8 4 8L4 17 9 17 9 9 41 9 41 23 46 23 46 8C46 5.8 44.2 4 42 4L8 4zM2 18C0.9 18 0 18.9 0 20L0 44C0 45.1 0.9 46 2 46L21 46C22.1 46 23 45.1 23 44L23 20C23 18.9 22.1 18 21 18L2 18zM4 22L19 22 19 40 4 40 4 22zM37 24C35.9 24 35 24.9 35 26L35 44C35 45.1 35.9 46 37 46L48 46C49.1 46 50 45.1 50 44L50 26C50 24.9 49.1 24 48 24L37 24zM39 28L46 28 46 40 39 40 39 28zM24 32L24 43 34 43 34 39 30 39 30 36 34 36 34 32 24 32zM11.5 41.5C12.1 41.5 12.5 41.9 12.5 42.5 12.5 43.1 12.1 43.5 11.5 43.5 10.9 43.5 10.5 43.1 10.5 42.5 10.5 41.9 10.9 41.5 11.5 41.5zM42.5 41.5C43.1 41.5 43.5 41.9 43.5 42.5 43.5 43.1 43.1 43.5 42.5 43.5 41.9 43.5 41.5 43.1 41.5 42.5 41.5 41.9 41.9 41.5 42.5 41.5z"/></svg>',
    idprop : "_id"
};

module.exports = schema;