var schema = {
    title : '${name}',
    info:"Check the status of each automated report tracking your content.",
    default_schema:true,
    listView:{
        title: function(i){
            return `<joe-subtext>${i.id || ''}</joe-subtext>
            <joe-title>${i.name}</joe-title>
            <joe-subtitle>${i.info}</joe-subtitle>`
        },
        listWindowTitle: 'Reports'
    },
    menuicon:'<svg title="report" xmlns="http://www.w3.org/2000/svg" viewBox="-10 -10 120 120"><path d="M49.5 31.7l1.8-12.2A12.4 12.4 0 1 0 61.6 29Z"/><rect x="29" y="57" width="40" height="3"/><rect x="29" y="65" width="40" height="3"/><rect x="29" y="73" width="40" height="3"/><path d="M20 10V90H80V10ZM76 86H24V14H76Z"/></svg>',
    methods:{
        renderPreviews:function(report){
                var examples = [];
                var html ='';
                var ref0;
                var it0;
                report.content_items.map(function(ci){
                    if(_joe.Data[ci.itemtype]){
                        examples = examples.concat(_joe.Data[ci.itemtype]);
                        it0 = ci.itemtype;
                        ref0 = ci.reference;
                    }
                })
                var finalExamples = [];

                while(finalExamples.length < 5 && finalExamples.length < examples.length){
                    let ci = examples[Math.floor(Math.random()*examples.length)];
                    if(finalExamples.indexOf(ci) == -1){
                        finalExamples.push(ci);
                    }
                }
                var rep_url;
                finalExamples.map(function(ex){
                    rep_url = '/API/plugin/reportbuilder'+'?reportid='+report._id+'&'+ref0+'='+ex._id;
                    html += _joe.renderFieldListItem(ex,
                    '<joe-subtext>'+it0+'</joe-subtext>'+
                    '<joe-title>${name} ${date}</joe-title>\
                    <joe-subtitle>${info}</joe-subtitle>',
                    it0,
                    {
                        action:'onclick="window.open(\''+rep_url+'\');"',
                        icon:'report'
                    }   
                );
                })

                return html;
        }
    },
    subsets:function(i){
        var sets = 
            _joe.Filter.Options.getDatasetPropertyValues('report','template_type',{
                group:'template type'
            })
        return sets;
    },
    fields:[
        'name',
        'info',
        
        {name:'content_items',type:'objectList',
            properties:['itemtype','reference']
        },
        {section_start:'Template'},
            'template_type',
            {extend:'template',specs:{
                comment:'<div>${this.REPORT.name} ${this.CONTENT} <br/></div><div>for <b>module</b>: export function(data) that returns an html string. data.request, data.REPORT, <br/>shorthand: $J.get(itemid) $J.schema(schemaname)</div>',
                language:function(item){
                    switch(item.template_type){
                        case 'module':
                            return 'javascript';
                        break;
                        default:
                            return '';
                    }
                    
                }
            }},
        {section_end:'Template'},
        {section_start:'preview'},
            {name:'examples', type:'content',run:function(item){
                return $J.schema('report').methods.renderPreviews(item);
            }
            /*run:function(item){
                var examples = [];
                var html ='';
                var ref0;
                var it0;
                item.content_items.map(function(ci){
                    if(_joe.Data[ci.itemtype]){
                        examples = examples.concat(_joe.Data[ci.itemtype]);
                        it0 = ci.itemtype;
                        ref0 = ci.reference;
                    }
                })
                var finalExamples = [];

                while(finalExamples.length < 5 && finalExamples.length < examples.length){
                    let ci = examples[Math.floor(Math.random()*examples.length)];
                    finalExamples.push(ci);
                }
                var rep_url;
                finalExamples.map(function(ex){
                    rep_url = '/API/plugin/reportbuilder'+'?reportid='+item._id+'&'+ref0+'='+ex._id;
                    html += _joe.renderFieldListItem(ex,
                    '<joe-subtext>'+it0+'</joe-subtext>'+
                    '<joe-title>${name} ${date}</joe-title>\
                    <joe-subtitle>${info}</joe-subtitle>',
                    it0,
                    {
                        action:'onclick="window.open(\''+rep_url+'\');"',
                        icon:'report'
                    }   
                );
                })

                return html;
            }*/
        },
/*            {name:'report_preview',type:'preview',bodycontent:function(report){
                var temp = report.template;
                var payload = {
                    REPORT:{name:report.name,info:report.info},
                    CONTENT:report.content_item
                    
                };
                return fillTemplate(temp,payload);  
            }},*/
        {section_end:'preview'},
        {sidebar_start:'right',collapsed:true},
        'updated',
        {name:'id',comment:'short code to find this report',callback:function(val,ev){
            ev.target.value = _joe.Utils.cleanString(val);
        },validation:function(val){
            return false;}
        },
            {section_start:'advanced'},
                
                {name:'use_htmlframework', type:'boolean',display:'html framework',label:'use standard head and body tags'},
                {label:'includes'},

                'includes',
                {name:'createInclude', type:'create',schema:'include',label:false},
                {label:'access'},
                '_protected',
            {section_end:'advanced'},
        {sidebar_end:'right'},
        {section_start:'system',collapsed:true},
        '_id','created','itemtype',
        {section_end:'system'}
    ],
    idprop : "_id"
};

module.exports = schema;