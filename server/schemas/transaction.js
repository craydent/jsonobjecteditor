var schema = {
    title : '${name}',
    info:"A transaction is a debit or credit associated with a specific financial account or budget",
    listView:{
        title: function(item){
            var amount = parseFloat(item.amount || 0);
            var occurences = '';
            if(item.recurrence == "irregular" && item.occurences && item.occurences.length){
                item.occurences.map(function(rec){
                    amount += parseFloat(rec.amount);
                })
                occurences = ""+item.occurences.length+" irregular occurences";
            }
            return '<joe-full-right>$'
                +amount.toFixed(2)
                +'<joe-subtext>${RUN[_joe.getDataItemProp;${tags};\'tag\']}</joe-subtext>'
                +'<joe-subtext>${budget}</joe-subtext>'
                +'<joe-subtext>${recurrence}</joe-subtext>'
                +'</joe-full-right>'
                +'<joe-title>${name}</joe-title>'
                +'<joe-content>${info}</joe-content>'
                +'<joe-subtext>${start_date} > ${end_date}</joe-subtext>'
                +'<joe-subtitle>'+(occurences||item.next)
                +'</joe-subtitle>';
        },
        listWindowTitle: 'Transactions'
    },
    menuicon:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-10 -10 120 120"><defs><clipPath><path class="cls-1" d="M0 0V100H100V0ZM50 75A25 25 0 1 1 75 50 25 25 0 0 1 50 75Z"/></clipPath></defs><polygon points="28.2 89.3 50 89.3 50 78.7 28.2 78.7 28.2 71 13.9 84 28.2 97 28.2 89.3"/><polygon points="71.8 10.7 50 10.7 50 21.3 71.8 21.3 71.8 29 86.1 16 71.8 3 71.8 10.7"/><path d="M50 27.5A22.5 22.5 0 1 0 72.5 50 22.5 22.5 0 0 0 50 27.5Zm2 33.8V65H48V61.5a16.8 16.8 0 0 1-6.2-1.6l1.1-4.3A13 13 0 0 0 49 57.2c2.1 0 3.5-0.8 3.5-2.3s-1.2-2.3-3.9-3.2C44.7 50.4 41.9 48.6 41.9 45c0-3.2 2.1-5.8 6.1-6.5V35h4v3.2a15 15 0 0 1 5.3 1.2l-1.1 4.2a12.3 12.3 0 0 0-5.3-1.2c-2.4 0-3.2 1-3.2 2.1 0 1.2 1.3 2 4.4 3.2 4.4 1.5 6.3 3.6 6.3 6.9S56 60.5 52 61.2Z"/></svg>',
    searchables:['name','info','_id'],
    subsets:function(){
        var subs=[
            {name:'Paycheck',bgcolor:'#00ffbf',filter:{paycheck:true}},
            {name:'Bill',stripecolor:'orangered',filter:{transaction_type:'debit'}},
            {name:'Income',stripecolor:'limegreen',filter:{transaction_type:'credit'}},
            {name:'OneTime',filter:{recurrence:{$in:['onetime']}}},
            {name:'Recurring',filter:{recurrence:{$in:['weekly','biweekly','monthly','yearly']}}},
            {name:'Irregular',filter:{recurrence:'irregular'}}

        ];
        // var years = ['2020','2019','2018','2017','2016'];
        // years.map(function(year){
        //     sets.push({name:year,filter:{__year:year}})
        // });
        return subs;
    },
    onload:function(transactions){
        transactions.map(function(trans){
            trans.__year = 0;
            if(trans.next){
                trans.next_int = trans.next.toDateTime({format:'Ymd'})
            }
            if(trans.recurrence == "irregular" && trans.occurences && trans.occurences.length){
                //trans.__year = trans.transaction_date.substr(-4);
            }else{
                trans.__year = trans.transaction_date.substr(-4);
            }
            
        })
        console.log();
    },
    /*headerMenu:function(){return [_joe.buttons.next,_joe.buttons.previous]},*/
    sorter:['name',{name:'updated',value:'!joeUpdated'}, {name:'date',value:'next'},{name:'amount',value:'!amount'},'recurrence'],
    filters:function(){
        var filters = _joe.Filter.Options.tags({group:true,untagged:true,collapsed:true});
        return filters
    },
    stripeColor:function(item){

        if(item.transaction_type =="credit"){
            return {title:'credit',color:'limegreen'};
        }
        else if(item.transaction_type =="debit"){
            return {title:'debit',color:'orangered'};
        }
        return '';
    },
    bgColor:function(item){
        if(item.paycheck){
            return {title:'paycheck',color:'#00ffbf'};
        }
    },
    /*aggregator:function(list){
        var totals = {
            amount:0,
            count:0,
            transactions:0
        }

        list.map(item=>{
            totals.amount += parseFloat(item.amount || 0);
            if(item.recurrence == "irregular" && item.occurences && item.occurences.length){
                item.occurences.map(function(rec){
                    totals.amount += (parseFloat(rec.amount)||0);
                    totals.transactions++;
                })
            }else{
                totals.transactions++;
            }

            totals.count++;
        })
        var html= `
        <joe-menu-label>Aggregating ${totals.count} transactions</joe-menu-label>
            <joe-content-section> 
            total amount <b>${formatMoney(totals.amount)}</b>
        </joe-content-section>`;
        



        return html;
    },*/
    fields:[
        'name',
        'info',
        {name:'budget', type:'select',values:'budget',blank:true,width:'50%', idprop:"name"},
        'tags',
        {section_start:'Accounts'},
            {name:'account_from',display:'from',type:'select',idprop:'_id',values:'financial_account',width:'50%',value:function(){
                var da =_joe.Data.financial_account.where({default_account:true})[0];
                if(da){
                    return da._id;
                }
                return null;
            },blank:true},
            {name:'account_to',display:'to',type:'select',idprop:'_id',values:'financial_account',width:'50%',blank:true},
            
        {section_end:'Accounts'},
        {section_start:'Frequency'},
        {name:'recurrence', width:'50%',
            type:'select',values:['onetime','irregular','weekly','biweekly','monthly','yearly'],
            rerender:'transaction_date,next,occurences,start_date,end_date'
        },
        {name:'transaction_date', width:'50%',rerender:'next',
            type:function(item){
            switch(item.recurrence){
                case 'monthly':
                    return 'number';   
                case 'weekly':
                    return 'select';
                default:
                    return 'date';
                  
            }
        },values:function(item){
            switch(item.recurrence){

                case 'weekly':
                    return [{name:"0",display:'sunday'},
                    {name:"1",display:'monday'},
                    {name:"2",display:'tuesday'},
                    {name:"3",display:'wednesday'},
                    {name:"4",display:'thursday'},
                    {name:"5",display:'friday'},
                    {name:"6",display:'saturday'}
                    ];
                default:
                    return 'date';
            }
        },hidden:function(item){
            return item.recurrence == 'irregular';
        }},
        {
            name:'start_date',
            type:'date',
            width:'50%',
            hidden:function(item){
                return ['weekly','biweekly','monthly','yearly'].indexOf(item.recurrence) == -1;
            }
        },
        {
            name:'end_date',
            type:'date',
            width:'50%',
            hidden:function(item){
                return ['weekly','biweekly','monthly','yearly'].indexOf(item.recurrence) == -1;
            }
        },
        {name:'next',type:'content',width:'50%',
            run:function(item){
                var built = _joe.Object.construct();
                var today = new Date();
                var nextdate = '';
                var recurrence = built.recurrence || item.recurrence;
                var transaction_date = built.transaction_date || item.transaction_date;


                switch(recurrence){
                    case 'onetime':
                        nextdate = transaction_date;
                        break;
                    case 'irregular':
                        nextdate = ((item.occurences||[]).sortBy('!date')[0]||{date:'none'}).date;
                        break;
                    case 'weekly':
                        var seeddate = transaction_date.contains('/')?0:transaction_date;
                        if(today.getDay() == seeddate){
                            nextdate = today;
                        }else{
                            var next = new Date();
                            next.setHours(24*(seeddate-next.getDay()));
                            if(next < today){
                                next.setHours(24*(7));
                            }
                            nextdate = next;
                        }
                        break;
                    case 'biweekly':
                        nextdate = transaction_date;
                        if(new Date(transaction_date) < today){
                            nextdate = new Date(transaction_date);
                            nextdate.setHours(24*14);
                        }

                        break;
                    case 'monthly':
                         nextdate = new Date();
                        var seeddate = transaction_date.contains('/')?1:transaction_date;
                        nextdate.setDate(seeddate);
                         if(transaction_date < today.getDate()){
                            nextdate.setMonth(today.getMonth()+1);

                         }
                        break;
                    default:
                        nextdate = recurrence+' - '+transaction_date;
                        break;
                }
                var datestr = new Date(nextdate);
                var finaldatestr = (datestr.getMonth()+1)+'/'+datestr.getDate()+'/'+datestr.getFullYear();
                _joe.current.object.next = finaldatestr;
                return finaldatestr
                    +'<joe-button onclick="_joe.Fields.rerender(\'next\')" class="joe-fright">'+
                    'refresh</joe-button>';
            },
            passthrough:true},
        {name:'next_int',passthough:true,width:'50%',locked:true},

        {name:'occurences',type:'objectList',
        template:function(occurence){
            /*if(_joe.sizeClass == "large-size"){return false;}*/
            var temp ='';
            if(occurence.info != ''){
                temp+='<joe-subtext>${info}</joe-subtext>';
            }
            temp+='<joe-title>${date} - <joe-bigger>$${amount}</joe-bigger></joe-title>';
                
            return temp;
        },
            properties:[
                {name:'date',type:'date', width:'140px'},
                
                {name:'amount',type:'number',width:'100px'},
                {name:'info'}
            ],
            sortable:false,

            hidden:function(item){
                return item.recurrence != 'irregular';
            }
        },
        {section_end:'Frequency'},
        {section_start:'Amount'},
        {name:'amount',display:'$ amount',type:'number', width:'40%',step:'0.01'},

        {name:'transaction_type',type:'select',width:'30%',display:'Debit/Credit',values:['debit','credit']},
        {name:'variable_amount',type:'boolean',width:'30%'},
        {section_end:'Amount'},
        'description',
        {sidebar_start:'right',collapsed:function(){
            return !_joe.isNewItem();
        }},
        'updated',
            {section_start:'adv'},
                {name:'autopay',type:'boolean'},
                {name:'paycheck',type:'boolean'},
            {section_end:'adv'},
            '_protected',
        {sidebar_end:'right'},
        {section_start:'system',collapsed:true},
        '_id','created','itemtype',
        {section_end:'system'}
    ],
    idprop : "_id"
};

module.exports = schema;