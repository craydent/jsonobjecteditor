var schema = {
    title : 'Action | ${name}',
    listView:{
        title: '<joe-title>${info}</joe-title><joe-subtitle>${session}</joe-subtitle><joe-subtitle>${visitor}</joe-subtitle>',
        listWindowTitle: 'Actions'
    },
    fields:[
        'visitor',
        'session',
        'info',
        'description',
        '_id','created','itemtype'
    ],
    idprop : "_id"
};

module.exports = schema;