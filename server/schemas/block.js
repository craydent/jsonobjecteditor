var block = {
    info:"A block is a small chunk of reusable content, which can be customized when it is placed in a page or layout.",
    sorter:['name',{value:'!is_template',display:'template'}],
    subsets:function(){
        var subs = [];
        var templates = _joe.Data.block.where({is_template:true}).sortBy('name');
        subs.push({name:'Templates',filter:{is_template:true},stripeColor:'goldenrod'})
        
        subs.push({group_start:'Templated'});
        templates.map(function(temp){
            subs.push({name:temp.name,filter:{use_template:temp._id}});
        });
        subs.push({group_end:'Templated'});

        subs.push({group_start:'sites'});
        _joe.Data.site.sortBy('name').map(function(site){
            subs.push({name:site.name,filter:{site:site._id}});
        });
        subs.push({group_end:'sites'});
        return subs;
    },
    _title: '${name}',
    _listWindowTitle: 'Blocks',
    menuicon:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-160 -160 640 640"><g data-name="paper square"><path d="M10 10V310H310V10ZM295 295H25V25H295Z"/><path d="M33 33V157H157V33ZM150 150H40V40H150Z"/><path d="M163 33V157H287V33ZM280 150H170V40H280Z"/><path d="M163 163V287H287V163ZM280 280H170V170H280Z"/><path d="M33 163V287H157V163ZM150 280H40V170H150Z"/><path d="M231 49V89h40V49Zm35 35H236V54h30Z"/><path d="M179 49V89h40V49Zm35 35H184V54h30Z"/><path d="M231 101v40h40V101Zm35 35H236V106h30Z"/><path d="M179 101v40h40V101Zm35 35H184V106h30Z"/></g></svg>',
    stripeColor:function(obj){
        if(obj.is_template){
            return {title:'block template',color:'goldenrod'};
        }
        return '';
    },
    methods:{

    },
    fields:function(block){
        function notBlockTemplate(item){

                if(item.is_template || !item.use_template){
                    
                    return false;
                }

                return true;
            //    return !item.use_template;
        }
        
        var block_template = _joe.getDataItem(block.use_template,'block');
        var bt_fields = [];
            if(block_template){
                //console.warn(block_template.block_fields)
                (block_template.block_fields || []).map(function(f){
                    
                    var fspecs = {};
                    try{
                        if(f.specs){
                            fspecs = eval('('+f.specs+')');
                        }
                    }catch(e){
                        logit('error with block field specs '+ f.field_name);
                    }
                    var fieldType = f.field_type;
                    if(fieldType == "objectreference"){
                        fieldType = "objectReference";
                    }else if(fieldType == "objectlist"){
                        fieldType = "objectList";
                    }
                    bt_fields.push({extend:f.field_name,
                        specs: $.extend({type: fieldType},fspecs)
                    });
                });
            }
        var fieldset = [
        //'display',
        'name','info',
        {extend:'description',specs:{type:'rendering', height:'100px'}},

        {section_start:'Template',collapsed:function(item){
            return (item.content && item.content.length);
        }},
            /*{name:'use_template',type:'boolean',display:'advanced mode',rerender:'template',label:'use your own template instead of configuration',width:'50%'},*/
            
            {name:'use_template',type:'select',display:'block template', rerender:'template,block_fields',comment:'use a block template or build a custom block <br/> use ${this.BLOCK_TEMPLATE} to reference template',width:'50%',goto:'block',idprop:'value',values:function(item){
                var options = [{display:'custom',value:''}];
                _joe.Data.block.where({is_template:true}).sortBy('name').map(function(t){
                    options.push({name:t.name,value:t._id})
                });
                return options;
            }},
            {name:'is_template',type:'boolean',display:'use as template',label:'use this block as a template for other blocks',width:'50%', rerender:'template,block_fields'},
            {name:'template_type',display:'template type',type:'select',values:['code','module'],comment:'use code for standard js content <br/> module to export a js function(data) that returns a string.'},
            {extend:'template',specs:{
                height:'500px',
                comment:'the template for this block:<br/>to reference self use ${this.BLOCK.[property]}',
                hidden:notBlockTemplate}
            },
            {label:'Fields',condition:function(block){
                return block.is_template;
            }},
            {
                name:'block_fields',
                display:'Block Fields',
                comment:'Values to use to populate blocks from other blocks',
                type:'objectList',
                hidden:notBlockTemplate,
                properties:[
                    {name:'field_name',display:'Name'},
                    {name:'field_type',display:'Field Type', type:'select',default:'text',
                        values:function(){
                            if(window.__joeFieldTypes){
                                return window.__joeFieldTypes;
                            }
                            var fieldtypes = []
                            for(var a in _joe.Fields){
                                if(typeof _joe.Fields[a] != 'function'){
                                    if(a == "objectlist"){
                                        a = "objectList";
                                    }else if(a == "objectReference"){
                                        a = "objectReference";
                                    }
                                    fieldtypes.push(a);
                                }
                            }
                            window.__joeFieldTypes = fieldtypes.sort();
                            return window.__joeFieldTypes;
                        }},
                    {name:'specs',type:'rendering',display:'Addt\'l Specs'}
                ]
            },
        {section_end:'Template'},
        {section_start:'TemplateContent',collapsed:function(item){return !item.content;}},
        {name:'content_type',type:'select',values:['wysiwyg','code','module'], rerender:'content'},
        //TODO:implement plugins in blocks
        {extend:'content',specs:{
            height:'500px',
            hidden:function(item){
                return item.content_type =='plugin';
            },
            comment:'only used when block is using a template <br/>reference as ${this.BLOCK.content} <br/> use ${this.BLOCK_TEMPLATE} to reference template',
            //'${this.DATA.dataset_name}<br/>\also available:PAGE,LAYOUT,SITE,JOEPATH,DATA,WEBCONFIG',
            type:function(item){
                if(!item.content_type){
                    return 'code';
                }
                if(["code","module"].indexOf(item.content_type) != -1){
                    return 'code';
                }
                return item.content_type;
            }
        }},
        {label:'block template fields, refresh to update',hidden:function(block){
            return !block.use_template;
        }}]

        fieldset = fieldset.concat( bt_fields,[
        {section_end:'TemplateContent'},
        {section_start:'Methods', collapsed:function(item){
            return !item.process_functions;
        }},
        'process_functions',
        {section_end:'Methods'},

        {sidebar_start:'right', collapsed:false},
        'updated',
        'site',
        {section_start:'includes'},
        'includes',
        {name:'createInclude', type:'create',schema:'include',label:false},
        {section_end:'includes'},
        {section_start:'usage'},
            {name:'block_usages',type:'content',display:'items using this block',
            run:function(item){
                var html = '';
                function returnInstances(itemtype,check){
                    var check = check || function(ins){
                        if(ins.blocks){
                            if(ins.blocks.where({block:item._id}).length){
                                return true;
                            }
                        }
                    };
                    var instances = _joe.Data[itemtype].filter(check)
                    html += '<joe-content>'+instances.length+' '+itemtype+(instances.length != 1 && 's'||'')+'</joe-content>';
                    instances.map(function(i){
                        html += _joe.renderFieldListItem(i,null,itemtype);
                    });
                }
                returnInstances('layout');
                html+='<hr/>';
                returnInstances('page');
                html+='<hr/>';
                returnInstances('block',function(ins){
                    return ins.use_template == item._id;
                });
                return html;
            }},
        {section_end:'usage'},
        // {section_start:'Info'},
        // 'block_type',
        // {name:'isDefault',type:'boolean',display:'Default Block',label:'use this on new pages?'},

        // {section_end:'Info'},
        {sidebar_end:'right'},
        //'_id','created','itemtype'
    ]);
    return fieldset.concat(__systemFieldsSection);
    },
    idprop: '_id',
    listTitle:function(item){
        var t='<joe-title>${name}</joe-title><joe-subtitle>${info}</joe-subtitle>';
        if(item.site){
            t= '<joe-subtext>['+_joe.Cache.get(item.site).name+']</joe-subtext>'+t;
        }
        if(item.use_template){
            t+='<joe-subtext>'+_joe.Cache.get(item.use_template).name+'</joe-subtext>';
        }
        return t;
    },

};
module.exports = block;
