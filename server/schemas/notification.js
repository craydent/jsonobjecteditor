var notification ={
    info:"A notification is an automated message, sent when an action is taken (ex: new subscriber email). A notification can be an email, sms, or system.",
    default_schema:true,
    title: '${name}',
    fields:[
        'name',
        'info',
        
        {name:'notification_type',type:'select',values:['email'],width:'50%'},
        {name:'notification_code',width:'50%'},
        'from',
        'to',
        'dataset',
        'subject',
        'content',
        'text:rendering',
        '_id','created','itemtype'
    ],

    idprop: '_id',
    listView:{
        listWindowTitle: 'Notifications',
                title: '<joe-title>${name}</joe-title><joe-subtitle>${info}</joe-subtitle>'+
        '<joe-subtext>${subject}</joe-subtext>'+
        '<joe-subtext style="color:orangered;">${notification_code}</joe-subtext>',
    },
    menuicon:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-8 -8 112 112"><path d="M48 13.1c-17.7 0-32 13-32 29.1S30.3 71.3 48 71.3V82.9s32-15.6 32-40.7C80 26.1 65.7 13.1 48 13.1ZM44 24.4h7v9.3L49.6 46.4H46.3L44 33.6Zm8 35H44v-8h8Z"/></svg>'
};

module.exports = notification;