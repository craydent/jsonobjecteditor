//layout//
var layout ={
    title: '${name}',
    info:"A layout is a standard, reusable page template for your site.",
    methods:{
        getSections:function(template_str,add_blank){
            var sections=[];
            (template_str.match(/\$\{this.SECTION.[a-zA-Z0-9\-\_]*}/g)||[]).map(function(mt){
                sections.push(mt.replace('${this.SECTION.','').replace('}',''));
            });
            if(add_blank){
                sections = [''].concat(sections);
            }
            return sections;
        }
    },
    subsets:function(){
      var subs = [{group_start:'sites'}];
        _joe.Data.site.sortBy('name').map(function(site){
            subs.push({name:site.name,filter:{site:site._id}});
        });
        subs.push({group_end:'sites'});
        return subs;
    },
   // listWindowTitle: 'Layouts',
    hideNumbers:true,

    fields:[
        //'display',
        'name',
        'info',
        {section_label:'Template', section_start:'template_info'},

        {extend:'template',specs:{
            comment:'${INCLUDES} - html string of all includes<br/>${this.PAGE},${this.LAYOUT},${this.SITE},${this.SECTION[section_name]}'
        }},
        {section_end:'template_info'},
                 
        {section_label:'Blocks', section_start:'blocks_info'},
        {name:'createBlock', type:'create',schema:'block',display:'crate new blocks', width:'40%'},
        'blocks',
        {section_end:'blocks_info'},
        {section_start:'Fields',collapsed:true},
            {
                name:'layout_fields',
                display:'Layout Fields',
                comment:'Values to use to populate layout from page',
                type:'objectList',
                properties:[
                    {name:'field_name',display:'Name'},
                    {name:'field_type',display:'Field Type', type:'select',default:'text',
                        values:function(){
                            if(window.__joeFieldTypes){
                                return window.__joeFieldTypes;
                            }
                            var fieldtypes = []
                            for(var a in _joe.Fields){
                                if(typeof _joe.Fields[a] != 'function'){
                                    fieldtypes.push(a);
                                }
                            }
                            window.__joeFieldTypes = fieldtypes.sort();
                            return window.__joeFieldTypes;
                        }},
                    {name:'specs',type:'rendering',display:'Addt\'l Specs'}
                ]
            },
        {section_end:'Fields'},

        {sidebar_start:'right',collapsed:true},
            'updated',
            'site',
            {section_start:'inc'},
            {name:'createInclude', type:'create',schema:'include',label:false},
            'includes',
            {section_end:'inc'},
            {section_start:'usage',collapsed:true},
            {name:'layout_usages',type:'content',icon:'page',display:function(item){
                var pages = _joe.Data.page.where({layout:item._id});
                return ('<joe-title>'+pages.length
                +(pages.length == 1 && ' page uses'||' pages use')
                +' this layout</joe-title>');
            },
            run:function(item){
                var pages = _joe.Data.page.where({layout:item._id});
                var html = '';
                pages.map(function(page){
                    html += _joe.renderFieldListItem(page,null,'page');
                });
                return html;
            }},
            {section_end:'usage'},
        {sidebar_end:'right'},
        {section_start:'system',collapsed:true},
        '_id','created','itemtype',
        {section_end:'system'}
    ],
    idprop: '_id',
    sorter:['name','site'],
    listView:{
        title: function(layout){
            var t = '<joe-title>${name}</joe-title><joe-subtitle>${info}</joe-subtitle>';
            if(layout.site){
                var s = _joe.Cache.get(layout.site);
                if(s){
                    t= '<joe-subtext>['+s.name+']</joe-subtext>'+t;
                }
            }
            var pages = _joe.Data.page.where({layout:layout._id});
            t+='<joe-subtext><b>'+pages.length+'</b> page'+((pages.length == 1)?' uses':'s use')+' this layout</joe-subtext>'
            return t;
        },
        listWindowTitle: 'Layouts'
    },
    menuicon:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-16 -16 82 82"><path d="M5 4C4.4 4 4 4.4 4 5L4 15C4 15.6 4.4 16 5 16L45 16C45.6 16 46 15.6 46 15L46 5C46 4.4 45.6 4 45 4L5 4zM5 18C4.4 18 4 18.4 4 19L4 45C4 45.6 4.4 46 5 46L29 46C29.6 46 30 45.6 30 45L30 19C30 18.4 29.6 18 29 18L5 18zM33 18C32.4 18 32 18.4 32 19L32 25C32 25.6 32.4 26 33 26L45 26C45.6 26 46 25.6 46 25L46 19C46 18.4 45.6 18 45 18L33 18zM33 28C32.4 28 32 28.4 32 29L32 35C32 35.6 32.4 36 33 36L45 36C45.6 36 46 35.6 46 35L46 29C46 28.4 45.6 28 45 28L33 28zM33 38C32.4 38 32 38.4 32 39L32 45C32 45.6 32.4 46 33 46L45 46C45.6 46 46 45.6 46 45L46 39C46 38.4 45.6 38 45 38L33 38z"/></svg>'
    //listMenu: function () {return uoListMenu('layout');}
};

module.exports = layout;