
var schema = {
    title : 'Schema | ${name}',
    info:'[development] Schemas define all the objects of JOE, they are stored locally with the source files and go with your git deployment.',
    listWindowTitle: 'Schemas',
    idprop:'_id',
    menuicon:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-130 -130 750 750"><path d="M442.7 296.8c-9.8-1.8-18.6-6.4-25.6-12.8l-60.2 98.1c-2.8-0.7-5.6-1-8.6-1 -20.7 0-37.6 16.8-37.6 37.6 0 20.7 16.8 37.6 37.6 37.6 20.7 0 37.6-16.8 37.6-37.6 0-7.3-2.1-14-5.7-19.8L442.7 296.8zM348.3 432.6c-7.7 0-13.9-6.2-13.9-13.9 0-7.7 6.2-13.9 13.9-13.9 7.7 0 13.9 6.2 13.9 13.9C362.3 426.3 356 432.6 348.3 432.6z"/><path d="M75 244.9c0-10.2-4.1-19.5-10.7-26.2l59.7-97.5c-9.3-3.1-17.4-8.8-23.6-16.1l-62.6 102.4c-0.1 0-0.2 0-0.3 0 -20.7-0.1-37.5 16.8-37.5 37.5s16.8 37.5 37.6 37.5c0.1 0 0.1 0 0.2 0l62.6 102.3c6.2-7.4 14.3-13 23.6-16.1l-59.7-97.5C70.9 264.4 75 255.2 75 244.9zM37.6 258.8c-7.7 0-13.9-6.2-13.9-13.9 -0.1-7.6 6.1-13.9 13.9-13.9 7.7 0 13.9 6.2 13.9 13.9C51.5 252.6 45.3 258.8 37.6 258.8z"/><path d="M295.7 417.3c0-3.7 0.4-7.3 1.1-10.9h-120c-0.9-2.9-2.1-5.7-3.6-8.3l62-101.3c-9.8-1.8-18.6-6.4-25.6-12.8l-59.3 96.9c-3-0.8-6.1-1.2-9.4-1.2 -20.7 0-37.6 16.8-37.6 37.6 0 0.2 0 0.5 0 0.7s0 0.5 0 0.7c0 20.7 16.8 37.5 37.6 37.5 14.9 0 27.7-8.7 33.8-21.2h124.1C296.8 429.5 295.7 423.5 295.7 417.3zM140.9 431.1c-7.5 0-13.5-5.9-13.9-13.2 0.4-7.4 6.4-13.2 13.9-13.2s13.5 5.9 13.9 13.2C154.4 425.3 148.4 431.1 140.9 431.1z"/><path d="M452.4 207.5c-14.3 0-26.8 8-33.1 19.8H294.6c2 5.5 3.1 11.5 3.1 17.7 0 3.7-0.4 7.3-1.1 10.9h119.9c4.7 15.4 19 26.7 35.9 26.7 20.7 0 37.6-16.8 37.6-37.6C490 224.3 473.2 207.5 452.4 207.5zM452.4 259c-7.7 0-13.9-6.2-13.9-13.9 0-7.7 6.2-13.9 13.9-13.9s13.9 6.2 13.9 13.9C466.4 252.7 460.1 259 452.4 259z"/><path d="M296.7 82.3c-0.7-3.5-1.1-7.1-1.1-10.9 0-6.2 1.1-12.2 3.1-17.7H174c-6.3-11.8-18.8-19.8-33.1-19.8 -20.7 0-37.6 16.8-37.6 37.6 0 20.6 16.8 37.4 37.6 37.4 3 0 5.8-0.4 8.6-1l60.1 98.2c7.1-6.4 15.9-11 25.6-12.8l-62.5-102.1c1.7-2.8 3.1-5.8 4.1-9v0.1H296.7zM140.9 85.2c-7.7 0-13.9-6.2-13.9-13.9 -0.1-7.6 6.2-13.9 13.9-13.9 7.7 0 13.9 6.2 13.9 13.9S148.6 85.2 140.9 85.2z"/><path d="M282.4 245.1c0-10.2-4.1-19.5-10.7-26.2l59.7-97.5c-9.2-3.2-17.3-8.9-23.5-16.2l-62.6 102.3c-0.1 0-0.2 0-0.3 0 -17 0-31.3 11.3-35.9 26.7h-120c0.7 3.5 1.1 7.1 1.1 10.9 0 6.2-1.1 12.2-3.1 17.7h124.8c6.3 11.8 18.8 19.8 33.1 19.8 0.1 0 0.1 0 0.2 0l62.6 102.3c6.2-7.4 14.3-13 23.6-16.1l-59.7-97.5C278.3 264.5 282.4 255.3 282.4 245.1zM245 258.9c-7.7 0-13.9-6.2-13.9-13.9 0-7.7 6.2-13.9 13.9-13.9s13.9 6.2 13.9 13.9C259 252.7 252.7 258.9 245 258.9z"/><path d="M380.1 91.2c3.6-5.8 5.7-12.5 5.7-19.8 0-20.7-16.8-37.6-37.6-37.6 -20.7 0-37.6 16.8-37.6 37.6 0 20.7 16.8 37.6 37.6 37.6 3.1-0.1 6-0.4 8.7-1.1l60.1 98.2c7.1-6.4 15.9-11 25.6-12.8L380.1 91.2zM348.3 85.3c-7.7 0-13.9-6.2-13.9-13.9 0-7.7 6.2-13.9 13.9-13.9 7.7 0 13.9 6.2 13.9 13.9C362.2 79.1 356 85.3 348.3 85.3z"/></svg>',
    //hideNumbers:true,
    fields:[
        'name',
        'title',
        'info',
        'menuicon',
        {name:'idprop',default:'_id'},
        {section_start:'list'},
        {name:'listTitle',type:'code',language:'js',comment:'Template for the joe list item.'},
        'listWindowTitle',
        'stripeColor',
        'bgColor',
        'sorter',
        'filters',
        'subsets',
        'hideAllSubset',
        'itemMenu',
        'onDemandExpander:boolean',
        {name:'itemExpander',type:'code',language:'js'},
        'checkbox',
        'colCount',
        'columns',
        
        'listMenu',

        'hideNumbers',
        'checkChanges',
        'onload:code',
        'onsave:code',
        {section_end:'list'},
        {section_start:'object'},
            {name:'fields',type:'code',language:'js'},
            'menu',
        {section_end:'object'},
        {sidebar_start:'right',collapsed:true},
            {section_start:'adv'},
                'listView_action_listAction',
            {section_end:'adv'},
        {sidebar_end:'right'},
        {section_start:'joe'},
        'documentTitle',
        {name:'onPanelShow',type:'code',language:'js'},
        {section_end:'joe'},








        '_id','created','itemtype'
    ]
};

module.exports = schema;