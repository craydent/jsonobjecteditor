var schema = {
    title : '${name}',
    menuinfo:"Organize users into a group with shared goals and specify leaders. Can also be used to protect all group items",
    default_schema:true,
    menuicon:'<svg xmlns="http://www.w3.org/2000/svg" viewBox="-10 -10 46 46"><path d="M9 1C7.3 1 6 2.3 6 4 6 5.7 7.3 7 9 7 10.7 7 12 5.7 12 4 12 2.3 10.7 1 9 1zM9 7C8.7 7 8.4 7 8.1 7.1 8.7 7.9 9 8.9 9 10 9 10.4 8.9 10.8 8.9 11.2 9.7 10 11 9.2 12.5 9 11.8 7.8 10.5 7 9 7zM17 1C15.3 1 14 2.3 14 4 14 5.7 15.3 7 17 7 18.7 7 20 5.7 20 4 20 2.3 18.7 1 17 1zM17 7C15.5 7 14.2 7.8 13.5 9 15 9.2 16.3 10 17.1 11.2 17.1 10.8 17 10.4 17 10 17 8.9 17.3 7.9 17.9 7.1 17.6 7 17.3 7 17 7zM4 7C2.3 7 1 8.3 1 10 1 11.7 2.3 13 4 13 5.7 13 7 11.7 7 10 7 8.3 5.7 7 4 7zM4 13C1.8 13 0 14.8 0 17L0 19.2C0 19.2 1 20 4 20 7 20 8 19.2 8 19.2L8 17C8 14.8 6.2 13 4 13zM22 7C20.3 7 19 8.3 19 10 19 11.7 20.3 13 22 13 23.7 13 25 11.7 25 10 25 8.3 23.7 7 22 7zM22 13C19.8 13 18 14.8 18 17L18 19.2C18 19.2 19 20 22 20 25 20 26 19.2 26 19.2L26 17C26 14.8 24.2 13 22 13zM13 11C11.3 11 10 12.3 10 14 10 15.7 11.3 17 13 17 14.7 17 16 15.7 16 14 16 12.3 14.7 11 13 11zM13 17C10.8 17 9 18.8 9 21L9 23.2C9 23.2 10 24 13 24 16 24 17 23.2 17 23.2L17 21C17 18.8 15.2 17 13 17z"/></svg>',
    listView:{
        title: 
        '<joe-full-right><joe-subtext>${code}</joe-subtext></joe-full-right>'+
        '<joe-subtext>${group_type}</joe-subtext>'+
        '<joe-title>${name}</joe-title><joe-subtitle>${info}</joe-subtitle><joe-content>${leader}</joe-content>',
        listWindowTitle: 'Groups'
    },
    fields:[
        'name',
        'info',
        'leader',
        'code',
        'description',
        {section_start:'members'},
        {extend:'members',specs:{width:'50%'}},
        {extend:'people',specs:{width:'50%'}},
        {section_end:'members'},

        {sidebar_start:'right'},
        'reports',
        {name:'group_type',display:"Group Type",
            autocomplete:{text:true},
            values:function(i){
                //get list list types
                var types = [];
                for(var l=0;l<_joe.Data.group.length;l++){
                    let lt = _joe.Data.group[l].group_type;
                    if(lt){
                        types.push(lt);
                    }
                }
                types = Array.from(new Set(types));
                return types;
            }},
        'tags',
        '_protected',
        {sidebar_end:'right'},

        '_id','created','itemtype'
    ],
    idprop : "_id"
};

module.exports = schema;