var joewebconfig = {
    setGlobal:true,
    name:'JOE',
    clusters:0,
    // port:2099,
    // socketPort:2098,
    // sitesPort:2100,
    PORT:2099,
    openWindow:true,
    DEBUG_MODE:true,
    joedb:'127.0.0.1/joe_core',
    joepath:'/JsonObjectEditor/',
    deleted:'_deleted',
    hostname:'localhost',
    filesDir:'_files',
    webDir:'_www',
    dataDir:'_datasets',
    schemaDir:'_schemas',
    appsDir:'_apps',
    pluginsDir:'_plugins',
    templatesDir:'_templates',
    //httpsPort:2100,
    default_schemas:['user','group','goal','initiative','event','report','tag','status','workflow','list','notification','note','include','instance','setting']
};


module.exports = joewebconfig;