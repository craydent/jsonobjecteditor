var ProjectBoard = function(){ 
    this.title='Projects';
    this.description="When you have an entire team working together toward goals, the best way to keep track of everything is to put it all in one place. That's what projectboard does. It is a project management tool that allows you to create teams, assign tasks to users, allocate resources, and really prioritize your project goals. If you're using JOE to it's maximum capacity, your team of users works together every day to grow your organization. Now you can set clear goals, assign the tasks related to them, and get to work with clear, shared expectations.",
    this.collections=['task','project','input','reward','location']
        .concat(JOE.webconfig.default_schemas);
    this.dashboard = [
         {
            type:'Card',
            config:{
                title:'Stats Overview',
                content:function(card){
                    // var addbtn = '<joe-button class="joe-button joe-svg-button joe-orange-button joe-capp-button" \
                    //            onclick="_joe.Object.create(\'task\')">'+
                    //            _joe.schemas.task.menuicon+'create <b>TASK</b></joe-button>';
                    var addbtn = `<joe-button action="new" color="orange" icon="plus" schema="task"></joe-button>`;
                    addbtn += `<joe-button action="new" color="orange" icon="plus" schema="instance"></joe-button>`;
                    var html = 
                    /*'<joe-title>There are '+(_joe.Data.task||[]).where({complete:{$nin:['true',true]}}).length+' active tasks beings tracked</joe-title>'+*/

                    '<capp-chart id="chart_'+card.cappid+'"></capp-chart>';
                    html+='<hr/>'+addbtn;
                    return html;
                },
                 onload:function(bk){
                    capp && capp.Chart.byStatus('task',bk.capp,{filter:{complete:{$nin:['true',true]}}});
                },
                
                left:0,
                cssclass:'w3 h3'
            }
        },
        {
            type:'Card',
            config:{
                title:'My Work',
                content:function(){
                    if(!_joe.User){
                        return 'no user logged in';
                    }
                    var tasks = (_joe.Data && _joe.Data.task ||[]).where({complete:false}).sortBy('!joeUpdated');
                    var instances = (_joe.Data && _joe.Data.instance ||[]).where({
                        instance_type:{$in:['task','reward']},
                        approved:{$ne:true}
                    }).sortBy('!joeUpdated');
                    var projects = {};
                    var projects_content='';
                    var tasks_content='',task_template;
                    var tcount = 0,pcount = 0, pending_count = instances.length;
                    var pending_content = '';
                    instances.map(i=>{
                        
                        pending_content+=_joe.Render.fieldListItem(i,'','instance');
                       
                    })
                    
                    tasks.map(function(task){

                        if(task.project){
                            projects[task.project] = projects[task.project] ||{tasks:[],updated:''};
                            if(task.joeUpdated > projects[task.project].updated){
                                projects[task.project].updated = task.joeUpdated;
                            }
                            if(!task.complete){
                                    projects[task.project].tasks.push(task);
                            }
                        }
                        if(_joe.schemas.task.methods.assigned(task)){
                            if(!task.status){
                                return;
                            }
                            else{
                                var s = _joe.Cache.get(task.status);
                                if(s.inactive || s.terminal || s.default){
                                    return;
                                }
                            }
                            var tproject,phaseinfo = '',phasename='';
                            if(task.project){
                                tproject = _joe.getDataItem(task.project,"project");
                                if(task.project_phase){
                                    phase = tproject.phases.where({id:task.project_phase})[0]||{name:''};
                                    phasename = (phase.name)?' > '+phase.name:'';
                                }
                                phaseinfo = '<joe-subtext>'+(tproject.name||'')+phasename+' </joe-subtext>' 
                            }
                            task_template = _joe.schemas.task.menuicon
                            +phaseinfo
                            +'<joe-title>'+task.name+'</joe-title><joe-subtitle>'
                            +((task.subtasks && task.subtasks.length && task.subtasks.length+' subtasks')||'')
                            +' </joe-subtitle>';
                            
                            tasks_content+=_joe.Render.fieldListItem(task,task_template,'task',{
                                bgcolor:_joe.schemas.task.bgColor,
                                checkbox:_joe.schemas.task.checkbox
                            });
                            tcount++;
                        }
                    });
                    var project_array = [];
                    var project;
                    for(var p in projects){
                        project = _joe.getDataItem(p,'project');
                        project_array.push($.extend(projects[p],project));
                    }
                    var onclick,template;
                    
                    project_array.sortBy('!updated').map(function(project){
                        if(project._id && project.members && (project.members.indexOf(_joe.User._id) != -1)){
                            if(project.status){
                                var s = _joe.Cache.get(project.status);
                                if(s.inactive || s.terminal || s.default){
                                    return;
                                }
                                template = _joe.schemas.project.menuicon+'<joe-title>'+project.name+'</joe-title><joe-subtitle>'
                                +((project.tasks && project.tasks.length)||0)+' open tasks</joe-subtitle>';

                                projects_content+=_joe.Render.fieldListItem(project,template,'project',{
                                    stripecolor:_joe.schemas.project.bgColor,
                                    bgcolor:_joe.schemas.project.bgColor
                                });
                                pcount++;
                            }
                        }
                    });
                    var active = ['','',''];
                    if(pending_count){
                        active[2] = "active";
                    }else if(tcount){
                        active[1] = "active";
                    }else{
                        active[0] = "active";
                    }
                    var togglers = 
                        `<capp-toggles>
                        <capp-button class="`+active[0]+`" data-toggle="projects" onclick="capp.Toggle.toggle(\'projects\')">`+_joe.schemas.project.menuicon+`Projects <small>(`+pcount+`)</small></capp-button>
                        <capp-button class="`+active[1]+`" data-toggle="tasks" onclick="capp.Toggle.toggle(\'tasks\')">`+_joe.schemas.task.menuicon+`Tasks <small>(`+tcount+`)</small></capp-button>
                        <capp-button class="`+active[2]+`" data-toggle="pending" onclick="capp.Toggle.toggle(\'pending\')">`+_joe.schemas.instance.menuicon+`Pending <small>(`+pending_count+`)</small></capp-button>
                        </capp-toggles>`;
                    

                    var html =togglers+ `<capp-toggled>
                    <capp-content class="`+active[0]+`" data-toggle="projects">`+projects_content+`</capp-content>   
                    <capp-content class="`+active[1]+`" data-toggle="tasks">`+tasks_content+`</capp-content>   
                    <capp-content class="`+active[2]+`" data-toggle="pending">`+pending_content+`</capp-content>`;       
                    return html;
                },
                left:3,
                cssclass:'w3 h4'
            }
        },
        JOE.Apps.Cards.recentlyUpdated(
            {
                cssclass:'w2 h3',
                left:6,
                top:0,
                title:"Recently Updated Tasks",
            },
            {schemas:['task'],limit:10,stripeOnly:true}
        ),
        JOE.Apps.Cards.appHome({left:8,top:0,cssclass:'w2 h2'}),
        JOE.Apps.Cards.systemStats({top:3,left:0})
    ]
    return this;
}
module.exports = new ProjectBoard();

