

var App = {
    title:'Questionnaire',
    description:"Maybe you want to survey potential customers to find out about a new service. Or maybe you just want to build a subscription form for your email database. Whatever the case, questionnaire lets you build forms that can be embedded anywhere, and all of the data you collect is stored in JOE.",
    collections:['visitor','question','form','submission','notification'].concat(JOE.webconfig.default_schemas),
    // plugins:['submissionhandler.js'],
    dashboard:[
        JOE.Apps.Cards.appHome(),
        JOE.Apps.Cards.systemStats({left:3})
    ]
}

module.exports = App;