var Church = function(){ 

    this.title ='Church';
    this.description = "A successful church is a huge organization, touching the lives of hundreds of families on a daily basis. But most churches are run by a small staff and just a few volunteers. The church app gives you everything you need to run a church seamlessly: a website builder, a membership database, project manaagement software... Your church may not have the biggest team, but it will be the best organized. And that makes all the difference. ";
    this.collections=['visitor','question','form','submission','notification']
        .concat(JOE.webconfig.default_schemas);
            this.dashboard = [
                JOE.Apps.Cards.appHome(),
                JOE.Apps.Cards.systemStats({left:3})
            ];
    return this;
}
module.exports = new Church();

