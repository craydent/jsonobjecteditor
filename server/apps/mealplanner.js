var Church = function(){ 

    this.title ='Meals';
    this.description = "Add ingredients, recipes and meals to make nutrition and shopping easy.";
    this.collections=['recipe','ingredient','meal','list']
        .concat(JOE.webconfig.default_schemas);
            this.dashboard = [
                {
                    type:'Card',
                    config:{
                        title:'Quick Add',
                        content:function(card){
        
                            var addbtn = `<joe-button action="new" color="orange" icon="plus" schema="ingredient"></joe-button>`;
                            addbtn += `<joe-button action="new" color="orange" icon="plus" schema="list"></joe-button>`;
                            var html = 
        
        
                            '<capp-chart id="chart_'+card.cappid+'"></capp-chart>';
                            html+='<hr/>'+addbtn;
                            return html;
                        },
                         onload:function(bk){
                            capp && capp.Chart.byStatus('task',bk.capp,{filter:{complete:{$nin:['true',true]}}});
                        },
                        
                        left:3,
                        cssclass:'w2 h1'
                    }
                },
                JOE.Apps.Cards.appHome({left:0,top:0,cssclass:'w3 h4'}),
                JOE.Apps.Cards.systemStats({top:1,left:3})
            ];

    return this;
}
module.exports = new Church();

