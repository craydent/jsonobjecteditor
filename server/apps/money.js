var App = function(){ 

    this.title='Money';
    this.description = "There is no better place to keep track of your finances than in the platform you run your business with. Why? Every piece of data that you've created can filter through this budget, so that if someone buys something on your site it's automatically logged. You can set budgeting goals, track individual transactions, and even pull transactions from your bank account. The money app gives you an opportunity to keep all of your goals in one place: JOE.";
    this.collections=['ledger','transaction','budget','financial_account'].concat(JOE.webconfig.default_schemas);
    this.onServerLoadComplete = function(){
        /*alert('money loaded');*/
        _joe.Data.ledger.map(function(ledger){
            _joe.schemas.ledger.methods.update(ledger);
        })
        if(_joe.current.schema && _joe.current.schema.name == 'ledger'){
            _joe.reload(true);
        }

    }
    this.dashboard=[
        {type:'Card',
                    config:{
                        left:3,
                        title:'Active Ledgers',
                        cssclass:'w2 h2',
                        content:function(){
                            var html = '';
                            var today_date = new Date();
                            var month = (today_date.getMonth()+1);
                            var day = today_date.getDate();
                            var year = today_date.getUTCFullYear().toString();
                            var date_str = ((month.length == 2)?month:'0'+month)+'/'+((day.toString().length == 2)?day:'0'+parseInt(day))+'/'+year;

                            var ledgers = (_joe.Data && _joe.Data.ledger||[]).filter(function(led){
                                if((new Date(led.start_date).toISOString() <= _joe.nowISO) 
                                && (new Date(led.end_date).toISOString() >= _joe.times.SOD)){
                                    return true;
                                }
                                // if(led.start_date <= date_str && led.end_date >= date_str ){
                                //     return true;
                                // }
                                return false;
                            })
                            
                            //html +='date: '+date_str+': '+ledgers.length;
                            var onclick;
                            /*ledgers.map(function(led){
                                var paycheckname = _joe.getDataItemProp(led.paycheck,'transaction');
                                var title = '<joe-title>'+(led.name && led.name ||paycheckname)+'</joe-title>';
                                onclick='onclick="goJoe(_joe.search(\''+led._id+'\')[0],{schema:\'ledger\'})"';
                                html+='<capp-button '+onclick+'>'+
                                '<capp-button-icon>'+_joe.schemas.ledger.menuicon+'</capp-button-icon>'+
                                '<joe-title>'+title+'</joe-title>'+
                                '<capp-content>'+led.start_date+' - '+led.end_date+'</capp-content>' +
                                '</capp-button>';
                            })*/
                            ledgers.sortBy('!start_date').map(function(led){
                                var paycheckname = _joe.getDataItemProp(led.paycheck,'transaction');
                                var template = 
                                 _joe.schemas.ledger.menuicon+
                                '<joe-title>'+(led.name && led.name ||paycheckname)+'</joe-title>'+
                                '<joe-subtext>'+led.start_date+' - '+led.end_date+'</joe-subtext>'+
                                '<joe-subtitle>ending balance <b>'+formatMoney(led.ending_balance)+'</b></joe-subtitle>'; 
                                
                                html+=_joe.Render.fieldListItem(led,template,'ledger',
                            {stripecolor:_joe.schemas.ledger.stripeColor});

                            })
                            return (html || 'no active ledgers');
                        }
                    }
        },
//_joe.Data.ledger.where({end_date:{$gte:'07/29/2016'},start_date:{$lte:'07/29/2016'}})
        JOE.Apps.Cards.quickAdd('transaction,ledger',{top:2,left:3,cssclass:'w2'}),
        JOE.Apps.Cards.appHome(),
        JOE.Apps.Cards.systemStats({left:3,top:3})
    ]


    return this;
}
module.exports = new App();