var App = {
    title:'VEM',
        //info:'the visitor experience manager is where you can get the data to track the visitor content',
    description:"What happens when you follow each and every visitor who comes across your site and watch what they're up to?Suddenly you can provide them valuable feedback, that matches their interests exactly.The Visitor Experience Manager is your tool to find out what device a customer most often visits your website from, their location, how often they visit, and what they look at. This means you can hand-tailor content according to their interests, and create reports to find out how succesful your efforts have been. Basically, this app ensures that you know exactly who your customers are, and what they're looking for.",
    collections:['visitor','session','touchpoint','site','location','device'].concat(JOE.webconfig.default_schemas),
    dashboard:[
        
        JOE.Apps.Cards.appHome(),
        JOE.Apps.Cards.systemStats({left:3})
    ]
}


module.exports = App;