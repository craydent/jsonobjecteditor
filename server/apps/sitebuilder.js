var App = function(){ 
    this.title ='Site Builder';
    this.description = "Want to build a website that maximizes your Craydent Platform and reaches your organizational outreach goals? Sitebuilder is a traditional content management system. You can use it to build templates for an entire site, single pages of content, blog posts, and more.  What makes sitebuilder unique is that it is powered by your entire Craydent Platform, and can include your data sets from anywhere in JOE. You can easily make pages that pull together your datasets, like an online profile database of all of your members, or a series of reports. You can store your files in a local directory, if you'd like, keeping your platform functionality and your entire site in one place.";
    this.collections=['site','page','post','layout','block','include','form','question','submission'].concat(JOE.webconfig.default_schemas);

    this.dashboard=[
        {
            type:'Card',
            config: {
                left:3,
                top:0,
                title: 'Sites at a Glance',
                content: function () {
                    try {
                        var h = '',path;
                        _joe.Data.site.map(function(s){
                            path = '//'+location.hostname+':'+__jsc.sitesPort+'/'+ s.url+'/';
                            h +='<capp-button onclick="goJoe(_joe.search(\''+s._id+'\')[0],{schema:\'site\'})">'+
                            '<joe-title>'+ s.name+'</joe-title>'+
                            '<joe-subtitle>'+path+'</joe-subtitle>'
                               // + '<iframe class="site-view" src="'+path+'"></iframe>'
                                +'</capp-button>';
                        });
                        return h;
                    } catch (e) {
                        return 'loading';
                    }
                },
                cssclass: 'w2 h2'
            }
        },
        /*{
            type:'Card',
            config: {
                title: 'Sites at a Glance',
                content: function () {
                    try {
                        var h = '',path;
                        _joe.Data.site.map(function(s){
                            path = '//'+location.hostname+':'+__jsc.sitesPort+'/'+ s.url+'/';
                            h +='<div onclick="goJoe(_joe.search(\''+s._id+'\')[0],{schema:\'site\'})">'+
                            '<joe-title>'+ s.name+'</joe-title>'+
                            '<joe-subtitle>'+path+'</joe-subtitle>'
                                + '<iframe class="site-view" src="'+path+'"></iframe>'
                                +'</div>';
                        });
                        return h;
                    } catch (e) {
                        return 'loading';
                    }
                },
                cssclass: 'w3 h5'
            }
        },*/
        JOE.Apps.Cards.appHome(),
        JOE.Apps.Cards.systemStats({top:2,left:3})
    ];
    return this;
}
module.exports = new App();