var Members = function(){ 
    this.title = 'Members';
    this.description = "Think about every account you have online. Your Amazon account, your Netflix account, even your online bank account. What do they all have in common? All of the information you view within your account is specifically hand-tailored for you. Suggestions for you. Billing information for you. Suddenly everyone you're trying to reach is logging on to your website to grab valuable content made specifically for them. Anyone who has an account with you is now a member of your organization. You can create an online directory, publicly listing all of your members. You can offer content exclusively to members, and allow them to see specials and pages that the general public can't access.";
    this.collections = ['member','form','question'].concat(JOE.webconfig.default_schemas);
    this.dashboard = [
        JOE.Apps.Cards.appHome(),
        JOE.Apps.Cards.systemStats({left:3})
    ]
    return this;
}
module.exports = new Members();