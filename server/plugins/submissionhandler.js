function SubmissionHandler(){
    var self = this;
    this.default = function(data,req){
        var fid =data.formid;
        if(!fid){
            return {errors:'no form id',failedat:'submissionhandler'};
        }
        var form = JOE.Cache.findByID('form',fid);
        var source = req.headers.referer || req.headers.host;
        
        var payload = {
            source:source,
            //headers:req.headers,
            data:data,
            message:'Form Submitted',
            formname:form.name
        };
        if(!form){
            return {errors:'form not found',failedat:'submissionhandler'};
        }
        if(form.server_action){
            try{
                var server_action = eval('('+form.server_action+')');
                var tpayload = server_action(data,form,req); 
                payload = tpayload;
            }catch(e){
                return {errors:'plugin error: '+e,failedat:'plugin'};
            }
        }

 
       
        
        return payload;
    }
    return self; 
}
module.exports = new SubmissionHandler();