function MemberRegisty(){
    var self = this;
    this.default = function(data,req){
        try{ 
            var payload = {
                params:req.params,
                data:data
            }
        }catch(e){
            return {errors:'plugin error: '+e,failedat:'plugin'};
        }
 
       
        
        return payload;
    };
    this.search = function(data,req){
        var query = data.query || {};
        var sortby = data.sortby  || 'name';
        var fields = data.fields || null;
        var makeSet = data.makeSet || false;
        if(makeSet === "true"){makeSet = true;}
        try{ 
            if(typeof query != 'object'){
                query = eval('('+query+')');
            }

            var members = JOE.Data.member.where(query).sortBy(sortby);
            var payload = {
                data:data,
                //reqQ:req.query,
                query:query,
                count:members.length,
                members:members
            }
            if(fields){
                payload.members = JOE.Utils.ProjectedItemsFromFields(fields,payload.members,makeSet);
                return payload;
            }
            return payload;
        }catch(e){
            return {errors:'plugin error: '+e,failedat:'plugin',e:e};
        }
    };
    var state_lookups = {
        'nm':'NM',
        'new mexico':'NM',
        
        'tx':'TX',
        'texas':'TX',
        
        'az':'AZ',
        'arizona':'AZ',
        'colorado':'CO'
        
        
    }
    this.getLocationList = function(data,req){
        try{ 
            var llQuery = {
                "active": {"$in": [true,"true"]},
                "profession.0": {
                    "$exists": true
                },
                itemtype:'member'
            };
            var members = JOE.Cache.list.where(llQuery)
            var loqs = {}
            members.map(m=>{
                if(!m.state){return;}
                let stname = state_lookups[m.state.trim().toLowerCase()] || m.state;
                //get state, add to obj
                loqs[stname] = loqs[stname] ||{cities:[]};
                loqs[stname].cities.push(m.city.trim().toLowerCase());
            });
            Object.keys(loqs).map(ck=>{
                loqs[ck].cities = Array.from(new Set(loqs[ck].cities)).sort();
                loqs[ck].count = loqs[ck].cities.length;
            })
            //var loqs = JOE.Utils.ProjectedItemsFromFields('city',members,true).sort();
            //JOE.Data.member.where(query)
            var payload = {
                data:data,
                //reqQ:req.query,
                query:llQuery,
                count:loqs.length,
                //members:members,
                loqs
            }

            return payload;
        }catch(e){
            return {errors:'plugin error: '+e,failedat:'plugin',e:e};
        }

    }
    this.update = function(data,req,res){
        try{ 
            var updates = data.updates;
            var username = data.username || req.cookies.jmr_username;
            var token = data.token || req.cookies.jmr_token;
            if(!username || !token){
                console.log(req);
                return 'Please select a user profile or return to <a href="directory">the directory</a>'+username+' | '+token;
            }
            var user = JOE.Data.member.where({name:username,token:token})[0];
            
            if(!user){
                return {'error':'user not found'};
            } 
            if(!updates){
                return {'error':'updates not specified'};
            }
            updates.password = user.password;
            console.log(updates);
            JOE.Storage.save(updates,'member',function(err,data){
                if(err){
                    res.jsonp({error:err});
                }else{
                    var payload = {message:'update successful',updates:updates};
                    res.jsonp(payload);
                }
            });

            return {use_callback:true};
        }catch(e){
            return {errors:'plugin error: '+e,failedat:'plugin'};
        }
    }
    this.profile = function(data,req){
        try{ 

            var username = data.name;
            var layout = data.layout;
            var remote = data.remote;
            if(!username){
                return 'Please select a user profile or return to <a href="directory">the directory</a>';
            }
            
            var user = JOE.Data.member.where({name:username})[0];
            if(layout){
                layout = JOE.Data.layout.where({_id:layout})[0]||null;
            }
            if(!user){
                return {'error':'user not found'};
            }
            user = $c.merge({},user);
            delete user.password;
            delete user.token;

            var payload = {
                USER:user,
                LAYOUT:layout
            };
            if(layout){
                var response = fillTemplate(layout.template,payload);
                if(remote){
                    return {response:response,user:user};
                }else{
                    return response;
                }
            }
            return payload;
        }catch(e){
            return {errors:'plugin error: '+e,failedat:'plugin'};
        }


    }
    this.autologin = function(data,req){
        var username = data.username || req.cookies.jmr_username;
        var token = data.token || req.cookies.jmr_token;
        
        try{ 
            if(!username || !token){
                return {'error':'invalid request'};
            }

            var user = JOE.Data.member.where({name:username,token:token})[0];
            if(!user){
                return {'error':'user not found',cookies:req.cookies};
            }
            user = $c.merge({},user);
            delete user.password;
            var payload = {
                user:user
            }
            return payload;
        }catch(e){
            return {errors:'plugin error: '+e,failedat:'plugin'};
        }
    }
    this.login = function(data,req){
        //takes a username and password/token,
        //returns userinfo without password and JOE stuff
        var username = data.username;// || (req.cookies && req.cookies.jmr_username);
        var password = data.password;
        var token = data.token;// ||(req.cookies && req.cookies.jmr_token);
        var email = data.email;
        var query = {};
        try{ 
            if((!username && !email) || (!password && !token)){
                return {'error':'invalid request'};
            }
            if(username){query.name =username; }
            if(email){query.email =email; }
            if(password){query.password =password; }
            if(token){query.token =token; }

            var user = JOE.Data.member.where(query)[0];
            if(!user){
                return {'error':'user not found',query:query};
            }
            user = $c.merge({},user);
            delete user.password;
            var payload = {
                cookies:req.cookies,
                user:user,
                query:query
            }
            return payload;
        }catch(e){
            return {errors:'plugin error: '+e,failedat:'plugin'};
        }
    }
    this.schema = function(){
        var member_schema = JOE.Schemas.schema['member'];
        //payload[s] = JOE.Schemas.load(s);
        //stringFunctions(member_schema);
        return {schema:member_schema,fields:JOE.Fields['core']||{error:'fields not found'}};
        
    }


    //this.resetTokens = {}; 



    this.requestPasswordReset = function(data,req,res){
        JOE.Cache.resetTokens = JOE.Cache.resetTokens || {}; 
        var email = data.email.toLowerCase();
        
        var member = JOE.Data.member.where({email:email})[0]||false;
        if(!member){
            return {error:'no member found'}
        }

        var reset_token = self.createResetToken(member);
        //send email
        if(JOE.Cache.settings.PASSWORD_RESET_URL){
            JOE.Apps.plugins.notifier.sendEmail(
                JOE.webconfig.name+' Password Reset',
            '<a href="'+JOE.Cache.settings.PASSWORD_RESET_URL+'?reset_token='+reset_token+'&email='+email+'">'+
            (data.email_message || JOE.Cache.settings.PASSWORD_RESET_MESSAGE || "Click this link to reset your "+JOE.webconfig.name+" password. ")
            +'</a>',
            'noreply@nmtia.net',
            email,
            {},
            function(err,data){
                if(err){
                    res.jsonp({error:err});
                    return; 
                }
                data.email = email;
                res.jsonp(data);
                return true; 
            }
            )
            return {use_callback:true}
        }
        return {error:'no PASSWORD_RESET_URL setting'}
    }
    this.createResetToken = function(member){
        var key = member.email.toLowerCase();
        var token = cuid();
        if(JOE.Cache.resetTokens[key]){
            delete JOE.Cache.resetTokens[key];
        }

        JOE.Cache.resetTokens[key] = {
            member_id:member._id,
            timestamp:new Date().toISOString(),
            email:member.email,
            token:token
        }
        return token;
        
    }
    this.listResetTokens = function(data,req){
        return JOE.Cache.resetTokens;
    }
    this.resetPassword = function(data,req,res){
        var reset_token = data.reset_token;
        var password = data.password;
        var email = data.email.toLowerCase();
        if(!reset_token || !password  || !email){
            return {error:'invalid reset request'}
        }
        JOE.Cache.resetTokens = JOE.Cache.resetTokens || {};
        var token = JOE.Cache.resetTokens[email];
        if(!token || (token.token != reset_token)){
            return {error:'invalid reset token '+ reset_token}
        }
        var member = JOE.Data.member.where({
            email:email
        })[0]||false;
        if(!member){
            return {error:'invalid member'}
        }

        var member = $c.merge(member,
        {password:password,joeUpdated:new Date().toISOString()});

        JOE.Storage.save(member,'member',function(err,data){
            if(!err){
                console.log(member.password);
                res.jsonp({status:'successful'});
                delete JOE.Cache.resetTokens[email];
            }else{
               res.jsonp({error:err}); 
            }
        },{user:{name:'memberRegistry'}})
        return {use_callback:true}
    }
    this.protected = ['resetTokens','createResetToken','listResetTokens'];
    return self; 
}
module.exports = new MemberRegisty();