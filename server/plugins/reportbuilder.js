function ReportBuilder(){
    var self = this;
    this.default = function(data,req,response){
        var rBM = new Benchmarker();
        var reportid =data.reportid;
        if(!reportid){
            return {errors:'no report id',failedat:'reportbuilder'};
        }
        var report = JOE.Cache.findByID('report',reportid);
        var source = req.headers.referer || req.headers.host;
        
        var payload = {
            source:source,
            data:data,
            report:report
        };
        var item,itemid,schema;
        if(data.itemid){
                //var reportName = data.name;
                itemid = data.itemid;
                item = JOE.Cache.findByID(itemid)||false;
                schema = JOE.Schemas.raw_schemas[item.itemtype];
        }
        if(!report && reportid){
            var posReps = JOE.Cache.search({itemtype:'report',id:reportid})[0];
            if(posReps){
                report = posReps;
                payload.report = posReps;
            }
            
        }
        if(!report){

            if(!item){
                return {errors:'report not found (1)',failedat:'reportbuilder'};
            }else{
                if(schema.reports && schema.reports[reportid]){
                    report = schema.reports[reportid];
                    report.format = 'json';
                    console.log(report);
                    if(!report){
                        return {errors:`report not found (3)`,failedat:'reportbuilder'};
                    }
                }else{
                    return {errors:`report not found (${reportid})`,failedat:'reportbuilder'};
                }
            }
            
        }
        if(!CheckAuth(null,report,req,response)){
            return {unauthorized:true};
        }
        try{
            var webconfig = Object.assign({},JOE.webconfig);
            delete webconfig.authorization;
            delete webconfig.joedb;
            var report_includes = '';
            if(report.includes){
                report_includes = getIncludes(report);
            }
            var template_data = {
                REPORT:report,
                WEBCONFIG:webconfig,
                INCLUDE_STRING:report_includes,
                request:{
                    query:req.query,
                    params:req.params
                },
                response
            }
            //logit('webconfig',webconfig);
            var c_obj,item;
            if(report.content_items){
                report.content_items.map(function(content){
                    if(data[content.reference]){
                        c_obj = JOE.Cache.findByID(content.itemtype,data[content.reference])||{name:content.reference+' not found'};
                        template_data[content.reference] = c_obj;
                    }
                })
                if(report.content_items.length == 1 && c_obj){
                    item = c_obj;
                }
            }
            //console.log('content items done.')
            //if template, get items and populate
            template_data.TIME = rBM.stop();
            var payload;
            if(report.template){
               // console.log(report.template_type);
                switch(report.template_type){
                    case "module":
                        if(report.format == "json"){
                            try{
                                var modFunc = report.template;
                                payload = modFunc(Object.assign(template_data,{ITEM:item}),{renderHTMLFramework:renderHTMLFramework});
                                if(payload && payload.use_callback){
                                    return payload;
                                }
                            }catch(e){
                                console.error(e);
                                return {errors:e}
                            }
                        }else{
                            try{
                                var modFunc = JOE.Utils.requireFromString(report.template,report._id);
                                payload = modFunc(template_data,{renderHTMLFramework:renderHTMLFramework});
                                if(payload && payload.use_callback){
                                    return payload;
                                }
                            }catch(e){
                                console.error(e);
                                return {errors:e}
                            }
                        }
                    break;
                    case "code":
                    case "wysiwyg":
                    default:
                        var template = report.template;
                        if(template.indexOf("function") == 0){
                            var f = tryEval(template);
                            if(f){
                                template = f(item,template_data);
                            }
                            
                        }
                        //template_data.JOE = JOE;
                        payload = fillTemplate(template,template_data);
                    break;
                }
            }
            logit(JOE.Utils.color('[reportbuilder]','plugin')+' in '+rBM.stop());

            function getIncludes(report){
                var i_html = '';
                report.includes.map(inc=>{
                    let include = $J.get(inc);
                    switch(include.filetype){
                        case 'css':
                                i_html +=`<link rel="stylesheet" href="/_include/${inc}"/>`;
                            break;
                        case 'js':
                                i_html +=`<script src="/_include/${inc}"></script>`;
                            break;
                    }
                })
                return i_html;
            }
            if(report.includes){
                payload = report_includes+payload;
            }
            if(report.use_htmlframework){
                payload = renderHTMLFramework(report,item,payload);
                logit('using html > '+payload.length);
                //+payload+'</body></html>';
            }
        }catch(e){
            console.error(e);
                return e;
        }
        return payload;
    }
    function renderMinimalFramework(){
        
    }
    function renderHTMLFramework(report,item,content,title){
        var title = title || report.name+': '+item.name;
        var html ='<html><head><meta name="viewport" content="initial-scale=1.0, user-scalable=no" />'
        + '<title>'+title+'</title>'
        +'<link rel="stylesheet" href="/JsonObjectEditor/js/plugins/c3/c3.min.css"/>'
        +'<link rel="stylesheet" href="/JsonObjectEditor/capp/capp.css"/>'
        +'<script src="/JsonObjectEditor/_joeinclude.js"></script>'
        +'<script src="/JsonObjectEditor/capp/capp.js"></script>'
        +'<script src="/JsonObjectEditor/js/plugins/c3/d3.v3.min.js"></script>'
        +'<script src="/JsonObjectEditor/js/plugins/c3/c3.min.js"></script>'
        +'<link rel="stylesheet" href="/JsonObjectEditor/css/joe.css" />'
        +'<link rel="stylesheet" href="/JsonObjectEditor/css/report-styles.css" />'
        +'<script src="/JsonObjectEditor/web-components/report-components.js"></script>'
        +'</head><body>';
        if(content){
            html +=content+'</body></html>';
        }
        return html;
    }
    function CheckAuth(item,report,req,res){
        //console.log(req.cookies);
        var username = req.cookies && req.cookies._j_user;
        var user = {};
        if(username){
            user = JOE.Data.user.where({name:req.cookies._j_user,token:req.cookies._j_token})[0]||{};
        }
        var rep_auth = JOE.Utils.propAsFuncOrValue(report._protected,item,null,user._id);
        var item_auth = (item && JOE.Utils.propAsFuncOrValue(item._protected,item,null,user._id))||false;
        var group_auth = false;
        if(item && item.group){
            let group = $J.get(item.group);
            console.log(group);
            group_auth = (group && JOE.Utils.propAsFuncOrValue(group._protected,group,null,user._id))||false;
        }
        //logit('auths: rep-'+rep_auth+' item-'+item_auth);
        if(!rep_auth && !item_auth && !group_auth){
            return true;
        }
        //logit('report requires auth');
        return JOE.isAuthorized(req,res);
    }
    this.renderStandardHeader = function(item,report){
        var schema = JOE.Schemas.raw_schemas[item.itemtype];
        var template = '<joe-report>\
            <report-header>'+
            
            ((schema.menuicon &&'<report-icon>'+schema.menuicon+'</report-icon>')||'')+
            
            `<a href="/JOE/joe#/${item.itemtype}/${item._id}" class="report-link report-block"><joe-subtext>goto</joe-subtext>joe</a>
            <joe-subtext>${this.REPORT.name}</joe-subtext>
            <report-title>${(item.name||item._id||'')}</report-title>
            <report-subtitle>${(item.info||'')}</report-subtitle>
            </report-header>`;
    };

    this.standard = function(data,req,res){
        var webconfig = Object.assign({},JOE.webconfig);
        delete webconfig.authorization;
        delete webconfig.joedb;
        var rBM = new Benchmarker();
        var payload = {
            data:data
        };
        var template_data={
            WEBCONFIG:webconfig
        };
        //var reportName = data.name;
        var itemid = data.itemid;
        var item = JOE.Cache.findByID(itemid)||false;

        if(!item){
            payload.error = "no item found for "+itemid;
            return payload;
        }
        var itemtype = item.itemtype.toUpperCase();
        
        var schema = JOE.Schemas.raw_schemas[item.itemtype];
        var template = '';
        if(!schema || !schema.report){
            payload.error = "no standard report found for schema "+item.itemtype;
            return payload;
        }
        var report = schema.report.standard || schema.report;
        //CHECK AUTH
        if(!CheckAuth(item,report,req,res)){
            return {unauthorized:true};
        }
        template_data[itemtype] = item;
        template_data.TIMESTAMP = rBM.stop();
        template_data.REPORT = report;
        template = JOE.Utils.propAsFuncOrValue(report.template,item,null,template_data);

        var header = renderHTMLFramework(report,item);
        var status = (item.status && JOE.Cache.findByID(item.status)[0]) || false;
        template = '<joe-report>\
            <report-header>'+
            
            ((schema.menuicon &&'<report-icon>'+schema.menuicon+'</report-icon>')||'')+
            
            '<a href="/JOE/joe#/'+item.itemtype+'/'+item._id+'" class="report-link report-block"><joe-subtext>goto</joe-subtext>joe</a>\
            <joe-subtext>${this.REPORT.name}</joe-subtext>\
            <report-title>'+(item.name||item._id||'')+'</report-title>\
            <report-subtitle>'+(item.info||'')+'</report-subtitle>\
            </report-header>'+
            ((status && 
            `<report-status class="report-block" style="background:${status.color}"><span class="joe-subtext">status </span>${status.name}</report-status>` )||'')+
            `<report-content>`+
            template+'</report-content></joe-report>';
        payload = header+fillTemplate(template,template_data);
        payload+='</body></html>';
        logit(JOE.Utils.color('[reportbuilder]','plugin')+' in '+rBM.stop());
        return payload;
    }
    return self; 
}
module.exports = new ReportBuilder();