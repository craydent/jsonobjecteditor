function AWSConnect(){
    var self = this;
this.default = function(data,req,res){
    // Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
var settings_config = tryEval(JOE.Cache.settings.AWS_S3CONFIG)||{};
var config = $c.merge(settings_config);

var filename = data.filename || req.params.filename||cuid();
var directory = data.directory|| req.params.directory || 'uploads';
var Key = data.Key || directory+'/'+filename;

// Set your region for future requests.
AWS.config.update(config);

var ACL = data.ACL || 'public-read';
var Bucket = JOE.Cache.settings.AWS_BUCKETNAME;
var file = data.file || 'body';
// Create a bucket using bound parameters and put something in it.
var s3Params = {
    Bucket: Bucket,
    Key:Key,
    Body:file
};

if(data.base64){
    //var buf = new Buffer(data.base64.replace(/^data:image\/\w+;base64,/, ""),'base64');
    var buf = new Buffer(data.base64.replace(/^data:[a-zA-Z0-9\_\-]*\/[a-zA-Z0-9\_\-]*;base64,/, ""),'base64');
    s3Params.Body = buf;
    s3Params.ContentEncoding = 'base64';
    s3Params.ContentType = data.extension || 'image/jpeg';
    
}

var s3bucket = new AWS.S3(s3Params);
var payload = {
    params:s3Params,
    config:config,
    bucket:Bucket,
    Key:Key,
    reqdata:data,
    body:req.body
}
var response = {
    Key:Key,
    bucket:Bucket
}
    s3Params.ACL = ACL || 'public-read';
    s3bucket.putObject(s3Params, function(err, data) {
        response.data = data;
        response.error = err;
        res.send(response);

        if (err) {
        console.log("Error uploading data: ", err);
        } else {
        console.log("Successfully uploaded data to "+Key);
        }
        return;
    });
    return {use_callback:true};

    },
    this.html = function(data,req){
        
 
       
        
        return JSON.stringify(self.default(data,req),'','\t\r\n <br/>');
    }
    return self; 
}
module.exports = new AWSConnect();


