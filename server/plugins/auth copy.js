var jwtDecode = require('jwt-decode');
var request = require("request");
function Auth(){
    var self = this;
    this.default = function(data,req,res){
        try{ 
            var payload = {
                params:req.params,
                data:data
            }

        }catch(e){
            return {errors:'plugin error: '+e,failedat:'plugin'};
        }
        
       
        
        return payload;
    };

    this.login = function(data,req,res){
    
        //make post request
        var originalUrl = data.state||'';
        console.log(originalUrl);

        var options = { method: 'POST',
        url: 'https://www.googleapis.com/oauth2/v4/token',
        qs: 
        {
            grant_type: 'authorization_code',
            code: data.code,
            redirect_uri: `${JOE.webconfig.authorization.host}/API/plugin/auth/login`,
            'Content-Type': 'application/x-www-form-urlencoded',
            'client_id':JOE.webconfig.authorization.client_id,
            'client_secret':JOE.webconfig.authorization.client_secret
        },
        headers: 
        {
            'cache-control': 'no-cache',
            Authorization: JOE.webconfig.authorization.header,
            Accept: 'application/json',

        },
            rejectUnauthorized:false,
            json: true 
        };

        request(options, function (error, response, body) {
            if (error){
                res.send(error);
                return;
            }
            if (body.error){
                res.send(body.error);
                return;
            }
            //res.send(body);
            
            //get creds
            var id_token = body.id_token;
            var access_token = body.access_token;
            var user = {};
            var idTokenInfo = jwtDecode(id_token);
            //var accessTokenInfo = jwtDecode(access_token);
            
            //user.username = accessTokenInfo.username.toLowerCase();
            user.email = idTokenInfo.email.toLowerCase();
            var users = (JOE.Data && JOE.Data.user) || [];
            var User = users.where({email:user.email})[0]||false;
            if(User){
                //req.cookies._j_user && req.cookies._j_token
                res.cookie('_j_user',User.name);
                res.cookie('_j_token',User.token);
                
            }else{
                var finalUrl = originalUrl+(originalUrl.indexOf('?') == -1?'?':'&')+'noSSO=true'
                res.redirect(finalUrl);
                return;
            }
        //redirect to home or gotoUrl
            res.redirect(originalUrl || `/JOE/${User.apps[0]}`);
            //res.send(body);
            

            
        });

     
        //return(data);
        return({use_callback:true})
    }
    this.html = function(data,req,res){
        
 
       
        
        return JSON.stringify(self.default(data,req),'','\t\r\n <br/>');
    }
    this.protected = [];
    return self; 
}
module.exports = new Auth();