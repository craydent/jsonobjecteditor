// import { Configuration, PlaidApi, PlaidEnvironments } from 'plaid';
  
function Money() {
  const plaid = require('plaid');
  var self = this;
  var plaidClient;
  var ACCESS_TOKEN = "";
  var PUBLIC_TOKEN = null;

  var ACCESS_TOKENS;

  function initPlaidClient() {
    //console.log('plaidinit',plaidClient);
    if(plaidClient){ return;}
    logit('initializing plaid client')
  try {
    const configuration = new Configuration({
      basePath: PlaidEnvironments.development,
      baseOptions: {
        headers: {
          'PLAID-CLIENT-ID': CLIENT_ID,
          'PLAID-SECRET': SECRET,
        },
      },
    });
    var client_id = JOE.Cache.settings.PLAID_CLIENTID;
    var secret = JOE.Cache.settings.PLAID_SECRET_development;
    var public_key = JOE.Cache.settings.PLAID_PUBLIC_KEY;
    ACCESS_TOKENS = JOE.Cache.settings.PLAID_ACCESS_TOKENS && JSON.parse(JOE.Cache.settings.PLAID_ACCESS_TOKENS);
    //console.log(ACCESS_TOKENS);
    
    configuration.headers={
      'PLAID-CLIENT-ID': client_id,
      'PLAID-SECRET': secret,
    }

    plaidClient = new PlaidApi(configuration);
    return true;
  } catch (e) {
    logit("plaid client init error",e);
    return e;
  }
}

this.LinkTest = async function(data, request, response){
  initPlaidClient();
  const pResponse = await plaidClient.itemPublicTokenExchange({ public_token });
  const access_token = pResponse.data.access_token;
  const accounts_response = await plaidClient.accountsGet({ access_token });
  const accounts = accounts_response.data.accounts;

  response.jsonp({ error: false,accounts });
  return { use_callback: true };
}

 

  this.default = function(data, req, res) {
    res.jsonp({ data: data });
    return { use_callback: true };
  };

  this.html = function(data, req) {
    logit('html route')
    return JSON.stringify(self.default(data, req), "", "\t\r\n <br/>");
  };
  this.protected = [];
  return self;
}
module.exports = new Money();
