function Calendar(){
    var self = this;
    this.default = function(data,req){
        try{ 
            var events = data.events || data.event;
            var payload = {
                params:req.params,
                data:data
            }
        }catch(e){
            return {errors:'plugin error: '+e,failedat:'plugin'};
        }
 
       
        
        return payload;
    };
    
    this.test = function(data,req){
        var ics = "BEGIN:VCALENDAR\
\nVERSION:2.0\
\nPRODID:-//JOE the Json Object Editor\
\nBEGIN:VEVENT\
\nX-WR-CALNAME:JOE Calendar\
\nUID:2073c980-9545-11e6-99f9-791bff9883ed@joe.com\
\nDTSTAMP:20161018T151122Z\
\nDTSTART:201601130T065000\
\nDTEND:20161130T150000\
\nSUMMARY:Bolder Boulder\
\nDESCRIPTION:Annual 10-kilometer run in Boulder, Colorado\
\nLOCATION:Folsom Field, University of Colorado (finish line)\
\nURL:http://www.bolderboulder.com\
\nSTATUS:confirmed\
\nGEO:40.0095;105.2669\
\nCATEGORIES:10k races,Memorial Day Weekend,Boulder CO\
\nEND:VEVENT\
\nEND:VCALENDAR";
        return {content:ics,"content-type":"text/calendar",filename:'test.ics'};
    }
    this.events = function(data,req){
        var events = (data.event || data.events ||'').split(',').condense();
        logit('events',events);
        if(!events || !events.length){
            return {error:'please specify at least one event'};
        }
        var ics = "BEGIN:VCALENDAR\r\nPRODID:-//JOE the Json Object Editor\r\nMETHOD:PUBLISH"
        +"\r\nVERSION:2.0\r\nCALSCALE:GREGORIAN"
        +"\r\nX-WR-TIMEZONE:US/Central";
            ics += getEventICS(events);
            /*
            \nBEGIN:VEVENT\
            \nX-WR-CALNAME:JOE Calendar\
            \nUID:2073c980-9545-11e6-99f9-791bff9883ed@joe.com\
            \nDTSTAMP:20161018T151122Z\
            \nDTSTART:201601130T065000\
            \nDTEND:20161130T150000\
            \nSUMMARY:Bolder Boulder\
            \nDESCRIPTION:Annual 10-kilometer run in Boulder, Colorado\
            \nLOCATION:Folsom Field, University of Colorado (finish line)\
            \nURL:http://www.bolderboulder.com\
            \nSTATUS:confirmed\
            \nGEO:40.0095;105.2669\
            \nCATEGORIES:10k races,Memorial Day Weekend,Boulder CO\
            \nEND:VEVENT\
            */

            ics+= "\r\nEND:VCALENDAR";
            logit(ics);
            if(data.mode && data.mode == "feed"){
                return ics;
            }
        return {content:ics,"content-type":"text/calendar",filename:'test.ics'};
    }
    function formatICSDate(date, isAllDay) {
        var noMsDate = new Date(date.getTime());
        noMsDate.setMilliseconds(0);
        var noMsDateISOString = noMsDate.toISOString();
        var icsDateString = noMsDateISOString.replace(/-/g, '').replace(/:/g, '').replace(/\.000/g, '');
        return isAllDay ? icsDateString.substring(0, icsDateString.indexOf('T')) : icsDateString;
    };
    function getEventICS(eventIDs){
        var events = eventIDs[0] == "all"?JOE.Data.event : JOE.Data.event.where({_id:{$in:eventIDs}});
        logit(events.length+' events',eventIDs);
        var ics ='';
        var orgname = JOE.webconfig.name;
        events.map(function(ev){
            ics +=
            '\r\nBEGIN:VEVENT'
            +'\r\nORGANIZER;CN='+orgname+':MAILTO:joe@craydent.com'
            +'\r\nX-WR-CALNAME:'+orgname+' Calendar'
            +'\r\nUID:'+ev._id+'@jsonobjecteditor.com'
            +'\r\nDTSTAMP:'+formatICSDate(new Date(ev.joeUpdated||ev.created))
            +'\r\nDTSTART:'+formatICSDate(new Date(ev.date+' '+ev.start_time+(ev.timezone_offset||'')))
            +'\r\nDTEND:'+formatICSDate(new Date(ev.date+' '+ev.end_time+(ev.timezone_offset||'')))
            +'\r\nSUMMARY:'+(ev.name||'untitled event')
            +'\r\nDESCRIPTION:'+ev.info+' - '//+ev.description
            +'\r\nEND:VEVENT';
        })
        return ics;
    }
    this.protected = [];
    return self; 
}
module.exports = new Calendar();