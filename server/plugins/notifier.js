var nodemailer = require('nodemailer');
var ses = require('nodemailer-ses-transport');
 
function Notifier(){
    var self = this;
    this.info = {
        info:"This plugin sends notifications via email",
        specs:['notification','recipient']
        
    };

    this.default = function(data,req,res){
        var payload = {};
        //get NOTIFICATION
        var notificationID = data.notification||'';
        
        var notification = JOE.Cache.findByID('notification',notificationID);
        if(!data.notification || !notification){
            return {error:'notification not found'};
        }
        payload.NOTIFICATION = notification;
        //GET recipient
        var recipientID = data.recipient||(data._recipient && data._recipient._id) || '';
        var recipient = data._recipient || JOE.Cache.findByID(notification.dataset,recipientID);
        if(!recipient){
            return {error:'recipient not found'};
        }
        payload.RECIPIENT = recipient;
        // console.log(notificationID);
        // var notification = JOE.Cache.findByID('notification',data.recipient||'');

        var ses_config = tryEval(JOE.Cache.settings.AWS_SESCONFIG)||{};
        ses_config.correctClockSkew =true;
        var transporter = nodemailer.createTransport(ses(ses_config));
        var mailOptions = {
            from: notification.from, // sender address 
            to: fillTemplate(notification.to,payload), // list of receivers
            subject: fillTemplate(notification.subject,payload), // Subject line
            text: fillTemplate(notification.text,payload), // plaintext body
            html: fillTemplate(notification.content,payload) // html body
        };
                

        transporter.sendMail(mailOptions, function(error, info){
            if(error){
                res && res.send(error);
                return console.log(error);
            }
                res && res.send({status:'success',info:info});
            return;
        });
        
        if(res){ 
            return {use_callback:true};
        }else{
            return {data:data};
        }
    }
    this.sendNotification = function(notificationIDs,payload,callback){
        // var notificationID = 'e9761fed-7a50-4c0f-9f59-663a2a955a2a';
        // var payload ={MEMBER:{
        //     first_name:'Corey',
        //     email:'craql@live.com',
        //     name:'craql'
        // }}
        //console.log(typeof notificationIDs);
        if(typeof notificationIDs == "string"){
            notificationIDs = [notificationIDs];
        }
        (notificationIDs ||[]).map(function(notificationID){
            if($c.isCuid(notificationID)){
                var notification = JOE.Cache.findByID('notification',notificationID);
            }else{
                var notification = JOE.Cache.findByID('notification',notificationID,{idprop:'notification_code'});
            }
            if(!notification){
                console.log(JOE.Utils.color('[plugin]','error')+' error: notification not found sendNotification('+notificationID+') ');
                return {error:'notification not found'};
            }
            if(!notification.from){
                return {error:'no sender for notification: '+notification.name};
            }
            if(!notification.to || !notification.to.length){
                return {error:'no recipient for notification: '+notification.name};
            }
            if(notification.notification_type == "email"){
                self.sendEmail(
                    notification.subject,
                    {html:notification.content,text:notification.content},
                    notification.from,
                    notification.to,
                    payload,
                    callback
                )
            }
        })
         
    }
    this.sendEmail = function(subject,content, from, recipient,payload,callback){
        var payload = payload || {};
        var ses_config = tryEval(JOE.Cache.settings.AWS_SESCONFIG)||{};
        ses_config.correctClockSkew =true;
        var transporter = nodemailer.createTransport(ses(ses_config));
        var mailOptions = {
            from: from, // sender address 
            to: fillTemplate(recipient,payload), // list of receivers
            subject: fillTemplate(subject,payload), // Subject line
            text: fillTemplate((content.text || content),payload), // plaintext body
            html: fillTemplate((content.html || content),payload) // html body
        };
        transporter.sendMail(mailOptions, function(error, info){
            if(error){
                
                callback && callback(error);
                return console.log(error);
            }
                callback && callback(null,{status:'success'});
            return;
        });
    }

    this.protected = ['sendEmail','sendNotification'];
    return self; 
}
module.exports = new Notifier();
