function Money(){
    var self = this;
    var plaid = require('plaid');
    var environment = plaid.environments.tartan;
    var plaidClient;

    
    //initPlaidClient();
    
    function initPlaidClient(){
        try{
            var client_id = JOE.Cache.settings.PLAID_CLIENTID;
            var secret = JOE.Cache.settings.PLAID_SECRET;
            plaidClient = new plaid.Client(client_id, secret, environment);
        }catch(e){
            logit('plaid client init error:'+e);
        }
    }
    this.institutions = function(data,req,res){
        if(!plaidClient){
            initPlaidClient();
        }
        var callback = function(err,mfaResponse,response){
            res.jsonp({err:err,mfaResponse:mfaResponse,response:response,environments:plaid.environments});
        };
        if(data['id']){
            plaid.getInstitution(data['id'], environment, callback);
        }else{
            plaid.getInstitutions(environment,callback);
        }
        return {use_callback:true};
    };

    this.authTest = function(data,req,res){
        if(!plaidClient){
            initPlaidClient();
        }
        try{
            var user = data['username']||'plaid_test';
            var pass = data['password']||'plaid_good';
            
            var credentials = {username:user,password:pass}
            var institution_type = 'wells';//wf
            plaidClient.addAuthUser(institution_type, credentials, function(err,mfaResponse,response){
                res.jsonp({err:err,mfaResponse:mfaResponse,response:response});
            });
        
            return {use_callback:true};
        }catch(e){
            return{error:e};
        }
    };

    this.connect = function(data,req,res){
        if(!plaidClient){
            initPlaidClient();
        }
        try{
            var user = data['username']||'plaid_test';
            var pass = data['password']||'plaid_good';
            
            var credentials = {username:user,password:pass}
            var institution_type = 'wells';//wf
            plaidClient.addConnectUser(institution_type, credentials, function(err,mfaResponse,response){
                res.jsonp({err:err,mfaResponse:mfaResponse,response:response});
            });
        
            return {use_callback:true};
        }catch(e){
            return{error:e};
        }
    };

    this.token = function(data,req,res){
        var public_token = data['token'];
        var callback = function(err,mfaResponse,response){
            res.jsonp({err:err,mfaResponse:mfaResponse,response:response});
        };
        try{
            plaidClient.exchangeToken(public_token, callback);
        }catch(e){
            return{error:e};
        }
    }

    
    
    this.default = function(data,req,res){
        res.jsonp({data:data});
        return {use_callback:true};
    };

    this.html = function(data,req){
        
 
       
        
        return JSON.stringify(self.default(data,req),'','\t\r\n <br/>');
    }
    this.protected = [];
    return self; 
}
module.exports = new Money();