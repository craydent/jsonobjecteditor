function Inventory(){
    var self = this;
    this.default = function(data,req){
        try{ 
            var schemas = data.schemas || JOE.Cache.settings.INVENTORY_SCHEMAS || 'item';
            if(!$c.isArray(schemas)){
                schemas = schemas.split(',');
            }
            var counts = {};
            schemas.map(function(sc){
                counts[sc] = (JOE.Data[sc] || []).length
            })

            var payload = {
                counts:counts
            }
        }catch(e){
            return {errors:'plugin error: '+e,failedat:'plugin'};
        }

        return payload;
    };

    this.find = function(data,req){
        try{ 
            var itemid = data.id;
            var itemtype = data.itemtype;
            var specs = data.specs||{};
            var payload = {};
            
            if(!itemid || !itemtype){
                return {error:'itemid or itemtype not specified'};
            }
            var itemObj = JOE.Cache.findByID(itemtype,itemid);
            if(itemObj){
                payload[itemtype] = itemObj;
                payload.itemtype = itemtype;
                return payload;
            }else{
                return {
                    error:itemtype+' '+itemid+' not found',
                    payload:{
                        id:itemid,
                        itemtype:itemtype,
                        specs:specs
                    }
                }
            }

        }catch(e){
            return {errors:'plugin error: '+e,failedat:'plugin'};
        }

    }
    this.save = function(data,req,res){//create or update
        try{ 
            var itemid = data.id;
            var itemtype = data.itemtype;
            var specs = data.specs || {};
            if(typeof specs =="string"){
                try{
                    specs = eval('('+specs+')');
                }catch(e){
                    specs = {};
                }
            }
            var payload = {};
            
            if(!itemid || !itemtype){
                return {error:'itemid or itemtype not specified'};
            }
            var itemObj = JOE.Cache.findByID(itemtype,itemid);
            if(itemObj){//do update
                itemObj = $c.merge({},itemObj);
                if($c.isEmpty(specs)){
                    payload[itemtype] = itemObj;
                    return payload;
                }else{
                    var object = $c.merge(specs,{joeUpdated:new Date().toISOString()});
                    object = $c.merge(itemObj,specs);
                    JOE.Storage.save(object,itemtype,function(err,data){
                        try{ 
                        if(err){
                            res.jsonp({error:err});
                        }else{
                            var payload = {message:itemtype+' updated'};
                            payload[itemtype] = data;
                            res.jsonp(payload);
                            var results = [data];
                            JOE.io.emit('item_updated', {results: results});
                        }
                        }catch(e){
                            res.jsonp({error:e})
                        }
                    },{user:{name:'inventory'}});
                    return {use_callback:true};
                }
                
            }else{//do create
                var object = $c.merge(specs,{
                    itemtype:itemtype,
                    created:new Date().toISOString(),
                    _id:itemid
                })
                JOE.Storage.save(object,itemtype,function(err,data){
                    try{ 
                    if(err){
                        res.jsonp({error:err});
                    }else{
                        var payload = {message:itemtype+' added'};
                        payload[itemtype] = data;
                        res.jsonp(payload);
                        var results = [data];
                        JOE.io.emit('item_updated', {results: results});
                    }
                    }catch(e){
                        res.jsonp({error:e})
                    }
                },{user:{name:'inventory'}});
                return {use_callback:true};
            }

        }catch(e){
            return {errors:'plugin error: '+e,failedat:'plugin'};
        }

    }
    this.protected = [];
    return self; 
}


module.exports = new Inventory();