function Money() {
  var self = this;
  const plaid = require("plaid");
  var environment = plaid.environments.development;
  var plaidClient;
  var ACCESS_TOKEN = "";
  var PUBLIC_TOKEN = null;

  var ACCESS_TOKENS
  //initPlaidClient();

  function initPlaidClient() {
      //console.log('plaidinit',plaidClient);
      if(plaidClient){ return;}
      logit('initializing plaid client')
    try {
      var client_id = JOE.Cache.settings.PLAID_CLIENTID;
      var secret = JOE.Cache.settings.PLAID_SECRET_development;
      var public_key = JOE.Cache.settings.PLAID_PUBLIC_KEY;
      ACCESS_TOKENS = JOE.Cache.settings.PLAID_ACCESS_TOKENS && JSON.parse(JOE.Cache.settings.PLAID_ACCESS_TOKENS);
      //console.log(ACCESS_TOKENS);
      plaidClient = new plaid.Client(
        client_id,
        secret,
        public_key,
        environment,
        { version: "2018-05-22" }
      );
      return true;
    } catch (e) {
      logit("plaid client init error",e);
      return e;
    }
  }
  function checkPlaidClient(res){
    var p = initPlaidClient();
    if(!plaidClient){
        res.jsonp(p);
        return false;
    }
    return true;
  }

  function getAT(data){
    var AT = ACCESS_TOKENS[data.bank];
    if(!AT){return false;}
    return AT.access_token;
  }

  this.getPublicToken = function(data, request, response){
    var p = initPlaidClient();
    if(!plaidClient){
        return p;
    }
    var AT = ACCESS_TOKENS[data.bank];
    if(!AT)return {error:'bank not found'}
    logit(AT);
    plaidClient.createPublicToken(AT.access_token, (err,data)=>{
        response.jsonp({err,data})
    });
    return { use_callback: true };
  }
  this.getItem = function(data, request, response){
    var p = initPlaidClient();
    if(!plaidClient){
        return p;
    }
    var AT = getAT(data);
    
    if(!AT){return {error:'access token not found',ACCESS_TOKENS};}
    //plaidClient.getItem(ACCESS_TOKEN, cb);
    plaidClient.getItem(AT, (err,pdata)=>{
        response.jsonp({err,pdata,data})
    });
    return { use_callback: true };
  }
  
  this.get_access_token = function(data, request, response) {
    var p = initPlaidClient();
    if(!plaidClient){
        return p;
    }
    console.log(data);
    PUBLIC_TOKEN = (request.query && request.query.public_token) 
    ||(request.body && request.body.public_token);
    plaidClient.exchangePublicToken(PUBLIC_TOKEN, function(
      error,
      tokenResponse
    ) {
      if (error != null) {
        console.log("Could not exchange public_token!" + "\n" + error);
        return response.jsonp({ error });
      }
      ACCESS_TOKEN = tokenResponse.access_token;
      ITEM_ID = tokenResponse.item_id;
      console.log("Access Token: " + ACCESS_TOKEN);
      console.log("Item ID: " + ITEM_ID);
      response.jsonp({ error: false });
    });
    return { use_callback: true };
  };
  this.accounts = function(data, request, response){
    var p = initPlaidClient();
    if(!plaidClient){
        return p;
    }
    var AT = getAT(data);
    if(!AT)return {error:'bank not found'}
    plaidClient.getAccounts(AT, (err, result) => {
        // Handle err
        if(err){
            response.json({error:err});
        }else{
        const accounts = result.accounts;
        response.jsonp(result);
        }
      });
    return { use_callback: true };
  }
  this.accountNumbers = function(data, request, response){
    var p = initPlaidClient();
    if(!plaidClient){
        return p;
    }
    var AT = getAT(data);
    if(!AT)return {error:'bank not found'}
    plaidClient.getAuth(AT,{}, (err, result) => {
        // Handle err
        if(err){
            response.json({error:err});
        }else{
          var accounts = result.accounts;
          response.jsonp(result);
        }
      });
    return { use_callback: true };
  }
  this.transactions = function(data, request, response,specs={}){
    var p = initPlaidClient();
    if(!plaidClient){
        return p;
    }
    var request = request || (data && data.request);
    console.log('params',data);
    var AT = request.query.at || (data.account && data.account.access_token)|| getAT(data);
    var account_ids = request.query.account_ids && (request.query.account_ids.split(',')) || null;
    console.log('accounts',account_ids);
    console.log('AT',AT)
    var curDate = new Date();
    var start_date  = data.start_date || curDate.getFullYear()+'-01-01';
    var end_date  = data.end_date || new Date().format('Y-m-d');
    var count = data.count || 500;
    if(!AT){return {error:'access token not found',ACCESS_TOKENS};}
    plaidClient.getTransactions(AT, start_date, end_date, {count,account_ids}, (error,pdata)=>{

      if(!specs.callback){
        if(error){
          return response.jsonp({error})
        }
        if(!request.query.full){
          return response.jsonp({error,transactions:pdata.transactions})
        }
        response.jsonp({error,pdata,data})
      }else{
        logit('[money] using callback')
        specs.callback(error,pdata,data)
        return;
      }
    });

    if(!specs.async){
      return { use_callback: true };
    }
  }
  this.institutions = function(data, req, res) {
    var p = initPlaidClient();
    if(!plaidClient){
        return p;
    }
    var callback = function(err, mfaResponse, response) {
      res.jsonp({
        err: err,
        mfaResponse: mfaResponse,
        response: response,
        environments: plaid.environments
      });
    };
    if (data["id"]) {
      plaid.getInstitution(data["id"], environment, callback);
    } else {
      plaid.getInstitutions(environment, callback);
    }
    return { use_callback: true };
  };

  this.authTest = function(data, req, res) {
    if (!plaidClient) {
      initPlaidClient();
    }
    try {
      var user = data["username"] || "plaid_test";
      var pass = data["password"] || "plaid_good";

      var credentials = { username: user, password: pass };
      var institution_type = "wells"; //wf
      plaidClient.addAuthUser(institution_type, credentials, function(
        err,
        mfaResponse,
        response
      ) {
        res.jsonp({ err: err, mfaResponse: mfaResponse, response: response });
      });

      return { use_callback: true };
    } catch (e) {
      return { error: e };
    }
  };

  this.connect = function(data, req, res) {
    if (!plaidClient) {
      initPlaidClient();
    }
    try {
      var user = data["username"] || "plaid_test";
      var pass = data["password"] || "plaid_good";

      var credentials = { username: user, password: pass };
      var institution_type = "wells"; //wf
      plaidClient.addConnectUser(institution_type, credentials, function(
        err,
        mfaResponse,
        response
      ) {
        res.jsonp({ err: err, mfaResponse: mfaResponse, response: response });
      });

      return { use_callback: true };
    } catch (e) {
      return { error: e };
    }
  };

  this.token = function(data, req, res) {
    var public_token = data["token"];
    var callback = function(err, mfaResponse, response) {
      res.jsonp({ err: err, mfaResponse: mfaResponse, response: response });
    };
    try {
      plaidClient.exchangeToken(public_token, callback);
    } catch (e) {
      return { error: e };
    }
  };

  this.default = function(data, req, res) {
    res.jsonp({ data: data });
    return { use_callback: true };
  };

  this.html = function(data, req) {
    logit('html route')
    return JSON.stringify(self.default(data, req), "", "\t\r\n <br/>");
  };
  this.protected = [];
  return self;
}
module.exports = new Money();
