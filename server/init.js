var path = require('path');
var fs = require('fs');
var os = require('os');
var pem = require('pem');
var app = function(config){
    var config = config || {};
    global.JOE = {Data:{}};
    var pjson = require(__dirname+'/../package.json');
    JOE.VERSION = pjson.version;
    JOE.STARTDATE =new Date();
    console.log('\n\n<!-- initializing JOE ['+JOE.STARTDATE+']-->');
    console.log('JOE version '+JOE.VERSION);
    console.log('Craydent version '+$c.VERSION);
    console.log('os hostname: '+os.hostname());
    
    this.init = function(){
        JOE.Utils = require('./modules/Utils.js');
        global.colorize = JOE.Utils.color;
        //console.log(colorize('['+new Date()+']','gray')+'\n'+colorize(`[JOE]`,'system')+' init running ');
        JOE.joedir = path.dirname(__dirname);
        JOE.appDir = path.dirname(require.main.filename);
        JOE.webconfig = require('./webconfig.js');
 
        JOE.Fields ={};

        try {
            $c.merge(JOE.webconfig, config);
            if(JOE.webconfig.PORT){
                console.log('omniport found:' +JOE.webconfig.PORT)
                JOE.webconfig.port = JOE.webconfig.socketPort = JOE.webconfig.sitesPort = JOE.webconfig.PORT;
            }
        } catch (e) {
            console.log('Issue loading config json:'+config);
        }
        $c.DEBUG_MODE = !!JOE.webconfig.DEBUG_MODE;
        console.log('DEBUG_MODE: '+$c.DEBUG_MODE);
        console.log(JSON.stringify(JOE.webconfig, '  ','  '));
        //JOE.Apps = require('./modules/Apps.js');
       // var timeouts = {};
        function initializeModule(moduleName){
            var modulePath = __dirname+'/modules/'+moduleName+'.js';
            JOE[moduleName] = require('./modules/'+moduleName+'.js');
            
            console.log(JOE.Utils.color(`[${moduleName}]`,'system')+' init > '+modulePath);

             
            fs.watch(modulePath,function(event,file){
                //clearTimeout(timeouts[moduleName]);
                setTimeout(function(){
                    console.log(JOE.Utils.color('[module]','system')+' '+file+' '+event);
                    if(JOE[moduleName] && JOE[moduleName].exit){
                        
                        JOE[moduleName].exit();
                    }
                    delete require.cache[require.resolve(modulePath)]; 
                    JOE[moduleName] = require(__dirname+'/modules/'+moduleName+'.js');
                    if(JOE[moduleName] && JOE[moduleName].init){
                        JOE[moduleName].init();
                    }
                },100)


            });
        }
        ['Apps','Schemas','Mongo','MySQL','Comments'].map(function(m){
            initializeModule(m);
        });
        //initializeModule('Apps');
        //initializeModule('Schemas');
        //JOE.Schemas = require('./modules/Schemas.js');
        //JOE.Mongo = require('./modules/Mongo.js');
        //JOE.MySQL = require('./modules/MySQL.js');
        //JOE.Comments = require('./modules/Comments.js');
        
        JOE.Storage = require('./modules/Storage.js');
        var cBM = new Benchmarker();
        // JOE.Cache = require('./modules/Cache.js')(function (data) {
        //     console.log(JOE.Utils.color('[cache]','module')+' ready in '+ cBM.stop()+' secs');
        // });

        JOE.Cache = require('./modules/Cache.js');
        JOE.Cache.update(function (data) {
            console.log(JOE.Utils.color('[cache]','module')+' ready in '+ cBM.stop()+' secs');
        });
        if(JOE.webconfig.httpsPort){
            pem.createCertificate({selfSigned:true}, function(err, keys){
                if(err){
                    console.log(err);
                }
                JOE.Pem = keys;
                initServers();
            })
        }else{
            initServers();
        }

        global.$J = require('./modules/UniversalShorthand.js');
        function initServers(){
            JOE.Server = require('./modules/Server.js');

            JOE.Sites = require('./modules/Sites.js');
            JOE.io = require('./modules/Socket.js');
        }
        
    }
    init();
    return this;
}

module.exports =  app;