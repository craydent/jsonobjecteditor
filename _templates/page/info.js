function template_function(data,req) {
    var component = component || {name:Renderizer.resolveName(__filename)};
    var componentName = component.name;
    console.log(req.query);
    var id = req.query.id;
    var schema = req.query.schema;
    var item = false;
    if(id && schema){
        
        item = JOE.Cache.findByID(schema,id);
        console.log(item);
    }

    try {

        var template = `<doctype HTML>
            <html>
            <head><title>${data.title || item && (`${item.itemtype} | ${item.name}`)|| 'untitled'}</title></head>
            <body>
            ${(!item && JSON.stringify(req.query)) || item.name}
            <div>${JSON.stringify(req.params)}</div>
            </body>
            </html>
        `;

        return template;
    } catch (e) {
        return `<e-section>error rendering '${componentName}' component:${e}</e-section>`;
    }
}

module.exports = template_function;