console.time("init");
var cluster = require("cluster");
var path = require("path");
//var opener = require("opener");

var craydent = require("craydent/global");
delete Object.prototype.map;

var spawn = require("child_process").spawn;
var child_process = require("child_process");
var process = require("process");
var fs = require("fs");
var os = require("os");
var filename = ".pid";

var app = function(config) {
    var config = config || require('./server/webconfig.js');
  if (config.clusters) {
    if (cluster.isMaster) {
      try {
        var pidfile = fs.readFileSync(filename, "utf8");
        if (pidfile) {
          console.log("killing " + pidfile);
          process.kill(pidfile);
        }
      } catch (e) {
        console.log(e);
      }
      if (process.pid) {
        //process.stdout.write(process.pid);
        console.log(
          "This process is your pid " + process.pid + " on " + process.platform
        );

        fs.writeFileSync(filename, process.pid);
      }
      function spawnWorker() {
        var worker = cluster.fork();
        worker.on("exit", function(worker, code, signal) {
          try {
            console.log("worker killed", cluster.workers[worker]);
            console.log(worker.pid);
          } catch (e) {
            console.log(
              JOE.Utils.color("[worker]", "error") + " exit error: " + e
            );
          }
          spawnWorker();
        });
      }
      // Fork workers.
      for (var i = 0; i < 1; i++) {
        spawnWorker();
      }
      //console.log(cluster.workers);
    } else {
      if (process.pid) {
        console.log("worker process spawned: " + process.pid);
      }
      var App = require("./server/init.js")(config);

      if (
        JOE.Utils.propAsFuncOrValue(JOE.webconfig.openWindow, os.hostname())
      ) {
        var adminPageURL = "http://localhost:" + JOE.webconfig.port + "/JOE/";
        console.log("opening: " + adminPageURL);
        //var adminPage = opener(adminPageURL);
        console.timeEnd("init");
      }
    }
  }else{//no clusters mode
    try {
      var pidfile = fs.readFileSync(filename, "utf8");
      if (pidfile) {
        console.log("killing " + pidfile);
        process.kill(pidfile);
      }
    } catch (e) {
      console.log(e);
    }
    if (process.pid) {
      //process.stdout.write(process.pid);
      console.log(
        "This process is your pid " + process.pid + " on " + process.platform
      );

      fs.writeFileSync(filename, process.pid.toString());
    }
    var App = require("./server/init.js")(config);

    if (
      JOE.Utils.propAsFuncOrValue(JOE.webconfig.openWindow, os.hostname())
    ) {
      var adminPageURL = "http://localhost:" + JOE.webconfig.port + "/JOE/";
      console.log("opening: " + adminPageURL);
      //var adminPage = opener(adminPageURL);
      console.timeEnd("init");
    }
  }
};
if (module == require.main) {
  app();
}
module.exports = app;
